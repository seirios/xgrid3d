#include <errno.h>

#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "cb_contour.h"
#include "cb_overview.h"
#include "color_gradient.h"

#include "util.h"

#define FORMAT_CONTOUR "%11.6g Gy %5.3g%%"

/* activate objects in contour form */
static void _activate_contour_obj(FD_contour *contour) {
	activate_obj(contour->spin_nc,FL_MCOL);
	activate_obj(contour->cnt_exp,FL_YELLOW);
	activate_obj(contour->btn_auto,FL_COL1);
	activate_obj(contour->ch_lut,FL_BLACK);
	activate_obj(contour->inp_contour,FL_MCOL);
	activate_obj(contour->br_contour,FL_BLACK);
	activate_obj(contour->br_contour_lut,FL_BLACK);
	activate_obj(contour->btn_add,FL_COL1);
	activate_obj(contour->btn_mod,FL_COL1);
	activate_obj(contour->btn_rem,FL_COL1);
	activate_obj(contour->btn_clr,FL_INDIANRED);
	activate_obj(contour->frame_auto,FL_BLACK);
    activate_obj(contour->label_pick,FL_BLACK);
}

/* deactivate objects in contour form */
static void _deactivate_contour_obj(FD_contour *contour) {
	deactivate_obj(contour->spin_nc);
	deactivate_obj(contour->cnt_exp);
	deactivate_obj(contour->btn_auto);
	deactivate_obj(contour->ch_lut);
	deactivate_obj(contour->inp_contour);
	deactivate_obj(contour->br_contour);
	deactivate_obj(contour->br_contour_lut);
	deactivate_obj(contour->btn_add);
	deactivate_obj(contour->btn_mod);
	deactivate_obj(contour->btn_rem);
	deactivate_obj(contour->btn_clr);
	deactivate_obj(contour->frame_auto);
    deactivate_obj(contour->label_pick);
}

/* refresh contour levels browser */
static void _refresh_contour_browser(FD_contour *contour, const double min, const double max) {
	FL_OBJECT *browser_lut = contour->br_contour_lut;
	FL_OBJECT *browser = contour->br_contour;
	const FL_PACKED *lut = CONTOUR_LUT(contour);
	const unsigned int nc = CONTOUR_NC(contour);
	const double *z = CONTOUR_Z(contour);

	fl_freeze_form(contour->contour);
	fl_clear_browser(browser);
	fl_clear_browser(browser_lut);
    fl_set_object_color(browser,FL_WHITE,FL_YELLOW);
    fl_set_object_color(browser_lut,FL_WHITE,FL_YELLOW);
	for(unsigned int i=0;i<nc;i++) {
		fl_add_browser_line_f(browser,FORMAT_CONTOUR,z[i],(z[i]-min)/(max-min)*100.0);
        fl_mapcolor(FL_FREE_COL1+i,FL_GETR(lut[i]),FL_GETG(lut[i]),FL_GETB(lut[i]));
		fl_add_browser_line_f(browser_lut,"@C%d██",FL_FREE_COL1+i);
    }
	fl_set_input(contour->inp_contour,"");
	fl_unfreeze_form(contour->contour);
}

/* show contour form */
void cb_contour_show ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const unsigned int nc = CONTOUR_NC(GUI->contour);

	if(fl_get_button(obj)) {
		if(ENV->data->dose != NULL) {
			_activate_contour_obj(GUI->contour);
			nc == 0 ? fl_call_object_callback(GUI->contour->btn_auto) :
                _refresh_contour_browser(GUI->contour,
                        ENV->data->dose->stats->min,
                        ENV->data->dose->stats->max);
			refresh_ov_canvases(GUI->overview);
		} else {
			fl_show_alert2(0,_("No dose estimate!\fPlease load or compute a dose estimate first."));
			fl_set_button(obj,0);
		}
	} else {
		_deactivate_contour_obj(GUI->contour);
		refresh_ov_canvases(GUI->overview);
	}
}

/* auto contour levels as intervals */
void cb_contour_auto ( FL_OBJECT * obj  , long data  ) {
    (void)data;
	const unsigned int nc = fl_get_spinner_value(GUI->contour->spin_nc);
	const double exp = fl_get_counter_value(GUI->contour->cnt_exp);
    const double min = ENV->data->dose->stats->min;
    const double rng = ENV->data->dose->stats->max - min;
    double **z = (double**)&CONTOUR_Z(GUI->contour);

    CONTOUR_NC(GUI->contour) = nc;
	*z = realloc(*z,nc*sizeof(double));
	for(unsigned int i=0;i<nc;i++)
		(*z)[i] = min + rng * pow((1.0/(double)(nc+1))*(i+1),exp);
	fl_call_object_callback(GUI->contour->ch_lut);
}

/* contour browser callback */
void cb_contour_browser ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const double *z = CONTOUR_Z(GUI->contour);
    int ix;

    switch(fl_get_object_return_state(obj)) {
        case FL_RETURN_SELECTION:
            if((ix = fl_get_browser(obj)) >= 1)
                fl_set_input_f(GUI->contour->inp_contour,"%.8g",z[ix-1]);
            break;
        case FL_RETURN_DESELECTION:
                fl_set_input(GUI->contour->inp_contour,"");
            break;
        case FL_RETURN_CHANGED:
        case FL_RETURN_CHANGED | FL_RETURN_END:
            fl_set_browser_yoffset(GUI->contour->br_contour_lut,fl_get_browser_yoffset(obj));
            break;
    }
}

/* set contour color with picker */
void cb_contour_browser_lut ( FL_OBJECT * obj  , long data  ) {
    (void)data;
	const int k = fl_get_browser(GUI->contour->br_contour_lut);
    FL_PACKED *lut = CONTOUR_LUT(GUI->contour);
    const int rgb_in[3] = {
        FL_GETR(lut[k-1]),
        FL_GETG(lut[k-1]),
        FL_GETB(lut[k-1])
    };
    int rgb_out[3];

	if(k == 0) return;

    if(fl_show_color_chooser(rgb_in,rgb_out)) {
        lut[k-1] = FL_PACK(rgb_out[0],rgb_out[1],rgb_out[2]);
        _refresh_contour_browser(GUI->contour,
                ENV->data->dose->stats->min,ENV->data->dose->stats->max);
        if(fl_get_button(GUI->contour->btn_show))
            refresh_ov_canvases(GUI->overview);
    }
}

/* contour browser operations */
void cb_contour_op ( FL_OBJECT * obj  , long data  ) {
    const FL_PACKED *lut = CONTOUR_LUT(GUI->contour);
    double **zc = (double**)&CONTOUR_Z(GUI->contour);
    const unsigned int nc = CONTOUR_NC(GUI->contour);
	const int ix = fl_get_browser(GUI->contour->br_contour);
    const double min = ENV->data->dose->stats->min;
    const double max = ENV->data->dose->stats->max;
	const char *contxt;
	double z;

	switch(data) {
		case 0: /* Add */
            if(nc == MAX_CONTOURS - 1) {
                fl_show_alert2(0,_("Too many contours!\fMaximum number of contours reached."));
                return;
            }
            contxt = fl_get_input(GUI->contour->inp_contour);
            if(contxt == NULL || strlen(contxt) == 0) {
                fl_show_alert2(0,_("Empty input!\fPlease enter a contour value."));
                return;
            }
            z = strtod(contxt,NULL);
            if((z == -HUGE_VAL || z == HUGE_VAL) && errno == ERANGE) return;
            if(bsearch(&z,*zc,nc,sizeof(double),cmp_double) != NULL) {
                fl_show_alert2(0,_("Duplicate found!\fPlease enter a unique contour value."));
                return;
            }
            *zc = realloc(*zc,(nc+1)*sizeof(double));
            (*zc)[nc] = z;
            qsort(*zc,nc+1,sizeof(double),cmp_double);
            CONTOUR_NC(GUI->contour)++;
            _refresh_contour_browser(GUI->contour,min,max);
            fl_set_input(GUI->contour->inp_contour,"");
            fl_call_object_callback(GUI->contour->ch_lut);
            break;
        case 1: /* Modify */
            if(ix == 0) return;
            contxt = fl_get_input(GUI->contour->inp_contour);
            if(contxt == NULL || strlen(contxt) == 0) {
                fl_show_alert2(0,_("Empty input!\fPlease enter a contour value."));
                return;
            }
            z = strtod(contxt,NULL); if((z == -HUGE_VAL || z == HUGE_VAL) && errno == ERANGE) return;
            if(bsearch(&z,*zc,nc,sizeof(double),cmp_double) != NULL) {
                fl_show_alert2(0,_("Duplicate found!\fPlease enter a unique contour value."));
                return;
            }
            fl_mapcolor(FL_FREE_COL1+ix-1,FL_GETR(lut[ix-1]),FL_GETG(lut[ix-1]),FL_GETB(lut[ix-1]));
            fl_replace_browser_line_f(GUI->contour->br_contour,ix,FORMAT_CONTOUR,FL_FREE_COL1+ix-1,z,(z-min)/(max-min)*100.0);
            (*zc)[ix-1] = z;
            qsort(*zc,nc,sizeof(double),cmp_double);
            fl_set_input(GUI->contour->inp_contour,"");
            fl_call_object_callback(GUI->contour->ch_lut);
            break;
		case 2: /* Remove */
            if(ix == 0) return;
            fl_delete_browser_line(GUI->contour->br_contour,ix);
            memmove(&(*zc)[ix-1],&(*zc)[ix],(nc-ix)*sizeof(double));
            *zc = realloc(*zc,(nc-1)*sizeof(double));
            CONTOUR_NC(GUI->contour)--;
            fl_set_input(GUI->contour->inp_contour,"");
            fl_call_object_callback(GUI->contour->ch_lut);
            break;
		case 3: /* Clear */
			free(*zc); *zc = NULL;
            CONTOUR_NC(GUI->contour) = 0;
			fl_clear_browser(GUI->contour->br_contour);
			fl_call_object_callback(GUI->contour->ch_lut);
			break;
	}
}

/* contour level input callback */
void cb_contour_inp ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const double min = ENV->data->dose->stats->min;
    const double rng = ENV->data->dose->stats->max - min;
	const char *tval = fl_get_input(obj);
	char *eptr;
	double val;
	
	if(tval != NULL && strlen(tval) > 0 && fl_validate_input(obj) == FL_VALID) {
		val = strtod(tval,&eptr);
		if(*eptr == '%') {
			val = min + rng * val/100.0;
			fl_set_input_f(obj,"%.8g",val);
		}
	}
}

/* contour LUT choice callback */
void cb_contour_lut ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    FL_PACKED **lut = (FL_PACKED**)&CONTOUR_LUT(GUI->contour);
    const unsigned int nc = CONTOUR_NC(GUI->contour);
	bool inv = false,rev = false;

	*lut = realloc(*lut,nc*sizeof(FL_PACKED));

	_colorf *colorf = NULL;
	switch(fl_get_choice(obj)) {
		case 1: colorf = _grad_nih;      break; /* NIH */
		case 2: colorf = _grad_fire;     break; /* Fire */
		case 3: colorf = _grad_hotmetal; break; /* Hotmetal */
		case 4: colorf = _grad_hotmetal;
                inv = rev = true;        break; /* Cold ice */
		case 5: colorf = _grad_msh;      break; /* Diverging */
		case 6: colorf = _grad_rand;     break; /* Random */
	}
	
	uint8_t *rgb = colorf(nc,0.0);
	for(unsigned int i=0;i<nc;i++) {
		unsigned int ix = rev ? nc-i-1 : i;
        uint8_t r = rgb[3*i];
        uint8_t g = rgb[3*i+1];
        uint8_t b = rgb[3*i+2];
		if(inv) { r = ~r; g = ~g; b = ~b; }
		(*lut)[ix] = FL_PACK(r,g,b);
	}
	free(rgb);

	_refresh_contour_browser(GUI->contour,
            ENV->data->dose->stats->min,ENV->data->dose->stats->max);
	if(fl_get_button(GUI->contour->btn_show))
		refresh_ov_canvases(GUI->overview);
}
