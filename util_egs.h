#ifndef XG3D_util_egs_h_
#define XG3D_util_egs_h_

#include <stdlib.h>
#include <inttypes.h>

#define DOSXYZNRC_INPUT_LEN 45
#define DOSXYZPP_INPUT_LEN 67

char** setup_dosxyznrc_input(const char*,const char*,const double,const double,const uintmax_t,const unsigned int,const int,const int,const char*);
void free_dosxyznrc_input(char**);

char** setup_dosxyzpp_input(const char*,const char*,const double,const double,const uintmax_t,const unsigned int,const int,const int,const char*);
void free_dosxyzpp_input(char**);

char* serialize_egs_input(char**,const size_t);
int export_egs_input(const char*,char**,const size_t);

#endif
