#ifndef XG3D_util_units_h_
#define XG3D_util_units_h_

enum unit_time {
	UNIT_SECOND,
	UNIT_MINUTE,
	UNIT_HOUR,
	UNIT_DAY,
	UNIT_YEAR
};

enum unit_length {
	UNIT_MM,
	UNIT_CM
};

enum unit_area {
    UNIT_MM2,
    UNIT_CM2
};

enum unit_volume {
    UNIT_MM3,
    UNIT_CM3
};

enum unit_activity {
	UNIT_BECQUEREL,
	UNIT_MUCURIE
};

#define MINUTE_TO_SECOND           60.0
#define HOUR_TO_SECOND           3600.0
#define HOUR_TO_MINUTE             60.0
#define DAY_TO_SECOND           86400.0
#define DAY_TO_MINUTE            1440.0
#define DAY_TO_HOUR                24.0
#define YEAR_TO_SECOND       31536000.0
#define YEAR_TO_MINUTE         525600.0
#define YEAR_TO_HOUR             8760.0
#define YEAR_TO_DAY               365.0

#define SECOND_TO_MINUTE            0.016666666666666666667
#define SECOND_TO_HOUR              0.00027777777777777777778
#define MINUTE_TO_HOUR              0.016666666666666666667
#define SECOND_TO_DAY               0.000011574074074074074074
#define MINUTE_TO_DAY               0.00069444444444444444444
#define HOUR_TO_DAY                 0.041666666666666666667
#define SECOND_TO_YEAR              0.000000031709791983764586504
#define MINUTE_TO_YEAR              0.0000019025875190258751903
#define HOUR_TO_YEAR                0.00011415525114155251142
#define DAY_TO_YEAR                 0.0027397260273972602740

#define MICROCURIE_TO_BECQUEREL 37000.0
#define BECQUEREL_TO_MICROCURIE     0.000027027027027027027027

#define M_TO_CM                   100.0
#define CM_TO_M                     0.01

#define CM_TO_MM                   10.0
#define MM_TO_CM	                0.1

#define MICRON_TO_CM                0.0001
#define CM_TO_MICRON            10000.0

#define CM3_TO_MM3               1000.0
#define MM3_TO_CM3                  0.001

#define CM2_TO_MM2                100.0
#define MM2_TO_CM2                  0.01

#endif
