#ifndef XG3D_rois_cb_h_
#define XG3D_rois_cb_h_

#include "fd_rois.h"
#include "xgrid3d.h"

#define ROI_SELECTED(fd_rois) (((FD_rois*)(fd_rois))->bit_hist->u_ldata)

void xroi_free(xg3d_roi*);
xg3d_roi** xrois_dup(xg3d_roi**,const nroi_t);
void xrois_free(xg3d_roi**,const nroi_t);
void update_roi_info(xg3d_env*);
void update_roi_values(xg3d_env*);
void refresh_roi_browsers(xg3d_env*);
void refresh_overlay_rois(xg3d_env*);

FL_HANDLE pre_roi_hist;
FL_HANDLE pre_roi_color;

#endif
