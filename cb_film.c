#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "cb_datasets.h"
#include "cb_film.h"
#include "cb_overview.h"
#include "cb_selection.h"
#include "cb_dose.h"
#include "fd_contour.h"
#include "fd_dvhs.h"

#include <gsl/gsl_sort_double.h>
#include <gsl/gsl_statistics_float.h>

#include "selection.h"

#include "io.h"
#include "util.h"
#include "util_fit2d.h"

#include "response_1.xbm"
#include "response_2.xbm"
#include "response_3.xbm"

typedef double ( RESPONSE_FUNC ) ( const double I  , const double I_ref  , const double I_0  ) ;

/* response/error/dose/error functions */
static double response_intensity ( const double I  , const double I_ref  , const double I_0  ) {
    (void)I_ref;
(void)I_0;
    return I;
}

static double response_OD ( const double I  , const double I_ref  , const double I_0  ) {
    (void)I_0;
    return log10(I_ref/I);
}

static double response_ODbkg ( const double I  , const double I_ref  , const double I_0  ) {
    return log10((I_ref-I_0)/(I-I_0));
}

typedef double ( RESPONSE_ERR_FUNC ) ( const double I  , const double err  , const double I_ref  , const double err_ref  , const double I_0  , const double err_0  ) ;

static double response_err_intensity ( const double I  , const double err  , const double I_ref  , const double err_ref  , const double I_0  , const double err_0  ) {
    (void)I;
(void)I_ref;
(void)err_ref;
(void)I_0;
(void)err_0;
    return err;
}

static double response_err_OD ( const double I  , const double err  , const double I_ref  , const double err_ref  , const double I_0  , const double err_0  ) {
    (void)I_0;
(void)err_0;
    return sqrt(SQR(err/I) + SQR(err_ref/I_ref)) / M_LN10;
}

static double response_err_ODbkg ( const double I  , const double err  , const double I_ref  , const double err_ref  , const double I_0  , const double err_0  ) {
    return sqrt((SQR(err) + SQR(err_0))/SQR(I-I_0) + (SQR(err_ref) + SQR(err_0))/SQR(I_ref-I_0)) / M_LN10;
}

typedef double ( DOSE_FUNC ) ( const double R  , const double a  , const double b  , const double c  ) ;

static double dose_intensity ( const double R  , const double a  , const double b  , const double c  ) {
    return c + b / (R - a);
}

static double dose_OD ( const double R  , const double a  , const double b  , const double c  ) {
    const double x = pow(10.0,R);
    return (c - a*x) / (b*x - 1.0);
}

typedef double ( DOSE_ERR_FUNC ) ( const double R  , const double err_R  , const double a  , const double err_a  , const double b  , const double err_b  , const double c  , const double err_c  ) ;

static double dose_err_intensity ( const double R  , const double err_R  , const double a  , const double err_a  , const double b  , const double err_b  , const double c  , const double err_c  ) {
    (void)c;
    const double bsq = SQR(b), rmasq = SQR(R-a);
    return sqrt(SQR(err_c) + bsq/rmasq * (SQR(err_b)/bsq + (SQR(err_R) + SQR(err_a))/rmasq));
}

static double dose_err_OD ( const double R  , const double err_R  , const double a  , const double err_a  , const double b  , const double err_b  , const double c  , const double err_c  ) {
    const double x = pow(10.0,R), D = dose_OD(R,a,b,c);
    return D * x * sqrt((SQR(err_c / x) + SQR(err_a) + a * SQR(M_LN10 * err_R)) / SQR(c - a * x) +
            (SQR(err_b) + b * SQR(M_LN10 * err_R)) / SQR(b * x - 1.0));
}

void cb_film_response ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    switch(fl_get_choice(obj)) {
        case 1: /* Raw intensity */
            fl_set_bitmap_data(GUI->film->bit_response,response_1_width,response_1_height,response_1_bits);
            fl_set_input(GUI->film->inp_ref,""); deactivate_obj(GUI->film->inp_ref);
            fl_set_input(GUI->film->inp_ref_err,""); deactivate_obj(GUI->film->inp_ref_err);
            deactivate_obj(GUI->film->btn_ref_fromsel);
            fl_set_input(GUI->film->inp_bkg,""); deactivate_obj(GUI->film->inp_bkg);
            fl_set_input(GUI->film->inp_bkg_err,""); deactivate_obj(GUI->film->inp_bkg_err);
            deactivate_obj(GUI->film->btn_bkg_fromsel);
            break;
        case 2: /* Optical density */
            fl_set_bitmap_data(GUI->film->bit_response,response_2_width,response_2_height,response_2_bits);
            activate_obj(GUI->film->inp_ref,FL_MCOL); activate_obj(GUI->film->inp_ref_err,FL_MCOL);
            activate_obj(GUI->film->btn_ref_fromsel,FL_YELLOW);
            fl_set_input(GUI->film->inp_bkg,""); deactivate_obj(GUI->film->inp_bkg);
            fl_set_input(GUI->film->inp_bkg_err,""); deactivate_obj(GUI->film->inp_bkg_err);
            deactivate_obj(GUI->film->btn_bkg_fromsel);
            break;
        case 3: /* Optical density with background */
            fl_set_bitmap_data(GUI->film->bit_response,response_3_width,response_3_height,response_3_bits);
            activate_obj(GUI->film->inp_ref,FL_MCOL); activate_obj(GUI->film->inp_ref_err,FL_MCOL);
            activate_obj(GUI->film->btn_ref_fromsel,FL_YELLOW);
            activate_obj(GUI->film->inp_bkg,FL_MCOL); activate_obj(GUI->film->inp_bkg_err,FL_MCOL);
            activate_obj(GUI->film->btn_bkg_fromsel,FL_YELLOW);
            break;
    }

    /* Reset calibration */
    fl_clear_browser(GUI->film->br_calib_red); fl_clear_xyplot(GUI->film->plot_calib_red);
    fl_clear_browser(GUI->film->br_calib_green); fl_clear_xyplot(GUI->film->plot_calib_green);
    fl_clear_browser(GUI->film->br_calib_blue); fl_clear_xyplot(GUI->film->plot_calib_blue);
    fl_set_input(GUI->film->inp_dose,""); fl_set_input(GUI->film->inp_resp,"");
    fl_set_object_label(GUI->film->txt_fit,"");
}

void cb_film_calibration_op ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *br = NULL, *plot = NULL;
    const char *tdose = fl_get_input(GUI->film->inp_dose),
               *tresp = fl_get_input(GUI->film->inp_resp),
               *terr  = fl_get_input(GUI->film->inp_resp_err);
    char buf[64];

    if(fl_get_button(GUI->film->btn_red)) {
        br = GUI->film->br_calib_red;
        plot = GUI->film->plot_calib_red;
    } else if(fl_get_button(GUI->film->btn_green)) {
        br = GUI->film->br_calib_green;
        plot = GUI->film->plot_calib_green;
    } else if(fl_get_button(GUI->film->btn_blue)) {
        br = GUI->film->br_calib_blue;
        plot = GUI->film->plot_calib_blue;
    }

    switch(data) {
        case 0: { /* Add */
            if(strlen(tdose) == 0 || strlen(tresp) == 0) {
                fl_show_alert2(0,_("Missing dose or response!\fPlease check input fields."));
                return;
            }
            double dose = strtod(tdose,NULL);
            double resp = strtod(tresp,NULL);
            double err = strlen(terr) != 0 ? strtod(terr,NULL) : 0.0;
            fl_add_browser_line_f(br,"%8g Gy %g (%g)",dose,resp,err);
            fl_set_input(GUI->film->inp_dose,"");
            fl_set_input(GUI->film->inp_resp,"");
            fl_set_input(GUI->film->inp_resp_err,"");
            int n = fl_get_xyplot_numdata(plot,0);
            if(n == 0)
                fl_set_xyplot_data_double(plot,&dose,&resp,1,"",_("Dose [Gy]"),_("Response"));
            else
                fl_insert_xyplot_data(plot,0,n,dose,resp);
            fl_insert_xyplot_data(plot,1,n,dose,err);
            } break;
        case 1: { /* Remove */
            int ix = fl_get_browser(br);
            if(ix == 0) return;
            int n = fl_get_xyplot_data_size(plot);
            if(n == 1)
                fl_call_object_callback(GUI->film->btn_clear);
            else {
                fl_delete_browser_line(br,ix);
                float *x = malloc(n*sizeof(float));
                float *y = malloc(n*sizeof(float));
                fl_get_xyplot_data(plot,x,y,&n);
                memmove(x+ix-1,x+ix,(n-ix)*sizeof(float));
                memmove(y+ix-1,y+ix,(n-ix)*sizeof(float));
                fl_set_xyplot_data(plot,x,y,n-1,"",_("Dose [Gy]"),_("Response"));
                fl_get_xyplot_overlay_data(plot,1,x,y,&n);
                memmove(x+ix-1,x+ix,(n-ix)*sizeof(float));
                memmove(y+ix-1,y+ix,(n-ix)*sizeof(float));
                fl_add_xyplot_overlay(plot,1,x,y,n-1,FL_BLACK);
                free(x); free(y);
                fl_set_input(GUI->film->inp_dose,"");
                fl_set_input(GUI->film->inp_resp,"");
                fl_set_input(GUI->film->inp_resp_err,"");
            }
            } break;
        case 2: { /* Replace */
            int ix = fl_get_browser(br);
            if(ix == 0) return;
            if(strlen(tdose) == 0 || strlen(tresp) == 0) {
                fl_show_alert2(0,_("Missing values!\fPlease check input fields."));
                return;
            }
            double dose = strtod(tdose,NULL);
            double resp = strtod(tresp,NULL);
            double err = strlen(terr) != 0 ? strtod(terr,NULL) : 0.0;
            fl_replace_browser_line_f(br,ix,"%8g Gy %g (%g)",dose,resp,err);
            fl_replace_xyplot_point(plot,ix-1,dose,resp);
            int n; float *x,*y;
            fl_get_xyplot_data_pointer(plot,1,&x,&y,&n);
            x[ix-1] = dose; y[ix-1] = err;
            fl_redraw_object(plot);
            } break;
        case 3: { /* Clear */
            fl_clear_browser(br);
            fl_clear_xyplot(plot);
            fl_set_object_label(GUI->film->txt_fit,"");
            fl_set_input(GUI->film->inp_dose,"");
            fl_set_input(GUI->film->inp_resp,"");
            fl_set_input(GUI->film->inp_resp_err,"");
            } break;
        case 4: { /* Load */
            const char *file = fl_show_fselector(_("Load calibration from..."),"","*","");
            if(file == NULL) return;
            FILE *fp = fopen(file,"r");
            if(fp == NULL) {
                fl_show_alert2(0,_("Cannot open file for reading!\f%s"),file);
                return;
            }
            fl_freeze_form(GUI->film->film);
            int n = 0;
            double val;
            ssize_t read;
            size_t len = 0;
            char *line = NULL;
            while((read = getline(&line,&len,fp)) != -1) {
                if(line[0] == '#') {
                    if(strncmp(line+1,"RESPONSE",8) == 0) {
                        sscanf(line,"#RESPONSE: %s",buf);
                        if(strcmp(buf,"intensity") == 0) {
                            fl_set_choice(GUI->film->ch_response,1);
                            fl_call_object_callback(GUI->film->ch_response);
                        } else if(strcmp(buf,"optical_density") == 0) {
                            fl_set_choice(GUI->film->ch_response,2);
                            fl_call_object_callback(GUI->film->ch_response);
                        } else if(strcmp(buf,"optical_density_with_background") == 0) {
                            fl_set_choice(GUI->film->ch_response,3);
                            fl_call_object_callback(GUI->film->ch_response);
                        }
                    } else if(strncmp(line+1,"REFERENCE",9) == 0) {
                        sscanf(line,"#REFERENCE: %lf",&val);
                        fl_set_input_f(GUI->film->inp_ref,"%g",val);
                    } else if(strncmp(line+1,"BACKGROUND",10) == 0) {
                        sscanf(line,"#BACKGROUND: %lf",&val);
                        fl_set_input_f(GUI->film->inp_bkg,"%g",val);
                    } else if(strncmp(line+1,"CHANNEL",7) == 0) {
                        sscanf(line,"#CHANNEL: %s",buf);
                        if(strcmp(buf,"red") == 0) {
                            plot = GUI->film->plot_calib_red;
                            br = GUI->film->br_calib_red;
                            fl_set_button(GUI->film->btn_red,1);
                            fl_call_object_callback(GUI->film->btn_red);
                        } else if(strcmp(buf,"green") == 0) {
                            plot = GUI->film->plot_calib_green;
                            br = GUI->film->br_calib_green;
                            fl_set_button(GUI->film->btn_green,1);
                            fl_call_object_callback(GUI->film->btn_green);
                        } else if(strcmp(buf,"blue") == 0) {
                            plot = GUI->film->plot_calib_blue;
                            br = GUI->film->br_calib_blue;
                            fl_set_button(GUI->film->btn_blue,1);
                            fl_call_object_callback(GUI->film->btn_blue);
                        }
                        fl_clear_xyplot(plot);
                        fl_clear_browser(br);
                    }
                } else {
                    double dose,resp,err; sscanf(line,"%lf %lf %lf",&dose,&resp,&err);
                    if(n == 0)
                        fl_set_xyplot_data_double(plot,&dose,&resp,1,"",_("Dose [Gy]"),_("Response"));
                    else
                        fl_insert_xyplot_data(plot,0,n,dose,resp);
                    fl_insert_xyplot_data(plot,1,n,dose,err);
                    fl_add_browser_line_f(br,"%8g Gy %g (%g)",dose,resp,err);
                    n++;
                }
            }
            free(line);
            fl_unfreeze_form(GUI->film->film);
            } break;
        case 5: { /* Save */
            if(fl_get_browser_maxline(br) == 0) return;
            const char *file = fl_show_fselector(_("Save calibration to..."),"","*","");
            if(file == NULL) return;
            FILE *fp = fopen(file,"w");
            if(fp == NULL) {
                fl_show_alert2(0,_("Cannot open file for writing!\f%s"),file);
                return;
            }
            switch(fl_get_choice(GUI->film->ch_response)) {
                case 1:
                    fprintf(fp,"#RESPONSE: raw_intensity\n");
                    break;
                case 2:
                    fprintf(fp,"#RESPONSE: optical_density\n");
                    fprintf(fp,"#REFERENCE: %s\n",fl_get_input(GUI->film->inp_ref));
                    break;
                case 3:
                    fprintf(fp,"#RESPONSE: optical_density_with_background\n");
                    fprintf(fp,"#REFERENCE: %s\n",fl_get_input(GUI->film->inp_ref));
                    fprintf(fp,"#BACKGROUND: %s\n",fl_get_input(GUI->film->inp_bkg));
                    break;
            }
            if(fl_get_button(GUI->film->btn_red))
                fprintf(fp,"#CHANNEL: red\n");
            else if(fl_get_button(GUI->film->btn_green))
                fprintf(fp,"#CHANNEL: green\n");
            else if(fl_get_button(GUI->film->btn_blue))
                fprintf(fp,"#CHANNEL: blue\n");
            for(int i=1;i<=fl_get_browser_maxline(br);i++) {
                double dose,resp,err;
                sscanf(fl_get_browser_line(br,i),"%lf Gy %lf (%lf)",&dose,&resp,&err);
                fprintf(fp,"%g %g %g\n",dose,resp,err);
            }
            fclose(fp);
            } break;
    }

    if(data == 0 || data == 1 || data == 2 || data == 4) {
        int n; float *x,*y;
        fl_get_xyplot_data_pointer(plot,0,&x,&y,&n);
        if(x != NULL && y != NULL && n > 0) {
            float min,max,rng;
            gsl_stats_float_minmax(&min,&max,x,1,n); rng = max - min;
            fl_set_xyplot_xbounds(plot,min-0.05*rng,max+0.05*rng);
            gsl_stats_float_minmax(&min,&max,y,1,n); rng = max - min;
            fl_set_xyplot_ybounds(plot,min-0.05*rng,max+0.05*rng);
        }
    }
}

void cb_film_calibration_browser ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    double dose,resp,err;
    const char *line;

    line = fl_get_browser_line(obj,fl_get_browser(obj));
    sscanf(line,"%lf Gy %lf (%lf)",&dose,&resp,&err);
    fl_set_input_f(GUI->film->inp_dose,"%g",dose);
    fl_set_input_f(GUI->film->inp_resp,"%g",resp);
    fl_set_input_f(GUI->film->inp_resp_err,"%g",err);
}

void cb_film_calibration_channel ( FL_OBJECT * obj  , long data  ) {
    struct fit_result *fit = NULL;

    switch(data) {
        case 0: /* Red */
            fit = FILM_FIT_RESULT(GUI->film,red);
            fl_hide_object(GUI->film->grp_film_calib_green);
            fl_hide_object(GUI->film->grp_film_calib_blue);
            fl_show_object(GUI->film->grp_film_calib_red);
            break;
        case 1: /* Green */
            fit = FILM_FIT_RESULT(GUI->film,green);
            fl_hide_object(GUI->film->grp_film_calib_red);
            fl_hide_object(GUI->film->grp_film_calib_blue);
            fl_show_object(GUI->film->grp_film_calib_green);
            break;
        case 2: /* Blue */
            fit = FILM_FIT_RESULT(GUI->film,blue);
            fl_hide_object(GUI->film->grp_film_calib_red);
            fl_hide_object(GUI->film->grp_film_calib_green);
            fl_show_object(GUI->film->grp_film_calib_blue);
            break;
    }

    if(fit == NULL)
        fl_set_object_label(GUI->film->txt_fit,"");
    else
        fl_set_object_label_f(GUI->film->txt_fit,
                "%s\n\na: %g\n ± %g\nb: %g\n ± %g\nc: %g\n ± %g",_("Fit parameters"),
                fit->par[0],fit->err[0],fit->par[1],fit->err[1],fit->par[2],fit->err[2]);
}

void cb_film_fromsel ( FL_OBJECT * obj  , long data  ) {
    xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);

    if(DATA->g3d == NULL || swk->nvox == 0) return;

    double mean = 0.0;
    double sd = 0.0;
    index_t nvx = 0;
    for(index_t ix=0;ix<swk->g3d->nvox;ix++)
        if(g3d_selbit_check(swk->g3d,ix)) {
            double dat = g3d_get_dvalue(DATA->g3d,ix);
            double del = dat - mean;
            mean += del / ++nvx;
            sd += del * (dat - mean);
        }
    sd = nvx >= 2 ? sqrt(sd / (nvx - 1)) : 0.0;

    switch(data) {
        case 0: { /* Reference */
            fl_set_input_f(GUI->film->inp_ref,"%g",mean);
            fl_set_input_f(GUI->film->inp_ref_err,"%g",sd);
            } break;
        case 1: { /* Background */
            fl_set_input_f(GUI->film->inp_bkg,"%g",mean);
            fl_set_input_f(GUI->film->inp_bkg_err,"%g",sd);
            } break;
        case 2: { /* Response */
            RESPONSE_FUNC *response;
            RESPONSE_ERR_FUNC *response_err;
            double I_ref = 0.0, err_ref = 0.0, I_0 = 0.0, err_0 = 0.0;

            switch(fl_get_choice(GUI->film->ch_response)) {
                default:
                case 1: { /* Raw intensity */
                    response = response_intensity;
                    response_err = response_err_intensity;
                    } break;
                case 2: { /* Optical density */
                    response = response_OD;
                    response_err = response_err_OD;
                    const char *tval = fl_get_input(GUI->film->inp_ref);
                    if(tval == NULL || strlen(tval) == 0) {
                        fl_show_alert2(0,_("Missing reference value!\fPlease input a reference value."));
                        return;
                    }
                    I_ref = strtod(tval,NULL);
                    tval = fl_get_input(GUI->film->inp_ref_err);
                    err_ref = strlen(tval) != 0 ? strtod(tval,NULL) : 0.0;
                    } break;
                case 3: { /* Optical density with background */
                    response = response_ODbkg;
                    response_err = response_err_ODbkg;
                    const char *tval = fl_get_input(GUI->film->inp_ref);
                    if(tval == NULL || strlen(tval) == 0) {
                        fl_show_alert2(0,_("Missing reference value!\fPlease input a reference value."));
                        return;
                    }
                    I_ref = strtod(tval,NULL);
                    tval = fl_get_input(GUI->film->inp_bkg);
                    if(tval == NULL || strlen(tval) == 0) {
                        fl_show_alert2(0,_("Missing background value!\fPlease input a background value."));
                        return;
                    }
                    I_0 = strtod(tval,NULL);
                    tval = fl_get_input(GUI->film->inp_ref_err);
                    err_ref = strlen(tval) != 0 ? strtod(tval,NULL) : 0.0;
                    tval = fl_get_input(GUI->film->inp_bkg_err);
                    err_0 = strlen(tval) != 0 ? strtod(tval,NULL) : 0.0;
                    } break;
            }
            fl_set_input_f(GUI->film->inp_resp,"%g",response(mean,I_ref,I_0));
            fl_set_input_f(GUI->film->inp_resp_err,"%g",response_err(mean,sd,I_ref,err_ref,I_0,err_0));
            } break;
    }
}

#define FIT_POINTS 100
void cb_film_calibration_fit ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    bool log = false, wei = true;

    FL_COLOR col;
    FL_OBJECT *plot;
    if(fl_get_button(GUI->film->btn_red)) {
        plot = GUI->film->plot_calib_red;
        col = FL_RED;
    } else if(fl_get_button(GUI->film->btn_green)) {
        plot = GUI->film->plot_calib_green;
        col = FL_GREEN;
    } else {
        plot = GUI->film->plot_calib_blue;
        col = FL_BLUE;
    }

    int n;
    float *fx,*fy;
    fl_get_xyplot_data_pointer(plot,0,&fx,&fy,&n);
    if(n < 3) {
        fl_show_alert2(0,_("Too few points!\fPlease add at least three points."));
        return;
    }
    float *ex,*ey;
    fl_get_xyplot_data_pointer(plot,1,&ex,&ey,&n);
    for(int i=0;i<n;i++)
        if(isinf(1.0 / ey[i])) {
            wei = false; /* unweighted if any error is zero */
            break;
        }

    double *x = malloc(n*sizeof(double));
    double *y = malloc(n*sizeof(double));
    for(int i=0;i<n;i++) {
        x[i] = (double)fx[i];
        y[i] = (double)fy[i];
    }
    gsl_sort2(x,1,y,1,n);

    double *e = NULL;
    if(wei) {
        e = malloc(n*sizeof(double));
        for(int i=0;i<n;i++) {
            x[i] = (double)fx[i];
            e[i] = (double)(1.0 / SQR(ey[i]));
        }
        gsl_sort2(x,1,e,1,n);
    }

    double p_init[3];
    struct fit_result *fit;
    if(fl_get_choice(GUI->film->ch_response) == 1) {
        p_init[0] = y[n-1]; p_init[1] = y[0]; p_init[2] = -1;
        fit = fit_rational(x,y,wei?e:NULL,n,p_init);
    } else {
        log = true;
        p_init[0] = p_init[1] = p_init[2] = 1;
        fit = fit_log10rational(x,y,wei?e:NULL,n,p_init);
    }
    free(x); free(y); free(e);

    if(fit == NULL) {
        fl_show_alert2(0,_("Fit failed!\fPlease check calibration plot."));
        return;
    }

    gnuplot_errors(fit);
    free(plot->u_vdata);
    plot->u_vdata = fit; /* fit results stored */

    fl_delete_xyplot_overlay(plot,2);
    float *mx = malloc(FIT_POINTS*sizeof(float));
    float *my = malloc(FIT_POINTS*sizeof(float));
    float min,max; fl_get_xyplot_xbounds(plot,&min,&max);
    float dx = (max-min) / (FIT_POINTS-1);
    for(int i=0;i<FIT_POINTS;i++) {
        mx[i] = fmaf(i,dx,min);
        my[i] = (log == false) ? fit_func_rational(mx[i],fit->par[0],fit->par[1],fit->par[2]) :
            fit_func_log10rational(mx[i],fit->par[0],fit->par[1],fit->par[2]);
    }
    fl_set_xyplot_overlay_type(plot,2,FL_NORMAL_XYPLOT);
    fl_add_xyplot_overlay(plot,2,mx,my,FIT_POINTS,col);
    free(mx); free(my);

    fl_set_object_label_f(GUI->film->txt_fit,
            "%s\n\na: %g\n ± %g\nb: %g\n ± %g\nc: %g\n ± %g",_("Fit parameters"),
            fit->par[0],fit->err[0],fit->par[1],fit->err[1],fit->par[2],fit->err[2]);
}

void cb_film_dose ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    struct fit_result *fit;
    const char *tval;
    coord_t nx,ny,nz;
    slice_t nxy;
    xg3d_data *dat_dose = NULL,
              *dat_error = NULL;
    int mode;
    double I_ref = 0.0,
           I_0 = 0.0,
           err_ref = 0.0,
           err_0 = 0.0;
    RESPONSE_FUNC *response;
    RESPONSE_ERR_FUNC *response_err;
    DOSE_FUNC *dose;
    DOSE_ERR_FUNC *dose_err;

    if(DATA->g3d == NULL) return;

    nx = DATA->g3d->nx;
    ny = DATA->g3d->ny;
    nz = DATA->g3d->nz;
    nxy = DATA->g3d->nxy;

    if(nz != 3) {
        fl_show_alert2(0,_("Other than three slices!\fIs this an RGB radiochromic film scan?"));
        return;
    }

    switch(fl_get_choice(GUI->film->ch_response)) {
        default:
        case 1: /* Raw intensity */
            response = response_intensity;
            response_err = response_err_intensity;
            dose = dose_intensity;
            dose_err = dose_err_intensity;
            break;
        case 2: /* Optical density */
            response = response_OD;
            response_err = response_err_OD;
            dose = dose_OD;
            dose_err = dose_err_OD;
            tval = fl_get_input(GUI->film->inp_ref);
            if(tval == NULL || strlen(tval) == 0) {
                fl_show_alert2(0,_("Missing reference value!\fPlease input a reference value."));
                return;
            }
            I_ref = strtod(tval,NULL);
            tval = fl_get_input(GUI->film->inp_ref_err);
            err_ref = strlen(tval) != 0 ? strtod(tval,NULL) : 0.0;
            break;
        case 3: /* Optical density with background */
            response = response_ODbkg;
            response_err = response_err_ODbkg;
            dose = dose_OD;
            dose_err = dose_err_OD;
            tval = fl_get_input(GUI->film->inp_ref);
            if(tval == NULL || strlen(tval) == 0) {
                fl_show_alert2(0,_("Missing reference value!\fPlease input a reference value."));
                return;
            }
            I_ref = strtod(tval,NULL);
            tval = fl_get_input(GUI->film->inp_bkg);
            if(tval == NULL || strlen(tval) == 0) {
                fl_show_alert2(0,_("Missing background value!\fPlease input a background value."));
                return;
            }
            I_0 = strtod(tval,NULL);
            tval = fl_get_input(GUI->film->inp_ref_err);
            err_ref = strlen(tval) != 0 ? strtod(tval,NULL) : 0.0;
            tval = fl_get_input(GUI->film->inp_bkg_err);
            err_0 = strlen(tval) != 0 ? strtod(tval,NULL) : 0.0;
            break;
    }

    fl_show_oneliner(_("Computing dose from film..."),fl_scrw/2,fl_scrh/2);
    switch((mode = fl_get_choice(GUI->film->ch_mode))) {
        case 1: /* Red channel */
        case 2: /* Green channel */
        case 3: /* Blue channel */
            fit = THREEWAY_IF(mode==1,FILM_FIT_RESULT(GUI->film,red),
                              mode==2,FILM_FIT_RESULT(GUI->film,green),
                                      FILM_FIT_RESULT(GUI->film,blue));
            if(fit == NULL) {
                fl_hide_oneliner();
                fl_show_alert2(0,_("No calibration curve found for this channel!\fPlease fit a calibration curve first."));
                return;
            }

            dat_dose = calloc(1,sizeof(xg3d_data));
            dat_dose->hdr = xg3d_header_dup(DATA->hdr);
            free(dat_dose->hdr->file);
            dat_dose->hdr->file = strdup("Dose from film");
            dat_dose->g3d = g3d_alloc(G3D_FLOAT64,nx,ny,nz,.alloc=true);
            dat_error = calloc(1,sizeof(xg3d_data));
            dat_error->hdr = xg3d_header_dup(DATA->hdr);
            free(dat_error->hdr->file);
            dat_error->hdr->file = strdup("Dose error from film");
            dat_error->g3d = g3d_alloc(G3D_FLOAT64,nx,ny,nz,.alloc=true);

#ifdef _OPENMP
#pragma omp parallel for
#endif
            for(slice_t ix=0;ix<nxy;ix++) {
                double I = g3d_get_dvalue(DATA->g3d,ix);
                double R = response(I,I_ref,I_0);
                double err_R = response_err(I,0.0,I_ref,err_ref,I_0,err_0);
                g3d_set_dvalue(dat_dose->g3d,ix,dose(R,fit->par[0],fit->par[1],fit->par[2]));
                g3d_set_dvalue(dat_error->g3d,ix,dose_err(R,err_R,fit->par[0],fit->err[0],
                    fit->par[1],fit->err[1],fit->par[2],fit->err[2]));
            }

            dat_dose->stats = g3d_stats_get(dat_dose->g3d,NULL,G3D_STATS_MINMAXSUM);
            dat_error->stats = g3d_stats_get(dat_error->g3d,NULL,G3D_STATS_MINMAXSUM);
            break;
        case 4: /* Dual channel */
            /* TODO: do something */ return;
            break;
        case 5: /* Triple channel */
            /* TODO: do something */ return;
            break;
    }

    xg3d_datasets_add(ENV->data,dat_dose);
    xg3d_datasets_add(ENV->data,dat_error);
    refresh_datasets_browser(GUI->datasets->browser,ENV->data);

    setup_dose(ENV,dat_dose);
    setup_dose_err(ENV,dat_error);

    fl_hide_oneliner();
    refresh_ov_canvases(GUI->overview);
    fl_call_object_callback(GUI->dvhs->btn_clear);
}
