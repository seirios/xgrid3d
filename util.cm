#include <stdlib.h>
#include <math.h>
#include <errno.h>
#include <inttypes.h>
#include <stdbool.h>
#include <ctype.h>
%include "util.hm"

/* compare floats */
%def COMPARE cmp_float {
    const float a = *(const float*)keyval;
    const float b = *(const float*)datum;

    return (a < b) ? -1 : ((a > b) ? 1 : 0);
}

/* compare doubles */
%def COMPARE cmp_double {
    const double a = *(const double*)keyval;
    const double b = *(const double*)datum;

    return (a < b) ? -1 : ((a > b) ? 1 : 0);
}

/* silent cue for flimage operations */
int quiet_cue(FL_IMAGE *im, const char *msg) {
    %unused im,msg;
    return 0;
}

/* at close set button and call button callback */
%def FL_FORM_ATCLOSE atclose_toggle {
    FL_OBJECT *obj = (FL_OBJECT*)data;
    %unused form;

    fl_set_button(obj,!fl_get_button(obj));
    fl_call_object_callback(obj);

    return FL_OK;
}

/* at close trigger objects */
%def FL_FORM_ATCLOSE atclose_trigger {
    FL_OBJECT *obj = (FL_OBJECT*)data;
    %unused form;

    if(obj != NULL)
        fl_trigger_object(obj);

    return FL_IGNORE;
}

/* at close call object callback */
%def FL_FORM_ATCLOSE atclose_call {
    FL_OBJECT *obj = (FL_OBJECT*)data;
    %unused form;

    fl_call_object_callback(obj);

    return FL_IGNORE;
}

/* deactivate an object and change appearance accordingly */
void deactivate_obj(FL_OBJECT *obj) {
    int objclass;

    if(obj == NULL) return; /* no input */
    objclass = fl_get_object_objclass(obj);

    /* object is group */
    if(objclass == FL_BEGIN_GROUP) {
        for(FL_OBJECT *o = obj->next; o && o->objclass != FL_END_GROUP; o = o->next) {
            deactivate_obj(o);
        }
        return;
    }

    if(!fl_object_is_active(obj)) return;

    fl_set_object_lcolor(obj,FL_INACTIVE);
    fl_deactivate_object(obj);
    if(objclass != FL_LABELFRAME)
        fl_set_object_color(obj,FL_COL1,FL_INACTIVE);
    if(objclass == FL_INPUT)
        fl_set_input_color(obj,FL_INACTIVE,FL_BLACK);
    if(objclass == FL_SPINNER)
        fl_set_input_color(fl_get_spinner_input(obj),FL_INACTIVE,FL_INACTIVE);
}

/* activate an object and change appearance accordingly */
void activate_obj(FL_OBJECT *obj, const FL_COLOR color) {
    int objclass;

    if(obj == NULL) return; /* no input */
    objclass = fl_get_object_objclass(obj);

    /* object is group */
    if(objclass == FL_BEGIN_GROUP) {
        for(FL_OBJECT *o = obj->next; o && o->objclass != FL_END_GROUP; o = o->next) {
            activate_obj(o,color);
        }
        return;
    }

    if(fl_object_is_active(obj)) return;

    fl_set_object_lcolor(obj, FL_BLACK);
    fl_activate_object(obj);
    if(objclass == FL_BROWSER)
        fl_set_object_color(obj,FL_WHITE,color);
    else if(objclass == FL_BUTTON) {
        if(fl_get_object_type(obj) == FL_PUSH_BUTTON)
            fl_set_object_color(obj,FL_COL1,color);
        else
            fl_set_object_color(obj,color,FL_COL1);
    } else if(objclass == FL_MENU)
        fl_set_object_color(obj,color,FL_MCOL);
    else if(objclass != FL_LABELFRAME)
        fl_set_object_color(obj,FL_COL1,color);
    if(objclass == FL_INPUT)
        fl_set_input_color(obj,FL_BLACK,FL_BLUE);
    if(objclass == FL_SPINNER)
        fl_set_input_color(fl_get_spinner_input(obj),FL_BLACK,FL_BLUE);
}

/* validator for DOSXYZnrc run filenames */
%def FL_INPUT_VALIDATOR normal_file_validator {
    %unused obj,oldstr;
    
    if ( ! *str )
        return FL_VALID;

    if( ! str[ 1 ] && ( newc == '.' || newc == '_' || newc == '-' ) )
        return FL_INVALID;

    if( newc == '_' || newc == '.' || newc == '-' )
        return FL_VALID;

    if( !isalnum(newc) )
        return FL_INVALID;

    return FL_VALID;
}

/* validator for positive integers */
%def FL_INPUT_VALIDATOR int_pos_validator {
    %unused obj,oldstr,newc;
    char *eptr = NULL;
    uintmax_t dummy;

    if ( ! *str )
        return FL_VALID;

    dummy = strtoumax( str, &eptr, 10 );

    if (    ( dummy == UINTMAX_MAX && errno == ERANGE ) || *eptr )
        return FL_INVALID | FL_RINGBELL;

    return FL_VALID;
}

/* validator for times with optional m/h/d suffix */
%def FL_INPUT_VALIDATOR float_time_validator {
    %unused obj,oldstr,newc;
    char *eptr = NULL;
    double dummy;
    size_t len;

    if ( ! *str )
        return FL_INVALID;

    if ( *str == 'm' || *str == 'h' || *str == 'd' || *str == '-' )
        return FL_INVALID | FL_RINGBELL;

    dummy = strtod( str, &eptr );

    if (    ! ( ( dummy == HUGE_VAL || dummy == -HUGE_VAL ) && errno == ERANGE )
            && ! *eptr )
        return FL_VALID;

    len = strlen( str );

    if ( len == 1 )
        return ( *str == '.' ) ?
            FL_VALID : FL_INVALID | FL_RINGBELL;

    if ( len == 2 )
        return (   *eptr == 'm'
                || *eptr == 'h'
                || *eptr == 'd' ) ?
            FL_VALID : ( FL_INVALID | FL_RINGBELL );

    if ( ( *eptr == 'm' || *eptr == 'h' || *eptr == 'd' )
            && strchr( str, *eptr ) == eptr && eptr == str + len - 1
            && eptr[-1] != '.')
        return FL_VALID;

    return FL_INVALID | FL_RINGBELL;
}

/* validator for positive floating-point numbers */
%def FL_INPUT_VALIDATOR float_pos_validator {
    %unused obj,oldstr;
    char *eptr = NULL;
    double dummy;
    size_t len;

    if ( ! *str )
        return FL_VALID;

    if ( *str == '+' )
        return FL_INVALID | FL_RINGBELL;

    dummy = strtod( str, &eptr );

    if (    ! ( ( dummy == HUGE_VAL || dummy == -HUGE_VAL ) && errno == ERANGE )
            && ! *eptr )
        return FL_VALID;

    if ( ! newc )
        return FL_INVALID | FL_RINGBELL;

    len = strlen( str );

    if ( len == 1 )
        return ( newc == '.' ) ?
            FL_VALID : FL_INVALID | FL_RINGBELL;

    if ( len == 2 )
        return (    *eptr == 'e'
                 || *eptr == 'E' ) ?
            FL_VALID : ( FL_INVALID | FL_RINGBELL );

    if ( ( *eptr == 'e' || *eptr == 'E' )
            && strchr( str, *eptr ) == eptr
            && (    eptr == str + len - 1
                || (    eptr == str + len - 2
                    && ( eptr[ 1 ] == '+' || eptr[ 1 ] == '-' ) ) ) )
        return FL_VALID;

    return FL_INVALID | FL_RINGBELL;
}

/* validator for floating-point numbers with optional percent suffix */
%def FL_INPUT_VALIDATOR float_perc_validator {
    %unused obj,oldstr;
    char *eptr = NULL;
    double dummy;
    size_t len;

    if ( ! *str )
        return FL_VALID;

    dummy = strtod( str, &eptr );

    if (    ! ( ( dummy == HUGE_VAL || dummy == -HUGE_VAL ) && errno == ERANGE )
            && ! *eptr )
        return FL_VALID;

    if ( ! newc )
        return (*eptr == '%') ? FL_VALID : (FL_INVALID | FL_RINGBELL);

    len = strlen( str );

    if ( len == 1 )
        return ( newc == '+' || newc == '-' || newc == '.' ) ?
            FL_VALID : FL_INVALID | FL_RINGBELL;

    if ( len == 2 )
        return (   ! strcmp( str, "+." )
                || ! strcmp( str, "-." )
                || *eptr == 'e'
                || *eptr == 'E' 
                || *eptr == '%') ?
            FL_VALID : ( FL_INVALID | FL_RINGBELL );

    if( *str != '-'
            && *eptr == '%'
            && strchr( str, *eptr ) == eptr
            && eptr == str + len - 1)
        return FL_VALID;

    if ( ( *eptr == 'e' || *eptr == 'E' )
            && strchr( str, *eptr ) == eptr
            && (    eptr == str + len - 1
                || (    eptr == str + len - 2
                    && ( eptr[ 1 ] == '+' || eptr[ 1 ] == '-' ) ) ) )
        return FL_VALID;

    return FL_INVALID | FL_RINGBELL;
}

/* event handler for copying a text object's label on double click */
static %def FL_HANDLE _pos_txt {
    %unused mx,my,key,xev;
    const char *label = fl_get_object_label(obj);

    if(event == FL_DBLCLICK)
        fl_stuff_clipboard(obj,0,label,strlen(label),NULL);

    return FL_IGNORE;
}

/* event handler for spinner change on mouse wheel */
static %def FL_HANDLE _pos_spin {
    %unused mx,my,xev;
    double a,b,val;

    if(event == FL_PUSH && (key == FL_SCROLLUP_MOUSE || key == FL_SCROLLDOWN_MOUSE)) {
        a = fl_get_spinner_step(obj);
        val = fl_get_spinner_value(obj) + (key == FL_SCROLLUP_MOUSE ? a : -a);
        fl_get_spinner_bounds(obj,&a,&b);
        if(val < a || val > b) return FL_IGNORE;
        fl_set_spinner_value(obj,val);
        fl_call_object_callback(obj);
    }

    return FL_IGNORE;
}

/* event handler for slider change on mouse wheel */
static %def FL_HANDLE _pos_sli {
    %unused mx,my,xev;
    double a,b,val;

    if(event == FL_PUSH && (key == FL_SCROLLUP_MOUSE || key == FL_SCROLLDOWN_MOUSE)) {
        fl_get_slider_increment(obj,&a,&b);
        val = fl_get_slider_value(obj) + (key == FL_SCROLLUP_MOUSE ? a : -a);
        fl_get_slider_bounds(obj,&a,&b);
        if(val < a || val > b) return FL_IGNORE;
        fl_set_slider_value(obj,val);
        fl_call_object_callback(obj);
    }

    return FL_IGNORE;
}

/* event handler for counter change on mouse wheel */
static %def FL_HANDLE _pos_count {
    %unused mx,my,xev;
    double a,b,val;

    if(event == FL_PUSH && (key == FL_SCROLLUP_MOUSE || key == FL_SCROLLDOWN_MOUSE)) {
        fl_get_counter_step(obj,&a,&b);
        val = fl_get_counter_value(obj) + (key == FL_SCROLLUP_MOUSE ? a : -a);
        fl_get_counter_bounds(obj,&a,&b);
        if(val < a || val > b) return FL_IGNORE;
        fl_set_counter_value(obj,val);
        fl_call_object_callback(obj);
    }

    return FL_IGNORE;
}

/* event handler for browser extra buttons */
static %def FL_HANDLE _pos_browser {
    %unused mx,my,xev;
    int num,line;

    if(event == FL_PUSH)
        fl_set_focus_object(obj->form,obj);

    if(event == FL_KEYPRESS) {
        if(key == XK_Up) {
            num = fl_get_browser_num_selected(obj);
            if(num == 1) {
                line = fl_get_browser_first_selected(obj);
                if(line > 1) {
                    fl_deselect_browser_line(obj,line--);
                    fl_select_browser_line(obj,line);
                    fl_show_browser_line(obj,line);
                    fl_call_object_callback(obj);
                }
            }
        }
        else if(key == XK_Down) {
            num = fl_get_browser_num_selected(obj);
            if(num == 1) {
                line = fl_get_browser_first_selected(obj);
                if(line < fl_get_browser_maxline(obj)) {
                    fl_deselect_browser_line(obj,line++);
                    fl_select_browser_line(obj,line);
                    fl_show_browser_line(obj,line);
                    fl_call_object_callback(obj);
                }
            }
        }
    }

    return FL_IGNORE;
}

/* make a text object copyable on double-click */
static void _activate_txt_dblclick_copy(FL_OBJECT *obj) {
    if(fl_get_object_objclass(obj) == FL_TEXT) {
        fl_activate_object(obj);
        fl_set_object_posthandler(obj,_pos_txt);
        fl_set_object_dblclick(obj,FL_CLICK_TIMEOUT);
    }
}

/* make a spinner object respond to scrollwheel */
static void _activate_spin_scrollwheel(FL_OBJECT *obj) {
    if(fl_get_object_objclass(obj) == FL_SPINNER)
        fl_set_object_posthandler(obj,_pos_spin);
}

/* make a slider object respond to scrollwheel */
static void _activate_slider_scrollwheel(FL_OBJECT *obj) {
    if(fl_get_object_objclass(obj) == FL_SLIDER
            || fl_get_object_objclass(obj) == FL_VALSLIDER)
        fl_set_object_posthandler(obj,_pos_sli);
}

/* make a counter object respond to scrollwheel */
static void _activate_counter_scrollwheel(FL_OBJECT *obj) {
    if(fl_get_object_objclass(obj) == FL_COUNTER)
        fl_set_object_posthandler(obj,_pos_count);
}

/* make a browser respond to arrows and extra mouse buttons */
static void _activate_browser_extra(FL_OBJECT *obj) {
    if(fl_get_object_objclass(obj) == FL_BROWSER)
        fl_set_object_posthandler(obj,_pos_browser);
}

/* iterate over the objects in a form and apply extra interaction features */
void activate_form_extra_interaction(FL_FORM *form) {
    FL_OBJECT *obj;

    obj = form->first;
    while(obj != NULL) {
        switch(fl_get_object_objclass(obj)) {
            case FL_SPINNER:   _activate_spin_scrollwheel(obj);      break;
            case FL_TEXT:      _activate_txt_dblclick_copy(obj);     break;
            case FL_SLIDER:    /* fall-through */
            case FL_VALSLIDER: _activate_slider_scrollwheel(obj);    break;
            case FL_COUNTER:   _activate_counter_scrollwheel(obj);   break;
            case FL_BROWSER:   _activate_browser_extra(obj);         break;
        }
        obj = obj->next;
    }
}

/* check for a file extension and if absent add it */
char *enforce_ext(const char *file, const char *ext, char *buf, const size_t bufsize) {
    const char *extptr = strstr(file,ext);

    if(extptr == NULL || extptr != file+strlen(file)-strlen(ext))
        snprintf(buf,bufsize,"%s.%s",file,ext);
    else
        snprintf(buf,bufsize,"%s",file);

    return buf;
}

/* convert exponent number to sup character */
const char* exp_to_utf8_sup(const int exp) {
    static char str[8],out[16];
    char *c;

    memset(str,0x0,sizeof(str));
    memset(out,0x0,sizeof(out));
    snprintf(str,sizeof(str),"%+d",exp);

    c = str;
    while(*c != '\0') {
        switch(*c) {
            case '-': strncat(out,"\u207B",3+1); break;
            case '+': strncat(out,"\u207A",3+1); break;
            case '0': strncat(out,"\u2070",3+1); break;
            case '1': strncat(out,"\u00B9",3+1); break;
            case '2': strncat(out,"\u00B2",3+1); break;
            case '3': strncat(out,"\u00B3",3+1); break;
            case '4': strncat(out,"\u2074",3+1); break;
            case '5': strncat(out,"\u2075",3+1); break;
            case '6': strncat(out,"\u2076",3+1); break;
            case '7': strncat(out,"\u2077",3+1); break;
            case '8': strncat(out,"\u2078",3+1); break;
            case '9': strncat(out,"\u2079",3+1); break;
        }
        c++;
    }

    return out;
}

/* resize a tabfolder for tab change */
void resize_form_tabfolder(FL_OBJECT *obj, FL_OBJECT *box) {
    FL_Coord x,y,w,box_h,folder_area_h;
    int tabfolder_h = obj->h;

    fl_get_folder_area(obj,&x,&y,&w,&folder_area_h);
    int tab_height = tabfolder_h - folder_area_h;

    fl_get_object_size(box,&w,&box_h);
    fl_freeze_form(obj->form);
    fl_set_object_size(obj,w,box_h + tab_height);
    fl_set_form_size(obj->form,obj->form->w,obj->form->h - tabfolder_h + box_h + tab_height);
    fl_unfreeze_form(obj->form);
}

/* string to double with default value fallback */
double strtod_checked(const char *txt, const double defval) {
    if(txt == NULL || strlen(txt) == 0)
        return defval;
    else {
        double val = strtod(txt,NULL);
        if((val == -HUGE_VAL || val == HUGE_VAL) && errno == ERANGE)
            return defval;
        return val;
    }
}

void toggle_browser_line(FL_OBJECT *obj, const int i, const bool on) {
    const char *line;

    if(obj == NULL || i < 0) return;

    line = fl_get_browser_line(obj,i);
    if(!on)
        fl_replace_browser_line_f(obj,i,"@N%s",line);
    else if(line != NULL && strlen(line) >= 2 && *line++ == '@' && *line++ == 'N')
        fl_replace_browser_line_f(obj,i,"%s",line);
}
