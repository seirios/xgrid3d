#ifndef XG3D_overlay_reg_h_
#define XG3D_overlay_reg_h_

#include "nifti1_io.h"
#include "io.h"

enum reg_mode {
	REG_RIGID = 1,
	REG_AFFINE = 2,
	REG_INIT = 4
};

int register_aladin(nifti_image*,nifti_image*,nifti_image*,nifti_image*,mat44*,enum reg_mode,int);
int register_resample(nifti_image*,nifti_image*,mat44*,nifti_image**,int,float);
nifti_image* xg3d_to_nifti(const grid3d*,const xg3d_header*);

#endif
