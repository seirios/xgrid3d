#include <libgen.h>

#include <babl/babl.h>

#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "cb_coloring.h"
#include "cb_viewport.h"
#include "cb_mainview.h"
#include "cb_overview.h"
#include "cb_selection.h"
#include "cb_quantization.h"

#include <gsl/gsl_spline.h>
#include <gsl/gsl_sort_float.h>

#include "selection.h"

#ifndef XG3D_util_h_
#define XG3D_util_h_
#include <forms.h>
#include <flimage.h>

typedef int ( COMPARE ) ( const void * keyval  , const void * datum  ) ;



COMPARE cmp_float;
COMPARE cmp_double;
int quiet_cue(FL_IMAGE*,const char*);
FL_FORM_ATCLOSE atclose_toggle;
FL_FORM_ATCLOSE atclose_trigger;
FL_FORM_ATCLOSE atclose_call;
FL_INPUT_VALIDATOR normal_file_validator;
FL_INPUT_VALIDATOR int_pos_validator;
FL_INPUT_VALIDATOR float_time_validator;
FL_INPUT_VALIDATOR float_pos_validator;
FL_INPUT_VALIDATOR float_perc_validator;
void deactivate_obj(FL_OBJECT*);
void activate_obj(FL_OBJECT*,const FL_COLOR);
void activate_form_extra_interaction(FL_FORM*);
char *enforce_ext(const char*,const char*,char*,const size_t);
const char* exp_to_utf8_sup(const int);
void resize_form_tabfolder(FL_OBJECT*,FL_OBJECT*);
double strtod_checked(const char*,const double);
void toggle_browser_line(FL_OBJECT*,const int,const bool);
#endif
#include "util_linsimp.h"


#define FORM_W 300 /* basal form width */
#define FORM_H 135 /* basal form height */

#define CELLSIZE 13
#define CELLNUM 16

static void update_tab_canvas(FD_coloring*,FD_col_table*);
static void _tab_select(FD_col_table*,const xg3d_lut*,uint8_t,uint8_t,int);

#undef ENV
inline static void _refresh_coloring(xg3d_env *ENV) {
    if(GUI->viewport != NULL) refresh_canvas(ENV);
    refresh_ov_canvases(GUI->overview);
}
#define ENV _default_env

/* =============
 * FORM COLORING
 * ============= */

inline static void _prepare_lut_pix(FL_IMAGE **lut_img, const xg3d_lut *lut) {
    const unsigned int len = 256 / lut->ncol;

    if(*lut_img == NULL) {
        *lut_img = flimage_alloc();
        (*lut_img)->type = FL_IMAGE_CI;
        (*lut_img)->map_len = G3D_VISUAL_MAX_LEVELS;
        (*lut_img)->w = 256;
        (*lut_img)->h = 25;
        flimage_getmem(*lut_img);
        for(int i=0;i<(*lut_img)->w*(*lut_img)->h;i++)
            (*lut_img)->ci[i/(*lut_img)->w][i%(*lut_img)->w] = i%(*lut_img)->w;
    }
    (*lut_img)->sw = len * lut->ncol;
    for(int i=0;i<(*lut_img)->sw;i++) {
        (*lut_img)->red_lut[i]   = FL_GETR(lut->colors[i/len]);
        (*lut_img)->green_lut[i] = FL_GETG(lut->colors[i/len]);
        (*lut_img)->blue_lut[i]  = FL_GETB(lut->colors[i/len]);
     }
    (*lut_img)->modified = 1;
}

void update_lut_pix(FD_coloring *coloring) {
    FL_IMAGE **lut_img = (FL_IMAGE**)&COLORING_LUT_IMG(coloring);
    const Window win = FL_ObjWin(coloring->lut_pix);
    const xg3d_lut *lut = COLORING_LUT(coloring);

    _prepare_lut_pix(lut_img,lut);

    if(coloring->lut_pix->w != (*lut_img)->sw)
        fl_set_object_size(coloring->lut_pix,(*lut_img)->sw+2,(*lut_img)->h);

    if(fl_winisvalid(win)) {
        fl_free_pixmap_pixmap(coloring->lut_pix);
        fl_set_pixmap_pixmap(coloring->lut_pix,flimage_to_pixmap(*lut_img,win),None);
    }
}

/* update a flow button pixmap */
#define LINE "***********************************"
void refresh_flow_pix(FL_OBJECT *button, const FL_PACKED color) {
    static char *data[21] = {
        "35 19 1 1",
        NULL,
        LINE,LINE,LINE,LINE,LINE,LINE,LINE,LINE,LINE,LINE,
        LINE,LINE,LINE,LINE,LINE,LINE,LINE,LINE,LINE
    };

    data[1] = malloc(12*sizeof(char));
    snprintf(data[1],12,"* c #%.2x%.2x%.2x",FL_GETR(color),FL_GETG(color),FL_GETB(color));
    fl_free_pixmapbutton_pixmap(button);
    fl_set_pixmapbutton_data(button,data);
    free(data[1]);
    button->u_ldata = color; /* save color in button ldata */
}
#undef LINE

/* set under/over flow colors from extremes of LUT */
void cb_flow_fromlut ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const FL_PACKED lut_under = COLORING_LUT_UNDER(GUI->coloring);
    const FL_PACKED lut_over = COLORING_LUT_OVER(GUI->coloring);
    const bool underflow = COLORING_UNDERFLOW(GUI->coloring);
    const bool overflow = COLORING_OVERFLOW(GUI->coloring);
    const xg3d_lut *lut = COLORING_LUT(GUI->coloring);
    int flag = 0;

    if(lut_under != lut->colors[0]) {
        refresh_flow_pix(GUI->coloring->btn_under,lut->colors[0]);
        flag++;
    }

    if(lut_over != lut->colors[lut->ncol-1]) {
        refresh_flow_pix(GUI->coloring->btn_over,lut->colors[lut->ncol-1]);
        flag++;
    }

    if(flag != 0 && (underflow || overflow))
        _refresh_coloring(ENV);
}

/* change under/over flow colors */
void cb_flow_color ( FL_OBJECT * obj  , long data  ) {
    const bool underflow = COLORING_UNDERFLOW(GUI->coloring);
    const bool overflow = COLORING_OVERFLOW(GUI->coloring);
    const uint8_t cur = COLORING_TAB_IDX(GUI->col_table);
    xg3d_lut *lut = COLORING_LUT(GUI->coloring);
    FL_PACKED color = obj->u_ldata; /* get color from button ldata */
    const int rgb_in[3] = {
        FL_GETR(color),
        FL_GETG(color),
        FL_GETB(color)
    };
    int rgb_out[3];

    if(!fl_show_color_chooser(rgb_in,rgb_out)) return;
    color = FL_PACK(rgb_out[0],rgb_out[1],rgb_out[2]);
    refresh_flow_pix(obj,color);

    if((!data && !underflow) || (data && !overflow)) return;

    /* replace LUT color */
    lut->colors[data ? lut->ncol-1 : 0] = color;
    update_lut_pix(GUI->coloring);
    if(COLORING_MODE(GUI->coloring) == COLOR_PALETTE) {
        update_tab_canvas(GUI->coloring,GUI->col_table);
        _tab_select(GUI->col_table,lut,cur/CELLNUM,cur%CELLNUM,true);
    }
    _refresh_coloring(ENV);
}

/* toggle lut reverse and invert */
void cb_lutflag ( FL_OBJECT * obj  , long data  ) {
    const unsigned int st = COLORING_UNDERFLOW(GUI->coloring) ? 1 : 0;
    const unsigned int et = COLORING_OVERFLOW(GUI->coloring) ? 1 : 0;
    const uint8_t cur = COLORING_TAB_IDX(GUI->col_table);
    xg3d_lut *lut = COLORING_LUT(GUI->coloring);

    if(!data) /* reverse*/
        for(unsigned int i=st;i<(lut->ncol-et)/2;i++) {
            FL_PACKED tmp = lut->colors[i];
            lut->colors[i] = lut->colors[lut->ncol-1-i];
            lut->colors[lut->ncol-1-i] = tmp;
        }
    else /* invert*/
        for(unsigned int i=st;i<lut->ncol-et;i++)
            lut->colors[i] = FL_PACK(
                    (uint8_t)~FL_GETR(lut->colors[i]),
                    (uint8_t)~FL_GETG(lut->colors[i]),
                    (uint8_t)~FL_GETB(lut->colors[i]));

    update_lut_pix(GUI->coloring);
    if(COLORING_MODE(GUI->coloring) == COLOR_PALETTE) {
        update_tab_canvas(GUI->coloring,GUI->col_table);
        _tab_select(GUI->col_table,lut,cur/CELLNUM,cur%CELLNUM,true);
    }
    _refresh_coloring(ENV);
}

/* change tab (coloring mode) */
void cb_coloring_mode ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const xg3d_lut *lut = COLORING_LUT(GUI->coloring);
    const enum xg3d_color_mode mode = fl_get_active_folder_number(obj);

    switch(mode) {
        case COLOR_GRADIENT: /* Gradient */
            fl_set_button(GUI->col_table->btn_lutfile,0);
            resize_form_tabfolder(obj,GUI->col_grad->box);
            break;
        case COLOR_PREDEF: /* Predefined */
            fl_set_button(GUI->col_table->btn_lutfile,0);
            resize_form_tabfolder(obj,GUI->col_predef->box);
            if(fl_get_browser(GUI->col_predef->browser) == 0) {
                fl_select_browser_line(GUI->col_predef->browser,1);
                COLORING_PREDEF_TYPE(GUI->col_predef) = 0;
            }
            break;
        case COLOR_PALETTE: /* Palette */
            resize_form_tabfolder(obj,GUI->col_table->box);
            break;
        case COLOR_RAMP: /* Ramp */
            fl_set_button(GUI->col_table->btn_lutfile,0);
            resize_form_tabfolder(obj,GUI->col_ramp->box);
            break;
    }
    update_coloring(GUI->coloring,lut->ncol);
    if(mode == COLOR_PALETTE)
        update_palette_interval(GUI->col_table,GUI->quantization,DATA->hdr);
    _refresh_coloring(ENV);
}

/* =================
 * FORM COL_GRADIENT
 * ================= */

/* change hue slider */
void cb_hue_slider ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const xg3d_lut *lut = COLORING_LUT(GUI->coloring);

    fl_set_object_label_f(GUI->col_grad->txt_hue,_("+%d°"),(int)fl_get_slider_value(obj));
    update_coloring(GUI->coloring,lut->ncol);
    _refresh_coloring(ENV);
}

/* change gradient type (buttons) */
void cb_gradient_type ( FL_OBJECT * obj  , long data  ) {
    const xg3d_lut *lut = COLORING_LUT(GUI->coloring);
    enum col_grad_type type = data;

    if(type == COL_GRAD_MATPLOTLIB) {
        activate_obj(GUI->col_grad->ch_grad_mpl,FL_BLACK);
        type += fl_get_choice(GUI->col_grad->ch_grad_mpl);
    } else
        deactivate_obj(GUI->col_grad->ch_grad_mpl);

    COLORING_GRAD_TYPE(GUI->col_grad) = type;
    update_coloring(GUI->coloring,lut->ncol);
    _refresh_coloring(ENV);
}

/* ===============
 * FORM COL_PREDEF
 * =============== */

/* predefined palette browser */
void cb_col_predef ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const enum col_predef_type type = fl_get_browser(obj) - 1;
    const xg3d_lut *lut = COLORING_LUT(GUI->coloring);

    if(type < COL_PREDEF_FIRST || type > COL_PREDEF_LAST) return;

    COLORING_PREDEF_TYPE(GUI->col_predef) = type;
    update_coloring(GUI->coloring,lut->ncol);
    _refresh_coloring(ENV);
}

/* ==============
 * FORM COL_TABLE
 * ============== */

/* prepares the color table image */
static void _prepare_table_img(FL_IMAGE **tab_img, const xg3d_lut *lut) {
    if(*tab_img == NULL) {
        *tab_img = flimage_alloc();
        (*tab_img)->type = FL_IMAGE_CI;
        (*tab_img)->map_len = G3D_VISUAL_MAX_LEVELS;
        (*tab_img)->w = CELLNUM * CELLSIZE;
        (*tab_img)->h = (256/CELLNUM) * CELLSIZE;
        flimage_getmem(*tab_img);
        for(int i=0;i<(*tab_img)->w*(*tab_img)->h;i++)
            (*tab_img)->ci[i/(*tab_img)->w][i%(*tab_img)->w] =
                (i/(*tab_img)->w)/CELLSIZE * CELLNUM + (i%(*tab_img)->h)/CELLSIZE;
    }
    for(int i=0;i<(int)lut->ncol;i++) {
        (*tab_img)->red_lut[i] = FL_GETR(lut->colors[i]);
        (*tab_img)->green_lut[i] = FL_GETG(lut->colors[i]);
        (*tab_img)->blue_lut[i] = FL_GETB(lut->colors[i]);
    }
    for(int i=lut->ncol;i<(*tab_img)->map_len;i++)
        (*tab_img)->red_lut[i] = (*tab_img)->green_lut[i] = (*tab_img)->blue_lut[i] = 173; /* FL_COL1 */
    (*tab_img)->modified = 1;
}

/* refresh the color table image canvas */
static void update_tab_canvas(FD_coloring *coloring, FD_col_table *col_table) {
    FL_IMAGE **tab_img = (FL_IMAGE**)&COLORING_TAB_IMG(col_table);
    const xg3d_lut *lut = COLORING_LUT(coloring);
    const Window win = FL_ObjWin(col_table->tab_canvas);

    _prepare_table_img(tab_img,lut);
    if(fl_winisvalid(win))
        flimage_display(*tab_img,win);
}

/* draw selection on color table */
static void _tab_select(FD_col_table *col_table, const xg3d_lut *lut, const uint8_t row, const uint8_t col, const int reset) {
    const uint8_t old = COLORING_TAB_IDX(col_table);
    const uint8_t oldrow = old / CELLNUM;
    const uint8_t oldcol = old % CELLNUM;
    const uint8_t new = CELLNUM*row+col;
    const Window win = FL_ObjWin(col_table->tab_canvas);
    int dm;

    if(new < lut->ncol && fl_winisvalid(win)) {
        fl_winset(win);
        dm = fl_get_drawmode();
        fl_drawmode(GXinvert);
        if(!reset) {
            if(row != oldrow || col != oldcol) {
                fl_rect(CELLSIZE*oldcol,CELLSIZE*oldrow,CELLSIZE-1,CELLSIZE-1,FL_BLACK); /* repaint old square */
                fl_rect(CELLSIZE*col,CELLSIZE*row,CELLSIZE-1,CELLSIZE-1,FL_BLACK);   /* paint new square */
            }
        } else
            fl_rect(CELLSIZE*col,CELLSIZE*row,CELLSIZE-1,CELLSIZE-1,FL_BLACK); /* paint new square */
        fl_drawmode(dm);
        COLORING_TAB_IDX(col_table) = new; /* stores new position */

        /* update color value */
        fl_set_object_label_f(col_table->txt_red,"%d",(int)FL_GETR(lut->colors[new]));
        fl_set_object_label_f(col_table->txt_green,"%d",(int)FL_GETG(lut->colors[new]));
        fl_set_object_label_f(col_table->txt_blue,"%d",(int)FL_GETB(lut->colors[new]));
    }
}

/* update interval text for selected palette color */
void update_palette_interval(FD_col_table *col_table, FD_quantization *quantization, const xg3d_header *hdr) {
    const int flag_calib = QUANTIZATION_GET_CALIB(quantization);
    const uint8_t cur = COLORING_TAB_IDX(col_table);

    fl_set_object_label_f(col_table->txt_interval,"%s",quant_level_string(quantization,cur,hdr,flag_calib));
    fl_set_object_label(col_table->txt_occup,"");
}

/* palette pick color */
static void _pal_pick(FD_col_table *col_table, FD_coloring *coloring, const unsigned int idx) {
    xg3d_lut *lut = COLORING_LUT(coloring);
    const int rgb_in[3] = {
        FL_GETR(lut->colors[idx]),
        FL_GETG(lut->colors[idx]),
        FL_GETB(lut->colors[idx])
    };
    int rgb_out[3];

    if(fl_show_color_chooser(rgb_in,rgb_out)) {
        lut->colors[idx] = FL_PACK(rgb_out[0],rgb_out[1],rgb_out[2]);
        update_lut_pix(coloring);
        update_tab_canvas(coloring,col_table);
        _tab_select(col_table,lut,idx/CELLNUM,idx%CELLNUM,true);
    }
}

/* palette canvas interaction */
int cb_tab_canvas(FL_OBJECT *obj, Window win, int ww FL_UNUSED_ARG, int wh FL_UNUSED_ARG, XEvent *xev, void *data FL_UNUSED_ARG) {
    const uint8_t cur = COLORING_TAB_IDX(GUI->col_table);
    const xg3d_lut *lut = COLORING_LUT(GUI->coloring);
    const xg3d_header *hdr = DATA->hdr;
    unsigned int keymask;
    FL_Coord wx,wy;

    /* stuff for double-click detection */
    static unsigned long last_clicktime = 0;
    static FL_Coord last_wx,last_wy;
    static int last_key = 0;

    switch(xev->type) {
        case Expose:
            update_tab_canvas(GUI->coloring,GUI->col_table);
            _tab_select(GUI->col_table,lut,cur/CELLNUM,cur%CELLNUM,true);
            update_palette_interval(GUI->col_table,GUI->quantization,hdr);
            break;
        case ButtonPress:
            switch(xev->xbutton.button) {
                case FL_LEFT_MOUSE:
                    fl_get_win_mouse(win,&wx,&wy,&keymask);
                    _tab_select(GUI->col_table,lut,wy/CELLSIZE,wx/CELLSIZE,false);
                    update_palette_interval(GUI->col_table,GUI->quantization,hdr);
                    break;
                case FL_SCROLLUP_MOUSE:
                    if(shiftkey_down(((XMotionEvent*)xev)->state)) {
                                                                            if(cur/CELLNUM < lut->ncol/CELLNUM) {
                                _tab_select(GUI->col_table,lut,cur/CELLNUM+1,cur%CELLNUM,false);
                                update_palette_interval(GUI->col_table,GUI->quantization,hdr);
                            }
                                           } else {
                                                                            if(cur < lut->ncol - 1) {
                                _tab_select(GUI->col_table,lut,(cur+1)/CELLNUM,(cur+1)%CELLNUM,false);
                                update_palette_interval(GUI->col_table,GUI->quantization,hdr);
                            }
                                           }
                    break;
                case FL_SCROLLDOWN_MOUSE:
                    if(shiftkey_down(((XMotionEvent*)xev)->state)) {
                                                                            if(cur/CELLNUM >= 1) {
                                _tab_select(GUI->col_table,lut,cur/CELLNUM-1,cur%CELLNUM,false);
                                update_palette_interval(GUI->col_table,GUI->quantization,hdr);
                            }
                                           } else {
                                                                            if(cur >= 1) {
                                _tab_select(GUI->col_table,lut,(cur-1)/CELLNUM,(cur-1)%CELLNUM,false);
                                update_palette_interval(GUI->col_table,GUI->quantization,hdr);
                            }
                                           }
                    break;
            }
            break;
        case ButtonRelease:
            fl_get_win_mouse(win,&wx,&wy,&keymask);

            { /* check for double click (code from xforms' FL_DBLCLICK) */
                int key = xev->xbutton.button;
                if (    key == last_key /* same mouse button */
                        && ! ( key == 4 || key == 5 ) /* non-scrollwheel */
                        && ! (    FL_abs( last_wx - wx ) > 4
                               || FL_abs( last_wy - wy ) > 4 ) /* moved 4 px or less */
                        && xev /* event not NULL */
                        && xev->xbutton.time - last_clicktime < FL_CLICK_TIMEOUT /* short delay */
                        ) {
                    _pal_pick(GUI->col_table,GUI->coloring,cur);
                    _refresh_coloring(ENV);
                }
                last_clicktime = xev ? xev->xbutton.time : 0;
                last_key = key;
                last_wx = wx;
                last_wy = wy;
            }
            break;
        case KeyPress:
            switch(XLookupKeysym((XKeyEvent*)xev,0)) {
                case XK_Up:
                                                if(cur/CELLNUM >= 1) {
                                _tab_select(GUI->col_table,lut,cur/CELLNUM-1,cur%CELLNUM,false);
                                update_palette_interval(GUI->col_table,GUI->quantization,hdr);
                            }
                                           break;
                case XK_Down:
                                                if(cur/CELLNUM < lut->ncol/CELLNUM) {
                                _tab_select(GUI->col_table,lut,cur/CELLNUM+1,cur%CELLNUM,false);
                                update_palette_interval(GUI->col_table,GUI->quantization,hdr);
                            }
                                           break;
                case XK_Left:
                                                if(cur >= 1) {
                                _tab_select(GUI->col_table,lut,(cur-1)/CELLNUM,(cur-1)%CELLNUM,false);
                                update_palette_interval(GUI->col_table,GUI->quantization,hdr);
                            }
                                           break;
                case XK_Right:
                                                if(cur < lut->ncol - 1) {
                                _tab_select(GUI->col_table,lut,(cur+1)/CELLNUM,(cur+1)%CELLNUM,false);
                                update_palette_interval(GUI->col_table,GUI->quantization,hdr);
                            }
                                           break;
                case XK_Return:
                    _pal_pick(GUI->col_table,GUI->coloring,cur);
                    _refresh_coloring(ENV);
                    break;
            }
            break;
    }
    return FL_IGNORE;
}

/* palette modify menu */
void cb_pal_modify ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const uint8_t idx = COLORING_TAB_IDX(GUI->col_table);
    xg3d_lut *lut = COLORING_LUT(GUI->coloring);

    if(idx >= lut->ncol) return;

    switch(fl_get_menu(obj)) {
        case 1: /* invert */
            lut->colors[idx] = FL_PACK(
                    (uint8_t)~FL_GETR(lut->colors[idx]),
                    (uint8_t)~FL_GETG(lut->colors[idx]),
                    (uint8_t)~FL_GETB(lut->colors[idx]));
            break;
        case 2: /* reflect */
            if(idx >= lut->ncol / 2)
                for(int i=idx+1;i<(int)lut->ncol;i++)
                    lut->colors[i] = lut->colors[2*idx-i];
            else
                for(int i=idx-1;i>=0;i--)
                    lut->colors[i] = lut->colors[2*idx-i];
            break;
        case 3: /* highlight */
            memset(lut->colors,0,lut->ncol*sizeof(FL_PACKED));
            lut->colors[idx] = FL_PACK(255,255,255);
            break;
    }
    update_lut_pix(GUI->coloring);
    update_tab_canvas(GUI->coloring,GUI->col_table);
    _tab_select(GUI->col_table,lut,idx/CELLNUM,idx%CELLNUM,true);
    _refresh_coloring(ENV);
}

/* palette select menu */
#define ONLY 1
#define ADD 2
#define REMOVE 3
void cb_pal_select ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);
    xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);
    xg3d_view_workspace *vwk = VIEW_WORKSPACE(GUI->viewport);
    const enum xg3d_scope scope = QUANTIZATION_GET_SCOPE(GUI->quantization);
    const uint8_t idx = COLORING_TAB_IDX(GUI->col_table);
    const uint8_t s = fl_get_menu(obj) == REMOVE ? 1 : 0; /* 1 if remove, 0 if only/add */
    const grid3d *dat;
    index_t nvox;
    grid3d *sel;
    double val;

    fl_show_oneliner(_("Selecting..."),fl_scrw/2,fl_scrh/2);

    if(scope == SCOPE_GLOBAL) {
        dat = DATA->g3d;
        sel = swk->g3d;
        if(fl_get_menu(obj) == ONLY)
            fl_call_object_callback(GUI->selection->btn_clear_global);
    } else {
        dat = VIEW_SLICE(GUI->viewport);
        sel = vwk->sel->g3d;
        if(fl_get_menu(obj) == ONLY)
            fl_call_object_callback(GUI->selection->btn_clear_slice);
    }

    sel_mode_level(sel,dat,idx,0,s,qwk->info,&nvox,&val);

    if(scope == SCOPE_SLICE) {
        g3d_ret ret = g3d_replace_slice(swk->g3d,vwk->sel->g3d,VIEW_K(GUI->viewport),vwk->plane);
        if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
    }

    swk->nvox  += (1 - 2*s) * nvox;
    swk->value += (1 - 2*s) * val;

    replace_selslice(GUI->viewport,GUI->selection);
    sel_sanity_check(GUI->viewport,GUI->selection);
    sel_update_info(GUI->viewport,GUI->selection);
    if(GUI->viewport != NULL) refresh_canvas(ENV);

    fl_hide_oneliner();
}
#undef ONLY
#undef ADD
#undef REMOVE

void cb_occup_compute ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const enum xg3d_scope scope = QUANTIZATION_GET_SCOPE(GUI->quantization);
    const uint8_t cur = COLORING_TAB_IDX(GUI->col_table);
    index_t occup=0;

    occup = (scope == SCOPE_GLOBAL) ?
        g3d_quant_get_occup(DATA->g3d,cur,QUANTIZATION_INFO(GUI->quantization),true) :
        g3d_quant_get_occup(VIEW_QUANT_SLICE(GUI->viewport),cur,NULL,false);

    fl_set_object_label_f(GUI->col_table->txt_occup,"%"PRIindex,occup);
}

/* choose LUT from file */
void cb_lutfile ( FL_OBJECT * obj  , long data  ) {
    uint8_t *lutfile = COLORING_LUTFILE(GUI->col_table);
    const xg3d_lut *lut = COLORING_LUT(GUI->coloring);
    const char *file = NULL;
    size_t items = 0;
    uint8_t *tmp;
    char *base;
    FILE *fp;

    switch(data) {
        case 0: /* Load binary LUT */
            file = fl_show_fselector(_("Load LUT from..."),"","*.lut","");
            if(file != NULL) {
                fp = fopen(file,"rb");
                if(fp == NULL) {
                    fl_show_alert2(0,_("Cannot open file for reading!\f%s"),file);
                    return;
                }
                tmp = malloc(768 * sizeof(uint8_t));
                if(fread(tmp,sizeof(uint8_t),4,fp) == 4) {
                    if(tmp[0] == 'I' && tmp[1] == 'C' && tmp[2] == 'O' && tmp[3] == 'L') { // ICOL
                        if(fseek(fp,28,SEEK_CUR) == 0)
                            items = fread(tmp,sizeof(uint8_t),768,fp);
                    } else
                        items = 4 + fread(tmp + 4,sizeof(uint8_t),768 - 4,fp);
                }
                fclose(fp);
                base = strdup(file);
                if(items == 768) { /* Success reading binary lut file */
                    memcpy(lutfile,tmp,768 * sizeof(uint8_t));
                    fl_set_object_label_f(GUI->col_table->txt_file,"%s",basename(base)); free(base);
                    fl_set_button(GUI->col_table->btn_lutfile,1);
                    fl_call_object_callback(GUI->col_table->btn_lutfile);
                } else {
                    fl_show_alert2(0,_("No LUT loaded!\fReading LUT from %s failed."),basename(base));
                    free(tmp); free(base);
                    return;
                }
            }
            break;
        case 1: /* Save text LUT */
            file = fl_show_fselector(_("Save LUT as..."),"","*.lut","");
            if(file != NULL) {
                fp = fopen(file,"w");
                if(fp == NULL) {
                    fl_show_alert2(0,_("Cannot open file for writing!\f%s"),file);
                    return;
                }
                for(size_t i=0;i<lut->ncol;i++)
                    fprintf(fp,"%3u %3u %3u\n",FL_GETR(lut->colors[i]),FL_GETG(lut->colors[i]),FL_GETB(lut->colors[i]));
                fclose(fp);
            }
            break;
    }
}

/* use lut from file button */
void cb_lutfile_toggle ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const int pup_mode = fl_get_button(obj) ? FL_PUP_GRAY : FL_PUP_NONE;
    const xg3d_lut *lut = COLORING_LUT(GUI->coloring);

    fl_set_menu_item_mode(GUI->col_table->menu_mod,2,pup_mode);
    fl_set_menu_item_mode(GUI->col_table->menu_mod,3,pup_mode);

    if(fl_get_button(obj)) {
        update_coloring(GUI->coloring,lut->ncol);
        _refresh_coloring(ENV);
    }
}

/* edit current LUT as ramps */
void cb_pal_ramps ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const xg3d_lut *lut = COLORING_LUT(GUI->coloring);

    fl_call_object_callback(GUI->col_ramp->btn_red_clear);
    fl_call_object_callback(GUI->col_ramp->btn_green_clear);
    fl_call_object_callback(GUI->col_ramp->btn_blue_clear);

    if(lut->ncol < 3) {
        unsigned int n = 2;
        double *x = malloc(n*sizeof(double));
        double *y = malloc(n*sizeof(double));
        x[0] = 0.0; x[1] = 1.0;

                            y[0] = (double)FL_GETR(lut->colors[0]) / FL_PCMAX;
            y[1] = (lut->ncol == 1) ? y[0] : (double)FL_GETR(lut->colors[1]) / FL_PCMAX;
            fl_set_xyplot_data_double(GUI->col_ramp->plot_red,x,y,n,"","","");
                   y[0] = (double)FL_GETG(lut->colors[0]) / FL_PCMAX;
            y[1] = (lut->ncol == 1) ? y[0] : (double)FL_GETG(lut->colors[1]) / FL_PCMAX;
            fl_set_xyplot_data_double(GUI->col_ramp->plot_green,x,y,n,"","","");
                   y[0] = (double)FL_GETB(lut->colors[0]) / FL_PCMAX;
            y[1] = (lut->ncol == 1) ? y[0] : (double)FL_GETB(lut->colors[1]) / FL_PCMAX;
            fl_set_xyplot_data_double(GUI->col_ramp->plot_blue,x,y,n,"","","");
       
        free(x); free(y);
    } else {
        unsigned int n;
        double *x,*y;

                            n = lut->ncol;
            x = malloc(n*sizeof(double));
            y = malloc(n*sizeof(double));
            for(unsigned int l=0;l<n;l++) {
                x[l] = (double)l/(n - 1);
                y[l] = (double)FL_GETR(lut->colors[l]) / FL_PCMAX;
            }
            linsimp(&x,&y,&n,5e-4); /* modifies all of x,y,n */
            fl_set_xyplot_data_double(GUI->col_ramp->plot_red,x,y,n,"","","");
            free(x); free(y);
                   n = lut->ncol;
            x = malloc(n*sizeof(double));
            y = malloc(n*sizeof(double));
            for(unsigned int l=0;l<n;l++) {
                x[l] = (double)l/(n - 1);
                y[l] = (double)FL_GETG(lut->colors[l]) / FL_PCMAX;
            }
            linsimp(&x,&y,&n,5e-4); /* modifies all of x,y,n */
            fl_set_xyplot_data_double(GUI->col_ramp->plot_green,x,y,n,"","","");
            free(x); free(y);
                   n = lut->ncol;
            x = malloc(n*sizeof(double));
            y = malloc(n*sizeof(double));
            for(unsigned int l=0;l<n;l++) {
                x[l] = (double)l/(n - 1);
                y[l] = (double)FL_GETB(lut->colors[l]) / FL_PCMAX;
            }
            linsimp(&x,&y,&n,5e-4); /* modifies all of x,y,n */
            fl_set_xyplot_data_double(GUI->col_ramp->plot_blue,x,y,n,"","","");
            free(x); free(y);
           }

    fl_set_folder_bynumber(GUI->coloring->tf_coloring,4);
    fl_call_object_callback(GUI->coloring->tf_coloring);
}

/* =============
 * FORM COL_RAMP
 * ============= */

#define RED 0
#define GREEN 1
#define BLUE 2
#define SMOOTH_SPLINE gsl_interp_steffen

void init_ramp_plots(FD_col_ramp *col_ramp) {
    static float zone[2] = {0.0,1.0};

    fl_set_xyplot_data(col_ramp->plot_red,zone,zone,2,"","","");
    fl_set_xyplot_data(col_ramp->plot_green,zone,zone,2,"","","");
    fl_set_xyplot_data(col_ramp->plot_blue,zone,zone,2,"","","");
}

void cb_ramp_update ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const xg3d_lut *lut = COLORING_LUT(GUI->coloring);

    update_coloring(GUI->coloring,lut->ncol);
    _refresh_coloring(ENV);
}

void cb_ramp_pm ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *p = NULL,
              *m = NULL,
              *l = NULL;

    switch(data) {
        case RED:
            p = GUI->col_ramp->btn_red_plus;
            m = GUI->col_ramp->btn_red_minus;
            l = GUI->col_ramp->plot_red;
            break;
        case GREEN:
            p = GUI->col_ramp->btn_green_plus;
            m = GUI->col_ramp->btn_green_minus;
            l = GUI->col_ramp->plot_green;
            break;
        case BLUE:
            p = GUI->col_ramp->btn_blue_plus;
            m = GUI->col_ramp->btn_blue_minus;
            l = GUI->col_ramp->plot_blue;
            break;
    }

    if(fl_get_button(p) && fl_get_button(m))
        fl_set_button(obj == p ? m : p,0);

    fl_set_cursor(FL_ObjWin(obj),XC_target);

    if(l != GUI->col_ramp->plot_red)
        fl_deactivate_object(GUI->col_ramp->plot_red);
    if(l != GUI->col_ramp->plot_green)
        fl_deactivate_object(GUI->col_ramp->plot_green);
    if(l != GUI->col_ramp->plot_blue)
        fl_deactivate_object(GUI->col_ramp->plot_blue);
}

void cb_ramp_smooth ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *plot = NULL;

    switch(data) {
        case RED:   plot = GUI->col_ramp->plot_red;   break;
        case GREEN: plot = GUI->col_ramp->plot_green; break;
        case BLUE:  plot = GUI->col_ramp->plot_blue;  break;
    }

    if(fl_get_button(obj)) {
        if((unsigned int)fl_get_xyplot_numdata(plot,0) >= gsl_interp_type_min_size(SMOOTH_SPLINE)) {
            fl_set_xyplot_overlay_type(plot,1,FL_NORMAL_XYPLOT);
            fl_call_object_callback(plot);
        } else
            fl_set_button(obj,0);
    } else {
        fl_set_xyplot_overlay_type(plot,1,FL_EMPTY_XYPLOT);
        fl_call_object_callback(plot);
    }
}

void cb_ramp_clear ( FL_OBJECT * obj  , long data  ) {
    static float zone[2] = {0.0,1.0};
    FL_OBJECT *plot   = NULL,
              *interp = NULL;

    switch(data) {
        case RED:
            plot   = GUI->col_ramp->plot_red;
            interp = GUI->col_ramp->btn_red_smooth;
            break;
        case GREEN:
            plot   = GUI->col_ramp->plot_green;
            interp = GUI->col_ramp->btn_green_smooth;
            break;
        case BLUE:
            plot   = GUI->col_ramp->plot_blue;
            interp = GUI->col_ramp->btn_blue_smooth;
            break;
    }

    fl_set_xyplot_data(plot,zone,zone,2,"","","");
    fl_set_button(interp,0);
    fl_call_object_callback(interp);
}

void cb_ramp_copy ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *plot   = NULL,
              *interp = NULL;
    const char *titem = fl_get_menu_text(obj);
    float *x,*y;
    int n;

    switch(data) {
        case RED:
            plot   = GUI->col_ramp->plot_red;
            interp = GUI->col_ramp->btn_red_smooth;
            break;
        case GREEN:
            plot   = GUI->col_ramp->plot_green;
            interp = GUI->col_ramp->btn_green_smooth;
            break;
        case BLUE:
            plot   = GUI->col_ramp->plot_blue;
            interp = GUI->col_ramp->btn_blue_smooth;
            break;
    }

    if(strcmp(titem,"Red") == 0) {
        fl_set_button(interp,fl_get_button(GUI->col_ramp->btn_red_smooth));
        fl_get_xyplot_data_pointer(GUI->col_ramp->plot_red,0,&x,&y,&n);
        fl_set_xyplot_data(plot,x,y,n,"","","");
        fl_call_object_callback(interp);
    } else if(strcmp(titem,"Green") == 0) {
        fl_set_button(interp,fl_get_button(GUI->col_ramp->btn_green_smooth));
        fl_get_xyplot_data_pointer(GUI->col_ramp->plot_green,0,&x,&y,&n);
        fl_set_xyplot_data(plot,x,y,n,"","","");
        fl_call_object_callback(interp);
    } else if(strcmp(titem,"Blue") == 0) {
        fl_set_button(interp,fl_get_button(GUI->col_ramp->btn_blue_smooth));
        fl_get_xyplot_data_pointer(GUI->col_ramp->plot_blue,0,&x,&y,&n);
        fl_set_xyplot_data(plot,x,y,n,"","","");
        fl_call_object_callback(interp);
    }
}

/* remove data point idx from plot data */
static void _remove_xyplot_data(FL_OBJECT *obj, const int idx) {
    float *x,*y,*xnew,*ynew;
    int n;

    fl_get_xyplot_data_pointer(obj,0,&x,&y,&n); n--;
    xnew = malloc(n*sizeof(float)); ynew = malloc(n*sizeof(float));
    memcpy(xnew,x,idx*sizeof(float)); memcpy(&xnew[idx],&x[idx+1],(n-idx)*sizeof(float));
    memcpy(ynew,y,idx*sizeof(float)); memcpy(&ynew[idx],&y[idx+1],(n-idx)*sizeof(float));
    fl_set_xyplot_data(obj,xnew,ynew,n,"","","");
}

/* TODO: fix second button release while in plus/minus mode */
int post_ramp_red ( FL_OBJECT * obj  , int event  , FL_Coord mx  , FL_Coord my  , int key  , void * xev  ) {
    (void)xev;
    xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);    float *x,*y;
    float wx,wy;
    const float ax = obj->w-4,
    bx = obj->x+1,
    ay = -(obj->h-4),
    by = obj->y+1-ay;
    int n,idx,pt;
    char buf[64];
    int asc;
    const int ch = fl_get_char_height(FL_FIXED_STYLE,FL_TINY_SIZE,&asc,NULL);
    const int flag_calib = fl_get_button(GUI->quantization->btn_units);
    const xg3d_header *hdr = DATA->hdr;
    double min,max;
   
    if(event == FL_PUSH && key == FL_LEFT_MOUSE) {
        fl_get_xyplot(obj,&wx,&wy,&pt);
        fl_get_xyplot_data_pointer(obj,0,&x,&y,&n);
        if(fl_get_button(GUI->col_ramp->btn_red_plus)) {
            if(pt == -1) {
                wx = (mx-bx)/ax; wy = (my-by)/ay;
                idx = 0; for(int i=0;i<n;i++) { if(x[i] < wx) { idx = i; }}
                if(x[idx+1] == wx) {
                    fl_set_button(GUI->col_ramp->btn_red_plus,0);
                    return FL_IGNORE;
                }
                fl_insert_xyplot_data(obj,0,idx,wx,wy);
                fl_call_object_callback(obj);
            }
            fl_set_button(GUI->col_ramp->btn_red_plus,0);
            fl_activate_object(GUI->col_ramp->plot_red);
            fl_activate_object(GUI->col_ramp->plot_green);
            fl_activate_object(GUI->col_ramp->plot_blue);
        }
        if(fl_get_button(GUI->col_ramp->btn_red_minus)) {
            if(pt != -1) {
                idx = 0; for(int i=0;i<n;i++) { if(x[i] < wx) { idx++; }}
                if(idx == 0 || idx == n-1) {
                    fl_set_button(GUI->col_ramp->btn_red_minus,0);
                    return FL_IGNORE;
                }
                _remove_xyplot_data(obj,idx);
                if((unsigned int)n < 1 + gsl_interp_type_min_size(SMOOTH_SPLINE))
                fl_set_button(GUI->col_ramp->btn_red_smooth,0);
                fl_call_object_callback(obj);
            }
            fl_set_button(GUI->col_ramp->btn_red_minus,0);
            fl_activate_object(GUI->col_ramp->plot_red);
            fl_activate_object(GUI->col_ramp->plot_green);
            fl_activate_object(GUI->col_ramp->plot_blue);
        }
    } else if(event == FL_PUSH || event == FL_MOTION) {
        fl_get_xyplot(obj,&wx,&wy,&pt);
        if(pt != -1) {
            min = flag_calib ? CALIBVAL(hdr,qwk->min) : qwk->min;
            max = flag_calib ? CALIBVAL(hdr,qwk->max) : qwk->max;
            snprintf(buf,64,"%g %g",fma(wx,max-min,min),wy);
            fl_set_oneliner_font(FL_FIXED_STYLE,FL_TINY_SIZE);
            if(wy <= 0.5)
            fl_show_oneliner(buf,obj->x + obj->form->x + 1,obj->y + obj->form->y + 1);
            else
            fl_show_oneliner(buf,obj->x + obj->form->x + 1,obj->y + obj->form->y + obj->h - (ch+asc) + 1);
        }
    } else if(event == FL_RELEASE) {
        fl_hide_oneliner();
        fl_set_oneliner_font(FL_NORMAL_STYLE,FL_NORMAL_SIZE);
    }

    return FL_IGNORE;
}
int post_ramp_green ( FL_OBJECT * obj  , int event  , FL_Coord mx  , FL_Coord my  , int key  , void * xev  ) {
    (void)xev;
    xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);    float *x,*y;
    float wx,wy;
    const float ax = obj->w-4,
    bx = obj->x+1,
    ay = -(obj->h-4),
    by = obj->y+1-ay;
    int n,idx,pt;
    char buf[64];
    int asc;
    const int ch = fl_get_char_height(FL_FIXED_STYLE,FL_TINY_SIZE,&asc,NULL);
    const int flag_calib = fl_get_button(GUI->quantization->btn_units);
    const xg3d_header *hdr = DATA->hdr;
    double min,max;
   
    if(event == FL_PUSH && key == FL_LEFT_MOUSE) {
        fl_get_xyplot(obj,&wx,&wy,&pt);
        fl_get_xyplot_data_pointer(obj,0,&x,&y,&n);
        if(fl_get_button(GUI->col_ramp->btn_green_plus)) {
            if(pt == -1) {
                wx = (mx-bx)/ax; wy = (my-by)/ay;
                idx = 0; for(int i=0;i<n;i++) { if(x[i] < wx) { idx = i; }}
                if(x[idx+1] == wx) {
                    fl_set_button(GUI->col_ramp->btn_green_plus,0);
                    return FL_IGNORE;
                }
                fl_insert_xyplot_data(obj,0,idx,wx,wy);
                fl_call_object_callback(obj);
            }
            fl_set_button(GUI->col_ramp->btn_green_plus,0);
            fl_activate_object(GUI->col_ramp->plot_red);
            fl_activate_object(GUI->col_ramp->plot_green);
            fl_activate_object(GUI->col_ramp->plot_blue);
        }
        if(fl_get_button(GUI->col_ramp->btn_green_minus)) {
            if(pt != -1) {
                idx = 0; for(int i=0;i<n;i++) { if(x[i] < wx) { idx++; }}
                if(idx == 0 || idx == n-1) {
                    fl_set_button(GUI->col_ramp->btn_green_minus,0);
                    return FL_IGNORE;
                }
                _remove_xyplot_data(obj,idx);
                if((unsigned int)n < 1 + gsl_interp_type_min_size(SMOOTH_SPLINE))
                fl_set_button(GUI->col_ramp->btn_green_smooth,0);
                fl_call_object_callback(obj);
            }
            fl_set_button(GUI->col_ramp->btn_green_minus,0);
            fl_activate_object(GUI->col_ramp->plot_red);
            fl_activate_object(GUI->col_ramp->plot_green);
            fl_activate_object(GUI->col_ramp->plot_blue);
        }
    } else if(event == FL_PUSH || event == FL_MOTION) {
        fl_get_xyplot(obj,&wx,&wy,&pt);
        if(pt != -1) {
            min = flag_calib ? CALIBVAL(hdr,qwk->min) : qwk->min;
            max = flag_calib ? CALIBVAL(hdr,qwk->max) : qwk->max;
            snprintf(buf,64,"%g %g",fma(wx,max-min,min),wy);
            fl_set_oneliner_font(FL_FIXED_STYLE,FL_TINY_SIZE);
            if(wy <= 0.5)
            fl_show_oneliner(buf,obj->x + obj->form->x + 1,obj->y + obj->form->y + 1);
            else
            fl_show_oneliner(buf,obj->x + obj->form->x + 1,obj->y + obj->form->y + obj->h - (ch+asc) + 1);
        }
    } else if(event == FL_RELEASE) {
        fl_hide_oneliner();
        fl_set_oneliner_font(FL_NORMAL_STYLE,FL_NORMAL_SIZE);
    }

    return FL_IGNORE;
}
int post_ramp_blue ( FL_OBJECT * obj  , int event  , FL_Coord mx  , FL_Coord my  , int key  , void * xev  ) {
    (void)xev;
    xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);    float *x,*y;
    float wx,wy;
    const float ax = obj->w-4,
    bx = obj->x+1,
    ay = -(obj->h-4),
    by = obj->y+1-ay;
    int n,idx,pt;
    char buf[64];
    int asc;
    const int ch = fl_get_char_height(FL_FIXED_STYLE,FL_TINY_SIZE,&asc,NULL);
    const int flag_calib = fl_get_button(GUI->quantization->btn_units);
    const xg3d_header *hdr = DATA->hdr;
    double min,max;
   
    if(event == FL_PUSH && key == FL_LEFT_MOUSE) {
        fl_get_xyplot(obj,&wx,&wy,&pt);
        fl_get_xyplot_data_pointer(obj,0,&x,&y,&n);
        if(fl_get_button(GUI->col_ramp->btn_blue_plus)) {
            if(pt == -1) {
                wx = (mx-bx)/ax; wy = (my-by)/ay;
                idx = 0; for(int i=0;i<n;i++) { if(x[i] < wx) { idx = i; }}
                if(x[idx+1] == wx) {
                    fl_set_button(GUI->col_ramp->btn_blue_plus,0);
                    return FL_IGNORE;
                }
                fl_insert_xyplot_data(obj,0,idx,wx,wy);
                fl_call_object_callback(obj);
            }
            fl_set_button(GUI->col_ramp->btn_blue_plus,0);
            fl_activate_object(GUI->col_ramp->plot_red);
            fl_activate_object(GUI->col_ramp->plot_green);
            fl_activate_object(GUI->col_ramp->plot_blue);
        }
        if(fl_get_button(GUI->col_ramp->btn_blue_minus)) {
            if(pt != -1) {
                idx = 0; for(int i=0;i<n;i++) { if(x[i] < wx) { idx++; }}
                if(idx == 0 || idx == n-1) {
                    fl_set_button(GUI->col_ramp->btn_blue_minus,0);
                    return FL_IGNORE;
                }
                _remove_xyplot_data(obj,idx);
                if((unsigned int)n < 1 + gsl_interp_type_min_size(SMOOTH_SPLINE))
                fl_set_button(GUI->col_ramp->btn_blue_smooth,0);
                fl_call_object_callback(obj);
            }
            fl_set_button(GUI->col_ramp->btn_blue_minus,0);
            fl_activate_object(GUI->col_ramp->plot_red);
            fl_activate_object(GUI->col_ramp->plot_green);
            fl_activate_object(GUI->col_ramp->plot_blue);
        }
    } else if(event == FL_PUSH || event == FL_MOTION) {
        fl_get_xyplot(obj,&wx,&wy,&pt);
        if(pt != -1) {
            min = flag_calib ? CALIBVAL(hdr,qwk->min) : qwk->min;
            max = flag_calib ? CALIBVAL(hdr,qwk->max) : qwk->max;
            snprintf(buf,64,"%g %g",fma(wx,max-min,min),wy);
            fl_set_oneliner_font(FL_FIXED_STYLE,FL_TINY_SIZE);
            if(wy <= 0.5)
            fl_show_oneliner(buf,obj->x + obj->form->x + 1,obj->y + obj->form->y + 1);
            else
            fl_show_oneliner(buf,obj->x + obj->form->x + 1,obj->y + obj->form->y + obj->h - (ch+asc) + 1);
        }
    } else if(event == FL_RELEASE) {
        fl_hide_oneliner();
        fl_set_oneliner_font(FL_NORMAL_STYLE,FL_NORMAL_SIZE);
    }

    return FL_IGNORE;
}

void cb_ramp_io ( FL_OBJECT * obj  , long data  ) {
    const char *file;
    float *x,*y;
    FILE *fp;
    int n;

    switch(data) {
        case 0: /* Import */
            file = fl_show_fselector(_("Load ramps from..."),"","*.txt","");
            if(file != NULL) {
                fp = fopen(file,"r");
                if(fp == NULL) {
                    fl_show_alert2(0,_("Cannot open file for reading!\f%s"),file);
                    return;
                }

                fl_freeze_form(GUI->col_ramp->col_ramp);

                                                    fl_call_object_callback(GUI->col_ramp->btn_red_clear);
                    if(fscanf(fp,"#RED %d\n",&n) != 1) {
                        fl_show_alert2(0,_("Failure loading ramps from file!\f%s"),file);
                        return;
                    }
                    x = malloc(n*sizeof(float));
                    y = malloc(n*sizeof(float));
                    for(int i=0;i<n;i++)
                        if(fscanf(fp,"%f %f\n",x+i,y+i) != 2) {
                            fl_show_alert2(0,_("Failed parsing file!\f%s"),file);
                            free(x);
                            free(y);
                            return;
                        }
                    gsl_sort2_float(x,1,y,1,n);
                    fl_set_xyplot_data(GUI->col_ramp->plot_red,x,y,n,"","","");
                    free(x); free(y);
                    fl_call_object_callback(GUI->col_ramp->plot_red);
                                   fl_call_object_callback(GUI->col_ramp->btn_green_clear);
                    if(fscanf(fp,"#GREEN %d\n",&n) != 1) {
                        fl_show_alert2(0,_("Failure loading ramps from file!\f%s"),file);
                        return;
                    }
                    x = malloc(n*sizeof(float));
                    y = malloc(n*sizeof(float));
                    for(int i=0;i<n;i++)
                        if(fscanf(fp,"%f %f\n",x+i,y+i) != 2) {
                            fl_show_alert2(0,_("Failed parsing file!\f%s"),file);
                            free(x);
                            free(y);
                            return;
                        }
                    gsl_sort2_float(x,1,y,1,n);
                    fl_set_xyplot_data(GUI->col_ramp->plot_green,x,y,n,"","","");
                    free(x); free(y);
                    fl_call_object_callback(GUI->col_ramp->plot_green);
                                   fl_call_object_callback(GUI->col_ramp->btn_blue_clear);
                    if(fscanf(fp,"#BLUE %d\n",&n) != 1) {
                        fl_show_alert2(0,_("Failure loading ramps from file!\f%s"),file);
                        return;
                    }
                    x = malloc(n*sizeof(float));
                    y = malloc(n*sizeof(float));
                    for(int i=0;i<n;i++)
                        if(fscanf(fp,"%f %f\n",x+i,y+i) != 2) {
                            fl_show_alert2(0,_("Failed parsing file!\f%s"),file);
                            free(x);
                            free(y);
                            return;
                        }
                    gsl_sort2_float(x,1,y,1,n);
                    fl_set_xyplot_data(GUI->col_ramp->plot_blue,x,y,n,"","","");
                    free(x); free(y);
                    fl_call_object_callback(GUI->col_ramp->plot_blue);
               
                fl_unfreeze_form(GUI->col_ramp->col_ramp);

                fclose(fp);
            }
            break;
        case 1: /* Export */
            file = fl_show_fselector(_("Save ramps as..."),"","*.txt","");
            if(file != NULL) {
                fp = fopen(file,"w");
                if(fp == NULL) {
                    fl_show_alert2(0,_("Cannot open file for writing!\f%s"),file);
                    return;
                }

                                                    fl_get_xyplot_data_pointer(GUI->col_ramp->plot_red,0,&x,&y,&n);
                    fprintf(fp,"#RED %u\n",n);
                    for(int i=0;i<n;i++) fprintf(fp,"%g %g\n",x[i],y[i]);
                                   fl_get_xyplot_data_pointer(GUI->col_ramp->plot_green,0,&x,&y,&n);
                    fprintf(fp,"#GREEN %u\n",n);
                    for(int i=0;i<n;i++) fprintf(fp,"%g %g\n",x[i],y[i]);
                                   fl_get_xyplot_data_pointer(GUI->col_ramp->plot_blue,0,&x,&y,&n);
                    fprintf(fp,"#BLUE %u\n",n);
                    for(int i=0;i<n;i++) fprintf(fp,"%g %g\n",x[i],y[i]);
               
                fclose(fp);
            }
            break;
    }
}

void cb_ramp_preset ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    switch(fl_get_menu(obj)) {
        /* empty */
    }
}

/* ==================
 * COLORING FUNCTIONS
 * ================== */

/* update LUT */
void update_coloring(FD_coloring *coloring, const unsigned int nlvl) {
    const enum xg3d_color_mode mode = COLORING_MODE(coloring);
    const FL_PACKED lut_under = COLORING_LUT_UNDER(coloring);
    const FL_PACKED lut_over = COLORING_LUT_OVER(coloring);
    const int rev = fl_get_button(coloring->btn_reverse);
    const int inv = fl_get_button(coloring->btn_invert);
    const bool underflow = COLORING_UNDERFLOW(coloring);
    const bool overflow = COLORING_OVERFLOW(coloring);
    xg3d_lut *lut = COLORING_LUT(coloring);
    const uint8_t *lutfile;
    float hue;

    lut->ncol = nlvl;

    switch(mode) {
        case COLOR_GRADIENT:
            hue = COLORING_GET_HUE(COLORING_MODE_FDUI(coloring,mode));
            _update_lut_gradient(lut,rev,inv,hue,COLORING_GRAD_TYPE(COLORING_MODE_FDUI(coloring,mode)));
            break;
        case COLOR_PREDEF:
            _update_lut_predef(lut,rev,inv,COLORING_PREDEF_TYPE(COLORING_MODE_FDUI(coloring,mode)));
            break;
        case COLOR_RAMP:
            _update_lut_ramp(COLORING_MODE_FDUI(coloring,mode),lut,rev,inv);
            break;
        case COLOR_PALETTE:
            lutfile = COLORING_LUTFILE(COLORING_MODE_FDUI(coloring,mode));
            if(COLORING_USE_LUTFILE(COLORING_MODE_FDUI(coloring,mode)))
                _update_lut_file(lut,lutfile,rev,inv);
            break;
    }

    if(underflow) lut->colors[0]      = lut_under;
    if( overflow) lut->colors[nlvl-1] = lut_over;

    update_lut_pix(coloring);

    if(mode == COLOR_PALETTE) {
        update_tab_canvas(coloring,COLORING_MODE_FDUI(coloring,mode));
        _tab_select(COLORING_MODE_FDUI(coloring,mode),lut,0,0,true);
    }
}

/* ==================
 * SETTINGS FUNCTIONS
 * ================== */

void setup_coloring_settings(FD_coloring *coloring, const unsigned long valuemask, const xg3d_coloring_settings *settings) {
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FL_FORM *form;
    xg3d_lut *lut = COLORING_LUT(coloring);

    if(xg3d_settings_check(valuemask,COLORING_FLAG_REVERSE))
        fl_set_button(coloring->btn_reverse,settings->flag_reverse);
    if(xg3d_settings_check(valuemask,COLORING_FLAG_INVERT))
        fl_set_button(coloring->btn_invert,settings->flag_invert);
    if(xg3d_settings_check(valuemask,COLORING_SET_UNDER))
        refresh_flow_pix(coloring->btn_under,settings->lut_under);
    if(xg3d_settings_check(valuemask,COLORING_SET_OVER))
        refresh_flow_pix(coloring->btn_over,settings->lut_over);

    if(xg3d_settings_check(valuemask,COLORING_COLOR_MODE)) {
        form = fl_get_tabfolder_folder_bynumber(coloring->tf_coloring,settings->color_mode);
        fl_set_folder(coloring->tf_coloring,form);

        switch(settings->color_mode) {
            case COLOR_GRADIENT:
                col_grad = (FD_col_grad*)form->fdui;
                resize_form_tabfolder(coloring->tf_coloring,col_grad->box);
                if(xg3d_settings_check(valuemask,COLORING_SETT_HUE))
                    COLORING_SET_HUE(col_grad,settings->hue);
                if(xg3d_settings_check(valuemask,COLORING_SET_GRAD_TYPE)) {
                    COLORING_GRAD_TYPE(col_grad) = settings->grad_type;
                    switch(settings->grad_type) {
                        case COL_GRAD_VALUE:     fl_set_button(col_grad->btn_value,1);      break;
                        case COL_GRAD_SAT_HSL:   fl_set_button(col_grad->btn_saturation,1); break;
                        case COL_GRAD_LIGHTNESS: fl_set_button(col_grad->btn_lightness,1);  break;
                        case COL_GRAD_LUMA:      fl_set_button(col_grad->btn_luma,1);       break;
                        case COL_GRAD_GRAYSCALE: fl_set_button(col_grad->btn_grayscale,1);  break;
                        case COL_GRAD_HOTMETAL:  fl_set_button(col_grad->btn_hotmetal,1);   break;
                        case COL_GRAD_FIRE:      fl_set_button(col_grad->btn_fire,1);       break;
                        case COL_GRAD_TURBO:     fl_set_button(col_grad->btn_turbo,1);      break;
                        case COL_GRAD_RANDOM:    fl_set_button(col_grad->btn_random,1);     break;
                        case COL_GRAD_MSH:       fl_set_button(col_grad->btn_diverging,1);  break;
                        case COL_GRAD_MATPLOTLIB: /* separator only */ break;
                        case COL_GRAD_MAGMA:
                            fl_set_button(col_grad->btn_mpl,1);
                            fl_set_choice(col_grad->ch_grad_mpl,1);
                            break;
                        case COL_GRAD_INFERNO:
                            fl_set_button(col_grad->btn_mpl,1);
                            fl_set_choice(col_grad->ch_grad_mpl,2);
                            break;
                        case COL_GRAD_PLASMA:
                            fl_set_button(col_grad->btn_mpl,1);
                            fl_set_choice(col_grad->ch_grad_mpl,3);
                            break;
                        case COL_GRAD_VIRIDIS:
                            fl_set_button(col_grad->btn_mpl,1);
                            fl_set_choice(col_grad->ch_grad_mpl,4);
                            break;
                        case COL_GRAD_PARULA:
                            fl_set_button(col_grad->btn_mpl,1);
                            fl_set_choice(col_grad->ch_grad_mpl,5);
                            break;
                    }
                    if(settings->grad_type >= COL_GRAD_MATPLOTLIB)
                        activate_obj(col_grad->ch_grad_mpl,FL_BLACK);
                    else
                        deactivate_obj(col_grad->ch_grad_mpl);
                }
                break;
            case COLOR_PREDEF:
                col_predef = (FD_col_predef*)form->fdui;
                resize_form_tabfolder(coloring->tf_coloring,col_predef->box);
                if(xg3d_settings_check(valuemask,COLORING_SET_PREDEF_TYPE)) {
                    COLORING_PREDEF_TYPE(col_predef) = settings->predef_type;
                    fl_select_browser_line(col_predef->browser,settings->predef_type+1);
                }
                break;
            case COLOR_PALETTE:
                col_table = (FD_col_table*)form->fdui;
                resize_form_tabfolder(coloring->tf_coloring,col_table->box);
                COLORING_TAB_IDX(col_table) = 0;
                if(xg3d_settings_check(valuemask,COLORING_USE_LUT) && settings->use_lut) {
                    fl_set_button(col_table->btn_lutfile,1);
                    fl_set_object_label(col_table->txt_file,settings->lut_name);
                    memcpy(COLORING_LUTFILE(col_table),settings->lutfile,768*sizeof(uint8_t));
                } else {
                    if(xg3d_settings_check(valuemask,COLORING_TAB_NCOL|COLORING_TAB_LUT)) {
                        memcpy(lut->colors,settings->lut,settings->ncol*sizeof(FL_PACKED));
                        lut->ncol = settings->ncol;
                    }
                }
                break;
            case COLOR_RAMP:
                col_ramp = (FD_col_ramp*)form->fdui;
                resize_form_tabfolder(coloring->tf_coloring,col_ramp->box);
                if(xg3d_settings_check(valuemask,COLORING_RAMP_ACTIVE)) {
                    fl_set_button(col_ramp->btn_red,settings->ramp_active[0]);
                    fl_set_button(col_ramp->btn_green,settings->ramp_active[1]);
                    fl_set_button(col_ramp->btn_blue,settings->ramp_active[2]);
                }
                if(xg3d_settings_check(valuemask,COLORING_RAMP_SMOOTH)) {
                    fl_set_button(col_ramp->btn_red_smooth,settings->ramp_smooth[0]);
                    fl_set_button(col_ramp->btn_green_smooth,settings->ramp_smooth[1]);
                    fl_set_button(col_ramp->btn_blue_smooth,settings->ramp_smooth[2]);
                }
                if(xg3d_settings_check(valuemask,COLORING_RAMP_N|COLORING_RAMP_X|COLORING_RAMP_Y)) {
                    if(settings->ramp_n[0] >= 1) {
                        gsl_sort2_float(settings->ramp_x[0],1,settings->ramp_y[0],1,settings->ramp_n[0]);
                        fl_set_xyplot_data(col_ramp->plot_red,settings->ramp_x[0],settings->ramp_y[0],settings->ramp_n[0],"","","");
                    }
                    if(settings->ramp_n[1] >= 1) {
                        gsl_sort2_float(settings->ramp_x[1],1,settings->ramp_y[1],1,settings->ramp_n[1]);
                        fl_set_xyplot_data(col_ramp->plot_red,settings->ramp_x[1],settings->ramp_y[1],settings->ramp_n[1],"","","");
                    }
                    if(settings->ramp_n[2] >= 1) {
                        gsl_sort2_float(settings->ramp_x[2],1,settings->ramp_y[2],1,settings->ramp_n[2]);
                        fl_set_xyplot_data(col_ramp->plot_red,settings->ramp_x[2],settings->ramp_y[2],settings->ramp_n[2],"","","");
                    }
                }
                break;
        }
    }
}

xg3d_coloring_settings* store_coloring_settings(const FD_coloring *coloring, const unsigned long valuemask) {
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FL_FORM *form;
    const xg3d_lut *lut = COLORING_LUT(coloring);

    xg3d_coloring_settings *settings = calloc(1,sizeof(xg3d_coloring_settings));

    if(xg3d_settings_check(valuemask,COLORING_FLAG_REVERSE))
        settings->flag_reverse = fl_get_button(coloring->btn_reverse);
    if(xg3d_settings_check(valuemask,COLORING_FLAG_INVERT))
        settings->flag_invert = fl_get_button(coloring->btn_invert);
    if(xg3d_settings_check(valuemask,COLORING_SET_UNDER))
        settings->lut_under = COLORING_LUT_UNDER(coloring);
    if(xg3d_settings_check(valuemask,COLORING_SET_OVER))
        settings->lut_over = COLORING_LUT_OVER(coloring);

    if(xg3d_settings_check(valuemask,COLORING_COLOR_MODE)) {
        settings->color_mode = COLORING_MODE(coloring);
        form = fl_get_tabfolder_folder_bynumber(coloring->tf_coloring,settings->color_mode);
        switch(settings->color_mode) {
            case COLOR_GRADIENT:
                col_grad = (FD_col_grad*)form->fdui;
                if(xg3d_settings_check(valuemask,COLORING_SET_GRAD_TYPE))
                    settings->grad_type = COLORING_GRAD_TYPE(col_grad);
                if(xg3d_settings_check(valuemask,COLORING_SETT_HUE))
                    settings->hue = COLORING_GET_HUE(col_grad);
                break;
            case COLOR_PREDEF:
                col_predef = (FD_col_predef*)form->fdui;
                if(xg3d_settings_check(valuemask,COLORING_SET_PREDEF_TYPE))
                    settings->predef_type = COLORING_PREDEF_TYPE(col_predef);
                break;
            case COLOR_PALETTE:
                col_table = (FD_col_table*)form->fdui;
                if(xg3d_settings_check(valuemask,COLORING_USE_LUT) && (settings->use_lut = fl_get_button(col_table->btn_lutfile))) {
                    memcpy(settings->lutfile,COLORING_LUTFILE(col_table),768*sizeof(uint8_t));
                    settings->lut_name = strdup(fl_get_object_label(col_table->txt_file));
                } else {
                    if(xg3d_settings_check(valuemask,COLORING_TAB_NCOL|COLORING_TAB_LUT)) {
                        memcpy(settings->lut,lut->colors,lut->ncol*sizeof(FL_PACKED));
                        settings->ncol = lut->ncol;
                    }
                }
                break;
            case COLOR_RAMP:
                col_ramp = (FD_col_ramp*)form->fdui;
                if(xg3d_settings_check(valuemask,COLORING_RAMP_ACTIVE)) {
                    settings->ramp_active[0] = fl_get_button(col_ramp->btn_red);
                    settings->ramp_active[1] = fl_get_button(col_ramp->btn_green);
                    settings->ramp_active[2] = fl_get_button(col_ramp->btn_blue);
                }
                if(xg3d_settings_check(valuemask,COLORING_RAMP_SMOOTH)) {
                    settings->ramp_smooth[0] = fl_get_button(col_ramp->btn_red_smooth);
                    settings->ramp_smooth[1] = fl_get_button(col_ramp->btn_green_smooth);
                    settings->ramp_smooth[2] = fl_get_button(col_ramp->btn_blue_smooth);
                }
                if(xg3d_settings_check(valuemask,COLORING_RAMP_N|COLORING_RAMP_X|COLORING_RAMP_Y)) {
                    fl_get_xyplot_data_pointer(col_ramp->plot_red,1,&settings->ramp_x[0],&settings->ramp_y[0],&settings->ramp_n[0]);
                    fl_get_xyplot_data_pointer(col_ramp->plot_green,1,&settings->ramp_x[1],&settings->ramp_y[1],&settings->ramp_n[1]);
                    fl_get_xyplot_data_pointer(col_ramp->plot_blue,1,&settings->ramp_x[2],&settings->ramp_y[2],&settings->ramp_n[2]);
                }
                break;
        }
    }

    return settings;
}
