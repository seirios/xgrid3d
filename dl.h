#ifndef XG3D_dl_h_
#define XG3D_dl_h_

#include "grid3d.h"

typedef void (_selopf)(const int,void*,const coord_t,const coord_t,const coord_t);

void line3d(int,int,int,int,int,int,_selopf*,const int,void*);

#endif
