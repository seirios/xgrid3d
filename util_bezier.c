#include "util_bezier.h"

#include <string.h>
#include <math.h>

#include "bernstein_polynomial.h"
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_spline.h>

static double* linspace(double lo, double hi, int n) {
    double *lin = malloc(n * sizeof(double));
    double wid = hi - lo;

    for(int i=0;i<n;i++)
        lin[i] = fma((double)i/(n-1),wid,lo);

    return lin;
}

static double* diff(const double *x, int N) {
    double *y = malloc((N-1) * sizeof(double));

    for(int i=0;i<N-1;i++)
        y[i] = x[i+1] - x[i];

    return y;
}

static double* Bezier(const double *points, int N, const double *at, int num) {
    double *bernstein = malloc(num * N * sizeof(double));
    for(int i=0;i<num;i++) {
        double *tmp = bernstein_poly_01(N-1,at[i]);
        memcpy(&bernstein[N*i],tmp,N*sizeof(double));
        free(tmp);
    }

    double *matrix = calloc(num * 2,sizeof(double));
    for(int i=0;i<N;i++)
        cblas_dger(CblasRowMajor,num,2,1.0,&bernstein[i],N,&points[i],N,matrix,2);

    free(bernstein);

    return matrix;
}

static double* get_bezier_points_at(const double *at, int num, const double *points, int N) {
    static const int grid = 256;

    double *t = linspace(0.0,1.0,grid);
    double *matrix = Bezier(points,N,t,grid);

    double *x = malloc(grid * sizeof(double));
    double *y = malloc(grid * sizeof(double));
    for(int i=0;i<grid;i++) {
        x[i] = matrix[2*i+0];
        y[i] = matrix[2*i+1];
    }
    free(matrix);

    double *x_deltas = diff(x,grid); free(x);
    double *y_deltas = diff(y,grid); free(y);

    double *arclength_deltas = malloc(grid * sizeof(double));
    arclength_deltas[0] = 0.0;
    for(int i=1;i<grid;i++)
        arclength_deltas[i] = hypot(x_deltas[i-1],y_deltas[i-1]);
    free(x_deltas); free(y_deltas);

    double *arclength = malloc(grid * sizeof(double));
    arclength[0] = 0.0;
    for(int i=1;i<grid;i++)
        arclength[i] = arclength[i-1] + arclength_deltas[i];
    free(arclength_deltas);

    for(int i=0;i<grid;i++)
        arclength[i] /= arclength[grid-1];

    double *at_t = malloc(num * sizeof(double));

    gsl_interp_accel *acc = gsl_interp_accel_alloc();
    gsl_spline *spline = gsl_spline_alloc(gsl_interp_linear,grid);
    gsl_spline_init(spline,arclength,t,grid);
    for(int i=0;i<num;i++)
        at_t[i] = gsl_spline_eval(spline,at[i],acc);
    gsl_spline_free(spline);
    gsl_interp_accel_free(acc);
    free(arclength); free(t);

    matrix = Bezier(points,N,at_t,num);
    free(at_t);

    return matrix;
}

void get_Jpapbp(int num, const double *points, int N, double min_Jp, double max_Jp, double **Jp_o, double **ap_o, double **bp_o) {
    double *at = linspace(0.0,1.0,num);
    double *matrix = get_bezier_points_at(at,num,points,N);
    double delta = max_Jp - min_Jp;

    *Jp_o = malloc(num * sizeof(double));
    *ap_o = malloc(num * sizeof(double));
    *bp_o = malloc(num * sizeof(double));
    for(int i=0;i<num;i++) {
        (*Jp_o)[i] = fma(at[i],delta,min_Jp);
        (*ap_o)[i] = matrix[2*i+0];
        (*bp_o)[i] = matrix[2*i+1];
    }

    free(at);
    free(matrix);
}
