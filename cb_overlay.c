#include <X11/extensions/Xrender.h>
#include <libgen.h>

#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "grid3d_visual.h"
#include "cb_datasets.h"
#include "cb_dialog.h"
#include "cb_viewport.h"
#include "cb_overlay.h"
#include "overlay_reg.h"
#include "cb_quantization.h"
#include "cb_overview.h"
#include "fd_calibration.h"
#include "cb_coloring.h"
#include "fd_operations.h"
#include "cb_rois.h"

#include "io.h"
#include "util.h"
#include "util_coordinates.h"
#include "util_units.h"

void deactivate_overlay(FD_overlay *overlay) {
    deactivate_obj(overlay->btn_calibration);
    deactivate_obj(overlay->btn_quantization);
    deactivate_obj(overlay->btn_coloring);
    deactivate_obj(overlay->ch_op);
    deactivate_obj(overlay->sli_alpha);
    deactivate_obj(overlay->btn_reset_alpha);
    deactivate_obj(overlay->ch_upos);
    deactivate_obj(overlay->sli_x);
    deactivate_obj(overlay->spin_x);
    deactivate_obj(overlay->sli_y);
    deactivate_obj(overlay->spin_y);
    deactivate_obj(overlay->sli_z);
    deactivate_obj(overlay->spin_z);
    deactivate_obj(overlay->btn_reset);
    deactivate_obj(overlay->btn_trans_set);
    deactivate_obj(overlay->btn_trans_get);
    deactivate_obj(overlay->ch_reg_mode);
    deactivate_obj(overlay->ch_reg_mask_ref);
    deactivate_obj(overlay->ch_reg_mask_flo);
    deactivate_obj(overlay->btn_reg_sym);
    deactivate_obj(overlay->btn_reg_init);
    deactivate_obj(overlay->btn_register);
    deactivate_obj(overlay->menu_trans_clip);
    deactivate_obj(overlay->btn_transform);
    deactivate_obj(overlay->btn_trans_reset);
    deactivate_obj(overlay->btn_trans_invert);
    deactivate_obj(overlay->ch_trans_interp);
    deactivate_obj(overlay->inp_fillval);
    deactivate_obj(overlay->inp_m00);
    deactivate_obj(overlay->inp_m01);
    deactivate_obj(overlay->inp_m02);
    deactivate_obj(overlay->inp_m03);
    deactivate_obj(overlay->inp_m10);
    deactivate_obj(overlay->inp_m11);
    deactivate_obj(overlay->inp_m12);
    deactivate_obj(overlay->inp_m13);
    deactivate_obj(overlay->inp_m20);
    deactivate_obj(overlay->inp_m21);
    deactivate_obj(overlay->inp_m22);
    deactivate_obj(overlay->inp_m23);
    deactivate_obj(overlay->box_alpha);
    deactivate_obj(overlay->box_A);
    deactivate_obj(overlay->box_b);
}

static void _activate_overlay(FD_overlay *overlay) {
    activate_obj(overlay->btn_calibration,FL_YELLOW);
    activate_obj(overlay->btn_quantization,FL_YELLOW);
    activate_obj(overlay->btn_coloring,FL_YELLOW);
    activate_obj(overlay->ch_op,FL_BLACK);
    activate_obj(overlay->sli_alpha,FL_YELLOW);
    activate_obj(overlay->btn_reset_alpha,FL_YELLOW);
    activate_obj(overlay->ch_upos,FL_BLACK);
    activate_obj(overlay->sli_x,FL_YELLOW);
    activate_obj(overlay->spin_x,FL_MCOL);
    activate_obj(overlay->sli_y,FL_YELLOW);
    activate_obj(overlay->spin_y,FL_MCOL);
    activate_obj(overlay->sli_z,FL_YELLOW);
    activate_obj(overlay->spin_z,FL_MCOL);
    activate_obj(overlay->btn_reset,FL_COL1);
    activate_obj(overlay->btn_trans_set,FL_COL1);
    activate_obj(overlay->btn_trans_get,FL_COL1);
    activate_obj(overlay->ch_reg_mode,FL_BLACK);
    activate_obj(overlay->ch_reg_mask_ref,FL_BLACK);
    if(fl_get_button(overlay->btn_reg_sym))
        activate_obj(overlay->ch_reg_mask_flo,FL_BLACK);
    activate_obj(overlay->btn_reg_sym,FL_YELLOW);
    activate_obj(overlay->btn_reg_init,FL_YELLOW);
    activate_obj(overlay->btn_register,FL_YELLOW);
    activate_obj(overlay->menu_trans_clip,FL_MCOL);
    activate_obj(overlay->btn_transform,FL_YELLOW);
    activate_obj(overlay->btn_trans_reset,FL_COL1);
    activate_obj(overlay->btn_trans_invert,FL_COL1);
    activate_obj(overlay->ch_trans_interp,FL_BLACK);
    activate_obj(overlay->inp_fillval,FL_MCOL);
    activate_obj(overlay->inp_m00,FL_MCOL);
    activate_obj(overlay->inp_m01,FL_MCOL);
    activate_obj(overlay->inp_m02,FL_MCOL);
    activate_obj(overlay->inp_m03,FL_MCOL);
    activate_obj(overlay->inp_m10,FL_MCOL);
    activate_obj(overlay->inp_m11,FL_MCOL);
    activate_obj(overlay->inp_m12,FL_MCOL);
    activate_obj(overlay->inp_m13,FL_MCOL);
    activate_obj(overlay->inp_m20,FL_MCOL);
    activate_obj(overlay->inp_m21,FL_MCOL);
    activate_obj(overlay->inp_m22,FL_MCOL);
    activate_obj(overlay->inp_m23,FL_MCOL);
    activate_obj(overlay->box_alpha,FL_BLACK);
    activate_obj(overlay->box_A,FL_BLACK);
    activate_obj(overlay->box_b,FL_BLACK);
}

#undef ENV
void setup_overlay(xg3d_env *ENV, xg3d_data *overlay) {
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
    xg3d_env *env_overlay = OVERLAY_ENV(GUI->overlay);
    xg3d_gui *gui_overlay = env_overlay->gui;
    double lo,hi;

    if(overlay != NULL) {
        if(ENV->data->overlay != overlay) {
            ENV->data->overlay = overlay;
            env_overlay->data->main = overlay;
            VIEW_DATA_LAYER(ov_viewports[PLANE_XY],LAYER_OVERLAY) = overlay;
            VIEW_DATA_LAYER(ov_viewports[PLANE_XZ],LAYER_OVERLAY) = overlay;
            VIEW_DATA_LAYER(ov_viewports[PLANE_ZY],LAYER_OVERLAY) = overlay;
        }

        lo = 1.0 - overlay->g3d->nx; hi = DATA->g3d->nx - 1.0;
        fl_set_spinner_bounds(GUI->overlay->spin_x,lo,hi);
        fl_set_slider_bounds(GUI->overlay->sli_x,lo,hi);
        lo = 1.0 - overlay->g3d->ny; hi = DATA->g3d->ny - 1.0;
        fl_set_spinner_bounds(GUI->overlay->spin_y,lo,hi);
        fl_set_slider_bounds(GUI->overlay->sli_y,lo,hi);
        lo = 1.0 - overlay->g3d->nz; hi = DATA->g3d->nz - 1.0;
        fl_set_spinner_bounds(GUI->overlay->spin_z,lo,hi);
        fl_set_slider_bounds(GUI->overlay->sli_z,lo,hi);

        fl_set_spinner_value(GUI->overlay->spin_x,0.0);
        fl_set_slider_value(GUI->overlay->sli_x,0.0);
        fl_set_spinner_value(GUI->overlay->spin_y,0.0);
        fl_set_slider_value(GUI->overlay->sli_y,0.0);
        fl_set_spinner_value(GUI->overlay->spin_z,0.0);
        fl_set_slider_value(GUI->overlay->sli_z,0.0);

        if(fl_get_button(GUI->overlay->btn_show))
            fl_trigger_object(GUI->overlay->btn_show);
    } else {
        ENV->data->overlay = NULL;
        env_overlay->data->main = NULL;
        quantization_workspace_clear(QUANTIZATION_WORKSPACE(gui_overlay->quantization),false);
        fl_set_slider_bounds(GUI->overlay->sli_x,-1,1);
        fl_set_slider_bounds(GUI->overlay->sli_y,-1,1);
        fl_set_slider_bounds(GUI->overlay->sli_z,-1,1);
        fl_set_spinner_value(GUI->overlay->spin_x,0);
        fl_set_slider_value(GUI->overlay->sli_x,0);
        fl_set_spinner_value(GUI->overlay->spin_y,0);
        fl_set_slider_value(GUI->overlay->sli_y,0);
        fl_set_spinner_value(GUI->overlay->spin_z,0);
        fl_set_slider_value(GUI->overlay->sli_z,0);
        fl_set_button(GUI->overlay->btn_show,0);
        fl_trigger_object(GUI->overlay->btn_show);
    }
}
#define ENV _default_env

void adapt_overlay_img(FD_viewport *view, FD_overlay *overlay, const xg3d_lut *lut) {
    FL_IMAGE **img = (FL_IMAGE**)&VIEW_IMAGE_LAYER(view,LAYER_OVERLAY);
    const enum xg3d_orientation orient = VIEW_ORIENT(view);
    const enum g3d_slice_plane plane = VIEW_PLANE(view);
    const unsigned int zoom = VIEW_ZOOM_X(view); // X or Y is the same for overview
    const FL_IMAGE *amg = VIEW_IMAGE(view);
    int ofx,ofy;

    g3d_flimage_set_lut(*img,lut->colors);
    (*img)->fill_color = lut->colors[0];

    ofx = -OVERLAY_POS(overlay,PLANE_TO_J(plane)) * zoom;
    ofy = -OVERLAY_POS(overlay,PLANE_TO_I(plane)) * zoom;

    switch(orient) {
        case ORIENT_0:
            flimage_crop(*img,ofx,ofy,0,0);
            flimage_crop(*img,0,0,(*img)->w-amg->w,(*img)->h-amg->h);
            break;
        case ORIENT_90:
            flimage_crop(*img,ofy,0,0,ofx);
            flimage_crop(*img,0,(*img)->h-amg->h,(*img)->w-amg->w,0);
            break;
        case ORIENT_180:
            flimage_crop(*img,0,0,ofx,ofy);
            flimage_crop(*img,(*img)->w-amg->w,(*img)->h-amg->h,0,0);
            break;
        case ORIENT_270:
            flimage_crop(*img,0,ofx,ofy,0);
            flimage_crop(*img,(*img)->w-amg->w,0,0,(*img)->h-amg->h);
            break;
        case ORIENT_F0:
            flimage_crop(*img,ofx,0,0,ofy);
            flimage_crop(*img,0,(*img)->h-amg->h,(*img)->w-amg->w,0);
            break;
        case ORIENT_F90:
            flimage_crop(*img,ofy,ofx,0,0);
            flimage_crop(*img,0,0,(*img)->w-amg->w,(*img)->h-amg->h);
            break;
        case ORIENT_F180:
            flimage_crop(*img,0,ofy,ofx,0);
            flimage_crop(*img,(*img)->w-amg->w,0,0,(*img)->h-amg->h);
            break;
        case ORIENT_F270:
            flimage_crop(*img,0,0,ofy,ofx);
            flimage_crop(*img,(*img)->w-amg->w,(*img)->h-amg->h,0,0);
            break;
    }
}

void cb_overlay_blend ( FL_OBJECT * obj  , long data  ) {
    (void)obj;
(void)data;
}

void cb_overlay_refresh ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    refresh_ov_canvases(GUI->overview);
}

void cb_overlay_alpha_reset ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    fl_set_slider_value(GUI->overlay->sli_alpha,0.0);
    fl_call_object_callback(GUI->overlay->sli_alpha);
}

void cb_overlay_showhide ( FL_OBJECT * obj  , long data  ) {
    const int button = fl_get_button_numb(obj);
    FL_FORM *form = NULL;
    char buf[1024];

    switch(data) {
        case 0: /* Calibration */
            form = GUI->calibration->calibration;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Overlay calibration"));
            break;
        case 1: /* Quantization */
            form = GUI->quantization->quantization;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Overlay quantization"));
            break;
        case 2: /* Coloring */
            form = GUI->coloring->coloring;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Overlay coloring"));
            break;
    }

    if(button == FL_RIGHT_MOUSE && fl_form_is_visible(form)) {
        fl_set_button(obj,1);
        fl_raise_form(form);
        fl_winfocus(form->window);
        return;
    }

    if(fl_form_is_visible(form)) {
        fl_hide_form(form);
        fl_deactivate_form(form);
        return;
    } else {
        fl_activate_form(form);
        fl_show_form(form,FL_PLACE_SIZE,FL_FULLBORDER,buf);
        fl_raise_form(form);
        fl_winfocus(form->window);
    }

    if(form == GUI->coloring->coloring)
        update_lut_pix(GUI->coloring);
    else if(form == GUI->calibration->calibration)
        fl_set_form_size(form,225,116);
}

void cb_overlay_upos ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const xg3d_header *hdr = DATA->hdr;
    double ux,uy,uz;
    int prec,off;

    switch(fl_get_choice(obj)) {
        default:
        case 1: /* vox */
            ux = uy = uz = 1.0;
            prec = 0;
            break;
        case 2: /* cm */
            ux = hdr->vx;
            uy = hdr->vy;
            uz = hdr->vz;
            prec = 3;
            break;
        case 3: /* mm */
            ux = hdr->vx * CM_TO_MM;
            uy = hdr->vy * CM_TO_MM;
            uz = hdr->vz * CM_TO_MM;
            prec = 2;
            break;
    }

    off = (int)fl_get_slider_value(GUI->overlay->sli_x);
    fl_set_spinner_precision(GUI->overlay->spin_x,prec);
    fl_set_spinner_step(GUI->overlay->spin_x,ux);
    fl_set_spinner_value(GUI->overlay->spin_x,off*ux);

    off = (int)fl_get_slider_value(GUI->overlay->sli_y);
    fl_set_spinner_precision(GUI->overlay->spin_y,prec);
    fl_set_spinner_step(GUI->overlay->spin_y,uy);
    fl_set_spinner_value(GUI->overlay->spin_y,off*uy);

    off = (int)fl_get_slider_value(GUI->overlay->sli_z);
    fl_set_spinner_precision(GUI->overlay->spin_z,prec);
    fl_set_spinner_step(GUI->overlay->spin_z,uz);
    fl_set_spinner_value(GUI->overlay->spin_z,off*uz);
}

void cb_overlay_slider ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *spin = NULL;
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
    const double val = fl_get_slider_value(obj);
    const xg3d_header *hdr = DATA->hdr;
    double unit = 1.0;
    double ux,uy,uz;
    long int k;

    switch(fl_get_choice(GUI->overlay->ch_upos)) {
        default:
        case 1: /* vox */
            ux = uy = uz = 1.0;
            break;
        case 2: /* cm */
            ux = hdr->vx;
            uy = hdr->vy;
            uz = hdr->vz;
            break;
        case 3: /* mm */
            ux = hdr->vx * CM_TO_MM;
            uy = hdr->vy * CM_TO_MM;
            uz = hdr->vz * CM_TO_MM;
            break;
    }

    switch(data) {
        case 0: /* x */
            spin = GUI->overlay->spin_x;
            unit = ux;
            break;
        case 1: /* y */
            spin = GUI->overlay->spin_y;
            unit = uy;
            break;
        case 2: /* z */
            spin = GUI->overlay->spin_z;
            unit = uz;
            break;
    }

    k = VIEW_K(ov_viewports[PLANE_TO_K(data)]) - (int)val;
    VIEW_K_LAYER(ov_viewports[PLANE_TO_K(data)],LAYER_OVERLAY) = k < 0 ? -1 : k;
    fl_set_spinner_value(spin,val*unit);
    replace_ov_slice(ov_viewports[PLANE_TO_K(data)],LAYER_OVERLAY);
    replace_ov_quantslice(ov_viewports[PLANE_TO_K(data)],GUI->quantization,LAYER_OVERLAY);
    refresh_ov_canvases(GUI->overview);
}

void cb_overlay_spinner ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *slider = NULL;
    int val;

    switch(data) {
        case 0: slider = GUI->overlay->sli_x; break; /* x */
        case 1: slider = GUI->overlay->sli_y; break; /* y */
        case 2: slider = GUI->overlay->sli_z; break; /* z */
    }

    val = FL_nint(fl_get_spinner_value(obj)/fl_get_spinner_step(obj));
    if(val != (int)fl_get_slider_value(slider)) {
        fl_set_slider_value(slider,val);
        fl_call_object_callback(slider);
    } else
        fl_set_spinner_value(obj,val);
}

void cb_overlay_reset ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    fl_set_slider_value(GUI->overlay->sli_x,0);
    fl_set_slider_value(GUI->overlay->sli_y,0);
    fl_set_slider_value(GUI->overlay->sli_z,0);
    fl_call_object_callback(GUI->overlay->sli_x);
    fl_call_object_callback(GUI->overlay->sli_y);
    fl_call_object_callback(GUI->overlay->sli_z);
}

void cb_overlay_translation ( FL_OBJECT * obj  , long data  ) {
    const xg3d_header *hdr = DATA->hdr;

    switch(data) {
        case 0: /* Set */
            fl_set_input_f(GUI->overlay->inp_m03,"%g",
                    -(int)fl_get_slider_value(GUI->overlay->sli_x) * hdr->vx * CM_TO_MM);
            fl_set_input_f(GUI->overlay->inp_m13,"%g",
                    -(int)fl_get_slider_value(GUI->overlay->sli_y) * hdr->vy * CM_TO_MM);
            fl_set_input_f(GUI->overlay->inp_m23,"%g",
                    -(int)fl_get_slider_value(GUI->overlay->sli_z) * hdr->vz * CM_TO_MM);
            break;
        case 1: /* Get */
            fl_set_slider_value(GUI->overlay->sli_x,
                    FL_nint(atof(fl_get_input(GUI->overlay->inp_m03))*MM_TO_CM/hdr->vx));
            fl_call_object_callback(GUI->overlay->sli_x);
            fl_set_slider_value(GUI->overlay->sli_y,
                    FL_nint(atof(fl_get_input(GUI->overlay->inp_m13))*MM_TO_CM/hdr->vy));
            fl_call_object_callback(GUI->overlay->sli_y);
            fl_set_slider_value(GUI->overlay->sli_z,
                    FL_nint(atof(fl_get_input(GUI->overlay->inp_m23))*MM_TO_CM/hdr->vz));
            fl_call_object_callback(GUI->overlay->sli_z);
            break;
    }
}

void cb_overlay_show ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
    xg3d_env *env_main = (xg3d_env*)GUI->overview->overview->u_vdata;
    xg3d_gui *gui_main = env_main->gui;

    if(fl_get_button(obj)) {
        if(DATA == NULL) {
            fl_show_alert2(0,_("No overlay data!\fPlease load data first."));
            fl_set_button(obj,0);
            return;
        }
        _activate_overlay(GUI->overlay);

        /* turn off rotation preview */
        fl_set_button(gui_main->operations->btn_rot_preview,0);

        /* setup global quantization for overlay dataset (as in setup_from_data) */
        update_datarange(ENV,true);
        set_range_inpsli(ENV,true);
        compute_quantization(GUI->quantization,DATA->g3d);
        update_quant_coloring(GUI->quantization,GUI->coloring,DATA->hdr);

        VIEW_K_LAYER(ov_viewports[PLANE_XY],LAYER_OVERLAY) = VIEW_K(ov_viewports[PLANE_XY]);
        VIEW_K_LAYER(ov_viewports[PLANE_XZ],LAYER_OVERLAY) = VIEW_K(ov_viewports[PLANE_XZ]);
        VIEW_K_LAYER(ov_viewports[PLANE_ZY],LAYER_OVERLAY) = VIEW_K(ov_viewports[PLANE_ZY]);

        replace_ov_slices(ov_viewports,LAYER_OVERLAY);
        replace_ov_quantslices(ov_viewports,GUI->quantization,LAYER_OVERLAY);
        refresh_ov_canvases(GUI->overview);

        set_default_acq_settings(ENV,DATA->hdr,true);
    } else {
        /* close calibration form */
        if(fl_get_button(GUI->overlay->btn_calibration)) {
            fl_set_button(GUI->overlay->btn_calibration,0);
            fl_call_object_callback(GUI->overlay->btn_calibration);
        }
        /* close coloring form */
        if(fl_get_button(GUI->overlay->btn_coloring)) {
            fl_set_button(GUI->overlay->btn_coloring,0);
            fl_call_object_callback(GUI->overlay->btn_coloring);
        }
        /* close quantization form */
        if(fl_get_button(GUI->overlay->btn_quantization)) {
            fl_set_button(GUI->overlay->btn_quantization,0);
            fl_call_object_callback(GUI->overlay->btn_quantization);
        }
        deactivate_overlay(GUI->overlay);
        refresh_ov_canvases(GUI->overview);
    }
}

static void _set_matrix_from_input(FD_overlay *overlay, mat44 *aff) {
    aff->m[0][0] = atof(fl_get_input(overlay->inp_m00));
    aff->m[0][1] = atof(fl_get_input(overlay->inp_m01));
    aff->m[0][2] = atof(fl_get_input(overlay->inp_m02));
    aff->m[0][3] = atof(fl_get_input(overlay->inp_m03));
    aff->m[1][0] = atof(fl_get_input(overlay->inp_m10));
    aff->m[1][1] = atof(fl_get_input(overlay->inp_m11));
    aff->m[1][2] = atof(fl_get_input(overlay->inp_m12));
    aff->m[1][3] = atof(fl_get_input(overlay->inp_m13));
    aff->m[2][0] = atof(fl_get_input(overlay->inp_m20));
    aff->m[2][1] = atof(fl_get_input(overlay->inp_m21));
    aff->m[2][2] = atof(fl_get_input(overlay->inp_m22));
    aff->m[2][3] = atof(fl_get_input(overlay->inp_m23));
    aff->m[3][0] = aff->m[3][1] = aff->m[3][2] = 0.0;
    aff->m[3][3] = 1.0;
}

static void _set_input_from_matrix(FD_overlay *overlay, const mat44 aff) {
    fl_set_input_f(overlay->inp_m00,"%g",aff.m[0][0]);
    fl_set_input_f(overlay->inp_m01,"%g",aff.m[0][1]);
    fl_set_input_f(overlay->inp_m02,"%g",aff.m[0][2]);
    fl_set_input_f(overlay->inp_m03,"%g",aff.m[0][3]);
    fl_set_input_f(overlay->inp_m10,"%g",aff.m[1][0]);
    fl_set_input_f(overlay->inp_m11,"%g",aff.m[1][1]);
    fl_set_input_f(overlay->inp_m12,"%g",aff.m[1][2]);
    fl_set_input_f(overlay->inp_m13,"%g",aff.m[1][3]);
    fl_set_input_f(overlay->inp_m20,"%g",aff.m[2][0]);
    fl_set_input_f(overlay->inp_m21,"%g",aff.m[2][1]);
    fl_set_input_f(overlay->inp_m22,"%g",aff.m[2][2]);
    fl_set_input_f(overlay->inp_m23,"%g",aff.m[2][3]);
}

static int _reg_sel(FL_OBJECT *obj FL_UNUSED_ARG, long type FL_UNUSED_ARG, const void *data, long size) {
    mat44 aff;

    if(data != NULL && size > 0) {
        if(sscanf((const char*)data,
                    "%g %g %g %g\n%g %g %g %g\n%g %g %g %g\n%g %g %g %g",
                    &aff.m[0][0],&aff.m[0][1],&aff.m[0][2],&aff.m[0][3],
                    &aff.m[1][0],&aff.m[1][1],&aff.m[1][2],&aff.m[1][3],
                    &aff.m[2][0],&aff.m[2][1],&aff.m[2][2],&aff.m[2][3],
                    &aff.m[3][0],&aff.m[3][1],&aff.m[3][2],&aff.m[3][3]) == 16) {
            _set_input_from_matrix(GUI->overlay,aff);
            return 1;
        } else
            return 0;
    } else
        return 0;
}

void cb_trans_clip ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    char buf[512];
    mat44 aff;

    switch(fl_get_menu(obj)) {
        case 1: /* Copy */
            _set_matrix_from_input(GUI->overlay,&aff);
            snprintf(buf,sizeof(buf),
                    "%g %g %g %g\n%g %g %g %g\n%g %g %g %g\n%g %g %g %g",
                    aff.m[0][0],aff.m[0][1],aff.m[0][2],aff.m[0][3],
                    aff.m[1][0],aff.m[1][1],aff.m[1][2],aff.m[1][3],
                    aff.m[2][0],aff.m[2][1],aff.m[2][2],aff.m[2][3],
                    aff.m[3][0],aff.m[3][1],aff.m[3][2],aff.m[3][3]);
            fl_stuff_clipboard(obj,0,buf,strlen(buf),NULL);
            break;
        case 2: /* Paste */
            fl_request_clipboard(obj,0,_reg_sel);
            break;
    }
}

void cb_trans_input ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const char *tval = fl_get_input(obj);

    if(tval == NULL || strlen(tval) == 0)
        fl_set_input(obj,"0");
}

void cb_trans_matrix ( FL_OBJECT * obj  , long data  ) {
    static mat44 id = {{{1,0,0,0},
                        {0,1,0,0},
                        {0,0,1,0},
                        {0,0,0,1}}};
    mat44 aff;

    if(!data) /* reset */
        _set_input_from_matrix(GUI->overlay,id);
    else { /* invert */
        _set_matrix_from_input(GUI->overlay,&aff);
        aff = nifti_mat44_inverse(aff);
        if(aff.m[3][3] == 1.0)
            _set_input_from_matrix(GUI->overlay,aff);
        else
            fl_show_alert2(0,_("Singular matrix found!\fCannot compute inverse."));
    }
}

void cb_reg_sym ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_env *env_main = (xg3d_env*)GUI->overview->overview->u_vdata;

    if(fl_get_button(obj)) {
        refresh_overlay_rois(env_main);
        activate_obj(GUI->overlay->ch_reg_mask_flo,FL_BLACK);
    } else
        deactivate_obj(GUI->overlay->ch_reg_mask_flo);
}

void cb_overlay_register ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const xg3d_data *data_main = ((xg3d_env*)GUI->overview->overview->u_vdata)->data->main;
    const unsigned int i = fl_get_choice(GUI->overlay->ch_reg_mask_ref) - 1;
    const unsigned int j = fl_get_choice(GUI->overlay->ch_reg_mask_flo) - 1;
    const int sym = fl_get_button(GUI->overlay->btn_reg_sym);
    nifti_image *ref,*flo,*msk_ref=NULL,*msk_flo=NULL;
    const g3d_roi *roi;
    enum reg_mode mode;
    xg3d_header *hdr;
    grid3d *tmp;
    mat44 aff;
    int ret;

    switch(fl_get_choice(GUI->overlay->ch_reg_mode)) {
        default:
        case 1: mode = REG_RIGID;              break; /* Rigid only */
        case 2: mode = REG_RIGID | REG_AFFINE; break; /* Rigid + affine */
        case 3: mode = REG_AFFINE;             break; /* Affine only */
    }

    if(fl_get_button(GUI->overlay->btn_reg_init)) {
        _set_matrix_from_input(GUI->overlay,&aff);
        mode |= REG_INIT;
    }

    if(i >= 1) {
        roi = data_main->rois[i-1]->roi;
        tmp = g3d_alloc(G3D_UINT8,data_main->g3d->nx,data_main->g3d->ny,data_main->g3d->nz,.alloc=true);
        for(index_t ix=0;ix<roi->size;ix++)
            g3d_u8(tmp,ROI_IX(ix,roi,tmp)) = 1;
        hdr = xg3d_header_dup(data_main->hdr);
        free(hdr->file); hdr->file = strdup(roi->name);
        msk_ref = xg3d_to_nifti(tmp,hdr);
        xg3d_header_free(hdr);
        tmp->dat = NULL;
        g3d_free(tmp);
    }

    if(j >= 1) {
        roi = DATA->rois[i-1]->roi;
        tmp = g3d_alloc(G3D_UINT8,DATA->g3d->nx,DATA->g3d->ny,DATA->g3d->nz,.alloc=true);
        for(index_t ix=0;ix<roi->size;ix++)
            g3d_u8(tmp,ROI_IX(ix,roi,tmp)) = 1;
        hdr = xg3d_header_dup(DATA->hdr);
        free(hdr->file); hdr->file = strdup(roi->name);
        msk_flo = xg3d_to_nifti(tmp,hdr);
        xg3d_header_free(hdr);
        tmp->dat = NULL;
        g3d_free(tmp);
    }

    ref = xg3d_to_nifti(data_main->g3d,data_main->hdr);
    flo = xg3d_to_nifti(DATA->g3d,DATA->hdr);

    fl_show_oneliner(_("Registering..."),fl_scrw/2,fl_scrh/2);
    ret = register_aladin(ref,flo,msk_ref,msk_flo,&aff,mode,sym);
    fl_hide_oneliner();
    if(ret) {
        fl_show_alert2(0,_("Registration failed!\fPlease check input parameters."));
        return;
    }

    _set_input_from_matrix(GUI->overlay,aff);

    ref->data = NULL; nifti_image_free(ref);
    flo->data = NULL; nifti_image_free(flo);
    if(msk_ref != NULL) nifti_image_free(msk_ref);
    if(msk_flo != NULL) nifti_image_free(msk_flo);
}

void cb_overlay_transform ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_env *env_main = (xg3d_env*)GUI->overview->overview->u_vdata;
    xg3d_gui *gui_main = env_main->gui;
    const xg3d_data *data_main = env_main->data->main;
    nifti_image *ref,*flo;
    char buf[PATH_MAX];
    const char *tfill;
    nifti_image *res;
    xg3d_data *dat;
    int interp;
    float fill;
    mat44 aff;
    int ret;

    switch(fl_get_choice(GUI->overlay->ch_trans_interp)) {
        case 1: interp = 0; break; /* NN */
        default:
        case 2: interp = 1; break; /* LIN */
        case 3: interp = 3; break; /* CUB */
    }

    tfill = fl_get_input(GUI->overlay->inp_fillval);
    fill = strtod_checked(tfill,0.0);

    _set_matrix_from_input(GUI->overlay,&aff);

    ref = xg3d_to_nifti(data_main->g3d,data_main->hdr);
    flo = xg3d_to_nifti(DATA->g3d,DATA->hdr);

    fl_show_oneliner(_("Transforming..."),fl_scrw/2,fl_scrh/2);
    ret = register_resample(ref,flo,&aff,&res,interp,fill);
    fl_hide_oneliner();
    if(ret) {
        fl_show_alert2(0,_("Transformation failed!\fPlease check input parameters."));
        return;
    }

    /* add transformed dataset */
    dat = calloc(1,sizeof(xg3d_data));
    dat->hdr = xg3d_header_dup(DATA->hdr);
    snprintf(buf,sizeof(buf),"%s*",dat->hdr->file);
    free(dat->hdr->file);
    dat->hdr->file = strdup(buf);
    dat->g3d = g3d_alloc(DATA->g3d->type,res->nx,res->ny,res->nz,.alloc=false);
    dat->g3d->dat = res->data;
    xg3d_datasets_add(env_main->data,dat);
    refresh_datasets_browser(gui_main->datasets->browser,env_main->data);

    /* free structures but not data */
    res->data = NULL; nifti_image_free(res);
    ref->data = NULL; nifti_image_free(ref);
    flo->data = NULL; nifti_image_free(flo);
}
