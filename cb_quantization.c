#include <math.h>
#include <float.h>
#include <errno.h>
#include <libgen.h>

#include <gsl/gsl_sort.h>

#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "cb_quantization.h"
#include "cb_viewport.h"
#include "cb_mainview.h"
#include "cb_surface.h"
#include "cb_overview.h"
#include "cb_coloring.h"

#include "selection.h"

#include "util.h"
#include "util_histogram.h"
#include "util_units.h"
#include "util_coordinates.h"

#define FORMAT "%.13g"

#define FORM_W 300
#define FORM_H 450

xg3d_quantization_workspace *quantization_workspace_alloc(void) {
    xg3d_quantization_workspace *x = calloc(1,sizeof(xg3d_quantization_workspace));

    if(x == NULL) return NULL; /* malloc failed */

    x->info = calloc(1,sizeof(g3d_quant_info));
    if(x->info == NULL) {
        quantization_workspace_clear(x,true);
        return NULL; /* malloc failed */
    }

    return x;
}

void quantization_workspace_clear(xg3d_quantization_workspace *x, const bool delete) {
    if(x != NULL) {
        g3d_quant_info_clear(x->info,delete);
        if(delete) x->info = NULL;
        x->absmin = x->absmax = x->total = x->min = x->max = 0.0;
        if(delete) free(x);
    }
}

/* ===================
 * HISTOGRAM FUNCTIONS
 * =================== */

#define NBINS 288
static void _update_histogram(gsl_histogram **hist, const grid3d *data, const double min, const double max) {
    gsl_histogram *h;

    h = gsl_histogram_alloc(NBINS);
    gsl_histogram_set_ranges_uniform(h,min,nextafter(max,HUGE_VAL));
    histogram_fill(h,data);

    if(*hist != NULL) gsl_histogram_free(*hist);
    *hist = h;
}

#define HH 80
#define HW NBINS
static void _display_histogram(FD_quantization *quantization, const gsl_histogram *h) {
    const int skip = fl_get_button(quantization->btn_skip);
    const int log = fl_get_button(quantization->btn_log);

    if(h == NULL) return;

    double max[2];
    gsl_sort_largest(max,2,h->bin,1,h->n);
    double maxval = skip ? max[1] : max[0];
    if(log) maxval = log1p(maxval);

    unsigned char *bits = calloc(HH*HW/8,sizeof(unsigned char));
    fl_set_bitmap_data(quantization->bit_hist,HW,HH,bits);
    for(unsigned int j=0;j<NBINS/8;j++)
        for(unsigned int i=0;i<8;i++)
            for(unsigned int k=0;k<(unsigned int)round(HH * FL_clamp((log ?
                                log1p(gsl_histogram_get(h,8*j+i)) :
                                gsl_histogram_get(h,8*j+i))/maxval,0.0,1.0));k++)
                bits[NBINS/8*(HH-k-1)+j] += (1U << i);
    fl_set_bitmap_data(quantization->bit_hist,HW,HH,bits);
    free(bits);
}
#undef HH
#undef HW
#undef NBINS

void cb_quant_hist_refresh ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    _display_histogram(GUI->quantization,QUANTIZATION_HISTOGRAM(GUI->quantization));
}

#define CAL_TEST(x) (calib ? CALIBVAL(hdr,x) : (x))
#define CAL_STR(fmt,a,b) snprintf(buf,sizeof(buf),fmt,FL_min(CAL_TEST(a),CAL_TEST(b)),FL_max(CAL_TEST(a),CAL_TEST(b)));
const char* quant_level_string(FD_quantization *quantization, const uint8_t level, const xg3d_header *hdr, const bool calib) {
    const xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(quantization);
    const enum xg3d_quant_mode mode = QUANTIZATION_MODE(quantization);
    const unsigned int nlvl = qwk->info->nlevels;
    const double *bkpts = qwk->info->bkpts;
    const double absmin = qwk->absmin;
    const double absmax = qwk->absmax;
    static char buf[128];
    
    memset(buf,'\0',sizeof(buf));
    if(level >= nlvl) return buf;

    if(nlvl == 1)
        CAL_STR("[%.8g,%.8g]",absmin,absmax)
    else {
        switch(mode) {
            case QUANT_PREDEF:
                switch(QUANTIZATION_PREDEF_TYPE(fl_get_tabfolder_folder_bynumber(quantization->tf_qmode,mode)->fdui)) {
                    case QUANT_ORDMAG:
                        if(bkpts[level] < 0.0)
                            snprintf(buf,sizeof(buf),"-10%s",exp_to_utf8_sup((int)floor(log10(-bkpts[level]))));
                        else if(bkpts[level] > 0.0)
                            snprintf(buf,sizeof(buf),"+10%s",exp_to_utf8_sup((int)floor(log10(bkpts[level]))));
                        else
                            snprintf(buf,sizeof(buf),"0");
                        return buf;
                        break;
                    default: break; 
                }
                /* fall through */
            case QUANT_INTERVALS:
            case QUANT_CUSTOM:
                if(level == 0)
                    snprintf(buf,sizeof(buf),"(-∞,%.8g)",CAL_TEST(bkpts[level]));
                else if(level == nlvl - 1)
                    snprintf(buf,sizeof(buf),"[%.8g,∞)",CAL_TEST(bkpts[level-1]));
                else
                    CAL_STR("[%.8g,%.8g)",bkpts[level-1],bkpts[level]);
                break;
        }
    }
    return buf;
}
#undef CAL_TEST
#undef CAL_STR

#undef ENV
void set_range_inpsli(xg3d_env *ENV, const bool updglob) {
    const enum xg3d_scope scope = QUANTIZATION_GET_SCOPE(GUI->quantization);

    if(scope == SCOPE_SLICE || (scope == SCOPE_GLOBAL && updglob)) {
        FL_OBJECT *sli_min,*sli_max;
        const int flag_calib = QUANTIZATION_GET_CALIB(GUI->quantization);
        double min = QUANTIZATION_MIN(GUI->quantization);
        double max = QUANTIZATION_MAX(GUI->quantization);

        /* check which slider represents min and max */
        if(fl_get_slider_value(GUI->quant_rng_minmax->sli_min) >
                fl_get_slider_value(GUI->quant_rng_minmax->sli_max)) {
            sli_min = GUI->quant_rng_minmax->sli_max;
            sli_max = GUI->quant_rng_minmax->sli_min;
        } else {
            sli_min = GUI->quant_rng_minmax->sli_min;
            sli_max = GUI->quant_rng_minmax->sli_max;
        }

        fl_set_slider_value(sli_min,min); fl_set_slider_value(sli_max,max);
        fl_set_slider_value(GUI->quant_rng_cenwid->sli_cen,min+(max-min)/2.0);
        fl_set_slider_value(GUI->quant_rng_cenwid->sli_wid,(max-min)/2.0);
        if(flag_calib) {
            min = CALIBVAL(DATA->hdr,min);
            max = CALIBVAL(DATA->hdr,max);
        }
        fl_set_input_f(GUI->quant_rng_minmax->inp_min,FORMAT,min);
        fl_set_input_f(GUI->quant_rng_minmax->inp_max,FORMAT,max);
        fl_set_input_f(GUI->quant_rng_cenwid->inp_cen,FORMAT,min+(max-min)/2.0);
        fl_set_input_f(GUI->quant_rng_cenwid->inp_wid,FORMAT,(max-min)/2.0);
    }
}
#define ENV _default_env

#undef ENV
void update_datarange(xg3d_env *ENV, const bool updglob) {
    gsl_histogram **hist = (gsl_histogram**)&QUANTIZATION_HISTOGRAM(GUI->quantization);
    xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);
    const enum xg3d_scope scope = QUANTIZATION_GET_SCOPE(GUI->quantization);
    const grid3d *data = scope == SCOPE_GLOBAL ? DATA->g3d : VIEW_SLICE(GUI->viewport);
    const int flag_calib = QUANTIZATION_GET_CALIB(GUI->quantization);
    const xg3d_header *hdr = DATA->hdr;
    double *absmin = &qwk->absmin;
    double *absmax = &qwk->absmax;
    double *total = &qwk->total;
    double *min = &qwk->min;
    double *max = &qwk->max;
    g3d_stats *stats;
    double rng;

    if(scope == SCOPE_SLICE || (scope == SCOPE_GLOBAL && updglob)) {
        stats = g3d_stats_get(data,NULL,G3D_STATS_MINMAXSUM);
        *absmin = stats->min;
        *absmax = stats->max;
        *total = stats->sum;
        rng = stats->range;
        free(stats);
        *min = *absmin;
        *max = *absmax;
        _update_histogram(hist,data,*absmin,*absmax);
        _display_histogram(GUI->quantization,*hist);
        fl_set_slider_bounds(GUI->quant_rng_minmax->sli_min,*absmin,*absmax);
        fl_set_slider_bounds(GUI->quant_rng_minmax->sli_max,*absmin,*absmax);
        fl_set_slider_bounds(GUI->quant_rng_cenwid->sli_cen,*absmin,*absmax);
        fl_set_slider_bounds(GUI->quant_rng_cenwid->sli_wid,0.0,rng);
        fl_set_slider_increment(GUI->quant_rng_minmax->sli_min,0.01*rng,1);
        fl_set_slider_increment(GUI->quant_rng_minmax->sli_max,0.01*rng,1);
        fl_set_slider_increment(GUI->quant_rng_cenwid->sli_cen,0.01*rng,1);
        fl_set_slider_increment(GUI->quant_rng_cenwid->sli_wid,0.01*rng,1);
        fl_set_object_label_f(GUI->quantization->txt_vox,"%u",data->nvox);
        fl_set_object_label_f(GUI->quantization->txt_min,FORMAT,
                flag_calib ? CALIBVAL(hdr,*absmin) : *absmin);
        fl_set_object_label_f(GUI->quantization->txt_max,FORMAT,
                flag_calib ? CALIBVAL(hdr,*absmax) : *absmax);
        fl_set_object_label_f(GUI->quantization->txt_tot,FORMAT,
                flag_calib ? CALIBSUM(hdr,*total,data->nvox) : *total);
        fl_set_object_label_f(GUI->quantization->txt_avg,FORMAT,
               (flag_calib ? CALIBSUM(hdr,*total,data->nvox) : *total) / (double)data->nvox);
    }
}
#define ENV _default_env

/* computes quantization of data */
g3d_ret compute_quantization(FD_quantization *quantization, const grid3d *data) {
    xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(quantization);
    const enum xg3d_quant_mode mode = QUANTIZATION_MODE(quantization);
    const double absmin = qwk->absmin;
    const double absmax = qwk->absmax;
    enum xg3d_quant_predef_type predef_type;
    g3d_quant_info *info = qwk->info;
    enum g3d_quant_clamp_mode clamp;
    double min = qwk->min;
    double max = qwk->max;

    if(info == NULL) return G3D_FAIL; /* no input */

    /* do actual quantization */
    switch(mode) {
        case QUANT_INTERVALS:
            if(fl_get_button(((FD_quant_intervals*)QUANTIZATION_MODE_FDUI(quantization,mode))->btn_clamp))
                                                 clamp = QUANT_CLAMP_BOTH;
            else {
                if(min > absmin && max < absmax) clamp = QUANT_CLAMP_BOTH;
                else if(min > absmin)            clamp = QUANT_CLAMP_LO;
                else if(max < absmax)            clamp = QUANT_CLAMP_HI;
                else                             clamp = QUANT_CLAMP_NONE;
            }
            return g3d_quant_intervals(data,
                    fl_get_spinner_value(((FD_quant_intervals*)QUANTIZATION_MODE_FDUI(quantization,mode))->spin_nint),
                    fl_get_counter_value(((FD_quant_intervals*)QUANTIZATION_MODE_FDUI(quantization,mode))->cnt_exp),
                    min,max,clamp,info);
            break;
        case QUANT_CUSTOM:
            return g3d_quant_custom(data,
                    QUANTIZATION_BKPTS (((FD_quant_intervals*)QUANTIZATION_MODE_FDUI(quantization,mode))),
                    QUANTIZATION_NBKPTS(((FD_quant_intervals*)QUANTIZATION_MODE_FDUI(quantization,mode))),
                    info);
            break;
        case QUANT_PREDEF:
            predef_type = QUANTIZATION_PREDEF_TYPE(((FD_quant_intervals*)QUANTIZATION_MODE_FDUI(quantization,mode)));
            switch(predef_type) {
                case QUANT_ORDMAG:
                    return g3d_quant_ordmag(data,info);
                    break;
                case QUANT_CT_ABDOMEN:     /* CT window (abdomen) */
                    min =   -85; max =  165; break;
                case QUANT_CT_BRAIN:       /* CT window (brain) */
                    min =     0; max =   80; break;
                case QUANT_CT_EXTREMITIES: /* CT window (extremities) */
                    min =  -400; max = 1000; break;
                case QUANT_CT_LIVER:       /* CT window (liver) */
                    min =   -40; max =  160; break;
                case QUANT_CT_LUNG:        /* CT window (lung) */
                    min = -1350; max =  150; break;
                case QUANT_CT_PELVIS:      /* CT window (pelvis, soft tissue) */
                    min =  -140; max =  210; break;
                case QUANT_CT_SKULL_BASE:  /* CT window (skull base) */
                    min =   -60; max =  140; break;
                case QUANT_CT_SPINE_A:     /* CT window (spine A) */
                    min =   -35; max =  215; break;
                case QUANT_CT_SPINE_B:     /* CT window (spine B) */
                    min =  -300; max = 1200; break;
                case QUANT_CT_THORAX:      /* CT window (thorax, soft tissue) */
                    min =  -125; max =  225; break;
            }
            if(predef_type >= QUANT_CT_ABDOMEN && predef_type <= QUANT_CT_THORAX)
                return g3d_quant_intervals(data,G3D_VISUAL_MAX_LEVELS,1.0,min,max,QUANT_CLAMP_NONE,info);
            break;
    }

    return G3D_FAIL;
}

void update_quant_coloring(FD_quantization *quantization, FD_coloring *coloring, const xg3d_header *hdr) {
    xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(quantization);

    /* setup color under/over flow */
    COLORING_UNDERFLOW(coloring) = (qwk->info->clamp & QUANT_CLAMP_LO) == QUANT_CLAMP_LO;
    COLORING_OVERFLOW(coloring) = (qwk->info->clamp & QUANT_CLAMP_HI) == QUANT_CLAMP_HI;
    /* update if change in number of colors */
    if(COLORING_NCOL(coloring) != qwk->info->nlevels)
        update_coloring(coloring,qwk->info->nlevels);
    /* update pallete interval if in palette mode */
    if(COLORING_MODE(coloring) == COLOR_PALETTE)
        update_palette_interval(COLORING_MODE_FDUI(coloring,COLOR_PALETTE),quantization,hdr);
}

#define NP 85
void update_plot_exp(FL_OBJECT *plot, const double exp) {
    float *x = malloc(NP*sizeof(float));
    float *y = malloc(NP*sizeof(float));

    fl_clear_xyplot(plot);
    double m = round(10.0 * exp) / 10.0;
    for(int i=0;i<NP;i++) {
        x[i] = i * 1.0/NP;
        y[i] = pow(x[i],m);
    }
    fl_set_xyplot_data(plot,x,y,NP,"","","");
    free(x); free(y);
}
#undef NP

static void _refresh_bkpts_browser(FD_quant_custom *quant_custom, const bool flag_calib, const xg3d_header *hdr) {
    const unsigned int nbkpts = QUANTIZATION_NBKPTS(quant_custom);
    const double *bkpts = QUANTIZATION_BKPTS(quant_custom);

    fl_set_input(quant_custom->inp_bkpt,"");
    fl_clear_browser(quant_custom->browser);
    fl_freeze_form(quant_custom->quant_custom);
    for(unsigned int i=0;i<nbkpts;i++)
        fl_add_browser_line_f(quant_custom->browser,FORMAT,flag_calib ? CALIBVAL(hdr,bkpts[i]) : bkpts[i]);
    fl_unfreeze_form(quant_custom->quant_custom);
}

int pre_quant_hist(FL_OBJECT *obj, int event, FL_Coord mx, FL_Coord my FL_UNUSED_ARG, int key, void *xev FL_UNUSED_ARG) {
    FL_OBJECT *sli_min = GUI->quant_rng_minmax->sli_min,
              *sli_max = GUI->quant_rng_minmax->sli_max,
              *sli_cen = GUI->quant_rng_cenwid->sli_cen,
              *sli_wid = GUI->quant_rng_cenwid->sli_wid,
              *slider = NULL;
    const gsl_histogram *hist = QUANTIZATION_HISTOGRAM(GUI->quantization);
    const int flag_calib = QUANTIZATION_GET_CALIB(GUI->quantization);
    const Window win = fl_get_real_object_window(obj);
    double lo,up,smin,smax;
    FL_Coord x,y,w,h;
    char buf[128];
    int sw;

    if(event == FL_DBLCLICK && (key == FL_LEFT_MOUSE || key == FL_RIGHT_MOUSE)) {
        fl_hide_oneliner();
        fl_get_object_geometry(obj,&x,&y,&w,&h);
        if(mx < x) { mx = x; } else if(mx >= x+w) { mx = x+w-1; }
        gsl_histogram_get_range(hist,mx-x,&lo,&up);
        smin = fl_get_slider_value(sli_min);
        smax = fl_get_slider_value(sli_max);
        switch(fl_get_active_folder_number(GUI->quantization->tf_rng)) {
            case 1: /* min/max */
                if(key == FL_LEFT_MOUSE)
                    fl_set_slider_value(smin < smax ? (slider = sli_min) : (slider = sli_max),lo);
                else
                    fl_set_slider_value(smin < smax ? (slider = sli_max) : (slider = sli_min),lo);
                break;
            case 2: /* cen/wid */
                if(key == FL_LEFT_MOUSE)
                    fl_set_slider_value(slider = sli_cen,lo);
                else {
                    up = fl_get_slider_value(sli_cen);
                    fl_set_slider_value(slider = sli_wid,FL_abs(up-lo));
                }
                break;
        }
        fl_call_object_callback(slider);
        fl_redraw_object(slider);
    } else if((event == FL_PUSH || event == FL_MOTION) && key == FL_LEFT_MOUSE) {
        fl_get_object_geometry(obj,&x,&y,&w,&h);
        if(mx < x) { mx = x; } else if(mx >= x+w) { mx = x+w-1; }
        gsl_histogram_get_range(hist,mx-x,&lo,&up);
        snprintf(buf,sizeof(buf),"[%.10g,%.10g) %g",
                flag_calib ? CALIBVAL(DATA->hdr,lo) : lo,
                flag_calib ? CALIBVAL(DATA->hdr,up) : up,
                gsl_histogram_get(hist,mx-x));
        sw = fl_get_string_width(FL_FIXED_STYLE,FL_SMALL_SIZE,buf,strlen(buf));
        fl_set_oneliner_font(FL_FIXED_STYLE,FL_SMALL_SIZE);
        fl_show_oneliner(buf,x+obj->form->x-(sw-(x+w))/2,y+obj->form->y+h);
    } else if(event == FL_RELEASE) {
        fl_hide_oneliner();
        fl_set_oneliner_font(FL_NORMAL_STYLE,FL_NORMAL_SIZE);
    } else if(event == FL_ENTER) {
        fl_set_cursor(win,FL_CROSSHAIR_CURSOR);
    } else if(event == FL_LEAVE) {
        fl_reset_cursor(win);
        fl_hide_oneliner();
        fl_set_oneliner_font(FL_NORMAL_STYLE,FL_NORMAL_SIZE);
    }

    return FL_IGNORE;
}

void cb_quant_scope ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    switch(fl_get_button(obj)) {
        case SCOPE_SLICE:
            fl_set_object_label(obj,_("Slice"));
            update_datarange(ENV,false);
            set_range_inpsli(ENV,false);
            compute_quantization(GUI->quantization,VIEW_SLICE(GUI->viewport));
            update_quant_coloring(GUI->quantization,GUI->coloring,DATA->hdr);
            break;
        case SCOPE_GLOBAL:
            fl_set_object_label(obj,_("Global"));
            update_datarange(ENV,true);
            set_range_inpsli(ENV,true);
            compute_quantization(GUI->quantization,VIEW_SLICE(GUI->viewport));
            update_quant_coloring(GUI->quantization,GUI->coloring,DATA->hdr);
            break;
    }
    /* not in overlay */
    if(GUI->viewport != NULL) {
        replace_quantslice(ENV);
        replace_img(GUI->viewport,GUI->quantization);
        refresh_canvas(ENV);
    }
    /* update overview */
    if(GUI->overview != NULL) {
        FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
        replace_ov_quantslices(ov_viewports,GUI->quantization,QUANTIZATION_LAYER(GUI->quantization));
        replace_ov_images(ov_viewports,GUI->quantization,QUANTIZATION_LAYER(GUI->quantization));
        refresh_ov_canvases(GUI->overview);
    }
}

#undef ENV
static void _update_quant_guard(xg3d_env *ENV, const bool guard) {
    const enum xg3d_scope scope = QUANTIZATION_GET_SCOPE(GUI->quantization);
    const enum xg3d_quant_mode mode = QUANTIZATION_MODE(GUI->quantization);

    /* sanity check bounds */
    if(QUANTIZATION_MIN(GUI->quantization) > QUANTIZATION_MAX(GUI->quantization)) {
        error("Quantization min %c max, this shouldn't happen!",'>');
        return;
    }

    if(guard && mode != QUANT_INTERVALS) return;

    /* update global quantization */
    compute_quantization(GUI->quantization,scope == SCOPE_GLOBAL ? DATA->g3d : VIEW_SLICE(GUI->viewport));
    update_quant_coloring(GUI->quantization,GUI->coloring,DATA->hdr);

    if(GUI->viewport != NULL) {
        replace_quantslice(ENV);
        replace_img(GUI->viewport,GUI->quantization);
        refresh_canvas(ENV);
    }

    if(GUI->overview != NULL) {
        FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
        replace_ov_quantslices(ov_viewports,GUI->quantization,QUANTIZATION_LAYER(GUI->quantization));
        replace_ov_images(ov_viewports,GUI->quantization,QUANTIZATION_LAYER(GUI->quantization));
        refresh_ov_canvases(GUI->overview);
    }
}
#define ENV _default_env

void cb_rng_tf ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    set_range_inpsli(ENV,true);
}

void cb_rng_reset ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);

    qwk->min = qwk->absmin;
    qwk->max = qwk->absmax;
    set_range_inpsli(ENV,true);
    _update_quant_guard(ENV,true);
}

void cb_rng_to_custom ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const g3d_quant_info *info = QUANTIZATION_INFO(GUI->quantization);
    unsigned int *nbkpts = (unsigned int*)&QUANTIZATION_NBKPTS(GUI->quant_custom);
    double **bkpts = (double**)&QUANTIZATION_BKPTS(GUI->quant_custom);
    
    *nbkpts = info->nlevels - 1;
    *bkpts = realloc(*bkpts,(*nbkpts)*sizeof(double));
    memcpy(*bkpts,info->bkpts,(*nbkpts)*sizeof(double));
    /* switch tab without updating quantization */
    fl_set_folder_bynumber(GUI->quantization->tf_qmode,3);
    resize_form_tabfolder(GUI->quantization->tf_qmode,GUI->quant_custom->box);
    _refresh_bkpts_browser(GUI->quant_custom,QUANTIZATION_GET_CALIB(GUI->quantization),DATA->hdr);
}

void cb_sli_minmax ( FL_OBJECT * obj  , long data  ) {
    xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);
    const double smin = fl_get_slider_value(GUI->quant_rng_minmax->sli_min),
                 smax = fl_get_slider_value(GUI->quant_rng_minmax->sli_max);
    const int flag_calib = QUANTIZATION_GET_CALIB(GUI->quantization);
    double min,max;

    /* check which slider represents min and max, and update bounds */
    if(smin > smax) { /* sli_max has minimum */
        min = smax;
        max = smin;
        if(!data) { /* sli_min */
            qwk->min = qwk->min < qwk->absmin ? qwk->min : min;
            qwk->max = max;
        } else { /* sli_max */
            qwk->min = min;
            qwk->max = qwk->max > qwk->absmax ? qwk->max : max;
        }
    } else { /* sli_min has minimum */
        min = smin;
        max = smax;
        if(!data) { /* sli_min */
            qwk->min = min;
            qwk->max = qwk->max > qwk->absmax ? qwk->max : max;
        } else { /* sli_max */
            qwk->min = qwk->min < qwk->absmin ? qwk->min : min;
            qwk->max = max;
        }
    }

    fl_set_input_f(GUI->quant_rng_minmax->inp_min,FORMAT,
            flag_calib ? CALIBVAL(DATA->hdr,qwk->min) : qwk->min);
    fl_set_input_f(GUI->quant_rng_minmax->inp_max,FORMAT,
            flag_calib ? CALIBVAL(DATA->hdr,qwk->max) : qwk->max);
    
    _update_quant_guard(ENV,true);
}

void cb_inp_minmax ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *sli_min,*sli_max;
    xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);
    const int flag_calib = QUANTIZATION_GET_CALIB(GUI->quantization);
    double min = qwk->min,
           max = qwk->max,
           val;
    const char *tval;
    char *eptr;

    /* check which slider represents min and max */
    if(fl_get_slider_value(GUI->quant_rng_minmax->sli_min) >
            fl_get_slider_value(GUI->quant_rng_minmax->sli_max)) {
        sli_min = GUI->quant_rng_minmax->sli_max;
        sli_max = GUI->quant_rng_minmax->sli_min;
    } else {
        sli_min = GUI->quant_rng_minmax->sli_min;
        sli_max = GUI->quant_rng_minmax->sli_max;
    }

    /* if empty input, reset from corresponding slider */
    tval = fl_get_input(obj);
    if(tval == NULL || strlen(tval) == 0) {
        fl_call_object_callback(data ? sli_max : sli_min);
        return;
    }

    /* input value */
    val = strtod(tval,&eptr);
    if(*eptr == '%') /* percent, compute from absolute bounds */
        val = fma(val/100.0,qwk->absmax-qwk->absmin,qwk->absmin);
    else /* value, uncalibrate if flag_calib set */
        val = flag_calib ? UNCALIBVAL(DATA->hdr,val) : val;

    if(!data) { /* inp_min */
        if(val > max) max = val; /* larger than max, set as new max */
        else min = val; /* set min */
    } else { /* inp_max */
        if(val < min) min = val; /* smaller than min, set as new min */
        else max = val; /* set max */
    }

    /* store new bounds */
    qwk->min = min;
    qwk->max = max;

    /* set new input values */
    fl_set_input_f(GUI->quant_rng_minmax->inp_min,FORMAT,
            flag_calib ? CALIBVAL(DATA->hdr,qwk->min) : qwk->min);
    fl_set_input_f(GUI->quant_rng_minmax->inp_max,FORMAT,
            flag_calib ? CALIBVAL(DATA->hdr,qwk->max) : qwk->max);

    /* set new slider values */
    fl_set_slider_value(sli_min,qwk->min);
    fl_set_slider_value(sli_max,qwk->max);

    _update_quant_guard(ENV,true);
}

void cb_sli_cenwid ( FL_OBJECT * obj  , long data  ) {
    xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);
    const int flag_calib = QUANTIZATION_GET_CALIB(GUI->quantization);
    double cen,wid,min,max;

    if(!data) { /* sli_cen */
        cen = fl_get_slider_value(obj);
        wid = (qwk->max-qwk->min)/2.0;
    } else { /* sli_wid */
        wid = fl_get_slider_value(obj);
        cen = qwk->min+(qwk->max-qwk->min)/2.0;
    }

    /* compute new bounds from center and width */
    min = cen - wid;
    max = cen + wid;

    /* store new bounds */
    qwk->min = min;
    qwk->max = max;

    if(!data) /* sli_cen */
        fl_set_input_f(GUI->quant_rng_cenwid->inp_cen,FORMAT,
                flag_calib ? CALIBVAL(DATA->hdr,cen) : cen);
    else /* sli_wid */
        fl_set_input_f(GUI->quant_rng_cenwid->inp_wid,FORMAT,
                flag_calib ? wid * DATA->hdr->cal_slope : wid);

    _update_quant_guard(ENV,true);
}

void cb_inp_cenwid ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *slider = data ? GUI->quant_rng_cenwid->sli_wid : GUI->quant_rng_cenwid->sli_cen;
    xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);
    const int flag_calib = QUANTIZATION_GET_CALIB(GUI->quantization);
    double min,max,cen,wid,val;
    const char *tval;
    char *eptr;

    /* if empty input, reset from slider */
    tval = fl_get_input(obj);
    if(tval == NULL || strlen(tval) == 0) {
        fl_call_object_callback(slider);
        return;
    }

    /* input value */
    val = strtod(tval,&eptr);
    if(*eptr == '%')
        val = fma(val/100.0,qwk->absmax-qwk->absmin,data ? 0.0 : qwk->absmin);
    else
        val = flag_calib ? UNCALIBVAL(DATA->hdr,val) : val;

    if(!data) { /* inp_cen */
        cen = val; /* newly computed */
        wid = (qwk->max-qwk->min)/2.0; /* stored */
    } else { /* inp_wid */
        wid = val; /* newly computed */
        cen = qwk->min+(qwk->max-qwk->min)/2.0; /* stored */
    }

    /* invalid negative width */
    if(wid < 0.0) { 
        fl_call_object_callback(slider);
        return;
    }

    /* compute new bounds from center and width */
    min = cen - wid;
    max = cen + wid;

    /* store new bounds */
    qwk->min = min;
    qwk->max = max;

    /* update slider value */
    fl_set_slider_value(slider,data ? wid : cen);

    _update_quant_guard(ENV,true);
}

void cb_rng_update ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    _update_quant_guard(ENV,false);
}

void cb_nint_minmax ( FL_OBJECT * obj  , long data  ) {
    fl_set_spinner_value(GUI->quant_intervals->spin_nint,data ? 254 : 1);
    fl_call_object_callback(GUI->quant_intervals->spin_nint);
}

void cb_rng_exp ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    update_plot_exp(GUI->quant_intervals->plot,fl_get_counter_value(obj));
    _update_quant_guard(ENV,false);
}

void cb_rng_exp_reset ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    fl_set_counter_value(GUI->quant_intervals->cnt_exp,1.0);
    fl_call_object_callback(GUI->quant_intervals->cnt_exp);
}

static void _deactivate_rng(FD_quantization *quantization, FD_quant_rng_minmax *quant_rng_minmax, FD_quant_rng_cenwid *quant_rng_cenwid) {
    deactivate_obj(quant_rng_minmax->inp_min);
    deactivate_obj(quant_rng_minmax->inp_max);
    deactivate_obj(quant_rng_minmax->sli_min);
    deactivate_obj(quant_rng_minmax->sli_max);
    deactivate_obj(quant_rng_cenwid->inp_cen);
    deactivate_obj(quant_rng_cenwid->inp_wid);
    deactivate_obj(quant_rng_cenwid->sli_cen);
    deactivate_obj(quant_rng_cenwid->sli_wid);
    deactivate_obj(quantization->tf_rng);
    deactivate_obj(quantization->btn_rng_reset);
    deactivate_obj(quantization->btn_rng_fromsel);
}

static void _activate_rng(FD_quantization *quantization, FD_quant_rng_minmax *quant_rng_minmax, FD_quant_rng_cenwid *quant_rng_cenwid) {
    activate_obj(quant_rng_minmax->inp_min,FL_MCOL);
    activate_obj(quant_rng_minmax->inp_max,FL_MCOL);
    activate_obj(quant_rng_minmax->sli_min,FL_YELLOW);
    activate_obj(quant_rng_minmax->sli_max,FL_YELLOW);
    activate_obj(quant_rng_cenwid->inp_cen,FL_MCOL);
    activate_obj(quant_rng_cenwid->inp_wid,FL_MCOL);
    activate_obj(quant_rng_cenwid->sli_cen,FL_YELLOW);
    activate_obj(quant_rng_cenwid->sli_wid,FL_YELLOW);
    activate_obj(quantization->tf_rng,FL_COL1);
    activate_obj(quantization->btn_rng_reset,FL_COL1);
    activate_obj(quantization->btn_rng_fromsel,FL_COL1);
}

void cb_quant_mode ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    switch(fl_get_active_folder_number(obj)) {
        case QUANT_INTERVALS:
            _activate_rng(GUI->quantization,GUI->quant_rng_minmax,GUI->quant_rng_cenwid);
            resize_form_tabfolder(obj,GUI->quant_intervals->box);
            fl_call_object_callback(GUI->quant_intervals->spin_nint);
            break;
        case QUANT_PREDEF:
            _deactivate_rng(GUI->quantization,GUI->quant_rng_minmax,GUI->quant_rng_cenwid);
            resize_form_tabfolder(obj,GUI->quant_predef->box);
            if(fl_get_browser(GUI->quant_predef->browser) == 0)
                fl_select_browser_line(GUI->quant_predef->browser,1);
            fl_call_object_callback(GUI->quant_predef->browser);
            break;
        case QUANT_CUSTOM:
            _deactivate_rng(GUI->quantization,GUI->quant_rng_minmax,GUI->quant_rng_cenwid);
            resize_form_tabfolder(obj,GUI->quant_custom->box);
            _update_quant_guard(ENV,false);
            _refresh_bkpts_browser(GUI->quant_custom,QUANTIZATION_GET_CALIB(GUI->quantization),DATA->hdr);
            break;
    }
}

void cb_rng_fromsel ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    double min,max;

    if(SELECTION_NVOX(GUI->selection) == 0) {
        fl_show_alert2(0,_("Empty selection!\fPlease make a selection first."));
        return;
    }

    g3d_u8_selchk_minmax(SELECTION_G3D(GUI->selection),DATA->g3d,&min,&max);
    QUANTIZATION_MIN(GUI->quantization) = min;
    QUANTIZATION_MAX(GUI->quantization) = max;
    set_range_inpsli(ENV,true);
    _update_quant_guard(ENV,false);
}

void cb_quant_predef ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const enum xg3d_quant_predef_type type = fl_get_browser(obj) - 1;

    QUANTIZATION_PREDEF_TYPE(GUI->quant_predef) = type;
    _update_quant_guard(ENV,false);
}

void cb_quant_units ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);
    const enum xg3d_color_mode color_mode = COLORING_MODE(GUI->coloring);
    const int flag_calib = QUANTIZATION_GET_CALIB(GUI->quantization);
    const xg3d_header *hdr = DATA->hdr;

    fl_set_object_label(obj,fl_get_button(obj) ? "CAL" : "DAT");
    fl_set_object_label_f(GUI->quantization->txt_min,FORMAT,
            flag_calib ? CALIBVAL(hdr,qwk->absmin) : qwk->absmin);
    fl_set_object_label_f(GUI->quantization->txt_max,FORMAT,
            flag_calib ? CALIBVAL(hdr,qwk->absmax) : qwk->absmax);
    fl_set_object_label_f(GUI->quantization->txt_tot,FORMAT,
            flag_calib ? CALIBSUM(hdr,qwk->total,DATA->g3d->nvox) : qwk->total);
    fl_set_object_label_f(GUI->quantization->txt_avg,FORMAT,
           (flag_calib ? CALIBSUM(hdr,qwk->total,DATA->g3d->nvox) : qwk->total) / (double)DATA->g3d->nvox);

    _refresh_bkpts_browser(GUI->quant_custom,flag_calib,hdr);

    set_range_inpsli(ENV,true);
    if(color_mode == COLOR_PALETTE)
        update_palette_interval(GUI->col_table,GUI->quantization,hdr);
}

/* ====================
 * BREAKPOINT FUNCTIONS
 * ==================== */

void cb_bkpts_browser ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const int flag_calib = QUANTIZATION_GET_CALIB(GUI->quantization);
    const double *bkpts = QUANTIZATION_BKPTS(GUI->quant_custom);
    const unsigned int ix = fl_get_browser(obj);

    if(ix == 0)
        fl_set_input(GUI->quant_custom->inp_bkpt,"");
    else
        fl_set_input_f(GUI->quant_custom->inp_bkpt,FORMAT,
                flag_calib ? CALIBVAL(DATA->hdr,bkpts[ix-1]) : bkpts[ix-1]);
}

void cb_bkpts_manage ( FL_OBJECT * obj  , long data  ) {
    const unsigned int ix = fl_get_browser(GUI->quant_custom->browser);
    const int flag_calib = QUANTIZATION_GET_CALIB(GUI->quantization);
    const xg3d_header *hdr = DATA->hdr;
    unsigned int *nbkpts = (unsigned int*)&QUANTIZATION_NBKPTS(GUI->quant_custom);
    double **bkpts = (double**)&QUANTIZATION_BKPTS(GUI->quant_custom);
    const char *bkptxt;
    double bkpt;

    switch(data) {
        case 0: /* Add */
            if(*nbkpts >= G3D_VISUAL_MAX_LEVELS - 1) {
                fl_show_alert2(0,_("Too many breakpoints!\fMaximum number of levels reached."));
                return;
            }
            bkptxt = fl_get_input(GUI->quant_custom->inp_bkpt);
            if(bkptxt == NULL || strlen(bkptxt) == 0) {
                fl_show_alert2(0,_("Empty input!\fPlease enter a breakpoint."));
                return;
            }
            bkpt = strtod(bkptxt,NULL);
            if(flag_calib) bkpt = UNCALIBVAL(hdr,bkpt);
            if((bkpt == -HUGE_VAL || bkpt == HUGE_VAL) && errno == ERANGE) {
                fl_show_alert2(0,_("Invalid value!\fPlease enter a valid breakpoint."));
                return;
            }
            if(bsearch(&bkpt,*bkpts,*nbkpts,sizeof(double),cmp_double) != NULL) {
                fl_show_alert2(0,_("Duplicate found!\fPlease enter a unique breakpoint."));
                fl_set_input(GUI->quant_custom->inp_bkpt,"");
                return;
            }
            (*nbkpts)++;
            *bkpts = realloc(*bkpts,*nbkpts*sizeof(double));
            (*bkpts)[(*nbkpts)-1] = bkpt;
            qsort(*bkpts,*nbkpts,sizeof(double),cmp_double);
            _refresh_bkpts_browser(GUI->quant_custom,flag_calib,hdr);
            break;
        case 1: /* Delete */
            if(ix < 1) return;
            fl_delete_browser_line(GUI->quant_custom->browser,ix);
            memmove((*bkpts)+ix-1,(*bkpts)+ix,((*nbkpts)-ix)*sizeof(double));
            *bkpts = realloc(*bkpts,((*nbkpts)-1)*sizeof(double));
            (*nbkpts)--;
            fl_set_input(GUI->quant_custom->inp_bkpt,"");
            break;
        case 2: /* Modify */
            if(ix < 1) return;
            bkptxt = fl_get_input(GUI->quant_custom->inp_bkpt);
            if(bkptxt == NULL || strlen(bkptxt) == 0) {
                fl_show_alert2(0,_("Empty input!\fPlease enter a breakpoint."));
                return;
            }
            bkpt = strtod(bkptxt,NULL);
            if(flag_calib) bkpt = UNCALIBVAL(hdr,bkpt);
            if((bkpt == -HUGE_VAL || bkpt == HUGE_VAL) && errno == ERANGE) {
                fl_show_alert2(0,_("Invalid value!\fPlease enter a valid breakpoint."));
                return;
            }
            if(bkpt == (*bkpts)[ix-1]) return;
            if(bsearch(&bkpt,*bkpts,*nbkpts,sizeof(double),cmp_double) != NULL) {
                fl_show_alert2(0,_("Duplicate found!\fPlease enter a unique breakpoint."));
                return;
            }
            (*bkpts)[ix-1] = bkpt;
            qsort(*bkpts,*nbkpts,sizeof(double),cmp_double);
            _refresh_bkpts_browser(GUI->quant_custom,flag_calib,hdr);
            break;
        case 3: /* Clear */
            if(*bkpts == NULL || *nbkpts == 0) return;
            fl_set_input(GUI->quant_custom->inp_bkpt,"");
            fl_clear_browser(GUI->quant_custom->browser);
            free(*bkpts); *bkpts = NULL; *nbkpts = 0;
            break;
    }
    _update_quant_guard(ENV,false);
}

void cb_bkpts_split ( FL_OBJECT * obj  , long data  ) {
    const unsigned int ix = fl_get_browser(GUI->quant_custom->browser);
    const double absmin = QUANTIZATION_ABSMIN(GUI->quantization);
    const double absmax = QUANTIZATION_ABSMAX(GUI->quantization);
    unsigned int *nbkpts = (unsigned int*)&QUANTIZATION_NBKPTS(GUI->quant_custom);
    double **bkpts = (double**)&QUANTIZATION_BKPTS(GUI->quant_custom);
    double bkpt;

    if(ix == 0) {
        fl_show_alert2(0,_("No breakpoint selected!\fPlease select a breakpoint for splitting."));
        return;
    }
    if(*nbkpts >= G3D_VISUAL_MAX_LEVELS - 1) {
        fl_show_alert2(0,_("Too many breakpoints!\fMaximum number of levels reached."));
        return;
    }

    *bkpts = realloc(*bkpts,((*nbkpts)+1)*sizeof(double));
    switch(data) {
        case 0: /* Left */
            if(ix == 1)
                bkpt = fma(0.5,(*bkpts)[ix-1]-absmin,absmin);
            else
                bkpt = fma(0.5,(*bkpts)[ix-1]-(*bkpts)[ix-2],(*bkpts)[ix-2]);
            memmove((*bkpts)+ix,(*bkpts)+ix-1,((*nbkpts)+1-ix)*sizeof(double));
            (*bkpts)[ix-1] = bkpt;
            fl_insert_browser_line_f(GUI->quant_custom->browser,ix,FORMAT,bkpt);
            break;
        case 1: /* Right */
            if(ix == *nbkpts)
                bkpt = fma(0.5,absmax-(*bkpts)[ix-1],(*bkpts)[ix-1]);
            else
                bkpt = fma(0.5,(*bkpts)[ix]-(*bkpts)[ix-1],(*bkpts)[ix-1]);
            memmove((*bkpts)+ix,(*bkpts)+ix-1,((*nbkpts)+1-ix)*sizeof(double));
            (*bkpts)[ix] = bkpt;
            fl_insert_browser_line_f(GUI->quant_custom->browser,ix+1,FORMAT,bkpt);
            break;
    }
    (*nbkpts)++;
    _update_quant_guard(ENV,false);
}

void cb_bkpts_merge ( FL_OBJECT * obj  , long data  ) {
    unsigned int *nbkpts = (unsigned int*)&QUANTIZATION_NBKPTS(GUI->quant_custom);
    double **bkpts = (double**)&QUANTIZATION_BKPTS(GUI->quant_custom);
    unsigned int ix = fl_get_browser(GUI->quant_custom->browser);

    if(ix == 0) {
        fl_show_alert2(0,_("No breakpoint selected!\fPlease select a breakpoint for merging."));
        return;
    }

    switch(data) {
        case 0: /* Left */
            if(ix > 1) ix--;
            else fl_set_input(GUI->quant_custom->inp_bkpt,"");
            break;
        case 1: /* Right */
            if(ix < *nbkpts) ix++;
            else fl_set_input(GUI->quant_custom->inp_bkpt,"");
            break;
    }
    fl_delete_browser_line(GUI->quant_custom->browser,ix);
    memmove((*bkpts)+ix-1,(*bkpts)+ix,((*nbkpts)-ix)*sizeof(double));
    *bkpts = realloc(*bkpts,((*nbkpts)-1)*sizeof(double));
    (*nbkpts)--;
    _update_quant_guard(ENV,false);
}

void cb_bkpts_input ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const int flag_calib = QUANTIZATION_GET_CALIB(GUI->quantization);
    const double absmin = QUANTIZATION_ABSMIN(GUI->quantization);
    const double absmax = QUANTIZATION_ABSMAX(GUI->quantization);
    const char *tval = fl_get_input(obj);
    char *eptr;
    double val;

    if(tval == NULL || strlen(tval) == 0) return;

    val = strtod(tval,&eptr);
    if(*eptr == '%') {
        val = fma(val/100.0,absmax-absmin,absmin);
        val = flag_calib ? CALIBVAL(DATA->hdr,val) : val;
        fl_set_input_f(obj,FORMAT,val);
    }
}

void cb_bkpts_io ( FL_OBJECT * obj  , long data  ) {
    unsigned int *nbkpts = (unsigned int*)&QUANTIZATION_NBKPTS(GUI->quant_custom);
    double **bkpts = (double**)&QUANTIZATION_BKPTS(GUI->quant_custom);

    switch(data) {
        case 0: { /* Load */
            const char *file = fl_show_fselector(_("Load breakpoints from..."),"","*.txt","");
            if(file == NULL) return;
            FILE *fp = fopen(file,"r");
            if(fp == NULL) {
                fl_show_alert2(0,_("Cannot read file!\fError opening %s for reading."),file);
                return;
            }
            unsigned int ntmp;
            if(fscanf(fp,"%u",&ntmp) != 1) {
                fl_show_alert2(0,_("Failed parsing file!\f%s"),file);
                return;
            }
            double *tmp = malloc(ntmp*sizeof(double));
            unsigned int k = 0;
            for(unsigned int i=0;i<ntmp;i++) {
                int ret = fscanf(fp,"%lg\n",tmp+i);
                if(ret == 1) k++;
                else {
                    fl_show_alert2(0,_("Failed parsing file!\f%s"),file);
                    return;
                }
            }
            if(k != ntmp || feof(fp) == 0) {
                fl_show_alert2(0,_("Wrong number of breakpoints!\fPlease set file header to match contents."));
                fclose(fp);
                free(tmp);
                return;
            }
            fclose(fp);
            free(*bkpts);
            *bkpts = tmp;
            *nbkpts = ntmp;
            qsort(*bkpts,*nbkpts,sizeof(double),cmp_double);
            _refresh_bkpts_browser(GUI->quant_custom,QUANTIZATION_GET_CALIB(GUI->quantization),DATA->hdr);
            fl_set_input(GUI->quant_custom->inp_bkpt,"");
            _update_quant_guard(ENV,false);
            } break;
        case 1: { /* Save */
            if(*nbkpts == 0) {
                fl_show_alert2(0,_("No breakpoints!\fPlease add at least one breakpoint before saving."));
                return;
            }
            const char *file = fl_show_fselector(_("Save breakpoints to..."),"","*.txt","");
            if(file == NULL) return;
            FILE *fp = fopen(file,"w");
            if(fp == NULL) {
                fl_show_alert2(0,_("Cannot write file!\fError opening %s for writing."),file);
                return;
            }
            fprintf(fp,"%u\n",*nbkpts);
            for(unsigned int i=0;i<*nbkpts;i++)
                fprintf(fp,"%.16g\n",(*bkpts)[i]);
            fclose(fp);
            } break;
    }
}

void cb_bkpts_otsu ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const int flag_calib = QUANTIZATION_GET_CALIB(GUI->quantization);
    const gsl_histogram *hist = SELECTION_NVOX(GUI->selection) == 0
        ? QUANTIZATION_HISTOGRAM(GUI->quantization) /* use quantization histogram */
        : sel_get_histogram(SELECTION_G3D(GUI->selection),DATA->g3d); /* use selection histogram */
    double bkpt;

    if(hist == NULL) return;

    bkpt = hist->range[otsu_threshold(hist)];
    fl_set_input_f(GUI->quant_custom->inp_bkpt,FORMAT,
            flag_calib ? CALIBVAL(DATA->hdr,bkpt) : bkpt);
}

/* ==================
 * SETTINGS FUNCTIONS
 * ================== */

void setup_quantization_settings(FD_quantization *quantization, const unsigned long valuemask, const xg3d_quantization_settings *settings) {
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FL_FORM *form;

    if(xg3d_settings_check(valuemask,QUANTIZATION_SET_SCOPE))
        fl_set_button(quantization->btn_scope,settings->scope);
    if(xg3d_settings_check(valuemask,QUANTIZATION_CAL_UNITS))
        fl_set_button(quantization->btn_units,settings->cal_units);
    if(xg3d_settings_check(valuemask,QUANTIZATION_HIST_LOG))
        fl_set_button(quantization->btn_log,settings->hist_log);
    if(xg3d_settings_check(valuemask,QUANTIZATION_HIST_SKIP))
        fl_set_button(quantization->btn_skip,settings->hist_skip);

    if(xg3d_settings_check(valuemask,QUANTIZATION_RNG_TYPE)) {
        form = fl_get_tabfolder_folder_bynumber(quantization->tf_rng,settings->rng_type);
        fl_set_folder(quantization->tf_rng,form);
    }
    if(xg3d_settings_check(valuemask,QUANTIZATION_RNG_MIN))
        QUANTIZATION_MIN(quantization) = settings->min;
    if(xg3d_settings_check(valuemask,QUANTIZATION_RNG_MAX))
        QUANTIZATION_MAX(quantization) = settings->max;

    if(xg3d_settings_check(valuemask,QUANTIZATION_QUANT_MODE)) {
        form = fl_get_tabfolder_folder_bynumber(quantization->tf_qmode,settings->quant_mode);
        fl_set_folder(quantization->tf_qmode,form);
        switch(settings->quant_mode) {
            case QUANT_INTERVALS:
                quant_intervals = (FD_quant_intervals*)form->fdui;
                if(xg3d_settings_check(valuemask,QUANTIZATION_NINT))
                    fl_set_spinner_value(quant_intervals->spin_nint,settings->nint);
                if(xg3d_settings_check(valuemask,QUANTIZATION_EXP))
                    fl_set_counter_value(quant_intervals->cnt_exp,settings->exp);
                if(xg3d_settings_check(valuemask,QUANTIZATION_CLAMP))
                    fl_set_button(quant_intervals->btn_clamp,settings->clamp);
                break;
            case QUANT_PREDEF:
                quant_predef = (FD_quant_predef*)form->fdui;
                if(xg3d_settings_check(valuemask,QUANTIZATION_SET_PREDEF)) {
                    QUANTIZATION_PREDEF_TYPE(quant_predef) = settings->predef_type;
                    fl_select_browser_line(quant_predef->browser,settings->predef_type+1);
                }
                break;
            case QUANT_CUSTOM:
                quant_custom = (FD_quant_custom*)form->fdui;
                if(xg3d_settings_check(valuemask,QUANTIZATION_BREAKPOINTS)) {
                    QUANTIZATION_NBKPTS(quant_custom) = settings->nbkpts;
                    if(settings->nbkpts >= 1) {
                        QUANTIZATION_BKPTS(quant_custom) = realloc(QUANTIZATION_BKPTS(quant_custom),settings->nbkpts*sizeof(double));
                        memcpy(QUANTIZATION_BKPTS(quant_custom),settings->bkpts,settings->nbkpts*sizeof(double));
                    } else {
                        free(QUANTIZATION_BKPTS(quant_custom)); QUANTIZATION_BKPTS(quant_custom) = NULL;
                    }
                }
                break;
        }
    }
}

xg3d_quantization_settings* store_quantization_settings(const FD_quantization *quantization, const unsigned long valuemask) {
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FL_FORM *form;

    xg3d_quantization_settings *settings = calloc(1,sizeof(xg3d_quantization_settings));
    
    if(xg3d_settings_check(valuemask,QUANTIZATION_SET_SCOPE))
        settings->scope = fl_get_button(quantization->btn_scope);
    if(xg3d_settings_check(valuemask,QUANTIZATION_CAL_UNITS))
        settings->cal_units = fl_get_button(quantization->btn_units);
    if(xg3d_settings_check(valuemask,QUANTIZATION_HIST_LOG))
        settings->hist_log = fl_get_button(quantization->btn_log);
    if(xg3d_settings_check(valuemask,QUANTIZATION_HIST_SKIP))
        settings->hist_skip = fl_get_button(quantization->btn_skip);

    if(xg3d_settings_check(valuemask,QUANTIZATION_RNG_TYPE))
        settings->rng_type = fl_get_active_folder_number(quantization->tf_rng);
    if(xg3d_settings_check(valuemask,QUANTIZATION_RNG_MIN))
        settings->min = QUANTIZATION_MIN(quantization);
    if(xg3d_settings_check(valuemask,QUANTIZATION_RNG_MAX))
        settings->max = QUANTIZATION_MAX(quantization);

    if(xg3d_settings_check(valuemask,QUANTIZATION_QUANT_MODE)) {
        settings->quant_mode = QUANTIZATION_MODE(quantization);
        form = fl_get_tabfolder_folder_bynumber(quantization->tf_qmode,settings->quant_mode);
        switch(settings->quant_mode) {
            case QUANT_INTERVALS:
                quant_intervals = (FD_quant_intervals*)form->fdui;
                if(xg3d_settings_check(valuemask,QUANTIZATION_NINT))
                    settings->nint = fl_get_spinner_value(quant_intervals->spin_nint);
                if(xg3d_settings_check(valuemask,QUANTIZATION_EXP))
                    settings->exp = fl_get_counter_value(quant_intervals->cnt_exp);
                if(xg3d_settings_check(valuemask,QUANTIZATION_CLAMP))
                    settings->clamp = fl_get_button(quant_intervals->btn_clamp);
                break;
            case QUANT_PREDEF:
                quant_predef = (FD_quant_predef*)form->fdui;
                if(xg3d_settings_check(valuemask,QUANTIZATION_SET_PREDEF))
                    settings->predef_type = QUANTIZATION_PREDEF_TYPE(quant_predef);
                break;
            case QUANT_CUSTOM:
                quant_custom = (FD_quant_custom*)form->fdui;
                if(xg3d_settings_check(valuemask,QUANTIZATION_BREAKPOINTS) && QUANTIZATION_NBKPTS(quant_custom) >= 1) {
                    settings->nbkpts = QUANTIZATION_NBKPTS(quant_custom);
                    settings->bkpts = malloc(QUANTIZATION_NBKPTS(quant_custom)*sizeof(double));
                    memcpy(settings->bkpts,QUANTIZATION_BKPTS(quant_custom),QUANTIZATION_NBKPTS(quant_custom)*sizeof(double));
                }
                break;
        }
    }

    return settings;
}
