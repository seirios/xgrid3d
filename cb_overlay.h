#ifndef XG3D_overlay_cb_h
#define XG3D_overlay_cb_h

#include "xgrid3d.h"
#include "fd_overlay.h"

inline static int OVERLAY_POS(FD_overlay *overlay, int idx) {
    return THREEWAY_IF(idx==0,fl_get_slider_value(overlay->sli_x),
                       idx==1,fl_get_slider_value(overlay->sli_y),
                              fl_get_slider_value(overlay->sli_z));
}

#define OVERLAY_ENV(fd_overlay) (((FD_overlay*)(fd_overlay))->overlay->u_vdata)

void setup_overlay(xg3d_env*,xg3d_data*);
void adapt_overlay_img(FD_viewport*,FD_overlay*,const xg3d_lut*);
void deactivate_overlay(FD_overlay*);

#endif
