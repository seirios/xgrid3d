#ifndef XG3D_selection_cb_h
#define XG3D_selection_cb_h

#include <forms.h>
#include "xgrid3d.h"
#include "fd_selection.h"
#include "selection.h"

#define SEL_GXMODE GXinvert
#define SEL_COLOR FL_WHITE

/* Selection settings value mask bits */
#define SELECTION_SCOPE         (1L<<0)
#define SELECTION_VISIBLE       (1L<<1)
#define SELECTION_LABELS        (1L<<2)
#define SELECTION_SELPATT       (1L<<3)
#define SELECTION_APPEARANCE    (SELECTION_VISIBLE | \
                                 SELECTION_LABELS | \
                                 SELECTION_SELPATT)

#define SELECTION_SET_MODE      (1L<<4)
#define SELECTION_CLAMP         (1L<<5)
#define SELECTION_REPLACE       (1L<<6)
#define SELECTION_BRUSH_SHAPE   (1L<<7)
#define SELECTION_BRUSH_SIZE    (1L<<8)
#define SELECTION_SHAPE         (1L<<9)
#define SELECTION_NRAND         (1L<<10)
#define SELECTION_ISO_FILLED    (1L<<11)
#define SELECTION_ISO_GREATER   (1L<<12)
#define SELECTION_SELMODE       (SELECTION_SET_MODE | \
                                 SELECTION_CLAMP | \
                                 SELECTION_REPLACE | \
                                 SELECTION_BRUSH_SHAPE | \
                                 SELECTION_BRUSH_SIZE | \
                                 SELECTION_SHAPE | \
                                 SELECTION_NRAND | \
                                 SELECTION_ISO_FILLED | \
                                 SELECTION_ISO_GREATER)

#define SELECTION_REP           (1L<<13)
#define SELECTION_STEP          (1L<<14)
#define SELECTION_PROPAGATE     (1L<<15)
#define SELECTION_OPERATIONS    (SELECTION_REP | \
                                 SELECTION_STEP | \
                                 SELECTION_PROPAGATE)

#define SELECTION_MARKER_LIST   (1L<<16)
#define SELECTION_MARKERS       (SELECTION_MARKER_LIST)



typedef void ( _selpattf ) ( const FL_Coord wx  , const FL_Coord wy  , const unsigned int zoom_x  , const unsigned int zoom_y  , const FL_COLOR color  ) ;

enum xg3d_selection_pattern {
    SEL_PATT_CROSS,
    SEL_PATT_DOT,
    SEL_PATT_SQUARE
};

enum xg3d_selection_mode {
    SEL_MODE_NONE,
    SEL_MODE_MARKER,
    SEL_MODE_BRUSH,
    SEL_MODE_SHAPE,
    SEL_MODE_LEVEL,
    SEL_MODE_COMPONENT,
    SEL_MODE_FILL,
    SEL_MODE_RANDOM,
    SEL_MODE_CONTOUR,
    SEL_MODE_ISOCOMPONENT
};

typedef struct {
    enum xg3d_scope scope;                 /* selection scope */
    int visible;                           /* selection visible flag */
    int labels;                            /* labels visible flag */
    enum xg3d_selection_pattern patt;      /* selection pattern */
    enum xg3d_selection_mode mode;         /* selection mode */
    int clamp;                             /* histogram clamp flag */
    int replace;                           /* replace selection flag */
    enum xg3d_selection_brush brush_shape; /* brush shape */
    unsigned int brush_size;               /* brush size */
    enum xg3d_selection_shape shape;       /* shape */
    unsigned int nrand;                    /* number of random points */
    int iso_filled;                        /* fill isocontour flag */
    int iso_greater;                       /* isocontour > or < flag */
    unsigned int rep;                      /* selection move repetition */
    unsigned int step;                     /* selection move step */
    int propagate;                         /* propagate selection flag */
    unsigned int nmk;                      /* number of markers in list */
    marker *marker_list;                   /* marker list */
} xg3d_selection_settings;

void setup_selection_settings(FD_selection*,const unsigned long,const xg3d_selection_settings*);
xg3d_selection_settings* store_selection_settings(const FD_selection*,const unsigned long);

_selpattf sel_patt_cross;
_selpattf sel_patt_dot;
_selpattf sel_patt_square;

inline static _selpattf* SELECTION_PATTERN_FUNC(FD_selection *selection) {
    return THREEWAY_IF(
            fl_get_button(selection->btn_cross),sel_patt_cross,
            fl_get_button(selection->btn_dot),  sel_patt_dot,
                                                sel_patt_square);
}

inline static enum xg3d_selection_pattern SELECTION_PATTERN(FD_selection *selection) {
    return THREEWAY_IF(
            fl_get_button(selection->btn_cross),SEL_PATT_CROSS,
            fl_get_button(selection->btn_dot),  SEL_PATT_DOT,
                                                SEL_PATT_SQUARE);
}

typedef struct {
    grid3d *g3d;
    index_t nvox;
    double value;
} xg3d_selection_workspace;

void selection_workspace_clear(xg3d_selection_workspace*,const bool);
xg3d_selection_workspace* selection_workspace_dup(const xg3d_selection_workspace*);

#define SELECTION_MODE(fd_selection) (((FD_selection*)(fd_selection))->ldata)
#define SELECTION_NMK(fd_selection) (((FD_selection*)(fd_selection))->br_markers->u_ldata)
#define SELECTION_MKLIST(fd_selection) (((FD_selection*)(fd_selection))->br_markers->u_vdata)

#define SELECTION_WORKSPACE(fd_selection) ((xg3d_selection_workspace*)(((FD_selection*)(fd_selection))->vdata))
#define SELECTION_G3D(fd_selection) (SELECTION_WORKSPACE(fd_selection)->g3d)
#define SELECTION_NVOX(fd_selection) (SELECTION_WORKSPACE(fd_selection)->nvox)
#define SELECTION_VALUE(fd_selection) (SELECTION_WORKSPACE(fd_selection)->value)

#define SELECTION_IS_VISIBLE(fd_selection) (fl_get_button(((FD_selection*)(fd_selection))->btn_visible))
#define SELECTION_SET_VISIBLE(fd_selection,x) (fl_set_button(((FD_selection*)(fd_selection))->btn_visible,x))
#define SELECTION_GET_SCOPE(fd_selection) (fl_get_button(((FD_selection*)(fd_selection))->btn_scope))

void replace_selslice(FD_viewport*,FD_selection*);
void sel_update_info(FD_viewport*,FD_selection*);
void sel_sanity_check(FD_viewport*,FD_selection*);
void sel_set_cursor(FD_viewport*,FD_selection*);

void sel_clear_markers(FD_selection*);
void cb_marker_dblclick(FL_OBJECT*,long);
void sel_add_marker(xg3d_env*,const coord_t,const coord_t,const coord_t,const char*);
void sel_del_marker(xg3d_env*,const coord_t,const coord_t,const coord_t);
void sel_blink_marker(FD_viewport*,FD_selection*,const coord_t,const coord_t,const bool);

FL_HANDLE pre_sel_brush;
void sel_brush(xg3d_env*,const enum xg3d_selection_brush,const unsigned int,const coord_t,const coord_t,const coord_t,const enum xg3d_sel_op_mode);

void sel_shape(xg3d_env*,const enum xg3d_sel_op_mode);
void sel_random(xg3d_env*,const enum xg3d_sel_op_mode);
void sel_level(xg3d_env*,const coord_t,const coord_t,const enum xg3d_scope,const enum xg3d_sel_op_mode);
void sel_level_comp(xg3d_env*,const coord_t,const coord_t,const enum xg3d_scope,const enum xg3d_sel_op_mode);
void sel_fill(xg3d_env*,const coord_t,const coord_t,const enum xg3d_scope,const enum xg3d_sel_op_mode);
void sel_contour(xg3d_env*,const coord_t,const coord_t,const enum xg3d_scope,const enum xg3d_sel_op_mode,const bool);

#endif
