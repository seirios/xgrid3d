#include <stdlib.h>
#include "sort_uint32_t.h"

/* compare uint32_t integers */
int cmp_uint32_t(const void *a, const void *b) {
	if(*(uint32_t*)a > *(uint32_t*)b)
		return 1;
	else if(*(uint32_t*)a < *(uint32_t*)b)
		return -1;
	else
		return 0;
}

/* this function generates all (j,i,k) triples within a cubic grid
 * of 2*max+1 voxels per dimension, then computes all distances that
 * span the inscribed sphere (up to the radius of the cube) and counts
 * the number of voxels at that distance */

#define SQR(x) ((x)*(x))
#define CUB(x) (SQR(x)*(x))
int gengrid(int max, size_t *o_ndist, uint32_t **o_dists, uint32_t **o_occup) {
	/* grid has a center voxel and max voxels at each side, on every dimension
	 * distance from center voxel to voxel on side of the cube is max*max */
	size_t siz = CUB(2*max+1);
    size_t imax = siz;
	uint32_t *x = malloc((siz+1)*sizeof(uint32_t));

	/* compute all squared distances within grid */
	size_t ix = 0;
	for(int i=-max;i<=max;i++)
		for(int j=-max;j<=max;j++)
			for(int k=-max;k<=max;k++)
				x[ix++] = SQR(i)+SQR(j)+SQR(k);

	/* if we counted wrong, return */
	if(ix != siz) { free(x); return 1; }

	/* sort distances, max distance must be less than UINT_MAX */
	x[ix] = UINT32_MAX; /* sentinel for sorting algorithm */
	sedgesort_uint32_t(x,ix);

	size_t ndist = 0;
	/* iterate over squared distances for all voxels */
	for(size_t ix=0;ix<siz;ix++) {
		if(x[ix] != x[ix+1]) /* count unique values */
			ndist++;
		if(x[ix+1] > (uint32_t)max*max) { /* break if we're out of the inscribed sphere */
			imax = ix+1; break;
		}
	}

	uint32_t *occup = malloc(ndist*sizeof(uint32_t));
	uint32_t *dists = malloc(ndist*sizeof(uint32_t));

	uint32_t o = 0;
    size_t id = 0;
	/* iterate over distances within the inscribed sphere */
	for(size_t ix=1;ix<=imax;ix++) {
		o++; /* increment occupancy */
		if(x[ix-1] != x[ix]) { /* on value change */
			occup[id] = o; /* store occupancy */
			dists[id++] = x[ix-1]; /* store distance */
			o = 0; /* reset occupancy */
		}
	}
	free(x);

	/* if we counted wrong, return */
	if(id != ndist) { free(occup); free(dists); return 1; }

	if(o_ndist != NULL) *o_ndist = ndist;
	if(o_dists != NULL) *o_dists = dists;
	if(o_occup != NULL) *o_occup = occup;

	return 0;
}
#undef SQR
#undef CUB
