#ifndef XG3D_datasets_cb_h_
#define XG3D_datasets_cb_h_

#include "fd_datasets.h"

void setup_attrs(xg3d_env*,const int);
void refresh_datasets_browser(FL_OBJECT*,const xg3d_datasets*);
void refresh_datasets_info(FD_datasets*,const xg3d_datasets*,const bool);

#endif
