#ifndef XG3D_bresenham_h
#define XG3D_bresenham_h

#include <stdlib.h>
#include "grid3d.h"

typedef void (_plotdrawf)(const coord_t,const coord_t,void*);

void plotCircle(int,int,int,void*,_plotdrawf*);
void plotRotatedEllipse(int,int,int,int,float,void*,_plotdrawf*);

#endif
