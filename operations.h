#ifndef XG3D_operations_h_
#define XG3D_operations_h_

#include "grid3d_isom.h"
#include "util_markers.h"

enum align_mk_type {
    ALIGN_MK_LINE,
    ALIGN_MK_PLANE
};

g3d_aff_m* op_align_to_markers(const grid3d*,const marker*,const unsigned int,const enum g3d_axis,const enum align_mk_type);

#endif
