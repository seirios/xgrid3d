#include "overlay_reg.h"
#include "niftyreg_c.h"

int register_aladin(nifti_image *ref, nifti_image *flo, nifti_image *msk_ref, nifti_image *msk_flo, mat44 *aff, enum reg_mode mode, int sym) {
	/* Default reg_aladin parameters */
    int maxIter=5;
    int nLevels=3;
    int levelsToPerform=3;
    float blockPercentage=50.0f;
    float inlierLts=50.0f;
    float floatingSigma=0.0;
    float referenceSigma=0.0;

    regAladin *REG = sym ? reg_aladin_sym_new() : reg_aladin_new();
	reg_aladin_SetMaxIterations(REG,maxIter);
	reg_aladin_SetNumberOfLevels(REG,nLevels);
	reg_aladin_SetLevelsToPerform(REG,levelsToPerform);
	reg_aladin_SetReferenceSigma(REG,referenceSigma);
	reg_aladin_SetFloatingSigma(REG,floatingSigma);
	reg_aladin_AlignCentreOn(REG);
	reg_aladin_SetPerformRigid(REG,mode & REG_RIGID);
	reg_aladin_SetPerformAffine(REG,mode & REG_AFFINE);
	reg_aladin_SetBlockPercentage(REG,blockPercentage);
	reg_aladin_SetInlierLts(REG,inlierLts);
	reg_aladin_SetInterpolationToTrilinear(REG);
	reg_aladin_SetVerbose(REG,0);

	if(mode & REG_INIT)
		reg_aladin_SetInputTransform(REG,aff);

	if(reg_aladin_GetLevelsToPerform(REG) > reg_aladin_GetNumberOfLevels(REG))
		reg_aladin_SetLevelsToPerform(REG,reg_aladin_GetNumberOfLevels(REG));

	if(ref == NULL || flo == NULL) {
		reg_aladin_delete(REG);
		return 1;
	}

	reg_aladin_SetInputReference(REG,ref);
	reg_aladin_SetInputFloating(REG,flo);

	if(msk_ref != NULL){
		for(int i=1;i<=ref->dim[0];i++)
			if(ref->dim[i] != msk_ref->dim[i]) {
				reg_aladin_delete(REG);
				return 1;
			}
		reg_aladin_SetInputMask(REG,msk_ref);
	}

    if(sym && msk_flo != NULL) {
        for(int i=1;i<=flo->dim[0];i++)
            if(flo->dim[i] != msk_flo->dim[i]) {
                reg_aladin_delete(REG);
                return 1;
            }
        reg_aladin_SetInputFloatingMask(REG,msk_flo);
    }

	if(reg_aladin_Run(REG)) {
		reg_aladin_delete(REG);
	 	return 1;
	}

	memcpy(aff->m,reg_aladin_GetTransformationMatrix(REG)->m,sizeof(float[4][4]));
	reg_aladin_delete(REG);

    return 0;
}

int register_resample(nifti_image *ref, nifti_image *flo, mat44 *aff, nifti_image **res_o, int interp, float fill) {
	nifti_image *def, *res;
	int ret;

	def = nifti_copy_nim_info(ref);
	def->dim[0]=def->ndim=5;
	def->dim[1]=def->nx=ref->nx;
	def->dim[2]=def->ny=ref->ny;
	def->dim[3]=def->nz=ref->nz;
	def->dim[4]=def->nt=1;def->pixdim[4]=def->dt=1.0;
	if(ref->nz>1) def->dim[5]=def->nu=3;
	else def->dim[5]=def->nu=2;
	def->pixdim[5]=def->du=1.0;
	def->dim[6]=def->nv=1;def->pixdim[6]=def->dv=1.0;
	def->dim[7]=def->nw=1;def->pixdim[7]=def->dw=1.0;
	def->nvox=def->nx*def->ny*def->nz*def->nt*def->nu;
	if(sizeof(myDTYPE)==8) def->datatype = NIFTI_TYPE_FLOAT64;
	else def->datatype = NIFTI_TYPE_FLOAT32;
	def->nbyper = sizeof(myDTYPE);
	def->data = calloc(def->nvox,def->nbyper);
	affine_positionField(aff,ref,def);

	res = nifti_copy_nim_info(ref);
	res->dim[0]=res->ndim=flo->dim[0];
	res->dim[4]=res->nt=flo->dim[4];
	res->cal_min=flo->cal_min;
	res->cal_max=flo->cal_max;
	res->scl_slope=flo->scl_slope;
	res->scl_inter=flo->scl_inter;
	res->datatype = flo->datatype;
	res->nbyper = flo->nbyper;
	res->nvox = res->dim[1] * res->dim[2] * res->dim[3] * res->dim[4];
	res->data = calloc(res->nvox,res->nbyper);

	ret = resampleSourceImage(ref,flo,res,def,NULL,interp,fill);
	nifti_image_free(def);
	if(ret) {
		nifti_image_free(res);
		return 1;
	}
	*res_o = res;

	return 0;
}
