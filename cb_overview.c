#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif

#ifdef HAVE_RENDER_H
#include <X11/extensions/Xrender.h>
#endif
#include <libgen.h>

#include "grid3d_quant.h"
#include "cb_viewport.h"
#include "cb_dialog.h"
#include "cb_overview.h"
#include "cb_mainview.h"
#include "fd_calibration.h"
#include "cb_selection.h"
#include "cb_coloring.h"
#include "cb_contour.h"
#include "cb_overlay.h"
#include "cb_quantization.h"
#include "fd_rois.h"
#include "conrec.h"

#include "selection.h"

#include "util.h"
#include "util_coordinates.h"
#include "util_units.h"
#include "icons_orient_s.h"

static void _draw_ov_xhair(FD_viewport *view, const coord_t j, const coord_t i) {
    const FL_COLOR cur_color = VIEW_CURSOR_COLOR(view);
    const Window win = FL_ObjWin(VIEW_CANVAS(view));
    const unsigned int zoom = VIEW_ZOOM_X(view); // X or Y is the same for overview
    const int dm = fl_get_drawmode(),
              lw = fl_get_linewidth();
    FL_Coord wx = 0, wy = 0;
    XSegment seg[2];
    FL_Coord ww,wh;

    if(!fl_winisvalid(win)) return;

    fl_get_winsize(win,&ww,&wh);
    grid_to_win(view,&wx,&wy,j,i);
    fl_drawmode(GXxor);
    fl_linewidth(zoom);
    /* correct for line width */
    wx += zoom / 2; wy += zoom / 2;
    seg[0].x1 = seg[1].y1 = 0;
    seg[0].y2 = seg[0].y1 = wy;
    seg[1].x1 = seg[1].x2 = wx;
    seg[0].x2 = ww; seg[1].y2 = wh;
    fl_color(cur_color);
    XDrawSegments(fl_display,win,fl_get_gc(),seg,2);
    fl_drawmode(dm);
    fl_linewidth(lw);
}

static void _draw_pos_pix(FL_OBJECT *pix, coord_t k, const unsigned int zoom, const bool vertical, const bool invert) {
    static unsigned char shift[8] = {1,2,4,8,16,32,64,128};

    FL_Coord w,h; fl_get_object_size(pix,&w,&h);
    unsigned int iw = w/8 + (w%8 != 0);
    unsigned char *bits = calloc(h*iw,sizeof(unsigned char));
    if(vertical) {
        if(invert) k = h/zoom - 1 - k;
        for(unsigned int i=0;i<zoom;i++){
            unsigned int m = (k*zoom+i) << 1;
            bits[m] = 0xff;
            bits[m+1] = 0x7f;
        }
    } else {
        if(invert) k = w/zoom - 1 - k;
        for(unsigned int j=0;j<zoom;j++)
            for(unsigned int i=0;i<OV_THICK;i++) {
                unsigned int m = k*zoom+j;
                bits[m/8 + i*iw] += shift[m%8];
            }
    }
    fl_set_bitmap_data(pix,w,h,bits);
    free(bits);
}

static void _update_positioners(FD_viewport **views, FD_overview *overview, const enum g3d_slice_plane plane) {
    const enum xg3d_orientation orient_XY = VIEW_ORIENT(views[PLANE_XY]);
    const enum xg3d_orientation orient_XZ = VIEW_ORIENT(views[PLANE_XZ]);
    const enum xg3d_orientation orient_ZY = VIEW_ORIENT(views[PLANE_ZY]);
    const unsigned int zoom_XY = VIEW_ZOOM_X(views[PLANE_XY]); // X or Y is the same for overview
    const unsigned int zoom_XZ = VIEW_ZOOM_X(views[PLANE_XZ]); // X or Y is the same for overview
    const unsigned int zoom_ZY = VIEW_ZOOM_X(views[PLANE_ZY]); // X or Y is the same for overview
    const coord_t k = VIEW_K(views[plane]);

    
    
    
    
    switch(plane) {
        case PLANE_XY:
            if(fl_object_is_visible(overview->xz_fb))         switch(orient_XZ) {
            case ORIENT_0:   case ORIENT_F180:         _draw_pos_pix(views[PLANE_XZ]->pos_left,k,zoom_XZ,true,false);
        _draw_pos_pix(views[PLANE_XZ]->pos_right,k,zoom_XZ,true,false);
    break;
            case ORIENT_90:  case ORIENT_F90:          _draw_pos_pix(views[PLANE_XZ]->pos_top,k,zoom_XZ,false,false);
        _draw_pos_pix(views[PLANE_XZ]->pos_bottom,k,zoom_XZ,false,false);
    break;
            case ORIENT_180: case ORIENT_F0:           _draw_pos_pix(views[PLANE_XZ]->pos_left,k,zoom_XZ,true,true);
        _draw_pos_pix(views[PLANE_XZ]->pos_right,k,zoom_XZ,true,true);
    break;
            case ORIENT_270: case ORIENT_F270:         _draw_pos_pix(views[PLANE_XZ]->pos_top,k,zoom_XZ,false,true);
        _draw_pos_pix(views[PLANE_XZ]->pos_bottom,k,zoom_XZ,false,true);
    break;
        }
   
            if(fl_object_is_visible(overview->zy_fb))         switch(orient_ZY) {
            case ORIENT_0:   case ORIENT_F0:           _draw_pos_pix(views[PLANE_ZY]->pos_top,k,zoom_ZY,false,false);
        _draw_pos_pix(views[PLANE_ZY]->pos_bottom,k,zoom_ZY,false,false);
    break;
            case ORIENT_90:  case ORIENT_F270:         _draw_pos_pix(views[PLANE_ZY]->pos_left,k,zoom_ZY,true,true);
        _draw_pos_pix(views[PLANE_ZY]->pos_right,k,zoom_ZY,true,true);
    break;
            case ORIENT_180: case ORIENT_F180:         _draw_pos_pix(views[PLANE_ZY]->pos_top,k,zoom_ZY,false,true);
        _draw_pos_pix(views[PLANE_ZY]->pos_bottom,k,zoom_ZY,false,true);
    break;
            case ORIENT_270: case ORIENT_F90:          _draw_pos_pix(views[PLANE_ZY]->pos_left,k,zoom_ZY,true,false);
        _draw_pos_pix(views[PLANE_ZY]->pos_right,k,zoom_ZY,true,false);
    break;
        }
   
            break;

        case PLANE_XZ:
            if(fl_object_is_visible(overview->xy_fb))         switch(orient_XY) {
            case ORIENT_0:   case ORIENT_F180:         _draw_pos_pix(views[PLANE_XY]->pos_left,k,zoom_XY,true,false);
        _draw_pos_pix(views[PLANE_XY]->pos_right,k,zoom_XY,true,false);
    break;
            case ORIENT_90:  case ORIENT_F90:          _draw_pos_pix(views[PLANE_XY]->pos_top,k,zoom_XY,false,false);
        _draw_pos_pix(views[PLANE_XY]->pos_bottom,k,zoom_XY,false,false);
    break;
            case ORIENT_180: case ORIENT_F0:           _draw_pos_pix(views[PLANE_XY]->pos_left,k,zoom_XY,true,true);
        _draw_pos_pix(views[PLANE_XY]->pos_right,k,zoom_XY,true,true);
    break;
            case ORIENT_270: case ORIENT_F270:         _draw_pos_pix(views[PLANE_XY]->pos_top,k,zoom_XY,false,true);
        _draw_pos_pix(views[PLANE_XY]->pos_bottom,k,zoom_XY,false,true);
    break;
        }
   
            if(fl_object_is_visible(overview->zy_fb))         switch(orient_ZY) {
            case ORIENT_0:   case ORIENT_F180:         _draw_pos_pix(views[PLANE_ZY]->pos_left,k,zoom_ZY,true,false);
        _draw_pos_pix(views[PLANE_ZY]->pos_right,k,zoom_ZY,true,false);
    break;
            case ORIENT_90:  case ORIENT_F90:          _draw_pos_pix(views[PLANE_ZY]->pos_top,k,zoom_ZY,false,false);
        _draw_pos_pix(views[PLANE_ZY]->pos_bottom,k,zoom_ZY,false,false);
    break;
            case ORIENT_180: case ORIENT_F0:           _draw_pos_pix(views[PLANE_ZY]->pos_left,k,zoom_ZY,true,true);
        _draw_pos_pix(views[PLANE_ZY]->pos_right,k,zoom_ZY,true,true);
    break;
            case ORIENT_270: case ORIENT_F270:         _draw_pos_pix(views[PLANE_ZY]->pos_top,k,zoom_ZY,false,true);
        _draw_pos_pix(views[PLANE_ZY]->pos_bottom,k,zoom_ZY,false,true);
    break;
        }
   
            break;

        case PLANE_ZY:
            if(fl_object_is_visible(overview->xz_fb))         switch(orient_XZ) {
            case ORIENT_0:   case ORIENT_F0:           _draw_pos_pix(views[PLANE_XZ]->pos_top,k,zoom_XZ,false,false);
        _draw_pos_pix(views[PLANE_XZ]->pos_bottom,k,zoom_XZ,false,false);
    break;
            case ORIENT_90:  case ORIENT_F270:         _draw_pos_pix(views[PLANE_XZ]->pos_left,k,zoom_XZ,true,true);
        _draw_pos_pix(views[PLANE_XZ]->pos_right,k,zoom_XZ,true,true);
    break;
            case ORIENT_180: case ORIENT_F180:         _draw_pos_pix(views[PLANE_XZ]->pos_top,k,zoom_XZ,false,true);
        _draw_pos_pix(views[PLANE_XZ]->pos_bottom,k,zoom_XZ,false,true);
    break;
            case ORIENT_270: case ORIENT_F90:          _draw_pos_pix(views[PLANE_XZ]->pos_left,k,zoom_XZ,true,false);
        _draw_pos_pix(views[PLANE_XZ]->pos_right,k,zoom_XZ,true,false);
    break;
        }
   
            if(fl_object_is_visible(overview->xy_fb))         switch(orient_XY) {
            case ORIENT_0:   case ORIENT_F0:           _draw_pos_pix(views[PLANE_XY]->pos_top,k,zoom_XY,false,false);
        _draw_pos_pix(views[PLANE_XY]->pos_bottom,k,zoom_XY,false,false);
    break;
            case ORIENT_90:  case ORIENT_F270:         _draw_pos_pix(views[PLANE_XY]->pos_left,k,zoom_XY,true,true);
        _draw_pos_pix(views[PLANE_XY]->pos_right,k,zoom_XY,true,true);
    break;
            case ORIENT_180: case ORIENT_F180:         _draw_pos_pix(views[PLANE_XY]->pos_top,k,zoom_XY,false,true);
        _draw_pos_pix(views[PLANE_XY]->pos_bottom,k,zoom_XY,false,true);
    break;
            case ORIENT_270: case ORIENT_F90:          _draw_pos_pix(views[PLANE_XY]->pos_left,k,zoom_XY,true,false);
        _draw_pos_pix(views[PLANE_XY]->pos_right,k,zoom_XY,true,false);
    break;
        }
   
            break;

        default:
            fprintf(stderr,"%s: invalid g3d_slice_plane value passed: %d",__func__,plane);
            abort();
    }
}

void update_all_positioners(FD_viewport **views, FD_overview *overview) {
    _update_positioners(views,overview,PLANE_XY);
    _update_positioners(views,overview,PLANE_XZ);
    _update_positioners(views,overview,PLANE_ZY);
}

inline static void _draw_roi_vox(const FL_Coord x, const FL_Coord y, const unsigned int zoom, const FL_COLOR color) {
    zoom > 1 ? fl_rectf(x,y,zoom,zoom,color) : fl_point(x,y,color);
}

static void _draw_marker_label(FD_viewport *view, const marker *mk, const FL_COLOR color) {
    const enum g3d_slice_plane plane = VIEW_PLANE(view);
    const unsigned int zoom = VIEW_ZOOM_X(view); // X or Y is the same for overview
    FL_Coord wx = 0, wy = 0;

    grid_to_win(view,&wx,&wy,mk->vox[PLANE_TO_J(plane)],mk->vox[PLANE_TO_I(plane)]);
    fl_draw_text_beside(mk->align,wx+zoom/2,wy,1,1,color,UNIFONT,FL_NORMAL_SIZE,mk->label);
}

static void _ov_draw_roi(FD_viewport *view, const xg3d_roi *xroi) {
    const enum g3d_slice_plane plane = VIEW_PLANE(view);
    const unsigned int zoom = VIEW_ZOOM_X(view); // X or Y is the same for overview
    const Window win = FL_ObjWin(VIEW_CANVAS(view));
    const int dm = fl_get_drawmode();
    const coord_t k = VIEW_K(view);

    if(!fl_winisvalid(win)) return;

    fl_winset(win);
    fl_drawmode(GXcopy);
    fl_mapcolor(FL_FREE_COL1,FL_GETR(xroi->color),FL_GETG(xroi->color),FL_GETB(xroi->color));
    for(index_t ix=0;ix<xroi->roi->size;ix++)
        if(xroi->roi->vox[3*ix+PLANE_TO_K(plane)] == k) {
            FL_Coord x,y;
            grid_to_win(view,&x,&y,xroi->roi->vox[3*ix+PLANE_TO_J(plane)],xroi->roi->vox[3*ix+PLANE_TO_I(plane)]);
            _draw_roi_vox(x,y,zoom,FL_FREE_COL1);
        }
    fl_drawmode(dm);
}

void draw_ov_sel_voxel(FD_viewport *view, FD_selection *selection, const coord_t j, const coord_t i) {
    _selpattf *patt_sel = SELECTION_PATTERN_FUNC(selection);
    const Window win = FL_ObjWin(VIEW_CANVAS(view));
    const unsigned int zoom = VIEW_ZOOM_X(view); // X or Y is the same for overview
    const int dm = fl_get_drawmode();
    FL_Coord wx = 0, wy = 0;

    if(!fl_winisvalid(win)) return;

    fl_winset(win);
    fl_drawmode(SEL_GXMODE);
    grid_to_win(view,&wx,&wy,j,i);
    patt_sel(wx,wy,zoom,zoom,SEL_COLOR);
    fl_drawmode(dm);
}

void draw_ov_marker(FD_viewport *view, const coord_t j, const coord_t i, const FL_COLOR color) {
    const Window win = FL_ObjWin(VIEW_CANVAS(view));
    const unsigned int zoom = VIEW_ZOOM_X(view); // X or Y is the same for overview
    const int dm = fl_get_drawmode();
    FL_Coord wx = 0, wy = 0;

    if(!fl_winisvalid(win)) return;

    fl_winset(win);
    fl_drawmode(GXcopy);
    grid_to_win(view,&wx,&wy,j,i);
    _draw_roi_vox(wx,wy,zoom,color);
    fl_drawmode(dm);
}

void ov_restore_image_voxel(FD_viewport *view, FD_selection *selection, const coord_t j, const coord_t i) {
    const enum g3d_slice_plane plane = VIEW_PLANE(view);
    const grid3d *sel = SELECTION_G3D(selection);
    const FL_IMAGE *img = VIEW_IMAGE(view);
    const coord_t k = VIEW_K(view);
    coord_t vox[3];
    FL_Coord imx = 0,imy = 0;

    /* slice to global coordinates */
    vox[PLANE_TO_J(plane)] = j;
    vox[PLANE_TO_I(plane)] = i;
    vox[PLANE_TO_K(plane)] = k;

    grid_to_win(view,&imx,&imy,j,i);
    fl_mapcolor(FL_FREE_COL1,img->red_lut[img->ci[imy][imx]],img->green_lut[img->ci[imy][imx]],img->blue_lut[img->ci[imy][imx]]);
    draw_ov_marker(view,j,i,FL_FREE_COL1);

    if(g3d_selbit_check(sel,COORD_DIM(vox,sel->nx,sel->nxy)))
        draw_ov_sel_voxel(view,selection,j,i);
}

static void _ov_draw_sel(FD_viewport *view, FD_selection *selection) {
    _selpattf *patt_sel = SELECTION_PATTERN_FUNC(selection);
    const enum xg3d_orientation orient = VIEW_ORIENT(view);
    const enum g3d_slice_plane plane = VIEW_PLANE(view);
    const Window win = FL_ObjWin(VIEW_CANVAS(view));
    const unsigned int zoom = VIEW_ZOOM_X(view); // X or Y is the same for overview
    const int dm = fl_get_drawmode();
    const coord_t k = VIEW_K(view);

    if(!fl_winisvalid(win) || SELECTION_NVOX(selection) == 0) return;

    grid3d *selslice = g3d_slice(SELECTION_G3D(selection),k,plane);
    if(zoom == 1) {
        int w = (orient & 1U) ? selslice->ny : selslice->nx;
        int h = (orient & 1U) ? selslice->nx : selslice->ny;
        int iw = w/8 + (w%8 != 0);
        size_t siz = iw*h;
        unsigned char *data = calloc(siz,sizeof(unsigned char));
        for(slice_t ix=0;ix<selslice->nvox;ix++) {
            FL_Coord wx,wy;
            grid_to_win(view,&wx,&wy,ix%selslice->nx,ix/selslice->nx);
            data[wx/8 + wy*iw] += g3d_selbit_check(selslice,ix) * (1U << (wx%8));
        }
        XImage *xim = XCreateImage(fl_display,fl_get_visual(),1,XYBitmap,0,(char*)data,w,h,8,0);
        fl_drawmode(GXxor); fl_color(FL_WHITE); fl_bk_color(FL_BLACK);
        XPutImage(fl_display,win,fl_get_gc(),xim,0,0,0,0,w,h);
        XDestroyImage(xim);
    } else {
        fl_winset(win); fl_drawmode(SEL_GXMODE);
        for(slice_t ix=0;ix<selslice->nvox;ix++)
            if(g3d_selbit_check(selslice,ix)) {
                FL_Coord wx,wy;
                grid_to_win(view,&wx,&wy,ix%selslice->nx,ix/selslice->nx);
                patt_sel(wx,wy,zoom,zoom,SEL_COLOR);
            }
    }
    fl_drawmode(dm);
    g3d_free(selslice);
}

static void _ov_draw_markers(FD_viewport *view, FD_selection *selection) {
    const enum g3d_slice_plane plane = VIEW_PLANE(view);
    const FL_COLOR cur_color = VIEW_CURSOR_COLOR(view);
    const Window win = FL_ObjWin(VIEW_CANVAS(view));
    const int dm = fl_get_drawmode();
    const coord_t k = VIEW_K(view);
    marker *mk;

    if(!fl_winisvalid(win) || SELECTION_NMK(selection) == 0) return;

    fl_winset(win);
    fl_drawmode(GXcopy);
    mk = SELECTION_MKLIST(selection);
    while(mk != NULL) {
        if(mk->vox[PLANE_TO_K(plane)] == k) { /* map global coordinates to in-plane coordinates */
            draw_ov_marker(view,mk->vox[PLANE_TO_J(plane)],mk->vox[PLANE_TO_I(plane)],cur_color);
            if(fl_get_button(selection->btn_labels) && mk->label != NULL)
                _draw_marker_label(view,mk,cur_color);
        }
        mk = mk->next;
    }
    fl_drawmode(dm);
}

static void _ov_draw_rois(FD_viewport *view, FD_rois *rois, const xg3d_data *data) {
    for(nroi_t i=0;i<data->nrois;i++)
        if(fl_isselected_browser_line(rois->browser,i+1))
            _ov_draw_roi(view,data->rois[i]);
}

static void _draw_contline(const double x1, const double y1, const double x2, const double y2, const unsigned int k, void *p) {
    FD_viewport *view = (FD_viewport*)p;
    FL_Coord wx1 = 0, wy1 = 0, wx2 = 0, wy2 = 0;

    ccoord_to_win(view,&wx1,&wy1,x1,y1);
    ccoord_to_win(view,&wx2,&wy2,x2,y2);
    fl_line(wx1,wy1,wx2,wy2,FL_FREE_COL1+k);
}

static void _draw_contours(FD_viewport *view, FD_contour *contour) {
    const grid3d *doseslice = VIEW_SLICE_LAYER(view,LAYER_DOSE);
    const Window win = FL_ObjWin(VIEW_CANVAS(view));
    const FL_PACKED *lut = CONTOUR_LUT(contour);
    const unsigned int nc = CONTOUR_NC(contour);
    const double *z = CONTOUR_Z(contour);
    const int dm = fl_get_drawmode();

    if(!fl_winisvalid(win) || nc == 0 || z == NULL) return;

    for(unsigned int i=0;i<nc;i++)
        fl_mapcolor(FL_FREE_COL1 + i,FL_GETR(lut[i]),FL_GETG(lut[i]),FL_GETB(lut[i]));
    fl_winset(win);
    fl_drawmode(GXcopy);
    Contour(doseslice,doseslice->nx,doseslice->ny,nc,z,_draw_contline,view);
    fl_drawmode(dm);
}

void replace_ov_slice(FD_viewport *view, const enum xg3d_layer layer) {
    xg3d_view_workspace *vwk = VIEW_WORKSPACE(view);
    grid3d **slice = &vwk->slice[layer];

    g3d_free(*slice);
    if(vwk->data[layer] != NULL && vwk->k[layer] < vwk->data[layer]->hdr->dim[PLANE_TO_K(vwk->plane)])
        *slice = g3d_slice(vwk->data[layer]->g3d,vwk->k[layer],vwk->plane);
    else
        *slice = NULL;
}

void replace_ov_slices(FD_viewport **views, const enum xg3d_layer layer) {
    replace_ov_slice(views[PLANE_XY],layer);
    replace_ov_slice(views[PLANE_XZ],layer);
    replace_ov_slice(views[PLANE_ZY],layer);
}

void replace_ov_quantslice(FD_viewport *view, FD_quantization *quantization, const enum xg3d_layer layer) {
    xg3d_view_workspace *vwk = VIEW_WORKSPACE(view);
    const grid3d *slice = vwk->slice[layer];
    grid3d **qslice = &vwk->quant_slice[layer];

    g3d_free(*qslice);
    if(slice != NULL && vwk->k[layer] < vwk->data[layer]->hdr->dim[PLANE_TO_K(vwk->plane)]) {
        *qslice = g3d_alloc(G3D_UINT8,slice->nx,slice->ny,slice->nz,.alloc=true);
        g3d_quant_apply(slice,*qslice,QUANTIZATION_INFO(quantization));
    } else
        *qslice = NULL;
}

void replace_ov_quantslices(FD_viewport **views, FD_quantization *quantization, const enum xg3d_layer layer) {
    replace_ov_quantslice(views[PLANE_XY],quantization,layer);
    replace_ov_quantslice(views[PLANE_XZ],quantization,layer);
    replace_ov_quantslice(views[PLANE_ZY],quantization,layer);
}

void replace_ov_img(FD_viewport *view, FD_quantization *quantization, const enum xg3d_layer layer) {
    xg3d_view_workspace *vwk = VIEW_WORKSPACE(view);
    const unsigned int nlvl = QUANTIZATION_NLEVELS(quantization);
    const unsigned int theta = 90 * (vwk->orient &~ ORIENT_FLIP);
    const bool flip = orient_flip_check(vwk->orient);

    if(vwk->quant_slice[layer] == NULL) {
        flimage_free(vwk->image[layer]);
        vwk->image[layer] = NULL;
        return;
    }
    
    vwk->image[layer] = g3d_to_flimage(.x=vwk->quant_slice[layer],.levels=nlvl,.zoom_x=vwk->zoom_x,.zoom_y=vwk->zoom_y,.theta=theta,.flip=flip,.im=&vwk->image[layer]);
}

void replace_ov_images(FD_viewport **views, FD_quantization *quantization, const enum xg3d_layer layer) {
    replace_ov_img(views[PLANE_XY],quantization,layer);
    replace_ov_img(views[PLANE_XZ],quantization,layer);
    replace_ov_img(views[PLANE_ZY],quantization,layer);
}

static Pixmap _flimage_to_pixmap_alpha(FL_IMAGE *img, const Window win, const double alpha) {
    XWindowAttributes xwa;
    Pixmap pix;

    XGetWindowAttributes(fl_display,win,&xwa);
    if(!img->gc) { img->gc = XCreateGC(fl_display,win,0,0); }
    if(img->ximage) { XDestroyImage(img->ximage); img->ximage = None; }
    pix = XCreatePixmap(fl_display,win,img->w,img->h,xwa.depth);
    if(flimage_to_ximage(img,win,&xwa) >= 0) {
        XAddPixel(img->ximage,(FL_nint(alpha * 0xff) - 0xff) << 24);
        XPutImage(fl_display,pix,img->gc,img->ximage,0,0,0,0,img->w,img->h);
        XDestroyImage(img->ximage); img->ximage = None;
    }

    return pix;
}

static Pixmap _solid_pixmap(const Window win, const GC gc, const int w, const int h, const FL_PACKED color, const double alpha) {
    XWindowAttributes xwa;
    Pixmap pix;

    XGetWindowAttributes(fl_display,win,&xwa);
    pix = XCreatePixmap(fl_display,win,w,h,xwa.depth);
    XSetForeground(fl_display,gc,color+(FL_nint(alpha * 0xff) << 24)); /* color hack */
    XFillRectangle(fl_display,pix,gc,0,0,w,h);

    return pix;
}

#undef ENV
void refresh_ov_canvas(xg3d_env *ENV, const enum g3d_slice_plane plane) {
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
    const xg3d_env *env_overlay = OVERLAY_ENV(GUI->overlay);
    const xg3d_lut *lut = COLORING_LUT(GUI->coloring);
    const xg3d_lut *lut_over = COLORING_LUT(env_overlay->gui->coloring);
    const double alpha = fl_get_slider_value(GUI->overlay->sli_alpha);
    const Window win = FL_ObjWin(VIEW_CANVAS(ov_viewports[plane]));
    FL_IMAGE **omg = (FL_IMAGE**)&VIEW_IMAGE_LAYER(ov_viewports[plane],LAYER_OVERLAY);
    FL_IMAGE *pmg = VIEW_IMAGE_LAYER(ov_viewports[plane],LAYER_PREVIEW);
    FL_IMAGE *img = VIEW_IMAGE(ov_viewports[plane]);

    if(!fl_winisvalid(win)) return;

    if(!fl_get_button(GUI->overlay->btn_show)) {
        if(fl_get_button(GUI->operations->btn_rot_preview)) { /* preview rotation */
            g3d_flimage_set_lut(pmg,lut->colors);
            flimage_display(pmg,win);
        } else { /* normal display */
            g3d_flimage_set_lut(img,lut->colors);
            flimage_display(img,win);
        }
    } else { /* compose with overlay */
        Pixmap pix,pex;
        if(VIEW_QUANT_SLICE_LAYER(ov_viewports[plane],LAYER_OVERLAY) != NULL) {
            replace_ov_img(ov_viewports[plane],env_overlay->gui->quantization,LAYER_OVERLAY);
            adapt_overlay_img(ov_viewports[plane],GUI->overlay,lut_over);
            if(alpha >= 0.0) {
                pix = _flimage_to_pixmap_alpha(img,win,1.0);
                pex = _flimage_to_pixmap_alpha(*omg,win,alpha);
            } else {
                pix = _flimage_to_pixmap_alpha(*omg,win,1.0);
                pex = _flimage_to_pixmap_alpha(img,win,-alpha);
            }
        } else {
            if(alpha >= 0.0) {
                pix = _flimage_to_pixmap_alpha(img,win,1.0);
                pex = _solid_pixmap(win,img->gc,img->w,img->h,lut_over->colors[0],alpha);
            } else {
                pix = _solid_pixmap(win,img->gc,img->w,img->h,lut_over->colors[0],1.0);
                pex = _flimage_to_pixmap_alpha(img,win,-alpha);
            }
        }

        int op;
        switch(fl_get_choice(GUI->overlay->ch_op)) {
            default:
            case  1: op = PictOpOver;          break;
            case  2: op = PictOpMultiply;      break;
            case  3: op = PictOpScreen;        break;
            case  4: op = PictOpOverlay;       break;
            case  5: op = PictOpColorDodge;    break;
            case  6: op = PictOpColorBurn;     break;
            case  7: op = PictOpHardLight;     break;
            case  8: op = PictOpSoftLight;     break;
            case  9: op = PictOpDifference;    break;
            case 10: op = PictOpExclusion;     break;
            case 11: op = PictOpDarken;        break;
            case 12: op = PictOpLighten;       break;
            case 13: op = PictOpHSLHue;        break;
            case 14: op = PictOpHSLSaturation; break;
            case 15: op = PictOpHSLColor;      break;
            case 16: op = PictOpHSLLuminosity; break;
        }

#ifdef HAVE_RENDER_H
        const XRenderPictFormat *xrpf = XRenderFindVisualFormat(fl_display,fl_visual);
        Picture base = XRenderCreatePicture(fl_display,pix,xrpf,0,NULL);
        Picture over = XRenderCreatePicture(fl_display,pex,xrpf,0,NULL);
        XRenderComposite(fl_display,op,over,None,base,0,0,0,0,0,0,img->w,img->h);
#endif
        XCopyArea(fl_display,pix,win,img->gc,0,0,img->w,img->h,0,0);
#ifdef HAVE_RENDER_H
        XRenderFreePicture(fl_display,base);
        XRenderFreePicture(fl_display,over);
#endif
        XFreePixmap(fl_display,pix);
        XFreePixmap(fl_display,pex);
    }

    if(SELECTION_IS_VISIBLE(GUI->selection) && fl_get_button(GUI->overview->draw_sel))
        _ov_draw_sel(ov_viewports[plane],GUI->selection);
    if(fl_get_button(GUI->overview->show_rois))
        _ov_draw_rois(ov_viewports[plane],GUI->rois,DATA);
    if(fl_get_button(((FD_contour*)(GUI->contour))->btn_show))
        _draw_contours(ov_viewports[plane],GUI->contour);
    if(fl_get_button(GUI->overview->draw_sel))
        _ov_draw_markers(ov_viewports[plane],GUI->selection);
}
#define ENV _default_env

/* always use overview global */
#undef ENV
void refresh_ov_canvases(FD_overview *overview) {
    xg3d_env *ENV = (xg3d_env*)(overview->overview->u_vdata);

    refresh_ov_canvas(ENV,PLANE_XY);
    refresh_ov_canvas(ENV,PLANE_XZ);
    refresh_ov_canvas(ENV,PLANE_ZY);
}
#define ENV _default_env

/* explanation: canvas width + size of 2 positioners +
 * padding between canvas and left/right borders + 2 */
#define XY_FRAME_W FL_max(img_xy->w + 2 * OV_THICK + 2 * OV_PAD + 2,OV_FRAME_MIN_WIDTH)
#define XZ_FRAME_W FL_max(img_xz->w + 2 * OV_THICK + 2 * OV_PAD + 2,OV_FRAME_MIN_WIDTH)
#define ZY_FRAME_W FL_max(img_zy->w + 2 * OV_THICK + 2 * OV_PAD + 2,OV_FRAME_MIN_WIDTH)
/* explanation: canvas size + size of 2 positioners + slider height +
 * padding between bottom and slider, slider and canvas, canvas and top + 2 */
#define XY_FRAME_H (img_xy->h + 2 * OV_THICK + OV_SLIDER_THICK + 4 * OV_PAD + 2)
#define XZ_FRAME_H (img_xz->h + 2 * OV_THICK + OV_SLIDER_THICK + 4 * OV_PAD + 2)
#define ZY_FRAME_H (img_zy->h + 2 * OV_THICK + OV_SLIDER_THICK + 4 * OV_PAD + 2)
#define DEF_SCROLL_THICK 18 /* from fli_get_default_scrollbarsize */

#undef ENV
void resize_ov_canvases(xg3d_env *ENV) {
    FD_viewport *view_xy = OVERVIEW_VIEWPORTS(ENV->gui->overview)[PLANE_XY];
    FD_viewport *view_xz = OVERVIEW_VIEWPORTS(ENV->gui->overview)[PLANE_XZ];
    FD_viewport *view_zy = OVERVIEW_VIEWPORTS(ENV->gui->overview)[PLANE_ZY];
    const FL_IMAGE *img_xy = VIEW_IMAGE(view_xy);
    const FL_IMAGE *img_xz = VIEW_IMAGE(view_xz);
    const FL_IMAGE *img_zy = VIEW_IMAGE(view_zy);
    FL_Coord formw = OV_LEFT_FRAME_MARGIN, formh = 0;
    FL_Coord framew_max,frameh_max;
    FL_Coord framew[3] = {0,0,0},
             frameh[3] = {0,0,0};
    double xs = 0.0,ys = 0.0;
    FL_Coord dx;
    int k = 0;

    k = fl_object_is_visible(GUI->overview->xy_frame) +
        fl_object_is_visible(GUI->overview->xz_frame) +
        fl_object_is_visible(GUI->overview->zy_frame);

    /* split available space evenly among visible frames */
    framew_max = (OV_MAX_WIDTH - OV_LEFT_MARGIN - (k+1)*OV_PAD) / k;
    frameh_max = OV_MAX_HEIGHT - OV_BOT_MARGIN - OV_PAD;

                if(fl_object_is_visible(GUI->overview->xy_frame)) {
            if(XY_FRAME_W >= framew_max && XY_FRAME_H >= frameh_max) {
                framew[PLANE_XY] = FL_min(XY_FRAME_W+DEF_SCROLL_THICK+2,framew_max);
                frameh[PLANE_XY] = FL_min(XY_FRAME_H+DEF_SCROLL_THICK+2,frameh_max);
                xs = fl_get_formbrowser_xscroll(GUI->overview->xy_fb);
                ys = fl_get_formbrowser_yscroll(GUI->overview->xy_fb);
            } else if(XY_FRAME_W >= framew_max) {
                framew[PLANE_XY] = FL_min(XY_FRAME_W,framew_max);
                frameh[PLANE_XY] = FL_min(XY_FRAME_H+DEF_SCROLL_THICK+2,frameh_max);
                xs = fl_get_formbrowser_xscroll(GUI->overview->xy_fb);
                ys = 0.0;
            } else if(XY_FRAME_H >= frameh_max) {
                framew[PLANE_XY] = FL_min(XY_FRAME_W+DEF_SCROLL_THICK+2,framew_max);
                frameh[PLANE_XY] = FL_min(XY_FRAME_H,frameh_max);
                xs = 0.0;
                ys = fl_get_formbrowser_yscroll(GUI->overview->xy_fb);
            } else {
                framew[PLANE_XY] = FL_min(XY_FRAME_W,framew_max);
                frameh[PLANE_XY] = FL_min(XY_FRAME_H,frameh_max);
                xs = ys = 0.0;
            }
            formw += framew[PLANE_XY]+OV_PAD;
            if(frameh[PLANE_XY] > formh)
                formh = frameh[PLANE_XY];
        }
           if(fl_object_is_visible(GUI->overview->xz_frame)) {
            if(XZ_FRAME_W >= framew_max && XZ_FRAME_H >= frameh_max) {
                framew[PLANE_XZ] = FL_min(XZ_FRAME_W+DEF_SCROLL_THICK+2,framew_max);
                frameh[PLANE_XZ] = FL_min(XZ_FRAME_H+DEF_SCROLL_THICK+2,frameh_max);
                xs = fl_get_formbrowser_xscroll(GUI->overview->xz_fb);
                ys = fl_get_formbrowser_yscroll(GUI->overview->xz_fb);
            } else if(XZ_FRAME_W >= framew_max) {
                framew[PLANE_XZ] = FL_min(XZ_FRAME_W,framew_max);
                frameh[PLANE_XZ] = FL_min(XZ_FRAME_H+DEF_SCROLL_THICK+2,frameh_max);
                xs = fl_get_formbrowser_xscroll(GUI->overview->xz_fb);
                ys = 0.0;
            } else if(XZ_FRAME_H >= frameh_max) {
                framew[PLANE_XZ] = FL_min(XZ_FRAME_W+DEF_SCROLL_THICK+2,framew_max);
                frameh[PLANE_XZ] = FL_min(XZ_FRAME_H,frameh_max);
                xs = 0.0;
                ys = fl_get_formbrowser_yscroll(GUI->overview->xz_fb);
            } else {
                framew[PLANE_XZ] = FL_min(XZ_FRAME_W,framew_max);
                frameh[PLANE_XZ] = FL_min(XZ_FRAME_H,frameh_max);
                xs = ys = 0.0;
            }
            formw += framew[PLANE_XZ]+OV_PAD;
            if(frameh[PLANE_XZ] > formh)
                formh = frameh[PLANE_XZ];
        }
           if(fl_object_is_visible(GUI->overview->zy_frame)) {
            if(ZY_FRAME_W >= framew_max && ZY_FRAME_H >= frameh_max) {
                framew[PLANE_ZY] = FL_min(ZY_FRAME_W+DEF_SCROLL_THICK+2,framew_max);
                frameh[PLANE_ZY] = FL_min(ZY_FRAME_H+DEF_SCROLL_THICK+2,frameh_max);
                xs = fl_get_formbrowser_xscroll(GUI->overview->zy_fb);
                ys = fl_get_formbrowser_yscroll(GUI->overview->zy_fb);
            } else if(ZY_FRAME_W >= framew_max) {
                framew[PLANE_ZY] = FL_min(ZY_FRAME_W,framew_max);
                frameh[PLANE_ZY] = FL_min(ZY_FRAME_H+DEF_SCROLL_THICK+2,frameh_max);
                xs = fl_get_formbrowser_xscroll(GUI->overview->zy_fb);
                ys = 0.0;
            } else if(ZY_FRAME_H >= frameh_max) {
                framew[PLANE_ZY] = FL_min(ZY_FRAME_W+DEF_SCROLL_THICK+2,framew_max);
                frameh[PLANE_ZY] = FL_min(ZY_FRAME_H,frameh_max);
                xs = 0.0;
                ys = fl_get_formbrowser_yscroll(GUI->overview->zy_fb);
            } else {
                framew[PLANE_ZY] = FL_min(ZY_FRAME_W,framew_max);
                frameh[PLANE_ZY] = FL_min(ZY_FRAME_H,frameh_max);
                xs = ys = 0.0;
            }
            formw += framew[PLANE_ZY]+OV_PAD;
            if(frameh[PLANE_ZY] > formh)
                formh = frameh[PLANE_ZY];
        }
   
    formh += OV_BOT_MARGIN+OV_PAD;

    int ov_toolbar_h = OV_LEFT_MARGIN * (
            fl_object_is_visible(GUI->overview->rotateCCW) +
            fl_object_is_visible(GUI->overview->rotateCW) +
            fl_object_is_visible(GUI->overview->flip) +
            fl_object_is_visible(GUI->overview->zoomIn) +
            fl_object_is_visible(GUI->overview->zoomOut) +
            fl_object_is_visible(GUI->overview->planeXY) +
            fl_object_is_visible(GUI->overview->planeXZ) +
            fl_object_is_visible(GUI->overview->planeZY) +
            fl_object_is_visible(GUI->overview->draw_sel) +
            fl_object_is_visible(GUI->overview->show_overlay) +
            fl_object_is_visible(GUI->overview->show_rois) +
            fl_object_is_visible(GUI->overview->show_contours) +
            fl_object_is_visible(GUI->overview->save)
            );

    int ov_status_n = fl_object_is_visible(GUI->overview->ch_ulen) +
        fl_object_is_visible(GUI->overview->status);

    formw = FL_clamp(formw,OV_MIN_WIDTH,OV_MAX_WIDTH);
    formh = FL_clamp(formh,ov_toolbar_h+ov_status_n*OV_STATUS_H,OV_MAX_HEIGHT);

    fl_freeze_form(GUI->overview->overview);
    fl_set_form_size(GUI->overview->overview,formw,formh);

    if(fl_object_is_visible(GUI->overview->xy_frame)) {
                            fl_set_object_geometry(GUI->overview->xy_frame,0,OV_PAD+3,framew[PLANE_XY],frameh[PLANE_XY]);
            fl_set_object_position(GUI->overview->xy_zoom,3*OV_FRAME_LABEL_PAD,1);
            fl_set_object_position(GUI->overview->xy_tomain,framew[PLANE_XY]-OV_TOMAIN_W-4*OV_FRAME_LABEL_PAD,1);
            fl_set_object_geometry(GUI->overview->xy_bit_orient,OV_FRAME_LABEL_PAD,0,18,18);
            fl_set_object_geometry(GUI->overview->xy_fb,OV_PAD,3*OV_PAD+3,framew[PLANE_XY]-2*OV_PAD,frameh[PLANE_XY]-4*OV_PAD-OV_SLIDER_THICK);
            fl_set_object_geometry(GUI->overview->xy_k,OV_PAD,frameh[PLANE_XY]-OV_SLIDER_THICK+3,OV_LEFT_MARGIN,OV_SLIDER_THICK);
            fl_set_object_geometry(GUI->overview->xy_btn_dn,OV_PAD+OV_LEFT_MARGIN,frameh[PLANE_XY]-OV_SLIDER_THICK+3,OV_THICK,OV_SLIDER_THICK);
            fl_set_object_geometry(GUI->overview->xy_slider,OV_PAD+OV_THICK+OV_LEFT_MARGIN,frameh[PLANE_XY]-OV_SLIDER_THICK+3,framew[PLANE_XY]-2*OV_PAD-2*OV_THICK-OV_LEFT_MARGIN,OV_SLIDER_THICK);
            fl_set_object_geometry(GUI->overview->xy_btn_up,framew[PLANE_XY]-OV_PAD-OV_THICK,frameh[PLANE_XY]-OV_SLIDER_THICK+3,OV_THICK,OV_SLIDER_THICK);
            fl_move_object(GUI->overview->view_xy,OV_LEFT_FRAME_MARGIN,0);
           }

    if(fl_object_is_visible(GUI->overview->xz_frame)) {
                    fl_set_object_geometry(GUI->overview->xz_frame,0,OV_PAD+3,framew[PLANE_XZ],frameh[PLANE_XZ]);
            fl_set_object_position(GUI->overview->xz_zoom,3*OV_FRAME_LABEL_PAD,1);
            fl_set_object_position(GUI->overview->xz_tomain,framew[PLANE_XZ]-OV_TOMAIN_W-4*OV_FRAME_LABEL_PAD,1);
            fl_set_object_geometry(GUI->overview->xz_bit_orient,OV_FRAME_LABEL_PAD,0,18,18);
            fl_set_object_geometry(GUI->overview->xz_fb,OV_PAD,3*OV_PAD+3,framew[PLANE_XZ]-2*OV_PAD,frameh[PLANE_XZ]-4*OV_PAD-OV_SLIDER_THICK);
            fl_set_object_geometry(GUI->overview->xz_k,OV_PAD,frameh[PLANE_XZ]-OV_SLIDER_THICK+3,OV_LEFT_MARGIN,OV_SLIDER_THICK);
            fl_set_object_geometry(GUI->overview->xz_btn_dn,OV_PAD+OV_LEFT_MARGIN,frameh[PLANE_XZ]-OV_SLIDER_THICK+3,OV_THICK,OV_SLIDER_THICK);
            fl_set_object_geometry(GUI->overview->xz_slider,OV_PAD+OV_THICK+OV_LEFT_MARGIN,frameh[PLANE_XZ]-OV_SLIDER_THICK+3,framew[PLANE_XZ]-2*OV_PAD-2*OV_THICK-OV_LEFT_MARGIN,OV_SLIDER_THICK);
            fl_set_object_geometry(GUI->overview->xz_btn_up,framew[PLANE_XZ]-OV_PAD-OV_THICK,frameh[PLANE_XZ]-OV_SLIDER_THICK+3,OV_THICK,OV_SLIDER_THICK);
            fl_move_object(GUI->overview->view_xz,OV_LEFT_FRAME_MARGIN,0);
       
        if(fl_object_is_visible(GUI->overview->xy_frame)) {
            dx = GUI->overview->xy_frame->w + OV_PAD;
            fl_move_object(GUI->overview->view_xz,dx,0);
        }
    }

    if(fl_object_is_visible(GUI->overview->zy_frame)) {
                    fl_set_object_geometry(GUI->overview->zy_frame,0,OV_PAD+3,framew[PLANE_ZY],frameh[PLANE_ZY]);
            fl_set_object_position(GUI->overview->zy_zoom,3*OV_FRAME_LABEL_PAD,1);
            fl_set_object_position(GUI->overview->zy_tomain,framew[PLANE_ZY]-OV_TOMAIN_W-4*OV_FRAME_LABEL_PAD,1);
            fl_set_object_geometry(GUI->overview->zy_bit_orient,OV_FRAME_LABEL_PAD,0,18,18);
            fl_set_object_geometry(GUI->overview->zy_fb,OV_PAD,3*OV_PAD+3,framew[PLANE_ZY]-2*OV_PAD,frameh[PLANE_ZY]-4*OV_PAD-OV_SLIDER_THICK);
            fl_set_object_geometry(GUI->overview->zy_k,OV_PAD,frameh[PLANE_ZY]-OV_SLIDER_THICK+3,OV_LEFT_MARGIN,OV_SLIDER_THICK);
            fl_set_object_geometry(GUI->overview->zy_btn_dn,OV_PAD+OV_LEFT_MARGIN,frameh[PLANE_ZY]-OV_SLIDER_THICK+3,OV_THICK,OV_SLIDER_THICK);
            fl_set_object_geometry(GUI->overview->zy_slider,OV_PAD+OV_THICK+OV_LEFT_MARGIN,frameh[PLANE_ZY]-OV_SLIDER_THICK+3,framew[PLANE_ZY]-2*OV_PAD-2*OV_THICK-OV_LEFT_MARGIN,OV_SLIDER_THICK);
            fl_set_object_geometry(GUI->overview->zy_btn_up,framew[PLANE_ZY]-OV_PAD-OV_THICK,frameh[PLANE_ZY]-OV_SLIDER_THICK+3,OV_THICK,OV_SLIDER_THICK);
            fl_move_object(GUI->overview->view_zy,OV_LEFT_FRAME_MARGIN,0);
       
        dx = 0;
        if(fl_object_is_visible(GUI->overview->xy_frame))
            dx += GUI->overview->xy_frame->w + OV_PAD;
        if(fl_object_is_visible(GUI->overview->xz_frame))
            dx += GUI->overview->xz_frame->w + OV_PAD;
        if(dx > 0)
            fl_move_object(GUI->overview->view_zy,dx,0);
    }

    fl_unfreeze_form(GUI->overview->overview);

                if(fl_object_is_visible(GUI->overview->xy_fb)) {
            if(img_xy->w != view_xy->canvas->w ||
                    img_xy->h != view_xy->canvas->h) { /* if canvas size hasn't changed do nothing */
                fl_freeze_form(view_xy->viewport);
                fl_set_form_size(view_xy->viewport,2*OV_THICK+img_xy->w,2*OV_THICK+img_xy->h);
                fl_set_object_geometry(view_xy->canvas,OV_THICK,OV_THICK,img_xy->w,img_xy->h);
                fl_set_object_geometry(view_xy->pos_left,0,OV_THICK,OV_THICK,img_xy->h);
                fl_set_object_geometry(view_xy->pos_right,OV_THICK+img_xy->w,OV_THICK,OV_THICK,img_xy->h);
                fl_set_object_geometry(view_xy->pos_top,OV_THICK,0,img_xy->w,OV_THICK);
                fl_set_object_geometry(view_xy->pos_bottom,OV_THICK,OV_THICK+img_xy->h,img_xy->w,OV_THICK);
                fl_unfreeze_form(view_xy->viewport);
                fl_set_formbrowser_maxarea(GUI->overview->xy_fb,view_xy->viewport->w,view_xy->viewport->h);
                fl_redraw_object(GUI->overview->xy_fb);
                fl_set_formbrowser_xscroll(GUI->overview->xy_fb,xs);
                fl_set_formbrowser_yscroll(GUI->overview->xy_fb,ys);
                fl_redraw_object(GUI->overview->xy_fb);
            }
        }
           if(fl_object_is_visible(GUI->overview->xz_fb)) {
            if(img_xz->w != view_xz->canvas->w ||
                    img_xz->h != view_xz->canvas->h) { /* if canvas size hasn't changed do nothing */
                fl_freeze_form(view_xz->viewport);
                fl_set_form_size(view_xz->viewport,2*OV_THICK+img_xz->w,2*OV_THICK+img_xz->h);
                fl_set_object_geometry(view_xz->canvas,OV_THICK,OV_THICK,img_xz->w,img_xz->h);
                fl_set_object_geometry(view_xz->pos_left,0,OV_THICK,OV_THICK,img_xz->h);
                fl_set_object_geometry(view_xz->pos_right,OV_THICK+img_xz->w,OV_THICK,OV_THICK,img_xz->h);
                fl_set_object_geometry(view_xz->pos_top,OV_THICK,0,img_xz->w,OV_THICK);
                fl_set_object_geometry(view_xz->pos_bottom,OV_THICK,OV_THICK+img_xz->h,img_xz->w,OV_THICK);
                fl_unfreeze_form(view_xz->viewport);
                fl_set_formbrowser_maxarea(GUI->overview->xz_fb,view_xz->viewport->w,view_xz->viewport->h);
                fl_redraw_object(GUI->overview->xz_fb);
                fl_set_formbrowser_xscroll(GUI->overview->xz_fb,xs);
                fl_set_formbrowser_yscroll(GUI->overview->xz_fb,ys);
                fl_redraw_object(GUI->overview->xz_fb);
            }
        }
           if(fl_object_is_visible(GUI->overview->zy_fb)) {
            if(img_zy->w != view_zy->canvas->w ||
                    img_zy->h != view_zy->canvas->h) { /* if canvas size hasn't changed do nothing */
                fl_freeze_form(view_zy->viewport);
                fl_set_form_size(view_zy->viewport,2*OV_THICK+img_zy->w,2*OV_THICK+img_zy->h);
                fl_set_object_geometry(view_zy->canvas,OV_THICK,OV_THICK,img_zy->w,img_zy->h);
                fl_set_object_geometry(view_zy->pos_left,0,OV_THICK,OV_THICK,img_zy->h);
                fl_set_object_geometry(view_zy->pos_right,OV_THICK+img_zy->w,OV_THICK,OV_THICK,img_zy->h);
                fl_set_object_geometry(view_zy->pos_top,OV_THICK,0,img_zy->w,OV_THICK);
                fl_set_object_geometry(view_zy->pos_bottom,OV_THICK,OV_THICK+img_zy->h,img_zy->w,OV_THICK);
                fl_unfreeze_form(view_zy->viewport);
                fl_set_formbrowser_maxarea(GUI->overview->zy_fb,view_zy->viewport->w,view_zy->viewport->h);
                fl_redraw_object(GUI->overview->zy_fb);
                fl_set_formbrowser_xscroll(GUI->overview->zy_fb,xs);
                fl_set_formbrowser_yscroll(GUI->overview->zy_fb,ys);
                fl_redraw_object(GUI->overview->zy_fb);
            }
        }
   }
#define ENV _default_env
#undef XY_FRAME_W
#undef XZ_FRAME_W
#undef ZY_FRAME_W
#undef XY_FRAME_H
#undef XZ_FRAME_H
#undef ZY_FRAME_H

void cb_ov_units ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    fl_set_object_label(obj,fl_get_button(obj) ? "CAL" : "DAT");
}

#undef ENV
static void _update_ov_status(xg3d_env *ENV, enum g3d_slice_plane plane) {
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
    const xg3d_data *data_overlay = ((xg3d_env*)OVERLAY_ENV(GUI->overlay))->data->main;
    const grid3d *doserrslice = VIEW_SLICE_LAYER(ov_viewports[plane],LAYER_DOSE_ERR);
    const grid3d *overslice = VIEW_SLICE_LAYER(ov_viewports[plane],LAYER_OVERLAY);
    const grid3d *doseslice = VIEW_SLICE_LAYER(ov_viewports[plane],LAYER_DOSE);
    const grid3d *prevslice = VIEW_SLICE_LAYER(ov_viewports[plane],LAYER_PREVIEW);
    const Window win = FL_ObjWin(VIEW_CANVAS(ov_viewports[plane]));
    const xg3d_header *hdr = VIEW_DATA(ov_viewports[plane])->hdr;
    const grid3d *slice = VIEW_SLICE(ov_viewports[plane]);
    const coord_t k = VIEW_K(ov_viewports[plane]);
    const coord_t ox = hdr->ox;
    const coord_t oy = hdr->oy;
    const coord_t oz = hdr->oz;

    const coord_t *dj,*di,*dk;
    int flag_calib = false;
    const char *ulabel = "";
    const coord_t *vox[3];
    unsigned int keymask;
    FL_Coord x,y,w,h;
    coord_t j=0,i=0;
    int ofx=0,ofy=0;
    double vx,vy,vz;

    if(fl_get_button(GUI->overview->btn_uval)) {
        flag_calib = true;
        ulabel = fl_get_input(GUI->calibration->inp_ulabel);
    }

    switch(fl_get_choice(GUI->overview->ch_ulen)) {
        default:
        case 1: /* vox */
            vx = vy = vz = 1.0;
            break;
        case 2: /* cm */
            vx = hdr->vx;
            vy = hdr->vy;
            vz = hdr->vz;
            break;
        case 3: /* mm */
            vx = hdr->vx * CM_TO_MM;
            vy = hdr->vy * CM_TO_MM;
            vz = hdr->vz * CM_TO_MM;
            break;
    }

    ofx = OVERLAY_POS(GUI->overlay,PLANE_TO_J(plane));
    ofy = OVERLAY_POS(GUI->overlay,PLANE_TO_I(plane));
    vox[0] = &j; vox[1] = &i; vox[2] = &k;
    dj = vox[PLANE_TO_J(plane)];
    di = vox[PLANE_TO_I(plane)];
    dk = vox[PLANE_TO_K(plane)];

    fl_get_win_mouse(win,&x,&y,&keymask);
    fl_get_winsize(win,&w,&h);
    if(x >= 0 && x < w && y >= 0 && y < h) {
        win_to_grid(ov_viewports[plane],x,y,&j,&i);
        if(doseslice != NULL)
            fl_set_object_label_f(GUI->overview->status,
                    "x: %6.4g y: %6.4g z: %6.4g | %s: %.8g%s | %s: %.8g Gy ± %g",
                    vx * ((int)*dj - (int)ox),vy * ((int)*di - (int)oy),vz * ((int)*dk - (int)oz),_("value"),ulabel,
                    flag_calib ? CALIBDVAL(hdr,slice,j+i*slice->nx) : g3d_get_dvalue(slice,j+i*slice->nx),
                    _("dose"),g3d_get_dvalue(doseslice,j + i*doseslice->nx),
                    doserrslice != NULL ? g3d_get_dvalue(doserrslice,j + i*doserrslice->nx) : NAN);
        else if(fl_get_button(GUI->overlay->btn_show) && overslice != NULL &&
                ((int)j-ofx) >= 0 && ((int)j-ofx) < (int)overslice->nx &&
                ((int)i-ofy) >= 0 && ((int)i-ofy) < (int)overslice->ny)
            fl_set_object_label_f(GUI->overview->status,
                    "x: %6.4g y: %6.4g z: %6.4g | %s: %.8g%s | %s: %.8g",
                    vx * ((int)*dj - (int)ox),vy * ((int)*di - (int)oy),vz * ((int)*dk - (int)oz),_("value"),ulabel,
                    flag_calib ? CALIBDVAL(hdr,slice,j+i*slice->nx) : g3d_get_dvalue(slice,j+i*slice->nx),
                    _("overlay"),flag_calib ? CALIBDVAL(data_overlay->hdr,overslice,(j-ofx) + (i-ofy)*overslice->nx) :
                    g3d_get_dvalue(overslice,(j-ofx) + (i-ofy)*overslice->nx));
        else if(fl_get_button(GUI->operations->btn_rot_preview))
            fl_set_object_label_f(GUI->overview->status,
                    "x: %6.4g y: %6.4g z: %6.4g | %s: %.8g%s",
                    vx * ((int)*dj - (int)ox),vy * ((int)*di - (int)oy),vz * ((int)*dk - (int)oz),_("value"),ulabel,
                    flag_calib ? CALIBDVAL(hdr,prevslice,j+i*prevslice->nx) : g3d_get_dvalue(prevslice,j+i*prevslice->nx));
        else
            fl_set_object_label_f(GUI->overview->status,
                    "x: %6.4g y: %6.4g z: %6.4g | %s: %.8g%s",
                    vx * ((int)*dj - (int)ox),vy * ((int)*di - (int)oy),vz * ((int)*dk - (int)oz),_("value"),ulabel,
                    flag_calib ? CALIBDVAL(hdr,slice,j+i*slice->nx) : g3d_get_dvalue(slice,j+i*slice->nx));
    }
}
#define ENV _default_env

static void _set_ov_orient_bitmap(FD_viewport *view, FD_overview *overview) {
    FL_OBJECT *bit_orient;

        
    enum g3d_slice_plane plane = VIEW_PLANE(view);
    switch(plane) {
        case PLANE_XY:         bit_orient = overview->xy_bit_orient;
        switch(VIEW_ORIENT(view)) {
            case ORIENT_0:            fl_set_bitmap_data(bit_orient,orient_XY0_s_width,orient_XY0_s_height,orient_XY0_s_bits);
    break;
            case ORIENT_90:           fl_set_bitmap_data(bit_orient,orient_XY1_s_width,orient_XY1_s_height,orient_XY1_s_bits);
    break;
            case ORIENT_180:          fl_set_bitmap_data(bit_orient,orient_XY2_s_width,orient_XY2_s_height,orient_XY2_s_bits);
    break;
            case ORIENT_270:          fl_set_bitmap_data(bit_orient,orient_XY3_s_width,orient_XY3_s_height,orient_XY3_s_bits);
    break;
            case ORIENT_F0:           fl_set_bitmap_data(bit_orient,orient_XY4_s_width,orient_XY4_s_height,orient_XY4_s_bits);
    break;
            case ORIENT_F90:          fl_set_bitmap_data(bit_orient,orient_XY5_s_width,orient_XY5_s_height,orient_XY5_s_bits);
    break;
            case ORIENT_F180:         fl_set_bitmap_data(bit_orient,orient_XY6_s_width,orient_XY6_s_height,orient_XY6_s_bits);
    break;
            case ORIENT_F270:         fl_set_bitmap_data(bit_orient,orient_XY7_s_width,orient_XY7_s_height,orient_XY7_s_bits);
    break;
        }
    break;
        case PLANE_XZ:         bit_orient = overview->xz_bit_orient;
        switch(VIEW_ORIENT(view)) {
            case ORIENT_0:            fl_set_bitmap_data(bit_orient,orient_XZ0_s_width,orient_XZ0_s_height,orient_XZ0_s_bits);
    break;
            case ORIENT_90:           fl_set_bitmap_data(bit_orient,orient_XZ1_s_width,orient_XZ1_s_height,orient_XZ1_s_bits);
    break;
            case ORIENT_180:          fl_set_bitmap_data(bit_orient,orient_XZ2_s_width,orient_XZ2_s_height,orient_XZ2_s_bits);
    break;
            case ORIENT_270:          fl_set_bitmap_data(bit_orient,orient_XZ3_s_width,orient_XZ3_s_height,orient_XZ3_s_bits);
    break;
            case ORIENT_F0:           fl_set_bitmap_data(bit_orient,orient_XZ4_s_width,orient_XZ4_s_height,orient_XZ4_s_bits);
    break;
            case ORIENT_F90:          fl_set_bitmap_data(bit_orient,orient_XZ5_s_width,orient_XZ5_s_height,orient_XZ5_s_bits);
    break;
            case ORIENT_F180:         fl_set_bitmap_data(bit_orient,orient_XZ6_s_width,orient_XZ6_s_height,orient_XZ6_s_bits);
    break;
            case ORIENT_F270:         fl_set_bitmap_data(bit_orient,orient_XZ7_s_width,orient_XZ7_s_height,orient_XZ7_s_bits);
    break;
        }
    break;
        case PLANE_ZY:         bit_orient = overview->zy_bit_orient;
        switch(VIEW_ORIENT(view)) {
            case ORIENT_0:            fl_set_bitmap_data(bit_orient,orient_ZY0_s_width,orient_ZY0_s_height,orient_ZY0_s_bits);
    break;
            case ORIENT_90:           fl_set_bitmap_data(bit_orient,orient_ZY1_s_width,orient_ZY1_s_height,orient_ZY1_s_bits);
    break;
            case ORIENT_180:          fl_set_bitmap_data(bit_orient,orient_ZY2_s_width,orient_ZY2_s_height,orient_ZY2_s_bits);
    break;
            case ORIENT_270:          fl_set_bitmap_data(bit_orient,orient_ZY3_s_width,orient_ZY3_s_height,orient_ZY3_s_bits);
    break;
            case ORIENT_F0:           fl_set_bitmap_data(bit_orient,orient_ZY4_s_width,orient_ZY4_s_height,orient_ZY4_s_bits);
    break;
            case ORIENT_F90:          fl_set_bitmap_data(bit_orient,orient_ZY5_s_width,orient_ZY5_s_height,orient_ZY5_s_bits);
    break;
            case ORIENT_F180:         fl_set_bitmap_data(bit_orient,orient_ZY6_s_width,orient_ZY6_s_height,orient_ZY6_s_bits);
    break;
            case ORIENT_F270:         fl_set_bitmap_data(bit_orient,orient_ZY7_s_width,orient_ZY7_s_height,orient_ZY7_s_bits);
    break;
        }
    break;
        default:
            fprintf(stderr,"%s: invalid g3d_slice_plane value passed: %d",__func__,plane);
            abort();
    }
}

void cb_tomain ( FL_OBJECT * obj  , long data  ) {
    const enum g3d_slice_plane plane = data;
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
    const enum xg3d_orientation orient = VIEW_ORIENT(ov_viewports[plane]);
    FL_OBJECT *const xyz_buttons[3] = {GUI->mainview->planeXY,
                                       GUI->mainview->planeXZ,
                                       GUI->mainview->planeZY};
    const unsigned int zoom = VIEW_ZOOM_X(ov_viewports[plane]); // X or Y is the same for overview
    FL_OBJECT *button = xyz_buttons[plane];

    VIEW_ZOOM_X(GUI->viewport) = zoom;
    VIEW_ZOOM_Y(GUI->viewport) = zoom;
    VIEW_ORIENT(GUI->viewport) = orient;
    MAINVIEW_STORE_ORIENT(GUI->mainview,plane,orient); /* store mainview orient */
    fl_set_button(GUI->mainview->flip,orient_flip_check(VIEW_ORIENT(GUI->viewport)));
    fl_set_object_label_f(GUI->mainview->txt_zoom,"%ux",zoom);
    fl_freeze_form(GUI->mainview->mainview);
    fl_set_button(button,1); fl_call_object_callback(button);
    fl_set_slider_value(GUI->mainview->slider,VIEW_K(ov_viewports[plane])); /* set k from overview */
    fl_call_object_callback(GUI->mainview->slider);
    fl_unfreeze_form(GUI->mainview->mainview);
    fl_raise_form(GUI->mainview->mainview);
}

void cb_ov_view_op ( FL_OBJECT * obj  , long data  ) {
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
    static FL_POPUP *popup_orient = NULL;
    static FL_POPUP *popup_zoom = NULL;

    static FL_POPUP_ITEM items_orient[] = {
            {"XY",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
           {"XZ",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
           {"ZY",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
           {NULL,NULL,NULL,0,0}
    };

    static FL_POPUP_ITEM items_zoom[] = {
            {"XY",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
           {"XZ",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
           {"ZY",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
           {NULL,NULL,NULL,0,0}
    };

    if(popup_orient == NULL) {
        popup_orient = fl_popup_create(None,"",items_orient);
        fl_popup_set_policy(popup_orient,FL_POPUP_DRAG_SELECT);
        fl_popup_set_color(popup_orient,FL_POPUP_HIGHLIGHT_COLOR,FL_YELLOW);
        fl_popup_set_color(popup_orient,FL_POPUP_HIGHLIGHT_TEXT_COLOR,FL_BLACK);

                {
            FL_POPUP_ENTRY *entry = fl_popup_entry_get_by_position(popup_orient,0);
            fl_popup_entry_set_text(entry,_("XY"));
            fl_popup_entry_set_value(entry,PLANE_XY);
        }
               {
            FL_POPUP_ENTRY *entry = fl_popup_entry_get_by_position(popup_orient,1);
            fl_popup_entry_set_text(entry,_("XZ"));
            fl_popup_entry_set_value(entry,PLANE_XZ);
        }
               {
            FL_POPUP_ENTRY *entry = fl_popup_entry_get_by_position(popup_orient,2);
            fl_popup_entry_set_text(entry,_("ZY"));
            fl_popup_entry_set_value(entry,PLANE_ZY);
        }
           }

    if(popup_zoom == NULL) {
        popup_zoom = fl_popup_create(None,"",items_zoom);
        fl_popup_set_policy(popup_zoom,FL_POPUP_DRAG_SELECT);
        fl_popup_set_color(popup_zoom,FL_POPUP_HIGHLIGHT_COLOR,FL_YELLOW);
        fl_popup_set_color(popup_zoom,FL_POPUP_HIGHLIGHT_TEXT_COLOR,FL_BLACK);

                {
            FL_POPUP_ENTRY *entry = fl_popup_entry_get_by_position(popup_zoom,0);
            fl_popup_entry_set_text(entry,_("XY"));
            fl_popup_entry_set_value(entry,PLANE_XY);
        }
               {
            FL_POPUP_ENTRY *entry = fl_popup_entry_get_by_position(popup_zoom,1);
            fl_popup_entry_set_text(entry,_("XZ"));
            fl_popup_entry_set_value(entry,PLANE_XZ);
        }
               {
            FL_POPUP_ENTRY *entry = fl_popup_entry_get_by_position(popup_zoom,2);
            fl_popup_entry_set_text(entry,_("ZY"));
            fl_popup_entry_set_value(entry,PLANE_ZY);
        }
           }

            {
        FL_POPUP_ENTRY *entry_orient = fl_popup_entry_get_by_value(popup_orient,PLANE_XY);
        FL_POPUP_ENTRY *entry_zoom = fl_popup_entry_get_by_value(popup_zoom,PLANE_XY);
        if(fl_get_button(GUI->overview->planeXY)) {
            fl_popup_entry_clear_state(entry_orient,FL_POPUP_DISABLED);
            fl_popup_entry_clear_state(entry_zoom,FL_POPUP_DISABLED);
        } else {
            fl_popup_entry_raise_state(entry_orient,FL_POPUP_DISABLED);
            fl_popup_entry_raise_state(entry_zoom,FL_POPUP_DISABLED);
        }
    }
       {
        FL_POPUP_ENTRY *entry_orient = fl_popup_entry_get_by_value(popup_orient,PLANE_XZ);
        FL_POPUP_ENTRY *entry_zoom = fl_popup_entry_get_by_value(popup_zoom,PLANE_XZ);
        if(fl_get_button(GUI->overview->planeXZ)) {
            fl_popup_entry_clear_state(entry_orient,FL_POPUP_DISABLED);
            fl_popup_entry_clear_state(entry_zoom,FL_POPUP_DISABLED);
        } else {
            fl_popup_entry_raise_state(entry_orient,FL_POPUP_DISABLED);
            fl_popup_entry_raise_state(entry_zoom,FL_POPUP_DISABLED);
        }
    }
       {
        FL_POPUP_ENTRY *entry_orient = fl_popup_entry_get_by_value(popup_orient,PLANE_ZY);
        FL_POPUP_ENTRY *entry_zoom = fl_popup_entry_get_by_value(popup_zoom,PLANE_ZY);
        if(fl_get_button(GUI->overview->planeZY)) {
            fl_popup_entry_clear_state(entry_orient,FL_POPUP_DISABLED);
            fl_popup_entry_clear_state(entry_zoom,FL_POPUP_DISABLED);
        } else {
            fl_popup_entry_raise_state(entry_orient,FL_POPUP_DISABLED);
            fl_popup_entry_raise_state(entry_zoom,FL_POPUP_DISABLED);
        }
    }
   
    switch(data) {
        case 0: /* rotate CCW */
        case 1: /* rotate CW */
        {
            FL_OBJECT *button = data == 0 ? GUI->overview->rotateCCW : GUI->overview->rotateCW;

            FL_POPUP *popup = popup_orient;
            fl_popup_set_title(popup,data == 0 ? _("Rotate 90° CCW") : _("Rotate 90° CW"));
            fl_popup_set_position(popup,button->form->x+button->x+button->w,button->form->y+button->y);
            FL_POPUP_RETURN *retpop = fl_popup_do(popup); if(retpop == NULL) return;
            assert(retpop->val >= 0 && retpop->val <= 2);

            enum g3d_slice_plane plane = retpop->val;
            enum xg3d_orientation orient = VIEW_ORIENT(ov_viewports[plane]);

            int w = ((FL_IMAGE*)VIEW_IMAGE(ov_viewports[plane]))->w;
            int h = ((FL_IMAGE*)VIEW_IMAGE(ov_viewports[plane]))->h;
            if(data == 0) {
                VIEW_ORIENT(ov_viewports[plane]) = orient_flip_check(orient) ?
                        (orient - 1) | ORIENT_FLIP :
                        (orient + 1) &~ ORIENT_FLIP;
                flimage_rotate(VIEW_IMAGE(ov_viewports[plane]),900,FLIMAGE_NOSUBPIXEL);
            } else {
                VIEW_ORIENT(ov_viewports[plane]) = orient_flip_check(orient) ?
                        ((orient &~ ORIENT_FLIP) + 1) | ORIENT_FLIP :
                        ((orient | ORIENT_FLIP) - 1) &~ ORIENT_FLIP;
                flimage_rotate(VIEW_IMAGE(ov_viewports[plane]),-900,FLIMAGE_NOSUBPIXEL);
            }
            _set_ov_orient_bitmap(ov_viewports[plane],GUI->overview);

            if(w != ((FL_IMAGE*)VIEW_IMAGE(ov_viewports[plane]))->w ||
                    h != ((FL_IMAGE*)VIEW_IMAGE(ov_viewports[plane]))->h)
                resize_ov_canvases(ENV);
            else
                refresh_ov_canvas(ENV,plane);
        }
            break;
        case 2: /* flip */
        {
            FL_OBJECT *button = GUI->overview->flip;

            FL_POPUP *popup = popup_orient;
            fl_popup_set_title(popup,_("Flip"));
            fl_popup_set_position(popup,button->form->x+button->x+button->w,button->form->y+button->y);
            FL_POPUP_RETURN *retpop = fl_popup_do(popup); if(retpop == NULL) return;
            assert(retpop->val >= 0 && retpop->val <= 2);

            enum g3d_slice_plane plane = retpop->val;
            enum xg3d_orientation orient = VIEW_ORIENT(ov_viewports[plane]);

            VIEW_ORIENT(ov_viewports[plane]) = (orient + ORIENT_FLIP) &~ (ORIENT_FLIP << 1);
            flimage_flip(VIEW_IMAGE(ov_viewports[plane]),'r');
            _set_ov_orient_bitmap(ov_viewports[plane],GUI->overview);
            refresh_ov_canvas(ENV,plane);
        }
            break;
        case 3: /* zoom in */
        case 4: /* zoom out */
        {
            long mbtn = fl_mouse_button();
            int incr = 1 - 2 * (data - 3); // 3 -> 1; 4 -> -1;

            FL_OBJECT *const xyz_zoom[3] = {GUI->overview->xy_zoom,
                                            GUI->overview->xz_zoom,
                                            GUI->overview->zy_zoom};

            if(mbtn == 1) { // left mouse button, zoom all
                                                if(fl_get_button(GUI->overview->planeXY)) {
                                    {
                    FD_viewport *viewport = ov_viewports[PLANE_XY];
                    FL_OBJECT *btn_zoom = xyz_zoom[PLANE_XY];
                    unsigned int zoom = VIEW_ZOOM_X(viewport); // X or Y is the same for overview
                    zoom += incr;
                                        zoom = FL_clamp(zoom,1,OVC_MAX_ZOOM);

                    if(zoom != VIEW_ZOOM_X(viewport)) {
                        VIEW_ZOOM_X(viewport) = VIEW_ZOOM_Y(viewport) = zoom; // store the same value
                        fl_set_object_label_f(btn_zoom,"%ux",zoom);
                        replace_ov_img(viewport,GUI->quantization,LAYER_BASE);
                    }
                               }
                               }
                if(fl_get_button(GUI->overview->planeXZ)) {
                                    {
                    FD_viewport *viewport = ov_viewports[PLANE_XZ];
                    FL_OBJECT *btn_zoom = xyz_zoom[PLANE_XZ];
                    unsigned int zoom = VIEW_ZOOM_X(viewport); // X or Y is the same for overview
                    zoom += incr;
                                        zoom = FL_clamp(zoom,1,OVC_MAX_ZOOM);

                    if(zoom != VIEW_ZOOM_X(viewport)) {
                        VIEW_ZOOM_X(viewport) = VIEW_ZOOM_Y(viewport) = zoom; // store the same value
                        fl_set_object_label_f(btn_zoom,"%ux",zoom);
                        replace_ov_img(viewport,GUI->quantization,LAYER_BASE);
                    }
                               }
                               }
                if(fl_get_button(GUI->overview->planeZY)) {
                                    {
                    FD_viewport *viewport = ov_viewports[PLANE_ZY];
                    FL_OBJECT *btn_zoom = xyz_zoom[PLANE_ZY];
                    unsigned int zoom = VIEW_ZOOM_X(viewport); // X or Y is the same for overview
                    zoom += incr;
                                        zoom = FL_clamp(zoom,1,OVC_MAX_ZOOM);

                    if(zoom != VIEW_ZOOM_X(viewport)) {
                        VIEW_ZOOM_X(viewport) = VIEW_ZOOM_Y(viewport) = zoom; // store the same value
                        fl_set_object_label_f(btn_zoom,"%ux",zoom);
                        replace_ov_img(viewport,GUI->quantization,LAYER_BASE);
                    }
                               }
                               }
            } else if(mbtn == 3) { // right mouse button, popup
                FL_POPUP *popup = popup_zoom;
                FL_OBJECT *button = incr > 0 ? GUI->overview->zoomIn : GUI->overview->zoomOut;
                fl_popup_set_title(popup,incr > 0 ? _("Zoom in") : _("Zoom out"));
                fl_popup_set_position(popup,button->form->x+button->x+button->w,button->form->y+button->y);
                FL_POPUP_RETURN *retpop = fl_popup_do(popup); if(retpop == NULL) return;
                assert(retpop->val >= 0 && retpop->val <= 2);

                enum g3d_slice_plane plane = retpop->val;
                                {
                    FD_viewport *viewport = ov_viewports[plane];
                    FL_OBJECT *btn_zoom = xyz_zoom[plane];
                    unsigned int zoom = VIEW_ZOOM_X(viewport); // X or Y is the same for overview
                    zoom += incr;
                                        zoom = FL_clamp(zoom,1,OVC_MAX_ZOOM);

                    if(zoom != VIEW_ZOOM_X(viewport)) {
                        VIEW_ZOOM_X(viewport) = VIEW_ZOOM_Y(viewport) = zoom; // store the same value
                        fl_set_object_label_f(btn_zoom,"%ux",zoom);
                        replace_ov_img(viewport,GUI->quantization,LAYER_BASE);
                    }
                               }
                           }
            resize_ov_canvases(ENV);
        }
            break;
    }
    update_all_positioners(ov_viewports,GUI->overview);
}

void cb_ov_setzoom ( FL_OBJECT * obj  , long data  ) {
    const enum g3d_slice_plane plane = data;
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
    FL_OBJECT *const btns_zoom[3] = {GUI->overview->xy_zoom,
                                     GUI->overview->xz_zoom,
                                     GUI->overview->zy_zoom};
    static const char *plane_names[3] = {"XY","XZ","ZY"};
    FD_viewport *viewport = ov_viewports[plane];
    unsigned int zoom = VIEW_ZOOM_X(viewport); // X or Y is the same for overview
    FL_OBJECT *btn_zoom = btns_zoom[plane];
    char def[8],buf[64];

    snprintf(def,sizeof(def),"%u",zoom);
    snprintf(buf,sizeof(buf),"%s (%s):",_("Set zoom level"),plane_names[plane]);
    const char *txt = fl_show_input(buf,def);
    if(txt == NULL || strlen(txt) == 0) return;
    zoom = strtoul(txt,NULL,10);

                        zoom = FL_clamp(zoom,1,OVC_MAX_ZOOM);

                    if(zoom != VIEW_ZOOM_X(viewport)) {
                        VIEW_ZOOM_X(viewport) = VIEW_ZOOM_Y(viewport) = zoom; // store the same value
                        fl_set_object_label_f(btn_zoom,"%ux",zoom);
                        replace_ov_img(viewport,GUI->quantization,LAYER_BASE);
                    }
                   resize_ov_canvases(ENV);
    update_all_positioners(ov_viewports,GUI->overview);
}

void cb_ov_view ( FL_OBJECT * obj  , long data  ) {
    const enum g3d_slice_plane plane = data;
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);

    
    switch(plane) {
        case PLANE_XY:         if(fl_get_button(obj)) {
            fl_show_object(GUI->overview->view_xy); fl_show_object(GUI->overview->xy_fb);
            fl_set_cursor(FL_ObjWin(VIEW_CANVAS(ov_viewports[PLANE_XY])),cursor);
            fl_remove_selected_xevent(FL_ObjWin(VIEW_CANVAS(ov_viewports[PLANE_XY])),PointerMotionHintMask);
            resize_ov_canvases(ENV);
            refresh_ov_canvas(ENV,data);
            update_all_positioners(ov_viewports,GUI->overview);
        } else {
            if(!fl_get_button(GUI->overview->planeXZ)
                    && !fl_get_button(GUI->overview->planeZY))
                fl_set_button(obj,1);
            else {
                fl_hide_object(GUI->overview->view_xy);
                resize_ov_canvases(ENV);
            }
        }
    break;
        case PLANE_XZ:         if(fl_get_button(obj)) {
            fl_show_object(GUI->overview->view_xz); fl_show_object(GUI->overview->xz_fb);
            fl_set_cursor(FL_ObjWin(VIEW_CANVAS(ov_viewports[PLANE_XZ])),cursor);
            fl_remove_selected_xevent(FL_ObjWin(VIEW_CANVAS(ov_viewports[PLANE_XZ])),PointerMotionHintMask);
            resize_ov_canvases(ENV);
            refresh_ov_canvas(ENV,data);
            update_all_positioners(ov_viewports,GUI->overview);
        } else {
            if(!fl_get_button(GUI->overview->planeZY)
                    && !fl_get_button(GUI->overview->planeXY))
                fl_set_button(obj,1);
            else {
                fl_hide_object(GUI->overview->view_xz);
                resize_ov_canvases(ENV);
            }
        }
    break;
        case PLANE_ZY:         if(fl_get_button(obj)) {
            fl_show_object(GUI->overview->view_zy); fl_show_object(GUI->overview->zy_fb);
            fl_set_cursor(FL_ObjWin(VIEW_CANVAS(ov_viewports[PLANE_ZY])),cursor);
            fl_remove_selected_xevent(FL_ObjWin(VIEW_CANVAS(ov_viewports[PLANE_ZY])),PointerMotionHintMask);
            resize_ov_canvases(ENV);
            refresh_ov_canvas(ENV,data);
            update_all_positioners(ov_viewports,GUI->overview);
        } else {
            if(!fl_get_button(GUI->overview->planeXY)
                    && !fl_get_button(GUI->overview->planeXZ))
                fl_set_button(obj,1);
            else {
                fl_hide_object(GUI->overview->view_zy);
                resize_ov_canvases(ENV);
            }
        }
    break;
        default:
            fprintf(stderr,"%s: invalid g3d_slice_plane value passed: %d",__func__,plane);
            abort();
    }
}

void cb_ov_slibtn ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *slider;

    if(data == 0 || data == 1)
        slider = GUI->overview->xy_slider;
    else if(data == 2 || data == 3)
        slider = GUI->overview->xz_slider;
    else
        slider = GUI->overview->zy_slider;

    fl_set_slider_value(slider,fl_get_slider_value(slider) + (data % 2 == 0) ? -1.0 : 1.0);
    fl_call_object_callback(slider);
}

void cb_ov_slider ( FL_OBJECT * obj  , long data  ) {
    const enum g3d_slice_plane plane = data;
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
    xg3d_env *env_overlay = OVERLAY_ENV(GUI->overlay);
    FL_OBJECT *const xyz_k[3] = {GUI->overview->xy_k,
                                 GUI->overview->xz_k,
                                 GUI->overview->zy_k};
    const coord_t newk = fl_get_slider_value(obj);
    long int k;

    VIEW_K(ov_viewports[plane]) = newk;
    fl_set_object_label_f(xyz_k[plane],"%u",newk);
    if(fl_get_button(GUI->operations->btn_rot_preview)) {
        VIEW_K_LAYER(ov_viewports[plane],LAYER_PREVIEW) = newk;
        replace_ov_slice(ov_viewports[plane],LAYER_PREVIEW);
        replace_ov_quantslice(ov_viewports[plane],GUI->quantization,LAYER_PREVIEW);
        replace_ov_img(ov_viewports[plane],GUI->quantization,LAYER_PREVIEW);
    } else {
        replace_ov_slice(ov_viewports[plane],LAYER_BASE);
        replace_ov_quantslice(ov_viewports[plane],GUI->quantization,LAYER_BASE);
        replace_ov_img(ov_viewports[plane],GUI->quantization,LAYER_BASE);
        if(fl_get_button(GUI->overlay->btn_show)) {
            k = (int)newk - OVERLAY_POS(GUI->overlay,PLANE_TO_K(plane));
            VIEW_K_LAYER(ov_viewports[plane],LAYER_OVERLAY) = (k < 0) ? -1 : k;
            replace_ov_slice(ov_viewports[plane],LAYER_OVERLAY);
            replace_ov_quantslice(ov_viewports[plane],env_overlay->gui->quantization,LAYER_OVERLAY);
        }
        if(ENV->data->dose != NULL) {
            VIEW_K_LAYER(ov_viewports[plane],LAYER_DOSE) = newk;
            replace_ov_slice(ov_viewports[plane],LAYER_DOSE);
        }
        if(ENV->data->dose_err != NULL) {
            VIEW_K_LAYER(ov_viewports[plane],LAYER_DOSE_ERR) = newk;
            replace_ov_slice(ov_viewports[plane],LAYER_DOSE_ERR);
        }
    }
    _update_positioners(ov_viewports,GUI->overview,plane);
    refresh_ov_canvas(ENV,plane);
}

void cb_ov_setk ( FL_OBJECT * obj  , long data  ) {
    const enum g3d_slice_plane plane = data;
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
    FL_OBJECT *const xyz_slider[3] = {GUI->overview->xy_slider,
                                      GUI->overview->xz_slider,
                                      GUI->overview->zy_slider};
    static const char *plane_names[3] = {"XY","XZ","ZY"};
    FL_OBJECT *const slider = xyz_slider[plane];
    int k = VIEW_K(ov_viewports[plane]);
    char def[8],buf[64];

    snprintf(def,sizeof(def),"%d",k);
    snprintf(buf,sizeof(buf),"%s (%s):",_("Go to slice"),plane_names[plane]);
    const char *txt = fl_show_input(buf,def);
    if(txt == NULL || strlen(txt) == 0) return;

    if(*txt == '+' || *txt == '-')
        k += atoi(txt);
    else
        k = atoi(txt);

    fl_set_slider_value(slider,k);
    fl_call_object_callback(slider);
}

void cb_overview_refresh ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    refresh_ov_canvases(GUI->overview);
}

void cb_overview_showhide ( FL_OBJECT * obj  , long data  ) {
    const int button = fl_get_button_numb(obj);
    FL_FORM *form = NULL;
    char buf[1024];

    switch(data) {
        case 0: /* Overlay */
            form = GUI->overlay->overlay;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Overlay"));
            break;
        case 1: /* Contours */
            form = GUI->contour->contour;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Contours"));
            break;
    }

    if(button == FL_RIGHT_MOUSE && fl_form_is_visible(form)) {
        fl_set_button(obj,1);
        fl_raise_form(form);
        fl_winfocus(form->window);
        return;
    }

    if(fl_form_is_visible(form)) {
        fl_hide_form(form);
        fl_deactivate_form(form);
        return;
    } else {
        fl_activate_form(form);
        fl_show_form(form,FL_PLACE_SIZE,FL_FULLBORDER,buf);
        fl_raise_form(form);
        fl_winfocus(form->window);
    }
}

static void _dummy_free_text(FL_IMAGE *im FL_UNUSED_ARG) { ; }

static void _flimage_render_markers(FL_IMAGE *im, const FL_WINDOW win) {
    const int old_dont_display_text = im->dont_display_text;
    void (*old_free_text)(FL_IMAGE*) = im->free_text;

    im->dont_display_text = 1;
    im->free_text = _dummy_free_text;
    flimage_render_annotation(im,win);
    im->free_text = old_free_text;
    im->dont_display_text = old_dont_display_text;
}

#define RAD_TO_DECI_ANG (1800.0 / M_PI)
void _draw_contline_vector(const double x1, const double y1, const double x2, const double y2, const unsigned int k, void *p) {
    FD_viewport *view = (FD_viewport*)(((void**)p)[0]);
    const FL_PACKED *lut = (FL_PACKED*)(((void**)p)[1]);
    FILE *fp = (FILE*)(((void**)p)[2]);
    const FL_IMAGE *img = VIEW_IMAGE(view);
    const float r = FL_GETR(lut[k]) / 255.0,
                g = FL_GETG(lut[k]) / 255.0,
                b = FL_GETB(lut[k]) / 255.0;
    double wx1 = 0.0, wy1 = 0.0, wx2 = 0.0, wy2 = 0.0;

    ccoord_to_winf(view,&wx1,&wy1,x1,y1);
    ccoord_to_winf(view,&wx2,&wy2,x2,y2);
    fprintf(fp,"%g %g %g setrgbcolor %g %g %g %g moveto lineto stroke\n",r,g,b,wx2,img->h-wy2,wx1,img->h-wy1);
}
#undef RAD_TO_DECI_ANG

#define WHITE FL_PACK(255,255,255)
#define BLACK FL_PACK(0,0,0)
void cb_ov_save ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    static FL_POPUP *popup = NULL;
    FL_POPUP_RETURN *retpop;
    static FL_POPUP_ITEM items[] = {
        {"Raster",NULL,NULL,FL_POPUP_TOGGLE,FL_POPUP_CHECKED},
        {"_LUT",NULL,NULL,FL_POPUP_TOGGLE,FL_POPUP_NONE},
        {"Overlay LUT",NULL,NULL,FL_POPUP_TOGGLE,FL_POPUP_NONE},
        {"Contour key",NULL,NULL,FL_POPUP_TOGGLE,FL_POPUP_NONE},
        {"ROI key",NULL,NULL,FL_POPUP_TOGGLE,FL_POPUP_NONE},
        {"_Image XY",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
        {"Image XZ",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
        {"Image ZY",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
        {"_Slice XY",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
        {"Slice XZ",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
        {"Slice ZY",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
        {NULL,NULL,NULL,0,0}
    };
    static FL_POPUP_ENTRY *entry_raster,
                          *entry_lut,
                          *entry_overlay_lut,
                          *entry_contour_key,
                          *entry_roi_key,
                          *entry_image_xy,
                          *entry_image_xz,
                          *entry_image_zy,
                          *entry_slice_xy,
                          *entry_slice_xz,
                          *entry_slice_zy;

    FL_OBJECT *browser = GUI->rois->browser;
    static const char *plane_names[3] = {"XY","XZ","ZY"};
    const xg3d_env *env_overlay = OVERLAY_ENV(GUI->overlay);
    const xg3d_quantization_workspace *qwk_overlay = QUANTIZATION_WORKSPACE(env_overlay->gui->quantization);
    const xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);
    const xg3d_lut *lut_overlay = COLORING_LUT(env_overlay->gui->coloring);
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
    const int flag_calib = fl_get_button(GUI->overview->btn_uval);
    const xg3d_lut *lut = COLORING_LUT(GUI->coloring);
    const FL_PACKED *clut = CONTOUR_LUT(GUI->contour);
    const unsigned int nc = CONTOUR_NC(GUI->contour);
    const double *z = CONTOUR_Z(GUI->contour);
    const xg3d_header *hdr = DATA->hdr;
    const char *ulabel, *file = NULL;
    unsigned int ncol,nrow,padw,colw;
    unsigned int lvlh,toth,padh;
    enum g3d_slice_plane plane;
    size_t len,maxlen,sumlen;
    xg3d_header *hdr_overlay;
    int raster = true;
    char buf[PATH_MAX];
    unsigned int w,h;
    unsigned int nsel;
    grid3d *doseslice;
    FL_IMAGE *flimg;
    Window win;
    int cw,ch;
    FLIMAGE_MARKER marker;
    FLPS_CONTROL *psopt;
    FLIMAGE_TEXT txt;
    const void *par[3];
    char *line = NULL;
    size_t rlen = 0;
    xg3d_data *tmp;
    ssize_t read;
    FILE *fp;

    if(popup == NULL) {
        popup = fl_popup_create(None,_("Save view"),items);
        fl_popup_set_policy(popup,FL_POPUP_DRAG_SELECT);
        fl_popup_set_color(popup,FL_POPUP_HIGHLIGHT_COLOR,FL_YELLOW);
        fl_popup_set_color(popup,FL_POPUP_HIGHLIGHT_TEXT_COLOR,FL_BLACK);

        entry_raster = fl_popup_entry_get_by_text(popup,"Raster");
        fl_popup_entry_set_text(entry_raster,_("Raster"));
        entry_lut = fl_popup_entry_get_by_text(popup,"_LUT");
        fl_popup_entry_set_text(entry_lut,_("LUT"));
        entry_overlay_lut = fl_popup_entry_get_by_text(popup,"Overlay LUT");
        fl_popup_entry_set_text(entry_overlay_lut,_("Overlay LUT"));
        entry_contour_key = fl_popup_entry_get_by_text(popup,"Contour key");
        fl_popup_entry_set_text(entry_contour_key,_("Contour key"));
        entry_roi_key = fl_popup_entry_get_by_text(popup,"ROI key");
        fl_popup_entry_set_text(entry_roi_key,_("ROI key"));

        entry_image_xy = fl_popup_entry_get_by_text(popup,"_Image XY");
        fl_popup_entry_set_text(entry_image_xy,_("Image XY"));
        entry_image_xz = fl_popup_entry_get_by_text(popup,"Image XZ");
        fl_popup_entry_set_text(entry_image_xz,_("Image XZ"));
        entry_image_zy = fl_popup_entry_get_by_text(popup,"Image ZY");
        fl_popup_entry_set_text(entry_image_zy,_("Image ZY"));

        entry_slice_xy = fl_popup_entry_get_by_text(popup,"_Slice XY");
        fl_popup_entry_set_text(entry_slice_xy,_("Slice XY"));
        entry_slice_xz = fl_popup_entry_get_by_text(popup,"Slice XZ");
        fl_popup_entry_set_text(entry_slice_xz,_("Slice XZ"));
        entry_slice_zy = fl_popup_entry_get_by_text(popup,"Slice ZY");
        fl_popup_entry_set_text(entry_slice_zy,_("Slice ZY"));
    }

    if(fl_get_button(GUI->overlay->btn_show))
        fl_popup_entry_clear_state(entry_overlay_lut,FL_POPUP_DISABLED);
    else
        fl_popup_entry_raise_state(entry_overlay_lut,FL_POPUP_DISABLED);

    if(fl_get_button(GUI->contour->btn_show))
        fl_popup_entry_clear_state(entry_contour_key,FL_POPUP_DISABLED);
    else
        fl_popup_entry_raise_state(entry_contour_key,FL_POPUP_DISABLED);

    if(fl_get_button(GUI->overview->show_rois))
        fl_popup_entry_clear_state(entry_roi_key,FL_POPUP_DISABLED);
    else
        fl_popup_entry_raise_state(entry_roi_key,FL_POPUP_DISABLED);

    if(fl_get_button(GUI->overview->planeXY)) {
        fl_popup_entry_set_state(entry_image_xy,FL_POPUP_NONE);
        fl_popup_entry_set_state(entry_slice_xy,FL_POPUP_NONE);
    } else {
        fl_popup_entry_set_state(entry_image_xy,FL_POPUP_DISABLED);
        fl_popup_entry_set_state(entry_slice_xy,FL_POPUP_DISABLED);
    }

    if(fl_get_button(GUI->overview->planeXZ)) {
        fl_popup_entry_set_state(entry_image_xz,FL_POPUP_NONE);
        fl_popup_entry_set_state(entry_slice_xz,FL_POPUP_NONE);
    } else {
        fl_popup_entry_set_state(entry_image_xz,FL_POPUP_DISABLED);
        fl_popup_entry_set_state(entry_slice_xz,FL_POPUP_DISABLED);
    }

    if(fl_get_button(GUI->overview->planeZY)) {
        fl_popup_entry_set_state(entry_image_zy,FL_POPUP_NONE);
        fl_popup_entry_set_state(entry_slice_zy,FL_POPUP_NONE);
    } else {
        fl_popup_entry_set_state(entry_image_zy,FL_POPUP_DISABLED);
        fl_popup_entry_set_state(entry_slice_zy,FL_POPUP_DISABLED);
    }

    fl_popup_set_position(popup,obj->form->x+obj->x+obj->w,obj->form->y+obj->y);
    retpop = fl_popup_do(popup);
    if(retpop == NULL) return;

#define IMAGE_ENTRY 5
#define SLICE_ENTRY 8
    if(retpop->val >= SLICE_ENTRY) {
        plane = retpop->val - SLICE_ENTRY;
        tmp = calloc(1,sizeof(xg3d_data));
        tmp->hdr = ENV->data->main->hdr;
        tmp->g3d = VIEW_SLICE(ov_viewports[plane]);
        snprintf(buf,sizeof(buf),"%s_%s%d",tmp->hdr->file,plane_names[plane],VIEW_K(ov_viewports[plane]));
        savedlg_show(GUI->savedlg,tmp,buf);
        free(tmp);
    } else if(retpop->val >= IMAGE_ENTRY) {
        plane = retpop->val - IMAGE_ENTRY;
        if(((FL_IMAGE*)VIEW_IMAGE(ov_viewports[plane]))->h < (int)qwk->info->nlevels)
            fl_popup_entry_clear_state(entry_lut,FL_POPUP_CHECKED);
        if(fl_get_button(GUI->overlay->btn_show) &&
                ((FL_IMAGE*)VIEW_IMAGE_LAYER(ov_viewports[plane],LAYER_OVERLAY))->h < (int)qwk_overlay->info->nlevels)
            fl_popup_entry_clear_state(entry_overlay_lut,FL_POPUP_CHECKED);
        win = FL_ObjWin(VIEW_CANVAS(ov_viewports[plane]));
        ch = fl_get_char_height(FL_FIXED_STYLE,FL_TINY_SIZE,NULL,NULL);
        cw = fl_get_char_width(FL_FIXED_STYLE,FL_TINY_SIZE);
        raster = fl_popup_entry_get_state(entry_raster) == FL_POPUP_CHECKED;

        flimg = flimage_alloc();
        if(raster == false && fl_get_button(GUI->contour->btn_show)) {
            fl_set_button(GUI->contour->btn_show,0);
            refresh_ov_canvas(ENV,plane);
            fl_set_button(GUI->contour->btn_show,1);
        }
        flimage_from_pixmap(flimg,win);
        flimage_display(flimg,win);

        /* LUT (at left, right aligned) */
        if(fl_popup_entry_get_state(entry_lut) == FL_POPUP_CHECKED) {
            /* Parameters */
            w = flimg->w; h = flimg->h;

            /* Default text params */
            txt.align = FL_ALIGN_RIGHT; txt.style = FL_FIXED_STYLE;
            txt.size = raster ? FL_TINY_SIZE : FL_SMALL_SIZE;
            txt.color = BLACK; txt.bcolor = WHITE; txt.nobk = false; txt.angle = 0;

            /* Default marker params */
            marker.name = "line"; marker.style = FL_SOLID; marker.fill = false;
            marker.w = 16; marker.h = 2; marker.angle = 0;

            /* Geometric params */
            lvlh = h / lut->ncol; toth = lut->ncol * lvlh; padh = (h - toth) / 2;
            padw = 1+14*cw; marker.x = padw-2-marker.w/2;

            /* Image operations */
            flimg->fill_color = WHITE;
            flimage_crop(flimg,-padw,0,0,0);

            /* LUT bar */
            for(unsigned int i=0;i<padh;i++) {
                marker.y = h-(1+i); marker.color = lut->colors[0];
                flimage_add_marker_struct(flimg,&marker);
            }
            for(unsigned int i=0;i<lut->ncol;i++)
                for(unsigned int j=0;j<lvlh;j++) {
                    marker.y = h-(1+i*lvlh+j+padh); marker.color = lut->colors[i];
                    flimage_add_marker_struct(flimg,&marker);
                }
            for(unsigned int i=padh+lvlh+(lut->ncol-1)*lvlh;i<h;i++) {
                marker.y = h-(1+i); marker.color = lut->colors[lut->ncol-1];
                flimage_add_marker_struct(flimg,&marker);
            }
            unsigned int i = lut->ncol-1;
            unsigned int j = lvlh-1;

            marker.color = BLACK;
            /* Sidelines */
            marker.y = h/2;
            marker.w = h+1; marker.h = 0; marker.angle = 900;
            marker.x = padw-1; flimage_add_marker_struct(flimg,&marker);
            marker.x = padw-2-16; flimage_add_marker_struct(flimg,&marker);

            /* Tics */
            marker.x = padw-1-16-6; marker.w = 8; marker.h = 0; marker.angle = 0;
            marker.y = h-(1+padh); flimage_add_marker_struct(flimg,&marker);
            if(lut->ncol > 2) { marker.y = h-(1+(lut->ncol/2)*lvlh+(lvlh/2)+padh); flimage_add_marker_struct(flimg,&marker); }
            marker.y = h-(1+i*lvlh+j+padh); flimage_add_marker_struct(flimg,&marker);

            _flimage_render_markers(flimg,win);

            txt.len = 10;
            txt.x = raster ? padw-2-16 : padw-2-16;
            /* Bottom tic label */
            snprintf(buf,sizeof(buf),"%+8.3E",
                    flag_calib ? CALIBVAL(hdr,qwk->min) : qwk->min);
            txt.str = buf; txt.y = h-(1+padh+ch/2) - (raster ? 0 : 2);
            flimage_add_text_struct(flimg,&txt);

            /* Middle tic label */
            if(lut->ncol > 2) {
                snprintf(buf,sizeof(buf),"%+8.3E",
                        flag_calib ? CALIBVAL(hdr,qwk->info->bkpts[lut->ncol/2]) :
                        qwk->info->bkpts[lut->ncol/2]);
                txt.str = buf; txt.y = h-((lut->ncol/2)*lvlh+(lvlh/2)+padh-ch/2);
                flimage_add_text_struct(flimg,&txt);
            }

            /* Top tic label */
            snprintf(buf,sizeof(buf),"%+8.3E",
                    flag_calib ? CALIBVAL(hdr,qwk->max) : qwk->max);
            txt.str = buf; txt.y = h-(i*lvlh+j+padh-ch/2);
            flimage_add_text_struct(flimg,&txt);

            /* Units label */
            if(flag_calib) {
                ulabel = fl_get_input(GUI->calibration->inp_ulabel);
                if((txt.len = strlen(ulabel)) > 0) {
                    txt.str = (char*)ulabel; txt.y = h-(1+(lut->ncol/2)*lvlh+(lvlh/2)+padh+ch/2) - (raster ? 0 : 2);
                    flimage_add_text_struct(flimg,&txt);
                }
            }
        }

        /* Overlay LUT (at right, left aligned) */
        if(fl_popup_entry_get_state(entry_overlay_lut) == FL_POPUP_CHECKED) {
            /* Parameters */
            w = flimg->w; h = flimg->h;
            hdr_overlay = env_overlay->data->main->hdr;

            /* Default text parameters */
            txt.align = FL_ALIGN_LEFT; txt.style = FL_FIXED_STYLE;
            txt.size = raster ? FL_TINY_SIZE : FL_SMALL_SIZE;
            txt.color = BLACK; txt.bcolor = WHITE; txt.nobk = false; txt.angle = 0;

            /* Default marker params */
            marker.name = "line"; marker.style = FL_SOLID; marker.fill = false;
            marker.w = 16; marker.h = 2; marker.angle = 0;

            /* Geometric parameters */
            lvlh = h / lut_overlay->ncol; toth = lut_overlay->ncol * lvlh; padh = (h - toth) / 2;
            padw = 1+14*cw; marker.x = w+1+marker.w/2;

            /* Image operations */
            flimg->fill_color = WHITE;
            flimage_crop(flimg,0,0,-padw,0);

            /* LUT bar */
            for(unsigned int i=0;i<padh;i++) {
                marker.y = h-(1+i); marker.color = lut_overlay->colors[0];
                flimage_add_marker_struct(flimg,&marker);
            }
            for(unsigned int i=0;i<lut_overlay->ncol;i++)
                for(unsigned int j=0;j<lvlh;j++) {
                    marker.y = h-(1+i*lvlh+j+padh); marker.color = lut_overlay->colors[i];
                    flimage_add_marker_struct(flimg,&marker);
                }
            for(unsigned int i=padh+lvlh+(lut_overlay->ncol-1)*lvlh;i<h;i++) {
                marker.y = h-(1+i); marker.color = lut_overlay->colors[lut_overlay->ncol-1];
                flimage_add_marker_struct(flimg,&marker);
            }
            unsigned int i = lut_overlay->ncol-1;
            unsigned int j = lvlh-1;

            marker.color = BLACK;
            /* Sidelines */
            marker.y = h/2;
            marker.w = h+1; marker.h = 0; marker.angle = 900;
            marker.x = w; flimage_add_marker_struct(flimg,&marker);
            marker.x = w+2+16; flimage_add_marker_struct(flimg,&marker);

            /* Tics */
            marker.x = w+1+16+6; marker.w = 8; marker.h = 0; marker.angle = 0;
            marker.y = h-(1+padh); flimage_add_marker_struct(flimg,&marker);
            if(lut_overlay->ncol > 2) { marker.y = h-(1+(lut_overlay->ncol/2)*lvlh+(lvlh/2)+padh); flimage_add_marker_struct(flimg,&marker); }
            marker.y = h-(1+i*lvlh+j+padh); flimage_add_marker_struct(flimg,&marker);

            _flimage_render_markers(flimg,win);

            txt.len = 10;
            txt.x = raster ? w+1+16 : w+1+16+2;
            /* Bottom tic label */
            snprintf(buf,sizeof(buf),"%+8.3E",
                    flag_calib ? CALIBVAL(hdr_overlay,qwk_overlay->min) :
                    qwk_overlay->min);
            txt.str = buf; txt.y = h-(1+padh+ch/2) - (raster ? 0 : 2);
            flimage_add_text_struct(flimg,&txt);

            /* Middle tic label */
            if(lut_overlay->ncol > 2) {
                snprintf(buf,sizeof(buf),"%+8.3E",
                        flag_calib ? CALIBVAL(hdr_overlay,qwk_overlay->info->bkpts[lut_overlay->ncol/2]) :
                        qwk_overlay->info->bkpts[lut_overlay->ncol/2]);
                txt.str = buf; txt.y = h-((lut_overlay->ncol/2)*lvlh+(lvlh/2)+padh-ch/2);
                flimage_add_text_struct(flimg,&txt);
            }

            /* Top tic label */
            snprintf(buf,sizeof(buf),"%+8.3E",
                    flag_calib ? CALIBVAL(hdr_overlay,qwk_overlay->max) :
                    qwk_overlay->max);
            txt.str = buf; txt.y = h-(i*lvlh+j+padh-ch/2);
            flimage_add_text_struct(flimg,&txt);

            /* Units label */
            if(flag_calib) {
                ulabel = fl_get_input(env_overlay->gui->calibration->inp_ulabel);
                if((txt.len = strlen(ulabel)) > 0) {
                    txt.str = (char*)ulabel; txt.y = h-(1+(lut_overlay->ncol/2)*lvlh+(lvlh/2)+padh+ch/2) - (raster ? 0 : 2);
                    flimage_add_text_struct(flimg,&txt);
                }
            }
        }

        /* ROIs */
        if(fl_popup_entry_get_state(entry_roi_key) == FL_POPUP_CHECKED) {
            /* Parameters */
            w = flimg->w; h = flimg->h;

            /* Count ROIs */
            nsel = 0;
            for(nroi_t i=0;i<DATA->nrois;i++)
                if(fl_isselected_browser_line(browser,i+1)) nsel++;

            if(nsel >= 1) {
                /* Default text parameters */
                txt.align = FL_ALIGN_LEFT; txt.style = FL_FIXED_STYLE;
                txt.size = raster ? FL_TINY_SIZE : FL_SMALL_SIZE;
                txt.color = BLACK; txt.bcolor = WHITE; txt.nobk = false; txt.angle = 0;

                /* Geometric parameters */
                nrow = h / ch; ncol = nsel / nrow + (nsel % nrow > 0 ? 1 : 0); padw = 0;

                /* Separating line */
                flimage_add_marker(flimg,"line",w,h/2,h+1,0,FL_SOLID,false,900,BLACK,0);

                _flimage_render_markers(flimg,win);

                /* ROI key */
                unsigned int j = 0;
                unsigned int k = 0;
                maxlen = sumlen = 0;
                for(nroi_t i=0;i<DATA->nrois;i++) {
                    if(!fl_isselected_browser_line(browser,i+1)) continue;
                    xg3d_roi *xroi = DATA->rois[i];
                    if((len = strlen(xroi->roi->name)) > maxlen) maxlen = len;
                    flimage_add_marker(flimg,"rect",w+5+padw+cw,2+(2*j+1)*ch/2+2*j,2*cw,ch,FL_SOLID,true,0,xroi->color,0);
                    snprintf(buf,sizeof(buf),"%-s",xroi->roi->name); txt.str = buf; txt.len = strlen(buf);
                    txt.x = w+padw+2*cw+3 + (raster ? 0 : 4);
                    txt.y = (2*j+1)*ch/2+2*j + (raster ? 2 : 1); j++;
                    flimage_add_text_struct(flimg,&txt);
                    if(++k % (nrow-1) == 0) {
                        j = 0;
                        sumlen += maxlen + 1;
                        padw = (sumlen + 3) * cw;
                        maxlen = 0;
                    }
                }
                sumlen += maxlen;

                /* Image operations */
                flimg->fill_color = WHITE;
                flimage_crop(flimg,0,0,-((sumlen+3*(k/nrow+1)+1)*cw),0);

            }
        }

        /* Contours */
        if(fl_popup_entry_get_state(entry_contour_key) == FL_POPUP_CHECKED) {
            if(nc >= 1) {
                /* Parameters */
                w = flimg->w; h = flimg->h;
                
                /* Default text parameters */
                txt.align = FL_ALIGN_LEFT; txt.style = FL_FIXED_STYLE; txt.len = 11;
                txt.size = raster ? FL_TINY_SIZE : FL_SMALL_SIZE;
                txt.color = BLACK; txt.bcolor = WHITE; txt.nobk = false; txt.angle = 0;
                cw = fl_get_char_width(FL_FIXED_STYLE,txt.size);

                /* Geometric parameters */
                nrow = h / ch;
                colw = raster ? 15*cw : 11*cw;
                padw = -colw;
                unsigned int j = 0;
                ncol = nc / nrow + (nc % nrow > 0 ? 1 : 0);

                /* Image operations */
                flimg->fill_color = WHITE;
                flimage_crop(flimg,0,0,-(ncol*colw),0);

                /* Separating line */
                flimage_add_marker(flimg,"line",w,h/2,h+1,0,FL_SOLID,false,900,BLACK,0);

                _flimage_render_markers(flimg,win);

                /* Isodose key */
                for(unsigned int i=0;i<nc;i++) {
                    if(i % nrow == 0) { padw += colw; j = 0; }
                    flimage_add_marker(flimg,"rect",w+4+padw+cw,2+(2*j+1)*ch/2,2*cw,ch,FL_SOLID,true,0,clut[i],0);
                    flimage_add_marker(flimg,"rect",w+4+padw+cw,2+(2*j+1)*ch/2,2*cw,ch,FL_SOLID,false,0,BLACK,0);
                    snprintf(buf,sizeof(buf),"%8.2E Gy",z[i]); txt.str = buf;
                    txt.x = w+padw+5*cw/2 + (raster ? 0 : 4);
                    txt.y = 2+(2*(j++)+1)*ch/2 - (raster ? 0 : 1);
                    flimage_add_text_struct(flimg,&txt);
                }
            }
        }

        if(raster) /* Rasterize text also */
            flimage_render_annotation(flimg,win);

        switch(plane) {
            case PLANE_XY: snprintf(buf,sizeof(buf),"Save XY view as..."); break;
            case PLANE_XZ: snprintf(buf,sizeof(buf),"Save XZ view as..."); break;
            case PLANE_ZY: snprintf(buf,sizeof(buf),"Save ZY view as..."); break;
            default:
                fprintf(stderr,"%s: invalid g3d_slice_plane value passed: %d",__func__,plane);
                abort();
        }

        file = fl_show_fselector(buf,"",raster ? "*.ppm" : "*.ps","");
        if(file != NULL) {
            if(raster) {
                enforce_ext(file,"ppm",buf,sizeof(buf));
                flimage_dump(flimg,buf,"ppm");
            } else {
                psopt = flimage_ps_options();
                psopt->orientation = FLPS_PORTRAIT;
                psopt->auto_fit = 1;
                psopt->eps = 1;
                psopt->xdpi = psopt->ydpi = 72;
                enforce_ext(file,"ps",buf,sizeof(buf));
                flimage_dump(flimg,buf,"ps");

                /* Vector contours (ugly hack!) */
                if(raster == false && fl_get_button(GUI->contour->btn_show) && nc > 0 && z != NULL) {
                    fp = fopen(buf,"r+");
                    if(fp != NULL) {
                        while((read = getline(&line,&rlen,fp)) != -1) {
                            if(strcmp(line,"showpage\n") == 0)
                                break;
                        }
                        if(line) { free(line); }
                        fseek(fp,-read,SEEK_CUR);
                        fprintf(fp,"0.5 setlinewidth 1 setlinecap\n");
                        doseslice = VIEW_SLICE_LAYER(ov_viewports[plane],LAYER_DOSE);
                        par[0] = ov_viewports[plane]; par[1] = clut; par[2] = fp;
                        Contour(doseslice,doseslice->nx,doseslice->ny,nc,z,_draw_contline_vector,par);
                        fprintf(fp,"showpage\n");
                        fprintf(fp,"grestore\n");
                        fprintf(fp,"grestore\n");
                        fprintf(fp,"%%Trailer\n");
                        fclose(fp);
                    } else
                        fprintf(stderr,"[ERROR] Failed exporting vector contours. Cannot open file %s.",buf);
                }
            }
        }
        flimage_delete_all_text(flimg);
        flimage_delete_all_markers(flimg);
        flimage_free(flimg);
        refresh_ov_canvas(ENV,plane);
    }
}
#undef IMAGE_ENTRY
#undef SLICE_ENTRY
#undef WHITE
#undef BLACK

bool formbrowser_overflow(FL_OBJECT *obj, const bool vert) {
    FL_Coord ow,oh;
    int x,y,aw,ah;

    fl_get_formbrowser_area(obj,&x,&y,&aw,&ah);
    fl_get_object_size(obj,&ow,&oh);

    return vert ? (aw - 2 * obj->bw) != ow : (ah - 2 * obj->bw) != oh;
}

int cb_overview_canvas(FL_OBJECT *obj, Window win, int ww FL_UNUSED_ARG, int wh FL_UNUSED_ARG, XEvent *xev, void *data FL_UNUSED_ARG) {
    const enum g3d_slice_plane plane = VIEW_PLANE(obj->form->fdui); /* plane from calling view */
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
    const unsigned int zoom = VIEW_ZOOM_X(ov_viewports[plane]); // X or Y is the same for overview
    const coord_t k[3] = {VIEW_K(ov_viewports[PLANE_XY]),
                          VIEW_K(ov_viewports[PLANE_XZ]),
                          VIEW_K(ov_viewports[PLANE_ZY])};
    const coord_t nx = VIEW_NX(ov_viewports[plane]);
    slice_t *djold = &VIEW_OLDPOS(ov_viewports[PLANE_XY]);
    slice_t *diold = &VIEW_OLDPOS(ov_viewports[PLANE_XZ]);
    slice_t *dkold = &VIEW_OLDPOS(ov_viewports[PLANE_ZY]);
    coord_t jold = OVERVIEW_OLDPOS(GUI->overview) % nx;
    coord_t iold = OVERVIEW_OLDPOS(GUI->overview) / nx;
    FL_Coord *xold = &VIEW_XOLD(ov_viewports[plane]);
    FL_Coord *yold = &VIEW_YOLD(ov_viewports[plane]);
    bool *xhair = &VIEW_XHAIR(ov_viewports[plane]);
    bool *pan = &VIEW_PAN(ov_viewports[plane]);
    bool *slide = &VIEW_SLIDE(ov_viewports[plane]);
    const coord_t *dj=NULL,*di=NULL,*dk=NULL;
    FL_OBJECT *slider = NULL, *fb = NULL;
    unsigned int keymask;
    coord_t j=0,i=0;
    FL_Coord x,y;

    /* stuff for double-click detection */
    static unsigned long last_clicktime = 0;

    switch(plane) {
        case PLANE_XY:
            slider = GUI->overview->xy_slider;
            fb = GUI->overview->xy_fb;
            dj = &j; di = &i; dk = &k[PLANE_XY];
            break;
        case PLANE_XZ:
            slider = GUI->overview->xz_slider;
            fb = GUI->overview->xz_fb;
            dj = &j; di = &k[PLANE_XZ]; dk = &i;
            break;
        case PLANE_ZY:
            slider = GUI->overview->zy_slider;
            fb = GUI->overview->zy_fb;
            dj = &k[PLANE_ZY]; di = &i; dk = &j;
            break;
        default:
            fprintf(stderr,"%s: invalid g3d_slice_plane value passed: %d",__func__,plane);
            abort();
    }

    switch(xev->type) {
        case Expose:
            refresh_ov_canvas(ENV,plane);
            fl_set_cursor(win,cursor);
            fl_remove_selected_xevent(win,PointerMotionHintMask);
            break;
        case ButtonRelease:
            switch(xev->xbutton.button) {
                case FL_LEFT_MOUSE:
                    if(*xhair) {
                        *xhair = false;
                        fl_set_cursor(win,cursor);
                        /* redraw all views */
                        fl_set_slider_value(GUI->overview->xy_slider,*dkold);
                        fl_call_object_callback(GUI->overview->xy_slider);
                        fl_set_slider_value(GUI->overview->xz_slider,*diold);
                        fl_call_object_callback(GUI->overview->xz_slider);
                        fl_set_slider_value(GUI->overview->zy_slider,*djold);
                        fl_call_object_callback(GUI->overview->zy_slider);
                    } else if(*pan) {
                        *pan = false;
                        fl_set_cursor(win,cursor);
                        _update_ov_status(ENV,plane);
                    }
                    break;
                case FL_MIDDLE_MOUSE:
                    if(*slide) {
                        *slide = false;
                        fl_set_cursor(win,cursor);
                        _update_ov_status(ENV,plane);
                    }
                    break;
                case FL_RIGHT_MOUSE:
                    break;
                case FL_SCROLLUP_MOUSE:
                    /* fall-through */
                case FL_SCROLLDOWN_MOUSE:
                    _update_ov_status(ENV,plane);
                    if(*xhair) _draw_ov_xhair(ov_viewports[plane],jold,iold);
                    break;
            }
            break;
        case ButtonPress:
            switch(xev->xbutton.button) {
                case FL_LEFT_MOUSE:
                    if(xev->xbutton.state & Button2Mask || xev->xbutton.state & Button3Mask) return FL_IGNORE; /* exclude other buttons */

                    fl_get_win_mouse(win,&x,&y,&keymask);
                    win_to_grid(ov_viewports[plane],x,y,&j,&i);

                    /* check for double click (code from xforms' FL_DBLCLICK) */
                    if ( !(FL_abs(*xold - x) > 4 || FL_abs(*yold - y) > 4) /* moved 4 px or less */
                            && xev->xbutton.time - last_clicktime < FL_CLICK_TIMEOUT /* short delay */
                       ) { /* double click */
                        /* change cursor */
                        fl_set_cursor(win,XC_fleur);
                        /* set panning */
                        *pan = true;
                    } else { /* single click */
                        /* hide cursor */
                        fl_set_cursor(win,FL_INVISIBLE_CURSOR);
                        /* draw crosshairs */
                        _draw_ov_xhair(ov_viewports[PLANE_XY],*dj,*di);
                        _draw_ov_xhair(ov_viewports[PLANE_XZ],*dj,*dk);
                        _draw_ov_xhair(ov_viewports[PLANE_ZY],*dk,*di);
                        /* store mouse positions */
                        *djold = *dj; *diold = *di; *dkold = *dk;
                        OVERVIEW_OLDPOS(GUI->overview) = j + i*nx;
                        /* set drawing crosshair */
                        *xhair = true;
                    }

                    last_clicktime = xev ? xev->xbutton.time : 0;
                    *xold = x; *yold = y;

                    /* update overview status */
                    _update_ov_status(ENV,plane);
                    break;
                case FL_MIDDLE_MOUSE:
                    if(xev->xbutton.state & Button1Mask || xev->xbutton.state & Button3Mask) return FL_IGNORE; /* exclude other buttons */

                    fl_get_win_mouse(win,&x,&y,&keymask);
                    /* change cursor */
                    fl_set_cursor(win,XC_double_arrow);
                    /* store y mouse window position */
                    *yold = y;
                    /* set sliding */
                    *slide = true; 
                    break;
                case FL_RIGHT_MOUSE:
                    if(xev->xbutton.state & Button1Mask || xev->xbutton.state & Button2Mask) return FL_IGNORE; /* exclude other buttons */
                    _update_ov_status(ENV,plane);
                    break;
                case FL_SCROLLUP_MOUSE:
                    /* change slice +1 */
                    fl_set_slider_value(slider,k[plane]+1);
                    fl_call_object_callback(slider);
                    break;
                case FL_SCROLLDOWN_MOUSE:
                    /* change slice -1 */
                    fl_set_slider_value(slider,k[plane]-1);
                    fl_call_object_callback(slider);
                    break;
            }
            break;
        case MotionNotify:
            /* get mouse position in window*/
            fl_get_win_mouse(win,&x,&y,&keymask);
            /* get mouse position in grid */
            win_to_grid(ov_viewports[plane],x,y,&j,&i);

            /* if mouse outside canvas, do nothing */
            if(x < 0 || x >= ww || y < 0 || y >= wh) return FL_IGNORE;

            if(*slide) {
                fl_set_slider_value(slider,k[plane]+(*yold-y)); /* inverted y axis */
                fl_call_object_callback(slider);
                /* store y mouse window position */
                *yold = y;
            } else if(*xhair) {
                if(j != jold || i != iold) {
                    /* clear old crosshairs */
                    _draw_ov_xhair(ov_viewports[PLANE_XY],*djold,*diold);
                    _draw_ov_xhair(ov_viewports[PLANE_XZ],*djold,*dkold);
                    _draw_ov_xhair(ov_viewports[PLANE_ZY],*dkold,*diold);
                    /* draw new crosshairs */
                    _draw_ov_xhair(ov_viewports[PLANE_XY],*dj,*di);
                    _draw_ov_xhair(ov_viewports[PLANE_XZ],*dj,*dk);
                    _draw_ov_xhair(ov_viewports[PLANE_ZY],*dk,*di);
                }
                /* store mouse grid positions */
                OVERVIEW_OLDPOS(GUI->overview) = j + i*nx;
                *djold = *dj; *diold = *di; *dkold = *dk;
            } else if(*pan) {
                const bool fbh = formbrowser_overflow(fb,false);
                const bool fbv = formbrowser_overflow(fb,true);
                if(fbh || fbv) {
                    /* get formbrowser offsets */
                    int fbox = fl_get_formbrowser_xoffset(fb);
                    int fboy = fl_get_formbrowser_yoffset(fb);
                    /* get mouse position in toplevel window (not formbrowser's) */
                    fl_get_win_mouse(FL_ObjWin(fb),&x,&y,&keymask);
                    if(fbh && x != *xold)
                        fl_set_formbrowser_xoffset(fb,fbox+(x<*xold?-zoom:zoom));
                    if(fbv && y != *yold)
                        fl_set_formbrowser_yoffset(fb,fboy+(y<*yold?-zoom:zoom));
                    fl_redraw_object(fb);
                    /* store mouse toplevel window (not formbrowser's) position */
                    *xold = x; *yold = y;
                }
            }
            /* update overview status */
            _update_ov_status(ENV,plane);
            break;
        case EnterNotify:
            if(*xhair) /* crosshairs being drawn */
                fl_set_cursor(win,FL_INVISIBLE_CURSOR); /* hide cursor */
            /* update overview status */
            _update_ov_status(ENV,plane);
            break;
        case LeaveNotify:
            if(*xhair) /* crosshairs being drawn */
                fl_reset_cursor(win); /* show default cursor */
            /* clear overview status */
            fl_set_object_label(GUI->overview->status,"");
            break;
        case KeyPress:
            switch(XLookupKeysym((XKeyEvent*)xev,0)) {
                case XK_Left:  fl_get_mouse(&x,&y,&keymask); fl_set_mouse(x-zoom,y); break;
                case XK_Right: fl_get_mouse(&x,&y,&keymask); fl_set_mouse(x+zoom,y); break;
                case XK_Up:    fl_get_mouse(&x,&y,&keymask); fl_set_mouse(x,y-zoom); break;
                case XK_Down:  fl_get_mouse(&x,&y,&keymask); fl_set_mouse(x,y+zoom); break;
                case XK_y:
                    { /* copy status to clipboard */
                        const char *label = fl_get_object_label(GUI->overview->status);
                        fl_stuff_clipboard(GUI->overview->status,0,label,strlen(label),NULL);
                    }
                    break;
                case XK_Next:
                    /* change slice +1 */
                    fl_set_slider_value(slider,k[plane]+1);
                    fl_call_object_callback(slider);
                    _update_ov_status(ENV,plane);
                    if(*xhair) _draw_ov_xhair(ov_viewports[plane],jold,iold);
                    break;
                case XK_Prior:
                    /* change slice -1 */
                    fl_set_slider_value(slider,k[plane]-1);
                    fl_call_object_callback(slider);
                    _update_ov_status(ENV,plane);
                    if(*xhair) _draw_ov_xhair(ov_viewports[plane],jold,iold);
                    break;
            }
            break;
    }

    return FL_IGNORE;
}
