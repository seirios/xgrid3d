#ifndef XG3D_info_cb_h_
#define XG3D_info_cb_h_

#include "fd_info.h"
#include "xgrid3d.h"

#define INFO_HDR(fd_info) ((xg3d_header*)((FD_info*)(fd_info))->vdata)

void deactivate_info(FD_info*);
void setup_info(FD_info*,const xg3d_header*);
void update_volumes(xg3d_env*,const xg3d_header*);

#endif
