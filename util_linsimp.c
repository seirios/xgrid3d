#include "util_linsimp.h"

#include <string.h>
#include <float.h>

#include <gsl/gsl_spline.h>
#include <gsl/gsl_integration.h>

#define EPS_ABSINT 1e-6
#define EPS_RELINT 1e-6

struct params {
    gsl_spline *s;
    gsl_interp_accel *a;
};

static double _f_linsimp(double x, void *data) {
    struct params *par = (struct params*)data;
    return gsl_spline_eval(par->s,x,par->a);
}

void linsimp(double **x, double **y, unsigned int *n, const double eps_tol) {
    gsl_interp_accel *acc = gsl_interp_accel_alloc();
    gsl_spline *spline;
    struct params p;
    gsl_function F;

    unsigned int l0;
    double area0,area;
    double err0,err;
    double *fx,*fy;
    size_t inteval;
    double interr;

    /* compute initial integral */
    spline = gsl_spline_alloc(gsl_interp_linear,*n);
    gsl_spline_init(spline,*x,*y,*n);
    p.s = spline; p.a = acc;
    F.function = &_f_linsimp;
    F.params = &p;
    gsl_integration_qng(&F,0.0,1.0,EPS_ABSINT,EPS_RELINT,&area0,&interr,&inteval);
    gsl_spline_free(spline);

    /* loop */
    while(*n > 2) {
        err0 = DBL_MAX; l0 = 1;
        fx = malloc((*n-1)*sizeof(double)); fy = malloc((*n-1)*sizeof(double));
        spline = gsl_spline_alloc(gsl_interp_linear,*n-1);
        for(unsigned int l=1;l<*n-1;l++) {
            memcpy(fx,*x,l*sizeof(double)); memcpy(&fx[l],&(*x)[l+1],(*n-l-1)*sizeof(double));
            memcpy(fy,*y,l*sizeof(double)); memcpy(&fy[l],&(*y)[l+1],(*n-l-1)*sizeof(double));
            gsl_spline_init(spline,fx,fy,*n-1);
            p.s = spline;
            gsl_integration_qng(&F,0.0,1.0,EPS_ABSINT,EPS_RELINT,&area,&interr,&inteval);
            if((err = fabs(area0 - area)) < err0) {
                err0 = err;
                l0 = l;
            }
        }
        free(fx); free(fy);
        gsl_spline_free(spline);

        if(err0 < eps_tol) {
            (*n)--;
            memmove(&(*x)[l0],&(*x)[l0+1],(*n-l0)*sizeof(double));
            *x = realloc(*x,*n*sizeof(double));
            memmove(&(*y)[l0],&(*y)[l0+1],(*n-l0)*sizeof(double));
            *y = realloc(*y,*n*sizeof(double));
        } else
            break;
    }

    gsl_interp_accel_free(acc);
}
