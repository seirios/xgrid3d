/* Form definition file generated by fdesign on Sat Sep 28 21:14:16 2024 */

#include <stdlib.h>
#include "fd_calibration.h"

#ifdef ENABLE_NLS
#include <libintl.h>
#define _(String) gettext (String)
#else
#define _(String) (String)
#endif


/***************************************
 ***************************************/

FD_calibration *
create_form_calibration( void )
{
    FL_OBJECT *obj;
    FD_calibration *fdui = ( FD_calibration * ) fl_malloc( sizeof *fdui );

    fdui->vdata = fdui->cdata = NULL;
    fdui->ldata = 0;

    fdui->calibration = fl_bgn_form( FL_NO_BOX, 225, 350 );

    obj = fl_add_box( FL_FLAT_BOX, 0, 0, 225, 350, "" );

    obj = fl_add_labelframe( FL_ENGRAVED_FRAME, 5, 8, 215, 103, _("Calibration") );
    fl_set_object_lstyle( obj, FL_ITALIC_STYLE );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->tf_calib = obj = fl_add_tabfolder( FL_TOP_TABFOLDER, 5, 115, 215, 230, "" );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_calib_tf, 0 );

    fdui->inp_slope = obj = fl_add_input( FL_FLOAT_INPUT, 75, 20, 140, 25, _("Slope") );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_calib_input_factor, 0 );

    fdui->inp_inter = obj = fl_add_input( FL_FLOAT_INPUT, 75, 50, 140, 25, _("Intercept") );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_calib_input_factor, 1 );

    fdui->btn_vv = obj = fl_add_button( FL_NORMAL_BUTTON, 102, 2, 55, 15, _("Vox. vol.") );
    fl_set_object_boxtype( obj, FL_FRAME_BOX );
    fl_set_object_color( obj, FL_YELLOW, FL_COL1 );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_calib_framebut, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_reset = obj = fl_add_button( FL_NORMAL_BUTTON, 161, 2, 50, 15, _("Reset") );
    fl_set_object_boxtype( obj, FL_FRAME_BOX );
    fl_set_object_color( obj, FL_YELLOW, FL_COL1 );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_calib_framebut, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->inp_ulabel = obj = fl_add_input( FL_NORMAL_INPUT, 75, 80, 140, 25, _("Units label") );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fl_end_form( );

    fdui->calibration->fdui = fdui;

    return fdui;
}

/***************************************
 ***************************************/

FD_calib_std *
create_form_calib_std( void )
{
    FL_OBJECT *obj;
    FD_calib_std *fdui = ( FD_calib_std * ) fl_malloc( sizeof *fdui );

    fdui->vdata = fdui->cdata = NULL;
    fdui->ldata = 0;

    fdui->calib_std = fl_bgn_form( FL_NO_BOX, 210, 205 );

    fdui->box = obj = fl_add_box( FL_FLAT_BOX, 0, 0, 210, 205, "" );

    fdui->ch_uact = obj = fl_add_choice( FL_NORMAL_CHOICE, 180, 55, 25, 25, "" );
    fl_set_object_boxtype( obj, FL_SHADOW_BOX );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_addto_choice( obj, _("Bq") );
    fl_addto_choice( obj, _("µCi") );
    fl_set_choice( obj, 2 );

    fdui->ch_utime = obj = fl_add_choice( FL_NORMAL_CHOICE, 80, 55, 20, 25, "" );
    fl_set_object_boxtype( obj, FL_SHADOW_BOX );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_addto_choice( obj, _("s") );
    fl_addto_choice( obj, _("m") );
    fl_addto_choice( obj, _("h") );
    fl_set_choice( obj, 3 );

    obj = fl_add_button( FL_NORMAL_BUTTON, 5, 175, 200, 25, _("Calibrate from standard") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_calib_std, 0 );

    fdui->inp_time = obj = fl_add_input( FL_FLOAT_INPUT, 5, 55, 75, 25, _("Time lapse") );
    fl_set_object_lalign( obj, FL_ALIGN_LEFT_TOP );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_calib_std_inptime, 0 );

    fdui->inp_act = obj = fl_add_input( FL_FLOAT_INPUT, 105, 55, 75, 25, _("Activity") );
    fl_set_object_lalign( obj, FL_ALIGN_LEFT_TOP );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_calib_std_inpact, 0 );

    fdui->txt_val = obj = fl_add_text( FL_NORMAL_TEXT, 80, 145, 125, 25, "" );
    fl_set_object_boxtype( obj, FL_FRAME_BOX );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->inp_gamma = obj = fl_add_input( FL_FLOAT_INPUT, 125, 85, 80, 25, _("Gamma intensity") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_calib_std_inpgamma, 0 );

    fdui->ch_roi = obj = fl_add_choice( FL_NORMAL_CHOICE2, 35, 115, 170, 25, _("ROI") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_calib_std_roi, 0 );

    fdui->ch_nuclide = obj = fl_add_choice( FL_NORMAL_CHOICE2, 91, 5, 115, 25, _("Radionuclide") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    obj = fl_add_box( FL_NO_BOX, 0, 145, 80, 25, _("Total value:") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fl_end_form( );

    fdui->calib_std->fdui = fdui;

    return fdui;
}
