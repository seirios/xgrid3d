#include "niftyreg_c.h"
#include "_reg_aladin.h"
#include "_reg_aladin_sym.h"

/* this file implements a C interface to the CPP library niftyreg */

#define SetMacroC(name,type) \
	void reg_aladin_Set##name(regAladin *x, type _arg) { \
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin); \
        y->Set##name(_arg); \
	}

#define GetMacroC(name,type) \
	type reg_aladin_Get##name(regAladin *x) { \
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin); \
        return y->Get##name(); \
	}

#define BooleanMacroC(name,type) \
	void reg_aladin_##name##On(regAladin *x) { \
		reg_aladin_Set##name(x,(type)1); \
	} \
	void reg_aladin_##name##Off(regAladin *x) { \
		reg_aladin_Set##name(x,(type)0); \
	}

extern "C" {
	regAladin* reg_aladin_new(void) {
        regAladin *x = new regAladin;
        x->aladin = new reg_aladin<myDTYPE>;
        return x;
	}

	regAladin* reg_aladin_sym_new(void) {
        regAladin *x = new regAladin;
        x->aladin_sym = new reg_aladin_sym<myDTYPE>;
        return x;
	}

	void reg_aladin_delete(regAladin *x) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        delete y;
        delete x;
	}

	char* reg_aladin_GetExecutableName(regAladin *x) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        return y->GetExecutableName();
	}

	void reg_aladin_SetInputReference(regAladin *x, nifti_image* nim) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        y->SetInputReference(nim);
	}

	nifti_image* reg_aladin_GetInputReference(regAladin *x) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        return y->GetInputReference();
	}

	void reg_aladin_SetInputFloating(regAladin *x, nifti_image* nim) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        y->SetInputFloating(nim);
	}

	nifti_image* reg_aladin_GetInputFloating(regAladin *x) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        return y->GetInputFloating();
	}

	void reg_aladin_SetInputMask(regAladin *x, nifti_image* nim) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        y->SetInputMask(nim);
	}

	nifti_image* reg_aladin_GetInputMask(regAladin *x) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        return y->GetInputMask();
	}

	void reg_aladin_SetInputFloatingMask(regAladin *x, nifti_image* nim) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin_sym);
        y->SetInputFloatingMask(nim);
	}

	nifti_image* reg_aladin_GetInputFloatingMask(regAladin *x) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin_sym);
        return y->GetInputMask();
	}

	void reg_aladin_SetInputTransform(regAladin *x, mat44 *matrix) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        y->SetInputTransform(matrix);
	}

	mat44* reg_aladin_GetTransformationMatrix(regAladin *x) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        return y->GetTransformationMatrix();
	}

	nifti_image* reg_aladin_GetFinalWarpedImage(regAladin *x) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        return y->GetFinalWarpedImage();
	}

	SetMacroC(Verbose,int)
	GetMacroC(Verbose,int)

	SetMacroC(MaxIterations,unsigned int)
	GetMacroC(MaxIterations,unsigned int)

	SetMacroC(NumberOfLevels,unsigned int)
	GetMacroC(NumberOfLevels,unsigned int)

	SetMacroC(LevelsToPerform,unsigned int)
	GetMacroC(LevelsToPerform,unsigned int)

	SetMacroC(BlockPercentage,float)
	GetMacroC(BlockPercentage,float)

	SetMacroC(InlierLts,float)
	GetMacroC(InlierLts,float)

	SetMacroC(ReferenceSigma,float)
	GetMacroC(ReferenceSigma,float)

	SetMacroC(FloatingSigma,float)
	GetMacroC(FloatingSigma,float)

	SetMacroC(PerformRigid,int)
	GetMacroC(PerformRigid,int)
	BooleanMacroC(PerformRigid,int)

	SetMacroC(PerformAffine,int)
	GetMacroC(PerformAffine,int)
	BooleanMacroC(PerformAffine,int)

	GetMacroC(AlignCentre,int)
	SetMacroC(AlignCentre,int)
	BooleanMacroC(AlignCentre,int)

	SetMacroC(Interpolation,int)
	GetMacroC(Interpolation,int)

	void reg_aladin_SetInterpolationToNearestNeighbor(regAladin *x) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        y->SetInterpolation(0);
	}

	void reg_aladin_SetInterpolationToTrilinear(regAladin *x) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        y->SetInterpolation(1);
	}

	void reg_aladin_SetInterpolationToCubic(regAladin *x) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        y->SetInterpolation(3);
	}

	int reg_aladin_Check(regAladin *x) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        return y->Check();
	}

	int reg_aladin_Print(regAladin *x) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        return y->Print();
	}

	int reg_aladin_Run(regAladin *x) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        return y->Run();
	}

	void reg_aladin_DebugPrintLevelInfo(regAladin *x, int d) {
        reg_aladin<myDTYPE>* y = static_cast<reg_aladin<myDTYPE>*>(x->aladin);
        y->DebugPrintLevelInfo(d);
	}

	int resampleSourceImage(nifti_image *ref, nifti_image *flo, nifti_image *res, nifti_image *def, int *mask, int interp, float fill) {
		return reg_resampleSourceImage(ref,flo,res,def,mask,interp,fill);
	}

	void affine_positionField(mat44 *aff, nifti_image *ref, nifti_image *def) {
		reg_affine_positionField(aff,ref,def);
	}
}
