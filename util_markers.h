#ifndef XG3D_util_markers_h
#define XG3D_util_markers_h

#include "grid3d.h"

/* Selection marker */
typedef struct _marker {
    coord_t vox[3];
    char *label;
    int align;
    struct _marker *next;
    struct _marker *prev;
} marker;

/* Marker functions */
marker* marker_alloc(const coord_t[3],const char*);
marker* marker_search(marker*,const coord_t[3],unsigned int*);
marker* marker_add(marker **mk, const coord_t[3],const char*,unsigned int*);
marker* marker_del(marker **mk, const coord_t[3],unsigned int*);
marker* marker_dup(const marker*);
void marker_free(marker*);
marker* marker_load(const char*,unsigned int*);
g3d_ret marker_save(const char*,const marker*);

#endif
