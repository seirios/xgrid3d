#ifdef HAVE_SHAPE_H
#include <X11/extensions/shape.h>
#endif

#include <libgen.h>
#include <matheval.h>

#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "cb_menu.h"
#include "cb_datasets.h"
#include "cb_dialog.h"
#include "cb_viewport.h"
#include "fd_mainview.h"
#include "fd_overview.h"
#include "cb_rois.h"
#include "cb_quantization.h"
#include "cb_selection.h"
#include "cb_operations.h"
#include "fd_dose.h"
#include "cb_overlay.h"

#include "util.h"
#include "util_coordinates.h"

#include "about.h"

#include "scroll_bg.xpm"
#ifdef HAVE_SHAPE_H
#include "scroll_mask.xbm"
#endif

#define HIDE_FORM(form) \
    if(fl_form_is_visible(form)) fl_hide_form(form);
#define SHOW_FORM(button) \
    if(fl_get_button(button)) fl_trigger_object(button);
void cb_menu_showhide ( FL_OBJECT * obj  , long data  ) {
    const xg3d_gui *gui_overlay = ((xg3d_env*)OVERLAY_ENV(GUI->overlay))->gui;
    const int button = fl_get_button_numb(obj);
    FL_FORM *form = NULL;
    char buf[1024];

    switch(data) {
        case 0: /* Datasets */
            form = GUI->datasets->datasets;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Datasets"));
            break;
        case 1: /* Function */
            form = GUI->function->function;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Function"));
            break;
        case 2: /* Mainview */
            form = GUI->mainview->mainview;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Mainview"));
            break;
        case 3: /* Kinetics */
            form = GUI->kinetics->kinetics;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Kinetics"));
            break;
        case 4: /* Kernel */
            form = GUI->kernel->kernel;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Kernel"));
            break;
        case 5: /* Film */
            form = GUI->film->film;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Film dosimetry"));
            break;
    }

    if(button == FL_RIGHT_MOUSE && fl_form_is_visible(form)) {
        fl_set_button(obj,1);
        fl_raise_form(form);
        fl_winfocus(form->window);
        return;
    }

    if(form == GUI->mainview->mainview) {
        if(fl_form_is_visible(form)) { /* hide subforms */
            HIDE_FORM(GUI->info->info);
            HIDE_FORM(GUI->overview->overview);
            HIDE_FORM(GUI->calibration->calibration);
            HIDE_FORM(GUI->selection->selection);
            HIDE_FORM(GUI->coloring->coloring);
            HIDE_FORM(GUI->quantization->quantization);
            HIDE_FORM(GUI->rois->rois);
            HIDE_FORM(GUI->roi_stats->roi_stats);
            HIDE_FORM(GUI->dose->dose);
            HIDE_FORM(GUI->dvhs->dvhs);
            HIDE_FORM(GUI->contour->contour);
            HIDE_FORM(GUI->operations->operations);
            HIDE_FORM(GUI->overlay->overlay);
            HIDE_FORM(GUI->measure->measure);
            HIDE_FORM(GUI->surface->surface);
            HIDE_FORM(GUI->ct->ct);
            HIDE_FORM(gui_overlay->calibration->calibration);
            HIDE_FORM(gui_overlay->quantization->quantization);
            HIDE_FORM(gui_overlay->coloring->coloring);
        } else { /* show previously visible subforms */
            if(ENV->data->main == NULL) {
                fl_show_alert2(0,_("No main dataset!\fPlease assign a main dataset first."));
                fl_set_button(GUI->menu->btn_mainview,0);
                return;
            }
            SHOW_FORM(GUI->mainview->btn_info);
            SHOW_FORM(GUI->mainview->btn_overview);
            SHOW_FORM(GUI->mainview->btn_calibration);
            SHOW_FORM(GUI->mainview->btn_selection);
            SHOW_FORM(GUI->mainview->btn_coloring);
            SHOW_FORM(GUI->mainview->btn_quantization);
            SHOW_FORM(GUI->mainview->btn_rois);
            SHOW_FORM(GUI->rois->btn_stats);
            SHOW_FORM(GUI->mainview->btn_dose);
            SHOW_FORM(GUI->mainview->btn_dvhs);
            SHOW_FORM(GUI->overview->show_contours);
            SHOW_FORM(GUI->mainview->btn_operations);
            SHOW_FORM(GUI->overview->show_overlay);
            SHOW_FORM(GUI->mainview->measure);
            SHOW_FORM(GUI->mainview->surface);
            SHOW_FORM(GUI->mainview->btn_ct);
            SHOW_FORM(GUI->overlay->btn_calibration);
            SHOW_FORM(GUI->overlay->btn_quantization);
            SHOW_FORM(GUI->overlay->btn_coloring);
            fl_set_app_mainform(form);
        }
    }

    if(fl_form_is_visible(form)) {
        fl_hide_form(form);
        fl_deactivate_form(form);
        return;
    } else {
        fl_activate_form(form);
        fl_show_form(form,FL_PLACE_SIZE,FL_FULLBORDER,buf);
        fl_raise_form(form);
        fl_winfocus(form->window);
    }

    if(form == GUI->mainview->mainview)
        sel_set_cursor(GUI->viewport,GUI->selection);
}
#undef HIDE_FORM_OVER
#undef HIDE_FORM

void cb_menu_io ( FL_OBJECT * obj  , long data  ) {
    xg3d_header *xhdr = NULL;
    char *dir = NULL;
    const char *fun;
    xg3d_data *dat;
    g3d_ret ret;
    int center;
    void *f;

    switch(data) {
        case 0: /* New */
            xhdr = newdlg_show(ENV,&fun,&center);
            if(xhdr == NULL) return;


            grid3d *g3d = g3d_alloc(g3d_type_from_raw_io_type(xhdr->type),xhdr->dim[0],xhdr->dim[1],xhdr->dim[2],.alloc=true);
            if(g3d == NULL) return;

            fl_show_oneliner(_("Creating new data..."),fl_scrw/2,fl_scrh/2);

            /* if asked to fill with function */
            if(fun != NULL && strlen(fun) > 0 && (f = evaluator_create((char*)fun)) != NULL) {
                evaluator_destroy(f);
#ifdef _OPENMP
#pragma omp parallel
                {
#endif
                    void *f = evaluator_create((char*)fun);
#ifdef _OPENMP
#pragma omp for
#endif
                    for(index_t ix=0;ix<g3d->nvox;ix++) {
                        coord_t jik[3];
                        UNCOORD_DIM(ix,jik,g3d->nx,g3d->nxy);
                        double fx = 0.5 + jik[0];
                        double fy = 0.5 + jik[1];
                        double fz = 0.5 + jik[2];
                        if(center) {
                            fx -= 0.5 * g3d->nx;
                            fy -= 0.5 * g3d->ny;
                            fz -= 0.5 * g3d->nz;
                        }
                        g3d_set_dvalue(g3d,ix,evaluator_evaluate_x_y_z(f,fx,fy,fz));
                    }
                    evaluator_destroy(f);
#ifdef _OPENMP
                }
#endif
            }

            dat = calloc(1,sizeof(xg3d_data));
            dat->name = (fun != NULL) ? strdup(fun) : strdup("New data");
            dat->g3d = g3d;
            dat->hdr = xg3d_header_dup(xhdr);
            xg3d_datasets_add(ENV->data,dat);
            xg3d_header_free(xhdr);
            refresh_datasets_browser(GUI->datasets->browser,ENV->data);
            fl_hide_oneliner();

            if(ENV->data->nsets == 1)
                load_single_show_mainview(ENV);
            break;
        case 1: /* Load */
            switch(fl_get_menu(GUI->menu->menu_load)) {
                default:
                case 1: /* Data */
                    xhdr = opendlg_show(ENV);
                    if(xhdr == NULL) return;

                    fl_show_oneliner(_("Loading data..."),fl_scrw/2,fl_scrh/2);
                    if((ret = load_data(ENV,xhdr)) != G3D_OK) {
                        fl_hide_oneliner();
                        fl_show_alert2(0,_("Failed loading data!\fFile: %s\nERROR: %s"),xhdr->file,g3d_ret_msg(ret));
                        xg3d_header_free(xhdr);
                        return;
                    }

                    dir = strdup(xhdr->file); fl_set_directory(dirname(dir)); free(dir);
                    xg3d_header_free(xhdr);
                    refresh_datasets_browser(GUI->datasets->browser,ENV->data);

                    if(ENV->data->nsets == 1)
                        load_single_show_mainview(ENV);
                    fl_hide_oneliner();
                    break;
                case 2: /* TODO: workspace load */
                    break;
            }
            break;
        case 2: /* Save */
            switch(fl_get_menu(GUI->menu->menu_save)) {
                default:
                case 1: /* Data */
                    if(ENV->data->main == NULL) {
                        fl_show_alert2(0,_("No main dataset!\fPlease assign a main dataset first."));
                        return;
                    }
                        savedlg_show(GUI->savedlg,ENV->data->main,ENV->data->main->hdr->file);
                    break;
                case 2: /* TODO: workspace save */
                    break;
            }
            break;
        case 3: /* Close */
            if(ENV->data->main == NULL) return;
            unsigned int ix = 0;
            for(unsigned int i=0;i<ENV->data->nsets;i++)
                if(ENV->data->sets[i] == ENV->data->main) {
                    ix = i; break;
                }
            fl_select_browser_line(GUI->datasets->browser,ix+1);
            fl_call_object_callback(GUI->datasets->btn_unload);
            break;
    }
}

int about_cb(XEvent *xev, void *data FL_UNUSED_ARG) {
    const Window win = ((XAnyEvent*)xev)->window;

    if(xev->type == Expose) {
        fl_winset(win);
        fl_draw_text(FL_ALIGN_LEFT,85,75,0,0,FL_BLACK,FL_FIXED_STYLE|FL_BOLD_STYLE,FL_MEDIUM_SIZE,ABOUT_WRITTEN_OATH);
        fl_draw_text(FL_ALIGN_LEFT,85,135,0,0,FL_BLACK,FL_FIXED_STYLE|FL_BOLD_STYLE,FL_SMALL_SIZE,ABOUT_SOFTWARE_CREDITS_HEADER);
        fl_draw_text(FL_ALIGN_LEFT,85,195,0,0,FL_BLACK,FL_FIXED_STYLE|FL_BOLD_STYLE,FL_SMALL_SIZE,ABOUT_SOFTWARE_CREDITS);
        fl_draw_text(FL_ALIGN_CENTER,265,265,0,0,FL_BLACK,FL_FIXED_STYLE|FL_BOLD_STYLE,FL_SMALL_SIZE,ABOUT_DEDICATED_TO_HEADER);
        fl_draw_text(FL_ALIGN_CENTER,265,280,0,0,FL_BLACK,FL_FIXED_STYLE|FL_BOLD_STYLE,FL_SMALL_SIZE,ABOUT_DEDICATED_TO);
        fl_draw_text(FL_ALIGN_CENTER,265,305,0,0,FL_BLACK,FL_FIXED_STYLE|FL_BOLD_STYLE,FL_NORMAL_SIZE,ABOUT_COPYRIGHT);
    } else if(xev->type == ButtonPress) {
        fl_winclose(win);
        fl_activate_all_forms();
    }
    return 0;
}

void cb_menu_about ( FL_OBJECT * obj  , long data  ) {
    (void)obj;
(void)data;
    unsigned int w,h;
    Pixmap bg,mask;
    Window win;
    Atom atom;
#ifdef HAVE_SHAPE_H
    int evbp,errbp;
#endif

    fl_winsize(456,342);
    fl_transient();
    win = fl_wincreate("xgrid3d - About");
    bg = fl_create_from_pixmapdata(win,scroll_bg_xpm,&w,&h,&mask,NULL,NULL,0);
    XSetWindowBackgroundPixmap(fl_display,win,bg);
    fl_free_pixmap(bg); fl_free_pixmap(mask);

    /* undecorate */
    atom = XInternAtom(fl_display,"_OB_WM_STATE_UNDECORATED",False);
    XChangeProperty(fl_display,win,XInternAtom(fl_display,"_NET_WM_STATE",False),XA_ATOM,32,PropModeAppend,(unsigned char *)&atom,1);
    atom = XInternAtom(fl_display,"_NET_WM_WINDOW_TYPE_SPLASH",False);
    XChangeProperty(fl_display,win,XInternAtom(fl_display,"_NET_WM_WINDOW_TYPE",False),XA_ATOM,32,PropModeAppend,(unsigned char *)&atom,1);

#ifdef HAVE_SHAPE_H
    if(XShapeQueryExtension(fl_display,&evbp,&errbp)) {
        mask = fl_create_from_bitmapdata(win,(const char*)scroll_mask_bits,scroll_mask_width,scroll_mask_height);
        XShapeCombineMask(fl_display,win,ShapeBounding,0,0,mask,ShapeSet);
        fl_free_pixmap(mask);
    }
#endif

    fl_deactivate_all_forms();
    fl_winshow(win);

    fl_set_event_callback(about_cb,NULL);
}
