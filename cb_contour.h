#ifndef XG3D_contour_cb_h_
#define XG3D_contour_cb_h_

#include "fd_contour.h"

#define MAX_CONTOURS 19

#define CONTOUR_LUT(fd_contour) (((FD_contour*)(fd_contour))->br_contour_lut->u_vdata)
#define CONTOUR_NC(fd_contour) (((FD_contour*)(fd_contour))->br_contour->u_ldata)
#define CONTOUR_Z(fd_contour) (((FD_contour*)(fd_contour))->br_contour->u_vdata)

#endif
