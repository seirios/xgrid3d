#ifndef XG3D_operations_cb_h_
#define XG3D_operations_cb_h_

#include "fd_operations.h"
#include "xgrid3d.h"

#define BACKUP_DATA(fd_operations) ((xg3d_data*)(((FD_operations*)(fd_operations))->vdata))
#define BACKUP_HDR(fd_operations) (BACKUP_DATA(fd_operations)->hdr)
#define BACKUP_G3D(fd_operations) (BACKUP_DATA(fd_operations)->g3d)
#define BACKUP_NROI(fd_operations) (BACKUP_DATA(fd_operations)->nroi)
#define BACKUP_ROIS(fd_operations) (BACKUP_DATA(fd_operations)->roi)

void update_op_crop_inputs(FD_operations*,const xg3d_header*);

#endif
