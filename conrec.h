#ifndef XG3D_conrec_h_
#define XG3D_conrec_h_

#include "grid3d.h"

typedef void (_contdrawf)(const double,const double,const double,const double,const unsigned int,void*);

void Contour(const grid3d*,int,int,int,const double*,_contdrawf*,void*);

#endif
