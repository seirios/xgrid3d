#ifndef XG3D_selection_h_
#define XG3D_selection_h_

#include "dl.h"
#include "grid3d_uint8.h"
#include "grid3d_quant.h"

#include <gsl/gsl_histogram.h>

#include "util_markers.h"

#define KAHAN_SUM(dat,y,t,c,val) \
    y = (dat) - (c); t = (val) + (y); c = ((t) - (val)) - (y); val = (t);

/* clear SEL_TMP and clear SEL_BIT */
#define xg3d_sel_bctc(g3d,ix) (g3d_u8(g3d,ix) &= ~(SEL_TMP | SEL_BIT))
/* set SEL_TMP and clear SEL_BIT */
#define xg3d_sel_bcts(g3d,ix) (g3d_u8(g3d,ix) = ((g3d_u8(g3d,ix) & ~SEL_BIT) | SEL_TMP))
/* clear SEL_TMP and set SEL_BIT */
#define xg3d_sel_bstc(g3d,ix) (g3d_u8(g3d,ix) = ((g3d_u8(g3d,ix) & ~SEL_TMP) | SEL_BIT))
/* check SEL_TMP and check no SEL_BIT */
#define xg3d_sel_bkntk(g3d,ix) ((g3d_u8(g3d,ix) & (SEL_TMP | SEL_BIT)) == SEL_TMP)
/* clear SEL_TMP and toggle SEL_BIT */
#define xg3d_sel_bgtc(g3d,ix) (g3d_u8(g3d,ix) = ((g3d_u8(g3d,ix) & ~SEL_TMP) ^ SEL_BIT))
/* check no SEL_TMP and check no SEL_BIT */
#define xg3d_sel_bkntkn(g3d,ix) ((g3d_u8(g3d,ix) & (SEL_TMP | SEL_BIT)) == 0)

/* clear SEL_TMP and set or clear SEL_BIT, only if SEL_TMP is set */
#define xg3d_sel_bstkc(g3d,ix) (g3d_u8(g3d,ix) = ((g3d_u8(g3d,ix) & ~SEL_TMP) | g3d_seltmp_check(g3d,ix)))
#define xg3d_sel_bctkc(g3d,ix) (g3d_u8(g3d,ix) = ((g3d_u8(g3d,ix) & ~SEL_TMP) & (255U ^ g3d_seltmp_check(g3d,ix))))
/* clear SEL_TMP and set or clear SEL_BIT, only if SEL_TMP is not set */
#define xg3d_sel_bstknc(g3d,ix) (g3d_u8(g3d,ix) = ((g3d_u8(g3d,ix) & ~SEL_TMP) | !g3d_seltmp_check(g3d,ix)))
#define xg3d_sel_bctknc(g3d,ix) (g3d_u8(g3d,ix) = ((g3d_u8(g3d,ix) & ~SEL_TMP) & (255U ^ !g3d_seltmp_check(g3d,ix))))
/* set SEL_TMP only if SEL_BIT is not set */
#define xg3d_sel_bknts(g3d,ix) (g3d_u8(g3d,ix) |= (!g3d_selbit_check(g3d,ix) << 1))

/* Neighborhood stuff */
#define NHOOD_MASK(nn,mask) (((nn) & (mask)) == (mask))

#define NHOOD_TEST_SLICE(n,nn,g3d,cx,plane,val) \
    (NHOOD_MASK(nn,nhood_slice[plane][n]) && (g3d_selbit_check(g3d,cx) == (val)))

#define NHOOD_TEST_GLOBAL(n,nn,g3d,cx,val) \
    (NHOOD_MASK(nn,nhood_global[n]) && (g3d_selbit_check(g3d,cx) == (val)))

#define NHOOD_TEMP_SLICE(n,nn,g3d,cx,plane) \
    (NHOOD_MASK(nn,nhood_slice[plane][n]) && g3d_seltmp_set(g3d,cx))

#define NHOOD_TEMP_GLOBAL(n,nn,g3d,cx) \
    (NHOOD_MASK(nn,nhood_global[n]) && g3d_seltmp_set(g3d,cx))

#define GROW_TEMP_SLICE(nn,g3d,ix,plane) ( \
           NHOOD_TEMP_SLICE( 1,nn,g3d,(ix)-1,plane) \
        && NHOOD_TEMP_SLICE( 2,nn,g3d,(ix)+1,plane) \
        && NHOOD_TEMP_SLICE( 4,nn,g3d,(ix)-(g3d)->nx,plane) \
        && NHOOD_TEMP_SLICE( 8,nn,g3d,(ix)+(g3d)->nx,plane) \
        && NHOOD_TEMP_SLICE( 5,nn,g3d,(ix)-(g3d)->nx-1,plane) \
        && NHOOD_TEMP_SLICE( 6,nn,g3d,(ix)-(g3d)->nx+1,plane) \
        && NHOOD_TEMP_SLICE( 9,nn,g3d,(ix)+(g3d)->nx-1,plane) \
        && NHOOD_TEMP_SLICE(10,nn,g3d,(ix)+(g3d)->nx+1,plane) )

#define GROW_TEMP_GLOBAL(nn,g3d,ix,dim) ( \
           NHOOD_TEMP_GLOBAL( 1,nn,g3d,(ix)-1) \
        && NHOOD_TEMP_GLOBAL( 2,nn,g3d,(ix)+1) \
        && NHOOD_TEMP_GLOBAL( 4,nn,g3d,(ix)-(g3d)->nx) \
        && NHOOD_TEMP_GLOBAL( 8,nn,g3d,(ix)+(g3d)->nx) \
        && NHOOD_TEMP_GLOBAL(16,nn,g3d,((ix)-(dim))) \
        && NHOOD_TEMP_GLOBAL(32,nn,g3d,((ix)+(dim))) \
        && NHOOD_TEMP_GLOBAL( 5,nn,g3d,(ix)-(g3d)->nx-1) \
        && NHOOD_TEMP_GLOBAL( 6,nn,g3d,(ix)-(g3d)->nx+1) \
        && NHOOD_TEMP_GLOBAL( 9,nn,g3d,(ix)+(g3d)->nx-1) \
        && NHOOD_TEMP_GLOBAL(10,nn,g3d,(ix)+(g3d)->nx+1) \
        && NHOOD_TEMP_GLOBAL(17,nn,g3d,((ix)-(dim))-1) \
        && NHOOD_TEMP_GLOBAL(18,nn,g3d,((ix)-(dim))+1) \
        && NHOOD_TEMP_GLOBAL(20,nn,g3d,((ix)-(dim))-(g3d)->nx) \
        && NHOOD_TEMP_GLOBAL(24,nn,g3d,((ix)-(dim))+(g3d)->nx) \
        && NHOOD_TEMP_GLOBAL(21,nn,g3d,((ix)-(dim))-(g3d)->nx-1) \
        && NHOOD_TEMP_GLOBAL(22,nn,g3d,((ix)-(dim))-(g3d)->nx+1) \
        && NHOOD_TEMP_GLOBAL(25,nn,g3d,((ix)-(dim))+(g3d)->nx-1) \
        && NHOOD_TEMP_GLOBAL(26,nn,g3d,((ix)-(dim))+(g3d)->nx+1) \
        && NHOOD_TEMP_GLOBAL(33,nn,g3d,((ix)+(dim))-1) \
        && NHOOD_TEMP_GLOBAL(34,nn,g3d,((ix)+(dim))+1) \
        && NHOOD_TEMP_GLOBAL(36,nn,g3d,((ix)+(dim))-(g3d)->nx) \
        && NHOOD_TEMP_GLOBAL(40,nn,g3d,((ix)+(dim))+(g3d)->nx) \
        && NHOOD_TEMP_GLOBAL(37,nn,g3d,((ix)+(dim))-(g3d)->nx-1) \
        && NHOOD_TEMP_GLOBAL(38,nn,g3d,((ix)+(dim))-(g3d)->nx+1) \
        && NHOOD_TEMP_GLOBAL(41,nn,g3d,((ix)+(dim))+(g3d)->nx-1) \
        && NHOOD_TEMP_GLOBAL(42,nn,g3d,((ix)+(dim))+(g3d)->nx+1) )

#define BORDER_TEST_SLICE(nn,g3d,ix,plane) ( \
           NHOOD_TEST_SLICE( 1,nn,g3d,(ix)-1,plane,0) \
        || NHOOD_TEST_SLICE( 2,nn,g3d,(ix)+1,plane,0) \
        || NHOOD_TEST_SLICE( 4,nn,g3d,(ix)-(g3d)->nx,plane,0) \
        || NHOOD_TEST_SLICE( 8,nn,g3d,(ix)+(g3d)->nx,plane,0) \
        || NHOOD_TEST_SLICE( 5,nn,g3d,(ix)-(g3d)->nx-1,plane,0) \
        || NHOOD_TEST_SLICE( 6,nn,g3d,(ix)-(g3d)->nx+1,plane,0) \
        || NHOOD_TEST_SLICE( 9,nn,g3d,(ix)+(g3d)->nx-1,plane,0) \
        || NHOOD_TEST_SLICE(10,nn,g3d,(ix)+(g3d)->nx+1,plane,0) )

#define BORDER_TEST_GLOBAL(nn,g3d,ix,dim) ( \
        NHOOD_TEST_GLOBAL(1,nn,g3d,(ix)-1,0) \
        || NHOOD_TEST_GLOBAL(2,nn,g3d,(ix)+1,0) \
        || NHOOD_TEST_GLOBAL(4,nn,g3d,(ix)-(g3d)->nx,0) \
        || NHOOD_TEST_GLOBAL(8,nn,g3d,(ix)+(g3d)->nx,0) \
        || NHOOD_TEST_GLOBAL(16,nn,g3d,((ix)-(dim)),0) \
        || NHOOD_TEST_GLOBAL(32,nn,g3d,((ix)+(dim)),0) \
        || NHOOD_TEST_GLOBAL(5,nn,g3d,(ix)-(g3d)->nx-1,0) \
        || NHOOD_TEST_GLOBAL(6,nn,g3d,(ix)-(g3d)->nx+1,0) \
        || NHOOD_TEST_GLOBAL(9,nn,g3d,(ix)+(g3d)->nx-1,0) \
        || NHOOD_TEST_GLOBAL(10,nn,g3d,(ix)+(g3d)->nx+1,0) \
        || NHOOD_TEST_GLOBAL(17,nn,g3d,((ix)-(dim))-1,0) \
        || NHOOD_TEST_GLOBAL(18,nn,g3d,((ix)-(dim))+1,0) \
        || NHOOD_TEST_GLOBAL(20,nn,g3d,((ix)-(dim))-(g3d)->nx,0) \
        || NHOOD_TEST_GLOBAL(24,nn,g3d,((ix)-(dim))+(g3d)->nx,0) \
        || NHOOD_TEST_GLOBAL(21,nn,g3d,((ix)-(dim))-(g3d)->nx-1,0) \
        || NHOOD_TEST_GLOBAL(22,nn,g3d,((ix)-(dim))-(g3d)->nx+1,0) \
        || NHOOD_TEST_GLOBAL(25,nn,g3d,((ix)-(dim))+(g3d)->nx-1,0) \
        || NHOOD_TEST_GLOBAL(26,nn,g3d,((ix)-(dim))+(g3d)->nx+1,0) \
        || NHOOD_TEST_GLOBAL(33,nn,g3d,((ix)+(dim))-1,0) \
        || NHOOD_TEST_GLOBAL(34,nn,g3d,((ix)+(dim))+1,0) \
        || NHOOD_TEST_GLOBAL(36,nn,g3d,((ix)+(dim))-(g3d)->nx,0) \
        || NHOOD_TEST_GLOBAL(40,nn,g3d,((ix)+(dim))+(g3d)->nx,0) \
        || NHOOD_TEST_GLOBAL(37,nn,g3d,((ix)+(dim))-(g3d)->nx-1,0) \
        || NHOOD_TEST_GLOBAL(38,nn,g3d,((ix)+(dim))-(g3d)->nx+1,0) \
        || NHOOD_TEST_GLOBAL(41,nn,g3d,((ix)+(dim))+(g3d)->nx-1,0) \
        || NHOOD_TEST_GLOBAL(42,nn,g3d,((ix)+(dim))+(g3d)->nx+1,0) )

static const uint8_t nhood_slice[3][11] = {
    { /* PLANE_XY */
        0,             /* C  */
        NN_Jm1,        /* W  */
        NN_Jp1,        /* E  */
        0,
        NN_Im1,        /* N  */
        NN_Im1|NN_Jm1, /* NW */
        NN_Im1|NN_Jp1, /* NE */
        0,
        NN_Ip1,        /* S  */
        NN_Ip1|NN_Jm1, /* SW */
        NN_Ip1|NN_Jp1  /* SE */
    },
    { /* PLANE_XZ */
        0,             /* C  */
        NN_Jm1,        /* W  */
        NN_Jp1,        /* E  */
        0,
        NN_Km1,        /* N  */
        NN_Km1|NN_Jm1, /* NW */
        NN_Km1|NN_Jp1, /* NE */
        0,
        NN_Kp1,        /* S  */
        NN_Kp1|NN_Jm1, /* SW */
        NN_Kp1|NN_Jp1  /* SE */
    },
    { /* PLANE_ZY */
        0,             /* C  */
        NN_Km1,        /* W  */
        NN_Kp1,        /* E  */
        0,
        NN_Im1,        /* N  */
        NN_Im1|NN_Km1, /* NW */
        NN_Im1|NN_Kp1, /* NE */
        0,
        NN_Ip1,        /* S  */
        NN_Ip1|NN_Km1, /* SW */
        NN_Ip1|NN_Kp1  /* SE */
    }
};

static const uint8_t nhood_global[43] = {
    0,                     /* C   */
    NN_Jm1,                /* W   */
    NN_Jp1,                /* E   */
    0,
    NN_Im1,                /* N   */
    NN_Im1|NN_Jm1,         /* NW  */
    NN_Im1|NN_Jp1,         /* NE  */
    0,
    NN_Ip1,                /* S   */
    NN_Ip1|NN_Jm1,         /* SW  */
    NN_Ip1|NN_Jp1,         /* SE  */
    0, 0, 0, 0, 0,
    NN_Km1,                /* B   */
    NN_Km1|NN_Jm1,         /* BW  */
    NN_Km1|NN_Jp1,         /* BE  */
    0,
    NN_Km1|NN_Im1,         /* BN  */
    NN_Km1|NN_Im1|NN_Jm1,  /* BNW */
    NN_Km1|NN_Im1|NN_Jp1,  /* BNE */
    0,
    NN_Km1|NN_Ip1,         /* BS  */
    NN_Km1|NN_Ip1|NN_Jm1,  /* BSW */
    NN_Km1|NN_Ip1|NN_Jp1,  /* BSE */
    0, 0, 0, 0, 0,
    NN_Kp1,                /* F   */
    NN_Kp1|NN_Jm1,         /* FW  */
    NN_Kp1|NN_Jp1,         /* FE  */
    0,
    NN_Kp1|NN_Im1,         /* FN  */
    NN_Kp1|NN_Im1|NN_Jm1,  /* FNW */
    NN_Kp1|NN_Im1|NN_Jp1,  /* FNE */
    0,
    NN_Kp1|NN_Ip1,         /* FS  */
    NN_Kp1|NN_Ip1|NN_Jm1,  /* FSW */
    NN_Kp1|NN_Ip1|NN_Jp1   /* FSE */
};

/* Selection operation mode */
enum xg3d_sel_op_mode {
    SEL_OP_UNSET = -1,
    SEL_OP_SET = 1,
    SEL_OP_TMP_UNSET = -2,
    SEL_OP_TMP_SET = 2
};

/* CA selection stuff */
typedef struct _lili { /* linked list */
    index_t ix;        /* absolute position in data */
    struct _lili *next;
} lili;

inline static lili* lili_alloc(const index_t ix) {
    lili *l = malloc(sizeof(lili));
    l->ix = ix;
    l->next = NULL;
    return l;
}

struct selection_cargs {
    lili *stack;
    grid3d *selection;
    const grid3d *data;
    const g3d_quant_info *info;
    uint8_t sel;
    uint8_t nnmask;
    index_t nvx;
    double y,t,c;
    double val;
    uint8_t lvl;
    coord_t nx;
    coord_t ny;
    slice_t dim;
};

/* SECTION: Selection mode functions */
typedef void (_selmodef)(grid3d*,const grid3d*,const int,const enum g3d_slice_plane,const int8_t,const g3d_quant_info *qinfo,index_t*,double*);
_selmodef sel_mode_fill;
_selmodef sel_mode_level;
_selmodef sel_mode_level_comp;
_selmodef sel_mode_random;
_selmodef sel_mode_contour;

/* SECTION: Draw cursors in XBM */
typedef void (_selcurf)(unsigned char*,const unsigned int,const unsigned int,const unsigned int,const int,const int);
_selcurf sel_cursor_dot;
_selcurf sel_cursor_square;
_selcurf sel_cursor_circle;
_selcurf sel_cursor_diamond;
_selcurf sel_cursor_line_horiz;
_selcurf sel_cursor_line_vert;
_selcurf sel_cursor_line_diag;
_selcurf sel_cursor_line_antidiag;

/* SECTION: Draw brushes in selection */
enum xg3d_selection_brush {
    SEL_BRUSH_SQUARE = 1,
    SEL_BRUSH_CIRCLE,
    SEL_BRUSH_DIAMOND,
    SEL_BRUSH_HLINE,
    SEL_BRUSH_VLINE,
    SEL_BRUSH_DIAG,
    SEL_BRUSH_ANTIDIAG
};

typedef void (_selbrushf)(const coord_t,const coord_t,const coord_t,const enum xg3d_sel_op_mode,enum g3d_slice_plane,const bool,const unsigned int,const coord_t,const coord_t,_selopf*,void*);
_selbrushf sel_brush_dot;
_selbrushf sel_brush_square;
_selbrushf sel_brush_circle;
_selbrushf sel_brush_diamond;
_selbrushf sel_brush_line_horiz;
_selbrushf sel_brush_line_vert;
_selbrushf sel_brush_line_diag;
_selbrushf sel_brush_line_antidiag;

/* SECTION: Draw shapes in selection */
enum xg3d_selection_shape {
    SEL_SHAPE_BOX = 1,
    SEL_SHAPE_SPHERE,
    SEL_SHAPE_ELLIPSOID,
    SEL_SHAPE_CYLINDER,
    SEL_SHAPE_LINE,
    SEL_SHAPE_PLANE
};

typedef void (_selshapef)(grid3d*,const grid3d*,coord_t,coord_t,coord_t,coord_t,coord_t,coord_t,const double,const double,const double,const enum xg3d_sel_op_mode,index_t*,double*);
_selshapef sel_shape_box;
_selshapef sel_shape_sphere;
_selshapef sel_shape_ellipsoid;
_selshapef sel_shape_cylinder;
_selshapef sel_shape_line;
_selshapef sel_shape_plane;

/* SECTION: Operations in selection */
typedef void (_seloperf)(grid3d*,const grid3d*,const int,const enum g3d_slice_plane,const double,const double,index_t*,double*);
_seloperf sel_oper_count;
_seloperf sel_oper_move;
_seloperf sel_oper_invert;
_seloperf sel_oper_grow;
_seloperf sel_oper_shrink;
_seloperf sel_oper_border;
_seloperf sel_oper_fillin;
_seloperf sel_oper_smooth;
_seloperf sel_oper_cnvxhull;
_seloperf sel_oper_histclamp;
_seloperf sel_oper_sphcover;

/* SECTION: Draw with selection markers */
typedef int (_seldrawf)(grid3d*,const grid3d*,const marker*,const enum g3d_slice_plane,const coord_t,const coord_t,const coord_t,const bool,index_t*,double*);
_seldrawf sel_draw_polygon;
_seldrawf sel_draw_circle3p;
_seldrawf sel_draw_ellipse5p;
_seldrawf sel_draw_lines;
_seldrawf sel_draw_points;
_seldrawf sel_draw_brushpath;

/* SECTION: Utility functions */
g3d_ret sel_clear(grid3d*);
g3d_ret sel_recal(const grid3d*,const grid3d*,index_t* restrict,index_t* restrict,double* restrict,double* restrict,double* restrict,double* restrict,double* restrict);
gsl_histogram* sel_get_histogram(const grid3d*,const grid3d*);

#endif
