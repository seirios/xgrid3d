#include "dosxyzpp_c.h"
#include "dosxyzpp.hpp"

extern "C" {
    dosxyzpp* dosxyzpp_new(void) {
        const char *argv = {"dosxyzpp"};
        return new DOSXYZpp_Application(1,(char**)&argv);
    }

    void dosxyzpp_delete(dosxyzpp *x) {
        delete x;
    }

    stringMACROdefine(Input);
    stringMACROdefine(HenHouse);
    stringMACROdefine(EgsHome);
    stringMACROdefine(PegsFile);
    stringMACROdefine(InputFile);
    stringMACROdefine(OutputFile);
    stringMACROdefine(AppName);
    stringMACROdefine(AppDir);
    stringMACROdefine(RunDir);

    const char* dosxyzpp_getRunDir(dosxyzpp *x) {
        return x->getRunDir().c_str();
    }

    void dosxyzpp_setParallel(dosxyzpp *x, long npar, long ipar) {
        x->setParallel(npar,ipar);
    }

    voidMACROdefine(Batch);
    voidMACROdefine(Quiet);
    voidMACROdefine(SimpleRun);

    intMACROdefine(initGeom);
    intMACROdefine(initSimulation);
    intMACROdefine(runSimulation);
    intMACROdefine(finishSimulation);

    double dosxyzpp_getEtot(dosxyzpp *x) {
        return x->getEtot();
    }

    double dosxyzpp_getMass(dosxyzpp *x, int ir) {
        return x->getMass(ir);
    }

    void dosxyzpp_getDoseScore(dosxyzpp *x, int ir, double *sum, double *sum2) {
        double s,s2;
        x->getDoseScore(ir,s,s2);
        *sum = s;
        *sum2 = s2;
    }

    int dosxyzpp_getNRegions(dosxyzpp *x) {
        return x->getNRegions();
    }

    long dosxyzpp_getNCase(dosxyzpp *x) {
        return x->getNCase();
    }
}
