#ifndef XG3D_surface_cb_h_
#define XG3D_surface_cb_h_

#include "fd_surface.h"
#include "xgrid3d.h"

#define SURFACE_GPLOT(fd_surface) (((FD_surface*)(fd_surface))->canvas->u_vdata)

void update_surface_plot(xg3d_env*);

#endif
