#ifndef XG3D_overview_h_
#define XG3D_overview_h_

#include "xgrid3d.h"
#include "grid3d_visual.h"

#define OVC_MAX_ZOOM 8

#define OV_PAD 5
#define OV_TOP_PAD 8
#define OV_LEFT_MARGIN 30
#define OV_THICK 15
#define OV_SLIDER_THICK 20
#define OV_STATUS_H 25
#define OV_TOMAIN_W 75
#define OV_FRAME_LABEL_PAD 15
#define OV_TOP_FRAME_PAD 3

#define OV_LEFT_FRAME_MARGIN (OV_LEFT_MARGIN+OV_PAD)
#define OV_BOT_MARGIN (OV_STATUS_H+OV_TOP_PAD)

#define OV_MIN_WIDTH 500
#define OV_MAX_WIDTH (int)(1.0*fl_scrw)
#define OV_MAX_HEIGHT (int)(0.9*fl_scrh)

#define OV_FRAME_MIN_WIDTH 220

#define OVERVIEW_OLDPOS(fd_overview) (((FD_overview*)(fd_overview))->overview->u_ldata)
#define OVERVIEW_VIEWPORTS(fd_overview) ((FD_viewport**)(((FD_overview*)(fd_overview))->vdata))

void update_all_positioners(FD_viewport**,FD_overview*);
void draw_ov_sel_voxel(FD_viewport*,FD_selection*,const coord_t,const coord_t);
void draw_ov_marker(FD_viewport*,const coord_t,const coord_t,const FL_COLOR);
void ov_restore_image_voxel(FD_viewport*,FD_selection*,const coord_t,const coord_t);

void replace_ov_slice(FD_viewport*,const enum xg3d_layer);
void replace_ov_slices(FD_viewport**,const enum xg3d_layer);
void replace_ov_quantslice(FD_viewport*,FD_quantization*,const enum xg3d_layer);
void replace_ov_quantslices(FD_viewport**,FD_quantization*,const enum xg3d_layer);
void replace_ov_img(FD_viewport*,FD_quantization*,const enum xg3d_layer);
void replace_ov_images(FD_viewport**,FD_quantization*,const enum xg3d_layer);
void refresh_ov_canvas(xg3d_env*,const enum g3d_slice_plane);
void refresh_ov_canvases(FD_overview*);
void resize_ov_canvases(xg3d_env*);

bool formbrowser_overflow(FL_OBJECT*,const bool);
int cb_overview_canvas(FL_OBJECT*,Window,int,int,XEvent*,void*);

#endif
