#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "cb_function.h"
#include "cb_datasets.h"
#include "cb_info.h"

#include "io.h"
#include "matheval.h"

#include "util.h"

#define FMT_VAR "%s"
#define FMT_DATA "%s"
#define FMT_PARAM "%.13g"
#define FMT_SEP " → "

/* Function workspace */
void function_workspace_clear(xg3d_function_workspace *x, bool delete) {
    if(x != NULL) {
        for(size_t i=0;i<x->nvar;i++) {
            free(x->vars[i]->symbol);
            free(x->vars[i]);
        }
        free(x->vars); x->vars = NULL;
        x->nvar = 0;
        free(x->string); x->string = NULL;
        xg3d_data_clear(x->preview_data,true);
        x->preview_data = NULL;
        if(delete) free(x);
    }
}

static void refresh_variables_browser(FD_function *function) {
    fl_clear_browser(function->browser);
    for(size_t i=0;i<FUNCTION_NVAR(function);i++) {
        struct fun_var *var = FUNCTION_VARS(function)[i];
        switch(var->type) {
            case VAR_DATA:
                fl_add_browser_line_f(function->browser,FMT_VAR FMT_SEP FMT_DATA,
                        var->symbol,var->value.data.data != NULL ? var->value.data.data->name : _("(unassigned)"));
                break;
            case VAR_PARAM:
                fl_add_browser_line_f(function->browser,FMT_VAR FMT_SEP FMT_PARAM,
                        var->symbol,var->value.param.value);
                break;
        }
    }
    deactivate_obj(function->tf_vartype);
}

static void process_variables(FD_function *function, char **vars, const int nvar) {
    xg3d_function_workspace *fwk = FUNCTION_WORKSPACE(function);

    struct fun_var **new_vars = malloc(nvar * sizeof(struct fun_var*));
    for(int i=0;i<nvar;i++) {
        new_vars[i] = calloc(1,sizeof(struct fun_var));
        new_vars[i]->symbol = strdup(vars[i]);

        /* keep variables already present */
        for(size_t j=0;j<fwk->nvar;j++)
            if(strcmp(fwk->vars[j]->symbol,new_vars[i]->symbol) == 0) {
                new_vars[i]->type = fwk->vars[j]->type;
                switch(new_vars[i]->type) {
                    case VAR_DATA:
                        new_vars[i]->value.data.data = fwk->vars[j]->value.data.data;
                        break;
                    case VAR_PARAM:
                        new_vars[i]->value.param.left = fwk->vars[j]->value.param.left;
                        new_vars[i]->value.param.value = fwk->vars[j]->value.param.value;
                        new_vars[i]->value.param.right = fwk->vars[j]->value.param.right;
                        break;
                }
                break;
            }
    }

    for(size_t j=0;j<fwk->nvar;j++)
        free(fwk->vars[j]);
    free(fwk->vars);
    fwk->vars = new_vars;
    fwk->nvar = nvar;
}

void cb_fun_input ( FL_OBJECT * obj  , long data  ) {
    (void)data;

    char **string = (char**)&FUNCTION_STRING(GUI->function);
    const char* tval = fl_get_input(obj);
    void *f;

    if(tval == NULL || strlen(tval) == 0 || (f = evaluator_create((char*)tval)) == NULL ) {
        fl_set_input(obj,*string == NULL ? "" : *string);
        fl_show_alert2(0,_("Invalid input!\fPlease enter a valid function."));
        return;
    }
    char **vars; int nvar; evaluator_get_variables(f,&vars,&nvar);
    process_variables(GUI->function,vars,nvar);
    evaluator_destroy(f);
    refresh_variables_browser(GUI->function);

    if(*string != NULL) free(*string);
    *string = strdup(tval);
}

void cb_fun_browser ( FL_OBJECT * obj  , long data  ) {
    (void)data;

    const int i = fl_get_browser(obj) - 1; if(i < 0) return;
    struct fun_var *var = FUNCTION_VARS(GUI->function)[i];

    activate_obj(GUI->function->tf_vartype,FL_MCOL);
    fl_set_folder_bynumber(GUI->function->tf_vartype,var->type+1);
    fl_call_object_callback(GUI->function->tf_vartype);
}

void cb_fun_vartype ( FL_OBJECT * obj  , long data  ) {
    (void)data;

    const int i = fl_get_browser(GUI->function->browser) - 1; if(i < 0) return;
    struct fun_var *var = FUNCTION_VARS(GUI->function)[i];

    switch(fl_get_active_folder_number(obj)-1) {
        case VAR_DATA:
            /* reset if changing type */
            if(var->type == VAR_PARAM)
                var->value.data.data = NULL;
            refresh_datasets_browser(GUI->fun_data->browser,ENV->data);
            fl_replace_browser_line_f(GUI->function->browser,i+1,FMT_VAR FMT_SEP FMT_DATA,
                    var->symbol,var->value.data.data != NULL ? var->value.data.data->name : _("(unassigned)"));
            var->type = VAR_DATA;
            break;
        case VAR_PARAM:
            /* reset if changing type */
            if(var->type == VAR_DATA) {
                var->value.param.left = 0.0;
                var->value.param.value = 0.5;
                var->value.param.right = 1.0;
            }
            fl_set_slider_bounds(GUI->fun_param->slider,var->value.param.left,var->value.param.right);
            fl_set_slider_value(GUI->fun_param->slider,var->value.param.value);
            fl_set_input_f(GUI->fun_param->inp_left,FMT_PARAM,var->value.param.left);
            fl_set_input_f(GUI->fun_param->inp_value,FMT_PARAM,var->value.param.value);
            fl_set_input_f(GUI->fun_param->inp_right,FMT_PARAM,var->value.param.right);
            fl_replace_browser_line_f(GUI->function->browser,i+1,FMT_VAR FMT_SEP FMT_PARAM,
                    var->symbol,var->value.param.value);
            var->type = VAR_PARAM;
            break;
    }
    fl_select_browser_line(GUI->function->browser,i+1);
}

void cb_fun_data_browser ( FL_OBJECT * obj  , long data  ) {
    (void)data;

    const int i = fl_get_browser(GUI->function->browser) - 1; if(i < 0) return;
    struct fun_var *var = FUNCTION_VARS(GUI->function)[i];

    const int j = fl_get_browser(obj) - 1; if(j < 0) return;
    var->value.data.data = ENV->data->sets[j];
    fl_replace_browser_line_f(GUI->function->browser,i+1,FMT_VAR FMT_SEP FMT_DATA,
            var->symbol,var->value.data.data->name);
}

void cb_fun_param_slider ( FL_OBJECT * obj  , long data  ) {
    (void)data;

    const int i = fl_get_browser(GUI->function->browser) - 1; if(i < 0) return;
    struct fun_var *var = FUNCTION_VARS(GUI->function)[i];

    double val = fl_get_slider_value(obj);
    var->value.param.value = val;
    fl_replace_browser_line_f(GUI->function->browser,i+1,FMT_VAR FMT_SEP FMT_PARAM,
            var->symbol,var->value.param.value);
    fl_set_input_f(GUI->fun_param->inp_value,FMT_PARAM,val);
}

void cb_fun_param_inputs ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *const slider = GUI->fun_param->slider;
    const char *tval = fl_get_input(obj);

    const int i = fl_get_browser(GUI->function->browser) - 1; if(i < 0) return;
    struct fun_var *var = FUNCTION_VARS(GUI->function)[i];

    double left,right; fl_get_slider_bounds(slider,&left,&right);

    switch(data) {
        case 0: /* value */
            {
                double value = strtod_checked(tval,var->value.param.value);
                if(left <= right) {
                    if(value < left) left = value;
                    else if(value > right) right = value;
                } else if(right < left) {
                    if(value < right) right = value;
                    else if(value > left) left = value;
                }
                fl_set_slider_bounds(slider,left,right);
                fl_set_input_f(GUI->fun_param->inp_left,FMT_PARAM,left);
                fl_set_input_f(GUI->fun_param->inp_right,FMT_PARAM,right);
                fl_set_slider_value(slider,value);
            }
            break;
        case 1: /* min */
            {
                double left = strtod_checked(tval,var->value.param.left);
                var->value.param.left = left;
                fl_set_slider_bounds(slider,left,right);
            }
            break;
        case 2: /* max */
            {
                double right = strtod_checked(tval,var->value.param.right);
                var->value.param.right = right;
                fl_set_slider_bounds(slider,left,right);
            }
            break;
    }
    fl_call_object_callback(slider);
}

void cb_fun_compute ( FL_OBJECT * obj  , long data  ) {
    (void)data;

    xg3d_function_workspace *fwk = FUNCTION_WORKSPACE(GUI->function);
    if(fwk->string == NULL) return;

    slice_t nx = 0, ny = 0, nz = 0;
    size_t nvardata = 0;
    size_t nvarparam = 0;
    for(size_t i=0;i<fwk->nvar;i++) {
        struct fun_var *var = fwk->vars[i];
        switch(var->type) {
            case VAR_DATA:
                if(var->value.data.data == NULL) {
                    fl_show_alert2(0,_("Unassigned variables present!\fPlease assign all data variables to datasets."));
                    return;
                }
                /* get dimensions of first data variable */
                if(nvardata == 0) {
                    nx = var->value.data.data->g3d->nx;
                    ny = var->value.data.data->g3d->ny;
                    nz = var->value.data.data->g3d->nz;
                } else if(var->value.data.data->g3d->nx != nx
                        || var->value.data.data->g3d->ny != ny
                        || var->value.data.data->g3d->nz != nz) {
                    fl_show_alert2(0,_("Dimensions mismatch!\fData variables must be assigned to datasets of equal dimensions."));
                    return;
                }
                nvardata++;
                break;
            case VAR_PARAM:
                nvarparam++;
                break;
        }
    }

    if(nvardata == 0) {
        fl_show_alert2(0,_("No data variables found!\fPlease assign at least one data variable."));
        return;
    }

    /* retrieve index of variables according to type */
    size_t *ixdata = malloc(nvardata * sizeof(size_t));
    size_t *ixparam = malloc(nvarparam * sizeof(size_t));
    {
        size_t d = 0,p = 0;
        for(size_t i=0;i<fwk->nvar;i++)
            switch(fwk->vars[i]->type) {
                case VAR_DATA:   ixdata[d++] = i; break;
                case VAR_PARAM: ixparam[p++] = i; break;
            }
    }

    fl_show_oneliner(_("Computing function..."),fl_scrw/2,fl_scrh/2);
    const int doble = fl_get_button(GUI->function->btn_prec_double);
    grid3d *res = g3d_alloc(doble ? G3D_FLOAT64 : G3D_FLOAT32,nx,ny,nz,.alloc=true);
#ifdef _OPENMP
#pragma omp parallel
    {
#endif
        void *f = evaluator_create((char*)fwk->string);
        char **vars; int nvar; evaluator_get_variables(f,&vars,&nvar);
        double *vals = malloc(nvar * sizeof(double));
        /* set parameter variables */
        for(size_t i=0;i<nvarparam;i++)
            vals[ixparam[i]] = fwk->vars[ixparam[i]]->value.param.value;
#ifdef _OPENMP
#pragma omp for
#endif
        for(index_t ix=0;ix<res->nvox;ix++) {
            /* set data variables */
            for(size_t i=0;i<nvardata;i++)
                vals[ixdata[i]] = g3d_get_dvalue(fwk->vars[ixdata[i]]->value.data.data->g3d,ix);
            g3d_set_dvalue(res,ix,evaluator_evaluate(f,nvar,vars,vals));
        }
        free(vals);
        evaluator_destroy(f);
#ifdef _OPENMP
    }
#endif
    free(ixdata);
    fl_hide_oneliner();

    {
        xg3d_data *dat = calloc(1,sizeof(xg3d_data));

        { /* put parameter values in dataset name */
            char buf[1024];
            snprintf(buf,sizeof(buf),"%s",fwk->string);
            for(size_t i=0;i<nvarparam;i++) {
                size_t len = strlen(buf);
                snprintf(buf+len,sizeof(buf)-len,"; %s = %g",
                        fwk->vars[ixparam[i]]->symbol,fwk->vars[ixparam[i]]->value.param.value);
            }
            free(ixparam);
            dat->name = strdup(buf);
        }

        xg3d_header *hdr = calloc(1,sizeof(xg3d_header));
        hdr->type = doble ? RAW_FLOAT64 : RAW_FLOAT32;
        hdr->file = strdup(_("INTERNAL"));
        hdr->vx = hdr->vy = hdr->vz = 0.1;
        hdr->dim[0] = res->nx;
        hdr->dim[1] = res->ny;
        hdr->dim[2] = res->nz;
        hdr->cal_slope = 1.0;
        hdr->cal_inter = 0.0;
        hdr->acqmod = ACQ_UNKNOWN;
        dat->hdr = hdr;
        dat->g3d = res;
        xg3d_datasets_add(ENV->data,dat);
    }

    refresh_datasets_browser(GUI->datasets->browser,ENV->data);
    {
        int ilast = fl_get_browser_maxline(GUI->datasets->browser);
        fl_select_browser_line(GUI->datasets->browser,ilast);
        {
            FD_info *info = fl_get_formbrowser_form(GUI->datasets->fb_info,1)->fdui;
            setup_info(info,ENV->data->sets[ilast-1]->hdr);
        }
        setup_attrs(ENV,ilast-1);
    }
}

#define FUN_BUFSIZ 256
void cb_fun_preset ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    char buf[FUN_BUFSIZ] = { 0 };
    char var[32];
    const size_t nsets = ENV->data->nsets;
    size_t nleft = FUN_BUFSIZ - 1;

    int item = fl_get_menu(obj);
    switch(item) {
        case 1: // Absolute error
            fl_set_input(GUI->function->inp_fun,"abs(y-x)");
            break;
        case 2: // Squared error
            fl_set_input(GUI->function->inp_fun,"(y-x)^2");
            break;
        case 3: // Sum (all)
        case 4: // Product (all)
            if(nsets == 0) return;
            for(size_t i = 0; i < nsets && nleft > 0; ++i) {
                snprintf(var,sizeof(var),"x%zu",i + 1);
                size_t varlen = strlen(var);
                if(i > 0) {
                    if(nleft >= 1) {
                        strcat(buf,(item == 3) ? "+" : "*");
                        nleft--;
                    } else break;
                }
                if(nleft >= varlen) {
                    strcat(buf,var);
                    nleft -= varlen;
                } else break;
            }
            buf[FUN_BUFSIZ - 1] = '\0';
            fl_set_input(GUI->function->inp_fun,buf);
            break;
    }
    fl_call_object_callback(GUI->function->inp_fun);
}
#undef FUN_BUFSIZ

void cb_fun_preview ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    FL_FORM *form;
    char buf[1024];

    form = GUI->fun_preview->overview;
    snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Function preview"));
    if(fl_form_is_visible(form)) {
        fl_hide_form(form);
        fl_deactivate_form(form);
    } else {
        fl_activate_form(form);
        fl_show_form(form,FL_PLACE_SIZE,FL_FULLBORDER,buf);
        fl_raise_form(form);
        fl_winfocus(form->window);
    }
}

