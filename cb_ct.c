#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "cb_ct.h"
#include "cb_datasets.h"

#include "util.h"
#include "util_units.h"
#include "ctheader.h"
#include "astra_reco.h"

void cb_ct_clamp_toggle ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *inp;
    switch(data) {
        default:
        case 0: /* min */
            inp = GUI->ct->inp_clamp_min;
            break;
        case 1: /* max */
            inp = GUI->ct->inp_clamp_max;
            break;
    }
    if(fl_get_button(obj))
        activate_obj(inp,FL_COL1);
    else
        deactivate_obj(inp);
}

void cb_ct_load_conebeam ( FL_OBJECT * obj  , long data  ) {
    (void)data;

    const char *file = fl_show_fselector(_("Open CT header file..."),"","*","");
    if(file == NULL) return;

    CT_FILE_HEADER cthdr;
    if(ctheader_read(file,&cthdr)) {
        fl_show_alert2(0,_("Failed loading CT header file\f%s"),file);
        return;
    }

    fl_set_input_f(GUI->ct->inp_pix,"%g",cthdr.PixelSize);
    fl_set_input_f(GUI->ct->inp_R,"%g",cthdr.ctgeom.R);
    fl_set_input_f(GUI->ct->inp_D,"%g",cthdr.ctgeom.focal);
    fl_set_input_f(GUI->ct->inp_u0,"%g",cthdr.ctgeom.u_0);
    fl_set_input_f(GUI->ct->inp_v0,"%g",cthdr.ctgeom.v_0);
    fl_set_input_f(GUI->ct->inp_twist,"%g",cthdr.ctgeom.twist);
    fl_set_input_f(GUI->ct->inp_slant,"%g",cthdr.ctgeom.slant);
    fl_set_input_f(GUI->ct->inp_tilt,"%g",cthdr.ctgeom.tilt);
}

void cb_ct_start ( FL_OBJECT * obj  , long data  ) {
    (void)data;

#ifndef ASTRA
    fl_show_alert2(0,_("ASTRA support disabled!\fPlease enable ASTRA support at compile time."));
    return;
#endif

    const grid3d *sino = NULL;
    if(ENV->data->sino != NULL) sino = ENV->data->sino->g3d;
    if(sino == NULL) {
        fl_show_alert2(0,_("No sinogram assigned!\fPlease assign a sinogram first."));
        return;
    } else if(sino->type != G3D_FLOAT32) {
        fl_show_alert2(0,_("Wrong sinogram type!\fPlease make sure sinogram is float32."));
        return;
    }

    fl_show_oneliner(_("Reconstructing image..."),fl_scrw/2,fl_scrh/2);

    /* build CT header from sinogram and input fields */
    CT_FILE_HEADER cthdr;

    cthdr.NProy = sino->ny;
    cthdr.Pixels_X = sino->nx;
    cthdr.Pixels_Z = sino->nz;
    cthdr.PixelSize = strtod_checked(fl_get_input(GUI->ct->inp_pix),1.0);

    cthdr.ctgeom.R = strtod_checked(fl_get_input(GUI->ct->inp_R),100.0);
    cthdr.ctgeom.focal = strtod_checked(fl_get_input(GUI->ct->inp_D),100.0);
    cthdr.ctgeom.u_0 = strtod_checked(fl_get_input(GUI->ct->inp_u0),0.0);
    cthdr.ctgeom.v_0 = strtod_checked(fl_get_input(GUI->ct->inp_v0),0.0);
    cthdr.ctgeom.twist = strtod_checked(fl_get_input(GUI->ct->inp_twist),0.0);
    cthdr.ctgeom.slant = strtod_checked(fl_get_input(GUI->ct->inp_slant),0.0);
    cthdr.ctgeom.tilt = strtod_checked(fl_get_input(GUI->ct->inp_tilt),0.0);

    /* get sinogram data */
    const float *sino_data = sino->dat;

    /* build str_geom from input fields */
    unsigned int xres = fl_get_spinner_value(GUI->ct->spin_nx);
    unsigned int yres = fl_get_spinner_value(GUI->ct->spin_ny);
    unsigned int zres = fl_get_spinner_value(GUI->ct->spin_nz);
    double voxel_size = fl_get_spinner_value(GUI->ct->spin_vox);
    double xoff = fl_get_spinner_value(GUI->ct->spin_cx);
    double yoff = fl_get_spinner_value(GUI->ct->spin_cy);
    double zoff = fl_get_spinner_value(GUI->ct->spin_cz);
    char str_geom[1024];
    snprintf(str_geom,sizeof(str_geom),"%ux%ux%u@%g%+g%+g%+g",
            xres,yres,zres,voxel_size,xoff,yoff,zoff);

    /* get iterations, constraints and flags from CT form */
    const unsigned int iter = fl_get_spinner_value(GUI->ct->spin_iter);
    const bool clampMin = fl_get_button(GUI->ct->btn_clamp_min);
    const double min = strtod_checked(fl_get_input(GUI->ct->inp_clamp_min),0.0);
    const bool clampMax = fl_get_button(GUI->ct->btn_clamp_max);
    const double max = strtod_checked(fl_get_input(GUI->ct->inp_clamp_max),1.0);
    const bool pause = fl_get_button(GUI->ct->btn_pause);

    float *reco;
    const char *str_algo;
    switch(fl_get_choice(GUI->ct->ch_algo)) {
        default:
        case 1: /* SIRT3D */
            str_algo = "SIRT3D";
#ifdef ASTRA
            reco = astra_reco_SIRT3D(&cthdr,sino_data,str_geom,iter,clampMin,min,clampMax,max,.pause=pause,.debug=true,.size_out=NULL);
#else
            (void)pause;
(void)max;
(void)clampMax;
(void)min;
(void)clampMin;
(void)iter;
(void)sino_data;
(void)cthdr;
            reco = NULL;
#endif
            break;
        case 2: /* FDK */
            str_algo = "FDK";
#ifdef ASTRA
            reco = astra_reco_FDK(&cthdr,sino_data,str_geom,.debug=true,.size_out=NULL);
#else
            (void)pause;
(void)max;
(void)clampMax;
(void)min;
(void)clampMin;
(void)iter;
(void)sino_data;
(void)cthdr;
            reco = NULL;
#endif
            break;
    }
    if(reco == NULL) {
        fl_show_alert2(0,_("%s reconstruction failed!\fPlease check parameters."),str_algo);
        return;
    }

    xg3d_data *const dat_reco = calloc(1,sizeof(xg3d_data));
    char namebuf[2048];
    snprintf(namebuf,sizeof(namebuf),"%s reconstruction of %s",str_algo,ENV->data->sino->name);
    dat_reco->name = strdup(namebuf);
    dat_reco->hdr = calloc(1,sizeof(xg3d_header));
    dat_reco->hdr->type = RAW_FLOAT32;
    dat_reco->hdr->file = strdup(_("INTERNAL"));
    dat_reco->hdr->vx = dat_reco->hdr->vy = dat_reco->hdr->vz = voxel_size * MM_TO_CM;
    dat_reco->hdr->dim[0] = xres;
    dat_reco->hdr->dim[1] = yres;
    dat_reco->hdr->dim[2] = zres;
    dat_reco->hdr->cal_slope = 1.0;
    dat_reco->hdr->cal_inter = 0.0;
    dat_reco->hdr->acqmod = ACQ_CT;
    dat_reco->g3d = g3d_alloc(G3D_FLOAT32,xres,yres,zres,.alloc=false);
    dat_reco->g3d->dat = reco;
    dat_reco->stats = g3d_stats_get(dat_reco->g3d,NULL,G3D_STATS_MINMAXSUM);
    xg3d_datasets_add(ENV->data,dat_reco);
    refresh_datasets_browser(GUI->datasets->browser,ENV->data);

    fl_hide_oneliner();
}
