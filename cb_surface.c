#include "cb_surface.h"
#include "cb_viewport.h"
#include "cb_mainview.h"
#include "cb_coloring.h"
#include "cb_quantization.h"

#include <gsl/gsl_statistics_uchar.h>

#include "gnuplot_i.h"

#include "util_coordinates.h"

/* in gnuplot v5.0p4 no problems with pm3d on empty slices */
#undef ENV
void update_surface_plot(xg3d_env *ENV) {
    const unsigned int nlvl = QUANTIZATION_NLEVELS(GUI->quantization);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const grid3d *quantslice = VIEW_QUANT_SLICE(GUI->viewport);
    const coord_t k = MAINVIEW_GET_K(GUI->mainview,plane);
    const xg3d_lut *lut = COLORING_LUT(GUI->coloring);
    static const char axes[3] = {'X','Y','Z'};
    gnuplot_ctrl *gplot = SURFACE_GPLOT(GUI->surface);
    char buf[1024], palbuf[8192];
    uint8_t min,max;

    if(gplot == NULL) return;

    gsl_stats_uchar_minmax(&min,&max,(unsigned char*)quantslice->dat,1,quantslice->nvox);

    gnuplot_cmd(gplot,"set term x11 noreplotonresize ctrlq window '%""#lx'",FL_ObjWin(GUI->surface->canvas));
    gnuplot_cmd(gplot,"set xr [0:%u]",quantslice->nx-1);
    gnuplot_cmd(gplot,"set yr [%u:0]",quantslice->ny-1);
    gnuplot_cmd(gplot,"set zr [0.9:%u]",nlvl-1);

    /* labels */
    snprintf(buf,sizeof(buf),"%c%c %s %"PRIcoord,axes[PLANE_TO_J(plane)],axes[PLANE_TO_I(plane)],_("slice"),k);
    gnuplot_cmd(gplot,"set xlab '%c'",axes[PLANE_TO_J(plane)]);
    gnuplot_cmd(gplot,"set ylab '%c'",axes[PLANE_TO_I(plane)]);

    if(max > min) {
        /* color palette */
        unsigned int c = 0;
        for(unsigned int i=min;i<=max;i++) {
            snprintf(palbuf+14*c,15,"%3u '#%.2x%.2x%.2x',",c,
                    FL_GETR(lut->colors[i]),FL_GETG(lut->colors[i]),FL_GETB(lut->colors[i]));
            c++;
        }
        palbuf[14*c-1] = '\0';
        gnuplot_cmd(gplot,"set palette model RGB maxcolors %u defined (%s)",c,palbuf);
        gnuplot_cmd(gplot,"set cbr [%d:%d]",min,max);

        /* bottom plane */
        gnuplot_cmd(gplot,"set object 1 polygon from 0,0,0 to %u,0,0 to %u,%u,0 to 0,%u,0 to 0,0,0 fs solid nobo fc pal frac 0 behind",
                quantslice->nx-1,quantslice->nx-1,quantslice->ny-1,quantslice->ny-1);

        /* plot */
        gnuplot_cmd(gplot,"splot '-' volatile binary array=(%zu,%zu) format='%%uint8' title '%s' w pm3d",
                quantslice->nx,quantslice->ny,buf);
        fwrite(quantslice->dat,sizeof(uint8_t),quantslice->nvox,gplot->gnucmd);
        fflush(gplot->gnucmd);
    } else {
        gnuplot_cmd(gplot,"set cbr [0:1]");
        gnuplot_cmd(gplot,"set palette rgbformulae 0,0,0");
        gnuplot_cmd(gplot,"splot 0 title '%s' w pm3d",buf);
    }
}
#define ENV _default_env
