/* Header file generated by fdesign on Sat Sep 28 21:14:16 2024 */

#ifndef FD_overview_h_
#define FD_overview_h_

#include <forms.h>

#if defined __cplusplus
extern "C"
{
#endif

/* Callbacks, globals and object handlers */

FL_CALLBACK cb_ov_view_op;
FL_CALLBACK cb_ov_view;
FL_CALLBACK cb_overview_refresh;
FL_CALLBACK cb_overview_showhide;
FL_CALLBACK cb_ov_save;
FL_CALLBACK cb_ov_units;
FL_CALLBACK cb_ov_slibtn;
FL_CALLBACK cb_tomain;
FL_CALLBACK cb_ov_setzoom;
FL_CALLBACK cb_ov_slider;
FL_CALLBACK cb_ov_setk;


/* Forms and Objects */

typedef struct {
    FL_FORM   * overview;
    void      * vdata;
    char      * cdata;
    long        ldata;
    FL_OBJECT * zoomIn;
    FL_OBJECT * zoomOut;
    FL_OBJECT * planeXY;
    FL_OBJECT * planeXZ;
    FL_OBJECT * planeZY;
    FL_OBJECT * show_rois;
    FL_OBJECT * show_contours;
    FL_OBJECT * status;
    FL_OBJECT * ch_ulen;
    FL_OBJECT * save;
    FL_OBJECT * show_overlay;
    FL_OBJECT * draw_sel;
    FL_OBJECT * btn_uval;
    FL_OBJECT * rotateCCW;
    FL_OBJECT * rotateCW;
    FL_OBJECT * flip;
    FL_OBJECT * view_xy;
    FL_OBJECT * xy_frame;
    FL_OBJECT * xy_btn_dn;
    FL_OBJECT * xy_btn_up;
    FL_OBJECT * xy_fb;
    FL_OBJECT * xy_bit_orient;
    FL_OBJECT * xy_tomain;
    FL_OBJECT * xy_zoom;
    FL_OBJECT * xy_slider;
    FL_OBJECT * xy_k;
    FL_OBJECT * view_xz;
    FL_OBJECT * xz_frame;
    FL_OBJECT * xz_slider;
    FL_OBJECT * xz_tomain;
    FL_OBJECT * xz_btn_dn;
    FL_OBJECT * xz_btn_up;
    FL_OBJECT * xz_bit_orient;
    FL_OBJECT * xz_fb;
    FL_OBJECT * xz_zoom;
    FL_OBJECT * xz_k;
    FL_OBJECT * view_zy;
    FL_OBJECT * zy_frame;
    FL_OBJECT * zy_slider;
    FL_OBJECT * zy_tomain;
    FL_OBJECT * zy_btn_dn;
    FL_OBJECT * zy_btn_up;
    FL_OBJECT * zy_bit_orient;
    FL_OBJECT * zy_fb;
    FL_OBJECT * zy_zoom;
    FL_OBJECT * zy_k;
} FD_overview;

FD_overview * create_form_overview( void );

#if defined __cplusplus
}
#endif

#endif /* FD_overview_h_ */
