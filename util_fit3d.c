#include "util_fit3d.h"

/* compute vector of line that best fits points by SVD */
gsl_vector* best_fit_line_vector(gsl_matrix *X) {
    if(X == NULL) return NULL; /* no input */
    size_t np = X->size1;
    if(np < 2) return NULL; /* insufficient points */

    gsl_vector *u = gsl_vector_alloc(3);
    if(np == 2) {
        gsl_vector *v = gsl_vector_alloc(3);
        gsl_matrix_get_row(v,X,0);
        gsl_matrix_get_row(u,X,1);
        gsl_vector_sub(u,v);
        gsl_vector_free(v);
        gsl_vector_scale(u,1.0/gsl_blas_dnrm2(u));
    } else {
        /* compute centroid */
        double cx = 0.0;
        double cy = 0.0;
        double cz = 0.0;
        for(size_t n=0;n<np;n++) {
            cx += gsl_matrix_get(X,n,0);
            cy += gsl_matrix_get(X,n,1);
            cz += gsl_matrix_get(X,n,2);
        }
        cx /= np; cy /= np; cz /= np;

        /* subtract centroid */
        for(size_t n=0;n<np;n++) {
            gsl_matrix_set(X,n,0,gsl_matrix_get(X,n,0)-cx);
            gsl_matrix_set(X,n,1,gsl_matrix_get(X,n,1)-cy);
            gsl_matrix_set(X,n,2,gsl_matrix_get(X,n,2)-cz);
        }

        /* perform SVD */
        gsl_matrix *V = gsl_matrix_alloc(3,3);
        gsl_vector *S = gsl_vector_alloc(3);
        gsl_vector *w = gsl_vector_alloc(3);
        gsl_linalg_SV_decomp(X,V,S,w);
        gsl_vector_free(w);
        gsl_vector_free(S);

        /* get vector of largest singular value */
        gsl_matrix_get_col(u,V,0);
        gsl_matrix_free(V);
    }

    return u;
}

/* compute normal vector of plane that best fits points by SVD */
gsl_vector* best_fit_plane_normal_vector(gsl_matrix *X) {
    if(X == NULL) return NULL; /* no input */
    size_t np = X->size1;
    if(np < 3) return NULL; /* insufficient points */

    /* compute centroid */
    double cx = 0.0;
    double cy = 0.0;
    double cz = 0.0;
    for(size_t n=0;n<np;n++) {
        cx += gsl_matrix_get(X,n,0);
        cy += gsl_matrix_get(X,n,1);
        cz += gsl_matrix_get(X,n,2);
    }
    cx /= np; cy /= np; cz /= np;

    /* subtract centroid */
    for(size_t n=0;n<np;n++) {
        gsl_matrix_set(X,n,0,gsl_matrix_get(X,n,0)-cx);
        gsl_matrix_set(X,n,1,gsl_matrix_get(X,n,1)-cy);
        gsl_matrix_set(X,n,2,gsl_matrix_get(X,n,2)-cz);
    }

    /* perform SVD */
    gsl_matrix *V = gsl_matrix_alloc(3,3);
    gsl_vector *S = gsl_vector_alloc(3);
    gsl_vector *w = gsl_vector_alloc(3);
    gsl_linalg_SV_decomp(X,V,S,w);
    gsl_vector_free(w);
    gsl_vector_free(S);

    /* get vector of smallest singular value */
    gsl_vector *u = gsl_vector_alloc(3);
    gsl_matrix_get_col(u,V,2);
    gsl_matrix_free(V);

    return u;
}

gsl_matrix* get_rotation_align(const gsl_vector *u, const gsl_vector *v) {
    if(u == NULL || v == NULL) return NULL; /* no input */

    /* compute dot and cross product */
    double dot; gsl_blas_ddot(u,v,&dot);
    double cx = gsl_vector_get(u,1) * gsl_vector_get(v,2) -
        gsl_vector_get(u,2) * gsl_vector_get(v,1);
    double cy = gsl_vector_get(u,2) * gsl_vector_get(v,0) -
        gsl_vector_get(u,0) * gsl_vector_get(v,2);
    double cz = gsl_vector_get(u,0) * gsl_vector_get(v,1) -
        gsl_vector_get(u,1) * gsl_vector_get(v,0);

    /* compute skew-symmetric cross-product matrix */
    gsl_matrix *C = gsl_matrix_alloc(3,3);
    gsl_matrix_set(C,0,0,  0); gsl_matrix_set(C,0,1,-cz); gsl_matrix_set(C,0,2, cy);
    gsl_matrix_set(C,1,0, cz); gsl_matrix_set(C,1,1,  0); gsl_matrix_set(C,1,2,-cx);
    gsl_matrix_set(C,2,0,-cy); gsl_matrix_set(C,2,1, cx); gsl_matrix_set(C,2,2,  0);

    /* compute rotation matrix R = I + C + 1/(1+dot) * C^2
     * source: https://math.stackexchange.com/a/476311 */
    gsl_matrix *R = gsl_matrix_alloc(3,3);
    gsl_matrix_set_identity(R); /* R = I */
    gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0/(1.0+dot),C,C,1.0,R); /* R += 1/(1+dot) * C * C */
    gsl_matrix_add(R,C); /* R += C */
    gsl_matrix_free(C);

    return R;
}

gsl_vector* get_translation_center(const gsl_matrix *R, const gsl_vector *u) {
    if(R == NULL || u == NULL) return NULL; /* no input */
 
    gsl_vector *b = gsl_vector_alloc(3);
    gsl_vector_memcpy(b,u);
    gsl_blas_dgemv(CblasNoTrans,-1.0,R,u,1.0,b);

    return b;
}
