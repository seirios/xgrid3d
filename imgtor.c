#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "gengrid.h"

/* this function returns a representation of a kernel as (r,dose) values */

#define SQD(x,y,z) ((x)*(x)+(y)*(y)+(z)*(z))
void imgtor(const double *dvk, const size_t dim, const double vox, double **o_r, double **o_dose, size_t *o_ndist) {
	uint32_t *dists,*occup;
	double *r,*dose;
	size_t ndist;

	/* generate the squared distances and occupancies of the kernel grid */
	gengrid(dim/2,&ndist,&dists,&occup);

	r = malloc(ndist*sizeof(double));
	dose = malloc(ndist*sizeof(double));

	/* iterate over all voxels */
	for(size_t i=0;i<dim;i++)
		for(size_t j=i;j<dim;j++)
			for(size_t k=j;k<dim;k++) {
				uint32_t key = SQD(i-dim/2,j-dim/2,k-dim/2); /* squared distance to this voxel */
				uint32_t *res = bsearch(&key,dists,ndist,sizeof(uint32_t),cmp_uint32_t); /* find index of distance */
				if(res != NULL) /* if found (within inscribed sphere */
					dose[res-dists] = dvk[j + i*dim + k*dim*dim]; /* set dose to dose for this distance */
			}

#ifdef _OPENMP
#pragma omp parallel for
#endif
	for(size_t n=0;n<ndist;n++)
		r[n] = sqrt(dists[n]) * vox; /* compute actual distance for voxels of size @vox */

	free(dists);
	free(occup);

	if(o_r != NULL) *o_r = r;
	if(o_dose != NULL) *o_dose = dose;
	if(o_ndist != NULL) *o_ndist = ndist;
}
