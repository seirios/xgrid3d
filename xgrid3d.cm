%include "xgrid3d_forms.hm"

#include <X11/extensions/Xrender.h>

#include <libgen.h>
#include <limits.h>
#include <time.h>
#include <sys/stat.h>

#ifdef ENABLE_NLS
#include <libintl.h>
#include <locale.h>
#endif

#include <fftw3.h>

#include <babl/babl.h>

#include <gsl/gsl_errno.h>

#include "xgrid3d.h"
#include "grid3d_quant.h"
#include "grid3d_visual.h"
#include "grid3d_uint8.h"

#include "setup_forms.h"

#include "io.h"
#include "xg.xpm"
#include "cur_cross.xbm"
#include "util.h"
#include "util_units.h"
#include "util_symbols.h"
#include "util_coordinates.h"

#define TITLE_MENU "xgrid3d<%d>",getpid()

#undef ENV

/* !!! GLOBAL VARIABLES !!! */
char *kernel_dir = NULL;
int selcursor = None;
int cursor = None;
/* !!! END GLOBAL VARIABLES !!! */

g3d_ret xg3d_datasets_add(xg3d_datasets *x, xg3d_data *data) {
    void *p;

    if(x == NULL || data == NULL) return G3D_ENOINPUT; /* null input */
    if(data->g3d == NULL || data->hdr == NULL) return G3D_FAIL; /* empty data */

    p = realloc(x->sets,(x->nsets+1)*sizeof(xg3d_data*));
    if(p == NULL) { 
        xg3d_data_clear(data,true);
        return G3D_ENOMEM; /* malloc failed */
    }

    x->sets = (xg3d_data**)p;
    x->sets[x->nsets] = data;
    x->nsets++;

    return G3D_OK;
}

g3d_ret xg3d_datasets_remove(xg3d_datasets *x, const xg3d_data *data) {
    unsigned int ix = x->nsets;

    if(data == NULL) return G3D_ENOINPUT; /* null input */

    for(unsigned int i=0;i<x->nsets;i++)
        if(x->sets[i] == data) {
            ix = i; break;
        }

    if(ix == x->nsets) return G3D_FAIL; /* not found */

    xg3d_data_clear(x->sets[ix],true);
    memmove(&x->sets[ix],&x->sets[ix+1],(x->nsets-ix-1)*sizeof(xg3d_data*));
    x->sets = realloc(x->sets,(x->nsets-1)*sizeof(xg3d_data*));
    x->nsets--;

    return G3D_OK;
}

static void xg3d_datasets_clear(xg3d_datasets *x, const bool delete) {
    if(x != NULL) {
        for(unsigned int i=0;i<x->nsets;i++) {
            xg3d_data_clear(x->sets[i],true);
            x->sets[i] = NULL;
        }
        free(x->sets); x->sets = NULL;
        x->main = NULL;
        x->nsets = 0;
        if(delete) free(x);
    }
}

xg3d_data* xg3d_data_dup(const xg3d_data *x) {
    xg3d_data *y;

    y = calloc(1,sizeof(xg3d_data));
    if(y == NULL) return NULL; /* malloc failed */

    y->name = strdup(x->name);
    y->hdr = xg3d_header_dup(x->hdr);
    y->g3d = g3d_dup(x->g3d);
    y->stats = g3d_stats_dup(x->stats);
    y->rois = xrois_dup(x->rois,x->nrois);
    y->nrois = x->nrois;

    return y;
}

void xg3d_data_clear(xg3d_data *x, const bool delete) {
    if(x != NULL) {
        free(x->name); x->name = NULL;
        xg3d_header_free(x->hdr); x->hdr = NULL;
        g3d_free(x->g3d); x->g3d = NULL;
        free(x->stats); x->stats = NULL;
        xrois_free(x->rois,x->nrois);
        x->rois = NULL; x->nrois = 0;
        if(delete) free(x);
    }
}

static xg3d_env* xg3d_env_alloc(void) {
    xg3d_env *env = calloc(1,sizeof(xg3d_env));

    if(env == NULL) return NULL; /* malloc failed */

    env->use_sel = true;
    /* calloc is fundamental here, as we need NULL pointers initially */
    env->gui = calloc(1,sizeof(xg3d_gui));
    env->data = calloc(1,sizeof(xg3d_datasets));

    return env;
}

static void clear_workspace_memory(xg3d_env *ENV, const bool main, const bool clear) {
    if(GUI->viewport != NULL)
        view_workspace_clear(VIEW_WORKSPACE(GUI->viewport),clear);
    if(main) { /* overview viewports */
        if(GUI->view_xy != NULL)
            view_workspace_clear(VIEW_WORKSPACE(GUI->view_xy),clear);
        if(GUI->view_xz != NULL)
            view_workspace_clear(VIEW_WORKSPACE(GUI->view_xz),clear);
        if(GUI->view_zy != NULL)
            view_workspace_clear(VIEW_WORKSPACE(GUI->view_zy),clear);
    }
    if(GUI->fun_preview != NULL) { /* function viewports */
        if(GUI->fun_view_xy != NULL)
            view_workspace_clear(VIEW_WORKSPACE(GUI->fun_view_xy),clear);
        if(GUI->fun_view_xz != NULL)
            view_workspace_clear(VIEW_WORKSPACE(GUI->fun_view_xz),clear);
        if(GUI->fun_view_zy != NULL)
            view_workspace_clear(VIEW_WORKSPACE(GUI->fun_view_zy),clear);
    }
    if(GUI->selection != NULL) {
        sel_clear_markers(GUI->selection);
        selection_workspace_clear(SELECTION_WORKSPACE(GUI->selection),clear);
    }
    if(GUI->quantization != NULL) {
        if(QUANTIZATION_HISTOGRAM(GUI->quantization) != NULL) {
            gsl_histogram_free(QUANTIZATION_HISTOGRAM(GUI->quantization));
            QUANTIZATION_HISTOGRAM(GUI->quantization) = NULL;
        }
        quantization_workspace_clear(QUANTIZATION_WORKSPACE(GUI->quantization),clear);
    }
    if(GUI->operations != NULL)
        xg3d_data_clear(BACKUP_DATA(GUI->operations),clear);
}

static void xg3d_env_free(xg3d_env *ENV, const bool main) {
    /* free datasets */
    xg3d_datasets_clear(ENV->data,true);

    /* clear workspace memory */
    clear_workspace_memory(ENV,main,true);

    if(!main) { /* free shared resources only once */
        GUI->overview = NULL;
        GUI->overlay = NULL;
    }

    /* free hidden data */
    if(GUI->open_hdr != NULL) xg3d_header_free(OPEN_HEADER(GUI->open_hdr));
    if(GUI->overview != NULL) free(GUI->overview->vdata);
    if(GUI->coloring != NULL) {
        flimage_free(COLORING_LUT_IMG(GUI->coloring));
        free(COLORING_LUT(GUI->coloring));
    }
    if(GUI->col_table != NULL) {
        flimage_free(COLORING_TAB_IMG(GUI->col_table));
        free(COLORING_LUTFILE(GUI->col_table));
    }
    if(GUI->quant_custom != NULL) free(GUI->quant_custom->vdata);
    if(GUI->dose != NULL) kernel_data_clear(KERNEL_DATA(GUI->dose),true);
    if(GUI->kinetics != NULL) kinetics_workspace_clear(KINETICS_WORKSPACE(GUI->kinetics),true);
    if(GUI->function != NULL) function_workspace_clear(FUNCTION_WORKSPACE(GUI->function),true);
    if(GUI->fun_preview != NULL) free(GUI->fun_preview->vdata);

    /* free FDUIs */
    if(main) {
    %map xg3d_forms %{
        if(GUI->${name} != NULL) { fl_free(GUI->${name}); GUI->${name} = NULL; }
    %}
    } else {
    %map xg3d_forms_overlay %{
        if(GUI->${name} != NULL) { fl_free(GUI->${name}); GUI->${name} = NULL; }
    %}
    }

    free(GUI);
    free(ENV);
}

static void xg3d_init(xg3d_env *env, xg3d_env *env_overlay, Pixmap icon, Pixmap icon_mask) {
    /* main */
    %map xg3d_forms %{
    FD_${form} *${name} = create_form_${form}();
    %}

    /* overlay */
    %map xg3d_forms_overlay %{
    FD_${form} *${name}_over = create_form_${form}();
    %}

    /* default coloring settings */
    static xg3d_coloring_settings default_coloring_settings = {
        .flag_reverse = false,
        .flag_invert = false,
        .lut_under = FL_PACK(0,0,0),
        .lut_over = FL_PACK(255,255,255),
        .color_mode = COLOR_GRADIENT,
        .grad_type = COL_GRAD_LUMA,
        .hue = 120.0
    };

    /* default quantization settings */
    static xg3d_quantization_settings default_quantization_settings = {
        .scope = SCOPE_GLOBAL,
        .cal_units = false,
        .hist_log = true,
        .hist_skip = false,
        .rng_type = QUANT_RNG_MINMAX,
        .quant_mode = QUANT_INTERVALS,
        .nint = 256,
        .exp = 1.0,
        .clamp = false
    };

    /* default selection settings */
    static xg3d_selection_settings default_selection_settings = {
        .scope = SCOPE_SLICE,
        .visible = true,
        .labels = false,
        .patt = SEL_PATT_CROSS,
        .mode = SEL_MODE_NONE,
        .clamp = false,
        .replace = false,
        .brush_shape = SEL_BRUSH_SQUARE,
        .brush_size = 5,
        .shape = SEL_SHAPE_BOX,
        .nrand = 1,
        .iso_filled = false,
        .iso_greater = true,
        .rep = 1,
        .step = 1,
        .propagate = false,
    };

    /* setup cursor */
    cursor = fl_create_bitmap_cursor((char*)cur_cross_bits,(char*)cur_cross_bits,
            cur_cross_width,cur_cross_height,cur_cross_x_hot,cur_cross_y_hot);

    /* setup unifont */
    fl_set_font_name(UNIFONT,"-gnu-unifont-medium-r-*-*-*-160-*-*-*-*-*-*");
    fl_set_font_name(UNIFONT_BOLD,"-gnu-unifont-medium-r-*-*-*-160-*-*-*-*-*-*");
    fl_set_font_name(UNIFONT_ITALIC,"-gnu-unifont-medium-r-*-*-*-160-*-*-*-*-*-*");

    /* setup resources */
    setup_fl_resources();

    /* setup file selector */
    setup_fd_fselector(fl_get_fselector_fdstruct());

    /* setup command log */
    setup_fd_cmdlog(fl_get_command_log_fdstruct());

    /* setup symbols */
    fl_add_symbol("clip",draw_clip,0);
    fl_add_symbol("cursor",draw_xhair,0);

    /* set form atclose and icons, setup forms and GUI */
    %map xg3d_forms %{
        fl_set_form_atclose(${name}->${form}${atclose_args});
        fl_set_form_icon(${name}->${form},icon,icon_mask);
        setup_fd_${form}(${name}${setup_args});
        env->gui->${name} = ${name};
    %}

    %map xg3d_forms_overlay %{
        fl_set_form_atclose(${name}_over->${form}${atclose_args});
        fl_set_form_icon(${name}->${form},icon,icon_mask);
        setup_fd_${form}(${name}_over${setup_args});
        env_overlay->gui->${name} = ${name}_over;
    %}

    /* function preview has no extra functionality */
    fl_hide_object(fun_preview->draw_sel);
    fl_hide_object(fun_preview->show_overlay);
    fl_hide_object(fun_preview->show_rois);
    fl_hide_object(fun_preview->show_contours);
    fl_hide_object(fun_preview->ch_ulen);
    fl_hide_object(fun_preview->btn_uval);

    /* overlay calibration has no modes */
    fl_hide_object(calibration_over->tf_calib);
    /* overlay table coloring has no selection */
    deactivate_obj(col_table_over->menu_sel);
    /* in overlay only SCOPE_GLOBAL allowed */
    fl_set_button(quantization_over->btn_scope,1);
    deactivate_obj(quantization_over->btn_scope);
    fl_set_object_color(quantization_over->btn_scope,FL_COL1,FL_COL1);
    /* in overlay no interaction with selection is possible */
    deactivate_obj(quantization_over->btn_rng_fromsel);

    /* setup from settings - mutable GUI parameters and data */
    setup_coloring_settings(coloring,
            COLORING_COLORING|COLORING_COL_GRAD,
            &default_coloring_settings);
    setup_quantization_settings(quantization,
            QUANTIZATION_QUANTIZATION|QUANTIZATION_QUANT_RNG|QUANTIZATION_QUANT_INTERVALS,
            &default_quantization_settings);
    setup_selection_settings(selection,
            SELECTION_SCOPE|SELECTION_APPEARANCE|SELECTION_SELMODE|SELECTION_OPERATIONS,
            &default_selection_settings);
    /* overlay settings */
    setup_coloring_settings(coloring_over,
            COLORING_COLORING|COLORING_COL_GRAD,
            &default_coloring_settings);
    setup_quantization_settings(quantization_over,
            QUANTIZATION_QUANTIZATION|QUANTIZATION_QUANT_RNG|QUANTIZATION_QUANT_INTERVALS,
            &default_quantization_settings);

    /* setup shared resources */
    env_overlay->gui->overview = overview;
    env_overlay->gui->overlay = overlay;

    /* associate forms and envs */
    %map xg3d_forms %{
        ${name}->${form}->u_vdata =
    %} env;

    %map xg3d_forms_overlay %{
        ${name}_over->${form}->u_vdata =
    %} env_overlay;

    /* link with overlay env */
    overlay->overlay->u_vdata = env_overlay;

    update_ker_list(dose,&kernel_dir);

    init_ramp_plots(col_ramp);
    update_plot_exp(quant_intervals->plot,fl_get_counter_value(quant_intervals->cnt_exp));

    init_ramp_plots(col_ramp_over);
    update_plot_exp(quant_intervals_over->plot,fl_get_counter_value(quant_intervals_over->cnt_exp));

    if(!env->use_sel) {
        deactivate_obj(mainview->btn_selection);
        deactivate_obj(overview->draw_sel);
        deactivate_obj(rois->menu_select);
    }
}

void setup_main(xg3d_env *ENV, xg3d_data *main) {
    const bool notnull = (main != NULL);

    if(notnull) {
        ENV->data->main = main;
        GUI->info->vdata = (void*)main->hdr;
        set_default_acq_settings(ENV,main->hdr,true);
        setup_from_data(ENV,true);
        if(fl_get_button(GUI->overlay->btn_show))
            setup_overlay(ENV,ENV->data->overlay);
    } else {
        ENV->data->main = NULL;
        clear_workspace_memory(ENV,true,false);
        clear_workspace_memory(OVERLAY_ENV(GUI->overlay),false,false);
    }

    /* cosmetic changes */
    toggle_browser_line(GUI->datasets->br_attr_avail,ATTR_OVERLAY,notnull);
    toggle_browser_line(GUI->datasets->br_attr_avail,ATTR_DOSE,notnull);
    toggle_browser_line(GUI->datasets->br_attr_avail,ATTR_DOSE_ERROR,notnull);
    if(XOR(fl_form_is_visible(GUI->mainview->mainview),notnull)) {
        fl_set_button(GUI->menu->btn_mainview,notnull);
        fl_trigger_object(GUI->menu->btn_mainview);
    }
}

void setup_from_data(xg3d_env *ENV, const bool resized) {
    const enum g3d_slice_plane plane = MAINVIEW_GET_PLANE(GUI->mainview);
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);

    fl_show_oneliner(_("Setting up data..."),fl_scrw/2,fl_scrh/2);

    /* update data statistics */
    free(DATA->stats);
    DATA->stats = g3d_stats_get(DATA->g3d,NULL,G3D_STATS_MINMAXSUM);

    /* store data pointer */
    VIEW_DATA(GUI->viewport) = DATA;
    VIEW_DATA(ov_viewports[PLANE_XY]) = DATA;
    VIEW_DATA(ov_viewports[PLANE_XZ]) = DATA;
    VIEW_DATA(ov_viewports[PLANE_ZY]) = DATA;

    if(resized) {
        /* setup header dimensions */
        DATA->hdr->dim[0] = DATA->g3d->nx;
        DATA->hdr->dim[1] = DATA->g3d->ny;
        DATA->hdr->dim[2] = DATA->g3d->nz;

        /* set initial slice positions */
        MAINVIEW_STORE_K(GUI->mainview,PLANE_XY,ENV->buffered ? 0 : DATA->g3d->nz/2);
        MAINVIEW_STORE_K(GUI->mainview,PLANE_XZ,DATA->g3d->ny/2);
        MAINVIEW_STORE_K(GUI->mainview,PLANE_ZY,DATA->g3d->nx/2);

        /* initial plane is given by mainview planeXYZ button */
        fl_set_slider_bounds(GUI->mainview->slider,0.0,DATA->hdr->dim[PLANE_TO_K(plane)]-1);
        fl_set_slider_value(GUI->mainview->slider,DATA->hdr->dim[PLANE_TO_K(plane)]/2);
        fl_set_object_label_f(GUI->mainview->txt_k,"%u",DATA->hdr->dim[PLANE_TO_K(plane)]/2);
        VIEW_K(GUI->viewport) = DATA->hdr->dim[PLANE_TO_K(plane)]/2;
        VIEW_PLANE(GUI->viewport) = plane;

        /* setup overview */
        %snippet setup_ov_view = (XYZ,nxyz) %{
            VIEW_PLANE(ov_viewports[PLANE_${XYZ}]) = PLANE_${XYZ};
            VIEW_K(ov_viewports[PLANE_${XYZ}]) = DATA->g3d->${nxyz}/2;
            fl_set_slider_bounds(GUI->overview->$L{XYZ}_slider,0,DATA->g3d->${nxyz}-1);
            fl_set_slider_value(GUI->overview->$L{XYZ}_slider,VIEW_K(ov_viewports[PLANE_${XYZ}]));
            fl_set_object_label_f(GUI->overview->$L{XYZ}_k,"%u",VIEW_K(ov_viewports[PLANE_${XYZ}]));
        %}
        %recall setup_ov_view (`XY`,`nz`)(`XZ`,`ny`)(`ZY`,`nx`)

        if(DATA->g3d->nz == 1 || ENV->buffered) { /* single slice */
            deactivate_obj(GUI->mainview->grp_slider);
            deactivate_obj(GUI->mainview->planeXZ);
            deactivate_obj(GUI->mainview->planeZY);

            deactivate_obj(GUI->overview->xy_k);
            deactivate_obj(GUI->overview->xy_btn_dn);
            deactivate_obj(GUI->overview->xy_slider);
            deactivate_obj(GUI->overview->xy_btn_up);
            deactivate_obj(GUI->overview->planeXZ);
            fl_set_button(GUI->overview->planeXZ,0); fl_hide_object(GUI->overview->view_xz);
            deactivate_obj(GUI->overview->planeZY);
            fl_set_button(GUI->overview->planeZY,0); fl_hide_object(GUI->overview->view_zy);

            fl_set_button(GUI->selection->btn_scope,0);
            deactivate_obj(GUI->selection->btn_scope);
        } else { /* multiple slices */
            activate_obj(GUI->mainview->grp_slider,FL_YELLOW);
            activate_obj(GUI->mainview->planeXZ,FL_YELLOW);
            activate_obj(GUI->mainview->planeZY,FL_YELLOW);

            activate_obj(GUI->overview->xy_k,FL_COL1);
            activate_obj(GUI->overview->xy_btn_dn,FL_YELLOW);
            activate_obj(GUI->overview->xy_slider,FL_YELLOW);
            activate_obj(GUI->overview->xy_btn_up,FL_YELLOW);
            activate_obj(GUI->overview->planeXZ,FL_YELLOW);
            activate_obj(GUI->overview->planeZY,FL_YELLOW);

            activate_obj(GUI->selection->btn_scope,FL_YELLOW);
        }

        /* remove ROIs */
        xrois_free(DATA->rois,DATA->nrois);
        DATA->rois = NULL; DATA->nrois = 0;
        refresh_roi_browsers(ENV);

        /* setup operations */
        fl_set_spinner_bounds(GUI->operations->spin_off_x,-9999,DATA->g3d->nx-1);
        fl_set_spinner_bounds(GUI->operations->spin_off_y,-9999,DATA->g3d->ny-1);
        fl_set_spinner_bounds(GUI->operations->spin_off_z,-9999,DATA->g3d->nz-1);
        fl_call_object_callback(GUI->operations->btn_crop_reset);
    }

    /* update info */
    setup_info(GUI->info,DATA->hdr);
    update_volumes(ENV,DATA->hdr);

    /* setup calibration */
    fl_set_input_f(GUI->calibration->inp_slope,"%.16g",DATA->hdr->cal_slope);
    fl_set_input_f(GUI->calibration->inp_inter,"%.16g",DATA->hdr->cal_inter);
    update_roi_values(ENV);

    /* setup selection */
    selection_workspace_clear(SELECTION_WORKSPACE(GUI->selection),false);
    if(ENV->use_sel) {
        SELECTION_G3D(GUI->selection) = g3d_alloc(G3D_UINT8,DATA->g3d->nx,DATA->g3d->ny,DATA->g3d->nz,.alloc=true);
        g3d_u8_setup_nhood(SELECTION_G3D(GUI->selection),SCOPE_GLOBAL);
    } else
        SELECTION_G3D(GUI->selection) = NULL;
    sel_clear_markers(GUI->selection);

    /* setup slices */
    replace_slice(GUI->viewport,GUI->selection,DATA->g3d,VIEW_K(GUI->viewport));
    replace_ov_slices(ov_viewports,LAYER_BASE);

    /* setup global quantization */
    update_datarange(ENV,true);
    set_range_inpsli(ENV,true);
    compute_quantization(GUI->quantization,DATA->g3d);
    update_quant_coloring(GUI->quantization,GUI->coloring,DATA->hdr);

    replace_quantslice(ENV);
    replace_ov_quantslices(ov_viewports,GUI->quantization,LAYER_BASE);

    /* setup operations */
    fl_set_input_f(GUI->operations->inp_fillval,"%.16g",QUANTIZATION_WORKSPACE(GUI->quantization)->min);

    /* setup images */
    replace_img(GUI->viewport,GUI->quantization);
    replace_ov_images(ov_viewports,GUI->quantization,LAYER_BASE);

    /* resize canvases */
    resize_canvas(GUI->mainview,GUI->viewport);
    resize_ov_canvases(ENV);

    /* refresh canvases */
    refresh_canvas(ENV);
    refresh_ov_canvases(GUI->overview);
    update_all_positioners(ov_viewports,GUI->overview);

    fl_hide_oneliner();
}

void set_default_acq_settings(xg3d_env *ENV, xg3d_header *hdr, const bool trigger) {
    switch(hdr->acqmod) {
        case ACQ_UNKNOWN:
            if(GUI->info) fl_set_choice(GUI->info->ch_acqmod,1);
            fl_set_slider_value(GUI->col_grad->sli_hue,120);
            fl_set_button(GUI->col_grad->btn_luma,1);
            if(trigger) fl_trigger_object(GUI->col_grad->btn_luma);
            break;
        case ACQ_PET:
            if(GUI->info) fl_set_choice(GUI->info->ch_acqmod,2);
            if(GUI->operations != NULL) {
                fl_set_input(GUI->operations->inp_fillval,"0");
                fl_set_button(GUI->operations->btn_norm,1);
            }
            fl_set_slider_value(GUI->col_grad->sli_hue,0);
            fl_set_button(GUI->col_grad->btn_hotmetal,1);
            if(trigger) fl_trigger_object(GUI->col_grad->btn_hotmetal);
            break;
        case ACQ_CT:
            if(GUI->info) fl_set_choice(GUI->info->ch_acqmod,3);
            if(GUI->operations != NULL) {
                fl_set_input(GUI->operations->inp_fillval,"-1000");
                fl_set_button(GUI->operations->btn_norm,0);
            }
            fl_set_button(GUI->col_grad->btn_grayscale,1);
            if(trigger) fl_trigger_object(GUI->col_grad->btn_grayscale);
            break;
        case ACQ_SPECT:
            if(GUI->info) fl_set_choice(GUI->info->ch_acqmod,4);
            if(GUI->operations != NULL) {
                fl_set_input(GUI->operations->inp_fillval,"0");
                fl_set_button(GUI->operations->btn_norm,1);
            }
            fl_set_slider_value(GUI->col_grad->sli_hue,0);
            fl_set_button(GUI->col_grad->btn_turbo,1);
            if(trigger) fl_trigger_object(GUI->col_grad->btn_turbo);
            break;
    }
}

void load_single_show_mainview(xg3d_env *ENV) {
    FL_Coord x,y,w,h;
    int decor[4];

    /* set loaded data as main */
    setup_main(ENV,ENV->data->sets[0]);
    refresh_datasets_browser(GUI->datasets->browser,ENV->data);

    /* position mainview */
    if(fl_form_is_visible(GUI->menu->menu)) {
        fl_get_winorigin(GUI->menu->menu->window,&x,&y);
        fl_get_object_size(GUI->menu->box,&w,&h);
        fl_get_decoration_sizes(GUI->menu->menu,&decor[0],&decor[1],&decor[2],&decor[3]);
        fl_winmove(GUI->mainview->mainview->window,x-decor[1]-decor[3]+w+5,y-decor[0]);
    }
}

int main (int argc, char *argv[]) {
    /* initialize gettext */
#ifdef ENABLE_NLS
    char *base;
    setlocale(LC_MESSAGES,"");
    bindtextdomain("xgrid3d",(base = getenv("XG3D_LOCALE_DIR")) ? base : "/usr/share/locale");
    bind_textdomain_codeset("xgrid3d","UTF-8");
    textdomain("xgrid3d");
#endif

    /* setup resources */
    int not_use_sel = false;
    int buffered = false;
    int is_filelist = false;
    char default_fun[128] = { 0 };

    FL_CMD_OPT cmdopt[] = {
        {"-nosel", ".nosel", XrmoptionNoArg, "true"},
        {"-buffered", ".buffered", XrmoptionNoArg, "true"},
        {"-filelist", ".filelist", XrmoptionNoArg, "true"},
        {"-function", ".function", XrmoptionSepArg, 0}
    };

    FL_resource res[] = {
        {"nosel","",FL_BOOL,&not_use_sel,"false",0},
        {"buffered","",FL_BOOL,&buffered,"false",0},
        {"filelist","",FL_BOOL,&is_filelist,"false",0},
        {"function","",FL_STRING,&default_fun,"",128}
    };

    int nopts = %arrlen(cmdopt);

    /* initialize xforms */
    FL_IOPT flopt;
    memset(&flopt,0x0,sizeof(FL_IOPT));
    flopt.depth = 32;
    flopt.coordUnit = FL_COORD_PIXEL;
    flopt.borderWidth = 1;
    flopt.backingStore = true;
    fl_set_defaults(FL_PDDepth|FL_PDCoordUnit|FL_PDBorderWidth|FL_PDBS,&flopt);

    if(fl_initialize(&argc,argv,"xgrid3d",cmdopt,nopts) == NULL)
        fatal("XForms v%d initialization failed. Is X running?",FL_INCLUDE_VERSION);
    fl_get_app_resources(res,nopts);

#ifdef HAVE_RENDER_H
    int evbp,errbp;
    if(!XRenderQueryExtension(fl_display,&evbp,&errbp))
        fatal("XRender extension unavailable. %d",errbp);
#endif

    /* initialize flimage */
    FLIMAGE_SETUP flisetup;
    memset(&flisetup,0x0,sizeof(FLIMAGE_SETUP));
    flisetup.visual_cue = quiet_cue;
    flimage_setup(&flisetup);
    flimage_enable_png();
    flimage_enable_ps();

    /* initialize input */
    FL_EditKeymap keymap;
    memset(&keymap,0x0,sizeof(FL_EditKeymap));
    keymap.del_next_char = 0x7F; /* delete */
    fl_set_input_editkeymap(&keymap);
    fl_set_input_mode(FL_NORMAL_INPUT_MODE|FL_SELECT_INPUT_MODE);

    /* init FFTW threads */
    if(fftw_init_threads() == 0)
        fatal("Cannot initialize FFTW%c threads.",'3');

    /* init babl */
    babl_init();

    /* turn off gsl error handler */
    gsl_set_error_handler_off();

    /* set random seed */
    srandom((unsigned int)time(NULL));

    /* set app icon */
    Pixmap icon,icon_mask;
    unsigned int icon_w,icon_h;
    icon = fl_create_from_pixmapdata(fl_root,xg_xpm,&icon_w,&icon_h,&icon_mask,NULL,NULL,0);

    /* allocate environments */
    xg3d_env *ENV = xg3d_env_alloc();
    xg3d_env *env_overlay = xg3d_env_alloc();

    /* set global options from resources */
    ENV->use_sel = !not_use_sel;
    ENV->buffered = buffered;

    /* init xgrid3d */
    xg3d_init(ENV,env_overlay,icon,icon_mask);

    /* CLI interface for data load */
    if(argc > 1) {
        xg3d_header hdr = { 0 };
        char *file = strdup(argv[1]);
        if(argc == 2) {
            struct stat statbuf;
            if(stat(file,&statbuf) == 0 && S_ISDIR(statbuf.st_mode)) {
                fl_set_directory(file);
                fl_set_form_position(GUI->menu->menu,fl_scrw/10,fl_scrh/5);
                fl_show_form_f(GUI->menu->menu,FL_PLACE_SIZE|FL_PLACE_POSITION,FL_TRANSIENT,TITLE_MENU);
                cb_opendlg_fselect(GUI->menu->menu_load,0);
                fl_call_object_callback(GUI->menu->menu_load);
            } else {
                if(!HDR_TEST_ALL(file,&hdr))
                    fatal("Failed parsing header: %s. "
                          "Make sure this is a well-formed microPET, Interfile, NIfTI-1 or Nrrd header file.",file);
                char *dat = hdr.file; hdr.file = realpath(dat,NULL);
                if(hdr.file == NULL) {
                    char buf[1024];
                    snprintf(buf,sizeof(buf),"%s/%s",dirname(file),basename(dat));
                    hdr.file = strdup(buf);
                }
                free(dat);

                fl_set_form_position(GUI->menu->menu,fl_scrw/16,fl_scrh/8);
                if(!fl_form_is_visible(GUI->menu->menu))
                    fl_show_form_f(GUI->menu->menu,FL_PLACE_SIZE|FL_PLACE_POSITION,FL_TRANSIENT,TITLE_MENU);

                fl_show_oneliner(_("Loading data..."),fl_scrw/2,fl_scrh/2);
                g3d_ret ret;
                if((ret = load_data(ENV,&hdr)) != G3D_OK) {
                    fl_hide_oneliner();
                    fatal("Failed loading data from file: \"%s\" (%s)",file,g3d_ret_msg(ret));
                }
                fl_set_directory(dirname(hdr.file)); free(hdr.file); free(file);
                refresh_datasets_browser(GUI->datasets->browser,ENV->data);
                load_single_show_mainview(ENV);
            }
        } else if(argc <= 5) {
            errno = 0;
            hdr.file = realpath(file,NULL);
            if(NULL == hdr.file) fatal("Failed resolving path to \"%s\": %s (%d)",file,strerror(errno),errno);
            hdr.cal_slope = 1.0; hdr.cal_inter = 0.0;
            hdr.ox = hdr.oy = hdr.oz = 0;
            hdr.acqmod = ACQ_UNKNOWN;
            hdr.compr = file_compr_type(file);
#if __BYTE_ORDER == __LITTLE_ENDIAN
            hdr.endian = ENDIAN_LITTLE;
#else
            hdr.endian = ENDIAN_BIG;
#endif
            hdr.vx = 0.1; /* default value */
            if(sscanf(argv[2],"%"SCNcoord"x%"SCNcoord"x%"SCNcoord"@%lf",&hdr.dim[0],&hdr.dim[1],&hdr.dim[2],&hdr.vx) < 3)
                fatal("Wrong geometry argument: %s",argv[2]);
            if(hdr.dim[0] == 0 || hdr.dim[1] == 0 || hdr.dim[2] == 0)
                fatal("Zero dimension found: %d",0);
            hdr.vx = hdr.vy = hdr.vz = hdr.vx * MM_TO_CM;

            hdr.type = RAW_FLOAT32;
            if(argc >= 4) {
                if(parse_type_arg(argv[3],&hdr.type))
                    fatal("Wrong type argument: %s",argv[3]);
            }

            hdr.offset = 0;
            if(argc >= 5) {
                char *endptr = NULL;
                hdr.offset = strtoull(argv[4],&endptr,10); // get offset in bytes
                if('\0' != *endptr && hdr.type != RAW_BIT) {
                    if('c' == endptr[0] && '\0' == endptr[1]) // unit is [c]har (8-bit)
                        hdr.offset *= 1UL;
                    else if('w' == endptr[0] && '\0' == endptr[1]) // unit is [w]ord (16-bit)
                        hdr.offset *= 2UL;
                    else if('d' == endptr[0] && '\0' == endptr[1]) // unit is [d]word (32-bit)
                        hdr.offset *= 4UL;
                    else if('q' == endptr[0] && '\0' == endptr[1]) // unit is [q]word (64-bit)
                        hdr.offset *= 8UL;
                    else if('K' == endptr[0] && '\0' == endptr[1]) // unit is [K]ibibyte (1024 byte)
                        hdr.offset *= 1024UL;
                    else if('M' == endptr[0] && '\0' == endptr[1]) // unit is [M]ibibyte (1024^2 byte)
                        hdr.offset *= 1048576UL; // 1024 * 1024
                    else if('G' == endptr[0] && '\0' == endptr[1]) // unit is [G]ibibyte (1024^3 byte)
                        hdr.offset *= 1073741824UL; // 1024 * 1024 * 1024
                    else if('t' == endptr[0] && '\0' == endptr[1]) // units is sizeof [t]ype
                        hdr.offset *= g3d_sizeof_raw_io_type(hdr.type);
                    else if('l' == endptr[0] && '\0' == endptr[1]) // units is [l]ine
                        hdr.offset *= g3d_sizeof_raw_io_type(hdr.type) * hdr.dim[0];
                    else if('s' == endptr[0] && '\0' == endptr[1]) // units is [s]lice
                        hdr.offset *= g3d_sizeof_raw_io_type(hdr.type) * hdr.dim[0] * hdr.dim[1] ;
                    else fatal("Wrong offset argument: %s",argv[4]);
                }
            }

            fl_set_form_position(GUI->menu->menu,fl_scrw/16,fl_scrh/8);
            if(!fl_form_is_visible(GUI->menu->menu))
                fl_show_form_f(GUI->menu->menu,FL_PLACE_SIZE|FL_PLACE_POSITION,FL_TRANSIENT,TITLE_MENU);

            fl_show_oneliner(_("Loading data..."),fl_scrw/2,fl_scrh/2);
            if(is_filelist) {
                FILE *fp = fopen(file,"r");
                if(NULL == fp) fatal("Failed opening file list: \"%s\"",file);
                char *line = NULL;
                size_t len = 0;
                ssize_t nread;
                g3d_ret ret;
                %free(hdr.file);
                while(!(errno = 0) && (nread = getline(&line,&len,fp)) != -1) {
                    char *nl = strchr(line,'\n'); if(NULL != nl) *nl = '\0'; // remove newline
                    errno = 0;
                    hdr.file = realpath(line,NULL);
                    if(NULL == hdr.file) fatal("Failed resolving path to \"%s\": %s (%d)",line,strerror(errno),errno);
                    if((ret = load_data(ENV,&hdr)) != G3D_OK)
                        fatal("Failed loading data from file: \"%s\" (%s)",line,g3d_ret_msg(ret));
                    %free(hdr.file);
                }
                if(nread == -1 && errno) fatal("Failed reading line from file list: \"%s\"",file);
                free(line);
                fclose(fp);
                fl_hide_oneliner();
            } else {
                g3d_ret ret;
                if((ret = load_data(ENV,&hdr)) != G3D_OK) {
                    fl_hide_oneliner();
                    fatal("Failed loading data from file: \"%s\" (%s)",file,g3d_ret_msg(ret));
                }
            }
            fl_set_directory(dirname(hdr.file)); free(hdr.file); free(file);
            refresh_datasets_browser(GUI->datasets->browser,ENV->data);
            load_single_show_mainview(ENV);
        } else {
            fprintf(stderr,"Usage: %s <header_file>\n",argv[0]);
            fprintf(stderr,"   or: %s <file> <NX>x<NY>x<NZ>[@vox] <dtype> <offset>\n",argv[0]);
            fatal("Wrong number of arguments: %d instead of 1 or 5",argc - 1);
        }
    } else {
        fl_set_form_position(GUI->menu->menu,fl_scrw/16,fl_scrh/8);
        fl_show_form_f(GUI->menu->menu,FL_PLACE_SIZE|FL_PLACE_POSITION,FL_TRANSIENT,TITLE_MENU);
    }

    /* handle options */
    if('\0' != default_fun[0]) { // set default function
        fl_set_input(GUI->function->inp_fun,default_fun);
        fl_trigger_object(GUI->function->inp_fun);
    }

    /* start event loop */
    fl_do_forms();

    /* free environments */
    xg3d_env_free(ENV,true);
    xg3d_env_free(env_overlay,false);

    /* !!! FREE GLOBAL VARIABLES !!! */
    free(kernel_dir);
    /* !!! END FREE GLOBAL VARIABLES !!! */

    /* finish babl */
    babl_exit();

    /* cleanup FFTW threads */
    fftw_cleanup_threads();

    /* finish xforms */
    fl_finish();

    return 0;
}
