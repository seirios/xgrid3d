/*
 * line3d was dervied from DigitalLine.c published as "Digital Line Drawing"
 * by Paul Heckbert from "Graphics Gems", Academic Press, 1990
 * 
 * 3D modifications by Bob Pendleton. The original source code was in the public
 * domain, the author of the 3D version places his modifications in the
 * public domain as well.
 * 
 * line3d uses Bresenham's algorithm to generate the 3 dimensional points on a
 * line from (x1, y1, z1) to (x2, y2, z2)
 * 
 */

#include "dl.h"

/* take sign of a, either -1, 0, or 1 */
#define ZSGN(a) (((a)<0) ? -1 : (a)>0 ? 1 : 0)
/* absolute value */
#define ABS(a) ((a) < 0 ? (-(a)) : (a))
/* maximum value */
#define MAX(a,b) ((a) > (b) ? (a) : (b))

void line3d(int x1, int y1, int z1, int x2, int y2, int z2, _selopf *point3d, const int op, void *p) {
    int xd, yd, zd;
    int dx = x2 - x1, dy = y2 - y1, dz = z2 - z1;
    int ax = ABS(dx) << 1, ay = ABS(dy) << 1, az = ABS(dz) << 1;
    int sx = ZSGN(dx), sy = ZSGN(dy), sz = ZSGN(dz);
    int x = x1, y = y1, z = z1;

    if(ax >= MAX(ay,az)) { /* x dominant */
        yd = ay - (ax >> 1);
        zd = az - (ax >> 1);
		for(;;) {
            point3d(op,p,x,y,z);
            if(x == x2)
                return;
            if(yd >= 0) { y += sy; yd -= ax; }
            if(zd >= 0) { z += sz; zd -= ax; }
            x += sx; yd += ay; zd += az;
        }
    } else if(ay >= MAX(ax,az)) { /* y dominant */
        xd = ax - (ay >> 1);
        zd = az - (ay >> 1);
		for(;;) {
            point3d(op,p,x,y,z);
            if(y == y2)
                return;
            if(xd >= 0) { x += sx; xd -= ay; }
            if(zd >= 0) { z += sz; zd -= ay; }
            y += sy; xd += ax; zd += az;
        }
    } else if(az >= MAX(ax,ay)) { /* z dominant */
        xd = ax - (az >> 1);
        yd = ay - (az >> 1);
		for(;;) {
            point3d(op,p,x,y,z);
            if(z == z2)
                return;
            if(xd >= 0) { x += sx; xd -= az; }
            if(yd >= 0) { y += sy; yd -= az; }
            z += sz; xd += ax; yd += ay;
        }
    }
}
