#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "gengrid.h"

/* this function sums up all .pardose files and outputs an isotropic
 * kernel grid; for each distance within the grid it sums up all energy
 * deposited at that distance and divides between the number of voxels
 * at that distance, then outputting this same value on all of these
 * voxels, in units of Gy/particle */

/* FACTOR =
MeV * cm**3   J          1            J       1E6 * 1.60217662E-19 * 1E3
----------- * -- * ------------ = --------- * --------------------------
     g        eV   cm**3 * part   kg * part                1            
*/
#define FACTOR 1.60217662E-10 /* (1E9 * e), converts MeV to J and g to kg */
#define SQR(x) ((x)*(x))
#define SQD(x,y,z) (SQR(x)+SQR(y)+SQR(z))
#define IDX(i,j,k) ((i) + (j)*imax + (k)*imax*jmax)
int pardose(const char *egs_home, const char *name, size_t nfiles, double vox, double **o_dvk) {
    const double vol = vox * vox * vox;
    double *edep=NULL,*edep2=NULL;
    int imax,jmax,kmax;
    char file[1024];
    size_t ensize = 0;

    if(nfiles == 0) return 1;

    /* read all partial files */
    unsigned long int nrun = 0L;
    for(size_t n=1;n<=nfiles;n++) {
        snprintf(file,sizeof(file),"%s/dosxyznrc/%s_w%zu.pardose",egs_home,name,n);
        FILE *fp = fopen(file,"r");
        if(fp == NULL) { return 2; }
        double nhist;
        if(fread(&nhist,sizeof(double),1,fp) != 1) return 3; /* read histories in this file */
        nrun += (unsigned long int)nhist; /* add up total histories */
        /* grid dimensions are supposed to be odd and all equal */
        if(fread(&imax,sizeof(int),1,fp) != 1) return 3; /* read grid dimension x */
        if(fread(&jmax,sizeof(int),1,fp) != 1) return 3; /* read grid dimension y */
        if(fread(&kmax,sizeof(int),1,fp) != 1) return 3; /* read grid dimension z */
        if(n == 1) { /* allocate memory on first file */
            ensize = (size_t)imax * jmax * kmax; /* total voxels (same for all files) */
            edep = calloc(ensize,sizeof(double)); /* edep is 1/rho * sum_i E_i on each voxel */
            edep2 = calloc(ensize,sizeof(double));/* edep2 is 1/rho * sum_i (E_i)^2 on each voxel */
        }
        double *endep = calloc(ensize,sizeof(double)); /* same as edep but for this file */
        double *endep2 = calloc(ensize,sizeof(double));/* same as edep2 but for this file */
        if(fread(endep,sizeof(double),ensize,fp) != ensize) return 3; /* read edep for this file */
        if(fread(endep2,sizeof(double),ensize,fp) != ensize) return 3; /* read edep2 for this file */
        fclose(fp);
        for(size_t i=0;i<ensize;i++) {
            edep[i] += endep[i]; /* add edep of this file to global sum */
            edep2[i] += endep2[i];/*add edep2 of this file to global sum */
        }
        free(endep);
        free(endep2);
    }

    /* generate the squared distances and occupations for a sphere of cubic voxels,
     * that is, we only get distances of up to the radius of the cube */
    size_t ndist;
    uint32_t *occup,*dists;
    gengrid(imax/2,&ndist,&dists,&occup);

    /* these hold the total energy deposited as a function of distance */
    double *ede = calloc(ndist,sizeof(double));
    double *ede2 = calloc(ndist,sizeof(double)); /* energy squared */

    /* iterate over all voxels in grid */
    for(int i=0;i<imax;i++)
        for(int j=0;j<jmax;j++)
            for(int k=0;k<kmax;k++) {
                uint32_t key = SQD(i-imax/2,j-jmax/2,k-kmax/2); /* squared distance to this voxel */
                uint32_t *res = bsearch(&key,dists,ndist,sizeof(uint32_t),cmp_uint32_t); /* find index of distance */
                if(res != NULL) { /* if distance found, doesn't happen for voxels beyond the inscribed sphere  */
                    ede[res-dists] += edep[IDX(i,j,k)]; /* add up deposited energy from this voxel at that distance */
                    ede2[res-dists] += edep2[IDX(i,j,k)];/*add up deposited energy squared from this voxel at that distance */
                    /*   ^^^^^^^^^ pointer subtraction! @res is in principle >= @dists */
                }
            }
    free(edep);
    free(edep2);

    /* iterate over distances */
    for(size_t n=0;n<ndist;n++) {
        /* isotropize dividing by the number of voxels at that distance */
        ede[n] /= (double)occup[n];
        ede2[n] /= (double)occup[n];
        /* statistical analysis, identical to the one DOSXYZnrc does */
        ede2[n] = sqrt((ede2[n]/nrun - SQR(ede[n]/nrun))/(nrun - 1)); /* compute stdev */
        ede2[n] /= ede[n]/nrun; /* relative error */
        ede[n] *= FACTOR / (vol * nrun); /* normalize dose to Gy/particle */
    }

    /* allocate output matrix */
    double *dvk = calloc(ensize,sizeof(double));
    /* iterate over all voxels */
    for(int i=0;i<imax;i++)
        for(int j=0;j<jmax;j++)
            for(int k=0;k<kmax;k++) {
                uint32_t key = SQD(i-imax/2,j-jmax/2,k-kmax/2); /* squared distance to this voxel */
                uint32_t *res = bsearch(&key,dists,ndist,sizeof(uint32_t),cmp_uint32_t); /* find index of distance */
                if(res != NULL) /* if distance found, same remark as above */
                    dvk[IDX(i,j,k)] = ede[res-dists]; /* set voxel to the dose for that distance */
            }
    /* store result */
    if(o_dvk != NULL) *o_dvk = dvk;

    free(ede);
    free(ede2);

    free(occup);
    free(dists);

    return 0; /* 1 is zero files, 2 is error in file open, 3 is error in file parse */
}
