#include <libgen.h>
#include <math.h>
#include <matheval.h>

#include <gsl/gsl_sort.h>
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_randist.h>

#ifndef G3D_h_
#define G3D_h_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <stdbool.h>
#include <complex.h>
#ifdef _OPENMP
#include <omp.h>
#endif
/* one-liner to detect endianness */
#define IS_BIG_ENDIAN (*(uint16_t *)"\0\xff" < 0x100)
/* type for a single dimension */
typedef uint16_t coord_t;
#define COORD_MIN 0
#define COORD_MAX UINT16_MAX
#define PRIcoord PRIu16
#define SCNcoord SCNu16
/* signed type for a single dimension
 * 17-bits would suffice though */
typedef int32_t scoord_t;
#define SCOORD_MIN INT32_MIN
#define SCOORD_MAX INT32_MAX
#define PRIscoord PRIi16
#define SCNscoord SCNi16
/* type for the product of two dimensions */
typedef uint32_t slice_t;
#define SLICE_MIN 0
#define SLICE_MAX UINT32_MAX
#define PRIslice PRIu32
#define SCNslice SCNu32
/* type for the product of three (or four) dimensions */
typedef uint64_t index_t;
#define INDEX_MIN 0
#define INDEX_MAX UINT64_MAX
#define PRIindex PRIu64
#define SCNindex SCNu64
#define g3d_foreach(x,ix) for(index_t (ix) = 0; (ix) < (x)->nvox; ++(ix))
/* grid3d type */
enum g3d_type {
            G3D_FLOAT32, // 3d grid of float
           G3D_FLOAT64, // 3d grid of double
           G3D_UINT8, // 3d grid of uint8_t
           G3D_UINT16, // 3d grid of uint16_t
           G3D_UINT32, // 3d grid of uint32_t
           G3D_UINT64, // 3d grid of uint64_t
           G3D_INT8, // 3d grid of int8_t
           G3D_INT16, // 3d grid of int16_t
           G3D_INT32, // 3d grid of int32_t
           G3D_INT64, // 3d grid of int64_t
       G3D_LAST_STATIC = G3D_INT64, // last static data type
    G3D_LIVE // 3d grid computed on the fly
};
/* Quick access if type known */
#define g3d_f32_p(g3d) ((float*)((grid3d*)(g3d))->dat)
#define g3d_f32(g3d,ix) (g3d_f32_p(g3d)[ix])
#define g3d_f64_p(g3d) ((double*)((grid3d*)(g3d))->dat)
#define g3d_f64(g3d,ix) (g3d_f64_p(g3d)[ix])
#define g3d_u8_p(g3d) ((uint8_t*)((grid3d*)(g3d))->dat)
#define g3d_u8(g3d,ix) (g3d_u8_p(g3d)[ix])
#define g3d_u16_p(g3d) ((uint16_t*)((grid3d*)(g3d))->dat)
#define g3d_u16(g3d,ix) (g3d_u16_p(g3d)[ix])
#define g3d_u32_p(g3d) ((uint32_t*)((grid3d*)(g3d))->dat)
#define g3d_u32(g3d,ix) (g3d_u32_p(g3d)[ix])
#define g3d_u64_p(g3d) ((uint64_t*)((grid3d*)(g3d))->dat)
#define g3d_u64(g3d,ix) (g3d_u64_p(g3d)[ix])
#define g3d_i8_p(g3d) ((int8_t*)((grid3d*)(g3d))->dat)
#define g3d_i8(g3d,ix) (g3d_i8_p(g3d)[ix])
#define g3d_i16_p(g3d) ((int16_t*)((grid3d*)(g3d))->dat)
#define g3d_i16(g3d,ix) (g3d_i16_p(g3d)[ix])
#define g3d_i32_p(g3d) ((int32_t*)((grid3d*)(g3d))->dat)
#define g3d_i32(g3d,ix) (g3d_i32_p(g3d)[ix])
#define g3d_i64_p(g3d) ((int64_t*)((grid3d*)(g3d))->dat)
#define g3d_i64(g3d,ix) (g3d_i64_p(g3d)[ix])
enum g3d_slice_plane {
PLANE_XY = 0, /* seen from front */
PLANE_XZ = 1, /* seen from bottom */
PLANE_ZY = 2, /* seen from port */
};
#define enum_g3d_slice_plane_len 3
/* Coordinate index macros [DO NOT MODIFY!] */
#define PLANE_TO_J(plane) ((plane) & PLANE_ZY)
#define PLANE_TO_I(plane) (1U + ((plane) & PLANE_XZ))
#define PLANE_TO_K(plane) (2U - (plane))
/* Return type */
#define g3d_ret_error(ret,func) error("%s: %s",func,g3d_ret_msg(ret))
typedef enum {
    G3D_OK,
    G3D_FAIL,
    G3D_ENOFILE,
    G3D_EIO,
    G3D_ENOMEM,
    G3D_ENOINPUT,
    G3D_ENOOP,
    G3D_EBADGEOM,
    G3D_EBADINPUT,
} g3d_ret;
enum g3d_endian_type {
ENDIAN_KEEP = 0, /* keep endianness */
ENDIAN_LITTLE = 1, /* little endian */
ENDIAN_BIG = 2, /* big endian */
};
#define enum_g3d_endian_type_len 3
enum g3d_processing_pref {
PROC_PREFER_VALUE = 0, /* process per value */
PROC_PREFER_SLICE = 1, /* process per slice */
};
#define enum_g3d_processing_pref_len 2
enum g3d_complex_mode {
COMPLEX_REAL = 0, /* real part */
COMPLEX_IMAG = 1, /* imaginary part */
COMPLEX_NORM = 2, /* complex norm */
COMPLEX_PHASE = 3, /* complex phase */
};
#define enum_g3d_complex_mode_len 4
/* grid3d method macros (for historical reasons) */
#define g3d_get_dvalue(x,ix)     (((grid3d*)x)->getdval (x,ix))
#define g3d_set_dvalue(x,ix,val) (((grid3d*)x)->setdval (x,ix,val))
#define g3d_slice(x,ix,p)        (((grid3d*)x)->getslice(x,ix,p))
/**
 * grid3d
 * Holds a three-dimensional grid of numbers
 * stored in row-major order.
 */
typedef struct _grid3d {
    /* get value at index as double */
    double (*getdval)(const struct _grid3d*,const index_t);
    /* store value at index from double */
    void   (*setdval)(struct _grid3d*,const index_t,const double);
    /* extract slice along plane as a new grid3d of the same type */ 
    struct _grid3d* (*getslice)(const struct _grid3d*,const coord_t,const enum g3d_slice_plane);
    void *dat; // untyped data array
    void *ext; // extra untyped data for G3D_LIVE
    index_t nvox; // total voxels
    slice_t nxy; // XY dimension 
    coord_t nx; // X dimension
    coord_t ny; // Y dimension
    coord_t nz; // Z dimension
    enum g3d_type type; // data type
    enum g3d_processing_pref ppref; // processing preference
} grid3d;
/* Modes for computing statistics */
enum g3d_stats_mode {
            G3D_STATS_MIN = (1UL << (0 + 0)),
           G3D_STATS_MAX = (1UL << (0 + 1)),
           G3D_STATS_RANGE = (1UL << (0 + 2)),
           G3D_STATS_SUM = (1UL << (0 + 3)),
               G3D_STATS_MEAN = (1UL << (4 + 0)),
           G3D_STATS_STDEV = (1UL << (4 + 1)),
               G3D_STATS_MEDIAN = (1UL << (4 + 2 + 0)),
           G3D_STATS_PCTL05 = (1UL << (4 + 2 + 1)),
           G3D_STATS_PCTL10 = (1UL << (4 + 2 + 2)),
       G3D_STATS_MINMAXSUM = 0UL | G3D_STATS_MIN| G3D_STATS_MAX| G3D_STATS_RANGE| G3D_STATS_SUM,
    G3D_STATS_MEANSTDEV = 0UL | G3D_STATS_MEAN| G3D_STATS_STDEV,
    G3D_STATS_QUANTILES = 0UL | G3D_STATS_MEDIAN| G3D_STATS_PCTL05| G3D_STATS_PCTL10,
    G3D_STATS_ALL = 0UL | G3D_STATS_MINMAXSUM| G3D_STATS_MEANSTDEV| G3D_STATS_QUANTILES};
/**
 * grid3d statistics
 */
typedef struct {
            double min; // minimum
           double max; // maximum
           double range; // max - min
           double sum; // sum
               double mean; // mean
           double stdev; // standard deviation
               double median; // median
           double pctl05; // 5th percentile
           double pctl10; // 10th percentile
   } g3d_stats;
/* Return sizeof data array elements from g3d_type */
inline static size_t g3d_sizeof(const enum g3d_type type) {
    switch(type) {
                    case G3D_FLOAT32: return sizeof(float); break;
                   case G3D_FLOAT64: return sizeof(double); break;
                   case G3D_UINT8: return sizeof(uint8_t); break;
                   case G3D_UINT16: return sizeof(uint16_t); break;
                   case G3D_UINT32: return sizeof(uint32_t); break;
                   case G3D_UINT64: return sizeof(uint64_t); break;
                   case G3D_INT8: return sizeof(int8_t); break;
                   case G3D_INT16: return sizeof(int16_t); break;
                   case G3D_INT32: return sizeof(int32_t); break;
                   case G3D_INT64: return sizeof(int64_t); break;
               default: return 0; break;
    }
}
/* Fill dimensions */
inline static void g3d_setup_dim(grid3d *const x) {
    x->nxy = (slice_t)x->nx * x->ny;
    x->nvox = (index_t)x->nz * x->nxy;
}




struct g3d_alloc__args {
int _;
const enum g3d_type type ;
const coord_t nx ;
const coord_t ny ;
const coord_t nz ;
const bool alloc ;
};
grid3d * ( g3d_alloc__ ) ( struct g3d_alloc__args argv ) ;
#define g3d_alloc(...) g3d_alloc__((struct g3d_alloc__args){ ._=0, __VA_ARGS__ })
void ( _g3d_free ) ( grid3d * const x  ) ;
#define g3d_free(x) { _g3d_free(x); x = NULL; }
grid3d * ( g3d_dup ) ( const grid3d * const x  ) ;
void ( g3d_dfill ) ( const grid3d * const x  , const double fill  ) ;
struct g3d_alloc_dfill__args {
int _;
const enum g3d_type type ;
const coord_t nx ;
const coord_t ny ;
const coord_t nz ;
const double * const data ;
};
grid3d * ( g3d_alloc_dfill__ ) ( struct g3d_alloc_dfill__args argv ) ;
#define g3d_alloc_dfill(...) g3d_alloc_dfill__((struct g3d_alloc_dfill__args){ ._=0, __VA_ARGS__ })
typedef double ( G3D_DAPPLY_FUNC ) ( double x  , const void * p  ) ;
g3d_ret ( g3d_dapply ) ( grid3d * x  , G3D_DAPPLY_FUNC * f  , const void * p  ) ;
grid3d * ( g3d_add ) ( const grid3d * * const x  , const enum g3d_type type  , const coord_t nx  , const coord_t ny  , const coord_t nz  , const size_t n  ) ;
grid3d * ( g3d_dadd ) ( const grid3d * * const x  , const coord_t nx  , const coord_t ny  , const coord_t nz  , const size_t n  ) ;
g3d_ret ( g3d_byteswap ) ( grid3d * x  ) ;
typedef g3d_ret ( G3D_REPLACE_SLICE_FUNC ) ( grid3d * const x  , const grid3d * const slice  , const coord_t k  , const enum g3d_slice_plane plane  ) ;
G3D_REPLACE_SLICE_FUNC g3d_replace_slice;
G3D_REPLACE_SLICE_FUNC g3d_replace_dslice;
G3D_REPLACE_SLICE_FUNC g3d_replace_fslice;
grid3d * ( g3d_trim ) ( const grid3d * x  , const coord_t ox  , const coord_t oy  , const coord_t oz  , coord_t nx  , coord_t ny  , coord_t nz  ) ;
g3d_ret ( g3d_trim_inplace ) ( grid3d * x  , const coord_t ox  , const coord_t oy  , const coord_t oz  , coord_t nx  , coord_t ny  , coord_t nz  ) ;
g3d_ret ( g3d_trim_margins ) ( const grid3d * const x  , const double fill  , coord_t * jm_o  , coord_t * im_o  , coord_t * km_o  , coord_t * jM_o  , coord_t * iM_o  , coord_t * kM_o  ) ;
grid3d * ( g3d_trim_value ) ( const grid3d * const x  , const double fill  ) ;
g3d_ret ( g3d_trim_value_inplace ) ( grid3d * const x  , const double fill  ) ;
grid3d * ( g3d_bleed ) ( const grid3d * const x  , const int ox  , const int oy  , const int oz  , const coord_t nx  , const coord_t ny  , const coord_t nz  , const double fill  ) ;
g3d_stats * ( g3d_stats_dup ) ( const g3d_stats * const x  ) ;
g3d_stats * ( g3d_stats_get ) ( const grid3d * const x  , const double * calib  , const enum g3d_stats_mode modemask  ) ;
__attribute__ ((const))
const char * ( g3d_ret_msg ) ( const g3d_ret ret  ) ;
#endif
#include "grid3d_coord.h"
#include "grid3d_resamp.h"
#include "grid3d_isom.h"
#include "grid3d_fftw.h"
#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "cb_operations.h"
#include "cb_datasets.h"
#include "cb_info.h"
#include "cb_viewport.h"
#include "cb_mainview.h"
#include "cb_overview.h"
#include "cb_quantization.h"
#include "cb_rois.h"
#include "fd_overlay.h"
#include "fd_dose.h"

#include "selection.h"
#include "operations.h"

#include "io.h"
#include "util.h"
#include "util_filter.h"
#include "util_coordinates.h"
#include "util_units.h"

#include "icons/link_open.xbm"
#include "icons/link_closed.xbm"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#define FORMAT_CLIP_PRINT "%ux%ux%u%+d%+d%+d"
#define FORMAT_CLIP_SCAN  "%ux%ux%u%d%d%d"

static void _backup_data(FD_operations *operations, const xg3d_data *data) {
    xg3d_data_clear(BACKUP_DATA(operations),false);
    if(fl_get_button(operations->btn_undo_toggle)) {
        BACKUP_G3D(operations) = g3d_dup(data->g3d);
        BACKUP_HDR(operations) = xg3d_header_dup(data->hdr);
    }
}

#undef ENV
static void _reset_from_data(xg3d_env *ENV, const bool resize) {
    if(!fl_get_button(GUI->operations->btn_undo_toggle))
        xg3d_data_clear(BACKUP_DATA(GUI->operations),false);
    g3d_free(SELECTION_G3D(GUI->selection));
    SELECTION_G3D(GUI->selection) = NULL;
    setup_from_data(ENV,resize);
    if(fl_get_button(GUI->overlay->btn_show)) {
        fl_set_button(GUI->overlay->btn_show,0);
        fl_trigger_object(GUI->overlay->btn_show);
    }
    refresh_canvas(ENV);
    refresh_ov_canvases(GUI->overview);
}
#define ENV _default_env

void update_op_crop_inputs(FD_operations *operations, const xg3d_header *hdr) {
    const double unit = fl_get_button(operations->btn_unit) ? CM_TO_MM : 1.0;
        fl_set_input_f(operations->inp_off_x,"%.4g",
            fl_get_spinner_value(operations->spin_off_x) * hdr->vx * unit);
    fl_set_input_f(operations->inp_ext_x,"%.4g",
            fl_get_spinner_value(operations->spin_ext_x) * hdr->vx * unit);
       fl_set_input_f(operations->inp_off_y,"%.4g",
            fl_get_spinner_value(operations->spin_off_y) * hdr->vy * unit);
    fl_set_input_f(operations->inp_ext_y,"%.4g",
            fl_get_spinner_value(operations->spin_ext_y) * hdr->vy * unit);
       fl_set_input_f(operations->inp_off_z,"%.4g",
            fl_get_spinner_value(operations->spin_off_z) * hdr->vz * unit);
    fl_set_input_f(operations->inp_ext_z,"%.4g",
            fl_get_spinner_value(operations->spin_ext_z) * hdr->vz * unit);
   }

void cb_op_crop_bounds ( FL_OBJECT * obj  , long data  ) {
    coord_t jm,im,km,jM,iM,kM;

    switch(data) {
        default:
        case 0: /* reset */
            jm = im = km = 0;
            jM = DATA->g3d->nx - 1;
            iM = DATA->g3d->ny - 1;
            kM = DATA->g3d->nz - 1;
            break;
        case 1: /* bounds from selection */
            if(SELECTION_NVOX(GUI->selection) == 0) {
                fl_call_object_callback(GUI->operations->btn_crop_reset);
                return;
            }
            jm = im = km = COORD_MAX;
            jM = iM = kM = 0;
#if defined(_OPENMP) && _OPENMP >= 201107
#pragma omp parallel for reduction(min:jm) reduction(min:im) reduction(min:km) reduction(max:jM) reduction(max:iM) reduction(max:kM)
#endif
            for(index_t ix=0;ix<DATA->g3d->nvox;ix++)
                if(g3d_selbit_check(SELECTION_G3D(GUI->selection),ix)) {
                    coord_t jik[3];
                    UNCOORD_DIM(ix,jik,DATA->g3d->nx,DATA->g3d->nxy);
                                            (jm) = ((jik[0]) < (jm)) ? (jik[0]) : (jm);
                        (jM) = ((jik[0]) > (jM)) ? (jik[0]) : (jM);
                                           (im) = ((jik[1]) < (im)) ? (jik[1]) : (im);
                        (iM) = ((jik[1]) > (iM)) ? (jik[1]) : (iM);
                                           (km) = ((jik[2]) < (km)) ? (jik[2]) : (km);
                        (kM) = ((jik[2]) > (kM)) ? (jik[2]) : (kM);
                                   }
            break;
    }
        fl_set_spinner_value(GUI->operations->spin_off_x,jm);
    fl_set_spinner_value(GUI->operations->spin_ext_x,jM - jm + 1);
       fl_set_spinner_value(GUI->operations->spin_off_y,im);
    fl_set_spinner_value(GUI->operations->spin_ext_y,iM - im + 1);
       fl_set_spinner_value(GUI->operations->spin_off_z,km);
    fl_set_spinner_value(GUI->operations->spin_ext_z,kM - km + 1);
       update_op_crop_inputs(GUI->operations,DATA->hdr);
}

void cb_op_crop_units ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    fl_set_object_label(obj,fl_get_button(obj) ? "mm" : "cm");
    update_op_crop_inputs(GUI->operations,DATA->hdr);
}

static int _bounds_sel(FL_OBJECT *obj FL_UNUSED_ARG, long type FL_UNUSED_ARG, const void *data, long size) {
    unsigned int ext_x,ext_y,ext_z;
    int off_x,off_y,off_z;

    if(data != NULL && size > 0) {
        if(sscanf((const char*)data,FORMAT_CLIP_SCAN,&ext_x,&ext_y,&ext_z,&off_x,&off_y,&off_z) == 6) {
                        fl_set_spinner_value(GUI->operations->spin_off_x,off_x);
            fl_set_spinner_value(GUI->operations->spin_ext_x,ext_x);
                       fl_set_spinner_value(GUI->operations->spin_off_y,off_y);
            fl_set_spinner_value(GUI->operations->spin_ext_y,ext_y);
                       fl_set_spinner_value(GUI->operations->spin_off_z,off_z);
            fl_set_spinner_value(GUI->operations->spin_ext_z,ext_z);
                       update_op_crop_inputs(GUI->operations,DATA->hdr);
            return 1;
        } else
            return 0;
    } else
        return 0;
}

void cb_op_crop_clip ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const unsigned int extx = fl_get_spinner_value(GUI->operations->spin_ext_x);
    const unsigned int exty = fl_get_spinner_value(GUI->operations->spin_ext_y);
    const unsigned int extz = fl_get_spinner_value(GUI->operations->spin_ext_z);
    const int offx = fl_get_spinner_value(GUI->operations->spin_off_x);
    const int offy = fl_get_spinner_value(GUI->operations->spin_off_y);
    const int offz = fl_get_spinner_value(GUI->operations->spin_off_z);
    char buf[32];

    switch(fl_get_menu(obj)) {
        case 1: /* Copy */
            snprintf(buf,sizeof(buf),FORMAT_CLIP_PRINT,extx,exty,extz,offx,offy,offz);
            fl_stuff_clipboard(obj,0,buf,strlen(buf),NULL);
            break;
        case 2: /* Paste */
            fl_request_clipboard(obj,0,_bounds_sel);
            break;
    }
}

void cb_op_crop_inp_off ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *spin = NULL;
    const char *tval = fl_get_input(obj);
    double val,unit,invunit,factor=1.0;

    switch(data) {
        case 0:
            spin = GUI->operations->spin_off_x;
            factor = DATA->hdr->vx;
            break;
        case 1:
            spin = GUI->operations->spin_off_y;
            factor = DATA->hdr->vy;
            break;
        case 2:
            spin = GUI->operations->spin_off_z;
            factor = DATA->hdr->vz;
            break;
    }

    if(fl_get_button(GUI->operations->btn_unit)) {
        unit = CM_TO_MM;
        invunit = MM_TO_CM / factor;
    } else {
        unit = 1.0;
        invunit = 1.0 / factor;
    }

    if(tval != NULL && strlen(tval) > 0) {
        val = strtod(tval,NULL);
        val = roundf(val*invunit);
        fl_set_spinner_value(spin,val);
        fl_call_object_callback(spin);
    } else
        fl_set_input_f(obj,"%.4g",fl_get_spinner_value(spin)*factor*unit);
}

void cb_op_crop_inp_ext ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *spin = NULL;
    const char *tval = fl_get_input(obj);
    double val,unit,invunit,factor=1.0;
    unsigned int bound = 0;
    char *eptr;

    switch(data) {
        case 0:
            spin = GUI->operations->spin_ext_x;
            factor = DATA->hdr->vx;
            bound = DATA->g3d->nx;
            break;
        case 1:
            spin = GUI->operations->spin_ext_y;
            factor = DATA->hdr->vy;
            bound = DATA->g3d->ny;
            break;
        case 2:
            spin = GUI->operations->spin_ext_z;
            factor = DATA->hdr->vz;
            bound = DATA->g3d->nz;
            break;
    }

    if(fl_get_button(GUI->operations->btn_unit)) {
        unit = CM_TO_MM;
        invunit = MM_TO_CM / factor;
    } else {
        unit = 1.0;
        invunit = 1.0 / factor;
    }

    if(tval != NULL && strlen(tval) > 0) {
        val = strtod(tval,&eptr);
        if(*eptr == '%')
            val = roundf(val/100.0 * bound);
        else
            val = roundf(val*invunit);
        fl_set_spinner_value(spin,val);
        fl_call_object_callback(spin);
    } else
        fl_set_input_f(obj,"%.4g",fl_get_spinner_value(spin)*factor*unit);
}

void cb_op_crop_spin ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *inp = NULL, *spin = NULL;
    const double unit = fl_get_button(GUI->operations->btn_unit) ? CM_TO_MM : 1.0;
    const int val = fl_get_spinner_value(obj);
    double factor=1.0;
    int dim = 0;

    switch(data) {
        case 0:
            inp = GUI->operations->inp_off_x;
            spin = GUI->operations->spin_ext_x;
            factor = DATA->hdr->vx;
            dim = DATA->g3d->nx;
            break;
        case 1:
            inp = GUI->operations->inp_off_y;
            spin = GUI->operations->spin_ext_y;
            factor = DATA->hdr->vy;
            dim = DATA->g3d->ny;
            break;
        case 2:
            inp = GUI->operations->inp_off_z;
            spin = GUI->operations->spin_ext_z;
            factor = DATA->hdr->vz;
            dim = DATA->g3d->nz;
            break;
        case 3:
            inp = GUI->operations->inp_ext_x;
            factor = DATA->hdr->vx;
            break;
        case 4:
            inp = GUI->operations->inp_ext_y;
            factor = DATA->hdr->vy;
            break;
        case 5:
            inp = GUI->operations->inp_ext_z;
            factor = DATA->hdr->vz;
            break;
    }
    fl_set_input_f(inp,"%.4g",val*factor*unit);
    if(fl_get_button(GUI->operations->btn_link) && dim > 0) {
        fl_set_spinner_value(spin,dim-val);
        fl_call_object_callback(spin);
    }
}

void cb_op_btn_link ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    if(fl_get_button(obj))
        fl_set_bitmapbutton_data(obj,link_closed_width,link_closed_height,link_closed_bits);
    else
        fl_set_bitmapbutton_data(obj,link_open_width,link_open_height,link_open_bits);
}

void cb_op_btn_center ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    if(fl_get_button(obj)) {
        deactivate_obj(GUI->operations->spin_off_x);
        deactivate_obj(GUI->operations->spin_off_y);
        deactivate_obj(GUI->operations->spin_off_z);
        deactivate_obj(GUI->operations->inp_off_x);
        deactivate_obj(GUI->operations->inp_off_y);
        deactivate_obj(GUI->operations->inp_off_z);
    } else {
        activate_obj(GUI->operations->spin_off_x,FL_MCOL);
        activate_obj(GUI->operations->spin_off_y,FL_MCOL);
        activate_obj(GUI->operations->spin_off_z,FL_MCOL);
        activate_obj(GUI->operations->inp_off_x,FL_MCOL);
        activate_obj(GUI->operations->inp_off_y,FL_MCOL);
        activate_obj(GUI->operations->inp_off_z,FL_MCOL);
    }
}

void cb_op_crop ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const double fill = strtod_checked(fl_get_input(GUI->operations->inp_fillval),0.0);
    const unsigned int extx = fl_get_spinner_value(GUI->operations->spin_ext_x);
    const unsigned int exty = fl_get_spinner_value(GUI->operations->spin_ext_y);
    const unsigned int extz = fl_get_spinner_value(GUI->operations->spin_ext_z);
    const coord_t nx = DATA->g3d->nx;
    const coord_t ny = DATA->g3d->ny;
    const coord_t nz = DATA->g3d->nz;
    int offx = fl_get_spinner_value(GUI->operations->spin_off_x);
    int offy = fl_get_spinner_value(GUI->operations->spin_off_y);
    int offz = fl_get_spinner_value(GUI->operations->spin_off_z);

    if(offx == 0 && offy == 0 && offz == 0 && extx == nx && exty == ny && extz == nz) {
        fl_show_alert2(0,_("No cropping performed!\fInitial and final geometries are the same."));
        return;
    }

    if(fl_get_button(GUI->operations->btn_crop_center)) {
        offx = (((int)nx-(int)extx) / 2);
        offy = (((int)ny-(int)exty) / 2);
        offz = (((int)nz-(int)extz) / 2);
    }

    if(offx >= 0 && offx+extx <= nx && offy >= 0 && offy+exty <= ny && offz >= 0 && offz+extz <= nz) { /* trimming, do in-place */
        _backup_data(GUI->operations,DATA);
        g3d_ret ret = g3d_trim_inplace(DATA->g3d,offx,offy,offz,extx,exty,extz);
        if(ret != G3D_OK) { g3d_ret_error(ret,"g3d_trim_inplace"); return; }
    } else { /* bleeding, force use backup data. TODO: bleed in-place (libgrid3d) */
        xg3d_data_clear(BACKUP_DATA(GUI->operations),false);
        BACKUP_G3D(GUI->operations) = g3d_dup(DATA->g3d);
        BACKUP_HDR(GUI->operations) = xg3d_header_dup(DATA->hdr);
        g3d_free(DATA->g3d);
        DATA->g3d = g3d_bleed(BACKUP_G3D(GUI->operations),offx,offy,offz,extx,exty,extz,fill);
    }
    _reset_from_data(ENV,true);
}

void cb_op_crop_auto ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const double fill = strtod_checked(fl_get_input(GUI->operations->inp_fillval),0.0);
    coord_t jm,im,km,jM,iM,kM;
    char buf[32];

    if(g3d_trim_margins(DATA->g3d,fill,&jm,&im,&km,&jM,&iM,&kM) != G3D_OK) return;
    if(jm == 0 && im == 0 && km == 0 &&
        jM == DATA->g3d->nx-1 && iM == DATA->g3d->ny-1 && kM == DATA->g3d->nz-1) {
        fl_show_alert2(0,_("No cropping performed!\fInitial and final geometries are the same."));
        return;
    }
    _backup_data(GUI->operations,DATA);
    {
        g3d_ret ret = g3d_trim_inplace(DATA->g3d,jm,im,km,jM - jm + 1,iM - im + 1,kM - km + 1);
        if(ret != G3D_OK) { g3d_ret_error(ret,"g3d_trim_inplace"); return; }
    }

    fl_show_alert2(0,_("Autocrop information\fExtents x: %u y: %u z: %u\nOffsets x: %d y: %d z: %d"),jM-jm+1,iM-im+1,kM-km+1,jm,im,km);
    snprintf(buf,sizeof(buf),FORMAT_CLIP_PRINT,jM-jm+1,iM-im+1,kM-km+1,jm,im,km);
    fl_stuff_clipboard(obj,0,buf,strlen(buf),NULL);

    _reset_from_data(ENV,true);
}

void cb_op_replace ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const char* tval = fl_get_input(GUI->operations->inp_replace);
    void *f;

    _backup_data(GUI->operations,DATA);
    if(tval == NULL || strlen(tval) == 0 || (f = evaluator_create((char*)tval)) == NULL) {
        fl_show_alert2(0,_("Invalid input!\fPlease enter a valid function."));
        return;
    }
    evaluator_destroy(f);

    DATA->hdr->type = RAW_FLOAT32;
    grid3d *new = g3d_convert(DATA->g3d,G3D_FLOAT32,ENDIAN_KEEP);
    if(new == NULL) return; /* malloc failed */
    g3d_free(DATA->g3d);
    DATA->g3d = new;

    if(SELECTION_NVOX(GUI->selection) == 0) { /* apply to whole */
#ifdef _OPENMP
#pragma omp parallel private(f)
        {
#endif
            f = evaluator_create((char*)tval);
#ifdef _OPENMP
#pragma omp for
#endif
            for(index_t ix=0;ix<DATA->g3d->nvox;ix++)
                g3d_set_dvalue(DATA->g3d,ix,evaluator_evaluate_x(f,g3d_get_dvalue(DATA->g3d,ix)));
            evaluator_destroy(f);
#ifdef _OPENMP
        }
#endif
    } else { /* apply to selection */
#ifdef _OPENMP
#pragma omp parallel private(f)
        {
#endif
            f = evaluator_create((char*)tval);
#ifdef _OPENMP
#pragma omp for
#endif
            for(index_t ix=0;ix<DATA->g3d->nvox;ix++)
                if(g3d_selbit_check(SELECTION_G3D(GUI->selection),ix))
                    g3d_set_dvalue(DATA->g3d,ix,evaluator_evaluate_x(f,g3d_get_dvalue(DATA->g3d,ix)));
            evaluator_destroy(f);
#ifdef _OPENMP
        }
#endif
    }

    _reset_from_data(ENV,false);
}

void cb_op_resamp_btn ( FL_OBJECT * obj  , long data  ) {
    fl_clear_choice(GUI->operations->ch_resamp_mode);
    switch(data) {
        case 0: /* downsample */
            fl_addto_choice_f(GUI->operations->ch_resamp_mode,_("Average"));
            fl_addto_choice_f(GUI->operations->ch_resamp_mode,_("Linear interpolation"));
            fl_set_choice(GUI->operations->ch_resamp_mode,2);
            break;
        case 1: /* upsample */
            fl_addto_choice_f(GUI->operations->ch_resamp_mode,_("No interpolation (blocky)"));
            fl_addto_choice_f(GUI->operations->ch_resamp_mode,_("Linear interpolation"));
            fl_set_choice(GUI->operations->ch_resamp_mode,2);
            break;
    }
    fl_call_object_callback(GUI->operations->ch_resamp_mode);
}

void cb_op_resamp_mode ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    switch(fl_get_choice(obj)) {
        case 1:	/* RESAMP_AVERAGE */
            fl_set_spinner_value(GUI->operations->spin_sx,
                    round(fl_get_spinner_value(GUI->operations->spin_sx)));
            fl_set_spinner_value(GUI->operations->spin_sy,
                    round(fl_get_spinner_value(GUI->operations->spin_sy)));
            fl_set_spinner_value(GUI->operations->spin_sz,
                    round(fl_get_spinner_value(GUI->operations->spin_sz)));
            fl_set_spinner_precision(GUI->operations->spin_sx,0);
            fl_set_spinner_precision(GUI->operations->spin_sy,0);
            fl_set_spinner_precision(GUI->operations->spin_sz,0);
            fl_set_spinner_step(GUI->operations->spin_sx,1.0);
            fl_set_spinner_step(GUI->operations->spin_sy,1.0);
            fl_set_spinner_step(GUI->operations->spin_sz,1.0);
            break;
        case 2: /* RESAMP_LINEAR */
            fl_set_spinner_precision(GUI->operations->spin_sx,2);
            fl_set_spinner_precision(GUI->operations->spin_sy,2);
            fl_set_spinner_precision(GUI->operations->spin_sz,2);
            fl_set_spinner_step(GUI->operations->spin_sx,0.01);
            fl_set_spinner_step(GUI->operations->spin_sy,0.01);
            fl_set_spinner_step(GUI->operations->spin_sz,0.01);
            break;
    }
}

void cb_op_resample ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const enum g3d_resamp_mode mode = fl_get_choice(GUI->operations->ch_resamp_mode)-1;
    const double fill = strtod_checked(fl_get_input(GUI->operations->inp_fillval),0.0);
    const double sx = fl_get_spinner_value(GUI->operations->spin_sx);
    const double sy = fl_get_spinner_value(GUI->operations->spin_sy);
    const double sz = fl_get_spinner_value(GUI->operations->spin_sz);
    const bool doble = fl_get_button(GUI->operations->btn_double);
    const int norm = fl_get_button(GUI->operations->btn_norm);
    double vx,vy,vz;
    grid3d *rsmp;

    if(sx == 1.0 && sy == 1.0 && sz == 1.0) {
        fl_show_alert2(0,_("No scaling requested!\fSet at least one scale factor greater than 1."));
        return;
    }

    if(fl_get_button(GUI->operations->btn_downsamp)) { /* downsample */
        rsmp = g3d_downsample(DATA->g3d,mode,sx,sy,sz,fill,norm,doble);
        vx = DATA->hdr->vx * sx;
        vy = DATA->hdr->vy * sy;
        vz = DATA->hdr->vz * sz;
    } else { /* upsample */
        rsmp = g3d_upsample(DATA->g3d,mode,sx,sy,sz,fill,norm,doble);
        vx = DATA->hdr->vx / sx;
        vy = DATA->hdr->vy / sy;
        vz = DATA->hdr->vz / sz;
    }
    if(rsmp == NULL) {
        fl_show_alert2(0,_("Resampling failed!\fPlease try again."));
        return;
    }

    _backup_data(GUI->operations,DATA);
    g3d_free(DATA->g3d); DATA->g3d = rsmp;
    DATA->hdr->type = doble ? RAW_FLOAT64 : RAW_FLOAT32;
    DATA->hdr->vx = vx; DATA->hdr->vy = vy; DATA->hdr->vz = vz;
    _reset_from_data(ENV,true);
}

void cb_op_undo ( FL_OBJECT * obj  , long data  ) {
    xg3d_data *bkpdata = BACKUP_DATA(GUI->operations);

    switch(data) {
        case 0: /* Backup data toggle */
            if(fl_get_button(obj)) {
                activate_obj(GUI->operations->btn_undo,FL_INDIANRED);
            } else {
                xg3d_data_clear(bkpdata,false);
                deactivate_obj(GUI->operations->btn_undo);
            }
            break;
        case 1: /* Undo button */
            if(bkpdata->g3d != NULL) { /* backup exists */
                g3d_free(DATA->g3d);
                DATA->g3d = bkpdata->g3d; bkpdata->g3d = NULL;
                xg3d_header_free(DATA->hdr);
                DATA->hdr = bkpdata->hdr; bkpdata->hdr = NULL;
                _reset_from_data(ENV,true);
            } else
                fl_show_alert2(0,_("Cannot undo!\fNo undo information available."));
            break;
    }
}

void cb_op_reflect ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const enum g3d_axis axis = THREEWAY_IF(fl_get_button(GUI->operations->btn_axis_x),AXIS_X,
                                           fl_get_button(GUI->operations->btn_axis_y),AXIS_Y,
                                                                                      AXIS_Z);
    g3d_ret ret;

    _backup_data(GUI->operations,DATA);
    fl_show_oneliner(_("Reflecting..."),fl_scrw/2,fl_scrh/2);
    if((ret = g3d_isom_reflect_exact(DATA->g3d,axis)) != G3D_OK)
        fl_show_alert2(0,"ERROR\f%s",g3d_ret_msg(ret));
    fl_hide_oneliner();
    _reset_from_data(ENV,false);
}

struct live_rot_data {
    const grid3d *base;
    g3d_aff_m *R;
    double fill;
};

static grid3d* _getslice_live_rot(const grid3d *x, const coord_t k, const enum g3d_slice_plane p) {
    const coord_t dim[3] = {x->nx,x->ny,x->nz};
    struct live_rot_data *ext;

    if(x == NULL) return NULL; // no input
    grid3d *y = g3d_alloc(G3D_FLOAT32,dim[PLANE_TO_J(p)],dim[PLANE_TO_I(p)],1,.alloc=true);
    if(y == NULL) return NULL; // malloc failed
    ext = x->ext;

    double *U = malloc(y->nvox * 4 * sizeof(double));
    /* anchors */
    double *U1 = &U[y->nvox];
    double *U2 = &U[2*y->nvox];
    double *U3 = &U[3*y->nvox];


    switch(p) {
                case PLANE_XY:
#ifdef _OPENMP
#pragma omp parallel for
#endif
            g3d_foreach(y,ix) {
                size_t ix_mod = ix % y->nx;
                size_t ix_div = ix / y->nx;
                U [ix] = ix_mod + 0.5; // x
                U1[ix] = ix_div + 0.5; // y
                U2[ix] = k + 0.5; // z
                U3[ix] = 1.0;
            }
            break;
               case PLANE_XZ:
#ifdef _OPENMP
#pragma omp parallel for
#endif
            g3d_foreach(y,ix) {
                size_t ix_mod = ix % y->nx;
                size_t ix_div = ix / y->nx;
                U [ix] = ix_mod + 0.5; // x
                U1[ix] = k + 0.5; // y
                U2[ix] = ix_div + 0.5; // z
                U3[ix] = 1.0;
            }
            break;
               case PLANE_ZY:
#ifdef _OPENMP
#pragma omp parallel for
#endif
            g3d_foreach(y,ix) {
                size_t ix_mod = ix % y->nx;
                size_t ix_div = ix / y->nx;
                U [ix] = k + 0.5; // x
                U1[ix] = ix_div + 0.5; // y
                U2[ix] = ix_mod + 0.5; // z
                U3[ix] = 1.0;
            }
            break;
               default:
            fprintf(stderr,"%s: invalid g3d_slice_plane value passed: %d",__func__,p);
            abort();
    }

    double *V = malloc(3*y->nvox*sizeof(double));
    /* anchors */
    double *V1 = &V[y->nvox];
    double *V2 = &V[2*y->nvox];

    cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,3,
                y->nvox,4,1.0,ext->R->m,4,U,y->nvox,0.0,V,y->nvox);
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for(slice_t ix=0;ix<y->nvox;ix++)
        g3d_set_dvalue(y,ix,trilinear_interp(ext->base,V[ix],V1[ix],V2[ix],ext->fill));
    free(U);
    free(V);

    return y;
}

#undef ENV
inline static void _update_rotation_preview(xg3d_env *ENV) {
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
    const xg3d_data *dat = ((xg3d_view_workspace*)VIEW_WORKSPACE(ov_viewports[PLANE_XY]))->data[LAYER_PREVIEW];
    const double alpha = fl_get_dial_value(GUI->operations->dial_rot_x),
                  beta = fl_get_dial_value(GUI->operations->dial_rot_y),
                 gamma = fl_get_dial_value(GUI->operations->dial_rot_z);
    struct live_rot_data *ext = dat->g3d->ext;

    /* assign rotation matrix to g3d live */
    ext->R = g3d_isom_get_rotation_matrix(alpha,beta,gamma,.center=true,dat->g3d);
    replace_ov_slices(ov_viewports,LAYER_PREVIEW);
    replace_ov_quantslices(ov_viewports,GUI->quantization,LAYER_PREVIEW);
    replace_ov_images(ov_viewports,GUI->quantization,LAYER_PREVIEW);
    refresh_ov_canvases(GUI->overview);
}
#define ENV _default_env

void cb_op_rot_dial ( FL_OBJECT * obj  , long data  ) {
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
    const xg3d_data *dat = ((xg3d_view_workspace*)VIEW_WORKSPACE(ov_viewports[PLANE_XY]))->data[LAYER_PREVIEW];
    const enum g3d_axis axis = data;
    double val = fl_get_dial_value(obj);
    struct live_rot_data *ext; 
    FL_OBJECT *inp_rot;

    switch(axis) {
        default: /* fall-through */
                case AXIS_X: inp_rot = GUI->operations->inp_rot_x; break;
               case AXIS_Y: inp_rot = GUI->operations->inp_rot_y; break;
               case AXIS_Z: inp_rot = GUI->operations->inp_rot_z; break;
           }
    val = val > 180.0 ? val - 360.0 : val;
    fl_set_input_f(inp_rot,"%.5g",val); 

    if(fl_get_button(GUI->operations->btn_rot_preview)) {
        ext = dat->g3d->ext;
        free(ext->R); ext->R = NULL;
        _update_rotation_preview(ENV);
    }
}

void cb_op_rot_inp ( FL_OBJECT * obj  , long data  ) {
    const char *tval = fl_get_input(obj);
    const enum g3d_axis axis = data;
    FL_OBJECT *dial_rot;
    double val;

    switch(axis) {
        default: /* fall-through */
                case AXIS_X: dial_rot = GUI->operations->dial_rot_x; break;
               case AXIS_Y: dial_rot = GUI->operations->dial_rot_y; break;
               case AXIS_Z: dial_rot = GUI->operations->dial_rot_z; break;
           }

    if(tval == NULL || strlen(tval) == 0) {
        val = fl_get_dial_value(dial_rot);
        val = val > 180.0 ? val - 360.0 : val;
        fl_set_input_f(obj,"%.5g",val);
        return;
    } else {
        val = strtod(tval,NULL);
        val = fmod(val,360.0);
        if(val < 0) val += 360.0;
        fl_set_dial_value(dial_rot,val);
        fl_call_object_callback(dial_rot);
    }
}

void cb_op_rot_reset ( FL_OBJECT * obj  , long data  ) {
    (void)data;
        fl_set_dial_value(GUI->operations->dial_rot_x,0.0);
    fl_set_input(GUI->operations->inp_rot_x,"0");
       fl_set_dial_value(GUI->operations->dial_rot_y,0.0);
    fl_set_input(GUI->operations->inp_rot_y,"0");
       fl_set_dial_value(GUI->operations->dial_rot_z,0.0);
    fl_set_input(GUI->operations->inp_rot_z,"0");
       if(fl_get_button(GUI->operations->btn_rot_preview))
        _update_rotation_preview(ENV);
}

/* TODO: hide selection in overview when previewing rotation */
void cb_op_rot_preview ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
    const double fill = strtod_checked(fl_get_input(GUI->operations->inp_fillval),0.0);
    static xg3d_data dat = {NULL,NULL,NULL,NULL,0,NULL};
    static xg3d_header hdr;
    struct live_rot_data *ext;

    if(fl_get_button(obj)) { /* setup rotation preview */
        if(fl_get_button(GUI->overlay->btn_show)) {
            fl_show_alert2(0,_("Cannot preview rotation!\fPlease disable overlay first."));
            fl_set_button(obj,0);
            return;
        }
        dat.g3d = g3d_alloc(G3D_LIVE,DATA->g3d->nx,DATA->g3d->ny,DATA->g3d->nz,.alloc=false);
        dat.g3d->getslice = _getslice_live_rot;
        dat.g3d->ppref = PROC_PREFER_SLICE;
        hdr.dim[0] = DATA->g3d->nx;
        hdr.dim[1] = DATA->g3d->nx;
        hdr.dim[2] = DATA->g3d->nx;
        dat.hdr = &hdr;
        ext = calloc(1,sizeof(struct live_rot_data));
        ext->base = DATA->g3d;
        ext->fill = fill;
        dat.g3d->ext = ext;
        VIEW_DATA_LAYER(ov_viewports[PLANE_XY],LAYER_PREVIEW) = &dat;
        VIEW_DATA_LAYER(ov_viewports[PLANE_XZ],LAYER_PREVIEW) = &dat;
        VIEW_DATA_LAYER(ov_viewports[PLANE_ZY],LAYER_PREVIEW) = &dat;
        VIEW_K_LAYER(ov_viewports[PLANE_XY],LAYER_PREVIEW) = VIEW_K(ov_viewports[PLANE_XY]);
        VIEW_K_LAYER(ov_viewports[PLANE_XZ],LAYER_PREVIEW) = VIEW_K(ov_viewports[PLANE_XZ]);
        VIEW_K_LAYER(ov_viewports[PLANE_ZY],LAYER_PREVIEW) = VIEW_K(ov_viewports[PLANE_ZY]);
        QUANTIZATION_LAYER(GUI->quantization) = LAYER_PREVIEW;
        fl_set_form_title_f(GUI->overview->overview,XG3D_TITLE_FORMAT,_("Overview [Rotation preview]"));
        _update_rotation_preview(ENV);
    } else { /* reset overview canvases */
        ext = dat.g3d->ext;
        free(ext->R); ext->R = NULL;
        g3d_free(dat.g3d); dat.g3d = NULL;
        VIEW_DATA_LAYER(ov_viewports[PLANE_XY],LAYER_PREVIEW) = NULL;
        VIEW_DATA_LAYER(ov_viewports[PLANE_XZ],LAYER_PREVIEW) = NULL;
        VIEW_DATA_LAYER(ov_viewports[PLANE_ZY],LAYER_PREVIEW) = NULL;
        QUANTIZATION_LAYER(GUI->quantization) = LAYER_BASE;
        fl_set_form_title_f(GUI->overview->overview,XG3D_TITLE_FORMAT,_("Overview"));
        replace_ov_slices(ov_viewports,LAYER_BASE);
        replace_ov_quantslices(ov_viewports,GUI->quantization,LAYER_BASE);
        replace_ov_images(ov_viewports,GUI->quantization,LAYER_BASE);
        refresh_ov_canvases(GUI->overview);
    }
}

void cb_op_rotate_exact ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const enum g3d_axis axis = THREEWAY_IF(fl_get_button(GUI->operations->btn_axis_x),AXIS_X,
                                           fl_get_button(GUI->operations->btn_axis_y),AXIS_Y,
                                                                                      AXIS_Z);
    unsigned int itheta;
    g3d_ret ret;

    _backup_data(GUI->operations,DATA);
    fl_show_oneliner(_("Rotating..."),fl_scrw/2,fl_scrh/2);
    itheta = fl_get_menu(obj) * 90;
    if((ret = g3d_isom_rotate_exact(DATA->g3d,axis,itheta)) != G3D_OK) {
        fl_hide_oneliner();
        fl_show_alert2(0,"ERROR\f%s",g3d_ret_msg(ret));
        return;
    }
    fl_hide_oneliner();
    if(fl_get_button(GUI->operations->btn_rot_preview)) {
        fl_set_button(GUI->operations->btn_rot_preview,0);
        fl_trigger_object(GUI->operations->btn_rot_preview);
    }
    _reset_from_data(ENV,true);
}

void cb_op_rotate ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const double fill = strtod_checked(fl_get_input(GUI->operations->inp_fillval),0.0);
    const double alpha = fl_get_dial_value(GUI->operations->dial_rot_x),
                  beta = fl_get_dial_value(GUI->operations->dial_rot_y),
                 gamma = fl_get_dial_value(GUI->operations->dial_rot_z);
    const bool doble = fl_get_button(GUI->operations->btn_double);
    grid3d *res;

    _backup_data(GUI->operations,DATA);
    fl_show_oneliner(_("Rotating..."),fl_scrw/2,fl_scrh/2);
    if((res = g3d_isom_rotate(DATA->g3d,alpha,beta,gamma,fill,doble)) == NULL) {
        fl_hide_oneliner();
        return;
    }
    free(DATA->g3d);
    DATA->g3d = res;
    fl_hide_oneliner();
    if(fl_get_button(GUI->operations->btn_rot_preview)) {
        fl_set_button(GUI->operations->btn_rot_preview,0);
        fl_trigger_object(GUI->operations->btn_rot_preview);
    }
    _reset_from_data(ENV,true);
}

static double _calibrate ( double x  , const void * p  ) {
    const double *calib = (double*)p;
    return calib[0] + calib[1] * x;
}

void cb_op_calib ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const double calib_data[2] = {DATA->hdr->cal_inter,
                                  DATA->hdr->cal_slope};

    if(calib_data[0] == 0.0 && calib_data[1] == 1.0) return;

    _backup_data(GUI->operations,DATA);
    g3d_ret ret = g3d_dapply(DATA->g3d,_calibrate,calib_data);
    if(ret != G3D_OK) g3d_ret_error(ret,"g3d_dapply");
    DATA->hdr->cal_slope = 1.0;
    DATA->hdr->cal_inter = 0.0;
    _reset_from_data(ENV,false);
}

void cb_op_filter_type ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    FL_OBJECT *slider;

    switch(fl_get_choice(obj)) {
        /* spatial filters */
        case 2: /* Box */
            activate_obj(GUI->operations->ch_filter_size,FL_BLACK);
            slider = GUI->operations->sli_filter_par1;
            fl_set_object_label(slider,""); deactivate_obj(slider);
            slider = GUI->operations->sli_filter_par2;
            fl_set_object_label(slider,""); deactivate_obj(slider);
            break;
        case 3: /* Gaussian */
            activate_obj(GUI->operations->ch_filter_size,FL_BLACK);
            slider = GUI->operations->sli_filter_par1;
            fl_set_object_label(slider,"nσ");
            fl_set_slider_precision(slider,1);
            fl_set_slider_step(slider,0.1);
            fl_set_slider_increment(slider,0.1,0.1);
            fl_set_slider_bounds(slider,0.1,6.0);
            fl_set_slider_value(slider,3.0);
            activate_obj(slider,FL_YELLOW);
            slider = GUI->operations->sli_filter_par2;
            fl_set_object_label(slider,""); deactivate_obj(slider);
            break;
        case 4: /* Bilateral Gaussian */
            activate_obj(GUI->operations->ch_filter_size,FL_BLACK);
            slider = GUI->operations->sli_filter_par1;
            fl_set_object_label(slider,"nσ");
            fl_set_slider_precision(slider,1);
            fl_set_slider_step(slider,0.1);
            fl_set_slider_increment(slider,0.1,0.1);
            fl_set_slider_bounds(slider,0.1,6.0);
            fl_set_slider_value(slider,3.0);
            activate_obj(slider,FL_YELLOW);
            slider = GUI->operations->sli_filter_par2;
            fl_set_object_label(slider,"σᵥ");
            fl_set_slider_precision(slider,0);
            fl_set_slider_step(slider,1);
            fl_set_slider_increment(slider,1,1);
            fl_set_slider_bounds(slider,1,300);
            fl_set_slider_value(slider,3.0);
            activate_obj(slider,FL_YELLOW);
            break;
        case 5: /* Median */
            activate_obj(GUI->operations->ch_filter_size,FL_BLACK);
            slider = GUI->operations->sli_filter_par1;
            fl_set_object_label(slider,""); deactivate_obj(slider);
            slider = GUI->operations->sli_filter_par2;
            fl_set_object_label(slider,""); deactivate_obj(slider);
            break;
        /* frequency filters */
        case 7: /* Butterworth (low-pass) */
        case 8: /* Butterworth (high-pass) */
            deactivate_obj(GUI->operations->ch_filter_size);
            slider = GUI->operations->sli_filter_par1;
            fl_set_object_label(slider,"n");
            fl_set_slider_precision(slider,0);
            fl_set_slider_step(slider,1);
            fl_set_slider_increment(slider,1,1);
            fl_set_slider_bounds(slider,1,99);
            fl_set_slider_value(slider,5);
            activate_obj(slider,FL_YELLOW);
            slider = GUI->operations->sli_filter_par2;
            fl_set_object_label(slider,"ƒ₁");
            fl_set_slider_precision(slider,2);
            fl_set_slider_step(slider,0.01);
            fl_set_slider_increment(slider,0.01,0.01);
            fl_set_slider_bounds(slider,0.00,1.00);
            fl_set_slider_value(slider,0.5);
            activate_obj(slider,FL_YELLOW);
            break;
        case 9: /* Gaussian (low-pass) */
        case 10: /* Gaussian (high-pass) */
            deactivate_obj(GUI->operations->ch_filter_size);
            slider = GUI->operations->sli_filter_par1;
            fl_set_object_label(slider,""); deactivate_obj(slider);
            slider = GUI->operations->sli_filter_par2;
            fl_set_object_label(slider,"ƒ₁");
            fl_set_slider_precision(slider,2);
            fl_set_slider_step(slider,0.01);
            fl_set_slider_increment(slider,0.01,0.01);
            fl_set_slider_bounds(slider,0.00,1.00);
            fl_set_slider_value(slider,0.5);
            activate_obj(slider,FL_YELLOW);
            break;
    }
}

void cb_op_filter ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const unsigned int size = strtoul(fl_get_choice_text(GUI->operations->ch_filter_size),NULL,10);
    const double fill = strtod_checked(fl_get_input(GUI->operations->inp_fillval),0.0);
    const enum xg3d_scope scope = fl_get_button(GUI->operations->btn_scope_global);
    const double par1 = fl_get_slider_value(GUI->operations->sli_filter_par1);
    const double par2 = fl_get_slider_value(GUI->operations->sli_filter_par2);
    const int choice = fl_get_choice(GUI->operations->ch_filter_type);
    const bool doble = fl_get_button(GUI->operations->btn_double);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const grid3d *slice = VIEW_SLICE(GUI->viewport);
    const coord_t mvk = VIEW_K(GUI->viewport);
    const unsigned int b = (size-1)/2;
    const unsigned int ss = size*size,
                       sss = size*size*size;
    grid3d *ker,*flt;
    int cj,ci,ck;
    g3d_ret (*_replace_slice)(grid3d*,const grid3d*,const coord_t,const enum g3d_slice_plane)
        = doble ? g3d_replace_dslice : g3d_replace_fslice;

    fl_show_oneliner(_("Filtering..."),fl_scrw/2,fl_scrh/2);

    _backup_data(GUI->operations,DATA);
    switch(choice) {
        /* spatial filters */
        case 2: /* Box */
            switch(scope) {
                case SCOPE_SLICE:
                {
                    double *tmp = filter_spatial_box(2,size,true);
                    ker = g3d_alloc_dfill(G3D_FLOAT64,size,size,1,.data=tmp); free(tmp);
                    flt = g3d_convolve_fftw(slice,ker,fill,doble); g3d_free(ker);
                    g3d_ret ret = _replace_slice(DATA->g3d,flt,mvk,plane);
                    if(ret != G3D_OK) g3d_ret_error(ret,"_replace_slice");
                    g3d_free(flt);
                }
                    break;
                case SCOPE_GLOBAL:
                {
                    double *tmp = filter_spatial_box(3,size,true);
                    ker = g3d_alloc_dfill(G3D_FLOAT64,size,size,size,.data=tmp); free(tmp);
                    flt = g3d_convolve_fftw(DATA->g3d,ker,fill,doble); g3d_free(ker);
                    g3d_free(DATA->g3d);
                    DATA->g3d = flt;
                }
                    break;
            }
            break;
        case 3: /* Gaussian */
            switch(scope) {
                case SCOPE_SLICE:
                {
                    double *tmp = filter_spatial_gaussian2(size,par1,true);
                    ker = g3d_alloc_dfill(G3D_FLOAT64,size,size,1,.data=tmp); free(tmp);
                    flt = g3d_convolve_fftw(slice,ker,fill,doble); g3d_free(ker);
                    g3d_ret ret = _replace_slice(DATA->g3d,flt,mvk,plane);
                    if(ret != G3D_OK) g3d_ret_error(ret,"_replace_slice");
                    g3d_free(flt);
                }
                    break;
                case SCOPE_GLOBAL:
                {
                    double *tmp = filter_spatial_gaussian3(size,par1,true);
                    ker = g3d_alloc_dfill(G3D_FLOAT64,size,size,size,.data=tmp); free(tmp);
                    flt = g3d_convolve_fftw(DATA->g3d,ker,fill,doble); g3d_free(ker);
                    g3d_free(DATA->g3d);
                    DATA->g3d = flt;
                }
                    break;
            }
            break;
        case 4: /* Bilateral Gaussian */
            switch(scope) {
                case SCOPE_SLICE:
                {
                    double *tmp = filter_spatial_gaussian2(size,par1,false);
                    ker = g3d_alloc_dfill(G3D_FLOAT64,size,size,1,.data=tmp); free(tmp);
                    flt = g3d_alloc(slice->type,slice->nx,slice->ny,slice->nz,.alloc=true);
#ifdef _OPENMP
#pragma omp parallel
#endif
                    {
                        double *window = malloc(ss*sizeof(double));
#ifdef _OPENMP
#pragma omp for
#endif
                        for(index_t ix=0;ix<flt->nvox;ix++) {
                            double sum = 0.0;
                            double val = g3d_get_dvalue(slice,ix);
                            for(unsigned int ik=0;ik<ss;ik++) {
                                int j = ix%flt->nx + (int)(ik%size - b);
                                int i = ix/flt->nx + (int)(ik/size - b);
                                double valk = (j >= 0 && j < (int)flt->nx && i >= 0 && i < (int)flt->ny)
                                    ? g3d_get_dvalue(slice,j + i*flt->nx) : fill;
                                window[ik] = ((double*)ker->dat)[ik] * gsl_ran_gaussian_pdf(fabs(val-valk),par2);
                                sum += window[ik]; window[ik] *= valk;
                            }
                            for(unsigned int ik=0;ik<ss;ik++)
                                g3d_set_dvalue(flt,ix,g3d_get_dvalue(flt,ix) + window[ik] / sum);
                        }
                        free(window);
                    }

                    g3d_free(ker);
                    g3d_ret ret = _replace_slice(DATA->g3d,flt,mvk,plane);
                    if(ret != G3D_OK) g3d_ret_error(ret,"_replace_slice");
                    g3d_free(flt);
                }
                    break;
                case SCOPE_GLOBAL:
                {
                    double *tmp = filter_spatial_gaussian3(size,par1,true);
                    ker = g3d_alloc_dfill(G3D_FLOAT64,size,size,size,.data=tmp); free(tmp);
                    flt = g3d_alloc(DATA->g3d->type,DATA->g3d->nx,DATA->g3d->ny,DATA->g3d->nz,.alloc=true);
#ifdef _OPENMP
#pragma omp parallel
#endif
                    {
                        double *window = malloc(sss*sizeof(double));
#ifdef _OPENMP
#pragma omp for
#endif
                        for(index_t ix=0;ix<flt->nvox;ix++) {
                            double sum = 0.0;
                            double val = g3d_get_dvalue(DATA->g3d,ix);
                            for(unsigned int ik=0;ik<sss;ik++) {
                                int j = (ix%flt->nxy)%flt->nx + (int)((ik%ss)%size - b);
                                int i = (ix%flt->nxy)/flt->nx + (int)((ik%ss)/size - b);
                                int k = ix/flt->nxy + (int)(ik/ss - b);
                                double valk = (j >= 0 && j < (int)flt->nx
                                        && i >= 0 && i < (int)flt->ny && k >= 0 && k < (int)flt->nz)
                                    ? g3d_get_dvalue(DATA->g3d,j + i*flt->nx + k*flt->nxy) : fill;
                                window[ik] = ((double*)ker->dat)[ik] * gsl_ran_gaussian_pdf(fabs(val-valk),par2);
                                sum += window[ik]; window[ik] *= valk;
                            }
                            for(unsigned int ik=0;ik<sss;ik++)
                                g3d_set_dvalue(flt,ix,g3d_get_dvalue(flt,ix) + window[ik] / sum);
                        }
                        free(window);
                    }

                    g3d_free(ker);
                    g3d_free(DATA->g3d);
                    DATA->g3d = flt;
                }
                    break;
            }
            break;
        case 5: /* Median */
            switch(scope) {
                case SCOPE_SLICE:
                    flt = g3d_alloc(slice->type,slice->nx,slice->ny,slice->nz,.alloc=true);
#ifdef _OPENMP
#pragma omp parallel
#endif
                    {
                        double *window = malloc(ss*sizeof(double));
#ifdef _OPENMP
#pragma omp for
#endif
                        for(index_t ix=0;ix<flt->nvox;ix++) {
                            for(unsigned int ik=0;ik<ss;ik++) {
                                int j = ix%flt->nx + (int)(ik%size - b);
                                int i = ix/flt->nx + (int)(ik/size - b);
                                window[ik] = (j >= 0 && j < (int)flt->nx && i >= 0 && i < (int)flt->ny)
                                    ? g3d_get_dvalue(slice,j + i*flt->nx) : fill;
                            }
                            gsl_sort(window,1,ss);
                            g3d_set_dvalue(flt,ix,window[ss/2]);
                        }
                        free(window);
                    }

                    _replace_slice(DATA->g3d,flt,mvk,plane);
                    g3d_free(flt);
                    break;
                case SCOPE_GLOBAL:
                    flt = g3d_alloc(DATA->g3d->type,DATA->g3d->nx,DATA->g3d->ny,DATA->g3d->nz,.alloc=true);
#ifdef _OPENMP
#pragma omp parallel
                    {
#endif
                        double *window = malloc(sss*sizeof(double));
#ifdef _OPENMP
#pragma omp for
#endif
                        for(index_t ix=0;ix<flt->nvox;ix++) {
                            for(unsigned int ik=0;ik<sss;ik++) {
                                int j = (ix%flt->nxy)%flt->nx + (int)((ik%ss)%size - b);
                                int i = (ix%flt->nxy)/flt->nx + (int)((ik%ss)/size - b);
                                int k =  ix/flt->nxy + (int)(ik/ss - b);
                                window[ik] = (j >= 0 && j < (int)flt->nx && i >= 0 && i < (int)flt->ny && k >= 0 && k < (int)flt->nz)
                                    ? g3d_get_dvalue(DATA->g3d,j + i*flt->nx + k*flt->nxy) : fill;
                            }
                            gsl_sort(window,1,sss);
                            g3d_set_dvalue(flt,ix,window[sss/2]);
                        }
                        free(window);
#ifdef _OPENMP
                    }
#endif
                    g3d_free(DATA->g3d);
                    DATA->g3d = flt;
                    break;
            }
            break;
        /* frequency filters */
        case 7: /* Butterworth (low-pass) */
        case 8: /* Butterworth (high-pass) */
            switch(scope) {
                case SCOPE_SLICE:
                    ker = g3d_alloc(G3D_FLOAT32,slice->nx,slice->ny,slice->nz,.alloc=true);
                    cj = ker->nx / 2; ci = ker->ny / 2;
#ifdef _OPENMP
#pragma omp parallel
#endif
                    for(index_t ix=0;ix<ker->nvox;ix++) {
                        int j = (ix%ker->nxy)%ker->nx;
                        int i = (ix%ker->nxy)/ker->nx;
                        double val = SQR((double)(j-cj)/cj)
                                   + SQR((double)(i-ci)/ci);
                        g3d_set_dvalue(ker,ix,filter_frequency_butterworth(val,par1,par2,choice == 7 ? FILTER_LOWPASS : FILTER_HIGHPASS));
                    }
                    flt = g3d_fftw_mask(slice,ker,doble); g3d_free(ker);
                    _replace_slice(DATA->g3d,flt,mvk,plane);
                    g3d_free(flt);
                    break;
                case SCOPE_GLOBAL:
                    ker = g3d_alloc(G3D_FLOAT32,DATA->g3d->nx,DATA->g3d->ny,DATA->g3d->nz,.alloc=true);
                    cj = ker->nx / 2; ci = ker->ny / 2; ck = ker->nz / 2;
#ifdef _OPENMP
#pragma omp parallel
#endif
                    for(index_t ix=0;ix<ker->nvox;ix++) {
                        int j = (ix%ker->nxy)%ker->nx;
                        int i = (ix%ker->nxy)/ker->nx;
                        int k =  ix/ker->nxy;
                        double val = SQR((double)(j-cj)/cj)
                                   + SQR((double)(i-ci)/ci)
                                   + SQR((double)(k-ck)/ck);
                        g3d_set_dvalue(ker,ix,filter_frequency_butterworth(val,par1,par2,choice == 7 ? FILTER_LOWPASS : FILTER_HIGHPASS));
                    }
                    flt = g3d_fftw_mask(DATA->g3d,ker,doble); g3d_free(ker);
                    g3d_free(DATA->g3d);
                    DATA->g3d = flt;
                    break;
            }
            break;
        case 9: /* Gaussian (low-pass) */
        case 10: /* Gaussian (high-pass) */
            switch(scope) {
                case SCOPE_SLICE:
                    ker = g3d_alloc(G3D_FLOAT32,slice->nx,slice->ny,slice->nz,.alloc=true);
                    cj = ker->nx / 2; ci = ker->ny / 2;
#ifdef _OPENMP
#pragma omp parallel for
#endif
                    for(index_t ix=0;ix<ker->nvox;ix++) {
                        int j = (ix%ker->nxy)%ker->nx;
                        int i = (ix%ker->nxy)/ker->nx;
                        double val = SQR((double)(j-cj)/cj)
                                   + SQR((double)(i-ci)/ci);
                        g3d_set_dvalue(ker,ix,filter_frequency_gaussian(val,par2,choice == 9 ? FILTER_LOWPASS : FILTER_HIGHPASS));
                    }
                    flt = g3d_fftw_mask(slice,ker,doble); g3d_free(ker);
                    _replace_slice(DATA->g3d,flt,mvk,plane);
                    g3d_free(flt);
                    break;
                case SCOPE_GLOBAL:
                    ker = g3d_alloc(G3D_FLOAT32,DATA->g3d->nx,DATA->g3d->ny,DATA->g3d->nz,.alloc=true);
                    cj = ker->nx / 2; ci = ker->ny / 2; ck = ker->nz / 2;
#ifdef _OPENMP
#pragma omp parallel
#endif
                    for(index_t ix=0;ix<ker->nvox;ix++) {
                        int j = (ix%ker->nxy)%ker->nx;
                        int i = (ix%ker->nxy)/ker->nx;
                        int k =  ix/ker->nxy;
                        double val = SQR((double)(j-cj)/cj)
                                   + SQR((double)(i-ci)/ci)
                                   + SQR((double)(k-ck)/ck);
                        g3d_set_dvalue(ker,ix,filter_frequency_gaussian(val,par2,choice == 9 ? FILTER_LOWPASS : FILTER_HIGHPASS));
                    }
                    flt = g3d_fftw_mask(DATA->g3d,ker,doble); g3d_free(ker);
                    g3d_free(DATA->g3d);
                    DATA->g3d = flt;
                    break;
            }
            break;
    }

    fl_hide_oneliner();
    _reset_from_data(ENV,false);
}

static double _clamp ( double x  , const void * p  ) {
    const double *quant = (double*)p;
    return FL_clamp(x,quant[0],quant[1]);
}

void cb_op_clamp ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);
    const enum xg3d_scope glob_scope = fl_get_button(GUI->quantization->btn_scope);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const coord_t k = VIEW_K(GUI->viewport);
    const double quant[2] = {qwk->min,
                             qwk->max};
    enum xg3d_scope scope;
    grid3d *dat;

    if(QUANTIZATION_MODE(GUI->quantization) != QUANT_INTERVALS) {
        fl_show_alert2(0,_("Cannot perform clamping!\fPlease set intervals quantization mode first."));
        return;
    }

    if(fl_get_button(GUI->operations->btn_scope_global)) {
        if(glob_scope == SCOPE_GLOBAL) {
            scope = SCOPE_GLOBAL;
            dat = DATA->g3d;
        } else {
            fl_show_alert2(0,_("WARNING!\fPlease set global quantization.")); return; }
    } else {
        if(glob_scope == SCOPE_SLICE) {
            scope = SCOPE_SLICE;
            dat = VIEW_SLICE(GUI->viewport);
        } else {
            fl_show_alert2(0,_("WARNING!\fPlease set slice quantization.")); return; }
    }

    _backup_data(GUI->operations,DATA);
    g3d_ret ret = g3d_dapply(dat,_clamp,quant);
    if(ret != G3D_OK) g3d_ret_error(ret,"g3d_dapply");

    if(scope == SCOPE_SLICE)
        g3d_replace_slice(DATA->g3d,dat,k,plane);
    _reset_from_data(ENV,false);
}

void cb_op_swap ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    _backup_data(GUI->operations,DATA);
    fl_show_oneliner(_("Swapping bytes..."),fl_scrw/2,fl_scrh/2);
    g3d_ret ret = g3d_byteswap(DATA->g3d);
    if(ret != G3D_OK) g3d_ret_error(ret,"g3d_byteswap");
    fl_hide_oneliner();
    DATA->hdr->endian = DATA->hdr->endian == ENDIAN_BIG ? ENDIAN_LITTLE : ENDIAN_BIG;
    _reset_from_data(ENV,false);
    refresh_datasets_info(GUI->datasets,ENV->data,true);
}

void cb_op_roi_mask ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    if(DATA->nrois == 0) return;
    const g3d_roi *roi = DATA->rois[fl_get_choice(GUI->operations->ch_roi)-1]->roi;
    const double fill = strtod_checked(fl_get_input(GUI->operations->inp_fillval),0.0);

    _backup_data(GUI->operations,DATA);
    fl_show_oneliner(_("Masking ROI..."),fl_scrw/2,fl_scrh/2);
    g3d_roi_mask_dfill(DATA->g3d,roi,fill);
    fl_hide_oneliner();
    _reset_from_data(ENV,false);
}

void cb_op_roi_copynearest ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    if(DATA->nrois == 0) return;
    const g3d_roi *src = DATA->rois[fl_get_choice(GUI->operations->ch_roi)-1]->roi;

    _backup_data(GUI->operations,DATA);
    fl_show_oneliner(_("Copying nearest ROI value..."),fl_scrw/2,fl_scrh/2);
    g3d_roi *dst = (SELECTION_NVOX(GUI->selection) == 0) ? NULL
        : g3d_roi_new(SELECTION_G3D(GUI->selection),"_tmp",SELECTION_NVOX(GUI->selection));
    g3d_roi_copynearest(DATA->g3d,src,dst);
    g3d_roi_free(dst);
    fl_hide_oneliner();
    _reset_from_data(ENV,false);
}

void cb_op_coordinates ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const double fill = strtod_checked(fl_get_input(GUI->operations->inp_fillval),0.0);
    const bool doble = fl_get_button(GUI->operations->btn_double);
    grid3d *transf;
    g3d_ret ret;

    switch(fl_get_menu(obj)) {
        case 1: /* Cartesian -> polar */
            if((ret = g3d_coord_carpol(DATA->g3d,fill,doble,&transf)) != G3D_OK) {
                fl_show_alert2(0,"Coordinate transformation failed!\fERROR: %s",g3d_ret_msg(ret));
                return;
            }
            break;
        case 2: /* TODO: Polar -> cartesian */
            if((ret = g3d_coord_polcar(DATA->g3d,fill,doble,&transf)) != G3D_OK) {
                fl_show_alert2(0,"Coordinate transformation failed!\fERROR: %s",g3d_ret_msg(ret));
                return;
            }
            break;
    }
    _backup_data(GUI->operations,DATA);
    g3d_free(DATA->g3d); DATA->g3d = transf;
    DATA->hdr->type = doble ? RAW_FLOAT64 : RAW_FLOAT32;
    DATA->hdr->dim[0] = DATA->g3d->nx;
    DATA->hdr->dim[1] = DATA->g3d->ny;
    DATA->hdr->dim[2] = DATA->g3d->nz;
    _reset_from_data(ENV,true);
}

void cb_op_align_mk ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const enum g3d_axis axis = THREEWAY_IF(fl_get_button(GUI->operations->btn_axis_x),AXIS_X,
                                           fl_get_button(GUI->operations->btn_axis_y),AXIS_Y,
                                                                                      AXIS_Z);
    const double fill = strtod_checked(fl_get_input(GUI->operations->inp_fillval),0.0);
    const bool doble = fl_get_button(GUI->operations->btn_double);
    const marker *firstmk = SELECTION_MKLIST(GUI->selection);
    const unsigned int nmk = SELECTION_NMK(GUI->selection);
    enum align_mk_type align_type;
    unsigned int minp;
    g3d_aff_m *Rx;
    grid3d *res;

    switch(fl_get_menu(obj)) {
        default:
        case 1: minp = 2; break;
        case 2: minp = 3; break;
    }

    if(nmk < minp) {
        fl_show_alert2(0,_("Less than %d markers!\fPlease set at least %d markers."),minp,minp);
        return;
    }

    switch(fl_get_menu(obj)) {
        default:
        case 1: align_type = ALIGN_MK_LINE;  break;
        case 2: align_type = ALIGN_MK_PLANE; break;
    }

    Rx = op_align_to_markers(DATA->g3d,firstmk,nmk,axis,align_type);
    fl_show_oneliner(_("Rotating..."),fl_scrw/2,fl_scrh/2);
    res = g3d_isom_rotate_matrix(DATA->g3d,Rx,fill,doble);
    fl_hide_oneliner();
    free(Rx);
    if(res == NULL) return;
    _backup_data(GUI->operations,DATA);
    free(DATA->g3d); DATA->g3d = res;
    fl_hide_oneliner();
    fl_set_button(GUI->operations->btn_rot_preview,0);
    _reset_from_data(ENV,true);
}

void cb_op_repair ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const enum xg3d_scope scope = fl_get_button(GUI->operations->btn_scope_global);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const grid3d *selslice = VIEW_SELSLICE(GUI->viewport);
    coord_t kmin,kmax,incr;
    uint8_t m=0,p=0;

    if((scope == SCOPE_GLOBAL && SELECTION_NVOX(GUI->selection) == 0) ||
            (scope == SCOPE_SLICE && VIEW_SELNVOX(GUI->viewport) == 0)) {
        fl_show_alert2(0,"No selection!\fPlease make a selection first.");
        return;
    }

    double zero,maxk; fl_get_slider_bounds(GUI->mainview->slider,&zero,&maxk); 

    if(fl_get_button(GUI->operations->btn_line_x)) {
        m = Im1; p = Ip1; incr = selslice->nx;
    } else {
        m = Jm1; p = Jp1; incr = 1;
    }

    if(scope == SCOPE_GLOBAL) {
        kmin = 0; kmax = (coord_t)maxk;
    } else {
        kmin = kmax = VIEW_K(GUI->viewport);
    }

    _backup_data(GUI->operations,DATA);
    for(coord_t k=kmin;k<=kmax;k++) {
        grid3d *slice = g3d_slice(DATA->g3d,k,plane);
        for(slice_t ix=0;ix<selslice->nvox;ix++)
            if(g3d_selbit_check(selslice,ix)) {
                double avg = 0.0;
                uint8_t n = 0;
                if(g3d_nhood_check(m,g3d_nhood_get(selslice,ix))) {
                    avg += g3d_get_dvalue(slice,ix-incr); n++;
                }
                if(g3d_nhood_check(p,g3d_nhood_get(selslice,ix))) {
                    avg += g3d_get_dvalue(slice,ix+incr); n++;
                }
                if(n > 0) g3d_set_dvalue(slice,ix,avg/(double)n);
            }
        g3d_replace_slice(DATA->g3d,slice,k,plane);
        g3d_free(slice);
    }
    _reset_from_data(ENV,false);
}

void cb_op_flatten ( FL_OBJECT * obj  , long data  ) {
    (void)obj;
(void)data;
    fl_show_alert2(0,"Unimplemented, sorry!");
}
