struct ALBIRA_ctheader { 
	uint16_t NProy; 
	uint16_t Pixels_X; 
	uint16_t Pixels_Z; 
	uint16_t Bits; 
	double detect_size; 
	double focal; 
	double R; 
	double u_0; 
	double v_0; 
	double twist; 
	double slant; 
	double tilt; 
	uint16_t current; 
	uint16_t voltage; 
};

struct ALBIRA_mtxheader {
    uint32_t resx; /* dimension x */
    uint32_t resy; /* dimension y */
    uint32_t fmt;  /* 70 is XYE, 72 is XYEU, 75 is SPECTbin */
};
