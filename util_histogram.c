#include "util_histogram.h"

#include <math.h>
#include <float.h>
#include <gsl/gsl_errno.h>

#include "grid3d_uint8.h"

/* compute histogram from grid3d */
g3d_ret histogram_fill(gsl_histogram *h, const grid3d *dat) {
    if(h == NULL || dat == NULL) return G3D_ENOINPUT;

    double *bins = h->bin;

#if defined(_OPENMP) && _OPENMP >= 201511
    size_t nbin = h->n;
#pragma omp parallel for reduction(+:bins[:nbin])
#endif
    for(index_t ix=0;ix<dat->nvox;ix++) {
        size_t ibin;
        if(gsl_histogram_find(h,g3d_get_dvalue(dat,ix),&ibin) == GSL_SUCCESS)
            bins[ibin] += 1.0;
    }

    return G3D_OK;
}

/* compute histogram from grid3d, where selected */
g3d_ret histogram_fill_sel(gsl_histogram *h, const grid3d *dat, const grid3d *sel) {
    if(h == NULL || dat == NULL || sel == NULL) return G3D_ENOINPUT;

    double *bins = h->bin;

#if defined(_OPENMP) && _OPENMP >= 201511
    size_t nbin = h->n;
#pragma omp parallel for reduction(+:bins[:nbin])
#endif
    for(index_t ix=0;ix<dat->nvox;ix++) {
        size_t ibin;
        if(g3d_selbit_check(sel,ix)
                && gsl_histogram_find(h,g3d_get_dvalue(dat,ix),&ibin) == GSL_SUCCESS)
            bins[ibin] += 1.0;
    }

    return G3D_OK;
}

/* compute histogram from grid3d, within ROI */
g3d_ret histogram_fill_roi(gsl_histogram *h, const grid3d *dat, const g3d_roi *roi) {
    if(h == NULL || dat == NULL) return G3D_ENOINPUT;

    double *bins = h->bin;

#if defined(_OPENMP) && _OPENMP >= 201511
    size_t nbin = h->n;
#pragma omp parallel for reduction(+:bins[:nbin])
#endif
    for(index_t ix=0;ix<roi->size;ix++) {
        size_t ibin;
        if(gsl_histogram_find(h,g3d_get_dvalue(dat,ROI_IX(ix,roi,dat)),&ibin) == GSL_SUCCESS)
            bins[ibin] += 1.0;
    }

    return G3D_OK;
}

/* threshold and binning stuff */

/* Compute Otsu's threshold */
size_t otsu_threshold(const gsl_histogram *hist) {
    size_t otsu = hist->n / 2;

    double total = 0.0;
    double mu_T = 0.0;
    for(size_t t=0;t<hist->n;t++) {
        total += hist->bin[t];
        mu_T += t * hist->bin[t];
    }
    mu_T /= total;

    double tmpvar = 0.0;
    double mu = 0.0;
    double w = 0.0;
    for(size_t t=0;t<hist->n;t++) {
        w += hist->bin[t] / total;
        if(w > 0.0 && w < 1.0) {
            mu += t * hist->bin[t] / total;
            double var_B = pow(fma(mu_T,w,-mu),2.0) / (w * (1.0 - w));
            if(var_B > tmpvar) {
                tmpvar = var_B;
                otsu = t;
            }
        }
    }

    return otsu;
}

/* Compute optimal number of uniform bins, following
 * Birgé & Rozenholc (DOI: 10.1051/ps:2006001) 
 * NOTE: data must be in [0,1] */
static double P_biroz(const size_t D) {
    return (double)D - 1.0 + pow(log((double)D),2.5);
}

static double L_biroz(const gsl_histogram *hist, const size_t N) {
    size_t D = hist->n;

    double L = 0.0;
    for(size_t n=0;n<D;n++)
        L += hist->bin[n] * log((double)D * hist->bin[n] / (double)N);

    return L;
}

static double J_biroz(const gsl_histogram *hist, const size_t N) {
    return L_biroz(hist,N) - P_biroz(hist->n);
}

size_t nbins_biroz(const double *data, const size_t N) {
    size_t nbins = 1;

    double Jtmp = -DBL_MAX;
    for(size_t D=1;D<=N/log(N);D++) {
        gsl_histogram *hist = gsl_histogram_alloc(D);
        gsl_histogram_set_ranges_uniform(hist,0.0,nextafter(1.0,HUGE_VAL));
        /* fill histogram */
        for(size_t i=0;i<N;i++)
            gsl_histogram_increment(hist,data[i]);
        /* compute objective function */
        double J = J_biroz(hist,N);
        gsl_histogram_free(hist);
        /* take maximum */
        if(J > Jtmp) {
            Jtmp = J;
            nbins = D;
        }
    }

    return nbins;
}
