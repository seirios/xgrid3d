#include "color_uniform.h"

#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_cblas.h>

#include "util_bezier.h"
#include "luoetal2006.h"
#include "ciecam02.h"

float* uniform_gradient_from_bezier(const double *points, unsigned int N, double min_Jp, double max_Jp, unsigned int num, float hue_spin) {
    /* colorspace parameters */
    static const double xyzw[3] = {95.047, 100.0, 108.883}; /* D65 whitepoint */
    static const double yb = 20.0; /* 20% gray */
    static const double la = 64.0 / M_PI * 5.0; /* 64 lux ambient illumination (incorrect!) */
    /* exact matrix specified in IEC 61966-2-1:1999 */
    static const float XYZ_to_sRGB_matrix[9] = {
        3.2406, -1.5372, -0.4986,
        -0.9689,  1.8758,  0.0415,
        0.0557, -0.2040,  1.0570,
    };

    struct CIECAM02vc vc;
    struct CIECAM02color color;

    /* get values from bézier curve */
    double *Jp,*ap,*bp;
    get_Jpapbp(num,points,N,min_Jp,max_Jp,&Jp,&ap,&bp);

    /* convert from CAM02UCS to JMh */
    double *J,*M,*h;
    Jpapbp_to_JMh(CAM02UCS_KL,CAM02UCS_c1,CAM02UCS_c2,Jp,ap,bp,num,&J,&M,&h);
    free(Jp); free(ap); free(bp);

    /* CIECAM02 to XYZ */
    float *XYZ = malloc(3*num*sizeof(float));
    CIECAM02_initVC(&vc,xyzw,yb,la,VC_AVERAGE_f,VC_AVERAGE_c,VC_AVERAGE_nc);
    double fl_qtrt = pow(vc.fl,0.25);
    for(unsigned int i=0;i<num;i++) {
        color.J = J[i];
        color.C = M[i] / fl_qtrt;
        double hue = h[i] / 360.0 + hue_spin;
        hue = hue - (int)hue;
        color.h = hue * 360.0;
        color = CIECAM02_inverse(color,vc);
        XYZ[3*i+0] = color.x / 100.0;
        XYZ[3*i+1] = color.y / 100.0;
        XYZ[3*i+2] = color.z / 100.0;
    }
    free(J); free(M); free(h);

    /* XYZ to linear RGB */
    float *srgb = malloc(3*num*sizeof(float));
    cblas_sgemm(CblasRowMajor,CblasNoTrans,CblasTrans,num,3,3,1.0,XYZ,3,XYZ_to_sRGB_matrix,3,0.0,srgb,3);
    free(XYZ);

    return srgb;
}
