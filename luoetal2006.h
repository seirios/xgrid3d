#define CAM02UCS_KL 1.00
#define CAM02UCS_c1 0.007
#define CAM02UCS_c2 0.0228

#define CAM02LCD_KL 0.77
#define CAM02LCD_c1 0.007
#define CAM02LCD_c2 0.0053

#define CAM02SCD_KL 1.24
#define CAM02SCD_c1 0.007
#define CAM02SCD_c2 0.0363

void JMh_to_Jpapbp(double,double,double,const double*,const double*,const double*,unsigned int,double**,double**,double**);

void Jpapbp_to_JMh(double,double,double,const double*,const double*,const double*,unsigned int,double**,double**,double**);
