#ifndef XG3D_mainview_cb_h_
#define XG3D_mainview_cb_h_

#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_visual.h"
#include "fd_viewport.h"
#include "fd_mainview.h"
#include "cb_selection.h"

#define MVC_MAX_ZOOM 10
#define MV_LEFT_MARGIN 30
#define MV_THICK 15
#define MV_SLIDER_THICK 20
#define MV_TOP_MARGIN 25 
#define MV_BOT_MARGIN 25
#define MV_STATUS_W 785
#define MV_TOOLBAR_H 14*MV_LEFT_MARGIN+MV_SLIDER_THICK+25

#define MV_MIN_WIDTH MV_STATUS_W
#define MV_MIN_HEIGHT MV_TOP_MARGIN+MV_SLIDER_THICK+MV_TOOLBAR_H+MV_BOT_MARGIN
#define MV_MAX_WIDTH (int)(1.0*fl_scrw)
#define MV_MAX_HEIGHT (int)(0.95*fl_scrh)

#define MVC_MAX_WIDTH (MV_MAX_WIDTH-(MV_LEFT_MARGIN+MV_SLIDER_THICK))
#define MVC_MAX_HEIGHT (MV_MAX_HEIGHT-(MV_TOP_MARGIN+2*MV_SLIDER_THICK+MV_BOT_MARGIN))

inline static enum g3d_slice_plane MAINVIEW_GET_PLANE(FD_mainview *mainview) {
    return THREEWAY_IF(
            fl_get_button(mainview->planeXY),PLANE_XY,
            fl_get_button(mainview->planeXZ),PLANE_XZ,
                                             PLANE_ZY);
}

inline static coord_t MAINVIEW_GET_K(FD_mainview *mainview, const enum g3d_slice_plane plane) {
    return THREEWAY_IF(
            plane==PLANE_XY,mainview->planeXY->u_ldata,
            plane==PLANE_XZ,mainview->planeXZ->u_ldata,
                            mainview->planeZY->u_ldata);
}

inline static void MAINVIEW_STORE_K(FD_mainview *mainview, const enum g3d_slice_plane plane, const coord_t k) {
    THREEWAY_IF(
            plane==PLANE_XY,mainview->planeXY->u_ldata=k,
            plane==PLANE_XZ,mainview->planeXZ->u_ldata=k,
                            mainview->planeZY->u_ldata=k);
}

inline static enum xg3d_orientation MAINVIEW_GET_ORIENT(FD_mainview *mainview, const enum g3d_slice_plane plane) {
    return ((mainview->bit_orient->u_ldata >> 3*plane) & 7L);
}

inline static void MAINVIEW_STORE_ORIENT(FD_mainview *mainview, const enum g3d_slice_plane plane, const enum xg3d_orientation orient) {
    mainview->bit_orient->u_ldata = (mainview->bit_orient->u_ldata & ~(7L << (3*plane))) | (orient << (3*plane));
}

#define MAINVIEW_GET_CALIB(fd_mainview) (fl_get_button(((FD_mainview*)(fd_mainview))->btn_units))

void resize_canvas(FD_mainview*,FD_viewport*);
void refresh_canvas(xg3d_env*);
void replace_slice(FD_viewport*,FD_selection*,const grid3d*,const coord_t);
void replace_quantslice(xg3d_env*);
void replace_img(FD_viewport*,FD_quantization*);
void draw_sel_voxel(FD_viewport*,FD_selection*,const coord_t,const coord_t);
void draw_meas_point(FD_viewport*,const coord_t,const coord_t,const FL_COLOR);
void restore_image_voxel(FD_viewport*,FD_selection*,const coord_t,const coord_t);

int cb_mainview_canvas(FL_OBJECT*,Window,int,int,XEvent*,void*);

#endif
