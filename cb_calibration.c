#include <math.h>
#include <errno.h>
#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "cb_viewport.h"
#include "fd_mainview.h"
#include "cb_calibration.h"
#include "cb_info.h"
#include "cb_selection.h"
#include "cb_quantization.h"
#include "cb_rois.h"

#include "util.h"
#include "util_nuclides.h"
#include "util_units.h"

/* update units when calibration factor changes */
#undef ENV
static void _update_units(xg3d_env *ENV) {
    if(GUI->viewport != NULL && GUI->selection != NULL)
        sel_update_info(GUI->viewport,GUI->selection);
    if(GUI->rois != NULL)
        update_roi_info(ENV);
    if(GUI->quantization != NULL)
        fl_call_object_callback(GUI->quantization->btn_units);
    if(GUI->mainview != NULL && GUI->viewport != NULL && GUI->measure != NULL)
        if(fl_get_button(GUI->mainview->measure) && VIEW_SPSET(GUI->viewport) && VIEW_EPSET(GUI->viewport))
            fl_call_object_callback(GUI->measure->btn_units);
}
#define ENV _default_env

/* ===========
   CALIBRATION
   =========== */

/* calibration input callback */
void cb_calib_input_factor ( FL_OBJECT * obj  , long data  ) {
	const char *txt = fl_get_input(obj);
	const double defval = data ? 0.0 /* intercept */ : 1.0 /* slope */;
    double factor;

    factor = strtod_checked(txt,defval);
    fl_set_input_f(obj,"%.16g",factor);

    if(data) DATA->hdr->cal_inter = factor; /* intercept */
    else     DATA->hdr->cal_slope = factor; /* slope */

	_update_units(ENV);
}

/* calibration tabfolder callback */
#define FORM_W 220
#define FORM_H 90
void cb_calib_tf ( FL_OBJECT * obj  , long data  ) {
    (void)data;
	switch(fl_get_active_folder_number(obj)) {
		case 1: /* STD */
            resize_form_tabfolder(obj,GUI->calib_std->box);
			break;
	}
}
#undef FORM_W
#undef FORM_H

/* calibration framebuttons callback */
void cb_calib_framebut ( FL_OBJECT * obj  , long data  ) {
	switch(data) {
		case 0: /* Reset */
			fl_set_input(GUI->calibration->inp_slope,"1");
			fl_call_object_callback(GUI->calibration->inp_slope);
			fl_set_input(GUI->calibration->inp_inter,"0");
			fl_call_object_callback(GUI->calibration->inp_inter);
			break;
		case 1: /* voxel volume (VV) */
			fl_set_input_f(GUI->calibration->inp_slope,"%.16g",
                    DATA->hdr->vx*DATA->hdr->vy*DATA->hdr->vz);
			fl_call_object_callback(GUI->calibration->inp_slope);
			break;
	}
}

/* =========
   CALIB STD 
   ========= */

void cb_calib_std_inptime ( FL_OBJECT * obj  , long data  ) {
    (void)data;
	const char *txt = fl_get_input(obj);
	double num;

    num = strtod_checked(txt,0.0);
	fl_set_input_f(obj,"%.8g",num);
}

void cb_calib_std_inpact ( FL_OBJECT * obj  , long data  ) {
    (void)data;
	const char *txt = fl_get_input(obj);
	double num;

    num = strtod_checked(txt,1.0);
	fl_set_input_f(obj,"%.8g",num);
}

void cb_calib_std_inpgamma ( FL_OBJECT * obj  , long data  ) {
    (void)data;
	const char *txt = fl_get_input(obj);
	double num;

    num = strtod_checked(txt,1.0);
    num = FL_clamp(num,0.0,1.0);
	fl_set_input_f(obj,"%.8g",num);
}

void cb_calib_std_roi ( FL_OBJECT * obj  , long data  ) {
    (void)data;
	fl_set_object_label_f(GUI->calib_std->txt_val,"%.8g",DATA->rois[fl_get_choice(obj)-1]->total);
}

/* TODO: check procedure for calibration from standard! */
void cb_calib_std ( FL_OBJECT * obj  , long data  ) {
    (void)data;
(void)nuclides;
	const nroi_t roix = fl_get_choice(GUI->calib_std->ch_roi);
	double time, act, gamma, counts;
	double hl;
	double utime;
	double uact;
	double factor;

    if(roix < 1) return;

	switch(fl_get_choice(GUI->calib_std->ch_utime)) {
		case 1: utime = 1.0;              break; /* s */
		case 2: utime = MINUTE_TO_SECOND; break; /* m */
        default:
		case 3: utime = HOUR_TO_SECOND;   break; /* h */
	}

	switch(fl_get_choice(GUI->calib_std->ch_uact)) {
		case 1: uact = 1.0;                     break; /* Bq */
        default:
		case 2: uact = MICROCURIE_TO_BECQUEREL; break; /* uCi */
	}

	/* All units in SI after this (seconds,Becquerels) */
	hl = nuclide_halflife(fl_get_choice_text(GUI->calib_std->ch_nuclide),UNIT_SECOND);
	time = strtod(fl_get_input(GUI->calib_std->inp_time),NULL) * utime;
	act = strtod(fl_get_input(GUI->calib_std->inp_act),NULL) * uact;
	gamma = strtod(fl_get_input(GUI->calib_std->inp_gamma),NULL);
	counts = DATA->rois[roix-1]->total;

	factor = (act/gamma * exp(-(M_LN2/hl) * time)) / counts;
	/*factor = (act/gamma * time) / counts;*/
	DATA->hdr->cal_slope = factor;
    DATA->hdr->cal_inter = 0.0;
	fl_set_input_f(GUI->calibration->inp_slope,"%.16g",factor);
	fl_set_input_f(GUI->calibration->inp_inter,"%.16g",0.0);
	_update_units(ENV);
}
