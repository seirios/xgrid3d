#ifndef XG3D_gengrid_h_
#define XG3D_gengrid_h_

int cmp_uint32_t(const void*,const void*);
int gengrid(int,size_t*,unsigned int**,unsigned int**);

#endif
