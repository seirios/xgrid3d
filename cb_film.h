#ifndef XG3D_film_cb_h_
#define XG3D_film_cb_h_

#include "fd_film.h"

#define FILM_FIT_RESULT(fd_film,color) (((FD_film*)(fd_film))->plot_calib_##color->u_vdata)

#endif
