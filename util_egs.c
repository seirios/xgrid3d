#include "util_egs.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

char **setup_dosxyznrc_input(const char *name, const char *medium, const double vox, const double maxr, const uintmax_t nhist, const unsigned int njobs, const int Q, const int spectrum, const char *energy) {
	static char *dosxyznrc_input[DOSXYZNRC_INPUT_LEN] = {
        NULL,
		"1",
		NULL,
		"0.521, 0.01, 0, 0",
		"-1, -1, -1, 0",
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		"0, 0, 0, 0, 0, 0, 0, 0",
		"0, 0, 0, 0, 0, 0, 0, 0",
		"0, 0, 0, 0, 0, 0, 0, 1",
		NULL,
		NULL,
		NULL,
		NULL,
		" #########################",
		" :Start MC Transport Parameter:",
		" ",
		" Global ECUT= 0.521",
		" Global PCUT= 0.01",
		" Global SMAX= 1e10",
		" ESTEPE= 0.25",
		" XIMAX= 0.5",
		" Boundary crossing algorithm= EXACT",
		" Skin depth for BCA= 3",
		" Electron-step algorithm= PRESTA-II",
		" Spin effects= On",
		" Brems angular sampling= KM",
		" Brems cross sections= NRC",
		" Bound Compton scattering= Norej",
		" Compton cross sections= default",
		" Pair angular sampling= Simple",
		" Pair cross sections= NRC",
		" Photoelectron angular sampling= On",
		" Rayleigh scattering= On",
		" Atomic relaxations= On",
		" Electron impact ionization= Off",
		" Photon cross sections= epdl",
		" Photon cross-sections output= On",
		" ",
		" :Stop MC Transport Parameter:",
		" #########################",
	};
	unsigned int nvox = ceil(maxr/vox);
	double lobnd = nvox * (-vox);
    unsigned int seed;
	char buf[256];

    dosxyznrc_input[0] = strdup(name);
	dosxyznrc_input[2] = strdup(medium);
	snprintf(buf,sizeof(buf),"%.6g",lobnd);
	dosxyznrc_input[5] = dosxyznrc_input[7] = dosxyznrc_input[9] = strdup(buf);
	snprintf(buf,sizeof(buf),"%.6g, %u",vox,2*nvox+1);
	dosxyznrc_input[6] = dosxyznrc_input[8] = dosxyznrc_input[10] = strdup(buf);
	snprintf(buf,sizeof(buf),"%d, 6, 0, %.6g, 0, %.6g, 0, %.6g, 0, 0,",Q,vox,vox,vox);
	dosxyznrc_input[14] = strdup(buf);
	snprintf(buf,sizeof(buf),"%d, 0, 0, 0, 0, 0, 0, 0",spectrum ? 1 : 0);
	dosxyznrc_input[15] = strdup(buf);
	dosxyznrc_input[16] = strdup(energy);
    seed = 1 + (unsigned)(((double)random()/RAND_MAX) * (30081 - njobs));
    /* HOWFARLESS OFF */
	snprintf(buf,sizeof(buf),"%"PRIuMAX", 0, 24, 2, %u, 100.0, 0, 0, 1, 0, , 0, 0, 0, 1, 0, 0",nhist,seed);
	dosxyznrc_input[17] = strdup(buf);

	return dosxyznrc_input;
}

void free_dosxyznrc_input(char **dosxyznrc_input) {
	free(dosxyznrc_input[2]);
	free(dosxyznrc_input[5]);
	free(dosxyznrc_input[6]);
	free(dosxyznrc_input[14]);
	free(dosxyznrc_input[15]);
	free(dosxyznrc_input[16]);
	free(dosxyznrc_input[17]);
}

char **setup_dosxyzpp_input(const char *name, const char *medium, const double vox, const double maxr, const uintmax_t nhist, const unsigned int njobs, const int Q, const int spectrum, const char *energy) {
	static char *dosxyzpp_input[DOSXYZPP_INPUT_LEN] = {
        ":start geometry definition:",
        "    :start geometry:",
        "        name = xyz_geom",
        "        library = egs_ndgeometry",
        "        type = EGS_XYZGeometry",
        NULL, /* x-slabs */
        NULL, /* y-slabs */
        NULL, /* z-slabs */
        "        :start media input:",
        NULL, /* media */
        "        :stop media input:",
        "    :stop geometry:",
        "    simulation geometry = xyz_geom",
        ":stop geometry definition:\n",

        ":start source definition:",
        "    :start source:",
        "        name = isovox",
        "        library = egs_isotropic_source",
        NULL, /* charge */
        "        :start shape:",
        "            type = box",
        NULL, /* box size */
        "        :stop shape:",
        "        :start spectrum:",
        NULL, /* type */
        NULL, /* energy / spectrum file */
        "        :stop spectrum:",
        "    :stop source:",
        "    simulation source = isovox",
        ":stop source definition:\n",

        ":start rng definition:",
        "    type = ranmar",
        NULL, /* initial seeds */
        ":stop rng definition:\n",

        ":start run control:",
        NULL, /* ncase */
        ":stop run control:\n",

        ":start MC transport parameter:",            /* ======= variable ======= ===== block ==== */
		"    Global ECUT = 0.521",                   /* ecut        (in regions) BOUNDS           */
		"    Global PCUT = 0.01",                    /* pcut        (in regions) BOUNDS           */
		"    Global SMAX = 1e10",                    /* smaxir      (in regions) ET_CONTROL       */
		"    Bound Compton scattering = norej",      /* ibcmp       (in regions) XSECTION_OPTIONS */
        "    Radiative Compton corrections = Off",   /* radc_flag                XSECTION_OPTIONS */
		"    Rayleigh scattering = On",              /* iraylr      (in regions) XSECTION_OPTIONS */
		"    Atomic relaxations = On",               /* iedgfl      (in regions) XSECTION_OPTIONS */
        "    Photoelectron angular sampling = On",   /* iphter      (in regions) XSECTION_OPTIONS */
		"    Brems angular sampling = KM",           /* ibrdst                   XSECTION_OPTIONS */
        "    Brems cross sections = NRC",            /* ibr_nist                 XSECTION_OPTIONS */
		"    Pair angular sampling = Simple",        /* iprdst                   XSECTION_OPTIONS */
		"    Pair cross sections = NRC",             /* pair_nrc                 XSECTION_OPTIONS */
        "    Triplet production = Off",              /* itriplet                 XSECTION_OPTIONS */
		"    Spin effects = On",                     /* spin_effects             XSECTION_OPTIONS */
		"    Electron Impact Ionization = Off",      /* eii_flag                 XSECTION_OPTIONS */
		"    ESTEPE = 0.25",                         /* estepe                   ET_CONTROL       */
		"    XIMAX = 0.5",                           /* ximax                    ET_CONTROL       */
		"    Boundary crossing algorithm = Exact",   /* bca_algorithm, exact_bca ET_CONTROL       */
		"    Skin depth for BCA = 3",                /* skindepth_for_bca        ET_CONTROL       */
		"    Electron-step algorithm = PRESTA-II",   /* transport_algorithm      ET_CONTROL       */
        "    Photon cross sections = epdl",          /* photon_xsections         MEDIA            */
		"    Photon cross-sections output = Off",    /* xsec_out                 EGS_IO           */
		"    Compton cross sections = default",      /* comp_xsections           MEDIA            */
        "    Electric Field = 0",                    /* ExIN, EyIN, EzIN         EMF              */
        "    Magnetic Field = 0",                    /* BxIN, ByIN, BzIN         EMF              */
        "    EM ESTEPE = 0.02",                      /* EMLMTIN                  EMF              */
        "    Photonuclear attenuation = Off",        /* iphotonuc   (in regions) XSECTION_OPTIONS */
        "    Photonuclear cross sections = default", /* photonuc_xsections       MEDIA            */
        ":stop MC transport parameter:"
    };
	unsigned int nvox = ceil(maxr/vox);
	double lobnd = (nvox + 0.5) * (-vox);
    unsigned int seed1 = 1 + (unsigned)(((double)random()/RAND_MAX) * (31328 - njobs)),
                 seed2 = 1 + (unsigned)(((double)random()/RAND_MAX) * (30081 - njobs));
	char buf[256];

    (void)name;

    snprintf(buf,sizeof(buf),"        x-slabs = %g %g %u",lobnd,vox,2*nvox+1);
    dosxyzpp_input[5] = strdup(buf);
    snprintf(buf,sizeof(buf),"        y-slabs = %g %g %u",lobnd,vox,2*nvox+1);
    dosxyzpp_input[6] = strdup(buf);
    snprintf(buf,sizeof(buf),"        z-slabs = %g %g %u",lobnd,vox,2*nvox+1);
    dosxyzpp_input[7] = strdup(buf);
	snprintf(buf,sizeof(buf),"            media = %s",medium);
	dosxyzpp_input[9] = strdup(buf);
	snprintf(buf,sizeof(buf),"        charge = %d",Q);
	dosxyzpp_input[18] = strdup(buf);
	snprintf(buf,sizeof(buf),"            box size = %g",vox);
	dosxyzpp_input[21] = strdup(buf);
    if(spectrum) {
        snprintf(buf,sizeof(buf),"            type = tabulated spectrum");
        dosxyzpp_input[24] = strdup(buf);
        snprintf(buf,sizeof(buf),"            spectrum file = %s",energy);
        dosxyzpp_input[25] = strdup(buf);
    } else {
        snprintf(buf,sizeof(buf),"            type = monoenergetic");
        dosxyzpp_input[24] = strdup(buf);
        snprintf(buf,sizeof(buf),"            energy = %s",energy);
        dosxyzpp_input[25] = strdup(buf);
    }
    snprintf(buf,sizeof(buf),"    initial seeds = %u %u",seed1,seed2);
    dosxyzpp_input[32] = strdup(buf);
    snprintf(buf,sizeof(buf),"    ncase = %"PRIuMAX,nhist/njobs);
    dosxyzpp_input[35] = strdup(buf);

    return dosxyzpp_input;
}

void free_dosxyzpp_input(char **dosxyzpp_input) {
    free(dosxyzpp_input[5]);
    free(dosxyzpp_input[6]);
    free(dosxyzpp_input[7]);
    free(dosxyzpp_input[9]);
    free(dosxyzpp_input[18]);
    free(dosxyzpp_input[21]);
    free(dosxyzpp_input[24]);
    free(dosxyzpp_input[25]);
    free(dosxyzpp_input[32]);
    free(dosxyzpp_input[35]);
}

char* serialize_egs_input(char **egs_input, const size_t nlines) {
    char buf[1024];

    size_t len = 1;
    for(size_t i=0;i<nlines;i++)
        len += strlen(egs_input[i]) + 1;

    char *serial = malloc(len * sizeof(char));
    serial[0] = '\0';

    for(size_t i=0;i<nlines;i++) {
        snprintf(buf,sizeof(buf),"%s\n",egs_input[i]);
        strncat(serial,buf,1024);
    }

    return serial;
}

int export_egs_input(const char *file, char **egs_input, const size_t nlines) {
	FILE *fp = fopen(file,"w");
	if(fp == NULL) return 1;
	for(size_t i=0;i<nlines;i++)
		fprintf(fp,"%s\n",egs_input[i]);
	fclose(fp);

	return 0;
}
