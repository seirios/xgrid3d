#ifndef XG3D_util_filter_h_
#define XG3D_util_filter_h_

#include <stdbool.h>

enum xg3d_filter_pass {
    FILTER_LOWPASS,
    FILTER_HIGHPASS
};

double* filter_spatial_box(unsigned int,unsigned int,bool);
double* filter_spatial_gaussian2(unsigned int,double,bool);
double* filter_spatial_gaussian3(unsigned int,double,bool);
double filter_frequency_butterworth(double,unsigned int,double,enum xg3d_filter_pass);
double filter_frequency_gaussian(double,double,enum xg3d_filter_pass);

#endif
