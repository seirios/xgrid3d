
#include <libgen.h>

#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "cb_datasets.h"
#include "cb_info.h"
#include "cb_dialog.h"
#include "cb_viewport.h"
#include "cb_overlay.h"
#include "cb_dose.h"
#include "cb_quantization.h"
#include "cb_selection.h"

#include "selection.h"

#include "util.h"

#undef ENV
void setup_attrs(xg3d_env *ENV, const int i) {
    const xg3d_data *data;

    fl_clear_browser(GUI->datasets->br_attr_curr);
    if(i >= 0) {
        data = ENV->data->sets[i];
            fl_add_browser_line(GUI->datasets->br_attr_curr,data == ENV->data->main ? _("Main") : "");

            fl_add_browser_line(GUI->datasets->br_attr_curr,data == ENV->data->overlay ? _("Overlay") : "");

            fl_add_browser_line(GUI->datasets->br_attr_curr,data == ENV->data->dose ? _("Dose") : "");

            fl_add_browser_line(GUI->datasets->br_attr_curr,data == ENV->data->dose_err ? _("Dose error") : "");

            fl_add_browser_line(GUI->datasets->br_attr_curr,data == ENV->data->sino ? _("Sinogram") : "");

    }
    /* coupled browser offsets */
    fl_set_browser_yoffset(GUI->datasets->br_attr_curr,
            fl_get_browser_yoffset(GUI->datasets->br_attr_avail));
}
#define ENV _default_env

void refresh_datasets_browser(FL_OBJECT *browser, const xg3d_datasets *ds) {
    if(browser == NULL || ds == NULL) return; /* null input */
    const int ofy = fl_get_browser_yoffset(browser);

    fl_freeze_form(browser->form);
    fl_clear_browser(browser);
    for(unsigned int i=0;i<ds->nsets;i++) {
        if(ds->sets[i]->name == NULL) {
            char *base = strdup(ds->sets[i]->hdr->file);
            ds->sets[i]->name = strdup(basename(base));
            free(base);
        }
        if(ds->main == ds->sets[i])
            fl_add_browser_line_f(browser,"@_%s",ds->sets[i]->name);
        else
            fl_add_browser_line_f(browser,"%s",ds->sets[i]->name);
    }
    fl_set_browser_yoffset(browser,ofy);
    fl_unfreeze_form(browser->form);
}

void refresh_datasets_info(FD_datasets *datasets, const xg3d_datasets *ds, const bool current) {
    FD_info *info = fl_get_formbrowser_form(datasets->fb_info,1)->fdui;
    const int i = fl_get_browser(datasets->browser) - 1;

    if(i < 0) return;
    if(current && ds->main != ds->sets[i]) return;

    setup_info(info,ds->sets[i]->hdr);
}

void cb_datasets_browser ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    FD_info *info = fl_get_formbrowser_form(GUI->datasets->fb_info,1)->fdui;
    const int i = fl_get_browser(obj) - 1;

    switch(fl_get_object_return_state(obj)) {
        case FL_RETURN_SELECTION:
            if(i < 0) return;
            setup_info(info,ENV->data->sets[i]->hdr);
            setup_attrs(ENV,i);
            break;
        case FL_RETURN_DESELECTION:
            setup_info(info,NULL);
            setup_attrs(ENV,-1);
            break;
    }
}

void cb_datasets_io ( FL_OBJECT * obj  , long data  ) {
    FD_info *info = fl_get_formbrowser_form(GUI->datasets->fb_info,1)->fdui;
    int i = fl_get_browser(GUI->datasets->browser) - 1;
    xg3d_header *xhdr;
    xg3d_data *old;
    g3d_ret ret;

    switch(data) {
        case 0: /* Unload */
            if(i < 0) return;
            old = ENV->data->sets[i];
            /* ENV->data->main can be removed even if in use */
            if(old == ENV->data->overlay ||
                    old == ENV->data->dose ||
                    old == ENV->data->dose_err ||
                    old == ENV->data->sino) {
                fl_show_alert2(0,_("Dataset in use!\fPlease stop using it before unloading"));
                return;
            }
            xg3d_datasets_remove(ENV->data,old);
            if(ENV->data->nsets > 0) {
                i = FL_max(0,i-1);
                if(ENV->data->main == old)
                    setup_main(ENV,ENV->data->sets[i]);
                refresh_datasets_browser(GUI->datasets->browser,ENV->data);
                fl_select_browser_line(GUI->datasets->browser,i+1);
                setup_info(info,ENV->data->sets[i]->hdr);
                setup_attrs(ENV,i);
            } else {
                setup_main(ENV,NULL);
                refresh_datasets_browser(GUI->datasets->browser,ENV->data);
                setup_info(info,NULL);
                setup_attrs(ENV,-1);
            }
            break;
        case 1: /* Load */
            xhdr = opendlg_show(ENV);
            if(xhdr == NULL) return;

            fl_show_oneliner(_("Loading data..."),fl_scrw/2,fl_scrh/2);
            if((ret = load_data(ENV,xhdr)) != G3D_OK) {
                fl_hide_oneliner();
                fl_show_alert2(0,_("Failed loading data!\fFile: %s\nERROR: %s"),xhdr->file,g3d_ret_msg(ret));
                xg3d_header_free(xhdr);
                return;
            }
            xg3d_header_free(xhdr);
            fl_hide_oneliner();
            refresh_datasets_browser(GUI->datasets->browser,ENV->data);
            break;
        case 2: /* Save */
            if(i < 0) return;
            savedlg_show(GUI->savedlg,ENV->data->sets[i],ENV->data->sets[i]->name);
            break;
    }
}

void cb_datasets_tomain ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const int i = fl_get_browser(GUI->datasets->browser) - 1;

    if(i < 0) return;

    if(ENV->data->main != ENV->data->sets[i]) {
        setup_main(ENV,ENV->data->sets[i]);
        refresh_datasets_browser(GUI->datasets->browser,ENV->data);
        fl_select_browser_line(GUI->datasets->browser,i+1);
        setup_attrs(ENV,i);
    }
}

void cb_datasets_attr ( FL_OBJECT * obj  , long data  ) {
    const int i = fl_get_browser(GUI->datasets->browser) - 1;
    enum xg3d_attr attr;
    const char *txt;

    if(!data) { /* Available (set) */
        switch(fl_get_object_return_state(obj)) {
            case FL_RETURN_SELECTION:
                if(i < 0) return; /* no data selected */
                switch(attr = fl_get_browser(obj)) {
                    case(ATTR_MAIN):
                        if(ENV->data->main != ENV->data->sets[i])
                            setup_main(ENV,ENV->data->sets[i]);
                        break;
                    case(ATTR_OVERLAY):
                        if(ENV->data->overlay != ENV->data->sets[i] &&
                                ENV->data->main != NULL)
                            setup_overlay(ENV,ENV->data->sets[i]);
                        break;
                    case(ATTR_DOSE):
                        if(ENV->data->dose != ENV->data->sets[i] &&
                                ENV->data->main != NULL)
                            setup_dose(ENV,ENV->data->sets[i]);
                        break;
                    case(ATTR_DOSE_ERROR):
                        if(ENV->data->dose_err != ENV->data->sets[i] &&
                                ENV->data->main != NULL)
                            setup_dose_err(ENV,ENV->data->sets[i]);
                        break;
                    case(ATTR_SINOGRAM):   ENV->data->sino  = ENV->data->sets[i]; break;
                }
                refresh_datasets_browser(GUI->datasets->browser,ENV->data);
                fl_select_browser_line(GUI->datasets->browser,i+1);
                setup_attrs(ENV,i);
                break;
            case FL_RETURN_CHANGED:
            case FL_RETURN_CHANGED | FL_RETURN_END:
                fl_set_browser_yoffset(GUI->datasets->br_attr_curr,fl_get_browser_yoffset(obj));
                break;
        }
    } else { /* Current (unset) */
        switch(fl_get_object_return_state(obj)) {
            case FL_RETURN_SELECTION:
                if(i < 0) return; /* no data selected */
                attr = fl_get_browser(obj);
                txt = fl_get_browser_line(obj,attr);
                if(strlen(txt) == 0) return;
                switch(attr) {
                    case(ATTR_MAIN):       setup_main(ENV,NULL);     
                                           setup_overlay(ENV,NULL);
                                           setup_dose(ENV,NULL);
                                           setup_dose_err(ENV,NULL); break;
                    case(ATTR_OVERLAY):    setup_overlay(ENV,NULL);  break;
                    case(ATTR_DOSE):       setup_dose(ENV,NULL);     break;
                    case(ATTR_DOSE_ERROR): setup_dose_err(ENV,NULL); break;
                    case(ATTR_SINOGRAM):   ENV->data->sino = NULL;   break;
                }
                refresh_datasets_browser(GUI->datasets->browser,ENV->data);
                fl_select_browser_line(GUI->datasets->browser,i+1);
                setup_attrs(ENV,i);
                break;
            case FL_RETURN_CHANGED:
            case FL_RETURN_CHANGED | FL_RETURN_END:
                fl_set_browser_yoffset(GUI->datasets->br_attr_avail,fl_get_browser_yoffset(obj));
                break;
        }
    }
}

void cb_datasets_attr_clear ( FL_OBJECT * obj  , long data  ) {
    const int i = fl_get_browser(GUI->datasets->browser) - 1;
    const xg3d_data *dat;

    /* Main is not cleared */
    if(!data) { /* Available */
        setup_overlay(ENV,NULL);
        setup_dose(ENV,NULL);
        setup_dose_err(ENV,NULL);
        ENV->data->sino = NULL;
    } else { /* Current */
        if(i < 0) return;
        dat = ENV->data->sets[i];
        if(ENV->data->overlay == dat)
            setup_overlay(ENV,NULL);
        if(ENV->data->dose == dat)
            setup_dose(ENV,NULL);
        if(ENV->data->dose_err == dat)
            setup_dose_err(ENV,NULL);
        if(ENV->data->sino == dat)
            ENV->data->sino = NULL;
    }
    refresh_datasets_browser(GUI->datasets->browser,ENV->data);
    fl_select_browser_line(GUI->datasets->browser,i+1);
    setup_attrs(ENV,i);
}

void cb_datasets_shift ( FL_OBJECT * obj  , long data  ) {
    const int i = fl_get_browser(GUI->datasets->browser) - 1;
    xg3d_data *tmp;

    if(i < 0 || ENV->data->nsets == 1) return;

    if(data) { /* Up */
        if(i == 0) return;
        tmp = ENV->data->sets[i-1];
        ENV->data->sets[i-1] = ENV->data->sets[i];
    } else { /* Down */
        if(i == (int)ENV->data->nsets-1) return;
        tmp = ENV->data->sets[i+1];
        ENV->data->sets[i+1] = ENV->data->sets[i];
    }
    ENV->data->sets[i] = tmp;
    refresh_datasets_browser(GUI->datasets->browser,ENV->data);
    fl_select_browser_line(GUI->datasets->browser,data ? i : i+2);
}

void cb_datasets_rename ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const int i = fl_get_browser(GUI->datasets->browser) - 1;
    const char *str;

    if(i < 0) return;

    str = fl_show_input(_("Dataset name"),ENV->data->sets[i]->name);
    if(str == NULL || strlen(str) == 0) return;
    if(strcmp(ENV->data->sets[i]->name,str) == 0) return;
    free(ENV->data->sets[i]->name);
    ENV->data->sets[i]->name = strdup(str);
    refresh_datasets_browser(GUI->datasets->browser,ENV->data);
    fl_select_browser_line(GUI->datasets->browser,i+1);
}

void cb_datasets_dup ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    FD_info *info = fl_get_formbrowser_form(GUI->datasets->fb_info,1)->fdui;
    const int i = fl_get_browser(GUI->datasets->browser) - 1;
    int ilast;

    if(i < 0) return;

    fl_show_oneliner(_("Duplicating dataset..."),fl_scrw/2,fl_scrh/2);

    xg3d_datasets_add(ENV->data,xg3d_data_dup(ENV->data->sets[i]));
    refresh_datasets_browser(GUI->datasets->browser,ENV->data);
    ilast = fl_get_browser_maxline(GUI->datasets->browser);
    fl_select_browser_line(GUI->datasets->browser,ilast);
    setup_info(info,ENV->data->sets[ilast-1]->hdr);
    setup_attrs(ENV,ilast-1);

    fl_hide_oneliner();
}

void cb_datasets_convert ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    FD_info *info = fl_get_formbrowser_form(GUI->datasets->fb_info,1)->fdui;
    const int i = fl_get_browser(GUI->datasets->browser) - 1;
    const char *type_name = NULL;
    enum g3d_raw_io_type iotype;
    const xg3d_data *dat;
    enum g3d_type type;
    xg3d_data *new;

    if(i < 0) return;

    /* NOTE: menu entries must be in same order as in table g3d_type_assoc */
    switch(fl_get_menu(obj) - 1) {
        default: /* first entry */
                    case G3D_FLOAT32:
                type = G3D_FLOAT32;
                iotype = RAW_FLOAT32;
                type_name = "float32";
                break;
                   case G3D_FLOAT64:
                type = G3D_FLOAT64;
                iotype = RAW_FLOAT64;
                type_name = "float64";
                break;
                   case G3D_UINT8:
                type = G3D_UINT8;
                iotype = RAW_UINT8;
                type_name = "uint8";
                break;
                   case G3D_UINT16:
                type = G3D_UINT16;
                iotype = RAW_UINT16;
                type_name = "uint16";
                break;
                   case G3D_UINT32:
                type = G3D_UINT32;
                iotype = RAW_UINT32;
                type_name = "uint32";
                break;
                   case G3D_UINT64:
                type = G3D_UINT64;
                iotype = RAW_UINT64;
                type_name = "uint64";
                break;
                   case G3D_INT8:
                type = G3D_INT8;
                iotype = RAW_INT8;
                type_name = "int8";
                break;
                   case G3D_INT16:
                type = G3D_INT16;
                iotype = RAW_INT16;
                type_name = "int16";
                break;
                   case G3D_INT32:
                type = G3D_INT32;
                iotype = RAW_INT32;
                type_name = "int32";
                break;
                   case G3D_INT64:
                type = G3D_INT64;
                iotype = RAW_INT64;
                type_name = "int64";
                break;
           }

    dat = ENV->data->sets[i];
    new = calloc(1,sizeof(xg3d_data));
    if(new == NULL) return;
    new->g3d = g3d_convert(dat->g3d,type,ENDIAN_KEEP);
    if(new->g3d == NULL) {
        xg3d_data_clear(new,true);
        return;
    }
    new->hdr = xg3d_header_dup(dat->hdr);
    if(new->hdr == NULL) {
        xg3d_data_clear(new,true);
        return;
    }
    {
        char buf[1024];
        snprintf(buf,sizeof(buf),"(%s)%s",type_name,dat->name);
        free(new->name); new->name = strdup(buf);
    }
    new->hdr->type = iotype;
    xg3d_datasets_add(ENV->data,new);

    refresh_datasets_browser(GUI->datasets->browser,ENV->data);
    int ilast = fl_get_browser_maxline(GUI->datasets->browser);
    fl_select_browser_line(GUI->datasets->browser,ilast);
    setup_info(info,ENV->data->sets[ilast-1]->hdr);
    setup_attrs(ENV,ilast-1);
}

void cb_datasets_extract ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    FD_info *info = fl_get_formbrowser_form(GUI->datasets->fb_info,1)->fdui;
    xg3d_data *new;
    grid3d *g3d;
    char *name;

    if(ENV->data->main == NULL) return;

    switch(fl_get_menu(obj)) {
        default:
        case 1: /* Global quantization */
            name = strdup(_("Global quantization"));
            g3d = g3d_alloc(G3D_UINT8,DATA->g3d->nx,DATA->g3d->ny,DATA->g3d->nz,.alloc=true);
#ifdef _OPENMP
#pragma omp parallel for
#endif
            for(index_t ix=0;ix<g3d->nvox;ix++)
                g3d_u8(g3d,ix) = g3d_quant_apply_single(g3d_get_dvalue(DATA->g3d,ix),QUANTIZATION_INFO(GUI->quantization));
            break;
        case 2: /* Selection */
            if(SELECTION_NVOX(GUI->selection) == 0) return;
            name = strdup(_("Selection"));
            g3d = g3d_dup(SELECTION_G3D(GUI->selection));
#ifdef _OPENMP
#pragma omp parallel for
#endif
            for(index_t ix=0;ix<g3d->nvox;ix++)
                g3d_u8(g3d,ix) &= SEL_BIT;
            break;
    }

    new = calloc(1,sizeof(xg3d_data));
    new->g3d = g3d;
    new->name = name;
    new->hdr = calloc(1,sizeof(xg3d_header));
    new->hdr->file = strdup(_("INTERNAL"));
    new->hdr->type = RAW_UINT8;
    new->hdr->dim[0] = g3d->nx;
    new->hdr->dim[1] = g3d->ny;
    new->hdr->dim[2] = g3d->nz;
    new->hdr->vx = new->hdr->vy = new->hdr->vz = 1.0;
    new->hdr->cal_inter = 0.0;
    new->hdr->cal_slope = 1.0;
    new->hdr->acqmod = ACQ_UNKNOWN;
    xg3d_datasets_add(ENV->data,new);

    refresh_datasets_browser(GUI->datasets->browser,ENV->data);
    int ilast = fl_get_browser_maxline(GUI->datasets->browser);
    fl_select_browser_line(GUI->datasets->browser,ilast);
    setup_info(info,ENV->data->sets[ilast-1]->hdr);
    setup_attrs(ENV,ilast-1);
}

void cb_datasets_quantize ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    FD_info *info = fl_get_formbrowser_form(GUI->datasets->fb_info,1)->fdui;
    const int i = fl_get_browser(GUI->datasets->browser) - 1;
    enum g3d_raw_io_type iotype;
    g3d_quant_info *qinfo;
    const xg3d_data *dat;
    g3d_stats *stats;
    xg3d_data *new;
    char buf[2048];
    grid3d *g3d;
    int m,ilast;

    if(i < 0) return;

    dat = ENV->data->sets[i];
    stats = g3d_stats_get(dat->g3d,NULL,G3D_STATS_MINMAXSUM);

    qinfo = calloc(1,sizeof(g3d_quant_info));
    switch(m = fl_get_menu(obj)) {
        default:
        case 1: /* 8-bit linear */
            iotype = RAW_UINT8;
            snprintf(buf,sizeof(buf),"%s %s",_("8-bit linear quantization of"),dat->name);
            g3d_quant_intervals(dat->g3d,1U<<8,1.0,stats->min,stats->max,QUANT_CLAMP_NONE,qinfo);
            g3d = g3d_alloc(G3D_UINT8,dat->g3d->nx,dat->g3d->ny,dat->g3d->nz,.alloc=true);
            break;
        case 2: /* 16-bit linear */
            iotype = RAW_UINT16;
            snprintf(buf,sizeof(buf),"%s %s",_("16-bit linear quantization of"),dat->name);
            g3d_quant_intervals(dat->g3d,1U<<16,1.0,stats->min,stats->max,QUANT_CLAMP_NONE,qinfo);
            g3d = g3d_alloc(G3D_UINT16,dat->g3d->nx,dat->g3d->ny,dat->g3d->nz,.alloc=true);
            break;
        case 3: /* 8-bit unique */
            return;
            iotype = RAW_UINT8;
            snprintf(buf,sizeof(buf),"%s %s",_("8-bit unique quantization of"),dat->name);
            g3d = NULL;
            break;
        case 4: /* 16-bit unique */
            return;
            iotype = RAW_UINT16;
            snprintf(buf,sizeof(buf),"%s %s",_("16-bit unique quantization of"),dat->name);
            g3d = NULL;
            break;
    }
    g3d_quant_apply(dat->g3d,g3d,qinfo);
    g3d_quant_info_clear(qinfo,true);

    free(stats);

    new = calloc(1,sizeof(xg3d_data));
    new->g3d = g3d;
    new->name = strdup(buf);
    new->hdr = calloc(1,sizeof(xg3d_header));
    new->hdr->file = strdup(_("INTERNAL"));
    new->hdr->type = iotype;
    new->hdr->dim[0] = g3d->nx;
    new->hdr->dim[1] = g3d->ny;
    new->hdr->dim[2] = g3d->nz;
    new->hdr->vx = new->hdr->vy = new->hdr->vz = 1.0;
    new->hdr->cal_inter = 0.0;
    new->hdr->cal_slope = 1.0;
    new->hdr->acqmod = ACQ_UNKNOWN;
    xg3d_datasets_add(ENV->data,new);

    refresh_datasets_browser(GUI->datasets->browser,ENV->data);
    ilast = fl_get_browser_maxline(GUI->datasets->browser);
    fl_select_browser_line(GUI->datasets->browser,ilast);
    setup_info(info,ENV->data->sets[ilast-1]->hdr);
    setup_attrs(ENV,ilast-1);
}
