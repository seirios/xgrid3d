#ifndef XG3D_util_fit3d_h_
#define XG3D_util_fit3d_h_

#include <gsl/gsl_linalg.h>

gsl_vector* best_fit_line_vector(gsl_matrix*);
gsl_vector* best_fit_plane_normal_vector(gsl_matrix*);
gsl_matrix* get_rotation_align(const gsl_vector*,const gsl_vector*);
gsl_vector* get_translation_center(const gsl_matrix*,const gsl_vector*);

#endif
