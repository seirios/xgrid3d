#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif

#include <math.h>
#include <errno.h>
#include <limits.h>
#include <libgen.h>
#include <stdint.h>

#ifdef __CYGWIN__
#include <sys/cygwin.h>
#endif

#include "cb_dialog.h"
#include "cb_viewport.h"
#include "cb_mainview.h"
#include "cb_quantization.h"
#include "cb_menu.h"

#include "matheval.h"
#include "util.h"
#include "util_units.h"

#include "albira.h"

void cb_fselector_home(void *data) {
    (void)data;
#ifdef __CYGWIN__
    void *cygpath = cygwin_create_path(CCP_WIN_A_TO_POSIX|CCP_ABSOLUTE,getenv("USERPROFILE"));
    fl_set_directory((const char*)cygpath);
    free(cygpath);
#else
    fl_set_directory(getenv("HOME"));
#endif
}

/* =======
   OPENDLG
   ======= */

void cb_opendlg_fselect ( FL_OBJECT * obj  , long data  ) {
    xg3d_header *hdr = OPEN_HEADER(GUI->open_hdr);
    const char *file,*inp;
    char fltstr[16] = "*";
    char *dir,*base;
    off_t fsiz;
    FILE *fp;

    switch(data) {
        case 0: /* Header */
            inp = fl_get_input(GUI->open_hdr->inp_file);
            if(strlen(inp) > 0) { dir = strdup(inp); fl_set_directory(dirname(dir)); free(dir); }
            base = strdup(inp);
            file = fl_show_fselector(_("Open header file..."),"","*",basename(base)); free(base);
            if(file == NULL) return;
            free(hdr->file); hdr->file = NULL;
            if(!HDR_TEST_ALL(file,hdr)) {
                fl_show_alert2(0,_("Failed parsing header!\fPlease check file or select another."));
                return;
            }
            fl_set_input(GUI->open_hdr->inp_file,file);
            base = strdup(hdr->file);
            fl_set_object_label(GUI->open_hdr->txt_data,basename(base)); free(base);
            set_type_text(GUI->open_hdr->txt_type,hdr->type,hdr->endian);
            fl_set_object_label_f(GUI->open_hdr->txt_dim,"x: %3u (%5.3f)\ny: %3u (%5.3f)\nz: %3u (%5.3f)",
                    hdr->dim[0],hdr->vx,hdr->dim[1],hdr->vy,hdr->dim[2],hdr->vz);
            fl_set_object_label_f(GUI->open_hdr->txt_calib,"%gx%+g",hdr->cal_slope,hdr->cal_inter);
            switch(hdr->acqmod) {
                case ACQ_CT:    fl_set_object_label(GUI->open_hdr->txt_acqmod,_("CT"));      break;
                case ACQ_PET:   fl_set_object_label(GUI->open_hdr->txt_acqmod,_("PET"));     break;
                case ACQ_SPECT: fl_set_object_label(GUI->open_hdr->txt_acqmod,_("SPECT"));   break;
                default:        fl_set_object_label(GUI->open_hdr->txt_acqmod,_("Unknown")); break;
            }
            break;
        case 1: /* Raw */
            inp = fl_get_input(GUI->open_raw->inp_file);
            if(strlen(inp) > 0) { dir = strdup(inp); fl_set_directory(dirname(dir)); free(dir); }
            base = strdup(inp);
            file = fl_show_fselector(_("Open raw data file..."),"","*",basename(base)); free(base);
            if(file == NULL) return;
            fp = fopen(file,"r");
            if(fp == NULL) {
                fl_show_alert2(0,_("Cannot open file!\f%s"),file);
                return;
            }
            /* get file size */
            if(fseeko(fp,0,SEEK_END) != 0) {
                perror("ERROR fseeko()");
                return;
            }
            fsiz = ftello(fp);
            if(fsiz == -1) {
                perror("ERROR ftello()");
                return;
            }
            fclose(fp);
            fl_set_input(GUI->open_raw->inp_file,file);
            fl_set_object_label_f(GUI->open_raw->txt_fsize,"%lu",fsiz);
            fl_set_choice(GUI->open_raw->ch_compr,file_compr_type(file)+1);
            fl_call_object_callback(GUI->open_raw->ch_compr);
            break;
        case 2: /* Predef */
            inp = fl_get_input(GUI->open_def->inp_file);
            if(strlen(inp) > 0) { dir = strdup(inp); fl_set_directory(dirname(dir)); free(dir); }
            switch(fl_get_choice(GUI->open_def->ch_preset)) {
                case 1: snprintf(fltstr,sizeof(fltstr),"*.raw*"); break; /* ALBIRA CT RAW */
                case 2: snprintf(fltstr,sizeof(fltstr),"*.bin*"); break; /* ALBIRA SPECT BIN */
            }
            base = strdup(inp);
            file = fl_show_fselector(_("Open raw data file..."),"",fltstr,basename(base)); free(base);
            if(file != NULL) fl_set_input(GUI->open_def->inp_file,file);
            break;
    }
}

void cb_raw_compr ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const enum g3d_compr_type compr = fl_get_choice(obj)-1;

    fl_set_object_lcolor(GUI->open_raw->txt_fsize,compr == COMPR_NONE ? FL_BLACK : FL_RED);
}

void cb_dialog_inp_calib ( FL_OBJECT * obj  , long data  ) {
    const char *txt = fl_get_input(obj);
    const double defval = data ? 0.0 /* intercept */ : 1.0 /* slope */;
    double factor;

    factor = strtod_checked(txt,defval);
    fl_set_input_f(obj,"%g",factor);    
}

void cb_dialog_update_dsize ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *spx = NULL,
              *spy = NULL,
              *spz = NULL,
              *txt = NULL,
              *ch_type = NULL;
    coord_t nx,ny,nz;
    size_t dsiz,off;

    switch(data) {
        default:
        case 0: /* open_raw */
            spx = GUI->open_raw->spin_nx;
            spy = GUI->open_raw->spin_ny;
            spz = GUI->open_raw->spin_nz;
            off = fl_get_spinner_value(GUI->open_raw->spin_off);
            txt = GUI->open_raw->txt_dsize;
            ch_type = GUI->open_raw->ch_type;
            break;
        case 1: /* newdlg */
            spx = GUI->newdlg->spin_nx;
            spy = GUI->newdlg->spin_ny;
            spz = GUI->newdlg->spin_nz;
            off = 0;
            txt = GUI->newdlg->txt_dsize;
            ch_type = GUI->newdlg->ch_type;
            break;
    }

    nx = fl_get_spinner_value(spx);
    ny = fl_get_spinner_value(spy);
    nz = fl_get_spinner_value(spz);
    dsiz = nx*ny*nz;
    
    /* XXX: choice entries must be in same order as g3d_type_assoc table */
    switch(fl_get_choice(ch_type) - 1) {
                    case G3D_FLOAT32: fl_set_object_label_f(txt,"%"PRIuMAX,dsiz*sizeof(float)+off); break;
                   case G3D_FLOAT64: fl_set_object_label_f(txt,"%"PRIuMAX,dsiz*sizeof(double)+off); break;
                   case G3D_UINT8: fl_set_object_label_f(txt,"%"PRIuMAX,dsiz*sizeof(uint8_t)+off); break;
                   case G3D_UINT16: fl_set_object_label_f(txt,"%"PRIuMAX,dsiz*sizeof(uint16_t)+off); break;
                   case G3D_UINT32: fl_set_object_label_f(txt,"%"PRIuMAX,dsiz*sizeof(uint32_t)+off); break;
                   case G3D_UINT64: fl_set_object_label_f(txt,"%"PRIuMAX,dsiz*sizeof(uint64_t)+off); break;
                   case G3D_INT8: fl_set_object_label_f(txt,"%"PRIuMAX,dsiz*sizeof(int8_t)+off); break;
                   case G3D_INT16: fl_set_object_label_f(txt,"%"PRIuMAX,dsiz*sizeof(int16_t)+off); break;
                   case G3D_INT32: fl_set_object_label_f(txt,"%"PRIuMAX,dsiz*sizeof(int32_t)+off); break;
                   case G3D_INT64: fl_set_object_label_f(txt,"%"PRIuMAX,dsiz*sizeof(int64_t)+off); break;
           }
}

#undef ENV
xg3d_header* opendlg_show(xg3d_env *ENV) {
    FL_FORM *form = GUI->opendlg->opendlg;
    xg3d_header *xhdr = NULL;

    fl_deactivate_all_forms();

    fl_call_object_callback(GUI->open_raw->ch_type);
    fl_show_form_f(form,FL_PLACE_SIZE,FL_TRANSIENT,XG3D_TITLE_FORMAT,_("Load data"));
    fl_raise_form(form);
    fl_winfocus(form->window);

    FL_OBJECT *obj;
    do {

        obj = fl_do_only_forms();

        if(obj == GUI->opendlg->btn_open)
            switch(fl_get_active_folder_number(GUI->opendlg->tf_open)) {
                case 1: /* Header */
                    {
                        const char *file = fl_get_input(GUI->open_hdr->inp_file);
                        xg3d_header *vhdr = OPEN_HEADER(GUI->open_hdr);
                        if(strlen(file) == 0 || vhdr->file == NULL ) {
                            fl_show_alert2(0,_("No header file loaded!\fPlease select a header file."));
                            obj = NULL;
                            break;
                        }
                        xhdr = xg3d_header_dup(vhdr);
                        char *dir = strdup(file);
                        char buf[PATH_MAX];
                        snprintf(buf,sizeof(buf),"%s/%s",dirname(dir),basename(xhdr->file));
                        free(dir); free(xhdr->file);
                        xhdr->file = strdup(buf);
                    }
                    break;
                case 2: /* Raw */
                    {
                        const char *file = fl_get_input(GUI->open_raw->inp_file);
                        if(strlen(file) == 0) {
                            fl_show_alert2(0,_("No file selected!\fPlease select a file."));
                            obj = NULL;
                            break;
                        }
                        enum g3d_compr_type compr = fl_get_choice(GUI->open_raw->ch_compr)-1;
                        const char *fsize = fl_get_object_label(GUI->open_raw->txt_fsize);
                        const char *dsize = fl_get_object_label(GUI->open_raw->txt_dsize);
                        if(compr == COMPR_NONE && strtoul(fsize,NULL,10) < strtoul(dsize,NULL,10)) {
                            fl_show_alert2(0,_("Data larger than file!\fPlease check data dimensions and type."));
                            obj = NULL;
                            break;
                        }
                        xhdr = calloc(1,sizeof(xg3d_header));
                        xhdr->file = strdup(file);
                        xhdr->compr = compr;
                        xhdr->dim[0] = fl_get_spinner_value(GUI->open_raw->spin_nx);
                        xhdr->dim[1] = fl_get_spinner_value(GUI->open_raw->spin_ny);
                        xhdr->dim[2] = fl_get_spinner_value(GUI->open_raw->spin_nz);
                        xhdr->offset = (long)fl_get_spinner_value(GUI->open_raw->spin_off);
                        switch(fl_get_choice(GUI->open_raw->ch_type)) {
                            case  1: xhdr->type = RAW_FLOAT32; break; /* float 32-bit */
                            case  2: xhdr->type = RAW_FLOAT64; break; /* float 64-bit */
                            case  3: xhdr->type = RAW_UINT8;   break; /* unsigned 8-bit */
                            case  4: xhdr->type = RAW_UINT16;  break; /* unsigned 16-bit */
                            case  5: xhdr->type = RAW_UINT32;  break; /* unsigned 32-bit */
                            case  6: xhdr->type = RAW_UINT64;  break; /* unsigned 64-bit */
                            case  7: xhdr->type = RAW_INT8;    break; /* signed 8-bit */
                            case  8: xhdr->type = RAW_INT16;   break; /* signed 16-bit */
                            case  9: xhdr->type = RAW_INT32;   break; /* signed 32-bit */
                            case 10: xhdr->type = RAW_INT64;   break; /* signed 64-bit */
                        }
                        xhdr->endian = fl_get_button(GUI->open_raw->tog_le) ? ENDIAN_LITTLE : ENDIAN_BIG;
                        xhdr->vx = fl_get_spinner_value(GUI->open_raw->spin_vx);
                        xhdr->vy = fl_get_spinner_value(GUI->open_raw->spin_vy);
                        xhdr->vz = fl_get_spinner_value(GUI->open_raw->spin_vz);
                        xhdr->cal_slope = strtod(fl_get_input(GUI->open_raw->inp_slope),NULL);
                        xhdr->cal_inter = strtod(fl_get_input(GUI->open_raw->inp_inter),NULL);
                        xhdr->ox = xhdr->oy = xhdr->oz = 0;
                        xhdr->acqmod = ACQ_UNKNOWN;
                    }
                    break;
                case 3: /* Predef */
                    {
                        const char *file = fl_get_input(GUI->open_def->inp_file);
                        if(strlen(file) == 0) {
                            fl_show_alert2(0,_("No file selected!\fPlease select a file."));
                            obj = NULL;
                            break;
                        }
                        xhdr = calloc(1,sizeof(xg3d_header));
                        xhdr->ox = xhdr->oy = xhdr->oz = 0;
                        xhdr->file = strdup(file);
                        switch(fl_get_choice(GUI->open_def->ch_preset)) {
                            case 1: /* ALBIRA CT RAW */
                                {
                                    xhdr->compr = file_compr_type(file);

                                    /* TODO: support all types of compression */
                                    /* read CT header */
                                    znzFile zfp = znzopen(file,"rb",xhdr->compr > COMPR_NONE);
                                    if(znz_isnull(zfp)) {
                                        fl_show_alert2(0,_("Cannot open file!\f%s"),file);
                                        obj = NULL;
                                        break;
                                    }
                                    struct ALBIRA_ctheader cthdr;
                                    znzread(&cthdr.NProy,sizeof(uint16_t),1,zfp);
                                    znzread(&cthdr.Pixels_X,sizeof(uint16_t),1,zfp);
                                    znzread(&cthdr.Pixels_Z,sizeof(uint16_t),1,zfp);
                                    znzread(&cthdr.Bits,sizeof(uint16_t),1,zfp);
                                    znzread(&cthdr.detect_size,sizeof(double),1,zfp);
                                    znzread(&cthdr.focal,sizeof(double),1,zfp);
                                    znzread(&cthdr.R,sizeof(double),1,zfp);
                                    znzread(&cthdr.u_0,sizeof(double),1,zfp);
                                    znzread(&cthdr.v_0,sizeof(double),1,zfp);
                                    znzread(&cthdr.twist,sizeof(double),1,zfp);
                                    znzread(&cthdr.slant,sizeof(double),1,zfp);
                                    znzread(&cthdr.tilt,sizeof(double),1,zfp);
                                    znzread(&cthdr.current,sizeof(uint16_t),1,zfp);
                                    znzread(&cthdr.voltage,sizeof(uint16_t),1,zfp);
                                    znzclose(zfp);

                                    xhdr->endian = ENDIAN_LITTLE;
                                    xhdr->offset = 76; /* skip CT header */
                                    xhdr->type = RAW_UINT16; /* unsigned short */
                                    xhdr->dim[0] = cthdr.Pixels_X;
                                    xhdr->dim[1] = cthdr.Pixels_Z;
                                    xhdr->dim[2] = cthdr.NProy;
                                    xhdr->vx = xhdr->vy = cthdr.detect_size * MM_TO_CM;
                                    xhdr->vz = 1.0;
                                    xhdr->cal_slope = 1.0;
                                    xhdr->cal_inter = 0.0;
                                    xhdr->acqmod = ACQ_CT;
                                }
                                break;
                            case 2: /* ALBIRA SPECT BIN */
                                {
                                    /* read matrix header */
                                    FILE *fp = fopen(file,"rb");
                                    if(fp == NULL) {
                                        fl_show_alert2(0,_("Cannot open file!\f%s"),file);
                                        obj = NULL;
                                        break;
                                    }
                                    struct ALBIRA_mtxheader mtxhdr;
                                    uint8_t zero;
                                    if(fread(&mtxhdr.resx,sizeof(uint32_t),1,fp) != 1) {
                                        fl_show_alert2(0,_("Failed parsing file!\f%s"),file);
                                        obj = NULL;
                                        break;
                                    }
                                    if(fread(&mtxhdr.resy,sizeof(uint32_t),1,fp) != 1) {
                                        fl_show_alert2(0,_("Failed parsing file!\f%s"),file);
                                        obj = NULL;
                                        break;
                                    }
                                    if(fread(&zero,sizeof(uint8_t),1,fp) != 1) {
                                        fl_show_alert2(0,_("Failed parsing file!\f%s"),file);
                                        obj = NULL;
                                        break;
                                    }
                                    if(fread(&mtxhdr.fmt,sizeof(uint32_t),1,fp) != 1) {
                                        fl_show_alert2(0,_("Failed parsing file!\f%s"),file);
                                        obj = NULL;
                                        break;
                                    }
                                    /* get file size */
                                    if(fseeko(fp,0,SEEK_END) != 0) {
                                        perror("ERROR fseeko()");
                                        obj = NULL;
                                        break;
                                    }
                                    off_t fsiz = ftello(fp);
                                    if(fsiz == -1) {
                                        perror("ERROR ftello()");
                                        obj = NULL;
                                        break;
                                    }
                                    fclose(fp);

                                    if(mtxhdr.fmt != 75) {
                                        fl_show_alert2(0,_("Wrong type of file!\f%s"),file);
                                        obj = NULL;
                                        break;
                                    }

                                    xhdr->compr = COMPR_NONE;
                                    xhdr->endian = ENDIAN_LITTLE;
                                    xhdr->offset = 13;
                                    xhdr->type = RAW_FLOAT32;
                                    xhdr->dim[0] = mtxhdr.resx;
                                    xhdr->dim[1] = mtxhdr.resy;
                                    xhdr->dim[2] = (fsiz - xhdr->offset) / (sizeof(float) * xhdr->dim[0] * xhdr->dim[1]);
                                    xhdr->vx = xhdr->vy = xhdr->vz = 1.0;
                                    xhdr->cal_slope = 1.0;
                                    xhdr->cal_inter = 0.0;
                                    xhdr->acqmod = ACQ_SPECT;
                                }
                                break;
                        }
                        break;
                    }
            }
    
    } while(obj != GUI->opendlg->btn_cancel && obj != GUI->opendlg->btn_open);

    fl_hide_form(form);

    fl_activate_all_forms();

    return xhdr;
}
#define ENV _default_env

/* =======
   SAVEDLG
   ======= */

void cb_savedlg_fselect ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const char *file,*inp;
    char *dir,*base;
    size_t len;

    inp = fl_get_input(GUI->savedlg->inp_file);
    if(strlen(inp) > 0) { dir = strdup(inp); fl_set_directory(dirname(dir)); free(dir); }

    base = strdup(inp);
    file = fl_show_fselector(_("Save as..."),"","*",basename(base)); free(base);
    if(file == NULL) return;

    fl_set_input(GUI->savedlg->inp_file,file);
    fl_set_choice(GUI->savedlg->ch_compr,file_compr_type(file)+1);

    len = strlen(file);
    /* preset actions */
    if(strcmp(file + len - 4,".nii") == 0) { 
        fl_set_choice(GUI->savedlg->ch_format,1);
        fl_set_choice(GUI->savedlg->ch_compr,1);
    } else if (strcmp(file + len - 7,".nii.gz") == 0) {
        fl_set_choice(GUI->savedlg->ch_format,1);
        fl_set_choice(GUI->savedlg->ch_compr,2);
    } else if (strcmp(file + len - 5,".nrrd") == 0) {
        fl_set_choice(GUI->savedlg->ch_format,3);
        fl_set_choice(GUI->savedlg->ch_compr,1);
    } else if (strcmp(file + len - 4,".img") == 0)
        fl_set_choice(GUI->savedlg->ch_format,4);
    fl_call_object_callback(GUI->savedlg->ch_format);
}

void cb_savedlg_choices ( FL_OBJECT * obj  , long data  ) {
    if(data == 0) { /* format-specific setup */
        switch(fl_get_choice(obj)) {
            case 1: /* NIfTI-1 */
                fl_set_object_label(GUI->savedlg->txt_warn,_("NIfTI-1 header uses host byte order!"));
                /* data types */
                for(int i=1;i<=11;i++)
                    fl_set_choice_item_mode(GUI->savedlg->ch_type,i,FL_PUP_NONE);
                /* compression types */
                fl_set_choice_item_mode(GUI->savedlg->ch_compr,1,FL_PUP_NONE);
                fl_set_choice_item_mode(GUI->savedlg->ch_compr,2,FL_PUP_NONE);
                fl_set_choice_item_mode(GUI->savedlg->ch_compr,3,FL_PUP_GRAY);
                fl_set_choice_item_mode(GUI->savedlg->ch_compr,4,FL_PUP_GRAY);
                if(fl_get_choice(GUI->savedlg->ch_compr) == 3
                        || fl_get_choice(GUI->savedlg->ch_compr) == 4)
                    fl_set_choice(GUI->savedlg->ch_compr,2);
                break;
            case 2: /* µPET */
                fl_set_object_label(GUI->savedlg->txt_warn,_("µPET doesn't support all data types!"));
                /* data types */
                fl_set_choice_item_mode(GUI->savedlg->ch_type,2,FL_PUP_NONE);
                for(int i=3;i<=7;i++)
                    fl_set_choice_item_mode(GUI->savedlg->ch_type,i,FL_PUP_GRAY);
                for(int i=8;i<=10;i++)
                    fl_set_choice_item_mode(GUI->savedlg->ch_type,i,FL_PUP_NONE);
                fl_set_choice_item_mode(GUI->savedlg->ch_type,11,FL_PUP_GRAY);
                {
                    const xg3d_header *hdr = DATA->hdr;
                    if(!(hdr->type == RAW_INT8 || hdr->type == RAW_INT16 ||
                                hdr->type == RAW_INT32 || hdr->type == RAW_FLOAT32)) {
                        fl_set_choice(GUI->savedlg->ch_type,2);
                        fl_set_choice_item_mode(GUI->savedlg->ch_type,1,FL_PUP_GRAY);
                    } else {
                        fl_set_choice(GUI->savedlg->ch_type,1);
                        fl_set_choice_item_mode(GUI->savedlg->ch_type,1,FL_PUP_NONE);
                    }
                }
                /* compression types */
                fl_set_choice_item_mode(GUI->savedlg->ch_compr,1,FL_PUP_NONE);
                for(int i=2;i<=4;i++)
                    fl_set_choice_item_mode(GUI->savedlg->ch_compr,i,FL_PUP_GRAY);
                fl_set_choice(GUI->savedlg->ch_compr,1);
                break;
            case 3: /* Nrrd */
                fl_set_object_label(GUI->savedlg->txt_warn,_("Nrrd supports everything!"));
                /* data types */
                fl_set_choice_item_mode(GUI->savedlg->ch_type,1,FL_PUP_NONE);
                fl_set_choice(GUI->savedlg->ch_type,1);
                for(int i=2;i<=11;i++)
                    fl_set_choice_item_mode(GUI->savedlg->ch_type,i,FL_PUP_GRAY);
                /* compression types */
                fl_set_choice_item_mode(GUI->savedlg->ch_compr,1,FL_PUP_NONE);
                fl_set_choice_item_mode(GUI->savedlg->ch_compr,2,FL_PUP_NONE);
                fl_set_choice_item_mode(GUI->savedlg->ch_compr,3,FL_PUP_GRAY);
                fl_set_choice_item_mode(GUI->savedlg->ch_compr,4,FL_PUP_GRAY);
                if(fl_get_choice(GUI->savedlg->ch_compr) == 3
                        || fl_get_choice(GUI->savedlg->ch_compr) == 4)
                    fl_set_choice(GUI->savedlg->ch_compr,2);
                break;
            case 4: /* RAW */
                fl_set_object_label(GUI->savedlg->txt_warn,_("RAW has no header!"));
                /* data types */
                for(int i=1;i<=11;i++)
                    fl_set_choice_item_mode(GUI->savedlg->ch_type,i,FL_PUP_NONE);
                /* compression types */
                for(int i=1;i<=4;i++)
                    fl_set_choice_item_mode(GUI->savedlg->ch_compr,i,FL_PUP_NONE);
                break;
        }
    }

    { /* modify filename */
        static const char *ext_f[] = { ".nii", ".img", ".nrrd", ".img" };
        static const char *ext_z[] = { "", ".gz", ".bz2", ".xz" };

        char *inp = strdup(fl_get_input(GUI->savedlg->inp_file));
        { /* remove extension */
            char *last_dot = strrchr(inp,'.');
            if(last_dot != NULL) *last_dot = '\0';
        }

        const int fmt = fl_get_choice(GUI->savedlg->ch_format);
        int cmz = fl_get_choice(GUI->savedlg->ch_compr);
        if(fmt == 3) cmz = 1; // do not append .gz to .nrrd
        char buf[PATH_MAX];
        snprintf(buf,sizeof(buf),"%s%s%s",inp,ext_f[fmt - 1],ext_z[cmz - 1]);
        fl_set_input(GUI->savedlg->inp_file,buf);
        free(inp);
    }
}

#undef ENV
void savedlg_show(FD_savedlg *savedlg, const xg3d_data *data, const char *name) {
    FL_FORM *form = savedlg->savedlg;

    if(data == NULL) return; /* null input */

    fl_deactivate_all_forms();

    fl_set_input(savedlg->inp_file,name);
    fl_call_object_callback(savedlg->ch_format);
    fl_show_form_f(form,FL_PLACE_SIZE,FL_TRANSIENT,XG3D_TITLE_FORMAT,_("Save data"));
    fl_raise_form(form);
    fl_winfocus(form->window);

    FL_OBJECT *obj;
    do {

        obj = fl_do_only_forms();

        if(obj == savedlg->btn_save) {

            xg3d_header xhdr;
            xhdr.vx = data->hdr->vx; xhdr.vy = data->hdr->vy; xhdr.vz = data->hdr->vz;
            xhdr.ox = data->hdr->ox; xhdr.oy = data->hdr->oy; xhdr.oz = data->hdr->oz;

            const char *file = fl_get_input(savedlg->inp_file);
            if(file != NULL && strlen(file) > 0) {
                /* existing file check */
                FILE *fp = fopen(file,"r");
                if(fp == NULL || (fp != NULL && fl_show_question(_("File exists, replace?"),0))) {
                    if(fp != NULL) fclose(fp);
                    fl_show_oneliner(_("Exporting data..."),fl_scrw/2,fl_scrh/2);
                    xhdr.file = strdup(file);
                    xhdr.dim[0] = data->g3d->nx;
                    xhdr.dim[1] = data->g3d->ny;
                    xhdr.dim[2] = data->g3d->nz;
                    xhdr.cal_slope = data->hdr->cal_slope;
                    xhdr.cal_inter = data->hdr->cal_inter;
                    xhdr.offset = 0;
                    xhdr.acqmod = data->hdr->acqmod;
                    xhdr.endian = fl_get_button(savedlg->tog_le) ? ENDIAN_LITTLE : ENDIAN_BIG;
                    switch(fl_get_choice(savedlg->ch_type)) {
                        case  1: xhdr.type = g3d_type_to_raw_io_type(data->g3d->type); break;
                        case  2: xhdr.type = RAW_FLOAT32; break;
                        case  3: xhdr.type = RAW_FLOAT64; break;
                        case  4: xhdr.type = RAW_UINT8;   break;
                        case  5: xhdr.type = RAW_UINT16;  break;
                        case  6: xhdr.type = RAW_UINT32;  break;
                        case  7: xhdr.type = RAW_UINT64;  break;
                        case  8: xhdr.type = RAW_INT8;    break;
                        case  9: xhdr.type = RAW_INT16;   break;
                        case 10: xhdr.type = RAW_INT32;   break;
                        case 11: xhdr.type = RAW_INT64;   break;
                    }
                    xhdr.compr = fl_get_choice(savedlg->ch_compr) - 1;
                    switch(fl_get_choice(savedlg->ch_format)) {
                        case 1: /* NIfTI-1 */
                            {
                                nifti_image *nim = xg3d_to_nifti(data->g3d,&xhdr);
                                znzFile fp = nifti_image_write_hdr_img(nim,3,"wb");
                                if(fp == NULL)
                                    fl_show_alert2(0,_("Failed saving data!\fFILE: %s\nERROR: %s"),file,_("See stderr for error information"));
                                else
                                    znzclose(fp);
                                nim->data = NULL; nifti_image_free(nim);
                            }
                            break;
                        case 2: /* µPET */
                            {
                                g3d_ret ret;
                                if((ret = g3d_output_raw(data->g3d,xhdr.file,xhdr.type,xhdr.compr,xhdr.endian,.append=false)) != G3D_OK)
                                    fl_show_alert2(0,_("Failed saving data!\fFILE: %s\nERROR: %s"),file,g3d_ret_msg(ret));
                                xhdr.type = raw_io_type_to_mupet(xhdr.type,xhdr.endian);
                                char buf[PATH_MAX];
                                snprintf(buf,sizeof(buf),"%s.hdr",file);
                                export_mupet_header(buf,&xhdr);
                            }
                            break;
                        case 3: /* Nrrd */
                            {
                                Nrrd *nin = xg3d_to_nrrd(data->g3d,&xhdr);
                                NrrdIoState *nio = nrrdIoStateNew();
                                nrrdIoStateFormatSet(nio,nrrdFormatNRRD);
                                switch(xhdr.compr) {
                                    default:
                                    case COMPR_NONE: nrrdIoStateEncodingSet(nio,nrrdEncodingRaw); break;
                                    case COMPR_GZIP: nrrdIoStateEncodingSet(nio,nrrdEncodingGzip); break;
                                }
                                nio->endian = (xhdr.endian == ENDIAN_BIG) ? airEndianBig : airEndianLittle;
                                if(nrrdSave(xhdr.file,nin,nio)) {
                                    char *err = biffGetDone(NRRD);
                                    fl_show_alert2(0,_("Failed saving data!\fFILE: %s\nERROR: %s"),file,err);
                                    free(err);
                                }
                                nrrdIoStateNix(nio);
                                nin->data = NULL; nrrdNix(nin);
                            }
                            break;
                        case 4: /* RAW */
                            {
                                g3d_ret ret;
                                if((ret = g3d_output_raw(data->g3d,xhdr.file,xhdr.type,xhdr.compr,xhdr.endian,.append=false)) != G3D_OK)
                                    fl_show_alert2(0,_("Failed saving data!\fFILE: %s\nERROR: %s"),file,g3d_ret_msg(ret));
                            }
                            break;
                    }
                    free(xhdr.file);
                    fl_hide_oneliner();
                } else {
                    fclose(fp);
                    obj = NULL;
                }
            } else {
                fl_show_alert2(0,_("No file selected!\fPlease select a file."));
                obj = NULL;
            }
        }

    } while(obj != savedlg->btn_cancel && obj != savedlg->btn_save);

    fl_hide_form(savedlg->savedlg);

    fl_activate_all_forms();
}
#define ENV _default_env

/* ======
   NEWDLG
   ====== */

#undef ENV
xg3d_header* newdlg_show(xg3d_env *ENV, const char **fun_o, int *center_o) {
    FL_FORM *form = GUI->newdlg->newdlg;
    xg3d_header *xhdr = NULL;

    fl_deactivate_all_forms();

    fl_call_object_callback(GUI->newdlg->ch_type);
    fl_show_form_f(form,FL_PLACE_SIZE,FL_TRANSIENT,XG3D_TITLE_FORMAT,_("New data"));
    fl_raise_form(form);
    fl_winfocus(form->window);

    FL_OBJECT *obj;
    do {

        obj = fl_do_only_forms();

        if(obj == GUI->newdlg->btn_new) {

            const char *fun = NULL;
            void *f = NULL;

            if(!fl_get_button(GUI->newdlg->btn_fun)
                     || (fl_get_button(GUI->newdlg->btn_fun)
                         && (fun = fl_get_input(GUI->newdlg->inp_fun))
                         && (f = evaluator_create((char*)fun)) != NULL)) {
                if(f != NULL) evaluator_destroy(f);
                xhdr = calloc(1,sizeof(xg3d_header));
                xhdr->file = strdup(_("INTERNAL"));
                xhdr->compr = COMPR_NONE;
                xhdr->dim[0] = fl_get_spinner_value(GUI->newdlg->spin_nx);
                xhdr->dim[1] = fl_get_spinner_value(GUI->newdlg->spin_ny);
                xhdr->dim[2] = fl_get_spinner_value(GUI->newdlg->spin_nz);
                xhdr->offset = 0;
                switch(fl_get_choice(GUI->newdlg->ch_type)) {
                    case  1: xhdr->type = RAW_FLOAT32; break; /* float 32-bit */
                    case  2: xhdr->type = RAW_FLOAT64; break; /* float 64-bit */
                    case  3: xhdr->type = RAW_UINT8;   break; /* unsigned 8-bit */
                    case  4: xhdr->type = RAW_UINT16;  break; /* unsigned 16-bit */
                    case  5: xhdr->type = RAW_UINT32;  break; /* unsigned 32-bit */
                    case  6: xhdr->type = RAW_UINT64;  break; /* unsigned 64-bit */
                    case  7: xhdr->type = RAW_INT8;    break; /* signed 8-bit */
                    case  8: xhdr->type = RAW_INT16;   break; /* signed 16-bit */
                    case  9: xhdr->type = RAW_INT32;   break; /* signed 32-bit */
                    case 10: xhdr->type = RAW_INT64;   break; /* signed 64-bit */
                }
                xhdr->endian = IS_BIG_ENDIAN ? ENDIAN_BIG : ENDIAN_LITTLE;
                xhdr->vx = fl_get_spinner_value(GUI->newdlg->spin_vx);
                xhdr->vy = fl_get_spinner_value(GUI->newdlg->spin_vy);
                xhdr->vz = fl_get_spinner_value(GUI->newdlg->spin_vz);
                xhdr->cal_slope = 1.0; xhdr->cal_inter = 0.0;
                xhdr->ox = xhdr->oy = xhdr->oz = 0;
                xhdr->acqmod = ACQ_UNKNOWN;
            } else {
                fl_show_alert2(0,"Invalid input!\fPlease enter a valid function.");
                obj = NULL;
            }

            if(center_o != NULL) *center_o = fl_get_button(GUI->newdlg->btn_fun_center);
            if(fun_o != NULL) *fun_o = fun;
        }

    } while(obj != GUI->newdlg->btn_cancel && obj != GUI->newdlg->btn_new);

    fl_hide_form(GUI->newdlg->newdlg);

    fl_activate_all_forms();

    return xhdr;
}
#define ENV _default_env
