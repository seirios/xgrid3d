#include "grid3d.h"
#include "grid3d_roi.h"

enum roi_boolop {
    ROI_BOOLOP_OR = 0,
    ROI_BOOLOP_AND,
    ROI_BOOLOP_XOR
};

g3d_ret rois_boolop(const grid3d*,const g3d_roi_list*,const enum roi_boolop,g3d_roi**);
g3d_ret rois_select(const g3d_roi_list*,grid3d*,const bool);
g3d_ret rois_mss(const g3d_roi*,g3d_roi**);
