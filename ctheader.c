#include "ctheader.h"

int ctheader_read(const char *file, CT_FILE_HEADER *cthdr) {
    gzFile zfp;

    zfp = gzopen(file,"rb");
    if(zfp == NULL) return 1;
    if(gzfread(cthdr,sizeof(CT_FILE_HEADER),1,zfp) != 1) return 1;
    gzclose(zfp);

    return 0;
}
