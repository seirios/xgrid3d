/* Form definition file generated by fdesign on Sat Sep 28 21:14:16 2024 */

#include <stdlib.h>
#include "fd_viewport.h"

#ifdef ENABLE_NLS
#include <libintl.h>
#define _(String) gettext (String)
#else
#define _(String) (String)
#endif


/***************************************
 ***************************************/

FD_viewport *
create_form_viewport( void )
{
    FL_OBJECT *obj;
    FD_viewport *fdui = ( FD_viewport * ) fl_malloc( sizeof *fdui );

    fdui->vdata = fdui->cdata = NULL;
    fdui->ldata = 0;

    fdui->viewport = fl_bgn_form( FL_NO_BOX, 230, 230 );

    obj = fl_add_box( FL_FLAT_BOX, 0, 0, 230, 230, "" );

    fdui->canvas = obj = fl_add_canvas( FL_NORMAL_CANVAS, 15, 15, 200, 200, "" );
    fl_set_object_resize( obj, FL_RESIZE_NONE );

    fdui->pos_left = obj = fl_add_bitmap( FL_NORMAL_BITMAP, 0, 15, 15, 200, "" );
    fl_set_object_lcolor( obj, FL_YELLOW );
    fl_set_object_resize( obj, FL_RESIZE_NONE );

    fdui->pos_right = obj = fl_add_bitmap( FL_NORMAL_BITMAP, 215, 15, 15, 200, "" );
    fl_set_object_lcolor( obj, FL_YELLOW );
    fl_set_object_resize( obj, FL_RESIZE_NONE );

    fdui->pos_top = obj = fl_add_bitmap( FL_NORMAL_BITMAP, 15, 0, 200, 15, "" );
    fl_set_object_lcolor( obj, FL_YELLOW );
    fl_set_object_resize( obj, FL_RESIZE_NONE );

    fdui->pos_bottom = obj = fl_add_bitmap( FL_NORMAL_BITMAP, 15, 215, 200, 15, "" );
    fl_set_object_lcolor( obj, FL_YELLOW );
    fl_set_object_resize( obj, FL_RESIZE_NONE );

    fl_end_form( );

    fdui->viewport->fdui = fdui;

    return fdui;
}
