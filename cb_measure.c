#include <libgen.h>

#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif

#include "cb_measure.h"
#include "cb_viewport.h"
#include "cb_mainview.h"
#include "grid3d_resamp.h"

#include <gsl/gsl_statistics_float.h>
#include <gsl/gsl_sort_float.h>
#include <gsl/gsl_math.h>

#include "dl.h"
#include "util.h"
#include "util_units.h"
#include "util_coordinates.h"

#define FORMAT "%8.6g"

void update_measurement(FD_measure *measure, FD_viewport *view) {
    if(!fl_form_is_visible(measure->measure)) return;

    double vx,vy,vz;
    switch(fl_get_choice(measure->ch_ulen)) {
        case 1: /* vox */
            vx = vy = vz = 1.0;
            break;
        default:
        case 2: /* cm */
            {
                const xg3d_data *data = VIEW_DATA(view);
                vx = data->hdr->vx;
                vy = data->hdr->vy;
                vz = data->hdr->vz;
            }
            break;
        case 3: /* mm */
            {
                const xg3d_data *data = VIEW_DATA(view);
                vx = data->hdr->vx * CM_TO_MM;
                vy = data->hdr->vy * CM_TO_MM;
                vz = data->hdr->vz * CM_TO_MM;
            }
            break;
    }

    const xg3d_view_workspace *vwk = VIEW_WORKSPACE(view);
    const coord_t spj = vwk->sp[0];
    const coord_t spi = vwk->sp[1];
    const coord_t spk = vwk->sp[2];
    const coord_t epj = vwk->ep[0];
    const coord_t epi = vwk->ep[1];
    const coord_t epk = vwk->ep[2];

    if(vwk->spset && vwk->epset) { /* both points set */
        int dx = FL_abs(spj-epj);
        int dy = FL_abs(spi-epi);
        int dz = FL_abs(spk-epk);
        double d = gsl_hypot3(dx*vx,dy*vy,dz*vz);
        fl_set_object_label_f(measure->txt_r,FORMAT,d);
        if(fl_get_button(measure->btn_line)) { /* Line */
            fl_set_object_label_f(measure->txt_span,"(%u,%u,%u) ⟶ (%u,%u,%u)",
                    spj,spi,spk,epj,epi,epk);
            fl_set_object_label_f(measure->txt_dx,FORMAT,dx*vx);
            fl_set_object_label_f(measure->txt_dy,FORMAT,dy*vy);
            fl_set_object_label_f(measure->txt_dz,FORMAT,dz*vz);
            double r = gsl_hypot3(dx,dy,dz);
            double rad_to_deg = 180.0 * M_1_PI;
            fl_set_object_label_f(measure->txt_ax,FORMAT,acos(dx/r) * rad_to_deg);
            fl_set_object_label_f(measure->txt_ay,FORMAT,acos(dy/r) * rad_to_deg);
            fl_set_object_label_f(measure->txt_az,FORMAT,acos(dz/r) * rad_to_deg);
        } else { /* Circle */
            fl_set_object_label_f(measure->txt_span,"(%u,%u,%u) ⭯ (%u,%u,%u)",
                    spj,spi,spk,epj,epi,epk);
            fl_set_object_label_f(measure->txt_dx,FORMAT,M_PI * d);
            fl_set_object_label_f(measure->txt_dy,FORMAT,M_PI * d * d / 4.0);
            fl_set_object_label(measure->txt_dz,"");
            fl_set_object_label(measure->txt_ax,"");
            fl_set_object_label(measure->txt_ay,"");
            fl_set_object_label(measure->txt_az,"");
        }
    } else {
        fl_set_object_label(measure->txt_r,"");
        fl_set_object_label(measure->txt_dx,"");
        fl_set_object_label(measure->txt_dy,"");
        fl_set_object_label(measure->txt_dz,"");
        fl_set_object_label(measure->txt_ax,"");
        fl_set_object_label(measure->txt_ay,"");
        fl_set_object_label(measure->txt_az,"");
    }
}

void update_measurement_live(FD_measure *measure, FD_viewport *view, const coord_t j, const coord_t i, const coord_t k) {
    (void)measure;
(void)view;
(void)j;
(void)i;
(void)k;
    /* do nothing */ return;

    /*const xg3d_view_workspace *vwk = VIEW_WORKSPACE(view);*/
    /*const xg3d_data *data = VIEW_DATA(view);*/
    /*const coord_t spj = vwk->sp[0];*/
    /*const coord_t spi = vwk->sp[1];*/
    /*const coord_t spk = vwk->sp[2];*/
    /*const coord_t epj = vwk->ep[0];*/
    /*const coord_t epi = vwk->ep[1];*/
    /*const coord_t epk = vwk->ep[2];*/
    /*const int spset = vwk->spset;*/
    /*const int epset = vwk->epset;*/
    /*coord_t dj,di,dk;*/
    /*double vx,vy,vz;*/
    /*coord_t vox[3];*/
    /*int dx,dy,dz;*/

    /*if(!fl_form_is_visible(measure->measure)) return;*/

    /*switch(fl_get_choice(measure->ch_ulen)) {*/
        /*case 1: [> vox <]*/
            /*vx = vy = vz = 1.0;*/
            /*break;*/
        /*default:*/
        /*case 2: [> cm <]*/
            /*vx = data->hdr->vx;*/
            /*vy = data->hdr->vy;*/
            /*vz = data->hdr->vz;*/
            /*break;*/
        /*case 3: [> mm <]*/
            /*vx = data->hdr->vx * CM_TO_MM;*/
            /*vy = data->hdr->vy * CM_TO_MM;*/
            /*vz = data->hdr->vz * CM_TO_MM;*/
            /*break;*/
    /*}*/

    /*vox[0] = j; vox[1] = i; vox[2] = k;*/
    /*dj = vox[PLANE_TO_J(vwk->plane)];*/
    /*di = vox[PLANE_TO_I(vwk->plane)];*/
    /*dk = vox[PLANE_TO_K(vwk->plane)];*/

    /*if(spset || epset) {*/
        /*if(spset && epset) { [> startpoint to endpoint <]*/
            /*dx = FL_abs(spj-epj);*/
            /*dy = FL_abs(spi-epi);*/
            /*dz = FL_abs(spk-epk);*/
        /*} else if(!epset) { [> startpoint to current <]*/
            /*dx = FL_abs(spj-dj);*/
            /*dy = FL_abs(spi-di);*/
            /*dz = FL_abs(spk-dk);*/
        /*} else { [> current to endpoint <]*/
            /*dx = FL_abs(dj-epj);*/
            /*dy = FL_abs(di-epi);*/
            /*dz = FL_abs(dk-epk);*/
        /*}*/
        /*fl_set_object_label_f(measure->txt_dx,"%6.4g",dx*vx);*/
        /*fl_set_object_label_f(measure->txt_dy,"%6.4g",dy*vy);*/
        /*fl_set_object_label_f(measure->txt_dz,"%6.4g",dz*vz);*/
        /*fl_set_object_label_f(measure->txt_r,"%6.4g",gsl_hypot3(dx*vx,dy*vy,dz*vz));*/
        /*fl_set_object_label_f(measure->txt_ax,"%6.4g°",acos(dx/gsl_hypot3(dx,dy,dz))*180.0/M_PI);*/
        /*fl_set_object_label_f(measure->txt_ay,"%6.4g°",acos(dy/gsl_hypot3(dx,dy,dz))*180.0/M_PI);*/
        /*fl_set_object_label_f(measure->txt_az,"%6.4g°",acos(dz/gsl_hypot3(dx,dy,dz))*180.0/M_PI);*/
    /*}*/
}

void update_line_profile_plot(FD_measure *measure, FD_viewport *view) {
    const xg3d_view_workspace *vwk = VIEW_WORKSPACE(view);
    const int flag_calib = PROFILE_GET_CALIB(measure);
    const double *profile = PROFILE_VALUES(measure);
    const index_t npp = PROFILE_NPOINTS(measure);
    const xg3d_data *data = VIEW_DATA(view);
    const coord_t spj = vwk->sp[0];
    const coord_t spi = vwk->sp[1];
    const coord_t spk = vwk->sp[2];
    const coord_t epj = vwk->ep[0];
    const coord_t epi = vwk->ep[1];
    const coord_t epk = vwk->ep[2];

    if(!vwk->spset || !vwk->epset) return;

    fl_clear_xyplot(measure->plot_profile);
    float *x = malloc(npp*sizeof(float));
    float *y = malloc(npp*sizeof(float));
    for(index_t i=0;i<npp;i++) {
        x[i] = i;
        y[i] = flag_calib ? CALIBVAL(data->hdr,profile[i]) : profile[i];
    }

    /* compute statistics */
    float *ysort = malloc(npp*sizeof(float));
    memcpy(ysort,y,npp*sizeof(float));
    gsl_sort_float(ysort,1,npp);
    float min,max;
    gsl_stats_float_minmax(&min,&max,ysort,1,npp);
    float avg = gsl_stats_float_mean(ysort,1,npp);
    float sd = gsl_stats_float_sd_m(ysort,1,npp,avg);
    float med = gsl_stats_float_median_from_sorted_data(ysort,1,npp);

    /* trapezoid rule integral */
    /* TODO: use GSL numerical integration? */
    float dt = gsl_hypot3((float)spj-epj,(float)spi-epi,(float)spk-epk) / (float)(npp - 1);
    float tot = 0.0;
    for(index_t i=0;i<npp-1;i++)
        tot += dt * 0.5 * (y[i] + y[i+1]);
    free(ysort);

    fl_set_object_label_f(measure->txt_min,"%.12g",min);
    fl_set_object_label_f(measure->txt_max,"%.12g",max);
    fl_set_object_label_f(measure->txt_mean,"%.12g",avg);
    fl_set_object_label_f(measure->txt_sdev,"%.12g",sd);
    fl_set_object_label_f(measure->txt_median,"%.12g",med);
    fl_set_object_label_f(measure->txt_total,"%.12g",tot);

    if(min <= 0.0) {
        fl_set_button(measure->btn_log,0);
        fl_set_xyplot_yscale(measure->plot_profile,FL_LINEAR,0.0);
    }

    fl_set_xyplot_data(measure->plot_profile,x,y,npp,"","","");
    free(x); free(y);
}

static void _profile_count(enum xg3d_sel_op_mode op FL_UNUSED_ARG, void *p, const coord_t j FL_UNUSED_ARG, const coord_t i FL_UNUSED_ARG, const coord_t k FL_UNUSED_ARG) {
    (*((index_t*)p))++;
}

static void _profile_get(enum xg3d_sel_op_mode op FL_UNUSED_ARG, void *p, const coord_t j, const coord_t i, const coord_t k) {
    const grid3d *data = (grid3d*)(((void**)p)[0]);
    double *profile = (double*)(((void**)p)[1]);
    index_t *npp = (index_t*)(((void**)p)[2]);

    const index_t ix = (index_t)j + (index_t)i * data->nx + (index_t)k * data->nxy;
    profile[(*npp)++] = g3d_get_dvalue(data,ix);
}

void update_line_profile(FD_measure *measure, FD_viewport *view) {
    const xg3d_view_workspace *vwk = VIEW_WORKSPACE(view);

    if(!vwk->spset || !vwk->epset) return;

    size_t n = 0;
    double *profile = NULL;

    if(fl_get_button(measure->btn_line)) { /* Line */
        const xg3d_data *data = VIEW_DATA(view);

        const coord_t spj = vwk->sp[0];
        const coord_t spi = vwk->sp[1];
        const coord_t spk = vwk->sp[2];
        const coord_t epj = vwk->ep[0];
        const coord_t epi = vwk->ep[1];
        const coord_t epk = vwk->ep[2];

        if(fl_get_button(measure->btn_interp)) { // interpolate
            const double dpj = epj - spj;
            const double dpi = epi - spi;
            const double dpk = epk - spk;
            const double d = gsl_hypot3(dpj,dpi,dpk);

            n = 2 * d; /* approx. every half pixel */
            profile = malloc(n * sizeof(double));

            const double dt = 1.0 / (double)(n - 1);
            const double sj = 0.5 + spj;
            const double si = 0.5 + spi;
            const double sk = 0.5 + spk;

            double t = 0.0;
            for(size_t i=0;i<n;i++) {
                profile[i] = trilinear_interp(data->g3d,
                        sj + t * dpj,
                        si + t * dpi,
                        sk + t * dpk,
                        0.0);
                t += dt;
            }
        } else { // do not interpolate, get values along a discrete 3d line
            n = 0; line3d(spj,spi,spk,epj,epi,epk,_profile_count,0,&n);
            void *p[3] = {data->g3d,NULL,&n};
            profile = p[1] = malloc(n * sizeof(double));
            n = 0; line3d(spj,spi,spk,epj,epi,epk,_profile_get,0,p);
        }
    }
    
    /* TODO: add non-interpolated circle? */
    else { /* Circle */
        const grid3d *slice = VIEW_SLICE(view);

        const coord_t spj = vwk->sp[PLANE_TO_J(vwk->plane)];
        const coord_t spi = vwk->sp[PLANE_TO_I(vwk->plane)];
        const coord_t epj = vwk->ep[PLANE_TO_J(vwk->plane)];
        const coord_t epi = vwk->ep[PLANE_TO_I(vwk->plane)];
        
        const double dpj = epj - spj;
        const double dpi = epi - spi;
        const double d = hypot(dpj,dpi);

        n = 3 * M_PI * d; /* approx. every third of pixel */
        profile = malloc(n * sizeof(double));

        const double dt = 1.0 / (double)(n - 1);
        const double cx = 0.5 * (1.0 + spj + epj);
        const double cy = 0.5 * (1.0 + spi + epi);

        double t = 0.5 * atan2(cy-(0.5+spi),(0.5+spj)-cx) * M_1_PI; /* in [-0.5,0.5] */
        for(size_t i=0;i<n;i++) {
            profile[i] = bilinear_interp(slice,
                    cx + 0.5 * d * cos(2.0 * M_PI * t), /* CCW turn */
                    cy - 0.5 * d * sin(2.0 * M_PI * t), /* inverted y axis */
                    0.0);
            t += dt;
        }
    }

    PROFILE_NPOINTS(measure) = n;
    free(PROFILE_VALUES(measure));
    PROFILE_VALUES(measure) = profile;

    update_line_profile_plot(measure,view);
}

void cb_measure_mode ( FL_OBJECT * obj  , long data  ) {
    const xg3d_view_workspace *vwk = VIEW_WORKSPACE(GUI->viewport);

    switch(data) {
        case 0: /* Line */
            fl_set_object_label(GUI->measure->box_title,_("Line profile"));
            fl_set_object_label(GUI->measure->box_d,"d:");
            fl_set_object_label(GUI->measure->box_dx,"∆x:");
            fl_set_object_label(GUI->measure->box_dy,"∆y:");
            fl_set_object_label(GUI->measure->box_dz,"∆z:");
            fl_set_object_label(GUI->measure->box_ax,"∡x:");
            fl_set_object_label(GUI->measure->box_ay,"∡y:");
            fl_set_object_label(GUI->measure->box_az,"∡z:");
            activate_obj(GUI->measure->btn_interp,FL_YELLOW);

            if(vwk->spset && vwk->epset) {
                update_line_profile(GUI->measure,GUI->viewport);
                refresh_canvas(ENV);
            }
            break;
        case 1: /* Circle */
            fl_set_object_label(GUI->measure->box_title,_("Circular line profile"));
            fl_set_object_label(GUI->measure->box_d,"⌀:");
            fl_set_object_label(GUI->measure->box_dx,"◯:");
            fl_set_object_label(GUI->measure->box_dy,"⬤:");
            fl_set_object_label(GUI->measure->box_dz,"");
            fl_set_object_label(GUI->measure->box_ax,"");
            fl_set_object_label(GUI->measure->box_ay,"");
            fl_set_object_label(GUI->measure->box_az,"");
            deactivate_obj(GUI->measure->btn_interp);

            if(vwk->spset && vwk->epset) { /* both points set */
                if(vwk->sp[PLANE_TO_K(vwk->plane)] != vwk->ep[PLANE_TO_K(vwk->plane)]) {
                    fl_call_object_callback(GUI->measure->btn_clear);
                    return;
                } else { /* valid circle */
                    update_line_profile(GUI->measure,GUI->viewport);
                    refresh_canvas(ENV);
                }
            }
            break;
    }
    update_measurement(GUI->measure,GUI->viewport);
}

void cb_measure_units ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    update_measurement(GUI->measure,GUI->viewport);
}

void cb_profile_units ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    fl_set_object_label(obj,fl_get_button(obj) ? "CAL" : "DAT");
    update_line_profile_plot(GUI->measure,GUI->viewport);
}

void cb_profile_yaxis ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const int yt = fl_get_button(obj);
    fl_set_xyplot_ytics(GUI->measure->plot_profile,yt ? 5 : -1,yt ? 2 : -1);
}

/* TODO: make it work even for negative data (log1p or something...) */
void cb_profile_log ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    if(fl_get_button(obj)) {
        if(atof(fl_get_object_label(GUI->measure->txt_min)) > 0.0)
            fl_set_xyplot_yscale(GUI->measure->plot_profile,FL_LOG,10.0);
        else
            fl_set_button(obj,0);
    } else
        fl_set_xyplot_yscale(GUI->measure->plot_profile,FL_LINEAR,0.0);
}

void cb_profile_interp ( FL_OBJECT * obj  , long data  ) {
    (void)data;

    if(VIEW_SPSET(GUI->viewport) && VIEW_EPSET(GUI->viewport)) {
        update_line_profile(GUI->measure,GUI->viewport);
        update_measurement(GUI->measure,GUI->viewport);
    }
}

void cb_profile_reverse ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_view_workspace *vwk = VIEW_WORKSPACE(GUI->viewport);

    if(!vwk->spset || !vwk->epset) return; // both must be set

    coord_t tmp;
    tmp = vwk->sp[0]; vwk->sp[0] = vwk->ep[0]; vwk->ep[0] = tmp;
    tmp = vwk->sp[1]; vwk->sp[1] = vwk->ep[1]; vwk->ep[1] = tmp;
    tmp = vwk->sp[2]; vwk->sp[2] = vwk->ep[2]; vwk->ep[2] = tmp;

    update_line_profile(GUI->measure,GUI->viewport);
}

void cb_profile_export ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    char buf[PATH_MAX],*tmp;
    FLPS_CONTROL *flpsc;

    const char *file = fl_show_fselector(_("Profile basename..."),"","*","");
    if(file != NULL) {
        char *base = strdup(file); tmp = strdup(basename(base)); free(base); base = tmp;
        char *dir = strdup(file); tmp = strdup(dirname(dir)); free(dir); dir = tmp;

        /* save plot */
        snprintf(buf,sizeof(buf),"%s/%s_plot.eps",dir,base);
        flpsc = flps_init(); flpsc->orientation = FLPS_LANDSCAPE; flpsc->eps = 1;
        fl_object_ps_dump(GUI->measure->plot_profile,buf);

        /* save data */
        snprintf(buf,sizeof(buf),"%s/%s_data.txt",dir,base);
        FILE *fp = fopen(buf,"w");
        float *x,*y; int n;
        fl_get_xyplot_data_pointer(GUI->measure->plot_profile,0,&x,&y,&n);
        for(int i=0;i<n;i++)
            fprintf(fp,"%g %g\n",x[i],y[i]);
        fclose(fp);

        /* save stats */
        snprintf(buf,sizeof(buf),"%s/%s_stat.txt",dir,base);
        fp = fopen(buf,"w");
        const int flag_calib = PROFILE_GET_CALIB(GUI->measure);
        snprintf(buf,sizeof(buf),flag_calib ? "cal. units" : "data units");
        fprintf(fp,"%-14s: %s\n","File",DATA->hdr->file);
        fprintf(fp,"%-14s: %s\n","Span",fl_get_object_label(GUI->measure->txt_span));
        fprintf(fp,"%-14s: %s %s\n","Minimum value",fl_get_object_label(GUI->measure->txt_min),buf);
        fprintf(fp,"%-14s: %s %s\n","Maximum value",fl_get_object_label(GUI->measure->txt_max),buf);
        fprintf(fp,"%-14s: %s %s\n","Average value",fl_get_object_label(GUI->measure->txt_mean),buf);
        fprintf(fp,"%-14s: %s %s\n","Std. deviation",fl_get_object_label(GUI->measure->txt_sdev),buf);
        fprintf(fp,"%-14s: %s %s\n","Median value",fl_get_object_label(GUI->measure->txt_median),buf);
        fprintf(fp,"%-14s: %s %s\n","Total value",fl_get_object_label(GUI->measure->txt_total),buf);
        fclose(fp);

        free(base); free(dir);
    }
}

void cb_profile_clear ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_view_workspace *vwk = VIEW_WORKSPACE(GUI->viewport);

    vwk->epset = vwk->spset = false;
    PROFILE_NPOINTS(GUI->measure) = 0;
    free(PROFILE_VALUES(GUI->measure));
    PROFILE_VALUES(GUI->measure) = NULL;

    fl_clear_xyplot(GUI->measure->plot_profile);
    fl_set_object_label(GUI->measure->txt_span,"");
    fl_set_object_label(GUI->measure->txt_min,"");
    fl_set_object_label(GUI->measure->txt_max,"");
    fl_set_object_label(GUI->measure->txt_mean,"");
    fl_set_object_label(GUI->measure->txt_sdev,"");
    fl_set_object_label(GUI->measure->txt_median,"");
    fl_set_object_label(GUI->measure->txt_total,"");

    update_measurement(GUI->measure,GUI->viewport);
    refresh_canvas(ENV);
}
