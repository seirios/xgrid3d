#ifndef XG3D_opendlg_cb_h
#define XG3D_opendlg_cb_h

#include "fd_dialog.h"
#include "xgrid3d.h"
#include "io.h"

#define OPENDLG_MARGIN 40

#define OPEN_HEADER(fd_open_hdr) (((FD_open_hdr*)(fd_open_hdr))->vdata)

xg3d_header* opendlg_show(xg3d_env*);
void savedlg_show(FD_savedlg*,const xg3d_data*,const char*);
xg3d_header* newdlg_show(xg3d_env*,const char**,int*);
void cb_fselector_home(void*);

#endif
