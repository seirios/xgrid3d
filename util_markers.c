#include "util_markers.h"

/* Marker functions */
marker* marker_alloc(const coord_t vox[3], const char *label) {
    marker *mk = malloc(sizeof(marker));
    mk->label = (label != NULL && strlen(label) > 0) ? strdup(label) : NULL;
    mk->align = 1;
    memcpy(mk->vox,vox,3*sizeof(coord_t));
    mk->next = mk->prev = NULL;
    return mk;
}

marker* marker_search(marker *mk, const coord_t vox[3], unsigned int *ox) {
    unsigned int ix = 0;

    while(mk != NULL) {
        if(mk->vox[0] == vox[0] && mk->vox[1] == vox[1] && mk->vox[2] == vox[2]) {
            if(ox != NULL) *ox = ix;
            return mk;
        }
        mk = mk->next;  
        ix++;
    }

    return NULL;
}

void marker_free(marker *mk) {
    marker *p;

    while(mk != NULL) {
        p = mk->next;
        free(mk->label);
        free(mk);
        mk = p;
    }
}

inline static void marker_print(FILE *fp, const marker *mk) {
    while(mk != NULL) {
        fprintf(fp,"%"PRIcoord" %"PRIcoord" %"PRIcoord" \"%s\"\n",
                mk->vox[0],mk->vox[1],mk->vox[2],mk->label == NULL ? "" : mk->label);
        mk = mk->next;
    }
}

/* markers are saved in a circular doubly-linked list */
marker* marker_add(marker **mk, const coord_t vox[3], const char *label, unsigned int *nmk_o) {
    marker *nk = NULL;

    if(*mk == NULL) { /* first marker */
        *mk = nk = marker_alloc(vox,label);
        nk->prev = nk;
        if(nmk_o != NULL) *nmk_o = 1;
    } else if(marker_search(*mk,vox,NULL) == NULL) { /* unique marker */
        nk = marker_alloc(vox,label);
        (*mk)->prev->next = nk;
        nk->prev = (*mk)->prev;
        (*mk)->prev = nk;
        if(nmk_o != NULL) (*nmk_o)++;
    }

    return nk;
}

marker* marker_del(marker **mk, const coord_t vox[3], unsigned int *pos_o) {
    unsigned int pos;
    marker *dk;

    if((dk = marker_search(*mk,vox,&pos)) != NULL) {
        if((*mk)->prev == *mk) /* only */
            *mk = NULL;
        else if(dk == *mk) { /* first */
            *mk = dk->next;
            (*mk)->prev = dk->prev;
        } else if(dk->next == NULL) { /* last */
            (*mk)->prev = dk->prev;
            dk->prev->next = NULL;
        } else { /* intermediate */
            dk->prev->next = dk->next;
            dk->next->prev = dk->prev;
        }
    } else { /* delete last */
        pos = (unsigned)(-1);
        dk = (*mk)->prev;
        if((*mk)->prev == *mk) /* only */
            *mk = NULL;
        else if(dk->next == NULL) { /* last */
            (*mk)->prev = dk->prev;
            dk->prev->next = NULL;
        }
    }
    *pos_o = pos;

    /* isolate before free */
    dk->next = dk->prev = NULL;

    return dk;
}

marker* marker_dup(const marker *mk) {
    marker *mp,*mq,*mf;

    if(mk == NULL) mf = NULL; /* zero elements */
    else { /* at least one element */
        mf = marker_alloc(mk->vox,mk->label);
        mf->align = mk->align;
        mp = mf;
        mk = mk->next;
        while(mk != NULL) {
            mq = marker_alloc(mk->vox,mk->label);
            mq->align = mk->align;
            mp->next = mq;
            mq->prev = mp;
            mp = mq;
            mk = mk->next;
        }
    }

    return mf;
}

marker* marker_load(const char* file, unsigned int *nmk_o) {
    unsigned int nmk = 0;
    char *line = NULL;
    marker *mk = NULL;
    char label[1024];
    coord_t vox[3];
    ssize_t nchar;
    size_t n = 0;
    FILE *fp;

    fp = fopen(file,"r");
    if(fp == NULL) return NULL;
    while((nchar = getline(&line,&n,fp)) != -1)
        if(sscanf(line,"%"SCNcoord" %"SCNcoord" %"SCNcoord" \"%s\"",&vox[0],&vox[1],&vox[2],label) == 4) {
            label[strlen(label)-1] = '\0'; /* remove last double quote */
            marker_add(&mk,vox,label,&nmk);
        }
    free(line);
    fclose(fp);

    *nmk_o = nmk;

    return mk;
}

g3d_ret marker_save(const char* file, const marker* mk) {
    FILE *fp;

    fp = fopen(file,"w");
    if(fp == NULL) return G3D_EIO;
    marker_print(fp,mk);
    fclose(fp);

    return G3D_OK;
}
