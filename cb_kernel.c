#include <errno.h>
#include <unistd.h>
#include <libgen.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>

#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "cb_kernel.h"
#include "cb_dose.h"

#include "util.h"
#include "pardose.h"
#include "util_egs.h"

#ifdef EGSPP
#include "dosxyzpp_c.h"
#endif

#define FORMAT "%7.1f keV @ %1.5f"

void cb_kernel_choose ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *input = NULL;
    const char *file;
    char buf[128];

    switch(data) {
        case 0: /* HEN_HOUSE */
            input = GUI->kernel->inp_henhouse;
            snprintf(buf,sizeof(buf),_("Choose HEN_HOUSE..."));
            break;
        case 1: /* EGS_HOME */
            input = GUI->kernel->inp_egshome;
            snprintf(buf,sizeof(buf),_("Choose EGS_HOME..."));
            break;
        case 2: /* beta spectrum */
            input = GUI->kernel->inp_spectrum;
            snprintf(buf,sizeof(buf),_("Choose beta spectrum..."));
            break;
    }

    fl_set_directory(fl_get_input(input));
    file = fl_show_fselector(buf,"","*","");
    if(file != NULL) {
        if(strlen(file) == 0)
            fl_set_input(input,fl_get_directory());
        else
            fl_set_input(input,file);
    }
}

void cb_kernel_pegsfile ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    char *dir, *line = NULL;
    const char *file,*inp;
    char buf[25];
    FILE *fp;
    size_t n;

    if(fl_set_directory((inp = fl_get_input(GUI->kernel->inp_pegsfile)))) {
        dir = strdup(inp); fl_set_directory(dirname(dir)); free(dir);
    }
    file = fl_show_fselector(_("Load PEGS file..."),"","*.pegs*dat","");
    if(file == NULL) return;
    fp = fopen(file,"r");
    if(fp == NULL) {
        fl_show_alert2(0,_("Cannot open file for reading!\f%s"),file);
        return;
    }
    fl_set_input(GUI->kernel->inp_pegsfile,file);
    fl_clear_choice(GUI->kernel->ch_medium);
    while(getline(&line,&n,fp) != -1)
        if(sscanf(line," MEDIUM=%24s",buf) == 1)
            fl_addto_choice(GUI->kernel->ch_medium,buf);
    fclose(fp);
    free(line);
}

void cb_kernel_emission_op ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *inp_e,*inp_i,*br;
    const char *val_e,*val_i,*file;
    const long op = data / 2;
    double energy, intensity;
    FILE *fp;
    int ix;

    if(data % 2) { /* electron */
        inp_e = GUI->kernel->inp_elec_e;
        inp_i = GUI->kernel->inp_elec_i;
        br = GUI->kernel->br_elec;
    } else { /* photon */
        inp_e = GUI->kernel->inp_phot_e;
        inp_i = GUI->kernel->inp_phot_i;
        br = GUI->kernel->br_phot;
    }

    switch(op) {
        case 0: /* Add */
            val_e = fl_get_input(inp_e);
            val_i = fl_get_input(inp_i);
            if(strlen(val_e) == 0 || strlen(val_i) == 0 ||
                    (energy = strtod(val_e,NULL)) == 0.0 ||
                    (intensity = strtod(val_i,NULL)) == 0.0) {
                fl_show_alert2(0,_("Missing or invalid values!\fPlease check input fields."));
                return;
            }
            fl_add_browser_line_f(br,FORMAT,energy,intensity);
            fl_set_input(inp_e,""); fl_set_input(inp_i,"");
            break;
        case 1: /* Remove */
            fl_delete_browser_line(br,fl_get_browser(br));
            fl_set_input(inp_e,""); fl_set_input(inp_i,"");
            break;
        case 2: /* Replace */
            if((ix = fl_get_browser(br)) >= 1) {
                val_e = fl_get_input(inp_e);
                val_i = fl_get_input(inp_i);
                if(strlen(val_e) == 0 || strlen(val_i) == 0) {
                    fl_show_alert2(0,_("Missing values!\fPlease check input fields."));
                    return;
                }
                energy = strtod(val_e,NULL);
                intensity = strtod(val_i,NULL);
                fl_replace_browser_line_f(br,ix,FORMAT,energy,intensity);
            }
            break;
        case 3: /* Clear */
            fl_clear_browser(br);
            break;
        case 4: /* Load */
            file = fl_show_fselector(_("Load emissions from..."),"","*","");
            if(file != NULL)
                if(!fl_load_browser(br,file)) {
                    fl_show_alert2(0,_("Failed loading file!\f%s"),file);
                    return;
                }
            break;
        case 5: /* Save */
            file = fl_show_fselector(_("Save emissions to..."),"","*","");
            if(file != NULL) {
                fp = fopen(file,"w");
                if(fp == NULL) {
                    fl_show_alert2(0,_("Cannot open file for writing!\f%s"),file);
                    return;
                }
                for(int i=1;i<=fl_get_browser_maxline(br);i++)
                    fprintf(fp,"%s\n",fl_get_browser_line(br,i));
                fclose(fp);
            }
            break;
    }
}

void cb_kernel_emission_browser ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *inp_e,*inp_i;
    double energy,intensity;
    const char *line;

    if(data % 2) { /* electron */
        inp_e = GUI->kernel->inp_elec_e;
        inp_i = GUI->kernel->inp_elec_i;
    } else { /* photon */
        inp_e = GUI->kernel->inp_phot_e;
        inp_i = GUI->kernel->inp_phot_i;
    }

    line = fl_get_browser_line(obj,fl_get_browser(obj));
    sscanf(line,"%lf keV @ %lf",&energy,&intensity);
    fl_set_input_f(inp_e,"%g",energy);
    fl_set_input_f(inp_i,"%g",intensity);
}

void cb_kernel_dim ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const double vox = fl_get_spinner_value(GUI->kernel->spin_vox);
    const double maxr = fl_get_spinner_value(GUI->kernel->spin_maxr);
    const unsigned int nvox = ceil(maxr/vox);

    fl_set_object_label_f(GUI->kernel->txt_lobnd,"%g",nvox*(-vox));
    fl_set_object_label_f(GUI->kernel->txt_nvox,"%u",2*nvox+1);
}

static const char* _arg_string(const unsigned int njobs) {
    static char buf[64], dig[4];

    memset(buf,'\0',sizeof(buf)*sizeof(char));
    buf[0] = '1';
    for(unsigned int i=2;i<=njobs;i++) {
        snprintf(dig,4," %u",i);
        strcat(buf,dig);
    }

    return buf;
}

void cb_kernel_compute ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    char hen_house[PATH_MAX], egs_home[PATH_MAX], pegs_file[PATH_MAX];
    const char *title =    fl_get_input(GUI->kernel->inp_title),
               *nuclide =  fl_get_input(GUI->kernel->inp_nuclide),
               *spectrum = fl_get_input(GUI->kernel->inp_spectrum),
               *t_nhist =  fl_get_input(GUI->kernel->inp_nhist);
    const int nmed = fl_get_choice_maxitems(GUI->kernel->ch_medium);
    const unsigned int nphot = fl_get_browser_maxline(GUI->kernel->br_phot),
                       nelec = fl_get_browser_maxline(GUI->kernel->br_elec),
                       njobs = fl_get_spinner_value(GUI->kernel->spin_njobs);
    const int beta = fl_get_spinner_value(GUI->kernel->spin_beta) > 0.0;
    const double vox = fl_get_spinner_value(GUI->kernel->spin_vox);
    const double maxr = fl_get_spinner_value(GUI->kernel->spin_maxr);
    const unsigned int nvox = 2 * ceil(maxr/vox) + 1;
    char **egsinp, buf[PATH_MAX],name[1024];
    double energy,intensity;
    const char *medium;
    uintmax_t nhist;
    int charge;

    FL_FORM *form_cmdlog = ((FD_CMDLOG*)fl_get_command_log_fdstruct())->form;
    char cmd[1024];
    double *dvk;
    int status;

    FILE *fp;
    grid3d *tmp;
    grid3d *ker_beta = NULL;
    grid3d **ker_phot = NULL;
    grid3d **ker_elec = NULL;
    const double int_beta = fl_get_spinner_value(GUI->kernel->spin_beta);
    double *int_phot = NULL;
    double *int_elec = NULL;
    long sec0,sec1,usec;

    if(realpath(fl_get_input(GUI->kernel->inp_henhouse),hen_house) == NULL) {
        fl_show_alert2(0,_("ERROR realpath: %s\fPlease check paths."),strerror(errno));
        return;
    }
    if(realpath(fl_get_input(GUI->kernel->inp_egshome),egs_home) == NULL) {
        fl_show_alert2(0,_("ERROR realpath: %s\fPlease check paths."),strerror(errno));
        return;
    }
    if(realpath(fl_get_input(GUI->kernel->inp_pegsfile),pegs_file) == NULL) {
        fl_show_alert2(0,_("ERROR realpath: %s\fPlease check paths."),strerror(errno));
        return;
    }

    if(strlen(hen_house) == 0 || strlen(egs_home) == 0 || strlen(title) == 0 || nmed < 1
            || strlen(t_nhist) == 0 || (nphot == 0 && nelec == 0 && beta == false)
            || (beta && strlen(spectrum) == 0)) {
        fl_show_alert2(0,_("Missing or wrong parameters!\fPlease check inputs."));
        return;
    }

    fl_show_oneliner(_("Computing dose voxel kernel..."),fl_scrw/2,fl_scrh/2);
    fl_clear_command_log(); 
    fl_show_command_log(FL_TRANSIENT);

    medium = fl_get_choice_text(GUI->kernel->ch_medium);
    nhist = strtoumax(t_nhist,NULL,10);
    if(nhist < 100) { nhist = 100; }

    fl_addto_command_log_f("*** HEN_HOUSE is \"%s\"\n",hen_house);
    fl_addto_command_log_f("*** EGS_HOME is \"%s\"\n",egs_home);
    fl_addto_command_log_f("*** PEGS4 is \"%s\"\n",pegs_file);
    fl_addto_command_log_f("*** Starting computation of kernel \"%s\" for nuclide %s at %s\n",
            title,strlen(nuclide) == 0 ? "unspecified" : nuclide,fl_now());
    fl_addto_command_log_f("*** Geometry is %u cubic voxels of size %g cm\n",nvox,vox);
    fl_addto_command_log_f("*** Medium is %s\n",medium);
    fl_addto_command_log("*** Source is isotropic, uniformly distributed on center voxel\n");
    fl_addto_command_log_f("*** Running with %"PRIuMAX" histories per emission and %u parallel jobs\n",nhist,njobs);

    fl_addto_command_log_f("*** Creating directory \"%s\" inside %s/dosxyznrc\n",title,egs_home);
    snprintf(buf,sizeof(buf),"%s/dosxyznrc/%s",egs_home,title);
    if(mkdir(buf,S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH) == -1) {
        fl_hide_oneliner();
        fl_show_alert2(0,_("Failed creating directory!\fPlease check if it exists already."));
        return;
    }

    if(beta) {
        charge = -1;
        fl_addto_command_log("\n");
        fl_addto_command_log_f("*** Emission charge is %d (beta)\n",charge);
        fl_addto_command_log_f("*** Emission energy spectrum from file \"%s\"\n",spectrum);
        fl_addto_command_log_f("*** Emission intensity is %g\n",int_beta);
        snprintf(name,sizeof(name),"beta%lu",random());
        egsinp = setup_dosxyznrc_input(name,medium,vox,maxr,nhist,njobs,charge,true,spectrum);
        snprintf(buf,sizeof(buf),"%s/dosxyznrc/%s.egsinp",egs_home,name);
        fl_addto_command_log_f("*** Creating DOSXYZnrc input file %s.egsinp\n",name);
        export_egs_input(buf,egsinp,DOSXYZNRC_INPUT_LEN); free_dosxyznrc_input(egsinp);
        if(njobs == 1)
            snprintf(cmd,sizeof(cmd),"dosxyznrc -H '%s' -e '%s' -p '%s' -i '%s.egsinp' -b -P 1 -f 1 -j 1",
                    hen_house,egs_home,pegs_file,name);
        else
            snprintf(cmd,sizeof(cmd),
                    "parallel -v -j %u dosxyznrc -H '%s' -e '%s' -p '%s' -i '%s.egsinp' -b -P %u -f 1 -j {} ::: %s",
                    njobs,hen_house,egs_home,pegs_file,name,njobs,_arg_string(njobs));
        fl_gettime(&sec0,&usec); fl_addto_command_log_f("*** Starting batch run at %s\n",fl_now());
        fl_addto_command_log_f("*** Running command: %s\n",cmd);
        fl_deactivate_all_forms(); fl_activate_form(form_cmdlog);
        status = fl_exe_command(cmd,true);
        fl_activate_all_forms();
        if(WEXITSTATUS(status) != 0) {
            fl_hide_oneliner();
            fl_show_alert2(0,_("Process termination failure!\fPlease check command log."));
            return;
        }
        fl_gettime(&sec1,&usec); fl_addto_command_log_f("*** Finished batch run after %.2f minutes\n",(sec1-sec0)/60.0);
        fl_msleep(2000);
        fl_addto_command_log("*** Processing .pardose files\n");
        if(pardose(egs_home,name,njobs,vox,&dvk)) {
            fl_hide_oneliner();
            fl_show_alert2(0,_("Pardose failed!\fPlease check inputs and retry."));
            return;
        }
        fl_addto_command_log("*** Storing computed kernel\n");
        ker_beta = g3d_alloc(G3D_FLOAT64,nvox,nvox,nvox,.alloc=false);
        ker_beta->dat = dvk;
    }

    if(nphot >= 1) {
        charge = 0;
        ker_phot = calloc(nphot,sizeof(grid3d*));
        int_phot = calloc(nphot,sizeof(double));
        for(unsigned int i=1;i<=nphot;i++) {
            fl_addto_command_log("\n");
            fl_addto_command_log_f("*** Emission charge is %d (photon)\n",charge);
            sscanf(fl_get_browser_line(GUI->kernel->br_phot,i),"%lf keV @ %lf",&energy,&intensity);
            energy /= 1E3; /* keV to MeV */
            snprintf(buf,sizeof(buf),"%.6g",energy);
            snprintf(name,sizeof(name),"photon_%s",buf);
            egsinp = setup_dosxyznrc_input(name,medium,vox,maxr,nhist,njobs,charge,false,buf);
            fl_addto_command_log_f("*** Emission energy is %g MeV with intensity %g\n",energy,intensity);
            snprintf(buf,sizeof(buf),"%s/dosxyznrc/%s.egsinp",egs_home,name);
            fl_addto_command_log_f("*** Creating DOSXYZnrc input file %s.egsinp\n",name);
            export_egs_input(buf,egsinp,DOSXYZNRC_INPUT_LEN); free_dosxyznrc_input(egsinp);
            if(njobs == 1)
                snprintf(cmd,sizeof(cmd),"dosxyznrc -H '%s' -e '%s' -p '%s' -i '%s.egsinp' -b -P 1 -f 1 -j 1",
                        hen_house,egs_home,pegs_file,name);
            else
                snprintf(cmd,sizeof(cmd),
                        "parallel -v -j %u dosxyznrc -H '%s' -e '%s' -p '%s' -i '%s.egsinp' -b -P %u -f 1 -j {} ::: %s",
                        njobs,hen_house,egs_home,pegs_file,name,njobs,_arg_string(njobs));
            fl_gettime(&sec0,&usec); fl_addto_command_log_f("*** Starting batch run at %s\n",fl_now());
            fl_addto_command_log_f("*** Running command: %s\n",cmd);
            fl_deactivate_all_forms(); fl_activate_form(form_cmdlog);
            status = fl_exe_command(cmd,true);
            fl_activate_all_forms();
            if(WEXITSTATUS(status) != 0) {
                fl_hide_oneliner();
                fl_show_alert2(0,_("Process termination failure!\fPlease check command log."));
                return;
            }
            fl_gettime(&sec1,&usec); fl_addto_command_log_f("*** Finished batch run after %.2f minutes\n",(sec1-sec0)/60.0);
            fl_msleep(2000);
            fl_addto_command_log("*** Processing .pardose files\n");
            if(pardose(egs_home,name,njobs,vox,&dvk)) {
                fl_hide_oneliner();
                fl_show_alert2(0,_("Pardose failed!\fPlease check inputs and retry."));
                return;
            }
            fl_addto_command_log("*** Storing computed kernel\n");
            ker_phot[i-1] = g3d_alloc(G3D_FLOAT64,nvox,nvox,nvox,.alloc=false);
            ker_phot[i-1]->dat = dvk;
            int_phot[i-1] = intensity;
            fl_addto_command_log("*** Cleaning up\n");
            fl_deactivate_all_forms(); fl_activate_form(form_cmdlog);
            snprintf(cmd,sizeof(cmd),"mv %s/dosxyznrc/%s* %s/dosxyznrc/%s",egs_home,name,egs_home,title);
            status = fl_exe_command(cmd,true);
            fl_activate_all_forms();
            if(WEXITSTATUS(status) != 0)
                fl_addto_command_log("*** FAILED...moving on\n");
            else
                fl_addto_command_log("*** DONE\n");
        }
    }

    if(nelec >= 1) {
        charge = -1;
        ker_elec = calloc(nelec,sizeof(grid3d*));
        int_elec = calloc(nelec,sizeof(double));
        for(unsigned int i=1;i<=nelec;i++) {
            fl_addto_command_log("\n");
            fl_addto_command_log_f("*** Emission charge is %d (electron)\n",charge);
            sscanf(fl_get_browser_line(GUI->kernel->br_elec,i),"%lf keV @ %lf",&energy,&intensity);
            energy /= 1E3; /* keV to MeV */
            snprintf(buf,sizeof(buf),"%.6g",energy);
            snprintf(name,sizeof(name),"electron_%s",buf);
            egsinp = setup_dosxyznrc_input(name,medium,vox,maxr,nhist,njobs,charge,false,buf);
            fl_addto_command_log_f("*** Emission energy is %g MeV with intensity %g\n",energy,intensity);
            snprintf(buf,sizeof(buf),"%s/dosxyznrc/%s.egsinp",egs_home,name);
            fl_addto_command_log_f("*** Creating DOSXYZnrc input file %s.egsinp\n",name);
            export_egs_input(buf,egsinp,DOSXYZNRC_INPUT_LEN); free_dosxyznrc_input(egsinp);
            if(njobs == 1)
                snprintf(cmd,sizeof(cmd),"dosxyznrc -H '%s' -e '%s' -p '%s' -i '%s.egsinp' -b -P 1 -f 1 -j 1",
                        hen_house,egs_home,pegs_file,name);
            else
                snprintf(cmd,sizeof(cmd),
                        "parallel -v -j %u dosxyznrc -H '%s' -e '%s' -p '%s' -i '%s.egsinp' -b -P %u -f 1 -j {} ::: %s",
                        njobs,hen_house,egs_home,pegs_file,name,njobs,_arg_string(njobs));
            fl_gettime(&sec0,&usec); fl_addto_command_log_f("*** Starting batch run at %s\n",fl_now());
            fl_addto_command_log_f("*** Running command: %s\n",cmd);
            fl_deactivate_all_forms(); fl_activate_form(form_cmdlog);
            status = fl_exe_command(cmd,true);
            fl_activate_all_forms();
            if(WEXITSTATUS(status) != 0) {
                fl_hide_oneliner();
                fl_show_alert2(0,_("Process termination failure!\fPlease check command log."));
                return;
            }
            fl_gettime(&sec1,&usec); fl_addto_command_log_f("*** Finished batch run after %.2f minutes\n",(sec1-sec0)/60.0);
            fl_msleep(2000);
            fl_addto_command_log("*** Processing .pardose files\n");
            if(pardose(egs_home,name,njobs,vox,&dvk)) {
                fl_hide_oneliner();
                fl_show_alert2(0,_("Pardose failed!\fPlease check inputs and retry."));
                return;
            }
            fl_addto_command_log("*** Storing computed kernel\n");
            ker_elec[i-1] = g3d_alloc(G3D_FLOAT64,nvox,nvox,nvox,.alloc=false);
            ker_elec[i-1]->dat = dvk;
            int_elec[i-1] = intensity;
        }
    }

    fl_addto_command_log("\n");
    fl_addto_command_log("*** Writing kernel description file\n");
    snprintf(buf,sizeof(buf),"%s/%s.kernel",kernel_dir,title);
    fp = fopen(buf,"w");
    fprintf(fp,"nuclide %s\n",strlen(nuclide) == 0 ? "unspecified" : nuclide);
    fprintf(fp,"medium %s\n",medium);
    fprintf(fp,"electron %s\n",nelec >= 1 ? "yes" : "no");
    fprintf(fp,"photon %s\n",nphot >= 1 ? "yes" : "no");
    fprintf(fp,"beta %s\n",beta ? "yes" : "no");
    fprintf(fp,"dim %u\n",nvox);
    fprintf(fp,"vox %g\n",vox);
    fclose(fp);

    if(beta) {
        fl_addto_command_log("*** Processing beta kernel\n");
        tmp = g3d_alloc(G3D_FLOAT64,.nx=nvox,.ny=nvox,.nz=nvox,.alloc=true);
#ifdef _OPENMP
#pragma omp parallel for
#endif
        for(index_t ix=0;ix<tmp->nvox;ix++)
            ((double*)tmp->dat)[ix] = int_beta * ((double*)ker_beta->dat)[ix];
        snprintf(buf,sizeof(buf),"%s/%s_beta.img.gz",kernel_dir,title);
        g3d_output_raw(tmp,buf,RAW_FLOAT64,COMPR_GZIP,ENDIAN_LITTLE,.append=false); free(tmp);
    }

    if(nphot >= 1) {
        fl_addto_command_log("*** Processing photon kernel\n");
        tmp = g3d_alloc(G3D_FLOAT64,.nx=nvox,.ny=nvox,.nz=nvox,.alloc=true);
#ifdef _OPENMP
#pragma omp parallel for
#endif
        for(index_t ix=0;ix<tmp->nvox;ix++)
            for(unsigned int i=0;i<nphot;i++)
                ((double*)tmp->dat)[ix] += int_phot[i] * ((double*)ker_phot[i]->dat)[ix];
        snprintf(buf,sizeof(buf),"%s/%s_photon.img.gz",kernel_dir,title);
        g3d_output_raw(tmp,buf,RAW_FLOAT64,COMPR_GZIP,ENDIAN_LITTLE,.append=false); free(tmp);
    }

    if(nelec >= 1) {
        fl_addto_command_log("*** Processing electron kernel\n");
        tmp = g3d_alloc(G3D_FLOAT64,.nx=nvox,.ny=nvox,.nz=nvox,.alloc=true);
#ifdef _OPENMP
#pragma omp parallel for
#endif
        for(index_t ix=0;ix<tmp->nvox;ix++)
            for(unsigned int i=0;i<nelec;i++)
                ((double*)tmp->dat)[ix] += int_elec[i] * ((double*)ker_elec[i]->dat)[ix];
        snprintf(buf,sizeof(buf),"%s/%s_electron.img.gz",kernel_dir,title);
        g3d_output_raw(tmp,buf,RAW_FLOAT64,COMPR_GZIP,ENDIAN_LITTLE,.append=false); free(tmp);
    }

    fl_addto_command_log_f("*** Finished kernel computation at %s\n",fl_now());
    fl_hide_oneliner();
}

void cb_kernel_compute2 ( FL_OBJECT * obj  , long data  ) {
    (void)data;
#ifndef EGSPP
    (void)obj;
    fl_show_alert2(0,"NOTICE\fegspp support unavailable!");
#else
    char hen_house[PATH_MAX], egs_home[PATH_MAX], pegs_file[PATH_MAX];
    const char *t_nhist = fl_get_input(GUI->kernel->inp_nhist);
    const unsigned int njobs = fl_get_spinner_value(GUI->kernel->spin_njobs);
    const double vox = fl_get_spinner_value(GUI->kernel->spin_vox);
    const double maxr = fl_get_spinner_value(GUI->kernel->spin_maxr);
    const unsigned int nvox = 2 * ceil(maxr/vox) + 1;
    char buf[PATH_MAX],name[1024];
    const char *medium;
    uintmax_t nhist;
    double energy;
    int charge;

    /* fork-specific */
    pid_t *pidc;
    int status;
    pid_t pid;

    /* dosxyzpp-specific */
    char **egsinp,*egs_serial_input;
    dosxyzpp *dosxyzpp;

    /* results */
    long nreg, cix;
    /* shared memory */
    long *ncase_n;
    double **sum_n,**sum2_n;
    double *Etot_n;
    double *mass;
    /* final results */
    long ncase;
    double Etot;
    double *dose,*doserr;
    double edep_out, edep_tot;

    /* setup paths */
    realpath(fl_get_input(GUI->kernel->inp_henhouse),hen_house);
    strcat(hen_house,"/"); /* append final slash to HEN_HOUSE (or crash) */
    realpath(fl_get_input(GUI->kernel->inp_egshome),egs_home);
    realpath(fl_get_input(GUI->kernel->inp_pegsfile),pegs_file);

    charge = 0;
    energy = 0.113;
    snprintf(buf,sizeof(buf),"%.6g",energy);
    snprintf(name,sizeof(name),"photon_%s",buf);
    medium = fl_get_choice_text(GUI->kernel->ch_medium);
    nhist = strtoumax(t_nhist,NULL,10);
    if(nhist < 1000) { nhist = 1000; }

    egsinp = setup_dosxyzpp_input(name,medium,vox,maxr,nhist,njobs,charge,false,buf);
    egs_serial_input = serialize_egs_input(egsinp,DOSXYZPP_INPUT_LEN);
    free_dosxyzpp_input(egsinp);

    nreg = 1 + nvox * nvox * nvox;

    /* allocate private memory */
    sum_n = malloc(njobs*sizeof(double*));
    sum2_n = malloc(njobs*sizeof(double*));

    /* allocate shared memory */
    ncase_n = mmap(NULL, njobs*sizeof(long), PROT_READ | PROT_WRITE,
            MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    Etot_n = mmap(NULL, njobs*sizeof(double), PROT_READ | PROT_WRITE,
            MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    mass = mmap(NULL, (nreg-1)*sizeof(double), PROT_READ | PROT_WRITE,
            MAP_SHARED | MAP_ANONYMOUS, -1, 0);

    /* TODO: grab output to cmd_br */

    /* spawn children */
    pidc = malloc(njobs*sizeof(pid_t));
    unsigned int job;
    for(job=0;job<njobs;job++) {
        ncase_n[job] = 0;
        Etot_n[job] = 0.0;
        sum_n[job] = mmap(NULL, nreg*sizeof(double), PROT_READ | PROT_WRITE,
                MAP_SHARED | MAP_ANONYMOUS, -1, 0);
        sum2_n[job] = mmap(NULL, nreg*sizeof(double), PROT_READ | PROT_WRITE,
                MAP_SHARED | MAP_ANONYMOUS, -1, 0);
        pidc[job] = fork();
        if(pidc[job] < 0) {
            perror("fork");
            return;
        } else if(pidc[job] == 0) { /* child */
            dosxyzpp = dosxyzpp_new();

            dosxyzpp_setHenHouse(dosxyzpp,hen_house);
            dosxyzpp_setPegsFile(dosxyzpp,pegs_file);
            dosxyzpp_setAppName(dosxyzpp,"");
            dosxyzpp_setEgsHome(dosxyzpp,"/tmp/");
            dosxyzpp_setAppDir(dosxyzpp,"/tmp/");
            dosxyzpp_setOutputFile(dosxyzpp,name);
            dosxyzpp_setRunDir(dosxyzpp,"");
            dosxyzpp_setQuiet(dosxyzpp);
            if(njobs > 1) {
                dosxyzpp_setSimpleRun(dosxyzpp);
                dosxyzpp_setParallel(dosxyzpp,njobs,job+1);
            }

            dosxyzpp_setInput(dosxyzpp,egs_serial_input);

            dosxyzpp_initSimulation(dosxyzpp);
            dosxyzpp_runSimulation(dosxyzpp);
            dosxyzpp_finishSimulation(dosxyzpp);

            ncase_n[job] = dosxyzpp_getNCase(dosxyzpp);
            Etot_n[job] = dosxyzpp_getEtot(dosxyzpp);

            for(long ir=0;ir<nreg;ir++)
                dosxyzpp_getDoseScore(dosxyzpp,ir,&sum_n[job][ir],&sum2_n[job][ir]);

            if(job == 0) /* only first child */
                for(long ir=0;ir<nreg-1;ir++)
                    mass[ir] = dosxyzpp_getMass(dosxyzpp,1+ir);

            dosxyzpp_delete(dosxyzpp);

            _exit(0);
        }
    }

    /* wait for all children to exit */
    while(job > 0) {
        pid = wait(&status);
        debug("Child with PID %d exited with status %d",pid,WEXITSTATUS(status));
        job--;
    }

    /* reduction (+:Etot) (+:ncase) */
    ncase = 0;
    Etot = 0.0;
    for(unsigned int j=0;j<njobs;j++) {
        ncase += ncase_n[j];
        Etot += Etot_n[j];
    }

    /* free shared memory */
    munmap(ncase_n,njobs*sizeof(long));
    munmap(Etot_n,njobs*sizeof(double));

    /* reduction (+:sum_n) (+:sum2_n) */
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for(long ir=0;ir<nreg;ir++)
        for(unsigned int j=1;j<njobs;j++) {
            sum_n[0][ir] += sum_n[j][ir];
            sum2_n[0][ir] += sum2_n[j][ir];
        }

    /* free shared memory */
    for(unsigned int j=1;j<njobs;j++) {
        munmap(sum_n[j],nreg*sizeof(double));
        munmap(sum2_n[j],nreg*sizeof(double));
    }

    /* allocate private final results */
    /* indexes in dose and doserr are for inside regions only, zero-based */
    dose = malloc((nreg-1)*sizeof(double));
    doserr = malloc((nreg-1)*sizeof(double));

    /* scores for inside regions */
    edep_tot = 0.0;
#ifdef _OPENMP
#pragma omp parallel for reduction(+:edep_tot)
#endif
    for(long ir=0;ir<nreg-1;ir++) {
        dose[ir] = sum_n[0][1+ir];
        doserr[ir] = sum2_n[0][1+ir];
        edep_tot += dose[ir];
    }

    /* score for outside region */
    edep_out = sum_n[0][0];
    edep_tot += edep_out;

    /* free shared memory */
    munmap(sum_n[0],nreg*sizeof(double));
    munmap(sum2_n[0],nreg*sizeof(double));

    /* free private memory */
    free(sum_n);
    free(sum2_n);

    /* compute dose */
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for(long ir=0;ir<nreg-1;ir++) {
        dose[ir] /= ncase;
        doserr[ir] /= ncase;
        doserr[ir] = sqrt((doserr[ir] - dose[ir]*dose[ir])/(ncase - 1));
        doserr[ir] /= dose[ir];
        dose[ir] *= 1.6021766208E-10 / mass[ir];
    }

    /* free shared memory */
    munmap(mass,(nreg-1)*sizeof(double));

    /* dose and doserr contain the results of the simulation */
    cix = (nvox/2) + (nvox/2) * nvox + (nvox/2) * nvox * nvox;
    debug("%g +/- %g%%",dose[cix],100.0*doserr[cix]);
    debug("%g%% energy deposited outside geometry",100*edep_out/edep_tot);

    /* free private memory */
    free(dose);
    free(doserr);
#endif
}

void cb_cmdlog_save ( FL_OBJECT * obj  , long data  ) {
    (void)obj;
(void)data;
    FD_CMDLOG *cmdlog = fl_get_command_log_fdstruct();

    const char *file = fl_show_fselector(_("Save log as..."),"","*.log","");
    if(file == NULL) return;
    FILE *fp = fopen(file,"w");
    if(fp == NULL) {
        fl_show_alert2(0,_("Failed opening file for writing!\f%s"),file);
        return;
    }
    for(int i=1;i<=fl_get_browser_maxline(cmdlog->browser);i++)
        fprintf(fp,"%s\n",fl_get_browser_line(cmdlog->browser,i));
    fclose(fp);
}
