#include "rois.h"

#include <float.h>

#include <gsl/gsl_math.h>

typedef int (_boolop_test)(const unsigned int,const unsigned int); 

static int _cmpvox(const void *a, const void *b) {
    const coord_t *voxa = *((coord_t**)a);
    const coord_t *voxb = *((coord_t**)b);

    if(voxa[2] > voxb[2])
        return 1;
    else if(voxa[2] < voxb[2])
        return -1;
    else {
        if(voxa[1] > voxb[1])
            return 1;
        else if(voxa[1] < voxb[1])
            return -1;
        else {
            if(voxa[0] > voxb[0])
                return 1;
            else if(voxa[0] < voxb[0])
                return -1;
            else
                return 0;
        }
    }
}

static int _or_test (const unsigned int x, const unsigned int y) { (void)y; return x > 0; }
static int _and_test(const unsigned int x, const unsigned int y) {          return x == y; }
static int _xor_test(const unsigned int x, const unsigned int y) { (void)y; return x % 2 == 1; }

g3d_ret rois_boolop(const grid3d *selection, const g3d_roi_list *list, const enum roi_boolop op, g3d_roi **roi_o) {
    _boolop_test *op_test = NULL;
    char *cat, *name = NULL;

    if(list == NULL || list->size == 0) return G3D_ENOINPUT; /* no input ROIs */

    /* Retrieve combined ROI name and size */
    size_t len = 0;
    for(nroi_t i=0;i<list->size;i++)
        len += strlen(list->rois[i]->name);
    cat = calloc(len+list->size+1,sizeof(char));

    index_t size = 0;
    for(nroi_t i=0;i<list->size;i++) {
        strcat(cat,list->rois[i]->name);
        if(i < list->size - 1) strcat(cat,",");
        size += list->rois[i]->size;
    }

    switch(op) {
        case ROI_BOOLOP_OR:
            op_test = _or_test;
            name = calloc(9+strlen(cat)+1,sizeof(char));
            sprintf(name,"UNION of ");
            break;
        case ROI_BOOLOP_AND:
            op_test = _and_test;
            name = calloc(16+strlen(cat)+1,sizeof(char));
            sprintf(name,"INTERSECTION of ");
            break;
        case ROI_BOOLOP_XOR:
            op_test = _xor_test;
            name = calloc(13+strlen(cat)+1,sizeof(char));
            sprintf(name,"EXCLUSION of ");
            break;
    }
    name = strcat(name,cat); free(cat);
    g3d_roi *roi = g3d_roi_alloc(name,0); free(name);

    if(size * (3 * sizeof(coord_t) + sizeof(coord_t*)) < selection->nvox * sizeof(uint8_t) || list->size > UINT8_MAX) {
        /* create array of coord_t[3] for sorting */
        coord_t **vox = malloc(size*sizeof(coord_t*));
        size = 0;
        for(nroi_t i=0;i<list->size;i++) {
#ifdef _OPENMP
#pragma omp parallel for
#endif
            for(index_t ix=0;ix<list->rois[i]->size;ix++) {
                coord_t *v = malloc(3*sizeof(coord_t));
                v[0] = ROI_J(ix,list->rois[i]);
                v[1] = ROI_I(ix,list->rois[i]);
                v[2] = ROI_K(ix,list->rois[i]);
                vox[size+ix] = v;
            }
            size += list->rois[i]->size;
        }
        qsort(vox,size,sizeof(coord_t*),_cmpvox);

        /* keep voxels that pass test */
        index_t n = 0;
        index_t m = 1;
        for(index_t ix=0;ix<size-1;ix++) {
            if(_cmpvox(&vox[ix],&vox[ix+1]) == 0) m++;
            else { if(op_test(m,list->size)) memcpy(vox[n++],vox[ix],3*sizeof(coord_t)); m = 1; }
        }
        if(_cmpvox(&vox[size-1],&vox[0]) == 0) m++;
        else { if(op_test(m,list->size)) memcpy(vox[n++],vox[size-1],3*sizeof(coord_t)); m = 1; }

        if(n == 0) {
            free(vox);
            return G3D_FAIL;
        } else
            vox = realloc(vox,n*sizeof(coord_t*));

        roi->size = n;
        roi->vox = malloc(3*n*sizeof(coord_t));
        /* copy from array of coord_t[3] to g3d_roi */
        for(index_t ix=0;ix<n;ix++) {
            memcpy(&roi->vox[3*ix],vox[ix],3*sizeof(coord_t));
            free(vox[ix]);
        }
        free(vox);
    } else {
        grid3d *tmp = g3d_alloc(G3D_UINT8,selection->nx,selection->ny,selection->nz,.alloc=true);
        /* count voxels */
        for(nroi_t i=0;i<list->size;i++)
            for(index_t ix=0;ix<list->rois[i]->size;ix++)
                g3d_u8(tmp,ROI_IX(ix,list->rois[i],tmp))++;

        /* count voxels that pass test */
        index_t n = 0;
#ifdef _OPENMP
#pragma omp parallel for reduction(+:n)
#endif
        for(index_t ix=0;ix<selection->nvox;ix++)
            n += op_test(g3d_u8(tmp,ix),list->size);

        if(n == 0) {
            g3d_free(tmp);
            return G3D_FAIL;
        }

        roi->size = n;
        roi->vox = malloc(3*n*sizeof(coord_t));
        /* fill ROI voxels */
        n = 0;
        for(index_t ix=0;ix<selection->nvox;ix++)
            if(op_test(g3d_u8(tmp,ix),list->size)) {
                ROI_J(n,roi) = (ix%selection->nxy)%selection->nx;
                ROI_I(n,roi) = (ix%selection->nxy)/selection->nx;
                ROI_K(n,roi) = (ix/selection->nxy);
                n++;
            }

        g3d_free(tmp);
    }

    if(roi_o != NULL) {
        *roi_o = roi;
        return G3D_OK;
    } else
        return G3D_FAIL;
}

g3d_ret rois_select(const g3d_roi_list *list, grid3d *selection, const bool Tset_Fclear) {
    if(list == NULL || selection == NULL) return G3D_ENOINPUT;

    for(nroi_t i=0;i<list->size;i++)
        g3d_roi_select(list->rois[i],selection,Tset_Fclear);

    return G3D_OK;
}

/* Compute the mean spherical subset of a g3d_roi */
/* OBS: to be a maximum subset, it would need to check border voxels only */
#define D3(i,j,roi) gsl_hypot3(ROI_J(i,roi)-ROI_J(j,roi),ROI_I(i,roi)-ROI_I(j,roi),ROI_K(i,roi)-ROI_K(j,roi))
g3d_ret rois_mss(const g3d_roi *roi, g3d_roi **nroi_o) {
    char buf[256];

    if(roi == NULL) return G3D_ENOINPUT;

    /* compute mss radius */
#ifdef _OPENMP
    size_t nth = omp_get_max_threads();
#else
    size_t nth = 1;
#endif
    double *thmsd = malloc(nth*sizeof(double));
    index_t *thp = calloc(nth,sizeof(index_t));
    double *thr = malloc(nth*sizeof(double));
    for(size_t th=0;th<nth;th++)
        thmsd[th] = DBL_MAX;
#ifdef _OPENMP
#pragma omp parallel
    {
        size_t cth = omp_get_thread_num();
#else
        size_t cth = 0;
#endif

#ifdef _OPENMP
#pragma omp for
#endif
        for(index_t i=0;i<roi->size;i++) {
            double avg = 0.0;
            double sd = 0.0;
            for(index_t j=1;j<roi->size;j++) {
                double r = D3(i,(i+j)%roi->size,roi);
                double delta = r - avg;
                avg += delta / (double)j;
                sd += delta * (r - avg);
            }
            if(sd < thmsd[cth]) {
                thp[cth] = i;
                thr[cth] = avg;
                thmsd[cth] = sd;
            }
        }
#ifdef _OPENMP
    }
#endif

    double min_sd = DBL_MAX;
    double radius=0.0;
    index_t p=0;
    for(size_t th=0;th<nth;th++)
        if(thmsd[th] < min_sd) {
            p = thp[th];
            radius = thr[th];
            min_sd = thmsd[th];
        }
    free(thmsd);
    free(thp);
    free(thr);

    /* create mss roi */
    index_t size = roi->size;
#ifdef _OPENMP
#pragma omp parallel for reduction(+:size)
#endif
    for(index_t j=0;j<roi->size;j++)
        if(D3(p,j,roi) > radius) size--; /* discount outside radius */
    if(size == 0) return G3D_FAIL;

    snprintf(buf,sizeof(buf),"MSS of %s",roi->name);
    g3d_roi *nroi = g3d_roi_alloc(buf,size);
    for(index_t j=0,i=0;j<roi->size;j++)
        if(D3(p,j,roi) <= radius) /* include inside radius */
            memcpy(&nroi->vox[3*i++],&roi->vox[3*j],3*sizeof(coord_t));

    if(nroi_o != NULL) *nroi_o = nroi;
    else {
        g3d_roi_free(nroi);
        return G3D_FAIL;
    }

    return G3D_OK;
}
#undef D3
