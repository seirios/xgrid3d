#ifndef XG3D_cb_coloring_h_
#define XG3D_cb_coloring_h_

#include "fd_coloring.h"
#include "xgrid3d.h"
#include "coloring.h"


/* Coloring settings value mask bits */
/* coloring */
#define COLORING_FLAG_REVERSE       (1L<<0)
#define COLORING_FLAG_INVERT        (1L<<1)
#define COLORING_SET_UNDER          (1L<<2)
#define COLORING_SET_OVER           (1L<<3)
#define COLORING_COLORING           (COLORING_FLAG_REVERSE | \
                                     COLORING_FLAG_INVERT | \
                                     COLORING_SET_UNDER | \
                                     COLORING_SET_OVER)
#define COLORING_COLOR_MODE         (1L<<4)
/* col_grad */
#define COLORING_SET_GRAD_TYPE      (1L<<5)
#define COLORING_SETT_HUE           (1L<<6)
#define COLORING_COL_GRAD           (COLORING_COLOR_MODE | \
                                     COLORING_SET_GRAD_TYPE | \
                                     COLORING_SETT_HUE)
/* col_predef */
#define COLORING_SET_PREDEF_TYPE    (1L<<7)
#define COLORING_COL_PREDEF         (COLORING_COLOR_MODE | \
                                     COLORING_SET_PREDEF_TYPE)
/* col_table */
#define COLORING_TAB_NCOL           (1L<<8)
#define COLORING_TAB_LUT            (1L<<9)
#define COLORING_USE_LUT            (1L<<10)
#define COLORING_FILE_LUT           (1L<<11)
#define COLORING_FILE_NAME          (1L<<12)
#define COLORING_COL_TABLE          (COLORING_COLOR_MODE | \
                                     COLORING_TAB_NCOL | \
                                     COLORING_TAB_LUT | \
                                     COLORING_USE_LUT | \
                                     COLORING_FILE_LUT | \
                                     COLORING_FILE_NAME)
/* col_ramp */
#define COLORING_RAMP_ACTIVE        (1L<<13)
#define COLORING_RAMP_SMOOTH        (1L<<14)
#define COLORING_RAMP_N             (1L<<15)
#define COLORING_RAMP_X             (1L<<16)
#define COLORING_RAMP_Y             (1L<<17)
#define COLORING_COL_RAMP           (COLORING_COLOR_MODE | \
                                     COLORING_RAMP_ACTIVE | \
                                     COLORING_RAMP_SMOOTH | \
                                     COLORING_RAMP_N | \
                                     COLORING_RAMP_X | \
                                     COLORING_RAMP_Y)

typedef struct {
    int flag_reverse;                 /* reverse LUT? */
    int flag_invert;                  /* invert LUT? */
    FL_PACKED lut_under;              /* underflow color */
    FL_PACKED lut_over;               /* overflow color */
    enum xg3d_color_mode color_mode;  /* coloring mode */
    enum col_grad_type grad_type;     /* gradient type */
    unsigned int hue;                 /* hue spin */
    enum col_predef_type predef_type; /* predefined palette type */
    unsigned int ncol;                /* number of colors in LUT */
    FL_PACKED lut[256];               /* LUT */
    int use_lut;                      /* use LUT from file? */
    uint8_t lutfile[768];             /* LUT from file */
    char *lut_name;                   /* name of LUT from file */
    int ramp_active[3];               /* RGB ramp active flags */
    int ramp_smooth[3];               /* RGB ramp smooth flags */
    int ramp_n[3];                    /* RGB ramp number of values */
    float *ramp_x[3];                 /* RGB ramp x values */
    float *ramp_y[3];                 /* RGB ramp y values */
} xg3d_coloring_settings;

void setup_coloring_settings(FD_coloring*,const unsigned long,const xg3d_coloring_settings*);
xg3d_coloring_settings* store_coloring_settings(const FD_coloring*,const unsigned long);

#define COLORING_MODE(fd_coloring) ACTIVE_FOLDER(((FD_coloring*)(fd_coloring))->tf_coloring)
#define COLORING_MODE_FDUI(fd_coloring,mode) (fl_get_tabfolder_folder_bynumber(((FD_coloring*)(fd_coloring))->tf_coloring,mode)->fdui)
#define COLORING_LUT(fd_coloring) (((FD_coloring*)(fd_coloring))->vdata)
#define COLORING_LUT_IMG(fd_coloring) (((FD_coloring*)(fd_coloring))->lut_pix->u_vdata)
#define COLORING_LUT_UNDER(fd_coloring) (((FD_coloring*)(fd_coloring))->btn_under->u_ldata)
#define COLORING_LUT_OVER(fd_coloring) (((FD_coloring*)(fd_coloring))->btn_over->u_ldata)
#define COLORING_NCOL(fd_coloring) (((xg3d_lut*)COLORING_LUT(fd_coloring))->ncol)
#define COLORING_UNDERFLOW(fd_coloring) (((xg3d_lut*)COLORING_LUT(fd_coloring))->underflow)
#define COLORING_OVERFLOW(fd_coloring) (((xg3d_lut*)COLORING_LUT(fd_coloring))->overflow)
#define COLORING_TAB_IDX(fd_col_table) (((FD_col_table*)(fd_col_table))->tab_canvas->u_ldata)
#define COLORING_TAB_IMG(fd_col_table) (((FD_col_table*)(fd_col_table))->tab_canvas->u_vdata)
#define COLORING_LUTFILE(fd_col_table) (((FD_col_table*)(fd_col_table))->btn_lutfile->u_vdata)
#define COLORING_USE_LUTFILE(fd_col_table) (fl_get_button(((FD_col_table*)(fd_col_table))->btn_lutfile))
#define COLORING_GRAD_TYPE(fd_col_grad) (((FD_col_grad*)(fd_col_grad))->ldata)
#define COLORING_PREDEF_TYPE(fd_col_predef) (((FD_col_predef*)(fd_col_predef))->ldata)
#define COLORING_GET_HUE(fd_col_grad) (fl_get_slider_value(((FD_col_grad*)(fd_col_grad))->sli_hue))
#define COLORING_SET_HUE(fd_col_grad,hue) (fl_set_slider_value(((FD_col_grad*)(fd_col_grad))->sli_hue,hue))

void update_coloring(FD_coloring*,const unsigned int);
void update_palette_interval(FD_col_table*,FD_quantization*,const xg3d_header*);
void update_lut_pix(FD_coloring*);
void refresh_flow_pix(FL_OBJECT*,const FL_PACKED);
void init_ramp_plots(FD_col_ramp*);
FL_HANDLE post_ramp_red;
FL_HANDLE post_ramp_green;
FL_HANDLE post_ramp_blue;

int cb_lut_canvas_expose(FL_OBJECT*,Window,int,int,XEvent*,void*);
int cb_tab_canvas(FL_OBJECT*,Window,int,int,XEvent*,void*);

#endif
