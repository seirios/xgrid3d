#!/bin/bash

[ -z "$1" ] && echo "$0: No filename given, exiting." && exit

fd_name="$1"
fd_filename="${fd_name}.fd"
cb_name="${fd_name}_cb"

if [ -z "$2" ]; then
    form_name="$fd_name"
else
    form_name="$2"
fi

read -r -d '' fd_template <<EOF
Magic: 15000

Internal Form Definition File
    (do not change)

Number of forms: 1
Unit of measure: FL_COORD_PIXEL

=============== FORM ===============
Name: ${form_name}
Width: 320
Height: 250
Number of Objects: 1

--------------------
class: FL_BOX
type: FL_FLAT_BOX
box: 0 0 320 250
boxtype: FL_FLAT_BOX
colors: FL_COL1 FL_COL1
alignment: FL_ALIGN_CENTER
style: FL_NORMAL_STYLE
size: FL_DEFAULT_SIZE
lcol: FL_BLACK
label: 
shortcut: 
resize: FL_RESIZE_ALL
gravity: FL_NoGravity FL_NoGravity
name: 
callback: 
argument: 

==============================
create_the_forms
EOF

read -r -d '' cb_c_template <<EOF
#include "xgrid3d.h"
#include "${cb_name}.h"
EOF

read -r -d '' cb_h_template <<EOF
#ifndef XG3D_${cb_name}_h_
#define XG3D_${cb_name}_h_

#include "${fd_name}_fd.h"

#endif
EOF

# fd
echo "$fd_template" > "${fd_filename}"
# fd_c fd_h
./xforms/bin/fdesign -gettext -convert "${fd_filename}"
# cb_c
echo "$cb_c_template" > "${cb_name}.cm"
# cb_h
echo "$cb_h_template" > "${cb_name}.h"
