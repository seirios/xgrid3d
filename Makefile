# egs++ specific
ifdef EGS_CONFIG
include $(EGS_CONFIG)
include $(SPEC_DIR)egspp1.spec
include $(SPEC_DIR)egspp_$(my_machine).conf
USER_CODE := dosxyzpp
EGS_BASE_APPLICATION := egs_advanced_application
DOSE_LIBS := -L$(abs_dso) -lgfortran -legspp
STATICLIBS_DOSE := $(addprefix dosxyzpp/,$(egs_lib_objects)) $(addprefix dosxyzpp/,$(user_lib_objects))
endif

PREFIX ?= /usr
# progs
SHELL := /bin/bash
CC := c99
CXX := g++
RM := rm -f
GIT := git
MAKE := make
INSTALL := install
INSTALL_PROG := $(INSTALL) -m755 -s
INSTALL_DATA := $(INSTALL) -m644
# gettext stuff
PKG_NAME := xgrid3d
PKG_VERSION := 0.3
XGETTEXT := xgettext -LC --from-code=utf-8 --force-po -k_ --keyword="_p:1c,2" --copyright-holder="Sirio Bolaños Puchet" \
	--package-name="$(PKG_NAME)" --package-version="$(PKG_VERSION)"
MSGCAT := msgcat
MSGFMT := msgfmt
MSGMERGE := msgmerge
MSGATTRIB := msgattrib
# static libraries
STATICLIBS_REG := niftyreg/lib/lib_reg_blockMatching.a niftyreg/lib/lib_reg_globalTransformation.a niftyreg/lib/lib_reg_maths.a \
				 niftyreg/lib/lib_reg_tools.a niftyreg/lib/lib_reg_resampling.a niftyreg/lib/lib_reg_aladin.a
STATICLIBS := xforms/lib/libflimage.a xforms/lib/libforms.a libgrid3d/lib/libgrid3d.a sort/lib/sort_uint32_t.a \
			 insitu/chanhull.o insitu/heaphull.o libmatheval/lib/libmatheval.a nifticlib/lib/libniftiio.a \
			 nifticlib/lib/libznz.a gnuplot_i/gnuplot_i.o nrrdio/lib/libNrrdIO.a $(STATICLIBS_REG) $(STATICLIBS_DOSE)
# system shared libraries
PKG_BABL := babl-0.1
GSL_LIBS := $(shell gsl-config --libs)
BABL_LIBS := $(shell pkg-config $(PKG_BABL) --libs)
FFTW3_LIBS := $(shell pkg-config fftw3 --libs)
X11_LIBS := $(shell pkg-config x11 --libs)
XPM_LIBS := $(shell pkg-config xpm --libs)
XEXT_LIBS := $(shell pkg-config xext --libs)
XRENDER_LIBS := $(shell pkg-config xrender --libs)
ZLIB_LIBS := $(shell pkg-config zlib --libs)
BZLIB_LIBS = -lbz2
LZMA_LIBS := $(shell pkg-config liblzma --libs)
CUDA_LIBS := -L/opt/cuda/lib64 -lcudart -lcufft -pthread -lz
LIBS = $(X11_LIBS) $(XPM_LIBS) $(XEXT_LIBS) $(XRENDER_LIBS) $(ZLIB_LIBS) $(BZLIB_LIBS) $(LZMA_LIBS) \
	   $(FFTW3_LIBS) $(GSL_LIBS) $(BABL_LIBS) $(DOSE_LIBS)
# compilation flags
LDFLAGS += -Wl,--as-needed
INCFLAGS_REG := -Iniftyreg/include -Inifticlib/include
INCFLAGS_ASTRA := -I. -Iastra-toolbox -Iastra-toolbox/include -I/opt/cuda/include
INCFLAGS_DOSE = -Idosxyzpp $(INCL)
BABL_CFLAGS := $(shell pkg-config $(PKG_BABL) --cflags)
GSL_CFLAGS := $(shell gsl-config --cflags)
INCFLAGS = -Iicons -Ixforms/include -Ilibgrid3d/include -Iinsitu -Ilibmatheval/include -Inifticlib/include \
		   -Ignuplot_i/src -Inrrdio $(BABL_CFLAGS) $(GSL_CFLAGS)
WBASEFLAGS = -Wall -Wextra -pedantic -Winline -Wno-format-truncation
WFLAGS = $(WBASEFLAGS) -Werror
MARCH := native
ifdef DEBUG
OPTIMFLAGS := -O0 -ggdb3
else
OPTIMFLAGS := -march=$(MARCH) -O3 -DNDEBUG
endif
OMPFLAGS := -fopenmp
OMPLIBS := -lfftw3_omp
FFLAGS := -fno-unsafe-math-optimizations
FFLAGS_REG := -fPIC -O3 -D_BUILD_NR_DEV
FFLAGS_ASTRA := -march=$(MARCH) -O3 -DASTRA_CUDA
OMPFLAGS_REG := -fopenmp
OMPFLAGS_ASTRA := -fopenmp
DFLAGS := -D_POSIX_C_SOURCE=200809L -D_DEFAULT_SOURCE -D_BSD_SOURCE# source features
DFLAGS += -DHAVE_RENDER_H -DHAVE_SHAPE_H -DENABLE_NLS#XXX: these all should be the output of tests
DFLAGS_CB := -DFORMS
BUILDFLAGS = $(WFLAGS) $(OPTIMFLAGS) $(FFLAGS) $(OMPFLAGS) $(DFLAGS)
LINKFLAGS = $(OPTIMFLAGS) $(LIBS) $(OMPLIBS) $(OMPFLAGS)

ifdef EGS_CONFIG
OBJ_DOSE = dosxyzpp_c.o
DFLAGS += -DEGSPP
LDFLAGS := $(LDFLAGS),-rpath,$(abs_dso)
endif

ifeq ($(OS),Windows_NT)
LIBS += -lintl
endif

STATIC_DEPS = xgrid3d.h util_nuclides.h util_units.h icons/*.xbm icons/*.xpm color_predefined.h libgrid3d/include/*.h
OBJ_MAIN := xgrid3d.o io.o setup_forms.o
HDR_HM := cb_coloring.h util.h util_nuclides.h util_symbols.h xgrid3d.h astra_reco.h
OBJ_FD := $(addprefix fd_,$(subst .fd,.o,$(wildcard *.fd)))
OBJ_CB := $(subst .cm,.o,$(wildcard cb_*.cm))
OBJ_DO := selection.o operations.o rois.o coloring.o
OBJ_EXT := conrec.o dl.o bresenham.o toconic.o ctheader.o
OBJ_UTIL := util.o util_markers.o util_filter.o util_fit2d.o util_histogram.o util_egs.o util_bezier.o \
	   util_linsimp.o util_symbols.o util_nuclides.o util_fit3d.o \
	   bernstein_polynomial.o color_gradient.o color_uniform.o luoetal2006.o ciecam02.o
OBJ_REG := niftyreg_c.o overlay_reg.o
OBJ_KER := gengrid.o pardose.o imgtor.o

ifdef ASTRA
OBJ_ASTRA = astra_reco.o astra-toolbox/cpp/creators.o astra-toolbox/lib/libastra.a
LIBS += $(CUDA_LIBS)
DFLAGS_CB += -DASTRA
else
OBJ_ASTRA =
endif

OBJ = $(OBJ_FD) $(OBJ_DO) $(OBJ_CB) $(OBJ_MAIN) $(OBJ_EXT) $(OBJ_UTIL) $(OBJ_REG) $(OBJ_KER) $(OBJ_DOSE) $(OBJ_ASTRA)

LANGS = es

# Wrap function
wrap=$(addsuffix $3,$(addprefix $1,$2))

.PHONY: all
all: xgrid3d-build

# Git
.PHONY: git-setup git-update git-maintainer-update

git-setup:
	$(GIT) submodule init
	$(GIT) submodule update
	cd cmod && $(MAKE) git-init

git-update:
	$(GIT) pull
	$(GIT) submodule update

git-maintainer-update:
	$(GIT) pull
	$(GIT) submodule update --remote
	$(GIT) submodule foreach -q --recursive \
	'echo $$name; git checkout $$(git config -f $$toplevel/.gitmodules --get submodule.$$name.branch || echo master); git pull'

# Build

PROJECTS := cmod astra dosxyzpp gnuplot_i nifticlib nrrdio niftyreg \
	xforms libmatheval libgrid3d
.PHONY: $(PROJECTS)

PROJECTS_BUILD := $(addsuffix -build,$(PROJECTS))
.PHONY: build $(PROJECTS_BUILD)
build: $(PROJECTS_BUILD)

PROJECTS_CLEAN := $(addsuffix -clean,$(PROJECTS))
.PHONY: build-clean $(PROJECTS_CLEAN)
build-clean: $(PROJECTS_CLEAN)

# C%

cmod/cmod: cmod/Makefile
	cd cmod && $(MAKE)

cmod-build: cmod/cmod

cmod-clean: cmod/Makefile
	cd cmod && $(MAKE) clean

# ASTRA

astra-toolbox/build/linux/configure:
	cd $(dir $@) && ./autogen.sh

astra-toolbox/build/linux/Makefile: astra-toolbox/build/linux/configure
	cd $(dir $@) && ./$(notdir $<) --prefix="`pwd`/../../" --enable-static --disable-shared --with-cuda=/opt/cuda

ifdef ASTRA
astra-build: astra-toolbox/build/linux/Makefile
	cd astra-toolbox/build/linux && $(MAKE) -j install
	cd astra-toolbox/cpp && $(MAKE) -j all
	cd astra-toolbox/samples/cpp && $(MAKE) -j all

astra-clean:
	cd astra-toolbox/lib && $(RM) libastra.*
	cd astra-toolbox/build/linux && $(MAKE) distclean
	cd astra-toolbox/cpp && $(MAKE) clean
	cd astra-toolbox/samples/cpp && $(MAKE) clean

astra-toolbox/cpp/creators.o:
	cd astra-toolbox/cpp && $(MAKE)

astra-toolbox/lib/libastra.a:
	cd astra-toolbox/build/linux && $(MAKE) -j install
else
.SILENT: astra-build astra-clean
astra-build:
	echo "ASTRA undefined, astra-toolbox not built"
astra-clean:
	echo "ASTRA undefined, astra-toolbox not built"
endif

# DOSXYZPP

ifdef EGS_CONFIG
dosxyzpp-build:
	cd dosxyzpp && $(MAKE) -j

dosxyzpp-clean:
	cd dosxyzpp && $(MAKE) cleanall
else
.SILENT: dosxyzpp-build dosxyzpp-clean
dosxyzpp-build:
	echo "EGS_CONFIG undefined, dosxyzpp not built"
dosxyzpp-clean:
	echo "EGS_CONFIG undefined, dosxyzpp not built"
endif

# gnuplot_i

gnuplot_i-build:
	cd gnuplot_i && $(MAKE)

gnuplot_i-clean:
	cd gnuplot_i && $(MAKE) clean

# insitu

insitu/heaphull.o insitu/chanhull.o:
	cd $(dir $@) && $(MAKE) $(notdir $@)

insitu-clean:
	cd insitu && $(MAKE) clean

# sort

sort/include/sort_%.h:
	cd sort && $(MAKE) $*

sort/lib/sort_%.a: sort/include/sort_%.h

sort-clean:
	cd sort && $(MAKE) clean

# nifticlib

nifticlib-build:
	cd nifticlib && $(MAKE) SHELL=bash ARCH=X86_64 znz nifti
	cd nifticlib && $(MAKE) SHELL=bash ARCH=X86_64 znz_install nifti_install

nifticlib-clean:
	cd nifticlib && $(MAKE) SHELL=bash ARCH=X86_64 clean-all

# NiftyReg

niftyreg/build/Makefile:
	mkdir -p $(dir $@)
	cd $(dir $@) && cmake \
		-DCMAKE_INSTALL_PREFIX=.. \
		-DBUILD_TESTING=OFF \
		-DBUILD_NR_SLICER_EXT=OFF \
		-DBUILD_NR_DEV=ON \
		-DUSE_SSE=OFF \
		-DUSE_DOUBLE=ON \
		-DUSE_OPENMP=ON \
		..

niftyreg-build: niftyreg/build/Makefile
	cd niftyreg/build/reg-lib && $(MAKE) -j install

niftyreg-clean:
	rm -rf niftyreg/build niftyreg/include niftyreg/lib

# NrrdIO

nrrdio/build/Makefile:
	mkdir -p $(dir $@)
	cd $(dir $@) && cmake \
		-DQNANHIBIT=1 \
		..

nrrdio-build: nrrdio/build/Makefile
	cd nrrdio/build && $(MAKE) C_FLAGS="-DTEEM_ZLIB=1" -j
	install -d nrrdio/lib
	install -m644 nrrdio/build/libNrrdIO.a nrrdio/lib

nrrdio-clean:
	rm -rf nrrdio/build nrrdio/lib

# XForms

xforms/configure:
	cd $(dir $@) && ./autogen.sh

xforms/Makefile: xforms/configure
	cd $(dir $@) && CFLAGS='-Wall -Wextra -Werror' ./configure --prefix=`pwd` --disable-shared --enable-static --disable-gl --enable-optimization=-O2

xforms/bin/fdesign: xforms/Makefile
	cd xforms && $(MAKE) fdesign
	cd xforms/fdesign && $(MAKE) install

xforms-build: xforms/Makefile
	cd xforms && $(MAKE) -j install

xforms-clean:
	cd xforms && $(MAKE) uninstall && $(MAKE) distclean

# libmatheval

libmatheval/configure:
	cd $(dir $@) && ./autogen.sh

libmatheval/Makefile: libmatheval/configure
	cd $(dir $@) && ./configure --prefix=`pwd` --disable-shared --enable-static --enable-gsl \

libmatheval-build: libmatheval/Makefile
	cd libmatheval && $(MAKE) -i
	cd libmatheval && $(MAKE) install

libmatheval-clean:
	cd libmatheval && $(MAKE) uninstall && $(MAKE) distclean

# libgrid3d

libgrid3d/configure:
	cd $(dir $@) && ./autogen.sh

libgrid3d/Makefile: libgrid3d/configure
	cd $(dir $@) && CC="c99" CFLAGS="-O3 -march=$(MARCH) -DFORMS" \
		CPPFLAGS="-D_POSIX_C_SOURCE=200809L -I../../xforms/include" \
		./configure --prefix=`pwd` --disable-shared --enable-static

libgrid3d-build: libgrid3d/Makefile
	cd libgrid3d &&	$(MAKE) install

libgrid3d-clean:
	cd libgrid3d && $(MAKE) uninstall && $(MAKE) distclean

# xgrid3d
.PHONY: xgrid3d-build xgrid3d-clean

xgrid3d-build: $(HDR_HM)
	$(MAKE) locale xgrid3d

xgrid3d-clean: clean-obj clean-pot clean-bak clean-fdcode clean-locale clean-cb

# C% processing

CMOD := cmod/cmod
CMOD_INCFLAGS += -Icmod/include

%.h: %.hm
	$(CMOD) $(CMODFLAGS) $(CMOD_INCFLAGS) -o $@ $<

%.c: %.cm %.h
	$(CMOD) $(CMODFLAGS) $(CMOD_INCFLAGS) -o $@ $<

%.cpp: %.cmm %.h
	$(CMOD) $(CMODFLAGS) $(CMOD_INCFLAGS) -o $@ $<

# special cases

cb_datasets.c cb_dialog.c io.c: CMOD_INCFLAGS += -Ilibgrid3d/lib

# objects

ifdef ASTRA
astra_reco.o: astra_reco.h astra_reco.cpp astra-toolbox/cpp/creators.o astra-toolbox/lib/libastra.a
	$(CXX) -c -o $@ astra_reco.cpp -Wall -pedantic $(FFLAGS_ASTRA) $(INCFLAGS_ASTRA) $(OMPFLAGS_ASTRA)
endif

niftyreg_c.o: niftyreg_c.h niftyreg_c.cpp niftyreg/lib/lib_reg_aladin.a
	$(CXX) -c -o $@ niftyreg_c.cpp $(WFLAGS) $(FFLAGS_REG) $(INCFLAGS_REG) $(OMPFLAGS_REG)

dosxyzpp_c.o: dosxyzpp_c.h dosxyzpp_c.cpp dosxyzpp/dosxyzpp.hpp dosxyzpp/dosxyzpp.cpp
	$(CXX) -c -o $@ dosxyzpp_c.cpp -Wall -Wno-sign-compare $(FFLAGS_DOSE) $(INCFLAGS_DOSE) $(OMPFLAGS_DOSE)

overlay_reg.o: overlay_reg.c overlay_reg.h niftyreg_c.h
	$(CC) -c -o $@ $< $(BUILDFLAGS) $(INCFLAGS) $(INCFLAGS_REG)

gengrid.o: gengrid.c gengrid.h sort/include/sort_uint32_t.h
	$(CC) -c -o $@ $< $(WFLAGS) $(OPTIMFLAGS) -Isort/include

pardose.o: pardose.c pardose.h gengrid.o
	$(CC) -c -o $@ $< $(WFLAGS) $(OPTIMFLAGS)

imgtor.o: imgtor.c imgtor.h gengrid.o
	$(CC) -c -o $@ $< $(WFLAGS) $(OPTIMFLAGS) $(OMPFLAGS)

cb_menu.c: about.h

# Parts
.PHONY: gen-fd obj-fd obj-cb obj-ext obj-util obj-main

gen-fd: $(OBJ_FD:.o=.c)

obj-fd: $(OBJ_FD)

obj-cb: $(OBJ_CB)

obj-ext: $(OBJ_EXT)

obj-util: $(OBJ_UTIL)

obj-main: $(OBJ_MAIN)

# FDesign stuff

.SILENT: xgrid3d_fd.h xgrid3d_cb.h
xgrid3d_fd.h: $(OBJ_FD:.o=.h)
	truncate -s0 $@
	for fdh in $^; do\
		echo "#include \"$${fdh}\"" >> $@;\
	done

xgrid3d_cb.h: $(OBJ_CB:.o=.h)
	truncate -s0 $@
	for cbh in $^; do\
		echo "#include \"$${cbh}\"" >> $@;\
	done

$(OBJ_FD:.o=.c): fd_%.c : %.fd xforms/bin/fdesign
	$(word 2,$^) -gettext -convert $<

$(OBJ_FD:.o=.h): fd_%.h : %.fd xforms/bin/fdesign
	$(word 2,$^) -gettext -convert $<

$(OBJ_FD): %.o : %.c %.h
	$(CC) -c -o $@ $< -Os $(WBASEFLAGS) $(DFLAGS) -Iicons -Ixforms/include

$(addprefix po/,$(OBJ_FD:.o=.pot)): po/%.pot : %.c
	$(XGETTEXT) -o $@ $< 2> /dev/null

$(OBJ_CB): cb_%.o : cb_%.c cb_*.h fd_%.h $(STATIC_DEPS) libgrid3d/lib/libgrid3d.a
	$(CC) $(INCFLAGS) -c -o $@ $< $(BUILDFLAGS) $(DFLAGS_CB)

$(addprefix po/,$(OBJ_CB:.o=.pot)): po/%.pot : %.c
	$(XGETTEXT) -o $@ $< 2> /dev/null

$(OBJ_DO): %.o : %.c %.h
	$(CC) $(INCFLAGS) -c -o $@ $< $(BUILDFLAGS)

$(addprefix po/,$(OBJ_MAIN:.o=.pot)): po/%.pot : %.c
	$(XGETTEXT) -o $@ $< 2> /dev/null

$(OBJ_UTIL): %.o : %.c %.h
	$(CC) $(INCFLAGS) -c -o $@ $< $(BUILDFLAGS)

cb_operations.c: CMOD_INCFLAGS += -Ilibgrid3d/lib

# OBJ_MAIN

setup_forms.h: xgrid3d_cb.h

setup_forms.o: setup_forms.c setup_forms.h $(STATIC_DEPS) $(OBJ_FD:.o=.h) $(OBJ_CB:.o=.h) util.h
	$(CC) $(INCFLAGS) -c -o $@ $< -Os $(WFLAGS) $(DFLAGS)

xgrid3d.h: xgrid3d_fd.h xgrid3d_forms.hm

xgrid3d.o io.o: %.o : %.c %.h
	$(CC) $(INCFLAGS) -c -o $@ $< $(BUILDFLAGS) $(DFLAGS)

$(OBJ_EXT): %.o : %.c %.h $(STATIC_DEPS)
	$(CC) $(INCFLAGS) -c -o $@ $< $(BUILDFLAGS) $(DFLAGS)

# Locale
.PHONY: $(LANGS) locale

po/all.pot: $(addprefix po/,$(OBJ_FD:.o=.pot) $(OBJ_CB:.o=.pot) $(OBJ_MAIN:.o=.pot))
	$(MSGCAT) --to-code=utf-8 $^ -o po/all.pot 2>/dev/null

$(addprefix po/,$(LANGS:=.po)): po/all.pot
	$(MSGMERGE) -U $@ $< 2>/dev/null
	$(MSGATTRIB) --no-obsolete $@ -o $@ 2>/dev/null

locale/%/LC_MESSAGES/xgrid3d.mo: po/%.po
	mkdir -p $(dir $@)
	$(MSGFMT) -o locale/$*/LC_MESSAGES/xgrid3d.mo $<

$(LANGS): %: locale/%/LC_MESSAGES/xgrid3d.mo

locale: $(LANGS)

xgrid3d: $(OBJ) $(STATICLIBS)
	$(CXX) $(LDFLAGS) $^ -o $@ $(LINKFLAGS) $(DFLAGS)

# Utils
.PHONY: utils
utils: utils/slicer

utils/slicer: utils/slicer.c | coloring.h
	cd $(dir $@) && $(MAKE) $(notdir $@)

# Installation
.PHONY: install install-bin install-locale install-data

install: install-bin install-locale install-data

install-bin: $(PREFIX)/bin/xgrid3d

$(PREFIX)/bin/xgrid3d: xgrid3d
	$(INSTALL_PROG) $< $@

install-locale: $(call wrap,$(PREFIX)/share/locale/,$(LANGS),/LC_MESSAGES/xgrid3d.mo)

$(PREFIX)/share/locale/%/LC_MESSAGES/xgrid3d.mo: locale/%/LC_MESSAGES/xgrid3d.mo
	$(INSTALL_DATA) $< $@

install-data: $(PREFIX)/share/xgrid3d/kernel $(call wrap,$(PREFIX)/share/xgrid3d/kernel/,$(shell ls kernel),)

$(PREFIX)/share/xgrid3d/kernel: kernel
	$(INSTALL) -d $< $@

$(PREFIX)/share/xgrid3d/kernel/%: kernel/%
	$(INSTALL_DATA) $< $@

install-utils: $(PREFIX)/bin/slicer

$(PREFIX)/bin/slicer: utils/slicer
	$(INSTALL_PROG) $< $@

# Cleanup
.PHONY: clean clean-bin clean-obj clean-cb clean-pot clean-fdcode clean-bak \
	clean-fd clean-locale clean-ch

clean: clean-bin clean-obj clean-cb clean-pot clean-fdcode clean-bak clean-ch

clean-bin:
	$(RM) xgrid3d

clean-obj:
	$(RM) $(OBJ) $(OBJ_MAIN) astra_reco.o

clean-cb:
	$(RM) $(OBJ_CB) $(OBJ_CB:.o=.c)

clean-pot:
	cd po; $(RM) $(OBJ_FD:.o=.pot) $(OBJ_CB:.o=.pot) $(OBJ_MAIN:.o=.pot) all.pot; cd ..

clean-fdcode:
	$(RM) $(OBJ_FD:.o=.c) $(OBJ_FD:.o=.h)

clean-bak:
	$(RM) *~ *.bak

clean-fd:
	$(RM) $(OBJ_FD)

clean-locale:
	$(RM) -r locale

# clean additional C% generated sources
clean-ch:
	$(RM) coloring.c io.c selection.c setup_forms.c util.c util_filter.c util_fit2d.c xgrid3d.c
	$(RM) astra_reco.h cb_coloring.h cb_selection.h cb_viewport.h coloring.h util.h util_nuclides.h util_symbols.h xgrid3d.h

# Remake
.PHONY: remake remake-fd remake-cb relink

remake: clean all

remake-fd: clean-fd obj-fd all

remake-cb: clean-cb obj-cb all

relink: clean-bin xgrid3d
