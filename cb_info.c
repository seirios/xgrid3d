#include <libgen.h>
#include <math.h>
#include <errno.h>

#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "cb_info.h"
#include "cb_datasets.h"
#include "cb_viewport.h"
#include "cb_mainview.h"
#include "cb_measure.h"
#include "cb_selection.h"
#include "fd_coloring.h"
#include "cb_rois.h"
#include "cb_dose.h"
#include "cb_operations.h"
#include "cb_overlay.h"
#include "io.h"

#include "util.h"
#include "util_coordinates.h"

void deactivate_info(FD_info *info) {
    fl_hide_object(info->btn_zero);
    fl_hide_object(info->btn_middle);
    fl_hide_object(info->btn_pick);
    fl_hide_object(info->btn_show);

    fl_deactivate_object(info->ch_acqmod);
    fl_set_object_boxtype(info->ch_acqmod,FL_BORDER_BOX);
    fl_set_focus_object(info->info,info->ch_acqmod);

    fl_deactivate_object(info->inp_vx);
	fl_set_input_color(info->inp_vx,FL_BLACK,FL_MCOL);
	fl_set_object_boxtype(info->inp_vx,FL_FLAT_BOX);
    fl_deactivate_object(info->inp_vy);
	fl_set_input_color(info->inp_vy,FL_BLACK,FL_MCOL);
	fl_set_object_boxtype(info->inp_vy,FL_FLAT_BOX);
    fl_deactivate_object(info->inp_vz);
    fl_set_input_color(info->inp_vz,FL_BLACK,FL_MCOL);
    fl_set_object_boxtype(info->inp_vz,FL_FLAT_BOX);

    fl_deactivate_object(info->spin_ox);
    fl_hide_object(fl_get_spinner_up_button(info->spin_ox));
    fl_hide_object(fl_get_spinner_down_button(info->spin_ox));
    fl_set_input(fl_get_spinner_input(info->spin_ox),"");
    fl_set_object_boxtype(fl_get_spinner_input(info->spin_ox),FL_FLAT_BOX);
    fl_deactivate_object(info->spin_oy);
    fl_hide_object(fl_get_spinner_up_button(info->spin_oy));
    fl_hide_object(fl_get_spinner_down_button(info->spin_oy));
    fl_set_input(fl_get_spinner_input(info->spin_oy),"");
    fl_set_object_boxtype(fl_get_spinner_input(info->spin_oy),FL_FLAT_BOX);
    fl_deactivate_object(info->spin_oz);
    fl_hide_object(fl_get_spinner_up_button(info->spin_oz));
    fl_hide_object(fl_get_spinner_down_button(info->spin_oz));
    fl_set_input(fl_get_spinner_input(info->spin_oz),"");
    fl_set_object_boxtype(fl_get_spinner_input(info->spin_oz),FL_FLAT_BOX);
}

static void activate_info(FD_info *info) {
    fl_show_object(info->btn_zero);
    fl_show_object(info->btn_middle);
    fl_show_object(info->btn_pick);
    fl_show_object(info->btn_show);

    fl_activate_object(info->ch_acqmod);
    fl_set_object_boxtype(info->ch_acqmod,FL_UP_BOX);

    fl_activate_object(info->inp_vx);
    fl_set_object_boxtype(info->inp_vx,FL_DOWN_BOX);
    fl_set_input_color(info->inp_vx,FL_BLACK,FL_BLUE);
    fl_activate_object(info->inp_vy);
    fl_set_object_boxtype(info->inp_vy,FL_DOWN_BOX);
    fl_set_input_color(info->inp_vy,FL_BLACK,FL_BLUE);
    fl_activate_object(info->inp_vz);
    fl_set_object_boxtype(info->inp_vz,FL_DOWN_BOX);
    fl_set_input_color(info->inp_vz,FL_BLACK,FL_BLUE);

    fl_activate_object(info->spin_ox);
    fl_show_object(fl_get_spinner_up_button(info->spin_ox));
    fl_show_object(fl_get_spinner_down_button(info->spin_ox));
    fl_set_object_boxtype(fl_get_spinner_input(info->spin_ox),FL_DOWN_BOX);
    fl_activate_object(info->spin_oy);
    fl_show_object(fl_get_spinner_up_button(info->spin_oy));
    fl_show_object(fl_get_spinner_down_button(info->spin_oy));
    fl_set_object_boxtype(fl_get_spinner_input(info->spin_oy),FL_DOWN_BOX);
    fl_activate_object(info->spin_oz);
    fl_show_object(fl_get_spinner_up_button(info->spin_oz));
    fl_show_object(fl_get_spinner_down_button(info->spin_oz));
    fl_set_object_boxtype(fl_get_spinner_input(info->spin_oz),FL_DOWN_BOX);
}

void setup_info(FD_info *info, const xg3d_header *hdr) {
    if(hdr != NULL) {
        fl_set_input(info->inp_path,hdr->file);
        fl_set_object_helper(info->inp_path,fl_get_input(info->inp_path));
        set_type_text(info->txt_type,hdr->type,hdr->endian);
        fl_set_object_label_f(info->txt_grid,"x: %3u  y: %3u  z: %3u",hdr->dim[0],hdr->dim[1],hdr->dim[2]);
        fl_set_choice(info->ch_acqmod,hdr->acqmod+2);
        fl_set_input_f(info->inp_vx,"%.5g",hdr->vx);
        fl_set_input_f(info->inp_vy,"%.5g",hdr->vy);
        fl_set_input_f(info->inp_vz,"%.5g",hdr->vz);
        fl_set_spinner_bounds(info->spin_ox,0,hdr->dim[0]-1);
        fl_set_spinner_bounds(info->spin_oy,0,hdr->dim[1]-1);
        fl_set_spinner_bounds(info->spin_oz,0,hdr->dim[2]-1);
        fl_set_spinner_value(info->spin_ox,hdr->ox);
        fl_set_spinner_value(info->spin_oy,hdr->oy);
        fl_set_spinner_value(info->spin_oz,hdr->oz);
        activate_info(info);
        info->vdata = (void*)hdr;
    } else {
        fl_set_input(info->inp_path,"");
        fl_set_object_helper(info->inp_path,"");
        fl_set_object_label(info->txt_type,"");
        fl_set_object_label(info->txt_grid,"");
        fl_set_choice(info->ch_acqmod,1);
        fl_set_input(info->inp_vx,"");
        fl_set_input(info->inp_vy,"");
        fl_set_input(info->inp_vz,"");
        fl_set_input(fl_get_spinner_input(info->spin_ox),"");
        fl_set_input(fl_get_spinner_input(info->spin_oy),"");
        fl_set_input(fl_get_spinner_input(info->spin_oz),"");
        deactivate_info(info);
        info->vdata = NULL;
    }
}

#undef ENV
void update_volumes(xg3d_env *ENV, const xg3d_header *hdr) {
    if(ENV->data->main->rois != NULL)
        update_roi_info(ENV);
    if(GUI->viewport != NULL && GUI->selection != NULL)
        sel_update_info(GUI->viewport,GUI->selection);
    if(GUI->measure != NULL && GUI->viewport != NULL)
        update_measurement(GUI->measure,GUI->viewport);
    if(GUI->operations != NULL && DATA != NULL && hdr == DATA->hdr)
        update_op_crop_inputs(GUI->operations,DATA->hdr);
    if(GUI->selection != NULL)
        fl_call_object_callback(GUI->selection->ch_marker_units);
}
#define ENV _default_env

void cb_voxel_size ( FL_OBJECT * obj  , long data  ) {
	const char *txt = fl_get_input(obj);
    xg3d_header *hdr = INFO_HDR(obj->form->fdui);
	double val, *v = NULL;

	switch(data) {
		case 0: v = &hdr->vx; break; /* x */
		case 1: v = &hdr->vy; break; /* y */
		case 2: v = &hdr->vz; break; /* z */
	}

    val = strtod_checked(txt,*v);
    if(fpclassify(val) == FP_ZERO) val = *v;
	fl_set_input_f(obj,"%.5g",val);

	if(val != *v) {
		*v = val;
		update_volumes(ENV,hdr);
		update_ker_voxcolor(GUI->dose,hdr);
        refresh_datasets_info(GUI->datasets,ENV->data,true);
        if(DATA != NULL && hdr == DATA->hdr)
            setup_info(GUI->info,DATA->hdr);
	}
}

void cb_info_acqmod ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const enum xg3d_acq_mod acqmod = fl_get_choice(obj) - 2;
    xg3d_env *env_overlay = OVERLAY_ENV(GUI->overlay);
    xg3d_header *hdr = INFO_HDR(obj->form->fdui);
    
    hdr->acqmod = acqmod;
    if(DATA != NULL && hdr == DATA->hdr) {
        set_default_acq_settings(ENV,DATA->hdr,true);
        setup_info(GUI->info,DATA->hdr);
    } else if(fl_get_button(GUI->overlay->btn_show) && hdr == ENV->data->overlay->hdr)
        set_default_acq_settings(env_overlay,ENV->data->overlay->hdr,true);
    refresh_datasets_info(GUI->datasets,ENV->data,true);
}

void cb_origin_preset ( FL_OBJECT * obj  , long data  ) {
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    xg3d_header *hdr = INFO_HDR(obj->form->fdui);
    coord_t ox,oy,oz;
    coord_t *vox;

    switch(data) {
        default:
        case 0: /* Zero */
            ox = oy = oz = 0;
            break;
        case 1: /* Middle */
            ox = hdr->dim[0] / 2;
            oy = hdr->dim[1] / 2;
            oz = hdr->dim[2] / 2;
            break;
        case 2: /* Marker pick */
            if(DATA == NULL || hdr != DATA->hdr) return;
            if(SELECTION_NMK(GUI->selection) != 1) {
                fl_show_alert2(0,_("Other than one marker set!\fPlease set only one marker."));
                return;
            }
            vox = ((marker*)SELECTION_MKLIST(GUI->selection))->vox;
            ox = vox[PLANE_TO_J(plane)];
            oy = vox[PLANE_TO_I(plane)];
            oz = vox[PLANE_TO_K(plane)];
            break;
        case 3: /* Marker show */
            if(DATA == NULL || hdr != DATA->hdr) return;
            ox = hdr->ox;
            oy = hdr->oy;
            oz = hdr->oz;
            if(VIEW_K(GUI->viewport) != oz) {
                fl_set_slider_value(GUI->mainview->slider,oz);
                fl_call_object_callback(GUI->mainview->slider);
            }
            sel_blink_marker(GUI->viewport,GUI->selection,ox,oy,false);
            return;
            break;
    }

    hdr->ox = ox; fl_set_spinner_value(((FD_info*)obj->form->fdui)->spin_ox,ox);
    hdr->oy = oy; fl_set_spinner_value(((FD_info*)obj->form->fdui)->spin_oy,oy);
    hdr->oz = oz; fl_set_spinner_value(((FD_info*)obj->form->fdui)->spin_oz,oz);

    refresh_datasets_info(GUI->datasets,ENV->data,true);
    if(DATA != NULL && hdr == DATA->hdr)
        setup_info(GUI->info,DATA->hdr);
}

void cb_origin_spin ( FL_OBJECT * obj  , long data  ) {
    const coord_t val = fl_get_spinner_value(obj);
    const enum g3d_slice_plane plane = data;
    xg3d_header *hdr = INFO_HDR(obj->form->fdui);
    coord_t *o = THREEWAY_IF(plane==PLANE_XY,&hdr->ox,
                             plane==PLANE_XZ,&hdr->oy,
                                             &hdr->oz);

    *o = val;

    refresh_datasets_info(GUI->datasets,ENV->data,true);
    if(DATA != NULL && hdr == DATA->hdr)
        setup_info(GUI->info,DATA->hdr);
}
