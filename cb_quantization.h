#ifndef XG3D_quantization_cb_h_
#define XG3D_quantization_cb_h_

#include "fd_quantization.h"
#include "xgrid3d.h"

/* in same order as browser */
enum xg3d_quant_predef_type {
    QUANT_ORDMAG,
    QUANT_CT_ABDOMEN,
    QUANT_CT_BRAIN,
    QUANT_CT_EXTREMITIES,
    QUANT_CT_LIVER,
    QUANT_CT_LUNG,
    QUANT_CT_PELVIS,
    QUANT_CT_SKULL_BASE,
    QUANT_CT_SPINE_A,
    QUANT_CT_SPINE_B,
    QUANT_CT_THORAX
};

/* Quantization settings value mask bits */
/* quantization */
#define QUANTIZATION_SET_SCOPE       (1L<<0)
#define QUANTIZATION_CAL_UNITS       (1L<<1)
#define QUANTIZATION_HIST_LOG        (1L<<2)
#define QUANTIZATION_HIST_SKIP       (1L<<3)
#define QUANTIZATION_QUANTIZATION    (QUANTIZATION_SET_SCOPE | \
                                      QUANTIZATION_CAL_UNITS | \
                                      QUANTIZATION_HIST_LOG | \
                                      QUANTIZATION_HIST_SKIP)
/* quant_rng */
#define QUANTIZATION_RNG_TYPE        (1L<<4)
#define QUANTIZATION_RNG_MIN         (1L<<5)
#define QUANTIZATION_RNG_MAX         (1L<<6)
#define QUANTIZATION_QUANT_RNG       (QUANTIZATION_RNG_TYPE | \
                                      QUANTIZATION_RNG_MIN | \
                                      QUANTIZATION_RNG_MAX)
#define QUANTIZATION_QUANT_MODE      (1L<<7)
/* quant_intervals */
#define QUANTIZATION_NINT            (1L<<8)
#define QUANTIZATION_EXP             (1L<<9)
#define QUANTIZATION_CLAMP           (1L<<10)
#define QUANTIZATION_QUANT_INTERVALS (QUANTIZATION_QUANT_MODE | \
                                      QUANTIZATION_NINT | \
                                      QUANTIZATION_EXP | \
                                      QUANTIZATION_CLAMP)
/* quant_predef */
#define QUANTIZATION_SET_PREDEF      (1L<<11)
#define QUANTIZATION_QUANT_PREDEF    (QUANTIZATION_QUANT_MODE | \
                                      QUANTIZATION_SET_PREDEF)
/* quant_custom */
#define QUANTIZATION_BREAKPOINTS     (1L<<12)
#define QUANTIZATION_QUANT_CUSTOM    (QUANTIZATION_QUANT_MODE | \
                                      QUANTIZATION_BREAKPOINTS)

typedef struct {
    enum xg3d_scope scope;                   /* quantization scope */
    int cal_units;                           /* calibrated units? */
    int hist_log;                            /* log histrogram? */
    int hist_skip;                           /* skip first bin? */
    enum xg3d_quant_rng_type rng_type;       /* range selector type */
    double min;                              /* range minimum */
    double max;                              /* range maximum */
    enum xg3d_quant_mode quant_mode;         /* quantization mode */
    unsigned int nint;                       /* number of intervals */
    double exp;                              /* interval exponent */
    int clamp;                               /* clamp to bounds? */
    enum xg3d_quant_predef_type predef_type; /* predefined type */
    int nbkpts;                              /* number of breakpoints */
    double *bkpts;                           /* custom breakpoints */
} xg3d_quantization_settings;

void setup_quantization_settings(FD_quantization*,const unsigned long,const xg3d_quantization_settings*);
xg3d_quantization_settings* store_quantization_settings(const FD_quantization*,const unsigned long);

typedef struct {
    g3d_quant_info *info;  /* quantization info (set by compute_quantization) */
    enum xg3d_layer layer; /* layer we're operating on */
    double absmin;         /* data minimum (set by update_datarange) */
    double absmax;         /* data maximum (set by update_datarange) */
    double total;          /* data total (set by update_datarange) */
    double min;            /* quantization minimum (set by range sliders) */
    double max;            /* quantization maximum (set by range sliders) */
} xg3d_quantization_workspace;

#define QUANTIZATION_WORKSPACE(fd_quantization) ((xg3d_quantization_workspace*)(((FD_quantization*)(fd_quantization))->vdata))
#define QUANTIZATION_INFO(fd_quantization) ((g3d_quant_info*)(QUANTIZATION_WORKSPACE(fd_quantization)->info))
#define QUANTIZATION_NLEVELS(fd_quantization) (QUANTIZATION_INFO(fd_quantization)->nlevels)
#define QUANTIZATION_LAYER(fd_quantization) (QUANTIZATION_WORKSPACE(fd_quantization)->layer)
#define QUANTIZATION_ABSMIN(fd_quantization) (QUANTIZATION_WORKSPACE(fd_quantization)->absmin)
#define QUANTIZATION_ABSMAX(fd_quantization) (QUANTIZATION_WORKSPACE(fd_quantization)->absmax)
#define QUANTIZATION_TOTAL(fd_quantization) (QUANTIZATION_WORKSPACE(fd_quantization)->total)
#define QUANTIZATION_MIN(fd_quantization) (QUANTIZATION_WORKSPACE(fd_quantization)->min)
#define QUANTIZATION_MAX(fd_quantization) (QUANTIZATION_WORKSPACE(fd_quantization)->max)

#define QUANTIZATION_MODE(fd_quantization) ACTIVE_FOLDER((((FD_quantization*)(fd_quantization))->tf_qmode))
#define QUANTIZATION_MODE_FDUI(fd_quantization,mode) (fl_get_tabfolder_folder_bynumber(((FD_quantization*)(fd_quantization))->tf_qmode,mode)->fdui)
#define QUANTIZATION_HISTOGRAM(fd_quantization) (((FD_quantization*)(fd_quantization))->bit_hist->u_vdata)
#define QUANTIZATION_NBKPTS(fd_quant_custom) (((FD_quant_custom*)(fd_quant_custom))->ldata)
#define QUANTIZATION_BKPTS(fd_quant_custom) (((FD_quant_custom*)(fd_quant_custom))->vdata)
#define QUANTIZATION_GET_SCOPE(fd_quantization) (fl_get_button(((FD_quantization*)(fd_quantization))->btn_scope))
#define QUANTIZATION_GET_CALIB(fd_quantization) (fl_get_button(((FD_quantization*)(fd_quantization))->btn_units))
#define QUANTIZATION_PREDEF_TYPE(fd_quant_predef) (((FD_quant_predef*)(fd_quant_predef))->ldata)

xg3d_quantization_workspace* quantization_workspace_alloc(void);
void quantization_workspace_clear(xg3d_quantization_workspace*,const bool);

const char* quant_level_string(FD_quantization*,const uint8_t,const xg3d_header*,const bool);
void update_plot_exp(FL_OBJECT*,const double);
void set_range_inpsli(xg3d_env*,const bool);
void update_datarange(xg3d_env*,const bool);
void update_quant_coloring(FD_quantization*,FD_coloring*,const xg3d_header*);
g3d_ret compute_quantization(FD_quantization*,const grid3d*);

FL_HANDLE pre_quant_hist;

#endif
