#ifndef XG3D_util_histogram_h_
#define XG3D_util_histogram_h_

#include <inttypes.h>
#include <gsl/gsl_histogram.h>

#include "grid3d.h"
#include "grid3d_roi.h"

g3d_ret histogram_fill(gsl_histogram*,const grid3d*);
g3d_ret histogram_fill_sel(gsl_histogram*,const grid3d*,const grid3d*);
g3d_ret histogram_fill_roi(gsl_histogram*,const grid3d*,const g3d_roi*);
size_t otsu_threshold(const gsl_histogram*);
size_t nbins_biroz(const double*,const size_t);

#endif

