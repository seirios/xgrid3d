/* Form definition file generated by fdesign on Sat Sep 28 21:14:16 2024 */

#include <stdlib.h>
#include "fd_kernel.h"

#ifdef ENABLE_NLS
#include <libintl.h>
#define _(String) gettext (String)
#else
#define _(String) (String)
#endif


/***************************************
 ***************************************/

FD_kernel *
create_form_kernel( void )
{
    FL_OBJECT *obj;
    FD_kernel *fdui = ( FD_kernel * ) fl_malloc( sizeof *fdui );

    fdui->vdata = fdui->cdata = NULL;
    fdui->ldata = 0;

    fdui->kernel = fl_bgn_form( FL_NO_BOX, 465, 535 );

    obj = fl_add_box( FL_FLAT_BOX, 0, 0, 465, 535, "" );

    obj = fl_add_labelframe( FL_ENGRAVED_FRAME, 5, 430, 455, 70, _("Simulation parameters") );
    fl_set_object_lstyle( obj, FL_ITALIC_STYLE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    obj = fl_add_labelframe( FL_ENGRAVED_FRAME, 5, 345, 455, 75, _("Kernel parameters") );
    fl_set_object_lstyle( obj, FL_ITALIC_STYLE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    obj = fl_add_labelframe( FL_ENGRAVED_FRAME, 4, 8, 455, 102, _("EGSnrc paths") );
    fl_set_object_lstyle( obj, FL_ITALIC_STYLE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->btn_compute = obj = fl_add_button( FL_NORMAL_BUTTON, 5, 505, 225, 25, _("Compute dose voxel kernel") );
    fl_set_object_boxtype( obj, FL_ROUNDED3D_UPBOX );
    fl_set_object_color( obj, FL_YELLOW, FL_COL1 );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_kernel_compute, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_henhouse = obj = fl_add_button( FL_NORMAL_BUTTON, 395, 20, 60, 25, _("Choose") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_kernel_choose, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_egshome = obj = fl_add_button( FL_NORMAL_BUTTON, 395, 50, 60, 25, _("Choose") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_kernel_choose, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_pegsfile = obj = fl_add_button( FL_NORMAL_BUTTON, 395, 80, 60, 25, _("Load") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_kernel_pegsfile, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    obj = fl_add_labelframe( FL_ENGRAVED_FRAME, 5, 120, 455, 216, _("Emission parameters") );
    fl_set_object_lstyle( obj, FL_ITALIC_STYLE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->btn_spectrum = obj = fl_add_button( FL_NORMAL_BUTTON, 395, 140, 60, 25, _("Choose") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_kernel_choose, 2 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->br_phot = obj = fl_add_browser( FL_HOLD_BROWSER, 10, 185, 220, 100, _("Photons") );
    fl_set_object_lalign( obj, FL_ALIGN_TOP );
    fl_set_object_lstyle( obj, FL_BOLD_STYLE );
    fl_set_object_callback( obj, cb_kernel_emission_browser, 0 );
    fl_set_object_return( obj, FL_RETURN_SELECTION );
    fl_set_browser_hscrollbar( obj, FL_OFF );

    fdui->br_elec = obj = fl_add_browser( FL_HOLD_BROWSER, 235, 185, 220, 101, _("Electrons") );
    fl_set_object_lalign( obj, FL_ALIGN_TOP );
    fl_set_object_lstyle( obj, FL_BOLD_STYLE );
    fl_set_object_callback( obj, cb_kernel_emission_browser, 1 );
    fl_set_object_return( obj, FL_RETURN_SELECTION );
    fl_set_browser_hscrollbar( obj, FL_OFF );

    fdui->btn_plus_phot = obj = fl_add_button( FL_NORMAL_BUTTON, 10, 285, 20, 20, _("+") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_callback( obj, cb_kernel_emission_op, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    obj = fl_add_button( FL_NORMAL_BUTTON, 30, 285, 20, 20, _("-") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_callback( obj, cb_kernel_emission_op, 2 );
    fl_set_button_mouse_buttons( obj, 1 );

    obj = fl_add_button( FL_NORMAL_BUTTON, 145, 285, 40, 20, _("Load") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_callback( obj, cb_kernel_emission_op, 8 );
    fl_set_button_mouse_buttons( obj, 1 );

    obj = fl_add_button( FL_NORMAL_BUTTON, 185, 285, 45, 20, _("Save") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_callback( obj, cb_kernel_emission_op, 10 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_plus_elec = obj = fl_add_button( FL_NORMAL_BUTTON, 235, 285, 20, 20, _("+") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_callback( obj, cb_kernel_emission_op, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    obj = fl_add_button( FL_NORMAL_BUTTON, 255, 285, 20, 20, _("-") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_callback( obj, cb_kernel_emission_op, 3 );
    fl_set_button_mouse_buttons( obj, 1 );

    obj = fl_add_button( FL_NORMAL_BUTTON, 370, 285, 40, 20, _("Load") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_callback( obj, cb_kernel_emission_op, 9 );
    fl_set_button_mouse_buttons( obj, 1 );

    obj = fl_add_button( FL_NORMAL_BUTTON, 410, 285, 45, 20, _("Save") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_callback( obj, cb_kernel_emission_op, 11 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->ch_medium = obj = fl_add_choice( FL_NORMAL_CHOICE2, 230, 350, 225, 25, _("Medium") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_choice_align( obj, FL_ALIGN_LEFT );

    fdui->txt_nvox = obj = fl_add_text( FL_NORMAL_TEXT, 405, 385, 50, 25, _("21") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    obj = fl_add_button( FL_NORMAL_BUTTON, 100, 285, 45, 20, _("Clear") );
    fl_set_object_color( obj, FL_INDIANRED, FL_COL1 );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_callback( obj, cb_kernel_emission_op, 6 );
    fl_set_button_mouse_buttons( obj, 1 );

    obj = fl_add_button( FL_NORMAL_BUTTON, 325, 285, 45, 20, _("Clear") );
    fl_set_object_color( obj, FL_INDIANRED, FL_COL1 );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_callback( obj, cb_kernel_emission_op, 7 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->txt_lobnd = obj = fl_add_text( FL_NORMAL_TEXT, 250, 385, 90, 25, _("-1") );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->spin_beta = obj = fl_add_spinner( FL_FLOAT_SPINNER, 75, 140, 55, 24, _("Beta\nintensity") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_return( obj, FL_RETURN_NONE );
    fl_set_spinner_precision( obj, 3 );
    fl_set_spinner_bounds( obj, 0.000, 1.000 );
    fl_set_spinner_step( obj, 0.001 );

    fdui->inp_spectrum = obj = fl_add_input( FL_NORMAL_INPUT, 135, 140, 255, 25, "" );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT_TOP );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->inp_phot_e = obj = fl_add_input( FL_FLOAT_INPUT, 10, 310, 85, 20, _("keV") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->inp_phot_i = obj = fl_add_input( FL_FLOAT_INPUT, 143, 310, 87, 20, _("@@") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->inp_elec_e = obj = fl_add_input( FL_FLOAT_INPUT, 235, 310, 85, 20, _("keV") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->inp_elec_i = obj = fl_add_input( FL_FLOAT_INPUT, 367, 310, 87, 20, _("@@") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->spin_vox = obj = fl_add_spinner( FL_FLOAT_SPINNER, 85, 355, 80, 24, _("Voxel (cm)") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_kernel_dim, 0 );
    fl_set_spinner_precision( obj, 4 );
    fl_set_spinner_bounds( obj, 0.0001, 9.9999 );
    fl_set_spinner_value( obj, 0.1000 );
    fl_set_spinner_step( obj, 0.0050 );

    fdui->spin_maxr = obj = fl_add_spinner( FL_FLOAT_SPINNER, 85, 385, 80, 24, _("Max. radius\n(cm)") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_kernel_dim, 0 );
    fl_set_spinner_precision( obj, 2 );
    fl_set_spinner_bounds( obj, 0.00, 10.00 );
    fl_set_spinner_value( obj, 1.00 );
    fl_set_spinner_step( obj, 0.01 );

    fdui->inp_title = obj = fl_add_input( FL_NORMAL_INPUT, 45, 440, 245, 25, _("Title") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->inp_nuclide = obj = fl_add_input( FL_NORMAL_INPUT, 360, 440, 95, 25, _("Nuclide") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->inp_nhist = obj = fl_add_input( FL_INT_INPUT, 85, 470, 145, 25, _("# histories") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->spin_njobs = obj = fl_add_spinner( FL_INT_SPINNER, 360, 470, 60, 24, _("jobs") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT );
    fl_set_object_return( obj, FL_RETURN_NONE );
    fl_set_spinner_bounds( obj, 1, 999 );
    fl_set_spinner_value( obj, 1 );

    obj = fl_add_button( FL_NORMAL_BUTTON, 50, 285, 50, 20, _("Modify") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_callback( obj, cb_kernel_emission_op, 4 );
    fl_set_button_mouse_buttons( obj, 1 );

    obj = fl_add_button( FL_NORMAL_BUTTON, 275, 285, 50, 20, _("Modify") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_callback( obj, cb_kernel_emission_op, 5 );
    fl_set_button_mouse_buttons( obj, 1 );

    obj = fl_add_box( FL_NO_BOX, 173, 376, 77, 42, _("Low bound:\n(cm)") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    obj = fl_add_box( FL_NO_BOX, 340, 385, 65, 25, _("Voxels:") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    obj = fl_add_box( FL_NO_BOX, 235, 470, 125, 25, _("Parallel run with") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->btn_compute2 = obj = fl_add_button( FL_NORMAL_BUTTON, 235, 505, 225, 25, _("Compute dose voxel kernel") );
    fl_set_object_boxtype( obj, FL_ROUNDED3D_UPBOX );
    fl_set_object_color( obj, FL_MEDIUMVIOLETRED, FL_COL1 );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_kernel_compute2, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    obj = fl_add_box( FL_NO_BOX, 5, 50, 75, 25, _("EGS_HOME") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    obj = fl_add_box( FL_NO_BOX, 5, 80, 75, 25, _("PEGS file") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    obj = fl_add_box( FL_NO_BOX, 289, 119, 105, 20, _("Beta spectrum") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    obj = fl_add_box( FL_NO_BOX, 5, 20, 75, 25, _("HEN_HOUSE") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    fdui->inp_egshome = obj = fl_add_input( FL_NORMAL_INPUT, 80, 50, 310, 25, "" );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->inp_pegsfile = obj = fl_add_input( FL_NORMAL_INPUT, 80, 80, 310, 25, "" );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->inp_henhouse = obj = fl_add_input( FL_NORMAL_INPUT, 80, 20, 310, 25, "" );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fl_end_form( );

    fdui->kernel->fdui = fdui;

    return fdui;
}
