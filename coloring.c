#include <babl/babl.h>
#include <gsl/gsl_spline.h>

#include "coloring.h"
#include "color_gradient.h"
#include "color_predefined.h"

#include "selection.h"

#ifndef XG3D_util_h_
#define XG3D_util_h_
#include <forms.h>
#include <flimage.h>

typedef int ( COMPARE ) ( const void * keyval  , const void * datum  ) ;



COMPARE cmp_float;
COMPARE cmp_double;
int quiet_cue(FL_IMAGE*,const char*);
FL_FORM_ATCLOSE atclose_toggle;
FL_FORM_ATCLOSE atclose_trigger;
FL_FORM_ATCLOSE atclose_call;
FL_INPUT_VALIDATOR normal_file_validator;
FL_INPUT_VALIDATOR int_pos_validator;
FL_INPUT_VALIDATOR float_time_validator;
FL_INPUT_VALIDATOR float_pos_validator;
FL_INPUT_VALIDATOR float_perc_validator;
void deactivate_obj(FL_OBJECT*);
void activate_obj(FL_OBJECT*,const FL_COLOR);
void activate_form_extra_interaction(FL_FORM*);
char *enforce_ext(const char*,const char*,char*,const size_t);
const char* exp_to_utf8_sup(const int);
void resize_form_tabfolder(FL_OBJECT*,FL_OBJECT*);
double strtod_checked(const char*,const double);
void toggle_browser_line(FL_OBJECT*,const int,const bool);
#endif
#include "util_linsimp.h"


#define RED 0
#define GREEN 1
#define BLUE 2
#define SMOOTH_SPLINE gsl_interp_steffen

/* choose LUT from textual description */
int choose_lut_from_str(xg3d_lut *lut, const char *str, bool rev, bool inv, float hue) {
    if(NULL == str || '\0' == *str) return 1; // empty input

    if(strstr(str,"grad:") == str) { // gradient
        str += 5;

        enum col_grad_type type;
            if(strstr(str,"luma") == str
                && '\0' == *(str + 4)) {
            type = COL_GRAD_LUMA;
        }
   else        if(strstr(str,"lightness") == str
                && '\0' == *(str + 9)) {
            type = COL_GRAD_LIGHTNESS;
        }
   else        if(strstr(str,"value") == str
                && '\0' == *(str + 5)) {
            type = COL_GRAD_VALUE;
        }
   else        if(strstr(str,"sat_hsl") == str
                && '\0' == *(str + 7)) {
            type = COL_GRAD_SAT_HSL;
        }
   else        if(strstr(str,"grayscale") == str
                && '\0' == *(str + 9)) {
            type = COL_GRAD_GRAYSCALE;
        }
   else        if(strstr(str,"hotmetal") == str
                && '\0' == *(str + 8)) {
            type = COL_GRAD_HOTMETAL;
        }
   else        if(strstr(str,"turbo") == str
                && '\0' == *(str + 5)) {
            type = COL_GRAD_TURBO;
        }
   else        if(strstr(str,"fire") == str
                && '\0' == *(str + 4)) {
            type = COL_GRAD_FIRE;
        }
   else        if(strstr(str,"msh") == str
                && '\0' == *(str + 3)) {
            type = COL_GRAD_MSH;
        }
   else        if(strstr(str,"random") == str
                && '\0' == *(str + 6)) {
            type = COL_GRAD_RANDOM;
        }
   else        if(strstr(str,"matplotlib") == str
                && '\0' == *(str + 10)) {
            type = COL_GRAD_MATPLOTLIB;
        }
   else        if(strstr(str,"magma") == str
                && '\0' == *(str + 5)) {
            type = COL_GRAD_MAGMA;
        }
   else        if(strstr(str,"inferno") == str
                && '\0' == *(str + 7)) {
            type = COL_GRAD_INFERNO;
        }
   else        if(strstr(str,"plasma") == str
                && '\0' == *(str + 6)) {
            type = COL_GRAD_PLASMA;
        }
   else        if(strstr(str,"viridis") == str
                && '\0' == *(str + 7)) {
            type = COL_GRAD_VIRIDIS;
        }
   else        if(strstr(str,"parula") == str
                && '\0' == *(str + 6)) {
            type = COL_GRAD_PARULA;
        }
           else return 3; // bad grad

        _update_lut_gradient(lut,rev,inv,hue,type);
    } else if(strstr(str,"predef:") == str) { // predefined
        str += 7;

        enum col_predef_type type;
            if(strstr(str,"ColorBrewer Blues") == str
                && '\0' == *(str + 17)) {
            type = COL_PREDEF_COLORBREWER_SEQ_BLUES;
        }
   else        if(strstr(str,"ColorBrewer BuGn") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_SEQ_BUGN;
        }
   else        if(strstr(str,"ColorBrewer BuPu") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_SEQ_BUPU;
        }
   else        if(strstr(str,"ColorBrewer GnBu") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_SEQ_GNBU;
        }
   else        if(strstr(str,"ColorBrewer Greens") == str
                && '\0' == *(str + 18)) {
            type = COL_PREDEF_COLORBREWER_SEQ_GREENS;
        }
   else        if(strstr(str,"ColorBrewer Greys") == str
                && '\0' == *(str + 17)) {
            type = COL_PREDEF_COLORBREWER_SEQ_GREYS;
        }
   else        if(strstr(str,"ColorBrewer Oranges") == str
                && '\0' == *(str + 19)) {
            type = COL_PREDEF_COLORBREWER_SEQ_ORANGES;
        }
   else        if(strstr(str,"ColorBrewer OrRd") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_SEQ_ORRD;
        }
   else        if(strstr(str,"ColorBrewer PuBu") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_SEQ_PUBU;
        }
   else        if(strstr(str,"ColorBrewer PuBuGn") == str
                && '\0' == *(str + 18)) {
            type = COL_PREDEF_COLORBREWER_SEQ_PUBUGN;
        }
   else        if(strstr(str,"ColorBrewer PuRd") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_SEQ_PURD;
        }
   else        if(strstr(str,"ColorBrewer Purples") == str
                && '\0' == *(str + 19)) {
            type = COL_PREDEF_COLORBREWER_SEQ_PURPLES;
        }
   else        if(strstr(str,"ColorBrewer RdPu") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_SEQ_RDPU;
        }
   else        if(strstr(str,"ColorBrewer Reds") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_SEQ_REDS;
        }
   else        if(strstr(str,"ColorBrewer YlGn") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_SEQ_YLGN;
        }
   else        if(strstr(str,"ColorBrewer YlGnBu") == str
                && '\0' == *(str + 18)) {
            type = COL_PREDEF_COLORBREWER_SEQ_YLGNBU;
        }
   else        if(strstr(str,"ColorBrewer YlOrBr") == str
                && '\0' == *(str + 18)) {
            type = COL_PREDEF_COLORBREWER_SEQ_YLORBR;
        }
   else        if(strstr(str,"ColorBrewer YlOrRd") == str
                && '\0' == *(str + 18)) {
            type = COL_PREDEF_COLORBREWER_SEQ_YLORRD;
        }
   else        if(strstr(str,"ColorBrewer BrBG") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_DIV_BRBG;
        }
   else        if(strstr(str,"ColorBrewer PiYG") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_DIV_PIYG;
        }
   else        if(strstr(str,"ColorBrewer PrGN") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_DIV_PRGN;
        }
   else        if(strstr(str,"ColorBrewer PuOr") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_DIV_PUOR;
        }
   else        if(strstr(str,"ColorBrewer RdBu") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_DIV_RDBU;
        }
   else        if(strstr(str,"ColorBrewer RdGy") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_DIV_RDGY;
        }
   else        if(strstr(str,"ColorBrewer RdYlBu") == str
                && '\0' == *(str + 18)) {
            type = COL_PREDEF_COLORBREWER_DIV_RDYLBU;
        }
   else        if(strstr(str,"ColorBrewer RdYlGn") == str
                && '\0' == *(str + 18)) {
            type = COL_PREDEF_COLORBREWER_DIV_RDYLGN;
        }
   else        if(strstr(str,"ColorBrewer Spectral") == str
                && '\0' == *(str + 20)) {
            type = COL_PREDEF_COLORBREWER_DIV_SPECTRAL;
        }
   else        if(strstr(str,"ColorBrewer Accent") == str
                && '\0' == *(str + 18)) {
            type = COL_PREDEF_COLORBREWER_QUAL_ACCENT;
        }
   else        if(strstr(str,"ColorBrewer Dark2") == str
                && '\0' == *(str + 17)) {
            type = COL_PREDEF_COLORBREWER_QUAL_DARK2;
        }
   else        if(strstr(str,"ColorBrewer Paired") == str
                && '\0' == *(str + 18)) {
            type = COL_PREDEF_COLORBREWER_QUAL_PAIRED;
        }
   else        if(strstr(str,"ColorBrewer Pastel1") == str
                && '\0' == *(str + 19)) {
            type = COL_PREDEF_COLORBREWER_QUAL_PASTEL1;
        }
   else        if(strstr(str,"ColorBrewer Pastel2") == str
                && '\0' == *(str + 19)) {
            type = COL_PREDEF_COLORBREWER_QUAL_PASTEL2;
        }
   else        if(strstr(str,"ColorBrewer Set1") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_QUAL_SET1;
        }
   else        if(strstr(str,"ColorBrewer Set2") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_QUAL_SET2;
        }
   else        if(strstr(str,"ColorBrewer Set3") == str
                && '\0' == *(str + 16)) {
            type = COL_PREDEF_COLORBREWER_QUAL_SET3;
        }
   else        if(strstr(str,"Color Universal Design Protan") == str
                && '\0' == *(str + 29)) {
            type = COL_PREDEF_CUD_PROTAN;
        }
   else        if(strstr(str,"Color Universal Design Deutan") == str
                && '\0' == *(str + 29)) {
            type = COL_PREDEF_CUD_DEUTAN;
        }
   else        if(strstr(str,"Color Universal Design Tritan") == str
                && '\0' == *(str + 29)) {
            type = COL_PREDEF_CUD_TRITAN;
        }
   else        if(strstr(str,"Scientific Colour Maps acton") == str
                && '\0' == *(str + 28)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_ACTON;
        }
   else        if(strstr(str,"Scientific Colour Maps bamako") == str
                && '\0' == *(str + 29)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_BAMAKO;
        }
   else        if(strstr(str,"Scientific Colour Maps batlow") == str
                && '\0' == *(str + 29)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_BATLOW;
        }
   else        if(strstr(str,"Scientific Colour Maps berlin") == str
                && '\0' == *(str + 29)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_BERLIN;
        }
   else        if(strstr(str,"Scientific Colour Maps bilbao") == str
                && '\0' == *(str + 29)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_BILBAO;
        }
   else        if(strstr(str,"Scientific Colour Maps broc") == str
                && '\0' == *(str + 27)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_BROC;
        }
   else        if(strstr(str,"Scientific Colour Maps buda") == str
                && '\0' == *(str + 27)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_BUDA;
        }
   else        if(strstr(str,"Scientific Colour Maps cork") == str
                && '\0' == *(str + 27)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_CORK;
        }
   else        if(strstr(str,"Scientific Colour Maps davos") == str
                && '\0' == *(str + 28)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_DAVOS;
        }
   else        if(strstr(str,"Scientific Colour Maps devon") == str
                && '\0' == *(str + 28)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_DEVON;
        }
   else        if(strstr(str,"Scientific Colour Maps hawaii") == str
                && '\0' == *(str + 29)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_HAWAII;
        }
   else        if(strstr(str,"Scientific Colour Maps imola") == str
                && '\0' == *(str + 28)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_IMOLA;
        }
   else        if(strstr(str,"Scientific Colour Maps lajolla") == str
                && '\0' == *(str + 30)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_LAJOLLA;
        }
   else        if(strstr(str,"Scientific Colour Maps lapaz") == str
                && '\0' == *(str + 28)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_LAPAZ;
        }
   else        if(strstr(str,"Scientific Colour Maps lisbon") == str
                && '\0' == *(str + 29)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_LISBON;
        }
   else        if(strstr(str,"Scientific Colour Maps nuuk") == str
                && '\0' == *(str + 27)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_NUUK;
        }
   else        if(strstr(str,"Scientific Colour Maps oleron") == str
                && '\0' == *(str + 29)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_OLERON;
        }
   else        if(strstr(str,"Scientific Colour Maps oslo") == str
                && '\0' == *(str + 27)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_OSLO;
        }
   else        if(strstr(str,"Scientific Colour Maps roma") == str
                && '\0' == *(str + 27)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_ROMA;
        }
   else        if(strstr(str,"Scientific Colour Maps tofino") == str
                && '\0' == *(str + 29)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_TOFINO;
        }
   else        if(strstr(str,"Scientific Colour Maps tokyo") == str
                && '\0' == *(str + 28)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_TOKYO;
        }
   else        if(strstr(str,"Scientific Colour Maps turku") == str
                && '\0' == *(str + 28)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_TURKU;
        }
   else        if(strstr(str,"Scientific Colour Maps vik") == str
                && '\0' == *(str + 26)) {
            type = COL_PREDEF_SCIENTIFICCOLOURMAPS_VIK;
        }
           else return 4; // bad predef

        _update_lut_predef(lut,rev,inv,type);
    } else return 2; // bad prefix

    return 0;
}

/* ==================
   COLORING FUNCTIONS
   ================== */

/* fill the LUT from ramps */
void _update_lut_ramp(FD_col_ramp *col_ramp, xg3d_lut *lut, const int rev, const int inv) {
    const gsl_interp_type *type;
    gsl_interp_accel *acc[3];
    gsl_spline *spline[3];
    float *sx,*sy,dx;
    float *fx,*fy;
    double *x,*y;
    int n;

            FL_OBJECT *plot_red = col_ramp->plot_red;
        bool red_smooth = fl_get_button(col_ramp->btn_red_smooth);
        bool red_active = fl_get_button(col_ramp->btn_red);
           FL_OBJECT *plot_green = col_ramp->plot_green;
        bool green_smooth = fl_get_button(col_ramp->btn_green_smooth);
        bool green_active = fl_get_button(col_ramp->btn_green);
           FL_OBJECT *plot_blue = col_ramp->plot_blue;
        bool blue_smooth = fl_get_button(col_ramp->btn_blue_smooth);
        bool blue_active = fl_get_button(col_ramp->btn_blue);
   
                acc[RED] = gsl_interp_accel_alloc();
        type = red_smooth ? SMOOTH_SPLINE : gsl_interp_linear;
        fl_get_xyplot_data_pointer(plot_red,0,&fx,&fy,&n);
        spline[RED] = gsl_spline_alloc(type,n);
        x = malloc(n*sizeof(double));
        y = malloc(n*sizeof(double));
        for(int i=0;i<n;i++) { x[i] = (double)fx[i]; y[i] = (double)fy[i]; }
        gsl_spline_init(spline[RED],x,y,n);
        free(x); free(y);
        if(red_smooth) {
            n = plot_red->w;
            sx = malloc(n*sizeof(float));
            sy = malloc(n*sizeof(float));
            dx = 1.0 / (n - 1.0);
            for(int i=0;i<n;i++) {
                sx[i] = (float)i * dx;
                sy[i] = (float)FL_clamp(gsl_spline_eval(spline[RED],sx[i],acc[RED]),0.0,1.0);
            }
            fl_add_xyplot_overlay(plot_red,1,sx,sy,n,FL_RED);
            free(sx); free(sy);
        }
           acc[GREEN] = gsl_interp_accel_alloc();
        type = green_smooth ? SMOOTH_SPLINE : gsl_interp_linear;
        fl_get_xyplot_data_pointer(plot_green,0,&fx,&fy,&n);
        spline[GREEN] = gsl_spline_alloc(type,n);
        x = malloc(n*sizeof(double));
        y = malloc(n*sizeof(double));
        for(int i=0;i<n;i++) { x[i] = (double)fx[i]; y[i] = (double)fy[i]; }
        gsl_spline_init(spline[GREEN],x,y,n);
        free(x); free(y);
        if(green_smooth) {
            n = plot_green->w;
            sx = malloc(n*sizeof(float));
            sy = malloc(n*sizeof(float));
            dx = 1.0 / (n - 1.0);
            for(int i=0;i<n;i++) {
                sx[i] = (float)i * dx;
                sy[i] = (float)FL_clamp(gsl_spline_eval(spline[GREEN],sx[i],acc[GREEN]),0.0,1.0);
            }
            fl_add_xyplot_overlay(plot_green,1,sx,sy,n,FL_GREEN);
            free(sx); free(sy);
        }
           acc[BLUE] = gsl_interp_accel_alloc();
        type = blue_smooth ? SMOOTH_SPLINE : gsl_interp_linear;
        fl_get_xyplot_data_pointer(plot_blue,0,&fx,&fy,&n);
        spline[BLUE] = gsl_spline_alloc(type,n);
        x = malloc(n*sizeof(double));
        y = malloc(n*sizeof(double));
        for(int i=0;i<n;i++) { x[i] = (double)fx[i]; y[i] = (double)fy[i]; }
        gsl_spline_init(spline[BLUE],x,y,n);
        free(x); free(y);
        if(blue_smooth) {
            n = plot_blue->w;
            sx = malloc(n*sizeof(float));
            sy = malloc(n*sizeof(float));
            dx = 1.0 / (n - 1.0);
            for(int i=0;i<n;i++) {
                sx[i] = (float)i * dx;
                sy[i] = (float)FL_clamp(gsl_spline_eval(spline[BLUE],sx[i],acc[BLUE]),0.0,1.0);
            }
            fl_add_xyplot_overlay(plot_blue,1,sx,sy,n,FL_BLUE);
            free(sx); free(sy);
        }
   
    uint8_t *rgb_buf = malloc(3 * lut->ncol * sizeof(uint8_t));
    float *frgb_buf = malloc(3 * lut->ncol * sizeof(float));

    if(lut->ncol == 1) {
            frgb_buf[RED] = red_active ?
            FL_clamp(gsl_spline_eval(spline[RED],0.0,acc[RED]),0.0,1.0) : 0;
           frgb_buf[GREEN] = green_active ?
            FL_clamp(gsl_spline_eval(spline[GREEN],0.0,acc[GREEN]),0.0,1.0) : 0;
           frgb_buf[BLUE] = blue_active ?
            FL_clamp(gsl_spline_eval(spline[BLUE],0.0,acc[BLUE]),0.0,1.0) : 0;
       } else {
        for(unsigned int l = 0; l < lut->ncol; l++) {
            float cx = (float)l / (lut->ncol-1);
                    frgb_buf[RED + 3 * l] = red_active ?
                FL_clamp(gsl_spline_eval(spline[RED],cx,acc[RED]),0.0,1.0) : 0;
                   frgb_buf[GREEN + 3 * l] = green_active ?
                FL_clamp(gsl_spline_eval(spline[GREEN],cx,acc[GREEN]),0.0,1.0) : 0;
                   frgb_buf[BLUE + 3 * l] = blue_active ?
                FL_clamp(gsl_spline_eval(spline[BLUE],cx,acc[BLUE]),0.0,1.0) : 0;
               }
    }

            gsl_spline_free(spline[RED]);
        gsl_interp_accel_free(acc[RED]);
           gsl_spline_free(spline[GREEN]);
        gsl_interp_accel_free(acc[GREEN]);
           gsl_spline_free(spline[BLUE]);
        gsl_interp_accel_free(acc[BLUE]);
   
    const Babl *frgb = babl_format("R'G'B' float");
    const Babl *rgb = babl_format("R'G'B' u8");
    const Babl *fish = babl_fish(frgb,rgb);
    babl_process(fish,frgb_buf,rgb_buf,lut->ncol);
    for(unsigned int l=0;l<lut->ncol;l++) {
        unsigned int ix = rev ? lut->ncol-1-l : l;
        uint8_t r = rgb_buf[RED + 3 * l];
        uint8_t g = rgb_buf[GREEN + 3 * l];
        uint8_t b = rgb_buf[BLUE + 3 * l];
        if(inv) { r = ~r; g = ~g; b = ~b; }
        lut->colors[ix] = FL_PACK(r,g,b);
    }
    free(frgb_buf);
    free(rgb_buf);
}
#undef RED
#undef GREEN
#undef BLUE

/* fill the LUT with a gradient */
void _update_lut_gradient(xg3d_lut *lut, const int rev, const int inv, const float h, const enum col_grad_type type) {
    _colorf *colorf = NULL;
    switch(type) {
                case COL_GRAD_LUMA: colorf = _grad_luma; break;
               case COL_GRAD_LIGHTNESS: colorf = _grad_light; break;
               case COL_GRAD_VALUE: colorf = _grad_value; break;
               case COL_GRAD_SAT_HSL: colorf = _grad_sathsl; break;
               case COL_GRAD_GRAYSCALE: colorf = _grad_gray; break;
               case COL_GRAD_HOTMETAL: colorf = _grad_hotmetal; break;
               case COL_GRAD_TURBO: colorf = _grad_turbo; break;
               case COL_GRAD_FIRE: colorf = _grad_fire; break;
               case COL_GRAD_MSH: colorf = _grad_msh; break;
               case COL_GRAD_RANDOM: colorf = _grad_rand; break;
               case COL_GRAD_MATPLOTLIB: colorf = NULL; break;
               case COL_GRAD_MAGMA: colorf = _grad_magma; break;
               case COL_GRAD_INFERNO: colorf = _grad_inferno; break;
               case COL_GRAD_PLASMA: colorf = _grad_plasma; break;
               case COL_GRAD_VIRIDIS: colorf = _grad_viridis; break;
               case COL_GRAD_PARULA: colorf = _grad_parula; break;
               default: return;
    }

    float hue = h / 360.0;
    uint8_t *rgb = colorf(lut->ncol,hue);

    for(unsigned int i = 0; i < lut->ncol; i++) {
        unsigned int ix = rev ? lut->ncol - 1 - i : i;
        uint8_t r = rgb[3 * i];
        uint8_t g = rgb[3 * i + 1];
        uint8_t b = rgb[3 * i + 2];
        if(inv) { r = ~r; g = ~g; b = ~b; }
        lut->colors[ix] = FL_PACK(r,g,b);
    }
    free(rgb);
}

/* fill the LUT with predefined colors */
void _update_lut_predef(xg3d_lut *lut, const bool rev, const bool inv, const enum col_predef_type type) {
    unsigned int l;
    const FL_PACKED *predef = NULL;
    switch(type) {
                case COL_PREDEF_COLORBREWER_SEQ_BLUES:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_Blues[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_BUGN:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_BuGn[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_BUPU:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_BuPu[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_GNBU:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_GnBu[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_GREENS:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_Greens[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_GREYS:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_Greys[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_ORANGES:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_Oranges[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_ORRD:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_OrRd[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_PUBU:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_PuBu[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_PUBUGN:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_PuBuGn[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_PURD:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_PuRd[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_PURPLES:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_Purples[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_RDPU:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_RdPu[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_REDS:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_Reds[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_YLGN:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_YlGn[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_YLGNBU:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_YlGnBu[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_YLORBR:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_YlOrBr[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_SEQ_YLORRD:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_seq_YlOrRd[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_DIV_BRBG:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9,10,11, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_div_BrBG[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_DIV_PIYG:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9,10,11, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_div_PiYG[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_DIV_PRGN:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9,10,11, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_div_PrGN[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_DIV_PUOR:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9,10,11, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_div_PuOr[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_DIV_RDBU:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9,10,11, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_div_RdBu[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_DIV_RDGY:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9,10,11, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_div_RdGy[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_DIV_RDYLBU:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9,10,11, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_div_RdYlBu[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_DIV_RDYLGN:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9,10,11, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_div_RdYlGn[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_DIV_SPECTRAL:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9,10,11, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_div_Spectral[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_QUAL_ACCENT:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_qual_Accent[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_QUAL_DARK2:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_qual_Dark2[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_QUAL_PAIRED:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9,10,11,12, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_qual_Paired[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_QUAL_PASTEL1:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_qual_Pastel1[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_QUAL_PASTEL2:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_qual_Pastel2[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_QUAL_SET1:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_qual_Set1[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_QUAL_SET2:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_qual_Set2[i];
            }
            break;
               case COL_PREDEF_COLORBREWER_QUAL_SET3:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8,9,10,11,12, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ColorBrewer_qual_Set3[i];
            }
            break;
               case COL_PREDEF_CUD_PROTAN:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = CUD_protan[i];
            }
            break;
               case COL_PREDEF_CUD_DEUTAN:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = CUD_deutan[i];
            }
            break;
               case COL_PREDEF_CUD_TRITAN:
            {
                static const unsigned int ncol[] = { 3,4,5,6,7,8, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = CUD_tritan[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_ACTON:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_acton[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_BAMAKO:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_bamako[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_BATLOW:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_batlow[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_BERLIN:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_berlin[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_BILBAO:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_bilbao[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_BROC:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_broc[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_BUDA:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_buda[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_CORK:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_cork[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_DAVOS:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_davos[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_DEVON:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_devon[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_HAWAII:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_hawaii[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_IMOLA:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_imola[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_LAJOLLA:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_lajolla[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_LAPAZ:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_lapaz[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_LISBON:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_lisbon[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_NUUK:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_nuuk[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_OLERON:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_oleron[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_OSLO:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_oslo[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_ROMA:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_roma[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_TOFINO:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_tofino[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_TOKYO:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_tokyo[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_TURKU:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_turku[i];
            }
            break;
               case COL_PREDEF_SCIENTIFICCOLOURMAPS_VIK:
            {
                static const unsigned int ncol[] = { 10,25,50,100,256, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ScientificColourMaps_vik[i];
            }
            break;
               default: break; // unhandled placeholder values
    }

    if(predef != NULL) {
        if(lut->ncol <= l) {
            for(unsigned int i=0;i<lut->ncol;i++) {
                unsigned int ix = rev ? lut->ncol-1-i : i;
                uint8_t r = FL_GETR(predef[i%l]);
                uint8_t g = FL_GETG(predef[i%l]);
                uint8_t b = FL_GETB(predef[i%l]);
                if(inv) { r = ~r; g = ~g; b = ~b; }
                lut->colors[ix] = FL_PACK(r,g,b);
            }
        } else {
            /* here predef has max colors available */
            uint8_t r,g,b;
            for(unsigned int i=0;i<lut->ncol;i++) {
                unsigned int ix = rev ? lut->ncol-1-i : i;
                float x = (float)i/(float)(lut->ncol-1);
                unsigned int n = (unsigned int)(x * (l-1));
                if(n == l-1) {
                    r = FL_GETR(predef[n]);
                    g = FL_GETG(predef[n]);
                    b = FL_GETB(predef[n]);
                } else {
                    /* linear interpolation */
                    x = (x - (float)n/(l-1)) * (float)(l-1);
                    r = FL_nint(FL_GETR(predef[n]) * (1.0 - x) + FL_GETR(predef[n+1]) * x);
                    g = FL_nint(FL_GETG(predef[n]) * (1.0 - x) + FL_GETG(predef[n+1]) * x);
                    b = FL_nint(FL_GETB(predef[n]) * (1.0 - x) + FL_GETB(predef[n+1]) * x);
                }
                if(inv) { r = ~r; g = ~g; b = ~b; }
                lut->colors[ix] = FL_PACK(r,g,b);
            }
        }
    }
}

/* update LUT from file */
void _update_lut_file(xg3d_lut *lut, const uint8_t *lutfile, const bool rev, const bool inv) {
    if(lut->ncol == 1) {
        uint8_t r = lutfile[0];
        uint8_t g = lutfile[256];
        uint8_t b = lutfile[512];
        if(inv) { r = ~r; g = ~g; b = ~b; }
        lut->colors[0] = FL_PACK(r,g,b);
    } else 
        for(unsigned int i=0;i<lut->ncol;i++) {
            unsigned int ix = rev ? lut->ncol-1-i : i;
            unsigned int n = FL_nint(255 * ix/(float)(lut->ncol-1));
            uint8_t r = lutfile[n];
            uint8_t g = lutfile[n + 256];
            uint8_t b = lutfile[n + 512];
            if(inv) { r = ~r; g = ~g; b = ~b; }
            lut->colors[i] = FL_PACK(r,g,b);
        }
}
