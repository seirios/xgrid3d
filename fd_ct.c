/* Form definition file generated by fdesign on Sat Sep 28 21:14:16 2024 */

#include <stdlib.h>
#include "fd_ct.h"

#ifdef ENABLE_NLS
#include <libintl.h>
#define _(String) gettext (String)
#else
#define _(String) (String)
#endif


/***************************************
 ***************************************/

FD_ct *
create_form_ct( void )
{
    FL_OBJECT *obj;
    FD_ct *fdui = ( FD_ct * ) fl_malloc( sizeof *fdui );

    fdui->vdata = fdui->cdata = NULL;
    fdui->ldata = 0;

    fdui->ct = fl_bgn_form( FL_NO_BOX, 500, 350 );

    obj = fl_add_box( FL_FLAT_BOX, 0, 0, 500, 350, "" );

    obj = fl_add_labelframe( FL_ENGRAVED_FRAME, 5, 305, 490, 40, _("Iteration") );
    fl_set_object_lstyle( obj, FL_ITALIC_STYLE );

    obj = fl_add_labelframe( FL_ENGRAVED_FRAME, 220, 10, 275, 285, _("Cone-beam parameters") );
    fl_set_object_lstyle( obj, FL_ITALIC_STYLE );

    obj = fl_add_labelframe( FL_ENGRAVED_FRAME, 5, 225, 210, 70, _("Clamp values") );
    fl_set_object_lstyle( obj, FL_ITALIC_STYLE );

    obj = fl_add_labelframe( FL_ENGRAVED_FRAME, 5, 55, 210, 160, _("Reconstruction geometry") );
    fl_set_object_lstyle( obj, FL_ITALIC_STYLE );

    fdui->ch_algo = obj = fl_add_choice( FL_NORMAL_CHOICE2, 85, 10, 130, 25, _("Algorithm") );
    fl_addto_choice( obj, _("SIRT3D") );
    fl_addto_choice( obj, _("FDK") );
    fl_set_choice( obj, 1 );

    fdui->spin_vox = obj = fl_add_spinner( FL_FLOAT_SPINNER, 100, 70, 70, 24, _("Voxel size") );
    fl_set_object_return( obj, FL_RETURN_NONE );
    fl_set_spinner_precision( obj, 3 );
    fl_set_spinner_bounds( obj, 0.001, 9999.000 );
    fl_set_spinner_value( obj, 0.100 );
    fl_set_spinner_step( obj, 0.100 );

    fdui->spin_nx = obj = fl_add_spinner( FL_INT_SPINNER, 35, 125, 60, 24, _("x:") );
    fl_set_object_return( obj, FL_RETURN_NONE );
    fl_set_spinner_bounds( obj, 1, 9999 );
    fl_set_spinner_value( obj, 780 );

    fdui->spin_ny = obj = fl_add_spinner( FL_INT_SPINNER, 35, 155, 60, 24, _("y:") );
    fl_set_object_return( obj, FL_RETURN_NONE );
    fl_set_spinner_bounds( obj, 1, 9999 );
    fl_set_spinner_value( obj, 780 );

    fdui->spin_nz = obj = fl_add_spinner( FL_INT_SPINNER, 35, 185, 60, 24, _("z:") );
    fl_set_object_return( obj, FL_RETURN_NONE );
    fl_set_spinner_bounds( obj, 1, 9999 );
    fl_set_spinner_value( obj, 780 );

    obj = fl_add_box( FL_NO_BOX, 10, 100, 95, 25, _("Dimensions") );

    obj = fl_add_box( FL_NO_BOX, 110, 100, 100, 25, _("Center (mm)") );

    fdui->spin_cx = obj = fl_add_spinner( FL_FLOAT_SPINNER, 120, 125, 85, 24, _("x:") );
    fl_set_object_return( obj, FL_RETURN_NONE );
    fl_set_spinner_precision( obj, 3 );
    fl_set_spinner_bounds( obj, -9999.000, 9999.000 );
    fl_set_spinner_step( obj, 0.100 );

    fdui->spin_cy = obj = fl_add_spinner( FL_FLOAT_SPINNER, 120, 155, 85, 24, _("y:") );
    fl_set_object_return( obj, FL_RETURN_NONE );
    fl_set_spinner_precision( obj, 3 );
    fl_set_spinner_bounds( obj, -9999.000, 9999.000 );
    fl_set_spinner_step( obj, 0.100 );

    fdui->spin_cz = obj = fl_add_spinner( FL_FLOAT_SPINNER, 120, 185, 85, 24, _("z:") );
    fl_set_object_return( obj, FL_RETURN_NONE );
    fl_set_spinner_precision( obj, 3 );
    fl_set_spinner_bounds( obj, -9999.000, 9999.000 );
    fl_set_spinner_value( obj, 2.000 );
    fl_set_spinner_step( obj, 0.100 );

    fdui->inp_pix = obj = fl_add_input( FL_FLOAT_INPUT, 395, 20, 70, 25, _("mm") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT );

    fdui->inp_R = obj = fl_add_input( FL_FLOAT_INPUT, 395, 50, 70, 25, _("mm") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT );

    fdui->inp_D = obj = fl_add_input( FL_FLOAT_INPUT, 395, 80, 70, 25, _("mm") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT );

    obj = fl_add_box( FL_NO_BOX, 220, 20, 145, 25, _("Detector pixel") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    obj = fl_add_box( FL_NO_BOX, 220, 50, 145, 25, _("Source to object") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    obj = fl_add_box( FL_NO_BOX, 220, 80, 145, 25, _("Source to detector") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    fdui->inp_u0 = obj = fl_add_input( FL_FLOAT_INPUT, 395, 110, 70, 25, _("mm") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT );

    obj = fl_add_box( FL_NO_BOX, 220, 110, 145, 25, _("Focal projection x") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    fdui->inp_v0 = obj = fl_add_input( FL_FLOAT_INPUT, 395, 140, 70, 25, _("mm") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT );

    obj = fl_add_box( FL_NO_BOX, 220, 140, 145, 25, _("Focal projection y") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    fdui->inp_twist = obj = fl_add_input( FL_FLOAT_INPUT, 395, 170, 70, 25, _("°") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT );

    fdui->inp_slant = obj = fl_add_input( FL_FLOAT_INPUT, 395, 200, 70, 25, _("°") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT );

    fdui->inp_tilt = obj = fl_add_input( FL_FLOAT_INPUT, 395, 230, 70, 25, _("°") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT );

    obj = fl_add_box( FL_NO_BOX, 220, 170, 145, 25, _("Twist (in-plane)") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    obj = fl_add_box( FL_NO_BOX, 220, 200, 145, 25, _("Slant (polar)") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    obj = fl_add_box( FL_NO_BOX, 220, 230, 145, 25, _("Tilt (azimuthal)") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    fdui->spin_iter = obj = fl_add_spinner( FL_INT_SPINNER, 90, 315, 50, 24, _("Iterations") );
    fl_set_object_return( obj, FL_RETURN_NONE );
    fl_set_spinner_bounds( obj, 1, 9999 );
    fl_set_spinner_value( obj, 250 );

    fdui->btn_clamp_min = obj = fl_add_checkbutton( FL_PUSH_BUTTON, 10, 235, 110, 25, _("Minimum") );
    fl_set_object_callback( obj, cb_ct_clamp_toggle, 0 );
    fl_set_button( obj, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_clamp_max = obj = fl_add_checkbutton( FL_PUSH_BUTTON, 10, 265, 110, 25, _("Maximum") );
    fl_set_object_callback( obj, cb_ct_clamp_toggle, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->inp_clamp_min = obj = fl_add_input( FL_FLOAT_INPUT, 125, 235, 85, 25, "" );

    fdui->inp_clamp_max = obj = fl_add_input( FL_FLOAT_INPUT, 125, 265, 85, 25, "" );

    obj = fl_add_button( FL_NORMAL_BUTTON, 410, 315, 80, 25, _("Start") );
    fl_set_object_boxtype( obj, FL_ROUNDED3D_UPBOX );
    fl_set_object_color( obj, FL_YELLOW, FL_COL1 );
    fl_set_object_callback( obj, cb_ct_start, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_pause = obj = fl_add_checkbutton( FL_PUSH_BUTTON, 165, 315, 220, 25, _("Pause to allow GPU cooldown") );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->txt_det = obj = fl_add_box( FL_NO_BOX, 370, 20, 25, 25, _("p") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    fdui->txt_R = obj = fl_add_box( FL_NO_BOX, 370, 50, 25, 25, _("R") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    fdui->txt_D = obj = fl_add_box( FL_NO_BOX, 370, 80, 25, 25, _("D") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    fdui->txt_u0 = obj = fl_add_box( FL_NO_BOX, 370, 110, 25, 25, _("u₀") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    fdui->txt_v0 = obj = fl_add_box( FL_NO_BOX, 370, 140, 25, 25, _("v₀") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    fdui->txt_eta = obj = fl_add_box( FL_NO_BOX, 370, 170, 25, 25, _("η") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    fdui->txt_theta = obj = fl_add_box( FL_NO_BOX, 370, 200, 25, 25, _("θ") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    fdui->txt_phi = obj = fl_add_box( FL_NO_BOX, 370, 230, 25, 25, _("ϕ") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    obj = fl_add_box( FL_NO_BOX, 170, 70, 30, 25, _("mm") );

    obj = fl_add_button( FL_NORMAL_BUTTON, 225, 265, 265, 25, _("Load from CT header...") );
    fl_set_object_callback( obj, cb_ct_load_conebeam, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    fl_end_form( );

    fdui->ct->fdui = fdui;

    return fdui;
}
