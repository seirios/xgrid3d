#ifndef XG3D_kinetics_h_
#define XG3D_kinetics_h_

#include "fd_kinetics.h"
#include "xgrid3d.h"

typedef struct {
    unsigned int nframes; /* number of frames */
    xg3d_roi *xroi;       /* ROI used */
    xg3d_frame **frames;  /* frames */
} xg3d_kinetics_workspace;

#define KINETICS_WORKSPACE(fd_kinetics) ((xg3d_kinetics_workspace*)(((FD_kinetics*)(fd_kinetics))->vdata))
#define KINETICS_NFRAMES(fd_kinetics) (KINETICS_WORKSPACE(fd_kinetics)->nframes)
#define KINETICS_XROI(fd_kinetics) (KINETICS_WORKSPACE(fd_kinetics)->xroi)
#define KINETICS_ROI(fd_kinetics) (((xg3d_roi*)KINETICS_XROI(fd_kinetics))->roi)
#define KINETICS_FRAMES(fd_kinetics) (KINETICS_WORKSPACE(fd_kinetics)->frames)

void kinetics_workspace_clear(xg3d_kinetics_workspace*,const bool);
void xg3d_frame_free(xg3d_frame*);
void xg3d_frames_free(xg3d_frame**,const unsigned int);
int frame_cmp_time(const void*,const void*);
void refresh_kin_browser(FD_kinetics*);
int kin_input_time(uintmax_t*,const char*);
void deactivate_kin_inspector(FD_kinetics*,const bool,const bool);

#endif
