#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_roi.h"
#include "grid3d_quant.h"
#include "grid3d_visual.h"

#include "coloring.h"

#include <errno.h>
#include <unistd.h>

static FL_PACKED parse_hexcolor(const char *str) {
    unsigned r,g,b;
    if(sscanf(str,"#%2x%2x%2x",&r,&g,&b) != 3) {
        fprintf(stderr,"ERROR: malformed hex color %s\n",str);
        exit(EXIT_FAILURE);
    }
    return FL_PACK(r,g,b);
}

int quiet_cue(FL_IMAGE *im FL_UNUSED_ARG, const char *msg FL_UNUSED_ARG) {
    return 0;
}

void usage(void) {
    fputs("slicer: extract a slice as image, data, geometry, or ROI list\n",stderr);
    fputs("Usage: slicer <input file> <geometry spec> <slice spec> <output file>\n",stderr);
}

enum mode {
    MODE_PPM,
    MODE_IMG,
    MODE_TXT,
    MODE_ROI
};

int main (int argc, char *argv[])
{
    const char *file_in, *geom, *slice, *file_out;
    enum mode m = MODE_PPM; // operation mode

    bool trim = false; // trim background
    double background = 0.0; // background value

    enum g3d_raw_io_type raw_type = RAW_FLOAT32;
    enum g3d_compr_type compr_type = COMPR_GZIP;

    const char *coloring = "grad:grayscale"; // colormap
    FL_PACKED underflow = FL_PACK(0,0,0); // underflow color <black>
    bool has_underflow = false;
    FL_PACKED overflow = FL_PACK(255,255,255); // overflow color <white>
    bool has_overflow = false;
    bool rev = false; // reverse colors
    bool inv = false; // invert colors
    float hue = 0.0;  // colors hue spin

    unsigned long zoom = 1; // uniform scaling (no interpolation)

    double power = 1.0; // power law scaling of quantization intervals
    double min = 0.0; // quantization min
    bool min_set = false;
    double max = 1.0; // quantization max
    bool max_set = false;

    bool verbose = false; // verbose?
    { // handle CLI options
        opterr = 0;

        int c;
        while((c = getopt(argc,argv,"b:c:e:f:h:im:p:q:Q:rtvw:W:z:")) != -1) {
            switch(c) {
                case 'b': background = atof(optarg); break;
                case 'c': coloring = optarg; break;
                case 'h': hue = atof(optarg); break;
                case 'i': inv = true; break;
                case 'e':
                    if(strcmp(optarg,"raw") == 0 || strcmp(optarg,"none") == 0)
                        compr_type = COMPR_NONE;
                    else if(strcmp(optarg,"gzip") == 0 || strcmp(optarg,"gz") == 0)
                        compr_type = COMPR_GZIP;
                    else if(strcmp(optarg,"bzip2") == 0 || strcmp(optarg,"bz2") == 0)
                        compr_type = COMPR_BZIP2;
                    else if(strcmp(optarg,"xz") == 0)
                        compr_type = COMPR_LZMA2;
                    else {
                        fprintf(stderr,"ERROR: unknown compression type %s\n",optarg);
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 'f':
                    if(strcmp(optarg,"u8") == 0) raw_type = RAW_UINT8;
                    else if(strcmp(optarg,"u16") == 0) raw_type = RAW_UINT16;
                    else if(strcmp(optarg,"u32") == 0) raw_type = RAW_UINT32;
                    else if(strcmp(optarg,"u64") == 0) raw_type = RAW_UINT64;
                    else if(strcmp(optarg,"i8") == 0) raw_type = RAW_INT8;
                    else if(strcmp(optarg,"i16") == 0) raw_type = RAW_INT16;
                    else if(strcmp(optarg,"i32") == 0) raw_type = RAW_INT32;
                    else if(strcmp(optarg,"i64") == 0) raw_type = RAW_INT64;
                    else if(strcmp(optarg,"f32") == 0) raw_type = RAW_FLOAT32;
                    else if(strcmp(optarg,"f64") == 0) raw_type = RAW_FLOAT64;
                    else {
                        fprintf(stderr,"ERROR: unknown raw input type %s\n",optarg);
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 'm':
                    if(strcmp(optarg,"ppm") == 0) m = MODE_PPM;
                    else if(strcmp(optarg,"img") == 0) m = MODE_IMG;
                    else if(strcmp(optarg,"txt") == 0) m = MODE_TXT;
                    else if(strcmp(optarg,"roi") == 0) {
                        m = MODE_ROI;
                        raw_type = RAW_UINT8;
                    } else {
                        fprintf(stderr,"ERROR: unknown mode %s\n",optarg);
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 'p':
                    power = atof(optarg);
                    if(power < 1E-8) {
                        fprintf(stderr,"ERROR: power must be non-zero and positive\n");
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 'q': min = atof(optarg); min_set = true; break;
                case 'Q': max = atof(optarg); max_set = true; break;
                case 'r': rev = true; break;
                case 't': trim = true; break;
                case 'v': verbose = true; break;
                case 'w': // underflow color
                    underflow = parse_hexcolor(optarg);
                    has_underflow = true;
                    break;
                case 'W': // overflow color
                    overflow = parse_hexcolor(optarg);
                    has_overflow = true;
                    break;
                case 'z': zoom = strtoul(optarg,NULL,10); break;
                default:
                    fprintf(stderr,"ERROR: unknown option -%c\n",optopt);
                    exit(EXIT_FAILURE);
                    break;
            }
        }

        int narg = argc - optind;
        if(narg != 4) {
            usage();
            exit(EXIT_FAILURE);
        }

        file_in  = argv[optind];
        geom     = argv[optind + 1];
        slice    = argv[optind + 2];
        file_out = argv[optind + 3];
    }

    unsigned int xres,yres,zres;
    double voxel_size = 1.0;
    if(sscanf(geom,"%ux%ux%u@%lf",&xres,&yres,&zres,&voxel_size) != 4
            && sscanf(geom,"%ux%ux%u",&xres,&yres,&zres) != 3) {
        fprintf(stderr,"failed parsing geometry specification %s\n",geom);
        exit(EXIT_FAILURE);
    }

    coord_t k;
    enum g3d_slice_plane plane;
    { // read slice spec
        char p = '\0';
        sscanf(slice,"%c%"SCNcoord,&p,&k);

        unsigned int dim;
        switch(p) {
            case 'X':
                plane = PLANE_ZY;
                dim = xres;
                break;
            case 'Y':
                plane = PLANE_XZ;
                dim = yres;
                break;
            case 'Z':
                plane = PLANE_XY;
                dim = zres;
                break;
            default:
                fprintf(stderr,"ERROR: invalid slice plane specification %c\n",p);
                exit(EXIT_FAILURE);
        }
        if(k >= dim) {
            fprintf(stderr,"ERROR: slice number out of bounds (%d >= %u)\n",k,dim);
            exit(EXIT_FAILURE);
        }
    }

    grid3d *x;
    { // load data
        grid3d *in = NULL;
        g3d_input_raw(file_in,xres,yres,zres,raw_type,0,compr_type,ENDIAN_LITTLE,&in);

        grid3d *y = g3d_slice(in,k,plane); // extract slice
        g3d_free(in);

        if(trim) {
            x = g3d_trim_value(y,background);
            g3d_free(y);
        } else {
            x = y;
        }
    }

    switch(m) {
        case MODE_PPM: {
            { // setup flimage
                FLIMAGE_SETUP flisetup;
                memset(&flisetup,0x0,sizeof(FLIMAGE_SETUP));
                flisetup.visual_cue = quiet_cue;
                flimage_setup(&flisetup);
            }

            FL_IMAGE *img = NULL;
            { // create image of quantized slice
                g3d_quant_info qinfo = { 0 };
                { // apply quantization
                    g3d_stats *stats = g3d_stats_get(x,NULL,G3D_STATS_MINMAXSUM);

                    if(!min_set) min = stats->min;
                    if(!max_set) max = stats->max;
                    if(verbose) fprintf(stderr,"min = %.16f\nmax = %.16f\n",min,max);

                    /* set clamp mode */
                    enum g3d_quant_clamp_mode clamp;
                    if(min > stats->min && max < stats->max) clamp = QUANT_CLAMP_BOTH;
                    else if(min > stats->min) clamp = QUANT_CLAMP_LO;
                    else if(max < stats->max) clamp = QUANT_CLAMP_HI;
                    else clamp = QUANT_CLAMP_NONE;

                    free(stats);

                    /* max levels - 2 for under/overflow */
                    if(g3d_quant_intervals(x,G3D_VISUAL_MAX_LEVELS - 2,power,min,max,clamp,&qinfo) != G3D_OK) {
                        fprintf(stderr,"ERROR: quantization failed\n");
                        exit(EXIT_FAILURE);
                    }

                    grid3d *q = g3d_alloc(G3D_UINT8,x->nx,x->ny,x->nz,true);
                    g3d_quant_apply(x,q,&qinfo);
                    img = g3d_to_flimage(q,qinfo.nlevels,zoom,zoom,0,false,&img);
                    g3d_free(q);
                }

                { // apply coloring
                    xg3d_lut lut = {
                        .ncol= qinfo.nlevels,
                        .underflow= (qinfo.clamp & QUANT_CLAMP_LO) == QUANT_CLAMP_LO,
                        .overflow= (qinfo.clamp & QUANT_CLAMP_HI) == QUANT_CLAMP_HI
                    };

                    babl_init();
                    if(choose_lut_from_str(&lut,coloring,rev,inv,hue)) {
                        fprintf(stderr,"WARN: invalid coloring %s ...using fallback grayscale\n",coloring);
                        choose_lut_from_str(&lut,"grad:grayscale",rev,inv,0);
                    }

                    /* if not specified in CLI, take from LUT */
                    if(!has_underflow) underflow = lut.colors[0];
                    if(!has_overflow) overflow = lut.colors[lut.ncol - 1];

                    /* set under/overflow colors */
                    if(lut.underflow) lut.colors[0] = underflow;
                    if(lut.overflow) lut.colors[lut.ncol - 1] = overflow;

                    g3d_flimage_set_lut(img,lut.colors);
                }
                g3d_quant_info_clear(&qinfo,false);
            }

            flimage_dump(img,file_out,"ppm");
            flimage_free(img);
        } break;
        case MODE_IMG:
            g3d_output_raw(x,file_out,RAW_FLOAT32,COMPR_NONE,ENDIAN_KEEP,false);
            break;
        case MODE_TXT: {
            errno = 0;
            FILE *fp = fopen(file_out,"w");
            if(fp == NULL) {
                fprintf(stderr,"ERROR: failed opening file %s for write: %s (%d)\n",
                        file_out,strerror(errno),errno);
                exit(EXIT_FAILURE);
            }
            fprintf(fp,"%ux%ux%u@%g",x->nx,x->ny,x->nz,voxel_size);
            fclose(fp);
        } break;
        case MODE_ROI: {
            index_t nvox = 0;
            for(index_t ix = 0; ix < x->nvox; ix++) nvox += g3d_u8(x,ix);

            g3d_roi_list *lst = g3d_roi_list_alloc(1);
            { // create and save ROI
                g3d_roi *roi = g3d_roi_new(x,"MASK",nvox);
                lst->rois[0] = roi;
                g3d_roi_list_save(file_out,lst);
                g3d_roi_free(roi);
            }
            lst->rois[0] = NULL;
            g3d_roi_list_free(lst);
        } break;
        default: break;
    }

    g3d_free(x); // free data

    return 0;
}

