/* Code to find the equation of a conic */
/*               Tom Davis              */
/*             April 12, 1996           */

/* toconic takes five points in homogeneous coordinates, and returns the
 * coefficients of a general conic equation in a, b, c, ..., f:
 * 
 * a*x*x + b*x*y + c*y*y + d*x + e*y + f = 0.
 * 
 * The routine returns 1 on success; 0 otherwise.  (It can fail, for
 * example, if there are duplicate points.
 * 
 * Typically, the points will be finite, in which case the third (w)
 * coordinate for all the input vectors will be 1, although the code
 * deals cleanly with points at infinity.
 * 
 * For example, to find the equation of the conic passing through (5, 0), 
 * (-5, 0), (3, 2), (3, -2), and (-3, 2), set:
 * 
 * p0[0] =  5, p0[1] =  0, p0[2] = 1, 
 * p1[0] = -5, p1[1] =  0, p1[2] = 1, 
 * p2[0] =  3, p2[1] =  2, p2[2] = 1, 
 * p3[0] =  3, p3[1] = -2, p3[2] = 1, 
 * p4[0] = -3, p4[1] =  2, p4[2] = 1.
 *
 * But if you want the equation of the hyperbola that is tangent to the
 * line 2x=y at infinity,  simply make one of the points be the point at
 * infinity along that line, for example:
 *
 * p0[0] = 1, p0[1] = 2, p0[2] = 0.
 */

int toconic(double p0[3], double p1[3], double p2[3], double p3[3], double p4[3], 
		double *a, double *b, double *c, double *d, double *e, double *f)
{
	double L0[3], L1[3], L2[3], L3[3];
	double A, B, C, Q[3];
	double a1, a2, b1, b2, c1, c2;
	double x0, x4, y0, y4, w0, w4;
	double aa, bb, cc, dd, ee, ff;
	double y4w0, w4y0, w4w0, y4y0, x4w0, w4x0, x4x0, y4x0, x4y0;
	double a1a2, a1b2, a1c2, b1a2, b1b2, b1c2, c1a2, c1b2, c1c2;

	L0[0] = p0[1]*p1[2] - p0[2]*p1[1];
	L0[1] = p0[2]*p1[0] - p0[0]*p1[2];
	L0[2] = p0[0]*p1[1] - p0[1]*p1[0];
	L1[0] = p1[1]*p2[2] - p1[2]*p2[1];
	L1[1] = p1[2]*p2[0] - p1[0]*p2[2];
	L1[2] = p1[0]*p2[1] - p1[1]*p2[0];
	L2[0] = p2[1]*p3[2] - p2[2]*p3[1];
	L2[1] = p2[2]*p3[0] - p2[0]*p3[2];
	L2[2] = p2[0]*p3[1] - p2[1]*p3[0];
	L3[0] = p3[1]*p4[2] - p3[2]*p4[1];
	L3[1] = p3[2]*p4[0] - p3[0]*p4[2];
	L3[2] = p3[0]*p4[1] - p3[1]*p4[0];
	Q[0] = L0[1]*L3[2] - L0[2]*L3[1];
	Q[1] = L0[2]*L3[0] - L0[0]*L3[2];
	Q[2] = L0[0]*L3[1] - L0[1]*L3[0];
	A = Q[0]; B = Q[1]; C = Q[2];
	a1 = L1[0]; b1 = L1[1]; c1 = L1[2];
	a2 = L2[0]; b2 = L2[1]; c2 = L2[2];
	x0 = p0[0]; y0 = p0[1]; w0 = p0[2];
	x4 = p4[0]; y4 = p4[1]; w4 = p4[2];

	y4w0 = y4*w0;
	w4y0 = w4*y0;
	w4w0 = w4*w0;
	y4y0 = y4*y0;
	x4w0 = x4*w0;
	w4x0 = w4*x0;
	x4x0 = x4*x0;
	y4x0 = y4*x0;
	x4y0 = x4*y0;
	a1a2 = a1*a2;
	a1b2 = a1*b2;
	a1c2 = a1*c2;
	b1a2 = b1*a2;
	b1b2 = b1*b2;
	b1c2 = b1*c2;
	c1a2 = c1*a2;
	c1b2 = c1*b2;
	c1c2 = c1*c2;

	aa =-A*a1a2*y4w0 +A*a1a2*w4y0 -B*b1a2*y4w0 -B*c1a2*w4w0 +B*a1b2*w4y0
		+B*a1c2*w4w0 +C*b1a2*y4y0 +C*c1a2*w4y0 -C*a1b2*y4y0 -C*a1c2*y4w0;

	cc = A*c1b2*w4w0 +A*a1b2*x4w0 -A*b1c2*w4w0 -A*b1a2*w4x0 +B*b1b2*x4w0
		-B*b1b2*w4x0 +C*b1c2*x4w0 +C*b1a2*x4x0 -C*c1b2*w4x0 -C*a1b2*x4x0;

	ff = A*c1a2*y4x0 +A*c1b2*y4y0 -A*a1c2*x4y0 -A*b1c2*y4y0 -B*c1a2*x4x0
		-B*c1b2*x4y0 +B*a1c2*x4x0 +B*b1c2*y4x0 -C*c1c2*x4y0 +C*c1c2*y4x0;

	bb = A*c1a2*w4w0 +A*a1a2*x4w0 -A*a1b2*y4w0 -A*a1c2*w4w0 -A*a1a2*w4x0
		+A*b1a2*w4y0 +B*b1a2*x4w0 -B*b1b2*y4w0 -B*c1b2*w4w0 -B*a1b2*w4x0
		+B*b1b2*w4y0 +B*b1c2*w4w0 -C*b1c2*y4w0 -C*b1a2*x4y0 -C*b1a2*y4x0
		-C*c1a2*w4x0 +C*c1b2*w4y0 +C*a1b2*x4y0 +C*a1b2*y4x0 +C*a1c2*x4w0;

	dd = -A*c1a2*y4w0 +A*a1a2*y4x0 +A*a1b2*y4y0 +A*a1c2*w4y0 -A*a1a2*x4y0
		-A*b1a2*y4y0 +B*b1a2*y4x0 +B*c1a2*w4x0 +B*c1a2*x4w0 +B*c1b2*w4y0
		-B*a1b2*x4y0 -B*a1c2*w4x0 -B*a1c2*x4w0 -B*b1c2*y4w0 +C*b1c2*y4y0
		+C*c1c2*w4y0 -C*c1a2*x4y0 -C*c1b2*y4y0 -C*c1c2*y4w0 +C*a1c2*y4x0;

	ee = -A*c1a2*w4x0 -A*c1b2*y4w0 -A*c1b2*w4y0 -A*a1b2*x4y0 +A*a1c2*x4w0
		+A*b1c2*y4w0 +A*b1c2*w4y0 +A*b1a2*y4x0 -B*b1a2*x4x0 -B*b1b2*x4y0
		+B*c1b2*x4w0 +B*a1b2*x4x0 +B*b1b2*y4x0 -B*b1c2*w4x0 -C*b1c2*x4y0
		+C*c1c2*x4w0 +C*c1a2*x4x0 +C*c1b2*y4x0 -C*c1c2*w4x0 -C*a1c2*x4x0;

	if (aa != 0.0) {
		bb /= aa; cc /= aa; dd /= aa; ee /= aa; ff /= aa; aa = 1.0;
	} else if (bb != 0.0) {
		cc /= bb; dd /= bb; ee /= bb; ff /= bb; bb = 1.0;
	} else if (cc != 0.0) {
		dd /= cc; ee /= cc; ff /= cc; cc = 1.0;
	} else if (dd != 0.0) {
		ee /= dd; ff /= dd; dd = 1.0;
	} else if (ee != 0.0) {
		ff /= ee; ee = 1.0;
	} else {
		return 0;
	}

	*a = aa;
	*b = bb;
	*c = cc;
	*d = dd;
	*e = ee;
	*f = ff;

	return 1;
}
