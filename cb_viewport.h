#ifndef XG3D_viewport_cb_h_
#define XG3D_viewport_cb_h_

#include "fd_viewport.h"
#include "xgrid3d.h"
#include "cb_selection.h"



typedef struct {
    xg3d_selection_workspace *sel; /* selection data */
    xg3d_data* data[MAX_LAYERS]; // layer data
    FL_IMAGE* image[MAX_LAYERS]; // layer images
    grid3d* slice[MAX_LAYERS]; // layer data slices
    grid3d* quant_slice[MAX_LAYERS]; // layer quantized slices
    coord_t k[MAX_LAYERS]; // layer slice index
    coord_t sp[3]; // coordinates of start point
    coord_t ep[3]; // coordinates of end point
    slice_t oldpos; // old mouse grid position
    FL_Coord xold; // old mouse x window position
    FL_Coord yold; // old mouse y window position
    FL_COLOR cursor_color; // cursor color
    enum g3d_slice_plane plane; // slice plane
    enum xg3d_orientation orient; // orientation
    unsigned int zoom_x; // x zoom level
    unsigned int zoom_y; // y zoom level
    bool spset; // flags for start and end points
    bool epset; // flags for start and end points
    bool moved; // flag for drawing XXX: what is this for?
    bool xhair; // flag for drawing crosshairs with mouse
    bool pan; // flag for panning with mouse
    bool slide; // flag for changing slice with mouse
    bool level; // flag for changing quantization level with mouse
} xg3d_view_workspace;

#define VIEW_WORKSPACE(fd_view) ((xg3d_view_workspace*)(((FD_viewport*)(fd_view))->vdata))

#define VIEW_WORKSPACE_SEL(fd_view) ((xg3d_selection_workspace*)(VIEW_WORKSPACE(fd_view)->sel))
#define VIEW_SELSLICE(fd_view) (VIEW_WORKSPACE_SEL(fd_view)->g3d)
#define VIEW_SELNVOX(fd_view) (VIEW_WORKSPACE_SEL(fd_view)->nvox)
#define VIEW_SELVALUE(fd_view) (VIEW_WORKSPACE_SEL(fd_view)->value)

#define VIEW_DATA(fd_view) (VIEW_WORKSPACE(fd_view)->data[LAYER_BASE])
#define VIEW_DATA_LAYER(fd_view,layer) (VIEW_WORKSPACE(fd_view)->data[layer])
#define VIEW_IMAGE(fd_view) (VIEW_WORKSPACE(fd_view)->image[LAYER_BASE])
#define VIEW_IMAGE_LAYER(fd_view,layer) (VIEW_WORKSPACE(fd_view)->image[layer])
#define VIEW_SLICE(fd_view) (VIEW_WORKSPACE(fd_view)->slice[LAYER_BASE])
#define VIEW_SLICE_LAYER(fd_view,layer) (VIEW_WORKSPACE(fd_view)->slice[layer])
#define VIEW_QUANT_SLICE(fd_view) (VIEW_WORKSPACE(fd_view)->quant_slice[LAYER_BASE])
#define VIEW_QUANT_SLICE_LAYER(fd_view,layer) (VIEW_WORKSPACE(fd_view)->quant_slice[layer])
#define VIEW_K(fd_view) (VIEW_WORKSPACE(fd_view)->k[LAYER_BASE])
#define VIEW_K_LAYER(fd_view,layer) (VIEW_WORKSPACE(fd_view)->k[layer])

#define VIEW_NX(fd_view) (((grid3d*)(VIEW_SLICE(fd_view)))->nx)
#define VIEW_NY(fd_view) (((grid3d*)(VIEW_SLICE(fd_view)))->ny)

#define VIEW_SP(fd_view) (VIEW_WORKSPACE(fd_view)->sp)
#define VIEW_EP(fd_view) (VIEW_WORKSPACE(fd_view)->ep)
#define VIEW_OLDPOS(fd_view) (VIEW_WORKSPACE(fd_view)->oldpos)
#define VIEW_XOLD(fd_view) (VIEW_WORKSPACE(fd_view)->xold)
#define VIEW_YOLD(fd_view) (VIEW_WORKSPACE(fd_view)->yold)
#define VIEW_CURSOR_COLOR(fd_view) (VIEW_WORKSPACE(fd_view)->cursor_color)
#define VIEW_PLANE(fd_view) (VIEW_WORKSPACE(fd_view)->plane)
#define VIEW_ORIENT(fd_view) (VIEW_WORKSPACE(fd_view)->orient)
#define VIEW_ZOOM_X(fd_view) (VIEW_WORKSPACE(fd_view)->zoom_x)
#define VIEW_ZOOM_Y(fd_view) (VIEW_WORKSPACE(fd_view)->zoom_y)
#define VIEW_SPSET(fd_view) (VIEW_WORKSPACE(fd_view)->spset)
#define VIEW_EPSET(fd_view) (VIEW_WORKSPACE(fd_view)->epset)
#define VIEW_MOVED(fd_view) (VIEW_WORKSPACE(fd_view)->moved)
#define VIEW_XHAIR(fd_view) (VIEW_WORKSPACE(fd_view)->xhair)
#define VIEW_PAN(fd_view) (VIEW_WORKSPACE(fd_view)->pan)
#define VIEW_SLIDE(fd_view) (VIEW_WORKSPACE(fd_view)->slide)
#define VIEW_LEVEL(fd_view) (VIEW_WORKSPACE(fd_view)->level)

#define VIEW_CANVAS(fd_view) (((FD_viewport*)(fd_view))->canvas)

xg3d_view_workspace* view_workspace_alloc(void);
void view_workspace_clear(xg3d_view_workspace*,const bool);
xg3d_view_workspace* view_workspace_dup(const xg3d_view_workspace*);

void win_to_grid(FD_viewport*,FL_Coord,FL_Coord,coord_t*,coord_t*);
void grid_to_win(FD_viewport*,FL_Coord*,FL_Coord*,const coord_t,const coord_t);
void ccoord_to_winf(FD_viewport*,double*,double*,const double,const double);
void ccoord_to_win(FD_viewport*,FL_Coord*,FL_Coord*,const double,const double);

#endif
