#ifndef XG3D_util_fit2d_h_
#define XG3D_util_fit2d_h_

struct fit_result {
	size_t p;
	unsigned int dof;
	double chi;
	double *par;
	double *err;
};

void free_fit_result(struct fit_result*);
void gnuplot_errors(struct fit_result*);

double fit_func_exp(double,double,double);
double fit_func_biexp(double,double,double,double,double);
double fit_func_rational(double,double,double,double);
double fit_func_log10rational(double,double,double,double);

struct fit_result* fit_exp(const double*,const double*,double*,const size_t,double[2]);
struct fit_result* fit_biexp(const double*,const double*,double*,const size_t,double[4]);
struct fit_result* fit_rational(const double*,const double*,double*,const size_t,double[3]);
struct fit_result* fit_log10rational(const double*,const double*,double*,const size_t,double[3]);

#endif
