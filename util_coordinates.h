#ifndef XG3D_coordinates_h_
#define XG3D_coordinates_h_

#define UNCOORD_SLICE(x,k,jik,nx) \
    jik[0] = (x) % (nx); \
    jik[1] = (x) / (nx); \
    jik[2] = (k);

#define UNCOORD_DIM(x,jik,nx,dim) \
    jik[0] = ((x) % (dim)) % (nx); \
    jik[1] = ((x) % (dim)) / (nx); \
    jik[2] =  (x) / (dim);

#define COORD_GLOBAL(j,i,k,jik,plane) \
    jik[PLANE_TO_J(plane)] = j; \
    jik[PLANE_TO_I(plane)] = i; \
    jik[PLANE_TO_K(plane)] = k;

#define COORD_DIM_IX(jik,ixj,ixi,ixk,nx,dim) ((index_t)(jik)[ixj] + (jik)[ixi]*(nx) + (jik)[ixk]*(dim))
#define COORD_DIM(jik,nx,dim) COORD_DIM_IX(jik,0,1,2,nx,dim)

#define COORD_SPLIT_DIM(j,i,k,nx,dim,plane) \
    (plane == PLANE_XY ? (index_t)(j) + (i)*(nx) + (k)*(dim) : \
    (plane == PLANE_XZ ? (index_t)(j) + (k)*(nx) + (i)*(dim) : \
                         (index_t)(k) + (i)*(nx) + (j)*(dim)))

#define COORD_SLICE_XY(ix,k,dim)       (       (ix) +                            (k)*(dim))
#define COORD_SLICE_XZ(ix,k,nx,dim)    (((ix)%(nx)) +         (k)*(nx) + ((ix)/(nx))*(dim))
#define COORD_SLICE_ZY(ix,k,nx,nz,dim) (        (k) + ((ix)/(nz))*(nx) + ((ix)%(nz))*(dim))

#define orient_flip_check(orient) (((orient) >> 2) & 1U)

#endif
