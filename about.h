/* 
 * The contents of this file are private
 * and must be distributed verbatim.
 * No modifications allowed.
 */

#define ABOUT_WRITTEN_OATH \
    "Written Oath -\n" \
    "I hereby give my permission to\n" \
    "make the xgrid3d software into\n" \
    "a preclinical research tool."

#define ABOUT_SOFTWARE_CREDITS_HEADER \
    "SOFTWARE CREDITS"

#define ABOUT_SOFTWARE_CREDITS \
    "XForms Toolkit, libgrid3d, DOSXYZnrc,\n" \
    "FFTW, parallel, niftilib, NiftyReg,\n" \
    "GSL, libmatheval, gnuplot, gnuplot_i,\n" \
    "CONREC, sort (Faigon), toconic (Davis),\n" \
    "line3d (Pendleton), bresenham (Zingl),\n" \
    "HeapHull2 (Morin), babl, ColorBrewer,\n" \
    "Msh (Moreland), CUD (Okabe), NrrdIO."

#define ABOUT_DEDICATED_TO_HEADER \
    "In memoriam"

#define ABOUT_DEDICATED_TO \
    "Amelia del Carmen García León"

#define ABOUT_COPYRIGHT \
    "2016-2024 © Sirio Bolaños Puchet"
