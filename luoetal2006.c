#include <stdlib.h>
#include <math.h>

void JMh_to_Jpapbp(double KL, double c1, double c2, const double *J, const double *M, const double *h, unsigned int N, double **Jp_o, double **ap_o, double **bp_o) {
    *Jp_o = malloc(N * sizeof(double));
    *ap_o = malloc(N * sizeof(double));
    *bp_o = malloc(N * sizeof(double));
    for(unsigned int i=0;i<N;i++) {
        double _Jp = ((1.0 + 100.0 * c1) * J[i]) / (1.0 + c1 * J[i]);
        double _Mp = log(1.0 + c2 * M[i]) / c2;
        double _th = h[i] * M_PI / 180.0;
        (*Jp_o)[i] = _Jp / KL;
        (*ap_o)[i] = _Mp * cos(_th);
        (*bp_o)[i] = _Mp * sin(_th);
    }
}

void Jpapbp_to_JMh(double KL, double c1, double c2, const double *Jp, const double *ap, const double *bp, unsigned int N, double **J_o, double **M_o, double **h_o) {
    *J_o = malloc(N * sizeof(double));
    *M_o = malloc(N * sizeof(double));
    *h_o = malloc(N * sizeof(double));
    for(unsigned int i=0;i<N;i++) {
        double _Jp = Jp[i] * KL;
        double _Mp = hypot(ap[i],bp[i]);
        double _th = atan2(bp[i],ap[i]);
        (*J_o)[i] = - _Jp / (c1 * _Jp - 100.0 * c1 - 1.0);
        (*M_o)[i] = (exp(c2 * _Mp) - 1.0) / c2;
        (*h_o)[i] = _th * 180.0 * M_1_PI;
        (*h_o)[i] += (*h_o)[i] < 0 ? 360.0 : 0.0;
    }
}
