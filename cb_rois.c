#include <libgen.h>

#include <gsl/gsl_sort.h>

#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "cb_rois.h"
#include "cb_info.h"
#include "cb_dialog.h"
#include "cb_selection.h"
#include "cb_overview.h"
#include "cb_mainview.h"
#include "fd_calibration.h"
#include "cb_quantization.h"
#include "fd_dose.h"
#include "fd_dvhs.h"
#include "fd_overlay.h"
#include "cb_kinetics.h"

#include "selection.h"
#include "rois.h"

#include "io.h"
#include "util.h"
#include "util_units.h"
#include "util_histogram.h"

#define X(ix,nx,dim) (((ix)%(dim))%(nx))
#define Y(ix,nx,dim) (((ix)%(dim))/(nx))
#define Z(ix,nx,dim)  ((ix)/(dim))

#define FORMAT "%.11g"
#define FORMAT_LINDIM "x: %6g   y: %6g   z: %6g"

#define _wsel(browser) (fl_get_browser_first_selected(browser) - 1)

static xg3d_roi* xroi_alloc(const char *name, const index_t size) {
    xg3d_roi *x = malloc(sizeof(xg3d_roi));
    if(x == NULL) return NULL; /* malloc failed */

    x->roi = g3d_roi_alloc(name,size);
    if(x->roi == NULL) {
        xroi_free(x);
        return NULL; /* malloc failed */
    }
    x->total = x->min = x->max = 0.0;
    x->color = (FL_PACKED)random();
    x->hist = NULL;

    return x;
}

static xg3d_roi* xroi_wrap(g3d_roi *roi) {
    if(roi == NULL) return NULL; /* null input */
    xg3d_roi *x = malloc(sizeof(xg3d_roi));
    if(x == NULL) return NULL; /* malloc failed */

    x->roi = roi;
    x->total = x->min = x->max = 0.0;
    x->color = (FL_PACKED)random();
    x->hist = NULL;

    return x;
}

void xroi_free(xg3d_roi *x) {
    if(x != NULL) {
        g3d_roi_free(x->roi);
        if(x->hist != NULL)
            gsl_histogram_free(x->hist);
        free(x);
    }
}

static xg3d_roi* xroi_dup(const xg3d_roi *x) {
    if(x == NULL) return NULL; /* null input */
    xg3d_roi *y = malloc(sizeof(xg3d_roi));
    if(y == NULL) return NULL; /* malloc failed */

    y->roi = g3d_roi_dup(x->roi);
    y->total = x->total;
    y->min = x->min;
    y->max = x->max;
    y->color = x->color;
    y->hist = gsl_histogram_clone(x->hist);

    return y;
}

xg3d_roi** xrois_dup(xg3d_roi **x, const nroi_t n) {
    if(x == NULL || n == 0) return NULL; /* no input */
    xg3d_roi **y = malloc(n*sizeof(xg3d_roi*));
    if(y == NULL) return NULL; /* malloc failed */

    for(nroi_t i=0;i<n;i++)
        y[i] = xroi_dup(x[i]);

    return y;
}

void xrois_free(xg3d_roi **x, const nroi_t n) {
    if(x != NULL) {
        for(nroi_t i=0;i<n;i++)
            xroi_free(x[i]);
        free(x);
    }
}

static int xroi_cmp_name(const void *a, const void *b) {
    xg3d_roi *ra = *((xg3d_roi**)a);
    xg3d_roi *rb = *((xg3d_roi**)b);

    return strcmp(ra->roi->name,rb->roi->name);
}

static int xroi_equal(const xg3d_roi *x, const xg3d_roi *y) {
    return g3d_roi_equal(x->roi,y->roi);
}

static int xroi_rename_if_duplicate(xg3d_roi **xroi, xg3d_roi **list, const nroi_t size) {
    size_t len;
    char *buf;

    if(bsearch(xroi,list,size,sizeof(xg3d_roi*),xroi_cmp_name) == NULL) return 0;
    len = (*xroi)->roi->nlen + 16;
    buf = malloc(len*sizeof(char));
    snprintf(buf,len,"%s_%ld",(*xroi)->roi->name,random()%1000);
    free((*xroi)->roi->name);
    (*xroi)->roi->nlen = strlen(buf);
    (*xroi)->roi->name = strdup(buf);
    free(buf);

    return 1;
}

#define NBINS 240
static void _update_roi_histogram(const grid3d *data, xg3d_roi *xroi) {
    gsl_histogram *h;

    h = gsl_histogram_alloc(NBINS);
    gsl_histogram_set_ranges_uniform(h,xroi->min,nextafter(xroi->max,HUGE_VAL));
    histogram_fill_roi(h,data,xroi->roi);
    if(xroi->hist != NULL) gsl_histogram_free(xroi->hist);
    xroi->hist = h;
}

#define HH 64
#define HW NBINS
#undef ENV
static void _display_roi_histogram(xg3d_env *ENV, const unsigned int ix) {
    const int flag_calib = fl_get_button(GUI->rois->btn_uval);
    const int skip = fl_get_button(GUI->rois->btn_skip);
    const int log = fl_get_button(GUI->rois->btn_log);
    const xg3d_header *hdr = DATA->hdr;

    gsl_histogram *h = DATA->rois[ix]->hist;
    if(h == NULL) return;

    double max[2];
    gsl_sort_largest(max,2,h->bin,1,h->n);
    double maxval = skip ? max[1] : max[0];
    if(log) maxval = log1p(maxval);

    unsigned char *bits = calloc(HH*HW/8,sizeof(unsigned char));
    for(unsigned int j=0;j<NBINS/8;j++)
        for(unsigned int i=0;i<8;i++)
            for(unsigned int k=0;k<(unsigned int)round(HH * FL_clamp((log ?
                                log1p(gsl_histogram_get(h,8*j+i)) :
                                gsl_histogram_get(h,8*j+i))/maxval,0.0,1.0));k++)
                bits[NBINS/8*(HH-k-1)+j] += (1U << i);
    fl_set_bitmap_data(GUI->rois->bit_hist,HW,HH,bits);
    free(bits);

    fl_set_object_label_f(GUI->rois->txt_hist_min,"%.7g",
            flag_calib ? CALIBVAL(hdr,gsl_histogram_min(h)) : gsl_histogram_min(h));
    fl_set_object_label_f(GUI->rois->txt_hist_max,"%.7g",
            flag_calib ? CALIBVAL(hdr,gsl_histogram_max(h)) : gsl_histogram_max(h));
}
#define ENV _default_env
#undef HH
#undef HW
#undef NBINS

static void xroi_setup(xg3d_roi *xroi, const grid3d *data) {
    coord_t jm,jM,im,iM,km,kM;
    g3d_stats *stats;

    stats = g3d_roi_get_stats(data,xroi->roi,NULL,G3D_STATS_MINMAXSUM);
    xroi->min = stats->min;
    xroi->max = stats->max;
    xroi->total = stats->sum;
    free(stats);
    g3d_roi_get_bounds(xroi->roi,&jm,&jM,&im,&iM,&km,&kM);
    if(xroi->roi->size >= 1) {
        xroi->j = jm; xroi->dj = jM - jm + 1;
        xroi->i = im; xroi->di = iM - im + 1;
        xroi->k = km; xroi->dk = kM - km + 1;
    } else
        xroi->j = xroi->i = xroi->k = xroi->dj = xroi->di = xroi->dk = 0;
    _update_roi_histogram(data,xroi);
}

#define LINE "************************************************************\
****************************************"
void _refresh_roi_color(FL_OBJECT *pix_color, const FL_PACKED color) {
    static char *data[22] = {
        "100 20 1 1",
        NULL,
        LINE,LINE,LINE,LINE,LINE,LINE,LINE,LINE,LINE,LINE,
        LINE,LINE,LINE,LINE,LINE,LINE,LINE,LINE,LINE,LINE
    };

    data[1] = malloc(12*sizeof(char));
    snprintf(data[1],12,"* c #%.2x%.2x%.2x",FL_GETR(color),FL_GETG(color),FL_GETB(color));
    fl_free_pixmap_pixmap(pix_color);
    fl_set_pixmap_data(pix_color,data);
    free(data[1]);
}
#undef LINE

#undef ENV
void update_roi_values(xg3d_env *ENV) {
    for(nroi_t i=0;i<DATA->nrois;i++)
        xroi_setup(DATA->rois[i],DATA->g3d);

    update_roi_info(ENV);
}
#define ENV _default_env

int pre_roi_hist(FL_OBJECT *obj, int event, FL_Coord mx, FL_Coord my FL_UNUSED_ARG, int key, void *xev FL_UNUSED_ARG) {
    const int flag_calib = fl_get_button(GUI->rois->btn_uval);
    const Window win = fl_get_real_object_window(obj);
    const nroi_t ix = ROI_SELECTED(GUI->rois);
    const xg3d_header *hdr = DATA->hdr;
    FL_Coord x,y,w,h;
    char buf[128];
    double lo,up;
    int sw;

    if((event == FL_PUSH || event == FL_MOTION) && key == FL_LEFT_MOUSE) {
        fl_get_object_geometry(obj,&x,&y,&w,&h);
        if(mx < x) { mx = x; } else if(mx >= x+w) { mx = x+w-1; }
        gsl_histogram_get_range(DATA->rois[ix]->hist,mx-x,&lo,&up);
        snprintf(buf,sizeof(buf),"[%.7g,%.7g) %g",
                flag_calib ? CALIBVAL(hdr,lo) : lo,
                flag_calib ? CALIBVAL(hdr,up) : up,
                gsl_histogram_get(DATA->rois[ix]->hist,mx-x));
        sw = fl_get_string_width(FL_FIXED_STYLE,FL_SMALL_SIZE,buf,strlen(buf));
        fl_set_oneliner_font(FL_FIXED_STYLE,FL_SMALL_SIZE);
        fl_show_oneliner(buf,x+obj->form->x-(sw-(x+w))/2,y+obj->form->y+h);
    } else if(event == FL_RELEASE) {
        fl_hide_oneliner();
        fl_set_oneliner_font(FL_NORMAL_STYLE,FL_NORMAL_SIZE);
    } else if(event == FL_ENTER) {
        fl_set_cursor(win,FL_CROSSHAIR_CURSOR);
    } else if(event == FL_LEAVE) {
        fl_reset_cursor(win);
        fl_hide_oneliner();
        fl_set_oneliner_font(FL_NORMAL_STYLE,FL_NORMAL_SIZE);
    }
    return FL_IGNORE;
}

void cb_roi_randcolor ( FL_OBJECT * obj  , long data  ) {
    (void)data;

    for(nroi_t i=0;i<DATA->nrois;i++)
        if(fl_isselected_browser_line(GUI->rois->browser,i+1))
            DATA->rois[i]->color = (FL_PACKED)random();

                if(false) refresh_roi_browsers(ENV);
        update_roi_info(ENV);
        if(fl_get_button(GUI->overview->show_rois)) {
            refresh_ov_canvases(GUI->overview);
        }
   }

int pre_roi_color(FL_OBJECT *obj, int event, FL_Coord mx FL_UNUSED_ARG, FL_Coord my FL_UNUSED_ARG, int key FL_UNUSED_ARG, void *xev FL_UNUSED_ARG) {
    int rgb_in[3],rgb_out[3];
    nroi_t i;

    if(event == FL_DBLCLICK) {
        i = _wsel(GUI->rois->browser);
        rgb_in[0] = FL_GETR(DATA->rois[i]->color);
        rgb_in[1] = FL_GETG(DATA->rois[i]->color);
        rgb_in[2] = FL_GETB(DATA->rois[i]->color);
        if(fl_show_color_chooser(rgb_in,rgb_out)) {
            DATA->rois[i]->color = FL_PACK(rgb_out[0],rgb_out[1],rgb_out[2]);
                    if(false) refresh_roi_browsers(ENV);
        update_roi_info(ENV);
        if(fl_get_button(GUI->overview->show_rois)) {
            refresh_ov_canvases(GUI->overview);
        }
           }
    }
    return FL_IGNORE;
}

#undef ENV
void update_roi_info(xg3d_env *ENV) {
    const double uvol = fl_get_button(GUI->rois->btn_uvol) ? CM3_TO_MM3 : 1.0;
    const double ulin = fl_get_button(GUI->rois->btn_ulin) ? CM_TO_MM : 1.0;
    const nroi_t nsel = fl_get_browser_num_selected(GUI->rois->browser);
    const int flag_calib = fl_get_button(GUI->rois->btn_uval);
    const xg3d_header *hdr = DATA->hdr;
    const nroi_t nrois = DATA->nrois;
    const double vx = hdr->vx;
    const double vy = hdr->vy;
    const double vz = hdr->vz;
    xg3d_roi **rois = DATA->rois;

    double val = 0.0;
    index_t nvox = 0;

    if(nsel > 1) {
        fl_set_object_label(GUI->rois->txt_nam,_("(multiple)"));
        for(nroi_t i=0;i<nrois;i++)
            if(fl_isselected_browser_line(GUI->rois->browser,i+1)) {
                nvox += rois[i]->roi->size;
                val += rois[i]->total;
            }
        fl_set_object_label_f(GUI->rois->txt_vox,"%u",nvox);
        fl_set_object_label(GUI->rois->txt_lin,"");
        fl_set_object_label_f(GUI->rois->txt_vol,FORMAT,nvox*vx*vy*vz*uvol);
        fl_set_object_label_f(GUI->rois->txt_val,FORMAT,
                flag_calib ? CALIBSUM(hdr,val,nvox) : val);
        if(nvox == 0)
            fl_set_object_label(GUI->rois->txt_avg,"0");
        else
            fl_set_object_label_f(GUI->rois->txt_avg,FORMAT,
                    (flag_calib ? CALIBSUM(hdr,val,nvox) : val)/nvox);
        fl_hide_object(GUI->rois->pix_color);
        fl_hide_object(GUI->rois->bit_hist);
        fl_hide_object(GUI->rois->btn_log);
        fl_hide_object(GUI->rois->btn_skip);
        fl_set_object_label(GUI->rois->txt_hist_min,"");
        fl_set_object_label(GUI->rois->txt_hist_max,"");
        fl_set_object_label(GUI->rois->txt_color,_("(multiple)"));
        activate_obj(GUI->rois->btn_ulin,FL_COL1);
        activate_obj(GUI->rois->btn_uvol,FL_COL1);
        activate_obj(GUI->rois->btn_uval,FL_COL1);
    } else if(nsel == 1) {
        nroi_t i = _wsel(GUI->rois->browser);
        fl_set_object_label_f(GUI->rois->txt_nam,"%s",rois[i]->roi->name);
        fl_set_object_label_f(GUI->rois->txt_vox,"%u",rois[i]->roi->size);
        fl_set_object_label_f(GUI->rois->txt_lin,FORMAT_LINDIM,
                rois[i]->dj*vx*ulin,rois[i]->di*vy*ulin,rois[i]->dk*vz*ulin);
        fl_set_object_label_f(GUI->rois->txt_vol,FORMAT,rois[i]->roi->size*vx*vy*vz*uvol);
        fl_set_object_label_f(GUI->rois->txt_val,FORMAT,
                flag_calib ?
                CALIBSUM(hdr,rois[i]->total,rois[i]->roi->size) :
                rois[i]->total);
        if(rois[i]->roi->size == 0)
            fl_set_object_label(GUI->rois->txt_avg,"0");
        else
            fl_set_object_label_f(GUI->rois->txt_avg,FORMAT,
                    (flag_calib ?
                     CALIBSUM(hdr,rois[i]->total,rois[i]->roi->size) :
                     rois[i]->total)/rois[i]->roi->size);
        fl_set_object_label(GUI->rois->txt_color,"");
        fl_show_object(GUI->rois->pix_color);
        _refresh_roi_color(GUI->rois->pix_color,rois[i]->color);
        fl_show_object(GUI->rois->bit_hist);
        fl_show_object(GUI->rois->btn_log);
        fl_show_object(GUI->rois->btn_skip);
        _display_roi_histogram(ENV,i);
        activate_obj(GUI->rois->btn_ulin,FL_COL1);
        activate_obj(GUI->rois->btn_uvol,FL_COL1);
        activate_obj(GUI->rois->btn_uval,FL_COL1);
        ROI_SELECTED(GUI->rois) = i;
    } else {
        fl_set_object_label(GUI->rois->txt_nam,"");
        fl_set_object_label(GUI->rois->txt_vox,"");
        fl_set_object_label(GUI->rois->txt_lin,"");
        fl_set_object_label(GUI->rois->txt_vol,"");
        fl_set_object_label(GUI->rois->txt_val,"");
        fl_set_object_label(GUI->rois->txt_avg,"");
        fl_set_object_label(GUI->rois->txt_color,"");
        fl_hide_object(GUI->rois->pix_color);
        fl_hide_object(GUI->rois->bit_hist);
        fl_hide_object(GUI->rois->btn_log);
        fl_hide_object(GUI->rois->btn_skip);
        fl_set_object_label(GUI->rois->txt_hist_min,"");
        fl_set_object_label(GUI->rois->txt_hist_max,"");
        deactivate_obj(GUI->rois->btn_ulin);
        deactivate_obj(GUI->rois->btn_uvol);
        deactivate_obj(GUI->rois->btn_uval);
    }
}
#define ENV _default_env

void cb_roi_units ( FL_OBJECT * obj  , long data  ) {
    switch(data) {
        case 0: fl_set_object_label(obj,fl_get_button(obj) ? "mm" : "cm");   break; /* linear */
        case 1: fl_set_object_label(obj,fl_get_button(obj) ? "mm³" : "cm³"); break; /* volume */
        case 2: fl_set_object_label(obj,fl_get_button(obj) ? "CAL" : "DAT"); break; /* activity */
    }
    update_roi_info(ENV);
}

void cb_roi_browser ( FL_OBJECT * obj  , long data  ) {
    (void)data;

            if(false) refresh_roi_browsers(ENV);
        update_roi_info(ENV);
        if(fl_get_button(GUI->overview->show_rois)) {
            refresh_ov_canvases(GUI->overview);
        }
   }

void cb_roi_browser_select ( FL_OBJECT * obj  , long data  ) {
    (void)data;

    switch(fl_get_menu(obj)) {
        case 1: /* All */
            for(nroi_t i=0;i<DATA->nrois;i++)
                fl_select_browser_line(GUI->rois->browser,i+1);
            break;
        case 2: /* None */
            fl_deselect_browser(GUI->rois->browser);
            break;
        case 3: /* Invert */
            for(nroi_t i=0;i<DATA->nrois;i++) {
                if(fl_isselected_browser_line(GUI->rois->browser,i+1))
                    fl_deselect_browser_line(GUI->rois->browser,i+1);
                else
                    fl_select_browser_line(GUI->rois->browser,i+1);
            }
            break;
        case 4: /* Even */
            fl_deselect_browser(GUI->rois->browser);
            for(nroi_t i=0;i<DATA->nrois;i+=2)
                fl_select_browser_line(GUI->rois->browser,i+1);
            break;
        case 5: /* Odd */
            fl_deselect_browser(GUI->rois->browser);
            for(nroi_t i=1;i<DATA->nrois;i+=2)
                fl_select_browser_line(GUI->rois->browser,i+1);
            break;
    }

            if(false) refresh_roi_browsers(ENV);
        update_roi_info(ENV);
        if(fl_get_button(GUI->overview->show_rois)) {
            refresh_ov_canvases(GUI->overview);
        }
   }

#undef ENV
void refresh_roi_browsers(xg3d_env* ENV) {
    const nroi_t nrois = DATA->nrois;
    xg3d_roi **rois = DATA->rois;
    const char *name;

    fl_freeze_form(GUI->rois->rois);
    fl_freeze_form(GUI->dose->dose);
    fl_freeze_form(GUI->calib_std->calib_std);
    fl_freeze_form(GUI->dvhs->dvhs);
    fl_freeze_form(GUI->overlay->overlay);
    fl_freeze_form(GUI->operations->operations);

    fl_clear_browser(GUI->rois->browser);
    fl_clear_browser(GUI->dose->br_source);
    fl_clear_choice(GUI->calib_std->ch_roi);
    fl_clear_browser(GUI->dvhs->browser);
    fl_clear_choice(GUI->overlay->ch_reg_mask_ref);
    fl_addto_choice(GUI->overlay->ch_reg_mask_ref,_("None"));
    fl_clear_choice(GUI->operations->ch_roi);

    if(ENV->data->overlay == DATA) {
        fl_clear_choice(GUI->overlay->ch_reg_mask_flo);
        fl_addto_choice(GUI->overlay->ch_reg_mask_flo,_("None"));
        for(nroi_t i=0;i<nrois;i++)
            fl_addto_choice(GUI->overlay->ch_reg_mask_flo,rois[i]->roi->name);
    }

    for(nroi_t i=0;i<nrois;i++) {
        name = rois[i]->roi->name;
        fl_add_browser_line(GUI->rois->browser,name);
        fl_add_browser_line(GUI->dose->br_source,name);
        fl_addto_choice(GUI->calib_std->ch_roi,name);
        fl_add_browser_line(GUI->dvhs->browser,name);
        fl_addto_choice(GUI->overlay->ch_reg_mask_ref,name);
        fl_addto_choice(GUI->operations->ch_roi,name);
    }

    fl_unfreeze_form(GUI->rois->rois);
    fl_unfreeze_form(GUI->dose->dose);
    fl_unfreeze_form(GUI->calib_std->calib_std);
    fl_unfreeze_form(GUI->dvhs->dvhs);
    fl_unfreeze_form(GUI->overlay->overlay);
    fl_unfreeze_form(GUI->operations->operations);
}
#define ENV _default_env

#undef ENV
void refresh_overlay_rois(xg3d_env* ENV) {
    const nroi_t nrois_over = ENV->data->overlay->nrois;
    xg3d_roi **rois_over = ENV->data->overlay->rois;
    const nroi_t nrois = DATA->nrois;
    xg3d_roi **rois = DATA->rois;

    fl_freeze_form(GUI->overlay->overlay);
    fl_clear_choice(GUI->overlay->ch_reg_mask_ref);
    fl_addto_choice(GUI->overlay->ch_reg_mask_ref,_("None"));
    for(nroi_t i=0;i<nrois;i++)
        fl_addto_choice(GUI->overlay->ch_reg_mask_ref,rois[i]->roi->name);
    fl_clear_choice(GUI->overlay->ch_reg_mask_flo);
    fl_addto_choice(GUI->overlay->ch_reg_mask_flo,_("None"));
    for(nroi_t i=0;i<nrois_over;i++)
        fl_addto_choice(GUI->overlay->ch_reg_mask_flo,rois_over[i]->roi->name);
    fl_unfreeze_form(GUI->overlay->overlay);
}
#define ENV _default_env

void cb_roi_new ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);

    if(swk->nvox == 0) {
        fl_show_alert2(0,_("No voxels selected!\fPlease make a selection before attempting to create a ROI."));
        return;
    }

    const char *str;
    while((str = fl_show_input(_("ROI name:"),"")) != NULL && strlen(str) == 0)
        fl_show_alert2(0,_("Empty ROI name!\fPlease input a ROI name."));
    if(str == NULL) return;

    fl_show_oneliner(_("Creating ROI..."),fl_scrw/2,fl_scrh/2);
    g3d_roi *roi = g3d_roi_new(swk->g3d,str,swk->nvox);

                xg3d_roi *xroi = xroi_wrap(roi);
        xroi_setup(xroi,DATA->g3d);
        if(xroi_rename_if_duplicate(&xroi,DATA->rois,DATA->nrois))
            fl_show_alert2(0,_("Duplicate found!\fRenaming to %s."),xroi->roi->name);
        DATA->nrois++;
        DATA->rois = realloc(DATA->rois,DATA->nrois*sizeof(xg3d_roi*));
        DATA->rois[DATA->nrois-1] = xroi;
        qsort(DATA->rois,DATA->nrois,sizeof(xg3d_roi*),xroi_cmp_name);
   
            if(true) refresh_roi_browsers(ENV);
        update_roi_info(ENV);
        if(fl_get_button(GUI->overview->show_rois)) {
            refresh_ov_canvases(GUI->overview);
        }
   
    fl_hide_oneliner();
}

void cb_roi_delete ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const nroi_t nsel = fl_get_browser_num_selected(GUI->rois->browser);

    if(nsel == 0) return;

    xg3d_roi **rois = malloc((DATA->nrois-nsel)*sizeof(xg3d_roi*));
    nroi_t k = 0;
    for(nroi_t i=0;i<DATA->nrois;i++)
        if(fl_isselected_browser_line(GUI->rois->browser,i+1))
            xroi_free(DATA->rois[i]);
        else
            rois[k++] = DATA->rois[i];
    DATA->nrois -= nsel;
    free(DATA->rois);
    DATA->rois = rois;

            if(true) refresh_roi_browsers(ENV);
        update_roi_info(ENV);
        if(fl_get_button(GUI->overview->show_rois)) {
            refresh_ov_canvases(GUI->overview);
        }
   }

void cb_roi_select ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);
    nroi_t nsel = fl_get_browser_num_selected(GUI->rois->browser);
    g3d_roi_list *list;
    int m;

    if(nsel == 0) {
        fl_show_alert2(0,_("No ROI selected!\fPlease select at least one ROI for selection."));
        return;
    }

    switch((m = fl_get_menu(obj))) {
        case 1: /* Only */
            fl_call_object_callback(GUI->selection->btn_clear_global);
            /* fall-through */
        case 2: /* Add */
        case 3: /* Remove */
            /* create temporary ROI list */
            list = g3d_roi_list_alloc(nsel);
            for(nroi_t i=0,k=0;i<DATA->nrois;i++)
                if(fl_isselected_browser_line(GUI->rois->browser,i+1))
                    list->rois[k++] = DATA->rois[i]->roi;

            /* perform boolean operation on ROI list */
            rois_select(list,swk->g3d,m < 3);

            /* clear temporary ROI list */
            for(nroi_t i=0;i<list->size;i++)
                list->rois[i] = NULL;
            g3d_roi_list_free(list);

            /* recompute selection */
            sel_oper_count(swk->g3d,DATA->g3d,0,0,0,0,&swk->nvox,&swk->value);
            break;
    }
    replace_selslice(GUI->viewport,GUI->selection);
    sel_sanity_check(GUI->viewport,GUI->selection);
    sel_update_info(GUI->viewport,GUI->selection);
    refresh_canvas(ENV);
    if(fl_get_button(GUI->overview->draw_sel))
        refresh_ov_canvases(GUI->overview);
}

void cb_roi_rename ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const nroi_t nsel = fl_get_browser_num_selected(GUI->rois->browser);
    xg3d_roi *xroi, *tmp_xroi;
    const char *str;

    if(nsel == 0) return;
    else if(nsel > 1) {
        fl_show_alert2(0,_("More than one ROI selected!\fPlease select only one ROI for renaming."));
        return;
    }

    xroi = DATA->rois[_wsel(GUI->rois->browser)];
    str = fl_show_input(_("ROI name:"),xroi->roi->name);
    if(str == NULL || strlen(str) == 0) return;
    if(strcmp(xroi->roi->name,str) == 0) return;
    tmp_xroi = xroi_alloc(str,1);
    if(xroi_rename_if_duplicate(&tmp_xroi,DATA->rois,DATA->nrois))
        fl_show_alert2(0,_("Duplicate found!\fRenaming to %s."),tmp_xroi->roi->name);
    free(xroi->roi->name);
    xroi->roi->nlen = strlen(tmp_xroi->roi->name);
    xroi->roi->name = strdup(tmp_xroi->roi->name);
    tmp_xroi->roi->size = 0; xroi_free(tmp_xroi);
    qsort(DATA->rois,DATA->nrois,sizeof(xg3d_roi*),xroi_cmp_name);

    refresh_roi_browsers(ENV);
    update_roi_info(ENV);
}

void cb_roi_boolop ( FL_OBJECT * obj  , long data  ) {
    const nroi_t nsel = fl_get_browser_num_selected(GUI->rois->browser);
    const grid3d *selection = SELECTION_G3D(GUI->selection);

    if(nsel < 2) {
        fl_show_alert2(0,_("Less than two ROIs selected!\fPlease select at least two ROIs before attempting a boolean operation."));
        return;
    }

    /* create temporary ROI list */
    g3d_roi_list *list = g3d_roi_list_alloc(nsel);
    for(nroi_t i=0,k=0;i<DATA->nrois;i++)
        if(fl_isselected_browser_line(GUI->rois->browser,i+1))
            list->rois[k++] = DATA->rois[i]->roi;

    /* perform boolean operation on ROI list */
    g3d_roi *res;
    g3d_ret ret = rois_boolop(selection,list,data,&res);

    /* clear temporary ROI list */
    for(nroi_t i=0;i<list->size;i++)
        list->rois[i] = NULL;
    g3d_roi_list_free(list);

    if(ret != G3D_OK) {
        fl_show_alert2(0,_("Empty ROI!\fResult of operation is empty, no ROI created."));
        return;
    }

            xg3d_roi *xroi = xroi_wrap(res);
        xroi_setup(xroi,DATA->g3d);
        if(xroi_rename_if_duplicate(&xroi,DATA->rois,DATA->nrois))
            fl_show_alert2(0,_("Duplicate found!\fRenaming to %s."),xroi->roi->name);
        DATA->nrois++;
        DATA->rois = realloc(DATA->rois,DATA->nrois*sizeof(xg3d_roi*));
        DATA->rois[DATA->nrois-1] = xroi;
        qsort(DATA->rois,DATA->nrois,sizeof(xg3d_roi*),xroi_cmp_name);
               if(true) refresh_roi_browsers(ENV);
        update_roi_info(ENV);
        if(fl_get_button(GUI->overview->show_rois)) {
            refresh_ov_canvases(GUI->overview);
        }
   }

void cb_roi_load ( FL_OBJECT * obj  , long data  ) {
    xg3d_roi **xrois, *xroi;
    g3d_roi_list *list;
    const char *file;

    switch(data) {
        case 0: /* as list */
            file = fl_show_fselector(_("Load ROIs list from..."),"","*.roi*","");
            if(file == NULL) return;

            /* load ROIs from file */
            list = g3d_roi_list_load(file,DATA->g3d);
            if(list == NULL) {
                fl_show_alert2(0,_("No ROIs loaded!\fFailed loading ROIs from:\n%s"),file);
                return;
            } else if(list->size == 0) {
                fl_show_alert2(0,_("No ROIs loaded!\fAll ROIs out of bounds."));
                g3d_roi_list_free(list);
                return;
            }

            /* create xrois and rename if duplicate */
            xrois = malloc(list->size*sizeof(xg3d_roi*));
            nroi_t dup = 0;
            for(nroi_t i=0;i<list->size;i++) {
                xrois[i] = xroi = xroi_wrap(list->rois[i]);
                xroi_setup(xroi,DATA->g3d);
                dup += xroi_rename_if_duplicate(&xroi,DATA->rois,DATA->nrois);
            }

            /* add xrois to global ROI list */
            DATA->rois = realloc(DATA->rois,(DATA->nrois+list->size)*sizeof(xg3d_roi*));
            memcpy(&DATA->rois[DATA->nrois],xrois,list->size*sizeof(xg3d_roi*)); free(xrois);
            DATA->nrois += list->size;
            qsort(DATA->rois,DATA->nrois,sizeof(xg3d_roi*),xroi_cmp_name);

                    if(true) refresh_roi_browsers(ENV);
        update_roi_info(ENV);
        if(fl_get_button(GUI->overview->show_rois)) {
            refresh_ov_canvases(GUI->overview);
        }
   
            fl_show_alert2(0,dup > 0 ?
                    _("%d ROIs successfully loaded!\f%d duplicates renamed to avoid confusion.") :
                    _("%d ROIs successfully loaded!\fNo duplicates found."),list->size,dup);

            list->size = 0; g3d_roi_list_free(list);
            break;
    }
}

void cb_roi_save ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const nroi_t nsel = fl_get_browser_num_selected(GUI->rois->browser);
    const grid3d *selection = SELECTION_G3D(GUI->selection);
    const xg3d_header *hdr = DATA->hdr;
    g3d_roi_list *list;
    char buf[PATH_MAX];
    const char *file;
    xg3d_header xhdr;
    xg3d_data dat;
    grid3d *msk;
    g3d_ret ret;

    if(nsel == 0) {
        fl_show_alert2(0,_("No ROI selected!\fPlease select at least one ROI for saving."));
        return;
    }

    switch(fl_get_menu(obj)) {
        case 1: /* as list */
            if(nsel == 1)
                snprintf(buf,sizeof(buf),"%s.roi.gz",DATA->rois[_wsel(GUI->rois->browser)]->roi->name);
            else buf[0] = '\0';

            file = fl_show_fselector(_("Save ROIs list as..."),"","*.roi.gz",buf);
            if(file == NULL) return;
            fl_show_oneliner(_("Saving ROI list..."),fl_scrw/2,fl_scrh/2);

            /* prepare list */
            list = g3d_roi_list_alloc(DATA->nrois);
            for(index_t ix=0;ix<list->size;ix++)
                list->rois[ix] = DATA->rois[ix]->roi;

            /* save list */
            if((ret = g3d_roi_list_save(file,list)) != G3D_OK)
                fl_show_alert2(0,_("Failed saving ROI list!"));

            list->size = 0; g3d_roi_list_free(list);
            fl_hide_oneliner();
            break;
        case 2: /* as mask */
            if(nsel == 1)
                snprintf(buf,sizeof(buf),"%s",DATA->rois[_wsel(GUI->rois->browser)]->roi->name);
            else buf[0] = '\0';

            if(DATA->nrois <= UINT8_MAX) {
                msk = g3d_alloc(G3D_UINT8,selection->nx,selection->ny,selection->nz,.alloc=true);
                xhdr.type = RAW_UINT8;
                for(nroi_t i=0;i<DATA->nrois;i++)
                    for(index_t ix=0;ix<DATA->rois[i]->roi->size;ix++)
                        g3d_u8(msk,ROI_IX(ix,DATA->rois[i]->roi,msk)) = i+1;
            } else {
                msk = g3d_alloc(G3D_UINT16,selection->nx,selection->ny,selection->nz,.alloc=true);
                xhdr.type = RAW_UINT16;
                for(nroi_t i=0;i<DATA->nrois;i++)
                    for(index_t ix=0;ix<DATA->rois[i]->roi->size;ix++)
                        g3d_u16(msk,ROI_IX(ix,DATA->rois[i]->roi,msk)) = i+1;
            }
            xhdr.dim[0] = msk->nx; xhdr.dim[1] = msk->ny; xhdr.dim[2] = msk->nz;
            xhdr.vx = hdr->vx; xhdr.vy = hdr->vy; xhdr.vz = hdr->vz;
            xhdr.cal_slope = 1.0; xhdr.cal_inter = 0.0; xhdr.acqmod = ACQ_UNKNOWN;
            dat.hdr = &xhdr; dat.g3d = msk;
            savedlg_show(GUI->savedlg,&dat,buf);
            g3d_free(msk);
            break;
    }
}

void cb_roi_export ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const nroi_t nsel = fl_get_browser_num_selected(GUI->rois->browser);
    const grid3d *selection = SELECTION_G3D(GUI->selection);
    const xg3d_header *hdr = DATA->hdr;
    const double calib[2] = {hdr->cal_inter,
                             hdr->cal_slope};
    const char *file = NULL;
    grid3d *chk = NULL;
    char buf[PATH_MAX];
    xg3d_header xhdr;
    xg3d_roi *xroi;
    xg3d_data dat;
    double *vals;
    FILE *fp;

    if(nsel == 0) return;
    else if(nsel > 1) {
        fl_show_alert2(0,_("More than one ROI selected!\fPlease select only one ROI for exporting."));
        return;
    }

    xroi = DATA->rois[_wsel(GUI->rois->browser)];
    switch(fl_get_menu(obj)) {
        case 1: /* Data */
            chk = g3d_roi_chunk(DATA->g3d,xroi->roi);
            xhdr.type = hdr->type;
            xhdr.dim[0] = chk->nx; xhdr.dim[1] = chk->ny; xhdr.dim[2] = chk->nz;
            xhdr.vx = hdr->vx; xhdr.vy = hdr->vy; xhdr.vz = hdr->vz;
            xhdr.cal_slope = hdr->cal_slope; xhdr.cal_inter = hdr->cal_inter;
            xhdr.acqmod = hdr->acqmod;
            dat.hdr = &xhdr; dat.g3d = chk;
            savedlg_show(GUI->savedlg,&dat,xroi->roi->name);
            g3d_free(chk);
            break;
        case 2: /* Values */
            snprintf(buf,sizeof(buf),"%s.txt",xroi->roi->name);
            file = fl_show_fselector(_("Export ROI values (calibrated) as..."),"","*.txt",buf);
            if(file == NULL) return;
            fp = fopen(file,"w");
            if(fp == NULL) {
                fl_show_alert2(0,_("Couldn't export values!\fError opening\n%s\nfor writing."),file);
                return;
            }
            vals = g3d_roi_get_dvalues(DATA->g3d,xroi->roi,calib);
            for(index_t ix=0;ix<xroi->roi->size;ix++)
                fprintf(fp,"%u %u %u %.12g\n",
                    ROI_J(ix,xroi->roi),
                    ROI_I(ix,xroi->roi),
                    ROI_K(ix,xroi->roi),
                    vals[ix]);
            free(vals);
            fclose(fp);
            break;
        case 3: /* Mask (signed integer) */
            snprintf(buf,sizeof(buf),"%s_mask",xroi->roi->name);
            chk = g3d_alloc(G3D_INT8,selection->nx,selection->ny,selection->nz,.alloc=true);
            for(index_t ix=0;ix<xroi->roi->size;ix++)
                ((int8_t*)chk->dat)[ROI_IX(ix,xroi->roi,chk)] = 1;
            xhdr.type = RAW_INT8;
            xhdr.dim[0] = chk->nx; xhdr.dim[1] = chk->ny; xhdr.dim[2] = chk->nz;
            xhdr.vx = hdr->vx; xhdr.vy = hdr->vy; xhdr.vz = hdr->vz;
            xhdr.cal_slope = 1.0; xhdr.cal_inter = 0.0; xhdr.acqmod = ACQ_UNKNOWN;
            dat.hdr = &xhdr; dat.g3d = chk;
            savedlg_show(GUI->savedlg,&dat,buf);
            g3d_free(chk);
            break;
    }
}

void cb_roi_translate ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const nroi_t nsel = fl_get_browser_num_selected(GUI->rois->browser);
    FL_OBJECT *but;
    int dx,dy,dz;
    nroi_t i;

    if(nsel == 0) return;
    else if(nsel > 1) {
        fl_show_alert2(0,_("More than one ROI selected!\fPlease select only one ROI for color setting."));
        return;
    }

    i = _wsel(GUI->rois->browser);
    fl_set_object_label_f(GUI->roi_trans->txt_nam,"%s",DATA->rois[i]->roi->name);
    fl_set_choice(GUI->roi_trans->ch_unit,1);
    fl_set_object_label_f(GUI->roi_trans->txt_xi,"%d",DATA->rois[i]->j);
    fl_set_object_label_f(GUI->roi_trans->txt_yi,"%d",DATA->rois[i]->i);
    fl_set_object_label_f(GUI->roi_trans->txt_zi,"%d",DATA->rois[i]->k);
    fl_set_object_label_f(GUI->roi_trans->txt_xf,"%d",DATA->rois[i]->j+DATA->rois[i]->dj-1);
    fl_set_object_label_f(GUI->roi_trans->txt_yf,"%d",DATA->rois[i]->i+DATA->rois[i]->di-1);
    fl_set_object_label_f(GUI->roi_trans->txt_zf,"%d",DATA->rois[i]->k+DATA->rois[i]->dk-1);
    fl_set_slider_bounds(GUI->roi_trans->sli_dx,-(double)DATA->rois[i]->j,
            (double)DATA->g3d->nx-DATA->rois[i]->j-DATA->rois[i]->dj);
    fl_set_slider_value(GUI->roi_trans->sli_dx,0.0);
    fl_set_slider_bounds(GUI->roi_trans->sli_dy,-(double)DATA->rois[i]->i,
            (double)DATA->g3d->ny-DATA->rois[i]->i-DATA->rois[i]->di);
    fl_set_slider_value(GUI->roi_trans->sli_dy,0.0);
    fl_set_slider_bounds(GUI->roi_trans->sli_dz,-(double)DATA->rois[i]->k,
            (double)DATA->g3d->nz-DATA->rois[i]->k-DATA->rois[i]->dk);
    fl_set_slider_value(GUI->roi_trans->sli_dz,0.0);
    fl_set_input(GUI->roi_trans->inp_dx,"0");
    fl_set_input(GUI->roi_trans->inp_dy,"0");
    fl_set_input(GUI->roi_trans->inp_dz,"0");
    fl_deactivate_all_forms();
    fl_show_form_f(GUI->roi_trans->roi_trans,FL_PLACE_HOTSPOT,FL_TRANSIENT,XG3D_TITLE_FORMAT,_("ROI translation"));
    while((but = fl_do_only_forms()))
        if(but == GUI->roi_trans->btn_cancel || but == GUI->roi_trans->but_ok)
            break;
    fl_activate_all_forms();
    if(but == GUI->roi_trans->but_ok) {
        dx = (int)fl_get_slider_value(GUI->roi_trans->sli_dx);
        dy = (int)fl_get_slider_value(GUI->roi_trans->sli_dy);
        dz = (int)fl_get_slider_value(GUI->roi_trans->sli_dz);
        if(g3d_roi_translate(DATA->rois[i]->roi,dx,dy,dz) == G3D_OK)
            xroi_setup(DATA->rois[i],DATA->g3d);
    }
    fl_hide_form(GUI->roi_trans->roi_trans);
    fl_activate_all_forms();

            if(false) refresh_roi_browsers(ENV);
        update_roi_info(ENV);
        if(fl_get_button(GUI->overview->show_rois)) {
            refresh_ov_canvases(GUI->overview);
        }
   }

void cb_roi_trans_unit ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const nroi_t i = _wsel(GUI->rois->browser);
    const xg3d_header *hdr = DATA->hdr;
    double ux,uy,uz;
    double val;

    switch(fl_get_choice(obj)) {
        default:
        case 1: /* vox */
            ux = uy = uz = 1.0;
            break;
        case 2: /* cm */
            ux = hdr->vx;
            uy = hdr->vy;
            uz = hdr->vz;
            break;
        case 3: /* mm */
            ux = hdr->vx * CM_TO_MM;
            uy = hdr->vy * CM_TO_MM;
            uz = hdr->vz * CM_TO_MM;
            break;
    }

    val = fl_get_slider_value(GUI->roi_trans->sli_dx);
    fl_set_object_label_f(GUI->roi_trans->txt_xi,"%.4g",(DATA->rois[i]->j+(int)val)*ux);
    fl_set_object_label_f(GUI->roi_trans->txt_xf,"%.4g",(DATA->rois[i]->j+DATA->rois[i]->dj-1+(int)val)*ux);
    fl_set_input_f(GUI->roi_trans->inp_dx,"%.4g",(int)val*ux);
    val = fl_get_slider_value(GUI->roi_trans->sli_dy);
    fl_set_object_label_f(GUI->roi_trans->txt_yi,"%.4g",(DATA->rois[i]->i+(int)val)*uy);
    fl_set_object_label_f(GUI->roi_trans->txt_yf,"%.4g",(DATA->rois[i]->i+DATA->rois[i]->di-1+(int)val)*uy);
    fl_set_input_f(GUI->roi_trans->inp_dy,"%.4g",(int)val*uy);
    val = fl_get_slider_value(GUI->roi_trans->sli_dz);
    fl_set_object_label_f(GUI->roi_trans->txt_zi,"%.4g",(DATA->rois[i]->k+(int)val)*uz);
    fl_set_object_label_f(GUI->roi_trans->txt_zf,"%.4g",(DATA->rois[i]->k+DATA->rois[i]->dk-1+(int)val)*uz);
    fl_set_input_f(GUI->roi_trans->inp_dz,"%.4g",(int)val*uz);
}

void cb_roi_trans_slider ( FL_OBJECT * obj  , long data  ) {
    const nroi_t i = _wsel(GUI->rois->browser);
    const double val = fl_get_slider_value(obj);
    const xg3d_header *hdr = DATA->hdr;
    double ux,uy,uz;

    switch(fl_get_choice(GUI->roi_trans->ch_unit)) {
        default:
        case 1: /* vox */
            ux = uy = uz = 1.0;
            break;
        case 2:
            ux = hdr->vx;
            uy = hdr->vy;
            uz = hdr->vz;
            break;
        case 3:
            ux = hdr->vx * CM_TO_MM;
            uy = hdr->vy * CM_TO_MM;
            uz = hdr->vz * CM_TO_MM;
            break;
    }

    switch(data) {
        case 0: /* dx */
            fl_set_object_label_f(GUI->roi_trans->txt_xi,"%.4g",(DATA->rois[i]->j+(int)val)*ux);
            fl_set_object_label_f(GUI->roi_trans->txt_xf,"%.4g",(DATA->rois[i]->j+DATA->rois[i]->dj-1+(int)val)*ux);
            fl_set_input_f(GUI->roi_trans->inp_dx,"%.4g",(int)val*ux);
            break;
        case 1: /* dy */
            fl_set_object_label_f(GUI->roi_trans->txt_yi,"%.4g",(DATA->rois[i]->i+(int)val)*uy);
            fl_set_object_label_f(GUI->roi_trans->txt_yf,"%.4g",(DATA->rois[i]->i+DATA->rois[i]->di-1+(int)val)*uy);
            fl_set_input_f(GUI->roi_trans->inp_dy,"%.4g",(int)val*uy);
            break;
        case 2: /* dz */
            fl_set_object_label_f(GUI->roi_trans->txt_zi,"%.4g",(DATA->rois[i]->k+(int)val)*uz);
            fl_set_object_label_f(GUI->roi_trans->txt_zf,"%.4g",(DATA->rois[i]->k+DATA->rois[i]->dk-1+(int)val)*uz);
            fl_set_input_f(GUI->roi_trans->inp_dz,"%.4g",(int)val*uz);
            break;
    }
}

void cb_roi_trans_inp ( FL_OBJECT * obj  , long data  ) {
    const xg3d_header *hdr = DATA->hdr;
    const char *txt = fl_get_input(obj);
    double invunit,factor=1.0;
    FL_OBJECT *sli = NULL;
    double val,min,max;

    val = strtod_checked(txt,0.0);

    switch(data) {
        case 0: /* dx */
            sli = GUI->roi_trans->sli_dx;
            factor = hdr->vx;
            break;
        case 1: /* dy */
            sli = GUI->roi_trans->sli_dy;
            factor = hdr->vy;
            break;
        case 2: /* dz */
            sli = GUI->roi_trans->sli_dz;
            factor = hdr->vz;
            break;
    }

    fl_get_slider_bounds(sli,&min,&max);
    switch(fl_get_choice(GUI->roi_trans->ch_unit)) {
        default:
        case 1: invunit = 1.0;               break; /* vox */
        case 2: invunit = 1.0 / factor;      break; /* cm */
        case 3: invunit = MM_TO_CM / factor; break; /* mm */
    }
    val = FL_clamp(roundf(val*invunit),min,max);
    fl_set_slider_value(sli,val);
    fl_call_object_callback(sli);
}

#undef ENV
static void _update_roi_stats(xg3d_env *ENV) {
    FL_OBJECT *browser = GUI->roi_stats->browser;
    const char *ulab = fl_get_input(GUI->calibration->inp_ulabel);
    const int flag_uvol = fl_get_button(GUI->rois->btn_uvol);
    const int flag_ulin = fl_get_button(GUI->rois->btn_ulin);
    const int flag_uval = fl_get_button(GUI->rois->btn_uval);
    const xg3d_header *hdr = DATA->hdr;
    const double slope = flag_uval ? hdr->cal_slope : 1.0;
    const double inter = flag_uval ? hdr->cal_inter : 0.0;
    const double calib[2] = {inter,slope};
    const double uvol = flag_uvol ? CM3_TO_MM3 : 1.0;
    const double ulin = flag_ulin ? CM_TO_MM : 1.0;
    const char *uvol_txt = flag_uvol ? "mm" : "cm";
    const char *ulin_txt = flag_uvol ? "mm" : "cm";
    const nroi_t nrois = DATA->nrois;
    const double vx = hdr->vx;
    const double vy = hdr->vy;
    const double vz = hdr->vz;
    xg3d_roi **rois = DATA->rois;
    char uval_txt[16];

    snprintf(uval_txt,sizeof(uval_txt),"%s",flag_uval
            ? (strlen(ulab) > 0 ? ulab : _("cal. units"))
            : _("arb. units"));

    fl_clear_browser(browser);
    for(nroi_t i=0;i<nrois;i++)
        if(fl_isselected_browser_line(GUI->rois->browser,i+1)) {
            xg3d_roi *xroi = rois[i];
            g3d_stats *stats = g3d_roi_get_stats(DATA->g3d,xroi->roi,calib,G3D_STATS_ALL);
            fl_add_browser_line_f(browser,"%-20s\t%s",_("ROI name"),xroi->roi->name);
            fl_add_browser_line_f(browser,"%-20s\t%u",_("# voxels"),xroi->roi->size);
            fl_add_browser_line_f(browser,"%-20s\t%.12g\t%2s³",_("Volume"),xroi->roi->size*vx*vy*vz*uvol,uvol_txt);
            fl_add_browser_line_f(browser,"%-20s\t%.12g\t%2s",_("Extents x"),xroi->dj*vx*ulin,ulin_txt);
            fl_add_browser_line_f(browser,"%-20s\t%.12g\t%2s",_("Extents y"),xroi->di*vy*ulin,ulin_txt);
            fl_add_browser_line_f(browser,"%-20s\t%.12g\t%2s",_("Extents z"),xroi->dk*vz*ulin,ulin_txt);
            fl_add_browser_line_f(browser,"%-20s\t%.12g\t%s",_("Total"),stats->sum,uval_txt);
            fl_add_browser_line_f(browser,"%-20s\t%.12g\t%s",_("Minimum"),stats->min,uval_txt);
            fl_add_browser_line_f(browser,"%-20s\t%.12g\t%s",_("Maximum"),stats->max,uval_txt);
            fl_add_browser_line_f(browser,"%-20s\t%.12g\t%s",_("Mean"),stats->mean,uval_txt);
            fl_add_browser_line_f(browser,"%-20s\t%.12g\t%s",_("Std. deviation"),stats->stdev,uval_txt);
            fl_add_browser_line_f(browser,"%-20s\t%.12g\t%s",_("Median"),stats->median,uval_txt);
            fl_add_browser_line(browser,"");
            free(stats);
        }
}
#define ENV _default_env

void cb_roi_stats ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const int button = fl_get_button_numb(obj);
    FL_FORM *form = GUI->roi_stats->roi_stats;
    char buf[1024];

    form = GUI->roi_stats->roi_stats;
    snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("ROI statistics"));

    if(button == FL_RIGHT_MOUSE && fl_form_is_visible(form)) {
        fl_set_button(obj,1);
        fl_raise_form(form);
        fl_winfocus(form->window);
        return;
    }

    if(fl_form_is_visible(form)) {
        fl_hide_form(form);
        fl_deactivate_form(form);
        return;
    } else {
        fl_activate_form(form);
        fl_show_form(form,FL_PLACE_SIZE,FL_FULLBORDER,buf);
        fl_raise_form(form);
        fl_winfocus(form->window);
    }

    if(form == GUI->roi_stats->roi_stats)
        _update_roi_stats(ENV);
}

void cb_roi_stats_update ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    _update_roi_stats(ENV);
}

void cb_roi_stats_io ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    FL_OBJECT *browser = GUI->roi_stats->browser;
    char buf[PATH_MAX];

    const char *file = fl_show_fselector(_("Save ROI stats as..."),"","*.txt","");
    if(file == NULL) return;
    enforce_ext(file,"txt",buf,sizeof(buf));
    FILE *fp = fopen(file,"w");
    fprintf(fp,"%-20s\t%s\r\n\r\n",_("Data file"),fl_get_input(GUI->info->inp_path));
    for(int i=1;i<fl_get_browser_maxline(browser);i++)
        fprintf(fp,"%s\r\n",fl_get_browser_line(browser,i));
    fclose(fp);
}

void cb_roi_kinetics ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const nroi_t nsel = fl_get_browser_num_selected(GUI->rois->browser);
    const xg3d_header *hdr = DATA->hdr;
    const double calib[2] = {hdr->cal_inter,
                             hdr->cal_slope};
    xg3d_kinetics_workspace *kwk = KINETICS_WORKSPACE(GUI->kinetics);
    char *file,*base,buf[PATH_MAX];
    xg3d_frame *xframe;
    xg3d_roi *xroi;
    uintmax_t t;
    int ret;

    if(nsel == 0) return;
    else if(nsel > 1) {
        fl_show_alert2(0,_("More than one ROI selected!\fPlease select only one ROI for kinetics."));
        return;
    }

    snprintf(buf,sizeof(buf),_("Is current calibration OK?\nSlope: %g Intercept: %g"),hdr->cal_slope,hdr->cal_inter);
    if(!fl_show_question(buf,1)) return;
    if((ret = kin_input_time(&t,"")) < 1) {
        if(ret < 0)
            fl_show_alert2(0,_("Invalid time!\fPlease input a valid time for this frame."));
        return;
    }
    xroi = DATA->rois[_wsel(GUI->rois->browser)];
    if(kwk->nframes == 0) { /* add ROI */
        kwk->xroi = xroi_dup(xroi);
        fl_set_object_label_f(GUI->kinetics->txt_roi,"%-s",xroi->roi->name);
        fl_set_object_label_f(GUI->kinetics->txt_roi_size,"%zu vox",xroi->roi->size);
        fl_set_object_label_f(GUI->kinetics->txt_roi_vol,"%.6g cm³",xroi->roi->size*hdr->vx*hdr->vy*hdr->vz);
        fl_set_object_label_f(GUI->kinetics->txt_roi_vox,"%.3g × %.3g × %.3g cm",hdr->vx,hdr->vy,hdr->vz);
    } else /* check for same ROI */
        if(!xroi_equal(xroi,kwk->xroi)) {
            fl_show_alert2(0,_("Different ROI found!\fPlease use the same ROI throughout all frames."));
            return;
        }
    base = strdup(hdr->file); file = strdup(basename(base)); free(base);
    for(unsigned int i=0;i<kwk->nframes;i++) /* check for duplicate file or time */
        if(strcmp(file,kwk->frames[i]->file) == 0 || t == kwk->frames[i]->t) {
            fl_show_alert2(0,_("Duplicate values found!\fPlease add only one frame per file and time."));
            free(file);
            return;
        }
    xframe = calloc(1,sizeof(xg3d_frame));
    xframe->t = t;
    xframe->file = file;
    xframe->vals = g3d_roi_get_dvalues(DATA->g3d,kwk->xroi->roi,calib);
    kwk->nframes++;
    kwk->frames = realloc(kwk->frames,kwk->nframes*sizeof(xg3d_frame*));
    kwk->frames[kwk->nframes-1] = xframe;
    qsort(kwk->frames,kwk->nframes,sizeof(xg3d_frame*),frame_cmp_time);
    refresh_kin_browser(GUI->kinetics);
}

void cb_roi_mss ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const nroi_t nsel = fl_get_browser_num_selected(GUI->rois->browser);

    if(nsel == 0) return;
    else if(nsel > 1) {
        fl_show_alert2(0,_("More than one ROI selected!\fPlease select only one ROI for MSS calculation."));
        return;
    }

    g3d_roi *roi;
    if(rois_mss(DATA->rois[_wsel(GUI->rois->browser)]->roi,&roi) != G3D_OK){
        fl_show_alert2(0,_("Empty MSS!\fNo ROI created."));
        return;
    }

            xg3d_roi *xroi = xroi_wrap(roi);
        xroi_setup(xroi,DATA->g3d);
        if(xroi_rename_if_duplicate(&xroi,DATA->rois,DATA->nrois))
            fl_show_alert2(0,_("Duplicate found!\fRenaming to %s."),xroi->roi->name);
        DATA->nrois++;
        DATA->rois = realloc(DATA->rois,DATA->nrois*sizeof(xg3d_roi*));
        DATA->rois[DATA->nrois-1] = xroi;
        qsort(DATA->rois,DATA->nrois,sizeof(xg3d_roi*),xroi_cmp_name);
               if(true) refresh_roi_browsers(ENV);
        update_roi_info(ENV);
        if(fl_get_button(GUI->overview->show_rois)) {
            refresh_ov_canvases(GUI->overview);
        }
   }

void cb_roi_cc ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);
    const xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);

    nroi_t nr = 0;
    for(index_t ix=0;ix<DATA->g3d->nvox;ix++) {
        if(g3d_seltmp_check(swk->g3d,ix) || 
                g3d_quant_apply_single(g3d_get_dvalue(DATA->g3d,ix),qwk->info) == 0) continue;
        double val;
        index_t nvx = ix;
        char buf[128];
        sel_mode_level_comp(swk->g3d,DATA->g3d,SCOPE_GLOBAL,PLANE_XY,false,qwk->info,&nvx,&val);
        snprintf(buf,sizeof(buf),"Component %04d",nr);
        g3d_roi *roi = g3d_roi_alloc(buf,nvx);
        index_t nvox = 0;
        for(index_t cx=0;cx<swk->g3d->nvox;cx++)
            if(g3d_selbit_check(swk->g3d,cx)) {
                xg3d_sel_bcts(swk->g3d,cx);
                ROI_J(nvox,roi) = (cx%swk->g3d->nxy)%swk->g3d->nx;
                ROI_I(nvox,roi) = (cx%swk->g3d->nxy)/swk->g3d->nx;
                ROI_K(nvox,roi) = (cx/swk->g3d->nxy);
                if(++nvox == nvx) break;
            }
                xg3d_roi *xroi = xroi_wrap(roi);
        xroi_setup(xroi,DATA->g3d);
        if(xroi_rename_if_duplicate(&xroi,DATA->rois,DATA->nrois))
            fl_show_alert2(0,_("Duplicate found!\fRenaming to %s."),xroi->roi->name);
        DATA->nrois++;
        DATA->rois = realloc(DATA->rois,DATA->nrois*sizeof(xg3d_roi*));
        DATA->rois[DATA->nrois-1] = xroi;
        qsort(DATA->rois,DATA->nrois,sizeof(xg3d_roi*),xroi_cmp_name);
           nr++;
    }

    sel_clear(swk->g3d);

            if(true) refresh_roi_browsers(ENV);
        update_roi_info(ENV);
        if(fl_get_button(GUI->overview->show_rois)) {
            refresh_ov_canvases(GUI->overview);
        }
   }

void cb_roi_recalculate ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    
    if(fl_get_browser_num_selected(GUI->rois->browser) == 0) return;

    for(nroi_t i=0;i<DATA->nrois;i++)
        if(fl_isselected_browser_line(GUI->rois->browser,i+1))
            xroi_setup(DATA->rois[i],DATA->g3d);
    update_roi_info(ENV);
}

void cb_roi_hist_refresh ( FL_OBJECT * obj  , long data  ) {
    (void)data;
   if(fl_get_browser_num_selected(GUI->rois->browser) == 1) 
       _display_roi_histogram(ENV,_wsel(GUI->rois->browser));
}
