#include <math.h>
#include <float.h>
#include <libgen.h>

#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "grid3d.h"
#include "grid3d_roi.h"
#include "cb_dvhs.h"

#include <gsl/gsl_sort.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_statistics_double.h>

#include "util.h"

#define NBINS 77

/* WARNING: assumes all voxels have same volume and mass! */
void cb_dvh_compute ( FL_OBJECT * obj  , long data  ) {
    (void)data;
	const unsigned int ibr = fl_get_browser(GUI->dvhs->browser);
    const g3d_roi *roi = ibr >= 1 ? DATA->rois[ibr-1]->roi : NULL;

	if(ENV->data->dose == NULL) {
		fl_show_alert2(0,_("No dose estimate!\fPlease compute or import a dose estimate first."));
        return;
    }
    if(roi == NULL) {
        fl_show_alert2(0,_("No ROI selected!\fPlease select a target ROI."));
        return;
    }

    g3d_stats *stats = g3d_roi_get_stats(ENV->data->dose->g3d,roi,NULL,G3D_STATS_ALL);
    double *vals = g3d_roi_get_dvalues(ENV->data->dose->g3d,roi,NULL);

    gsl_histogram *h = gsl_histogram_alloc(NBINS);
    gsl_histogram_pdf *p = gsl_histogram_pdf_alloc(NBINS);
    gsl_histogram_set_ranges_uniform(h,stats->min,nextafter(stats->max,HUGE_VAL));
    for(index_t ix=0;ix<roi->size;ix++) gsl_histogram_increment(h,vals[ix]);
    gsl_histogram_scale(h,1.0/gsl_histogram_sum(h));
    gsl_histogram_pdf_init(p,h);

    float *intx = malloc(NBINS*sizeof(double));
    float *inty = malloc(NBINS*sizeof(double));
    float *difx = malloc(NBINS*sizeof(double));
    float *dify = malloc(NBINS*sizeof(double));
    for(unsigned int i=0;i<NBINS;i++) {
        difx[i] = (h->range[i] + h->range[i+1])/2.0;
        dify[i] = h->bin[i] * 100.0; /* percent */
        intx[i] = p->range[i];
        inty[i] = (1.0 - p->sum[i]) * 100.0; /* percent */
    }

    gsl_histogram_pdf_free(p);
    gsl_histogram_free(h);

    fl_set_object_label(GUI->dvhs->txt_roi,roi->name);
    fl_set_object_label_f(GUI->dvhs->txt_min,"%.12e Gy",stats->min);
    fl_set_object_label_f(GUI->dvhs->txt_max,"%.12e Gy",stats->max);
    fl_set_object_label_f(GUI->dvhs->txt_avg,"%.12e Gy",stats->mean);
    fl_set_object_label_f(GUI->dvhs->txt_std,"%.12e Gy",stats->stdev);
    fl_set_object_label_f(GUI->dvhs->txt_d50,"%.12e Gy",stats->median);
    fl_set_object_label_f(GUI->dvhs->txt_d90,"%.12e Gy",stats->pctl10);
    fl_set_object_label_f(GUI->dvhs->txt_d95,"%.12e Gy",stats->pctl05);

    fl_clear_xyplot(GUI->dvhs->plot_dif);
    fl_set_xyplot_xbounds(GUI->dvhs->plot_dif,stats->min,stats->max);
    fl_set_xyplot_fixed_xaxis(GUI->dvhs->plot_dif,"mmmmmmmm","");
    fl_set_xyplot_data(GUI->dvhs->plot_dif,difx,dify,NBINS,"",_("Dose [Gy]"),_("% volume"));
    free(difx);
    free(dify);

    fl_clear_xyplot(GUI->dvhs->plot_int);
    fl_set_xyplot_xbounds(GUI->dvhs->plot_int,stats->min,stats->max);
    fl_call_object_callback(GUI->dvhs->btn_log);
    fl_set_xyplot_fixed_xaxis(GUI->dvhs->plot_int,"mmmmmmmm","");
    fl_set_xyplot_data(GUI->dvhs->plot_int,intx,inty,NBINS,"",_("Dose [Gy]"),_("% volume"));
    free(intx);
    free(inty);

    free(stats);

    if(fl_get_button(GUI->dvhs->btn_dif))
        fl_call_object_callback(GUI->dvhs->btn_dif);
    else if(fl_get_button(GUI->dvhs->btn_int))
        fl_call_object_callback(GUI->dvhs->btn_int);

    fl_show_object(GUI->dvhs->pos_hist);
}

void cb_dvh_clear ( FL_OBJECT * obj  , long data  ) {
    (void)data;
	fl_set_object_label(GUI->dvhs->txt_roi,"");
	fl_set_object_label(GUI->dvhs->txt_min,"");
	fl_set_object_label(GUI->dvhs->txt_max,"");
	fl_set_object_label(GUI->dvhs->txt_avg,"");
	fl_set_object_label(GUI->dvhs->txt_std,"");
	fl_set_object_label(GUI->dvhs->txt_d50,"");
	fl_set_object_label(GUI->dvhs->txt_d90,"");
	fl_set_object_label(GUI->dvhs->txt_d95,"");
	fl_clear_xyplot(GUI->dvhs->plot_dif);
	fl_clear_xyplot(GUI->dvhs->plot_int);
	fl_hide_object(GUI->dvhs->plot_dif);
	fl_hide_object(GUI->dvhs->plot_int);
	fl_hide_object(GUI->dvhs->pos_hist);
}

void cb_dvh_log ( FL_OBJECT * obj  , long data  ) {
    (void)data;
	if(fl_get_button(obj)) {
		fl_set_xyplot_ybounds(GUI->dvhs->plot_int,0.0,0.0);
		fl_set_xyplot_yscale(GUI->dvhs->plot_int,FL_LOG,10.0);
	} else {
		fl_set_xyplot_yscale(GUI->dvhs->plot_int,FL_LINEAR,1.0);
		fl_set_xyplot_ybounds(GUI->dvhs->plot_int,0.0,100.0); /* percent */
	}
}

void cb_dvh_type ( FL_OBJECT * obj  , long data  ) {
	switch(data) {
		case 0: /* Differential */
			fl_hide_object(GUI->dvhs->plot_int);
			fl_show_object(GUI->dvhs->plot_dif);
			deactivate_obj(GUI->dvhs->btn_log);
			break;
		case 1: /* Integral */
			fl_hide_object(GUI->dvhs->plot_dif);
			fl_show_object(GUI->dvhs->plot_int);
			activate_obj(GUI->dvhs->btn_log,FL_YELLOW);
			break;
	}
}

void cb_dvh_export ( FL_OBJECT * obj  , long data  ) {
    (void)data;
	const unsigned int ix = fl_get_browser(GUI->dvhs->browser);

	if(fl_get_xyplot_data_size(GUI->dvhs->plot_dif) != NBINS ||
			fl_get_xyplot_data_size(GUI->dvhs->plot_int) != NBINS) {
		fl_show_alert2(0,_("No DVH present!\fPlease compute a DVH first."));
		return;
	}
	switch(fl_get_menu(obj)) {
		case 1: { /* DVH data */
			const char *file = fl_show_fselector(_("Export DVH data as..."),"","*.txt","");
            if(file == NULL) return;
            FILE *fp = fopen(file,"w");
            if(fp == NULL) {
                fl_show_alert2(0,_("Cannot open file for writing!\f%s"),file);
                return;
            }
            int n;
            float *dx,*dy;
            if(fl_get_button(GUI->dvhs->btn_dif)) {
                fl_get_xyplot_data_pointer(GUI->dvhs->plot_dif,0,&dx,&dy,&n);
                fprintf(fp,"# Differential DVH\n");
                float delta = 0.5 * (dx[1] - dx[0]);
                for(unsigned int i=0;i<NBINS;i++)
                    fprintf(fp,"%.8e %.8e %.8e\n",dx[i]-delta,dx[i]+delta,dy[i]);
            } else if(fl_get_button(GUI->dvhs->btn_int)) {
                fl_get_xyplot_data_pointer(GUI->dvhs->plot_int,0,&dx,&dy,&n);
                fprintf(fp,"# Integral DVH\n");
                for(unsigned int i=0;i<NBINS;i++)
                    fprintf(fp,"%.8e %.8e\n",dx[i],dy[i]);
            }
            fclose(fp);
            } break;
		case 2: { /* DVH plot */
			FLPS_CONTROL *flpsc = flps_init();
			flpsc->orientation = FLPS_LANDSCAPE;
			flpsc->eps = 1;
            fl_object_ps_dump(fl_get_button(GUI->dvhs->btn_dif) ?
                    GUI->dvhs->plot_dif : GUI->dvhs->plot_int,NULL);
			} break;
		case 3: { /* Dose values */
			if(ix == 0) {
                fl_show_alert2(0,_("No ROI selected!\fPlease select a target ROI for dose export."));
                return;
            }
            const char *file = fl_show_fselector(_("Export dose to target as..."),"","*.txt","");
            if(file == NULL) return;
            FILE *fp = fopen(file,"w");
            if(fp == NULL) {
                fl_show_alert2(0,_("Cannot open file for writing!\f%s"),file);
                return;
            }
            const g3d_roi *roi = DATA->rois[ix-1]->roi;
            fprintf(fp,"# Target ROI: %s\n",roi->name);
            for(index_t i=0;i<roi->size;i++)
                fprintf(fp,"%"PRIcoord" %"PRIcoord" %"PRIcoord" %.8e\n",
                        ROI_J(i,roi),ROI_I(i,roi),ROI_K(i,roi),
                        g3d_get_dvalue(ENV->data->dose->g3d,ROI_IX(i,roi,ENV->data->dose->g3d)));
            fclose(fp);
            } break;
        case 4: { /* Dose stats */
            const char *file = fl_show_fselector(_("Export dose stats as..."),"","*.txt","");
            if(file == NULL) return;
            FILE *fp = fopen(file,"w");
            if(fp == NULL) {
                fl_show_alert2(0,_("Cannot open file for writing!\f%s"),file);
                return;
            }
            fprintf(fp,"%14s: %s\n","ROI",fl_get_object_label(GUI->dvhs->txt_roi));
            fprintf(fp,"%14s: %s\n","Minimum dose",fl_get_object_label(GUI->dvhs->txt_min));
            fprintf(fp,"%14s: %s\n","Maximum dose",fl_get_object_label(GUI->dvhs->txt_max));
            fprintf(fp,"%14s: %s\n","Average dose",fl_get_object_label(GUI->dvhs->txt_avg));
            fprintf(fp,"%14s: %s\n","Std. deviation",fl_get_object_label(GUI->dvhs->txt_std));
            fprintf(fp,"%14s: %s\n","Dose to 50%",fl_get_object_label(GUI->dvhs->txt_d50));
            fprintf(fp,"%14s: %s\n","Dose to 90%",fl_get_object_label(GUI->dvhs->txt_d90));
            fprintf(fp,"%14s: %s\n","Dose to 95%",fl_get_object_label(GUI->dvhs->txt_d95));
            fclose(fp);
            } break;
	}
}

int pre_dvh_pos(FL_OBJECT *obj, int event, FL_Coord mx, FL_Coord my FL_UNUSED_ARG, int key, void *xev FL_UNUSED_ARG) {
	float delta, xitp, yitp;
	FL_Coord x,y,w,h;
	unsigned int i;
	float *dx,*dy;
	char buf[256];
	int n,sw,sh;

	if(event == FL_PUSH || event == FL_MOTION) {
		if(key == 1) {
			fl_get_object_geometry(obj,&x,&y,&w,&h);
			if(mx <= x) { mx = x+1; } else if(mx >= x+w) { mx = x+w-1; }
			if(fl_get_button(GUI->dvhs->btn_dif)) {
				fl_get_xyplot_data_pointer(GUI->dvhs->plot_dif,0,&dx,&dy,&n);
				i = (mx - x - 1)/3;
				delta = 0.5 * (dx[1] - dx[0]);
				snprintf(buf,sizeof(buf),"[%.4e,%.4e) Gy %.4g %%",dx[i]-delta,dx[i]+delta,dy[i]);
                fl_get_string_dimension(FL_FIXED_STYLE,FL_SMALL_SIZE,buf,strlen(buf),&sw,&sh);
                sw += ( 2 * FL_SMALL_SIZE ) / 3;
                sh += ( 2 * FL_SMALL_SIZE ) / 3;
				fl_set_oneliner_font(FL_FIXED_STYLE,FL_SMALL_SIZE);
				fl_show_oneliner(buf,x+obj->form->x-(sw-w)/2,y+obj->form->y-sh+1);
			} else if(fl_get_button(GUI->dvhs->btn_int)) {
				fl_get_xyplot_data_pointer(GUI->dvhs->plot_int,0,&dx,&dy,&n);
				i = (mx - x - 1)/3;
				delta = (dx[1]-dx[0])/3.0;
				xitp = fma((mx-x-1)%3,delta,dx[i]);
				if(i < NBINS - 1)
					yitp = fma(xitp-dx[i],(dy[i+1]-dy[i])/(dx[i+1]-dx[i]),dy[i]);
				else
					yitp = fma(xitp-dx[i],-dy[i]/(3.0*delta),dy[i]);;
				snprintf(buf,sizeof(buf),"%.8e Gy %.4g %%",xitp,yitp);
                fl_get_string_dimension(FL_FIXED_STYLE,FL_SMALL_SIZE,buf,strlen(buf),&sw,&sh);
                sw += ( 2 * FL_SMALL_SIZE ) / 3;
                sh += ( 2 * FL_SMALL_SIZE ) / 3;
				fl_set_oneliner_font(FL_FIXED_STYLE,FL_SMALL_SIZE);
				fl_show_oneliner(buf,x+obj->form->x-(sw-w)/2,y+obj->form->y-sh+1);
			}
		}
	} else if(event == FL_RELEASE) {
		fl_hide_oneliner();
		fl_set_oneliner_font(FL_NORMAL_STYLE,FL_NORMAL_SIZE);
	} else if(event == FL_ENTER) {
		fl_set_cursor(fl_get_real_object_window(obj),FL_CROSSHAIR_CURSOR);
	} else if(event == FL_LEAVE) {
		fl_reset_cursor(fl_get_real_object_window(obj));
		fl_hide_oneliner();
		fl_set_oneliner_font(FL_NORMAL_STYLE,FL_NORMAL_SIZE);
	}

	return FL_PREEMPT;
}
