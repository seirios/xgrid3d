#ifndef XG3D_color_gradient_h_
#define XG3D_color_gradient_h_

#include <forms.h>

typedef uint8_t* (_colorf)(unsigned int,float);

void _rgbtohsv(float,float,float,float*,float*,float*);
void _hsvtorgb(float,float,float,float*,float*,float*);
uint8_t zone_to_ztwoff(float);
uint8_t zone_to_ztoarg(float,unsigned int);
_colorf _grad_gray;
_colorf _grad_value;
_colorf _grad_sathsl;
_colorf _grad_light;
_colorf _grad_luma;
_colorf _grad_hotmetal;
_colorf _grad_fire;
_colorf _grad_nih;
_colorf _grad_rand;
_colorf _grad_turbo;
_colorf _grad_msh;
_colorf _grad_magma;
_colorf _grad_inferno;
_colorf _grad_plasma;
_colorf _grad_viridis;
_colorf _grad_parula;

#endif
