#include "operations.h"

#include "grid3d.h"

#include "util_fit3d.h"

g3d_aff_m* op_align_to_markers(const grid3d *g3d, const marker *firstmk, const unsigned int nmk, const enum g3d_axis axis, const enum align_mk_type type) {
    gsl_vector *u = NULL,*v,*c,*b;
    gsl_matrix *X,*R,*Ruv = NULL;
    gsl_matrix_view A;
    double theta = 0.0;
    const marker *mk;
    g3d_aff_m *Rx;

    /* set axis vector */
    v = gsl_vector_calloc(3);
    gsl_vector_set(v,axis,axis==AXIS_Y?-1:1);

    /* store markers in matrix */
    X = gsl_matrix_alloc(nmk,3);
    int n = 0;
    mk = firstmk;
    while(mk != NULL) {
        gsl_matrix_set(X,n,0, mk->vox[0]);
        gsl_matrix_set(X,n,1,-mk->vox[1]);
        gsl_matrix_set(X,n,2, mk->vox[2]);
        mk = mk->next;
        n++;
    }

    /* compute vector */
    switch(type) {
        case ALIGN_MK_LINE:  u = best_fit_line_vector(X);         break;
        case ALIGN_MK_PLANE: u = best_fit_plane_normal_vector(X); break;
    }
    gsl_matrix_free(X);

    /* get rotation that aligns u to v */
    Ruv = get_rotation_align(u,v);
    gsl_vector_free(u);
    gsl_vector_free(v);

    /* keep axes alignment */
    switch(axis) {
        case AXIS_X:
            theta = atan(gsl_matrix_get(Ruv,2,0)/gsl_matrix_get(Ruv,1,0));
            break;
        case AXIS_Y:
            theta = atan(-gsl_matrix_get(Ruv,2,0)/gsl_matrix_get(Ruv,0,0));
            break;
        case AXIS_Z:
            /* TODO: WTF? */
            theta = 0.0;
            break;
    }

    double M[9];
    g3d_isom_rot_matrix(theta,axis,M);
    A = gsl_matrix_view_array(M,3,3);
    R = gsl_matrix_alloc(3,3);
    gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,&A.matrix,Ruv,0.0,R);
    gsl_matrix_free(Ruv);

    /* compute translation to keep center: b = c - Rc */
    c = gsl_vector_alloc(3);
    gsl_vector_set(c,0,g3d->nx/2.0);
    gsl_vector_set(c,1,g3d->ny/2.0);
    gsl_vector_set(c,2,g3d->nz/2.0);
    b = get_translation_center(R,c);
    gsl_vector_free(c);

    /* build affine augmented matrix (without last row) */
    Rx = malloc(sizeof(g3d_aff_m));
    Rx->m[0] = gsl_matrix_get(R,0,0); Rx->m[1] = gsl_matrix_get(R,0,1); Rx->m[ 2] = gsl_matrix_get(R,0,2);
    Rx->m[4] = gsl_matrix_get(R,1,0); Rx->m[5] = gsl_matrix_get(R,1,1); Rx->m[ 6] = gsl_matrix_get(R,1,2);
    Rx->m[8] = gsl_matrix_get(R,2,0); Rx->m[9] = gsl_matrix_get(R,2,1); Rx->m[10] = gsl_matrix_get(R,2,2);
    Rx->m[3] = gsl_vector_get(b,0);   Rx->m[7] = gsl_vector_get(b,1);   Rx->m[11] = gsl_vector_get(b,2);
    for(int n=0;n<3;n++)
        fprintf(stderr,"%g %g %g %g\n",Rx->m[4*n+0],Rx->m[4*n+1],Rx->m[4*n+2],Rx->m[4*n+3]);
    fprintf(stderr,"0 0 0 1\n");
    gsl_vector_free(b);
    gsl_matrix_free(R);

    return Rx;
}
