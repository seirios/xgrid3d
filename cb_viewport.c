#include "cb_viewport.h"
#include "xgrid3d.h"

#include "util_coordinates.h"

xg3d_view_workspace* view_workspace_alloc(void) {
    xg3d_view_workspace *x = calloc(1,sizeof(xg3d_view_workspace));

    if(x == NULL) return NULL; /* malloc failed */

    x->sel = calloc(1,sizeof(xg3d_selection_workspace));
    if(x->sel == NULL) {
        view_workspace_clear(x,true);
        return NULL; /* malloc failed */
    }
    x->cursor_color = FL_YELLOW;
    x->plane = PLANE_XY;
    x->zoom_x = 1;
    x->zoom_y = 1;

    return x;
}

void view_workspace_clear(xg3d_view_workspace *x, const bool delete) {
    if(x != NULL) {
        for(unsigned int i=0;i<MAX_LAYERS;i++) {
            x->data[i] = NULL;
            flimage_free(x->image[i]);
            x->image[i] = NULL;
            g3d_free(x->slice[i]);
            x->slice[i] = NULL;
            g3d_free(x->quant_slice[i]);
            x->quant_slice[i] = NULL;
        }
        memset(x->k,0,MAX_LAYERS*sizeof(coord_t));
        selection_workspace_clear(x->sel,delete);
        memset(x->sp,0,3*sizeof(coord_t));
        memset(x->ep,0,3*sizeof(coord_t));
        x->oldpos = 0;
        x->xold = x->yold = 0;
        x->cursor_color = FL_YELLOW;
        x->plane = PLANE_XY;
        x->orient = ORIENT_0;
        x->zoom_x = 1;
        x->zoom_y = 1;
        x->spset = x->epset = false;
        x->moved = x->xhair = x->pan = x->slide = x->level = false;
        if(delete) free(x);
    }
}

xg3d_view_workspace* view_workspace_dup(const xg3d_view_workspace *x) {
    if(x == NULL) return NULL;
    xg3d_view_workspace *y = view_workspace_alloc();
    if(y == NULL) return NULL;

    for(unsigned int i=0;i<MAX_LAYERS;i++) {
        y->data[i] = x->data[i];
        y->image[i] = flimage_dup(x->image[i]);
        if(y->image[i] == NULL) {
            view_workspace_clear(y,true);
            return NULL;
        }
        y->slice[i] = g3d_dup(x->slice[i]);
        if(y->slice[i] == NULL) {
            view_workspace_clear(y,true);
            return NULL;
        }
        y->quant_slice[i] = g3d_dup(y->quant_slice[i]);
        if(y->quant_slice[i] == NULL) {
            view_workspace_clear(y,true);
            return NULL;
        }
    }
    memcpy(y->k,x->k,MAX_LAYERS*sizeof(coord_t));

    if(x->sel != NULL) {
        y->sel = selection_workspace_dup(x->sel);
        if(y->sel == NULL) {
            view_workspace_clear(y,true);
            return NULL;
        }
    }

    memcpy(y->sp,x->sp,3 * sizeof(coord_t));
    memcpy(y->ep,x->ep,3 * sizeof(coord_t));
    y->oldpos = x->oldpos;
    y->cursor_color = x->cursor_color;
    y->plane = x->plane;
    y->orient = x->orient;
    y->zoom_x = x->zoom_x;
    y->zoom_y = x->zoom_y;
    y->spset = x->spset;
    y->epset = x->epset;
    y->moved = x->moved;
    y->xhair = x->xhair;
    y->pan = x->pan;
    y->slide = x->slide;
    y->level = x->level;

    return y;
}



void win_to_grid(FD_viewport *view, FL_Coord x, FL_Coord y, coord_t *j_o, coord_t *i_o) {
    const enum xg3d_orientation orient = VIEW_ORIENT(view);
    const unsigned int zoom_x = (orient % 2 == 0) ? VIEW_ZOOM_X(view) : VIEW_ZOOM_Y(view);
    const unsigned int zoom_y = (orient % 2 == 0) ? VIEW_ZOOM_Y(view) : VIEW_ZOOM_X(view);
    const coord_t nx = VIEW_NX(view) - 1;
    const coord_t ny = VIEW_NY(view) - 1;
    coord_t j,i;

    #define COORD_INVERT(x,nn) ((nn) - (x))
    x /= zoom_x;
    y /= zoom_y;
    switch(orient) {
        case ORIENT_0:
        case ORIENT_F0:
			j = x;
            i = y;
			break;
		case ORIENT_90:
        case ORIENT_F270:
			j = COORD_INVERT(y,nx);
            i = x;
			break;	
		case ORIENT_180:
        case ORIENT_F180:
			j = COORD_INVERT(x,nx);
            i = COORD_INVERT(y,ny);
			break;	
		case ORIENT_270:
        case ORIENT_F90:
			j = y;
            i = COORD_INVERT(x,ny);
        break;	
        default:
            j = i = 0;
	}
    if(orient_flip_check(orient))
        i = COORD_INVERT(i,ny);
#undef COORD_INVERT

    if(j_o != NULL) *j_o = j;
    if(i_o != NULL) *i_o = i;
}

void grid_to_win(FD_viewport *view, FL_Coord *x_o, FL_Coord *y_o, const coord_t j, const coord_t i) {
    const enum xg3d_orientation orient = VIEW_ORIENT(view);
    const unsigned int zoom_x = (orient % 2 == 0) ? VIEW_ZOOM_X(view) : VIEW_ZOOM_Y(view);
    const unsigned int zoom_y = (orient % 2 == 0) ? VIEW_ZOOM_Y(view) : VIEW_ZOOM_X(view);
    const coord_t nx = VIEW_NX(view) - 1;
    const coord_t ny = VIEW_NY(view) - 1;
    FL_Coord x,y;

    #define COORD_INVERT(x,nn) ((nn) - (x))
	switch(orient) {
		case ORIENT_0:
			x = j;
            y = i;
			break;
		case ORIENT_90:
			x = i;
            y = COORD_INVERT(j,nx);
			break;	
		case ORIENT_180:
			x = COORD_INVERT(j,nx);
            y = COORD_INVERT(i,ny);
			break;	
		case ORIENT_270:
			x = COORD_INVERT(i,ny);
            y = j;
			break;	
		case ORIENT_F0:
			x = j;
            y = COORD_INVERT(i,ny);
			break;	
		case ORIENT_F90:
			x = i;
            y = j;
			break;	
		case ORIENT_F180:
			x = COORD_INVERT(j,nx);
            y = i;
			break;	
		case ORIENT_F270:
			x = COORD_INVERT(i,ny);
            y = COORD_INVERT(j,nx);
			break;	
        default:
            x = y = 0;
	}
    x *= zoom_x;
    y *= zoom_y;
#undef COORD_INVERT

    if(x_o != NULL) *x_o = x;
    if(y_o != NULL) *y_o = y;
}

void ccoord_to_winf(FD_viewport *view, double *x_o, double *y_o, const double cx, const double cy) {
    const enum xg3d_orientation orient = VIEW_ORIENT(view);
    const double zoom_x = (orient % 2 == 0) ? VIEW_ZOOM_X(view) : VIEW_ZOOM_Y(view);
    const double zoom_y = (orient % 2 == 0) ? VIEW_ZOOM_Y(view) : VIEW_ZOOM_X(view);
    const double nx = VIEW_NX(view) - 1;
    const double ny = VIEW_NY(view) - 1;
    double x,y;

    #define COORD_INVERT(x,nn) ((nn) - (x))
	switch(orient) {
		case ORIENT_0:
			x = cx;
            y = cy;
			break;
		case ORIENT_90:
			x = cy;
            y = COORD_INVERT(cx,nx);
			break;	
		case ORIENT_180:
			x = COORD_INVERT(cx,nx);
            y = COORD_INVERT(cy,ny);
			break;	
		case ORIENT_270:
			x = COORD_INVERT(cy,ny);
            y = cx;
			break;	
		case ORIENT_F0:
			x = cx;
            y = COORD_INVERT(cy,ny);
			break;	
		case ORIENT_F90:
			x = cy;
            y = cx;
			break;	
		case ORIENT_F180:
			x = COORD_INVERT(cx,nx);
            y = cy;
			break;	
		case ORIENT_F270:
			x = COORD_INVERT(cy,ny);
            y = COORD_INVERT(cx,nx);
			break;	
        default:
            x = y = 0;
	}
    x *= zoom_x;
    y *= zoom_y;
#undef COORD_INVERT

    if(x_o != NULL) *x_o = x;
    if(y_o != NULL) *y_o = y;
}

void ccoord_to_win(FD_viewport *view, FL_Coord *x_o, FL_Coord *y_o, const double cx, const double cy) {
    const enum xg3d_orientation orient = VIEW_ORIENT(view);
    const double zoom_x = (orient % 2 == 0) ? VIEW_ZOOM_X(view) : VIEW_ZOOM_Y(view);
    const double zoom_y = (orient % 2 == 0) ? VIEW_ZOOM_Y(view) : VIEW_ZOOM_X(view);
    const double nx = VIEW_NX(view) - 1;
    const double ny = VIEW_NY(view) - 1;
    double x,y;

    #define COORD_INVERT(x,nn) ((nn) - (x))
	switch(orient) {
		case ORIENT_0:
			x = cx;
            y = cy;
			break;
		case ORIENT_90:
			x = cy;
            y = COORD_INVERT(cx,nx);
			break;	
		case ORIENT_180:
			x = COORD_INVERT(cx,nx);
            y = COORD_INVERT(cy,ny);
			break;	
		case ORIENT_270:
			x = COORD_INVERT(cy,ny);
            y = cx;
			break;	
		case ORIENT_F0:
			x = cx;
            y = COORD_INVERT(cy,ny);
			break;	
		case ORIENT_F90:
			x = cy;
            y = cx;
			break;	
		case ORIENT_F180:
			x = COORD_INVERT(cx,nx);
            y = cy;
			break;	
		case ORIENT_F270:
			x = COORD_INVERT(cy,ny);
            y = COORD_INVERT(cx,nx);
			break;	
        default:
            x = y = 0;
	}
    x *= zoom_x;
    y *= zoom_y;
#undef COORD_INVERT

    if(x_o != NULL) *x_o = (unsigned int)floor(x);
    if(y_o != NULL) *y_o = (unsigned int)floor(y);
}
