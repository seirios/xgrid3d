/* Header file generated by fdesign on Sat Sep 28 21:14:16 2024 */

#ifndef FD_ct_h_
#define FD_ct_h_

#include <forms.h>

#if defined __cplusplus
extern "C"
{
#endif

/* Callbacks, globals and object handlers */

FL_CALLBACK cb_ct_clamp_toggle;
FL_CALLBACK cb_ct_start;
FL_CALLBACK cb_ct_load_conebeam;


/* Forms and Objects */

typedef struct {
    FL_FORM   * ct;
    void      * vdata;
    char      * cdata;
    long        ldata;
    FL_OBJECT * ch_algo;
    FL_OBJECT * spin_vox;
    FL_OBJECT * spin_nx;
    FL_OBJECT * spin_ny;
    FL_OBJECT * spin_nz;
    FL_OBJECT * spin_cx;
    FL_OBJECT * spin_cy;
    FL_OBJECT * spin_cz;
    FL_OBJECT * inp_pix;
    FL_OBJECT * inp_R;
    FL_OBJECT * inp_D;
    FL_OBJECT * inp_u0;
    FL_OBJECT * inp_v0;
    FL_OBJECT * inp_twist;
    FL_OBJECT * inp_slant;
    FL_OBJECT * inp_tilt;
    FL_OBJECT * spin_iter;
    FL_OBJECT * btn_clamp_min;
    FL_OBJECT * btn_clamp_max;
    FL_OBJECT * inp_clamp_min;
    FL_OBJECT * inp_clamp_max;
    FL_OBJECT * btn_pause;
    FL_OBJECT * txt_det;
    FL_OBJECT * txt_R;
    FL_OBJECT * txt_D;
    FL_OBJECT * txt_u0;
    FL_OBJECT * txt_v0;
    FL_OBJECT * txt_eta;
    FL_OBJECT * txt_theta;
    FL_OBJECT * txt_phi;
} FD_ct;

FD_ct * create_form_ct( void );

#if defined __cplusplus
}
#endif

#endif /* FD_ct_h_ */
