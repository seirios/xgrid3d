#include <babl/babl.h>
#include <gsl/gsl_spline.h>

#include "coloring.h"
#include "color_gradient.h"
#include "color_predefined.h"

#include "selection.h"

%include "util.hm"
#include "util_linsimp.h"

%include "coloring_type.hm"

#define RED 0
#define GREEN 1
#define BLUE 2
#define SMOOTH_SPLINE gsl_interp_steffen

/* choose LUT from textual description */
int choose_lut_from_str(xg3d_lut *lut, const char *str, bool rev, bool inv, float hue) {
    if(NULL == str || '\0' == *str) return 1; // empty input

    if(strstr(str,"grad:") == str) { // gradient
        str += %strlen(`grad:`);

        enum col_grad_type type;
    %map [sep=`else`] col_grad_type %{
        if(strstr(str,$S{name}) == str
                && '\0' == *(str + %strlen($b{name}))) {
            type = COL_GRAD_$U{name};
        }
    %}
        else return 3; // bad grad

        _update_lut_gradient(lut,rev,inv,hue,type);
    } else if(strstr(str,"predef:") == str) { // predefined
        str += %strlen(`predef:`);

        enum col_predef_type type;
    %map [sep=`else`] col_predef_type %{
        if(strstr(str,$S{title}) == str
                && '\0' == *(str + %strlen($b{title}))) {
            type = COL_PREDEF_$U{name};
        }
    %}
        else return 4; // bad predef

        _update_lut_predef(lut,rev,inv,type);
    } else return 2; // bad prefix

    return 0;
}

/* ==================
   COLORING FUNCTIONS
   ================== */

/* fill the LUT from ramps */
void _update_lut_ramp(FD_col_ramp *col_ramp, xg3d_lut *lut, const int rev, const int inv) {
    const gsl_interp_type *type;
    gsl_interp_accel *acc[3];
    gsl_spline *spline[3];
    float *sx,*sy,dx;
    float *fx,*fy;
    double *x,*y;
    int n;

    %map {red,green,blue}(color) %{
        FL_OBJECT *plot_${color} = col_ramp->plot_${color};
        bool ${color}_smooth = fl_get_button(col_ramp->btn_${color}_smooth);
        bool ${color}_active = fl_get_button(col_ramp->btn_${color});
    %}

    %snippet ramp_spline = (color) %{
        acc[$U{color}] = gsl_interp_accel_alloc();
        type = ${color}_smooth ? SMOOTH_SPLINE : gsl_interp_linear;
        fl_get_xyplot_data_pointer(plot_${color},0,&fx,&fy,&n);
        spline[$U{color}] = gsl_spline_alloc(type,n);
        x = malloc(n*sizeof(double));
        y = malloc(n*sizeof(double));
        for(int i=0;i<n;i++) { x[i] = (double)fx[i]; y[i] = (double)fy[i]; }
        gsl_spline_init(spline[$U{color}],x,y,n);
        free(x); free(y);
        if(${color}_smooth) {
            n = plot_${color}->w;
            sx = malloc(n*sizeof(float));
            sy = malloc(n*sizeof(float));
            dx = 1.0 / (n - 1.0);
            for(int i=0;i<n;i++) {
                sx[i] = (float)i * dx;
                sy[i] = (float)FL_clamp(gsl_spline_eval(spline[$U{color}],sx[i],acc[$U{color}]),0.0,1.0);
            }
            fl_add_xyplot_overlay(plot_${color},1,sx,sy,n,FL_$U{color});
            free(sx); free(sy);
        }
    %}
    %recall ramp_spline (`red`)(`green`)(`blue`)

    uint8_t *rgb_buf = malloc(3 * lut->ncol * sizeof(uint8_t));
    float *frgb_buf = malloc(3 * lut->ncol * sizeof(float));

    if(lut->ncol == 1) {
    %map {red,green,blue}(color) %{
        frgb_buf[$U{color}] = ${color}_active ?
            FL_clamp(gsl_spline_eval(spline[$U{color}],0.0,acc[$U{color}]),0.0,1.0) : 0;
    %}
    } else {
        for(unsigned int l = 0; l < lut->ncol; l++) {
            float cx = (float)l / (lut->ncol-1);
        %map {red,green,blue}(color) %{
            frgb_buf[$U{color} + 3 * l] = ${color}_active ?
                FL_clamp(gsl_spline_eval(spline[$U{color}],cx,acc[$U{color}]),0.0,1.0) : 0;
        %}
        }
    }

    %map {red,green,blue}(color) %{
        gsl_spline_free(spline[$U{color}]);
        gsl_interp_accel_free(acc[$U{color}]);
    %}

    const Babl *frgb = babl_format("R'G'B' float");
    const Babl *rgb = babl_format("R'G'B' u8");
    const Babl *fish = babl_fish(frgb,rgb);
    babl_process(fish,frgb_buf,rgb_buf,lut->ncol);
    for(unsigned int l=0;l<lut->ncol;l++) {
        unsigned int ix = rev ? lut->ncol-1-l : l;
        uint8_t r = rgb_buf[RED + 3 * l];
        uint8_t g = rgb_buf[GREEN + 3 * l];
        uint8_t b = rgb_buf[BLUE + 3 * l];
        if(inv) { r = ~r; g = ~g; b = ~b; }
        lut->colors[ix] = FL_PACK(r,g,b);
    }
    free(frgb_buf);
    free(rgb_buf);
}
#undef RED
#undef GREEN
#undef BLUE

/* fill the LUT with a gradient */
void _update_lut_gradient(xg3d_lut *lut, const int rev, const int inv, const float h, const enum col_grad_type type) {
    _colorf *colorf = NULL;
    switch(type) {
        %map col_grad_type %{
        case COL_GRAD_$U{name}: colorf = ${function}; break;
        %}
        default: return;
    }

    float hue = h / 360.0;
    uint8_t *rgb = colorf(lut->ncol,hue);

    for(unsigned int i = 0; i < lut->ncol; i++) {
        unsigned int ix = rev ? lut->ncol - 1 - i : i;
        uint8_t r = rgb[3 * i];
        uint8_t g = rgb[3 * i + 1];
        uint8_t b = rgb[3 * i + 2];
        if(inv) { r = ~r; g = ~g; b = ~b; }
        lut->colors[ix] = FL_PACK(r,g,b);
    }
    free(rgb);
}

/* fill the LUT with predefined colors */
void _update_lut_predef(xg3d_lut *lut, const bool rev, const bool inv, const enum col_predef_type type) {
    unsigned int l;
    const FL_PACKED *predef = NULL;
    switch(type) {
        %map col_predef_type %{
        case COL_PREDEF_$U{name}:
            {
                static const unsigned int ncol[] = { ${ncolors}, 0 }; // 0 is sentinel
                size_t i=0;
                while((l = ncol[i++]) > 0) { // stop at sentinel
                    if(l >= lut->ncol) break; // break if big enough palette found
                }
                if(ncol[i] == 0) i--; // use biggest possible palette
                predef = ${name}[i];
            }
            break;
        %}
        default: break; // unhandled placeholder values
    }

    if(predef != NULL) {
        if(lut->ncol <= l) {
            for(unsigned int i=0;i<lut->ncol;i++) {
                unsigned int ix = rev ? lut->ncol-1-i : i;
                uint8_t r = FL_GETR(predef[i%l]);
                uint8_t g = FL_GETG(predef[i%l]);
                uint8_t b = FL_GETB(predef[i%l]);
                if(inv) { r = ~r; g = ~g; b = ~b; }
                lut->colors[ix] = FL_PACK(r,g,b);
            }
        } else {
            /* here predef has max colors available */
            uint8_t r,g,b;
            for(unsigned int i=0;i<lut->ncol;i++) {
                unsigned int ix = rev ? lut->ncol-1-i : i;
                float x = (float)i/(float)(lut->ncol-1);
                unsigned int n = (unsigned int)(x * (l-1));
                if(n == l-1) {
                    r = FL_GETR(predef[n]);
                    g = FL_GETG(predef[n]);
                    b = FL_GETB(predef[n]);
                } else {
                    /* linear interpolation */
                    x = (x - (float)n/(l-1)) * (float)(l-1);
                    r = FL_nint(FL_GETR(predef[n]) * (1.0 - x) + FL_GETR(predef[n+1]) * x);
                    g = FL_nint(FL_GETG(predef[n]) * (1.0 - x) + FL_GETG(predef[n+1]) * x);
                    b = FL_nint(FL_GETB(predef[n]) * (1.0 - x) + FL_GETB(predef[n+1]) * x);
                }
                if(inv) { r = ~r; g = ~g; b = ~b; }
                lut->colors[ix] = FL_PACK(r,g,b);
            }
        }
    }
}

/* update LUT from file */
void _update_lut_file(xg3d_lut *lut, const uint8_t *lutfile, const bool rev, const bool inv) {
    if(lut->ncol == 1) {
        uint8_t r = lutfile[0];
        uint8_t g = lutfile[256];
        uint8_t b = lutfile[512];
        if(inv) { r = ~r; g = ~g; b = ~b; }
        lut->colors[0] = FL_PACK(r,g,b);
    } else 
        for(unsigned int i=0;i<lut->ncol;i++) {
            unsigned int ix = rev ? lut->ncol-1-i : i;
            unsigned int n = FL_nint(255 * ix/(float)(lut->ncol-1));
            uint8_t r = lutfile[n];
            uint8_t g = lutfile[n + 256];
            uint8_t b = lutfile[n + 512];
            if(inv) { r = ~r; g = ~g; b = ~b; }
            lut->colors[i] = FL_PACK(r,g,b);
        }
}
