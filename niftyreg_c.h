#ifndef XG3D_niftyreg_c_h_
#define XG3D_niftyreg_c_h_

#include "nifti1_io.h"

#define myDTYPE double

#define SetMacroProto(name,type) void reg_aladin_Set##name(regAladin*,type)
#define GetMacroProto(name,type) type reg_aladin_Get##name(regAladin*)
#define BooleanMacroProto(name,type) \
    void reg_aladin_##name##On(regAladin*);\
    void reg_aladin_##name##Off(regAladin*);
#define SetClampMacroProto(name,type) void reg_aladin_Set##name(regAladin*,type)

#ifdef __cplusplus
extern "C" {
#endif

    typedef union {
        void* aladin;
        void* aladin_sym;
    } regAladin;

    regAladin* reg_aladin_new();
    regAladin* reg_aladin_sym_new();
    void reg_aladin_delete(regAladin*);
    char* reg_aladin_GetExecutableName(regAladin*);

    void reg_aladin_SetInputReference(regAladin*,nifti_image*);
    nifti_image* reg_aladin_GetInputReference(regAladin*);

    void reg_aladin_SetInputFloating(regAladin*,nifti_image*);
    nifti_image* reg_aladin_GetInputFloating(regAladin*);

    void reg_aladin_SetInputMask(regAladin*,nifti_image*);
    nifti_image* reg_aladin_GetInputMask(regAladin*);

    void reg_aladin_SetInputFloatingMask(regAladin*,nifti_image*);
    nifti_image* reg_aladin_GetInputFloatingMask(regAladin*);

    void reg_aladin_SetInputTransform(regAladin*,mat44*);

    mat44* reg_aladin_GetTransformationMatrix(regAladin*);
    nifti_image* reg_aladin_GetFinalWarpedImage(regAladin*);

    SetMacroProto(Verbose,int);
    GetMacroProto(Verbose,int);

    SetMacroProto(MaxIterations,unsigned int);
    GetMacroProto(MaxIterations,unsigned int);

    SetMacroProto(NumberOfLevels,unsigned int);
    GetMacroProto(NumberOfLevels,unsigned int);

    SetMacroProto(LevelsToPerform,unsigned int);
    GetMacroProto(LevelsToPerform,unsigned int);

    SetMacroProto(BlockPercentage,float);
    GetMacroProto(BlockPercentage,float);

    SetMacroProto(InlierLts,float);
    GetMacroProto(InlierLts,float);

    SetMacroProto(ReferenceSigma,float);
    GetMacroProto(ReferenceSigma,float);

    SetMacroProto(FloatingSigma,float);
    GetMacroProto(FloatingSigma,float);

    SetMacroProto(PerformRigid,int);
    GetMacroProto(PerformRigid,int);
    BooleanMacroProto(PerformRigid,int)

    SetMacroProto(PerformAffine,int);
    GetMacroProto(PerformAffine,int);
    BooleanMacroProto(PerformAffine,int)

    GetMacroProto(AlignCentre,int);
    SetMacroProto(AlignCentre,int);
    BooleanMacroProto(AlignCentre,int)

    SetClampMacroProto(Interpolation,int);
    GetMacroProto(Interpolation,int);

    void reg_aladin_SetInterpolationToNearestNeighbor(regAladin*);
    void reg_aladin_SetInterpolationToTrilinear(regAladin*);
    void reg_aladin_SetInterpolationToCubic(regAladin*);

    int reg_aladin_Check(regAladin*);
    int reg_aladin_Print(regAladin*);
    int reg_aladin_Run(regAladin*);
    void reg_aladin_DebugPrintLevelInfo(regAladin*,int);

    int resampleSourceImage(nifti_image*,nifti_image*,nifti_image*,nifti_image*,int*,int,float);

    void affine_positionField(mat44*,nifti_image*,nifti_image*);

#ifdef __cplusplus
}
#endif

#endif
