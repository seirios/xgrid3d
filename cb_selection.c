#include <float.h>

#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "grid3d_fftw.h"
#ifndef XG3D_selection_cb_h
#define XG3D_selection_cb_h
#include <forms.h>
#include "xgrid3d.h"
#include "fd_selection.h"
#include "selection.h"
#define SEL_GXMODE GXinvert
#define SEL_COLOR FL_WHITE
/* Selection settings value mask bits */
#define SELECTION_SCOPE         (1L<<0)
#define SELECTION_VISIBLE       (1L<<1)
#define SELECTION_LABELS        (1L<<2)
#define SELECTION_SELPATT       (1L<<3)
#define SELECTION_APPEARANCE    (SELECTION_VISIBLE | \
                                 SELECTION_LABELS | \
                                 SELECTION_SELPATT)
#define SELECTION_SET_MODE      (1L<<4)
#define SELECTION_CLAMP         (1L<<5)
#define SELECTION_REPLACE       (1L<<6)
#define SELECTION_BRUSH_SHAPE   (1L<<7)
#define SELECTION_BRUSH_SIZE    (1L<<8)
#define SELECTION_SHAPE         (1L<<9)
#define SELECTION_NRAND         (1L<<10)
#define SELECTION_ISO_FILLED    (1L<<11)
#define SELECTION_ISO_GREATER   (1L<<12)
#define SELECTION_SELMODE       (SELECTION_SET_MODE | \
                                 SELECTION_CLAMP | \
                                 SELECTION_REPLACE | \
                                 SELECTION_BRUSH_SHAPE | \
                                 SELECTION_BRUSH_SIZE | \
                                 SELECTION_SHAPE | \
                                 SELECTION_NRAND | \
                                 SELECTION_ISO_FILLED | \
                                 SELECTION_ISO_GREATER)
#define SELECTION_REP           (1L<<13)
#define SELECTION_STEP          (1L<<14)
#define SELECTION_PROPAGATE     (1L<<15)
#define SELECTION_OPERATIONS    (SELECTION_REP | \
                                 SELECTION_STEP | \
                                 SELECTION_PROPAGATE)
#define SELECTION_MARKER_LIST   (1L<<16)
#define SELECTION_MARKERS       (SELECTION_MARKER_LIST)


typedef void ( _selpattf ) ( const FL_Coord wx  , const FL_Coord wy  , const unsigned int zoom_x  , const unsigned int zoom_y  , const FL_COLOR color  ) ;
enum xg3d_selection_pattern {
    SEL_PATT_CROSS,
    SEL_PATT_DOT,
    SEL_PATT_SQUARE
};
enum xg3d_selection_mode {
    SEL_MODE_NONE,
    SEL_MODE_MARKER,
    SEL_MODE_BRUSH,
    SEL_MODE_SHAPE,
    SEL_MODE_LEVEL,
    SEL_MODE_COMPONENT,
    SEL_MODE_FILL,
    SEL_MODE_RANDOM,
    SEL_MODE_CONTOUR,
    SEL_MODE_ISOCOMPONENT
};
typedef struct {
    enum xg3d_scope scope;                 /* selection scope */
    int visible;                           /* selection visible flag */
    int labels;                            /* labels visible flag */
    enum xg3d_selection_pattern patt;      /* selection pattern */
    enum xg3d_selection_mode mode;         /* selection mode */
    int clamp;                             /* histogram clamp flag */
    int replace;                           /* replace selection flag */
    enum xg3d_selection_brush brush_shape; /* brush shape */
    unsigned int brush_size;               /* brush size */
    enum xg3d_selection_shape shape;       /* shape */
    unsigned int nrand;                    /* number of random points */
    int iso_filled;                        /* fill isocontour flag */
    int iso_greater;                       /* isocontour > or < flag */
    unsigned int rep;                      /* selection move repetition */
    unsigned int step;                     /* selection move step */
    int propagate;                         /* propagate selection flag */
    unsigned int nmk;                      /* number of markers in list */
    marker *marker_list;                   /* marker list */
} xg3d_selection_settings;
void setup_selection_settings(FD_selection*,const unsigned long,const xg3d_selection_settings*);
xg3d_selection_settings* store_selection_settings(const FD_selection*,const unsigned long);
_selpattf sel_patt_cross;
_selpattf sel_patt_dot;
_selpattf sel_patt_square;
inline static _selpattf* SELECTION_PATTERN_FUNC(FD_selection *selection) {
    return THREEWAY_IF(
            fl_get_button(selection->btn_cross),sel_patt_cross,
            fl_get_button(selection->btn_dot),  sel_patt_dot,
                                                sel_patt_square);
}
inline static enum xg3d_selection_pattern SELECTION_PATTERN(FD_selection *selection) {
    return THREEWAY_IF(
            fl_get_button(selection->btn_cross),SEL_PATT_CROSS,
            fl_get_button(selection->btn_dot),  SEL_PATT_DOT,
                                                SEL_PATT_SQUARE);
}
typedef struct {
    grid3d *g3d;
    index_t nvox;
    double value;
} xg3d_selection_workspace;
void selection_workspace_clear(xg3d_selection_workspace*,const bool);
xg3d_selection_workspace* selection_workspace_dup(const xg3d_selection_workspace*);
#define SELECTION_MODE(fd_selection) (((FD_selection*)(fd_selection))->ldata)
#define SELECTION_NMK(fd_selection) (((FD_selection*)(fd_selection))->br_markers->u_ldata)
#define SELECTION_MKLIST(fd_selection) (((FD_selection*)(fd_selection))->br_markers->u_vdata)
#define SELECTION_WORKSPACE(fd_selection) ((xg3d_selection_workspace*)(((FD_selection*)(fd_selection))->vdata))
#define SELECTION_G3D(fd_selection) (SELECTION_WORKSPACE(fd_selection)->g3d)
#define SELECTION_NVOX(fd_selection) (SELECTION_WORKSPACE(fd_selection)->nvox)
#define SELECTION_VALUE(fd_selection) (SELECTION_WORKSPACE(fd_selection)->value)
#define SELECTION_IS_VISIBLE(fd_selection) (fl_get_button(((FD_selection*)(fd_selection))->btn_visible))
#define SELECTION_SET_VISIBLE(fd_selection,x) (fl_set_button(((FD_selection*)(fd_selection))->btn_visible,x))
#define SELECTION_GET_SCOPE(fd_selection) (fl_get_button(((FD_selection*)(fd_selection))->btn_scope))
void replace_selslice(FD_viewport*,FD_selection*);
void sel_update_info(FD_viewport*,FD_selection*);
void sel_sanity_check(FD_viewport*,FD_selection*);
void sel_set_cursor(FD_viewport*,FD_selection*);
void sel_clear_markers(FD_selection*);
void cb_marker_dblclick(FL_OBJECT*,long);
void sel_add_marker(xg3d_env*,const coord_t,const coord_t,const coord_t,const char*);
void sel_del_marker(xg3d_env*,const coord_t,const coord_t,const coord_t);
void sel_blink_marker(FD_viewport*,FD_selection*,const coord_t,const coord_t,const bool);
FL_HANDLE pre_sel_brush;
void sel_brush(xg3d_env*,const enum xg3d_selection_brush,const unsigned int,const coord_t,const coord_t,const coord_t,const enum xg3d_sel_op_mode);
void sel_shape(xg3d_env*,const enum xg3d_sel_op_mode);
void sel_random(xg3d_env*,const enum xg3d_sel_op_mode);
void sel_level(xg3d_env*,const coord_t,const coord_t,const enum xg3d_scope,const enum xg3d_sel_op_mode);
void sel_level_comp(xg3d_env*,const coord_t,const coord_t,const enum xg3d_scope,const enum xg3d_sel_op_mode);
void sel_fill(xg3d_env*,const coord_t,const coord_t,const enum xg3d_scope,const enum xg3d_sel_op_mode);
void sel_contour(xg3d_env*,const coord_t,const coord_t,const enum xg3d_scope,const enum xg3d_sel_op_mode,const bool);
#endif
#include "cb_viewport.h"
#include "cb_mainview.h"
#include "cb_overview.h"
#include "cb_quantization.h"

#include "selection.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include "util.h"
#include "util_filter.h"
#include "util_coordinates.h"
#include "util_units.h"

#define FORMAT "%.11g"
#define MK_FORMAT "%5g %5g %5g %s"

void selection_workspace_clear(xg3d_selection_workspace *x, const bool delete) {
    if(x != NULL) {
        g3d_free(x->g3d); x->g3d = NULL;
        x->nvox = 0;
        x->value = 0.0;
        if(delete) free(x);
    }
}

xg3d_selection_workspace* selection_workspace_dup(const xg3d_selection_workspace *x) {
    xg3d_selection_workspace *y;

    if(x == NULL) return NULL;
    
    y = calloc(1,sizeof(xg3d_selection_workspace));
    if(y == NULL) return NULL;

    if(x->g3d != NULL) {
        y->g3d = g3d_dup(x->g3d);
        if(y->g3d == NULL) {
            selection_workspace_clear(y,true);
            return NULL;
        }
    }
    y->nvox = x->nvox;
    y->value = x->value;

    return y;
}

#undef ENV
void sel_add_marker(xg3d_env *ENV, const coord_t j, const coord_t i, const coord_t k, const char *label) {
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const FL_COLOR cur_color = VIEW_CURSOR_COLOR(GUI->viewport);
    const xg3d_header *hdr = DATA->hdr;
    marker **firstmk = (marker**)&SELECTION_MKLIST(GUI->selection);
    unsigned int nmk = SELECTION_NMK(GUI->selection);
    double ux,uy,uz;
    coord_t vox[3];

    /* map in-slice coordinates to global coordinates */
    COORD_GLOBAL(j,i,k,vox,plane);

    if(marker_add(firstmk,vox,label,&nmk) == NULL) return; /* marker non-unique */
    SELECTION_NMK(GUI->selection) = nmk; /* update number of markers */

    /* draw recently added marker */
    if(VIEW_K(GUI->viewport) == vox[PLANE_TO_K(plane)])
        draw_meas_point(GUI->viewport,vox[PLANE_TO_J(plane)],vox[PLANE_TO_I(plane)],cur_color);
    if(fl_get_button(GUI->overview->draw_sel)) {
        FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
        if(fl_get_button(GUI->overview->planeXY) && VIEW_K(ov_viewports[PLANE_XY]) == vox[PLANE_TO_K(PLANE_XY)])
            draw_ov_marker(ov_viewports[PLANE_XY],vox[PLANE_TO_J(PLANE_XY)],vox[PLANE_TO_I(PLANE_XY)],cur_color);
        if(fl_get_button(GUI->overview->planeXZ) && VIEW_K(ov_viewports[PLANE_XZ]) == vox[PLANE_TO_K(PLANE_XZ)])
            draw_ov_marker(ov_viewports[PLANE_XZ],vox[PLANE_TO_J(PLANE_XZ)],vox[PLANE_TO_I(PLANE_XZ)],cur_color);
        if(fl_get_button(GUI->overview->planeZY) && VIEW_K(ov_viewports[PLANE_ZY]) == vox[PLANE_TO_K(PLANE_ZY)])
            draw_ov_marker(ov_viewports[PLANE_ZY],vox[PLANE_TO_J(PLANE_ZY)],vox[PLANE_TO_I(PLANE_ZY)],cur_color);
    }

    /* add browser line in currently selected units */
    switch(fl_get_choice(GUI->selection->ch_marker_units)) {
        default:
        case 1: /* vox */
            ux = uy = uz = 1.0;
            break;
        case 2: /* cm */
            ux = hdr->vx;
            uy = hdr->vy;
            uz = hdr->vz;
            break;
        case 3: /* mm */
            ux = hdr->vx * CM_TO_MM;
            uy = hdr->vy * CM_TO_MM;
            uz = hdr->vz * CM_TO_MM;
            break;
    }
    fl_addto_browser_f(GUI->selection->br_markers,MK_FORMAT,vox[0]*ux,vox[1]*uy,vox[2]*uz,label != NULL ? label : "");
}
#define ENV _default_env

#undef ENV
void sel_del_marker(xg3d_env *ENV, const coord_t j, const coord_t i, const coord_t k) {
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const unsigned int nmk = SELECTION_NMK(GUI->selection);
    marker **firstmk = (marker**)&SELECTION_MKLIST(GUI->selection);
    unsigned int pos;
    coord_t vox[3];
    marker *mk;

    if(nmk == 0) return;

    /* map in-slice coordinates to global coordinates */
    COORD_GLOBAL(j,i,k,vox,plane);

    mk = marker_del(firstmk,vox,&pos);
    if(pos == (unsigned)(-1))
        pos = SELECTION_NMK(GUI->selection);
    else
        pos++;

    vox[0] = mk->vox[0];
    vox[1] = mk->vox[1];
    vox[2] = mk->vox[2];

    if(mk->label != NULL && fl_get_button(GUI->selection->btn_labels)) {
        refresh_canvas(ENV);
        if(fl_get_button(GUI->overview->draw_sel))
            refresh_ov_canvases(GUI->overview);
    } else {
        if(VIEW_K(GUI->viewport) == k)
            restore_image_voxel(GUI->viewport,GUI->selection,vox[PLANE_TO_J(plane)],vox[PLANE_TO_I(plane)]);
        if(fl_get_button(GUI->overview->draw_sel)) {
            FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
            if(fl_get_button(GUI->overview->planeXY) && VIEW_K(ov_viewports[PLANE_XY]) == vox[PLANE_TO_K(PLANE_XY)])
                ov_restore_image_voxel(ov_viewports[PLANE_XY],GUI->selection,vox[PLANE_TO_J(PLANE_XY)],vox[PLANE_TO_I(PLANE_XY)]);
            if(fl_get_button(GUI->overview->planeXZ) && VIEW_K(ov_viewports[PLANE_XZ]) == vox[PLANE_TO_K(PLANE_XZ)])
                ov_restore_image_voxel(ov_viewports[PLANE_XZ],GUI->selection,vox[PLANE_TO_J(PLANE_XZ)],vox[PLANE_TO_I(PLANE_XZ)]);
            if(fl_get_button(GUI->overview->planeZY) && VIEW_K(ov_viewports[PLANE_ZY]) == vox[PLANE_TO_K(PLANE_ZY)])
                ov_restore_image_voxel(ov_viewports[PLANE_ZY],GUI->selection,vox[PLANE_TO_J(PLANE_ZY)],vox[PLANE_TO_I(PLANE_ZY)]);
        }
    }

    fl_delete_browser_line(GUI->selection->br_markers,pos);
    SELECTION_NMK(GUI->selection)--;
    marker_free(mk);
}
#define ENV _default_env

void sel_clear_markers(FD_selection *selection) {
    marker *mk = SELECTION_MKLIST(selection);

    marker_free(mk);
    SELECTION_NMK(selection) = 0;
    SELECTION_MKLIST(selection) = NULL;
    fl_clear_browser(selection->br_markers);
}

void cb_sel_marker_units ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const xg3d_header *hdr = DATA->hdr;
    double ux,uy,uz;
    marker *mk;

    if(SELECTION_NMK(GUI->selection) == 0) return;

    switch(fl_get_choice(obj)) {
        default:
        case 1: /* vox */
            ux = uy = uz = 1.0;
            break;
        case 2: /* cm */
            ux = hdr->vx;
            uy = hdr->vy;
            uz = hdr->vz;
            break;
        case 3: /* mm */
            ux = hdr->vx * CM_TO_MM;
            uy = hdr->vy * CM_TO_MM;
            uz = hdr->vz * CM_TO_MM;
            break;
    }

    fl_clear_browser(GUI->selection->br_markers);

    mk = SELECTION_MKLIST(GUI->selection);
    while(mk != NULL) {
        fl_add_browser_line_f(GUI->selection->br_markers,MK_FORMAT,
                mk->vox[0]*ux,mk->vox[1]*uy,mk->vox[2]*uz,mk->label != NULL ? mk->label : "");
        mk = mk->next;
    }
}

#define MSEC 100
void sel_blink_marker(FD_viewport *view, FD_selection *selection, const coord_t j, const coord_t i, const bool stay_on) {
    const FL_COLOR cur_color = VIEW_CURSOR_COLOR(view);

    if(!stay_on) {
        draw_meas_point(view,j,i,cur_color);
        fl_XFlush(); fl_msleep(MSEC);
    } else
        fl_XFlush();
    restore_image_voxel(view,selection,j,i);
    fl_XFlush(); fl_msleep(MSEC);
    draw_meas_point(view,j,i,cur_color);
    fl_XFlush(); fl_msleep(MSEC);
    restore_image_voxel(view,selection,j,i);
    fl_XFlush(); fl_msleep(MSEC);
    draw_meas_point(view,j,i,cur_color);
    fl_XFlush(); fl_msleep(MSEC);
    restore_image_voxel(view,selection,j,i);
    fl_XFlush();
    if(stay_on) {
        fl_msleep(MSEC);
        draw_meas_point(view,j,i,cur_color);
        fl_XFlush();
    }
}
#undef MSLEEP

void cb_sel_marker_op ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *slider;
    const unsigned int ix = fl_get_browser(GUI->selection->br_markers);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const unsigned int nmk = SELECTION_NMK(GUI->selection);
    marker **firstmk = (marker**)&SELECTION_MKLIST(GUI->selection);
    const char *label;
    marker *mk,*tmp;
    coord_t j,i,k;
    int a,b;

    if(nmk == 0) return;

    switch(data) {
        case 0: /* move up */
            if(ix == 0 || ix == 1) return;
            mk = *firstmk; for(unsigned int m=0;m<ix-1;m++) mk = mk->next; /* goto marker at position ix-1 */
            tmp = mk->prev; mk->prev = tmp->prev; if(ix > 2) mk->prev->next = mk; else *firstmk = mk;
            tmp->next = mk->next; if(tmp->next != NULL) tmp->next->prev = tmp; else (*firstmk)->prev = tmp;
            mk->next = tmp; tmp->prev = mk;
            fl_insert_browser_line(GUI->selection->br_markers,ix+1,fl_get_browser_line(GUI->selection->br_markers,ix-1));
            fl_delete_browser_line(GUI->selection->br_markers,ix-1);
            break;
        case 1: /* move down */
            if(ix == 0 || ix == nmk) return;
            mk = *firstmk; for(unsigned int m=0;m<ix-1;m++) mk = mk->next; /* goto marker at position ix-1 */
            tmp = mk->next; mk->next = tmp->next; if(mk->next != NULL) mk->next->prev = mk; else (*firstmk)->prev = mk;
            tmp->prev = mk->prev; if(ix > 1) tmp->prev->next = tmp; else *firstmk = tmp;
            mk->prev = tmp; tmp->next = mk;
            fl_insert_browser_line(GUI->selection->br_markers,ix,fl_get_browser_line(GUI->selection->br_markers,ix+1));
            fl_delete_browser_line(GUI->selection->br_markers,ix+2);
            break;
        case 2: /* set label */
            if(ix == 0) return;
            mk = *firstmk; for(unsigned int m=0;m<ix-1;m++) mk = mk->next;

            fl_set_goodies_font(UNIFONT,FL_NORMAL_SIZE);
            label = fl_show_input(_("Marker label:"),mk->label);
            fl_set_goodies_font(FL_NORMAL_STYLE,FL_NORMAL_SIZE);
            if(label != NULL) {
                if(strlen(label) == 0) {
                    free(mk->label);
                    mk->label = NULL;
                } else {
                    mk->label = strdup(label);
                    fl_set_choices_shortcut("1Tt","2Mm","3Bb");
                    a = fl_show_choices(_("Choose vertical alignment:"),3,_("Top"),_("Middle"),_("Bottom"),1);
                    fl_set_choices_shortcut("1Ll","2Mm","3Rr");
                    b = fl_show_choices(_("Choose horizontal alignment:"),3,_("Left"),_("Middle"),_("Right"),2);
                    mk->align = (3*a*a-11*a+10)/2 | (a == 2 ? 6*b*b-22*b+20 : 6*b*b-26*b+28);
                }
                fl_call_object_callback(GUI->selection->ch_marker_units);
                if(fl_get_button(GUI->selection->btn_labels)) {
                    refresh_canvas(ENV);
                    if(fl_get_button(GUI->overview->draw_sel))
                        refresh_ov_canvases(GUI->overview);
                }
            }
            break;
        case 3: /* show */
            if(ix == 0) return;
            mk = *firstmk; for(unsigned int m=0;m<ix-1;m++) mk = mk->next;

            j = mk->vox[PLANE_TO_J(plane)];
            i = mk->vox[PLANE_TO_I(plane)];
            k = mk->vox[PLANE_TO_K(plane)];

            if(fl_get_button(GUI->overview->draw_sel)) {
                FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
                if(VIEW_K(ov_viewports[PLANE_XY]) != k) {
                    slider = GUI->overview->xy_slider;
                    fl_set_slider_value(slider,mk->vox[2]);
                    fl_call_object_callback(slider);
                }
                if(VIEW_K(ov_viewports[PLANE_XZ]) != i) {
                    slider = GUI->overview->xz_slider;
                    fl_set_slider_value(slider,mk->vox[1]);
                    fl_call_object_callback(slider);
                }
                if(VIEW_K(ov_viewports[PLANE_ZY]) != j) {
                    slider = GUI->overview->zy_slider;
                    fl_set_slider_value(slider,mk->vox[0]);
                    fl_call_object_callback(slider);
                }
            }

            if(VIEW_K(GUI->viewport) != k) {
                slider = GUI->mainview->slider;
                fl_set_slider_value(slider,k);
                fl_call_object_callback(slider);
            }

            sel_blink_marker(GUI->viewport,GUI->selection,j,i,true);
            break;
        case 4: /* delete */
            if(ix == 0) return;
            mk = *firstmk; for(unsigned int m=0;m<ix-1;m++) mk = mk->next;
            if(nmk == 1) /* only */
                *firstmk = NULL;
            else if(mk == *firstmk) { /* first */
                *firstmk = mk->next;
                (*firstmk)->prev = mk->prev;
            } else if(mk->next == NULL) { /* last */
                (*firstmk)->prev = mk->prev;
                mk->prev->next = NULL;
            } else { /* intermediate */
                mk->prev->next = mk->next;
                mk->next->prev = mk->prev;
            }
            /* isolate before free */
            mk->next = mk->prev = NULL;
            if(VIEW_K(GUI->viewport) == mk->vox[PLANE_TO_K(plane)])
                restore_image_voxel(GUI->viewport,GUI->selection,mk->vox[PLANE_TO_J(plane)],mk->vox[PLANE_TO_I(plane)]);
            SELECTION_NMK(GUI->selection)--;
            marker_free(mk);
            fl_delete_browser_line(GUI->selection->br_markers,ix);
            break;
        case 5: /* clear */
            sel_clear_markers(GUI->selection);
            refresh_canvas(ENV);
            if(fl_get_button(GUI->overview->draw_sel))
                refresh_ov_canvases(GUI->overview);
            break;
    }
}

void cb_sel_marker_io ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const char *file = NULL;
    unsigned int nmk;
    marker *mk;

    switch(fl_get_menu(obj)) {
        case 1: /* Load */
            file = fl_show_fselector(_("Load markers from..."),"","*.txt","");
            if(file == NULL) return;
            mk = marker_load(file,&nmk);
            SELECTION_NMK(GUI->selection) = nmk;
            SELECTION_MKLIST(GUI->selection) = mk;
            fl_call_object_callback(GUI->selection->ch_marker_units);
            refresh_canvas(ENV);
            if(fl_get_button(GUI->overview->draw_sel))
                refresh_ov_canvases(GUI->overview);
            break;
        case 2: /* Save */
            if(SELECTION_NMK(GUI->selection) == 0) return;
            file = fl_show_fselector(_("Save markers to..."),"","*.txt","");
            if(file == NULL) return;
            marker_save(file,SELECTION_MKLIST(GUI->selection));
            break;
    }
}

void sel_update_info(FD_viewport *view, FD_selection *selection) {
    const int flag_calib = fl_get_button(selection->btn_uval);
    const enum g3d_slice_plane plane = VIEW_PLANE(view);
    const index_t nvoxels = SELECTION_NVOX(selection);
    const double value = SELECTION_VALUE(selection);
    const index_t selnvoxels = VIEW_SELNVOX(view);
    const double selvalue = VIEW_SELVALUE(view);
    const xg3d_data *data = VIEW_DATA(view);
    const double vx = data->hdr->vx;
    const double vy = data->hdr->vy;
    const double vz = data->hdr->vz;
    const double area[3] = {vx*vy,vx*vz,vz*vy};
    const double uvol = fl_get_button(selection->btn_uvol) ? CM3_TO_MM3 : 1.0,
                 uarea = fl_get_button(selection->btn_uvol) ? CM2_TO_MM2 : 1.0;
    double val;

    /* SLICE */
    fl_set_object_label_f(selection->tx_nvoxel_s,"%u",selnvoxels);
    fl_set_object_label_f(selection->tx_vol_s,FORMAT,selnvoxels*area[plane]*uarea);
    if(selnvoxels >= 1) {
        val = flag_calib ? CALIBSUM(data->hdr,selvalue,selnvoxels) : selvalue;
        fl_set_object_label_f(selection->tx_value_s,FORMAT,val);
        fl_set_object_label_f(selection->tx_mean_s,FORMAT,val/selnvoxels);
    } else {
        fl_set_object_label_f(selection->tx_min_s,"");
        fl_set_object_label_f(selection->tx_max_s,"");
        fl_set_object_label_f(selection->tx_value_s,"");
        fl_set_object_label_f(selection->tx_mean_s,"");
        fl_set_object_label_f(selection->tx_spread_s,"");
    }
    /* mark as dirty */
    fl_set_object_lcolor(selection->tx_spread_s,FL_RED);
    fl_set_object_lcolor(selection->tx_min_s,FL_RED);
    fl_set_object_lcolor(selection->tx_max_s,FL_RED);

    /* ENV */
    fl_set_object_label_f(selection->tx_nvoxel_g,"%u",nvoxels);
    fl_set_object_label_f(selection->tx_vol_g,FORMAT,nvoxels*vx*vy*vz*uvol);
    if(nvoxels >= 1) {
        val = flag_calib ? CALIBSUM(data->hdr,value,nvoxels) : value;
        fl_set_object_label_f(selection->tx_value_g,FORMAT,val);
        fl_set_object_label_f(selection->tx_mean_g,FORMAT,val/nvoxels);
    } else {
        fl_set_object_label_f(selection->tx_min_g,"");
        fl_set_object_label_f(selection->tx_max_g,"");
        fl_set_object_label_f(selection->tx_value_g,"");
        fl_set_object_label_f(selection->tx_mean_g,"");
        fl_set_object_label_f(selection->tx_spread_g,"");
    }
    /* mark as dirty */
    fl_set_object_lcolor(selection->tx_spread_g,FL_RED);
    fl_set_object_lcolor(selection->tx_min_g,FL_RED);
    fl_set_object_lcolor(selection->tx_max_g,FL_RED);
}

void sel_sanity_check(FD_viewport *view, FD_selection *selection) {
    if(VIEW_SELNVOX(view)        == 0)        VIEW_SELVALUE(view) = 0.0;
    if(SELECTION_NVOX(selection) == 0) SELECTION_VALUE(selection) = 0;
}

void sel_patt_dot ( const FL_Coord wx  , const FL_Coord wy  , const unsigned int zoom_x  , const unsigned int zoom_y  , const FL_COLOR color  ) {
    fl_point(wx + zoom_x / 2,wy + zoom_y / 2,color);
}

void sel_patt_cross ( const FL_Coord wx  , const FL_Coord wy  , const unsigned int zoom_x  , const unsigned int zoom_y  , const FL_COLOR color  ) {
    if(zoom_x == 1 && zoom_y == 1)
        fl_point(wx,wy,color);
    else if(zoom_x == 1 || zoom_y == 1)
        fl_rectf(wx,wy,zoom_x,zoom_y,color);
    else {
        fl_line(wx,wy,wx + zoom_x - 1,wy + zoom_y - 1,color);
        fl_line(wx,wy + zoom_y - 1,wx + zoom_x - 1,wy,color);
    }
}

void sel_patt_square ( const FL_Coord wx  , const FL_Coord wy  , const unsigned int zoom_x  , const unsigned int zoom_y  , const FL_COLOR color  ) {
    fl_rectf(wx,wy,zoom_x,zoom_y,color);
}

/* NOTE: may not work properly depending on video drivers (HW/SW cursor-related issue) */
void sel_set_cursor(FD_viewport *view, FD_selection *selection) {
    const unsigned int size = fl_get_spinner_value(selection->spin_brush);
    const enum xg3d_orientation orient = VIEW_ORIENT(view);
    const FL_COLOR cur_color = VIEW_CURSOR_COLOR(view);
    const unsigned int zoom_y = (orient % 2 == 0) ? VIEW_ZOOM_Y(view) : VIEW_ZOOM_X(view);
    const unsigned int h = size * zoom_y,
                       w = h/8 + (h%8 > 0);
    const int hotx = (h / 2) - (h % 2 == 0);
    _selcurf *selcurf = sel_cursor_dot;
    unsigned char *xbm;
    int hoty = hotx;

    if(SELECTION_MODE(selection) != SEL_MODE_BRUSH) {
        fl_set_cursor_color(cursor,cur_color,FL_BLACK);
        fl_set_cursor(FL_ObjWin(VIEW_CANVAS(view)),cursor);
        return;
    }

    xbm = calloc(w*h,sizeof(unsigned char));
    if(size > 1)
        switch(fl_get_choice(selection->ch_brush)) {
            case SEL_BRUSH_SQUARE:   selcurf = sel_cursor_square;        break;
            case SEL_BRUSH_CIRCLE:   selcurf = sel_cursor_circle;        break;
            case SEL_BRUSH_DIAMOND:  selcurf = sel_cursor_diamond;       break;
            case SEL_BRUSH_HLINE:    selcurf = sel_cursor_line_horiz;    break;
            case SEL_BRUSH_VLINE:    selcurf = sel_cursor_line_vert;     break;
            case SEL_BRUSH_DIAG:     selcurf = sel_cursor_line_diag; hoty = h/2; break;
            case SEL_BRUSH_ANTIDIAG: selcurf = sel_cursor_line_antidiag; break;
        }
    selcurf(xbm,size,w,h,hotx,hoty);

    if(!selcursor)
        selcursor = fl_create_bitmap_cursor((const char*)xbm,(const char*)xbm,h,h,hotx,hoty);
    else
        fl_replace_bitmap_cursor(selcursor,(const char*)xbm,(const char*)xbm,h,h,hotx,hoty);
    free(xbm);

    fl_set_cursor_color(selcursor,cur_color,FL_BLACK);
    fl_set_cursor(FL_ObjWin(VIEW_CANVAS(view)),selcursor);
}

int pre_sel_brush(FL_OBJECT *obj FL_UNUSED_ARG, int event, FL_Coord mx FL_UNUSED_ARG, FL_Coord my FL_UNUSED_ARG, int key FL_UNUSED_ARG, void *xev FL_UNUSED_ARG) {
    if(event == FL_PUSH)
        fl_setpup_default_fontstyle(UNIFONT);
    else if(event == FL_LEAVE)
        fl_setpup_default_fontstyle(FL_NORMAL_STYLE);
    return FL_IGNORE;
}

#undef ENV
static void _selop_live(enum xg3d_sel_op_mode op, void *data, const coord_t j, const coord_t i, const coord_t k) {
    const xg3d_env *ENV = (xg3d_env*)data;
    const xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const int visible = SELECTION_IS_VISIBLE(GUI->selection);
    xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);
    coord_t jik[3];
    int incr = 0;
    double dat;
    index_t p;

    /* to global coordinates */
    COORD_GLOBAL(j,i,k,jik,plane);

    /* absolute location */
    p = COORD_DIM(jik,DATA->g3d->nx,DATA->g3d->nxy);
    dat = g3d_get_dvalue(DATA->g3d,p);

    if(fl_get_button(GUI->selection->btn_clamp) && (dat < qwk->min || dat > qwk->max)) return;

    switch(op) {
        case SEL_OP_UNSET:
            if(!g3d_selbit_check(swk->g3d,p)) return;
            g3d_selbit_clear(swk->g3d,p);
            incr = -1;
            break;
        case SEL_OP_SET:
            if(g3d_selbit_check(swk->g3d,p)) return;
            g3d_selbit_set(swk->g3d,p);
            incr = 1;
            break;
        case SEL_OP_TMP_UNSET:
            g3d_seltmp_clear(swk->g3d,p); return;
            break;
        case SEL_OP_TMP_SET:
            g3d_seltmp_set(swk->g3d,p); return;
            break;
    }

    swk->nvox += incr;
    swk->value += dat * incr;

    if(k == VIEW_K(GUI->viewport)) {
        g3d_selbit_copy(VIEW_SELSLICE(GUI->viewport),j+i*VIEW_NX(GUI->viewport),swk->g3d,p);
        VIEW_SELNVOX(GUI->viewport) += incr;
        VIEW_SELVALUE(GUI->viewport) += dat * incr;
        if(visible) draw_sel_voxel(GUI->viewport,GUI->selection,j,i);
    }

    if(visible && fl_get_button(GUI->overview->draw_sel)) {
        FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);
        if(fl_get_button(GUI->overview->planeXY) && VIEW_K(ov_viewports[PLANE_XY]) == jik[2])
            draw_ov_sel_voxel(ov_viewports[PLANE_XY],GUI->selection,jik[0],jik[1]);
        if(fl_get_button(GUI->overview->planeXZ) && VIEW_K(ov_viewports[PLANE_XZ]) == jik[1])
            draw_ov_sel_voxel(ov_viewports[PLANE_XZ],GUI->selection,jik[0],jik[2]);
        if(fl_get_button(GUI->overview->planeZY) && VIEW_K(ov_viewports[PLANE_ZY]) == jik[0])
            draw_ov_sel_voxel(ov_viewports[PLANE_ZY],GUI->selection,jik[2],jik[1]);
    }
}
#define ENV _default_env


#undef ENV
void sel_brush(xg3d_env *ENV, const enum xg3d_selection_brush brush, const unsigned int size, const coord_t j, const coord_t i, const coord_t k, const enum xg3d_sel_op_mode op) {
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    _selbrushf *selbrushf = sel_brush_dot;

    if(size > 1)
        switch(brush) {
            default: break;
            case SEL_BRUSH_SQUARE:   selbrushf = sel_brush_square;        break;
            case SEL_BRUSH_CIRCLE:   selbrushf = sel_brush_circle;        break;
            case SEL_BRUSH_DIAMOND:  selbrushf = sel_brush_diamond;       break;
            case SEL_BRUSH_HLINE:    selbrushf = sel_brush_line_horiz;    break;
            case SEL_BRUSH_VLINE:    selbrushf = sel_brush_line_vert;     break;
            case SEL_BRUSH_DIAG:     selbrushf = sel_brush_line_diag;     break;
            case SEL_BRUSH_ANTIDIAG: selbrushf = sel_brush_line_antidiag; break;
        }

    selbrushf(j,i,k,op,plane,false,size,VIEW_NX(GUI->viewport),VIEW_NY(GUI->viewport),_selop_live,ENV);
}
#define ENV _default_env

#undef ENV
void sel_shape(xg3d_env *ENV, const enum xg3d_sel_op_mode op) {
    xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);
    const xg3d_view_workspace *vwk = VIEW_WORKSPACE(GUI->viewport);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const xg3d_data *data = VIEW_DATA(GUI->viewport);

    // choose selection function
    _selshapef *selshapef = NULL;
    const enum xg3d_selection_shape shape = fl_get_choice(GUI->selection->ch_shape);
    switch(shape) {
        case SEL_SHAPE_BOX:       selshapef = sel_shape_box;       break;
        case SEL_SHAPE_SPHERE:    selshapef = sel_shape_sphere;    break;
        case SEL_SHAPE_ELLIPSOID: selshapef = sel_shape_ellipsoid; break;
        case SEL_SHAPE_CYLINDER:  selshapef = sel_shape_cylinder;  break;
        case SEL_SHAPE_LINE:      selshapef = sel_shape_line;      break;
        case SEL_SHAPE_PLANE:     selshapef = sel_shape_plane;     break;
        default: fatal("passed invalid shape value %d",shape);
    }

    // point coordinates
        double bj = data->hdr->dim[0] - 1; // bound
    coord_t jm = vwk->sp[0]; // start point
    coord_t jM = vwk->ep[0] - 1; // end point
       double bi = data->hdr->dim[1] - 1; // bound
    coord_t im = vwk->sp[1]; // start point
    coord_t iM = vwk->ep[1] - 1; // end point
       double bk = data->hdr->dim[2] - 1; // bound
    coord_t km = vwk->sp[2]; // start point
    coord_t kM = vwk->ep[2] - 1; // end point
   
#if DEBUG
    fprintf(stderr,"%d %d %d\n",jm,im,km);
    fprintf(stderr,"%d %d %d\n",jM,iM,kM);
#endif

    switch(shape) {
        case SEL_SHAPE_BOX: /* FALLTHROUGH */
        case SEL_SHAPE_SPHERE: /* FALLTHROUGH */
        case SEL_SHAPE_ELLIPSOID: /* FALLTHROUGH */
        case SEL_SHAPE_CYLINDER:
            /* uses bounding-box */
            if(kM < km)     { coord_t _ = (km); (km) = (kM); (kM) = _; }
            if(jM < jm)     { coord_t _ = (jm); (jm) = (jM); (jM) = _; }
            if(iM < im)     { coord_t _ = (im); (im) = (iM); (iM) = _; }
            break;
        case SEL_SHAPE_LINE: /* global point to point */ break;
        case SEL_SHAPE_PLANE:
            switch(plane) { // set normal vector
                case PLANE_XY:
                    bj = bi = 0; bk = 1;
                    break;
                case PLANE_XZ:
                    bj = bk = 0; bi = 1;
                    break;
                case PLANE_ZY:
                    bi = bk = 0; bj = 1;
                    break;
            }
            break;
    }

    index_t nvx = 0;
    double val = 0.0;
    selshapef(swk->g3d,DATA->g3d,jm,im,km,jM,iM,kM,bj,bi,bk,op,&nvx,&val);
    if(op < 0) { swk->nvox -= nvx; swk->value -= val; } // remove from selection
          else { swk->nvox += nvx; swk->value += val; } // add to selection

    replace_selslice(GUI->viewport,GUI->selection);
    if(SELECTION_IS_VISIBLE(GUI->selection)) { 
        refresh_canvas(ENV);
        if(fl_get_button(GUI->overview->draw_sel))
            refresh_ov_canvases(GUI->overview);
    }
}
#define ENV _default_env

#undef ENV
void sel_random(xg3d_env *ENV, const enum xg3d_sel_op_mode op) {
    xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);
    const enum xg3d_scope scope = SELECTION_GET_SCOPE(GUI->selection);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    index_t nvx = fl_get_spinner_value(GUI->selection->spin_nrand);
    const grid3d *dat;
    grid3d *sel;
    double val;

    fl_show_oneliner(_("Selecting randomly..."),fl_scrw/2,fl_scrh/2);

    switch(scope) {
        default:
        case SCOPE_SLICE:
            dat = VIEW_SLICE(GUI->viewport);
            sel = VIEW_SELSLICE(GUI->viewport);
            break;
        case SCOPE_GLOBAL:
            dat = DATA->g3d;
            sel = swk->g3d;
            break;
    }

    sel_mode_random(sel,dat,scope,plane,op,NULL,&nvx,&val);
    if(op < 0) { swk->nvox -= nvx; swk->value -= val; }
          else { swk->nvox += nvx; swk->value += val; }

    if(scope == SCOPE_SLICE) {
        g3d_ret ret = g3d_replace_slice(swk->g3d,sel,VIEW_K(GUI->viewport),plane);
        if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
    }

    replace_selslice(GUI->viewport,GUI->selection);
    if(SELECTION_IS_VISIBLE(GUI->selection)) { 
        refresh_canvas(ENV);
        if(fl_get_button(GUI->overview->draw_sel))
            refresh_ov_canvases(GUI->overview);
    }

    fl_hide_oneliner();
}
#define ENV _default_env

#undef ENV
void sel_fill(xg3d_env *ENV, const coord_t j, const coord_t i, const enum xg3d_scope scope, const enum xg3d_sel_op_mode op) {
    xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    coord_t jik[3];
    uint8_t sval;
    index_t nvx;
    double val;

    /* to global coordinates */
    COORD_GLOBAL(j,i,VIEW_K(GUI->viewport),jik,plane);

    sval = g3d_selbit_check(VIEW_SELSLICE(GUI->viewport),j+i*VIEW_NX(GUI->viewport));
    if((op == SEL_OP_SET && sval) || (op == SEL_OP_UNSET && !sval)) return;

    fl_show_oneliner(_("Filling selection..."),fl_scrw/2,fl_scrh/2);

    nvx = COORD_DIM(jik,swk->g3d->nx,swk->g3d->nxy);
    sel_mode_fill(swk->g3d,DATA->g3d,scope,plane,sval,NULL,&nvx,&val);

    if(nvx >= 1) {
        if(op < 0) { swk->nvox -= nvx; swk->value -= val; }
              else { swk->nvox += nvx; swk->value += val; }

        replace_selslice(GUI->viewport,GUI->selection);
        if(SELECTION_IS_VISIBLE(GUI->selection)) { 
            refresh_canvas(ENV);
            if(fl_get_button(GUI->overview->draw_sel))
                refresh_ov_canvases(GUI->overview);
        }
    }

    fl_hide_oneliner();
}
#define ENV _default_env

#undef ENV
void sel_level(xg3d_env *ENV, const coord_t j, const coord_t i, const enum xg3d_scope scope, const enum xg3d_sel_op_mode op) {
    xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);
    const xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const grid3d *data;
    grid3d *selection;
    uint8_t sval;
    index_t nvx;
    double val;

    sval = g3d_selbit_check(VIEW_SELSLICE(GUI->viewport),j+i*VIEW_NX(GUI->viewport));
    if((op == SEL_OP_SET && sval) || (op == SEL_OP_UNSET && !sval)) return;

    fl_show_oneliner(_("Selecting level..."),fl_scrw/2,fl_scrh/2);

    switch(scope) {
        default:
        case SCOPE_SLICE:
            data = VIEW_SLICE(GUI->viewport);
            selection = VIEW_SELSLICE(GUI->viewport);
            nvx = j + i * selection->nx;
            break;
        case SCOPE_GLOBAL:
            data = DATA->g3d;
            selection = swk->g3d;
            nvx = COORD_SPLIT_DIM(j,i,VIEW_K(GUI->viewport),selection->nx,selection->nxy,plane);
            break;
    }

    sel_mode_level(selection,data,-1,plane,sval,qwk->info,&nvx,&val);
    if(op < 0) { swk->nvox -= nvx; swk->value -= val; }
          else { swk->nvox += nvx; swk->value += val; }

    if(scope == SCOPE_SLICE) {
        g3d_ret ret = g3d_replace_slice(swk->g3d,selection,VIEW_K(GUI->viewport),plane);
        if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
    }

    replace_selslice(GUI->viewport,GUI->selection);
    if(SELECTION_IS_VISIBLE(GUI->selection)) { 
        refresh_canvas(ENV);
        if(fl_get_button(GUI->overview->draw_sel))
            refresh_ov_canvases(GUI->overview);
    }
    
    fl_hide_oneliner();
}
#define ENV _default_env

#undef ENV
void sel_level_comp(xg3d_env *ENV,
                    const coord_t j, // window x position
                    const coord_t i, // window y position
                    const enum xg3d_scope scope,
                    const enum xg3d_sel_op_mode op) {
    xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);
    const xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    uint8_t sval;
    index_t nvx;
    double val;

    /* slice has same plane as window, so we check index directly */
    sval = g3d_selbit_check(VIEW_SELSLICE(GUI->viewport),(size_t)j + i * VIEW_NX(GUI->viewport));
    if((op == SEL_OP_SET && sval) || (op == SEL_OP_UNSET && !sval)) return;

    fl_show_oneliner(_("Selecting level component..."),fl_scrw/2,fl_scrh/2);

    /* global voxel index */
    nvx = COORD_SPLIT_DIM(j,i,VIEW_K(GUI->viewport),swk->g3d->nx,swk->g3d->nxy,plane);
    sel_mode_level_comp(swk->g3d,DATA->g3d,scope,plane,sval,qwk->info,&nvx,&val);

    if(nvx >= 1) {
        if(op < 0) { swk->nvox -= nvx; swk->value -= val; }
              else { swk->nvox += nvx; swk->value += val; }

        replace_selslice(GUI->viewport,GUI->selection);
        if(SELECTION_IS_VISIBLE(GUI->selection)) { 
            refresh_canvas(ENV);
            if(fl_get_button(GUI->overview->draw_sel))
                refresh_ov_canvases(GUI->overview);
        }
    }

    fl_hide_oneliner();
}
#define ENV _default_env

#undef ENV
void sel_contour(xg3d_env *ENV, const coord_t j, const coord_t i, const enum xg3d_scope scope, enum xg3d_sel_op_mode op, const bool component) {
    xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);
    const bool greater = fl_get_button(GUI->selection->btn_greater);
    const bool filled = fl_get_button(GUI->selection->btn_filled);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const grid3d *data;
    grid3d *selection;
    double zero,val;
    int8_t flags;
    index_t nvx;

    fl_get_slider_bounds(GUI->mainview->slider,&zero,&val);

    fl_show_oneliner(component ?
            _("Selecting isosurface component...") : _("Selecting isosurface..."),fl_scrw/2,fl_scrh/2);

    switch(scope) {
        default:
        case SCOPE_SLICE:
            data = VIEW_SLICE(GUI->viewport);
            selection = VIEW_SELSLICE(GUI->viewport);
            nvx = j + i * selection->nx;
            break;
        case SCOPE_GLOBAL:
            data = DATA->g3d;
            selection = swk->g3d;
            nvx = COORD_SPLIT_DIM(j,i,VIEW_K(GUI->viewport),selection->nx,selection->nxy,plane);
            break;
    }

    flags = op * (component + 2 * greater + 4 * filled);
    sel_mode_contour(selection,data,scope,plane,flags,NULL,&nvx,&val);
    if(op < 0) { swk->nvox -= nvx; swk->value -= val; }
          else { swk->nvox += nvx; swk->value += val; }

    if(scope == SCOPE_SLICE) {
        g3d_ret ret = g3d_replace_slice(swk->g3d,selection,VIEW_K(GUI->viewport),plane);
        if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
    }

    replace_selslice(GUI->viewport,GUI->selection);
    if(SELECTION_IS_VISIBLE(GUI->selection)) { 
        refresh_canvas(ENV);
        if(fl_get_button(GUI->overview->draw_sel))
            refresh_ov_canvases(GUI->overview);
    }

    fl_hide_oneliner();
}
#define ENV _default_env

void replace_selslice(FD_viewport *view, FD_selection *selection) {
    const enum g3d_slice_plane plane = VIEW_PLANE(view);
    const grid3d *slice = VIEW_SLICE(view);
    const coord_t k = VIEW_K(view);
    grid3d **selslice = (grid3d**)&VIEW_SELSLICE(view);
    index_t *selnvoxels = (index_t*)&VIEW_SELNVOX(view);
    double *selvalue = (double*)&VIEW_SELVALUE(view);

    g3d_free(*selslice);
    *selslice = g3d_slice(SELECTION_G3D(selection),k,plane);
    g3d_u8_setup_nhood(*selslice,SCOPE_SLICE);
    sel_oper_count(*selslice,slice,0,0,0,0,selnvoxels,selvalue);
}

void cb_sel_units ( FL_OBJECT * obj  , long data  ) {
    switch(data) {
        case 0: fl_set_object_label(obj,fl_get_button(obj) ? "mm" : "cm");   break; /* volume */
        case 1: fl_set_object_label(obj,fl_get_button(obj) ? "CAL" : "DAT"); break; /* activity */
    }
    sel_update_info(GUI->viewport,GUI->selection);
}

static void _setup_selmode(FD_selection *selection, const enum xg3d_selection_mode selmode) {
    switch(selmode) {
        case SEL_MODE_NONE: /* None */
            deactivate_obj(selection->ch_brush);
            deactivate_obj(selection->spin_brush);
            deactivate_obj(selection->ch_shape);
            deactivate_obj(selection->spin_nrand);
            deactivate_obj(selection->btn_filled);
            deactivate_obj(selection->btn_greater);
            deactivate_obj(selection->btn_lesser);
            break;
        case SEL_MODE_BRUSH: /* Brush */
            activate_obj(selection->ch_brush,FL_BLACK);
            activate_obj(selection->spin_brush,FL_COL1);
            deactivate_obj(selection->ch_shape);
            deactivate_obj(selection->spin_nrand);
            deactivate_obj(selection->btn_filled);
            deactivate_obj(selection->btn_greater);
            deactivate_obj(selection->btn_lesser);
            break;
        case SEL_MODE_SHAPE: /* Shape */
            activate_obj(selection->ch_shape,FL_BLACK);
            deactivate_obj(selection->ch_brush);
            deactivate_obj(selection->spin_brush);
            deactivate_obj(selection->spin_nrand);
            deactivate_obj(selection->btn_filled);
            deactivate_obj(selection->btn_greater);
            deactivate_obj(selection->btn_lesser);
            break;
        case SEL_MODE_LEVEL: /* Level */
            deactivate_obj(selection->ch_shape);
            deactivate_obj(selection->ch_brush);
            deactivate_obj(selection->spin_brush);
            deactivate_obj(selection->spin_nrand);
            deactivate_obj(selection->btn_filled);
            deactivate_obj(selection->btn_greater);
            deactivate_obj(selection->btn_lesser);
            break;
        case SEL_MODE_COMPONENT: /* Level component */
            deactivate_obj(selection->ch_shape);
            deactivate_obj(selection->ch_brush);
            deactivate_obj(selection->spin_brush);
            deactivate_obj(selection->spin_nrand);
            deactivate_obj(selection->btn_filled);
            deactivate_obj(selection->btn_greater);
            deactivate_obj(selection->btn_lesser);
            break;
        case SEL_MODE_FILL: /* Fill */
            deactivate_obj(selection->ch_shape);
            deactivate_obj(selection->ch_brush);
            deactivate_obj(selection->spin_brush);
            deactivate_obj(selection->spin_nrand);
            deactivate_obj(selection->btn_filled);
            deactivate_obj(selection->btn_greater);
            deactivate_obj(selection->btn_lesser);
            break;
        case SEL_MODE_RANDOM: /* Random */
            deactivate_obj(selection->ch_shape);
            deactivate_obj(selection->ch_brush);
            deactivate_obj(selection->spin_brush);
            activate_obj(selection->spin_nrand,FL_COL1);
            deactivate_obj(selection->btn_filled);
            deactivate_obj(selection->btn_greater);
            deactivate_obj(selection->btn_lesser);
            break;
        case SEL_MODE_MARKER: /* Marker */
            deactivate_obj(selection->ch_shape);
            deactivate_obj(selection->ch_brush);
            deactivate_obj(selection->spin_brush);
            deactivate_obj(selection->spin_nrand);
            deactivate_obj(selection->btn_filled);
            deactivate_obj(selection->btn_greater);
            deactivate_obj(selection->btn_lesser);
            break;
        case SEL_MODE_CONTOUR: /* Isocontour */
            deactivate_obj(selection->ch_shape);
            deactivate_obj(selection->ch_brush);
            deactivate_obj(selection->spin_brush);
            deactivate_obj(selection->spin_nrand);
            activate_obj(selection->btn_filled,FL_YELLOW);
            fl_call_object_callback(selection->btn_filled);
            break;
        case SEL_MODE_ISOCOMPONENT: /* Isocontour component */
            deactivate_obj(selection->ch_shape);
            deactivate_obj(selection->ch_brush);
            deactivate_obj(selection->spin_brush);
            deactivate_obj(selection->spin_nrand);
            activate_obj(selection->btn_filled,FL_YELLOW);
            fl_call_object_callback(selection->btn_filled);
            break;
    }
    SELECTION_MODE(selection) = selmode;
}

void cb_sel_mode ( FL_OBJECT * obj  , long data  ) {
    _setup_selmode(GUI->selection,data);
    VIEW_SPSET(GUI->viewport) = false;
    VIEW_EPSET(GUI->viewport) = false;
    refresh_canvas(ENV);
    sel_set_cursor(GUI->viewport,GUI->selection);
}

void cb_sel_clear ( FL_OBJECT * obj  , long data  ) {
    const enum xg3d_scope scope = data ? SCOPE_GLOBAL : SCOPE_SLICE;
    xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);
    grid3d *selslice = VIEW_SELSLICE(GUI->viewport);

    switch(scope) {
        case SCOPE_GLOBAL:
            swk->nvox = 0;
            swk->value = 0.0;
            sel_clear(swk->g3d);
            /* fall-through */
        case SCOPE_SLICE:
            sel_clear(selslice);
            if(scope == SCOPE_SLICE) {
                g3d_ret ret = g3d_replace_slice(swk->g3d,selslice,VIEW_K(GUI->viewport),VIEW_PLANE(GUI->viewport));
                if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
                swk->nvox -= VIEW_SELNVOX(GUI->viewport);
                swk->value -= VIEW_SELVALUE(GUI->viewport);
            }
            VIEW_SELNVOX(GUI->viewport) = 0;
            VIEW_SELVALUE(GUI->viewport) = 0.0;
            break;
    }

    sel_sanity_check(GUI->viewport,GUI->selection);
    sel_update_info(GUI->viewport,GUI->selection);

    if(SELECTION_IS_VISIBLE(GUI->selection)) { 
        refresh_canvas(ENV);
        if(fl_get_button(GUI->overview->draw_sel))
            refresh_ov_canvases(GUI->overview);
    }
}

void cb_sel_scope ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    if(fl_get_button(obj)) {
        fl_set_object_label(obj,_("Global"));
        fl_set_menu_item_mode(GUI->selection->menu_sel_op,7,FL_PUP_GREY);
    } else {
        fl_set_object_label(obj,_("Slice"));
        fl_set_menu_item_mode(GUI->selection->menu_sel_op,7,FL_PUP_NONE);
    }
}

/* NOTE: Inline neighborhood test has no memory overhead */
void cb_selop_menu ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);
    xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);
    const enum xg3d_scope scope = fl_get_button(GUI->selection->btn_scope);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const coord_t k = VIEW_K(GUI->viewport);
    index_t *selnvoxels = (index_t*)&VIEW_SELNVOX(GUI->viewport);
    double *selvalue = (double*)&VIEW_SELVALUE(GUI->viewport);
    grid3d *selslice = VIEW_SELSLICE(GUI->viewport);
    grid3d *slice = VIEW_SLICE(GUI->viewport);
    double val = 0.0;
    const char *tval;
    index_t nvx = 0;
    coord_t jik[3];
    char *endptr;
    double r;

    switch(fl_get_menu(obj)) {
        case 1: /* Invert */
            fl_show_oneliner(_("Inverting selection..."),fl_scrw/2,fl_scrh/2);
            switch(scope) {
                case SCOPE_SLICE:
                    swk->nvox -= *selnvoxels; swk->value -= *selvalue;
                    sel_oper_invert(selslice,slice,scope,plane,0,0,selnvoxels,selvalue);
                    swk->nvox += *selnvoxels; swk->value += *selvalue;
                    {
                        g3d_ret ret = g3d_replace_slice(swk->g3d,selslice,k,plane);
                        if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
                    }
                    break;
                case SCOPE_GLOBAL:
                    sel_oper_invert(swk->g3d,DATA->g3d,scope,plane,0,0,&swk->nvox,&swk->value);
                    replace_selslice(GUI->viewport,GUI->selection);
                    break;
            }
            break;
        case 2: /* Grow */
            fl_show_oneliner(_("Growing selection..."),fl_scrw/2,fl_scrh/2);
            switch(scope) {
                case SCOPE_SLICE: 
                    if(*selnvoxels == 0 || *selnvoxels == selslice->nvox) {
                        fl_hide_oneliner(); return;
                    }
                    sel_oper_grow(selslice,slice,scope,plane,0,0,&nvx,&val);
                    *selnvoxels += nvx; *selvalue += val;
                    swk->nvox += nvx; swk->value += val;
                    {
                        g3d_ret ret = g3d_replace_slice(swk->g3d,selslice,k,plane);
                        if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
                    }
                    break;
                case SCOPE_GLOBAL:
                    if(swk->nvox == 0 || swk->nvox == swk->g3d->nvox) {
                        fl_hide_oneliner(); return;
                    }
                    sel_oper_grow(swk->g3d,DATA->g3d,scope,plane,0,0,&nvx,&val);
                    swk->nvox += nvx; swk->value += val;
                    replace_selslice(GUI->viewport,GUI->selection);
                    break;
            }
            break;
        case 3: /* Shrink */
            fl_show_oneliner(_("Shrinking selection..."),fl_scrw/2,fl_scrh/2);
            switch(scope) {
                case SCOPE_SLICE:
                    if(*selnvoxels == 0) { fl_hide_oneliner(); return; }
                    sel_oper_shrink(selslice,slice,scope,plane,0,0,&nvx,&val);
                    *selnvoxels -= nvx; *selvalue -= val;
                    swk->nvox -= nvx; swk->value -= val;
                    {
                        g3d_ret ret = g3d_replace_slice(swk->g3d,selslice,k,plane);
                        if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
                    }
                    break;
                case SCOPE_GLOBAL:
                    if(swk->nvox == 0) { fl_hide_oneliner(); return; }
                    sel_oper_shrink(swk->g3d,DATA->g3d,scope,plane,0,0,&nvx,&val);
                    swk->nvox -= nvx; swk->value -= val;
                    replace_selslice(GUI->viewport,GUI->selection);
                    break;
            }
            break;
        case 4: /* Border */
            fl_show_oneliner(_("Selecting border..."),fl_scrw/2,fl_scrh/2);
            switch(scope) {
                case SCOPE_SLICE:
                    if(*selnvoxels == 0) { fl_hide_oneliner(); return; }
                    swk->nvox -= *selnvoxels; swk->value -= *selvalue;
                    sel_oper_border(selslice,slice,scope,plane,0,0,selnvoxels,selvalue);
                    swk->nvox += *selnvoxels; swk->value += *selvalue;
                    {
                        g3d_ret ret = g3d_replace_slice(swk->g3d,selslice,k,plane);
                        if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
                    }
                    break;
                case SCOPE_GLOBAL:
                    if(swk->nvox == 0) { fl_hide_oneliner(); return; }
                    sel_oper_border(selslice,slice,scope,plane,0,0,&swk->nvox,&swk->value);
                    replace_selslice(GUI->viewport,GUI->selection);
                    break;
            }
            break;
        case 5: /* Fill in */
            fl_show_oneliner(_("Filling in selection..."),fl_scrw/2,fl_scrh/2);
            switch(scope) {
                case SCOPE_SLICE:
                    if(*selnvoxels == 0) { fl_hide_oneliner(); return; }

                    /* determine outside */
                    if(SELECTION_NMK(GUI->selection) == 0 && !g3d_selbit_check(selslice,0)) {
                        /* if no markers and voxel 0 unset, use it */
                        jik[PLANE_TO_J(plane)] = 0;
                        jik[PLANE_TO_I(plane)] = 0;
                        jik[PLANE_TO_K(plane)] = k;
                    } else if(SELECTION_NMK(GUI->selection) == 1 &&
                            ((marker*)SELECTION_MKLIST(GUI->selection))->vox[PLANE_TO_K(plane)] == k) {
                        /* if one marker in this slice, use it */
                        memcpy(jik,((marker*)SELECTION_MKLIST(GUI->selection))->vox,3*sizeof(coord_t));
                        sel_clear_markers(GUI->selection);
                    } else { /* FAIL! */
                        fl_hide_oneliner();
                        fl_show_alert2(0,_("Cannot tell outside!\fPlease set a single marker outside."));
                        return;
                    }

                    nvx = jik[PLANE_TO_J(plane)] + jik[PLANE_TO_I(plane)] * selslice->nx;
                    sel_oper_fillin(selslice,slice,scope,plane,0,0,&nvx,&val);
                    *selnvoxels += nvx; *selvalue += val;
                    swk->nvox += nvx; swk->value += val;
                    {
                        g3d_ret ret = g3d_replace_slice(swk->g3d,selslice,k,plane);
                        if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
                    }
                    break;
                case SCOPE_GLOBAL:
                    if(swk->nvox == 0) { fl_hide_oneliner(); return; }

                    /* determine outside */
                    if(SELECTION_NMK(GUI->selection) == 0 && !g3d_selbit_check(swk->g3d,0))
                        /* if no markers and voxel 0 unset, use it */
                        jik[0] = jik[1] = jik[2] = 0;
                    else if(SELECTION_NMK(GUI->selection) == 1) {
                        /* if one marker, use it */
                        memcpy(jik,((marker*)SELECTION_MKLIST(GUI->selection))->vox,3*sizeof(coord_t));
                        sel_clear_markers(GUI->selection);
                    } else { /* FAIL! */
                        fl_hide_oneliner();
                        fl_show_alert2(0,_("Cannot tell outside!\fPlease set a single marker outside."));
                        return;
                    }

                    nvx = COORD_DIM(jik,swk->g3d->nx,swk->g3d->nxy);
                    sel_oper_fillin(swk->g3d,DATA->g3d,scope,plane,0,0,&nvx,&val);
                    swk->nvox += nvx; swk->value += val;
                    replace_selslice(GUI->viewport,GUI->selection);
                    break;
            }
            break;
        case 6: /* Smooth */
            fl_show_oneliner(_("Smoothing selection..."),fl_scrw/2,fl_scrh/2);
            switch(scope) {
                case SCOPE_SLICE:
                    if(*selnvoxels == 0) { fl_hide_oneliner(); return; }
                    swk->nvox -= *selnvoxels; swk->value -= *selvalue;
                    sel_oper_smooth(selslice,slice,scope,plane,0,0,selnvoxels,selvalue);
                    swk->nvox += *selnvoxels; swk->value += *selvalue;
                    {
                        g3d_ret ret = g3d_replace_slice(swk->g3d,selslice,k,plane);
                        if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
                    }
                    break;
                case SCOPE_GLOBAL:
                    if(swk->nvox == 0) { fl_hide_oneliner(); return; }
                    sel_oper_smooth(swk->g3d,DATA->g3d,scope,plane,0,0,&swk->nvox,&swk->value);
                    replace_selslice(GUI->viewport,GUI->selection);
                    break;
            }
            break;
        case 7: /* Convex hull */
            fl_show_oneliner(_("Selecting convex hull..."),fl_scrw/2,fl_scrh/2);
            switch(scope) {
                case SCOPE_SLICE:
                    if(*selnvoxels < 2) { fl_hide_oneliner(); return; }
                    swk->nvox -= *selnvoxels; swk->value -= *selvalue;
                    sel_oper_cnvxhull(selslice,slice,scope,plane,0,0,selnvoxels,selvalue);
                    swk->nvox += *selnvoxels; swk->value += *selvalue;
                    {
                        g3d_ret ret = g3d_replace_slice(swk->g3d,selslice,k,plane);
                        if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
                    }
                    break;
                case SCOPE_GLOBAL:
                    break;
            }
            break;
        case 8: /* Histogram clamp */
            fl_show_oneliner(_("Clamping selection..."),fl_scrw/2,fl_scrh/2);
            switch(scope) {
                case SCOPE_SLICE:
                    if(*selnvoxels == 0) { fl_hide_oneliner(); return; }
                    sel_oper_histclamp(selslice,slice,scope,plane,qwk->min,qwk->max,&nvx,&val);
                    *selnvoxels -= nvx; *selvalue -= val;
                    swk->nvox -= nvx; swk->value -= val;
                    {
                        g3d_ret ret = g3d_replace_slice(swk->g3d,selslice,k,plane);
                        if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
                    }
                    break;
                case SCOPE_GLOBAL:
                    if(swk->nvox == 0) { fl_hide_oneliner(); return; }
                    sel_oper_histclamp(swk->g3d,DATA->g3d,scope,plane,qwk->min,qwk->max,&nvx,&val);
                    swk->nvox -= nvx; swk->value -= val;
                    replace_selslice(GUI->viewport,GUI->selection);
                    break;
            }
            break;
        case 9: /* Spherical cover */
            tval = fl_show_input(_("Sphere radius (voxels):"),"5");
            if(tval != NULL && strlen(tval) > 0 &&
                    (r = strtod(tval,&endptr)) && !(r == 0 && endptr == tval) && r >= 0.0) {
                fl_show_oneliner(_("Selecting spherical cover..."),fl_scrw/2,fl_scrh/2);
                switch(scope) {
                    case SCOPE_SLICE:
                        if(*selnvoxels == 0) { fl_hide_oneliner(); return; }
                        sel_oper_sphcover(selslice,slice,scope,plane,r,r*r,&nvx,&val);
                        *selnvoxels += nvx; *selvalue += val;
                        swk->nvox += nvx; swk->value += val;
                        {
                            g3d_ret ret = g3d_replace_slice(swk->g3d,selslice,k,plane);
                            if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
                        }
                        break;
                    case SCOPE_GLOBAL:
                        if(swk->nvox == 0) { fl_hide_oneliner(); return; }
                        sel_oper_sphcover(swk->g3d,DATA->g3d,scope,plane,r,r*r,&nvx,&val);
                        swk->nvox += nvx; swk->value += val;
                        replace_selslice(GUI->viewport,GUI->selection);
                        break;
                }
            } else {
                if(tval == NULL) return;
                fl_show_alert2(0,_("Invalid input\fPlease enter a valid radius."));
                return;
            }
            break;
    }
    fl_hide_oneliner();

    sel_sanity_check(GUI->viewport,GUI->selection);
    sel_update_info(GUI->viewport,GUI->selection);

    if(SELECTION_IS_VISIBLE(GUI->selection)) { 
        refresh_canvas(ENV);
        if(fl_get_button(GUI->overview->draw_sel))
            refresh_ov_canvases(GUI->overview);
    }
}

void cb_sel_propagate ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    if(fl_get_button(obj)) {
        activate_obj(GUI->selection->spin_rep,FL_COL1);
    } else {
        fl_set_spinner_value(GUI->selection->spin_rep,1.0);
        deactivate_obj(GUI->selection->spin_rep);
    }
}

#define RIGHT 0
#define UP 1
#define LEFT 2
#define DOWN 3
#define PREV 4
#define NEXT 5
void cb_sel_move ( FL_OBJECT * obj  , long data  ) {
    FL_OBJECT *slider = GUI->mainview->slider;
    xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);
    const unsigned int step = fl_get_spinner_value(GUI->selection->spin_step);
    const enum xg3d_scope scope = fl_get_button(GUI->selection->btn_scope);
    const int copy = fl_get_button(GUI->selection->btn_propagate);
    const enum xg3d_orientation orient = VIEW_ORIENT(GUI->viewport);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const coord_t k = VIEW_K(GUI->viewport);
    unsigned int rep = copy ? fl_get_spinner_value(GUI->selection->spin_rep) : 1;
    index_t *selnvoxels = (index_t*)&VIEW_SELNVOX(GUI->viewport);
    double *selvalue = (double*)&VIEW_SELVALUE(GUI->viewport);
    grid3d *selslice = VIEW_SELSLICE(GUI->viewport);
    index_t k_i = k;
    scoord_t newk;
    grid3d *sel;

    double zero,maxk;
    fl_get_slider_bounds(GUI->mainview->slider,&zero,&maxk);

    int ixj = PLANE_TO_J(plane);
    int ixi = PLANE_TO_I(plane);
    int ixk = PLANE_TO_K(plane);

    switch(scope) {
        case SCOPE_SLICE:
#define COORD_JIK(jik) COORD_DIM_IX(jik,ixj,ixi,ixk,swk->g3d->nx,swk->g3d->nxy)
            for(unsigned int m=0;m<rep;m++) {
                if(*selnvoxels == 0) return;

                /* move right in window coordinate system */
                /**/ if((data == RIGHT && (orient == ORIENT_0   || orient == ORIENT_F0))   ||
                        (data == UP    && (orient == ORIENT_90  || orient == ORIENT_F270)) ||
                        (data == LEFT  && (orient == ORIENT_180 || orient == ORIENT_F180)) ||
                        (data == DOWN  && (orient == ORIENT_270 || orient == ORIENT_F90)))
                    sel_oper_move(swk->g3d,selslice,scope,plane,0,-step,&k_i,NULL);

                /* move down in window coordinate system */
                else if((data == RIGHT && (orient == ORIENT_90  || orient == ORIENT_F90))  ||
                        (data == UP    && (orient == ORIENT_180 || orient == ORIENT_F0))   ||
                        (data == LEFT  && (orient == ORIENT_270 || orient == ORIENT_F270)) ||
                        (data == DOWN  && (orient == ORIENT_0   || orient == ORIENT_F180)))
                    sel_oper_move(swk->g3d,selslice,scope,plane,1,-step,&k_i,NULL);

                /* move left in window coordinate system */
                else if((data == RIGHT && (orient == ORIENT_180 || orient == ORIENT_F180)) ||
                        (data == UP    && (orient == ORIENT_270 || orient == ORIENT_F90))  ||
                        (data == LEFT  && (orient == ORIENT_0   || orient == ORIENT_F0))   ||
                        (data == DOWN  && (orient == ORIENT_90  || orient == ORIENT_F270)))
                    sel_oper_move(swk->g3d,selslice,scope,plane,0,+step,&k_i,NULL);

                /* move up in window coordinate system */
                else if((data == RIGHT && (orient == ORIENT_270 || orient == ORIENT_F270)) ||
                        (data == UP    && (orient == ORIENT_0   || orient == ORIENT_F180)) ||
                        (data == LEFT  && (orient == ORIENT_90  || orient == ORIENT_F90))  ||
                        (data == DOWN  && (orient == ORIENT_180 || orient == ORIENT_F0)))
                    sel_oper_move(swk->g3d,selslice,scope,plane,1,+step,&k_i,NULL);

                /* move prev or next */
                else if(data == PREV || data == NEXT) {
                    newk = (int)VIEW_K(GUI->viewport) + (data == PREV ? -(int)step : (int)step);
                    if(newk >= 0 && newk <= (int)maxk) {
                        sel = g3d_slice(swk->g3d,(coord_t)newk,plane);
                        g3d_u8_selbit_over(sel,selslice);
                        {
                            g3d_ret ret = g3d_replace_slice(swk->g3d,sel,(coord_t)newk,plane);
                            if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
                        }
                        g3d_free(sel); sel = NULL;
                        if(!copy) { 
                            g3d_u8_selbit_clear(selslice);
                            g3d_ret ret = g3d_replace_slice(swk->g3d,selslice,k,plane);
                            if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
                        }
                        sel_oper_count(swk->g3d,DATA->g3d,0,0,0,0,&swk->nvox,&swk->value);
                        sel_sanity_check(GUI->viewport,GUI->selection);
                        fl_set_slider_value(slider,newk);
                        fl_call_object_callback(slider);
                    } else if(!copy) {
                        swk->nvox -= *selnvoxels; swk->value -= *selvalue;
                        sel = g3d_slice(swk->g3d,k,plane);
                        g3d_u8_selbit_clear(sel);
                        {
                            g3d_ret ret = g3d_replace_slice(swk->g3d,sel,k,plane);
                            if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
                        }
                        replace_selslice(GUI->viewport,GUI->selection);
                        sel_sanity_check(GUI->viewport,GUI->selection);
                        sel_update_info(GUI->viewport,GUI->selection);
                        if(SELECTION_IS_VISIBLE(GUI->selection)) refresh_canvas(ENV);
                        return;
                    }
                }

                if(data >= RIGHT && data <= DOWN) {
                    if(copy) {
                        sel = g3d_slice(swk->g3d,k,plane);
                        g3d_u8_selbit_over(sel,selslice);
                        g3d_ret ret = g3d_replace_slice(swk->g3d,sel,k,plane);
                        if(ret != G3D_OK) g3d_ret_error(ret,"g3d_replace_slice");
                        g3d_free(sel); sel = NULL;
                    }
                    swk->nvox -= *selnvoxels; swk->value -= *selvalue;
                    replace_selslice(GUI->viewport,GUI->selection);
                    swk->nvox += *selnvoxels; swk->value += *selvalue;
                    sel_sanity_check(GUI->viewport,GUI->selection);
                    sel_update_info(GUI->viewport,GUI->selection);
                    if(SELECTION_IS_VISIBLE(GUI->selection)) refresh_canvas(ENV);
                }
            }
#undef COORD_JIK
            break;

        case SCOPE_GLOBAL:
            for(unsigned int m=0;m<rep;m++) {
                if(swk->nvox == 0) return;

                sel = g3d_dup(swk->g3d); // XXX: Duplicate whole selection! Crazy!

                /* move right in window coordinate system */
                /**/ if((data == RIGHT && (orient == ORIENT_0   || orient == ORIENT_F0))   ||
                        (data == UP    && (orient == ORIENT_90  || orient == ORIENT_F270)) ||
                        (data == LEFT  && (orient == ORIENT_180 || orient == ORIENT_F180)) ||
                        (data == DOWN  && (orient == ORIENT_270 || orient == ORIENT_F90)))
                    sel_oper_move(swk->g3d,sel,scope,plane,ixj,-step,NULL,NULL);

                /* move down in window coordinate system */
                else if((data == RIGHT && (orient == ORIENT_90  || orient == ORIENT_F90))  ||
                        (data == UP    && (orient == ORIENT_180 || orient == ORIENT_F0))   ||
                        (data == LEFT  && (orient == ORIENT_270 || orient == ORIENT_F270)) ||
                        (data == DOWN  && (orient == ORIENT_0   || orient == ORIENT_F180)))
                    sel_oper_move(swk->g3d,sel,scope,plane,ixi,-step,NULL,NULL);

                /* move left in window coordinate system */
                else if((data == RIGHT && (orient == ORIENT_180 || orient == ORIENT_F180)) ||
                        (data == UP    && (orient == ORIENT_270 || orient == ORIENT_F90))  ||
                        (data == LEFT  && (orient == ORIENT_0   || orient == ORIENT_F0))   ||
                        (data == DOWN  && (orient == ORIENT_90  || orient == ORIENT_F270)))
                    sel_oper_move(swk->g3d,sel,scope,plane,ixj,+step,NULL,NULL);

                /* move up in window coordinate system */
                else if((data == RIGHT && (orient == ORIENT_270 || orient == ORIENT_F270)) ||
                        (data == UP    && (orient == ORIENT_0   || orient == ORIENT_F180)) ||
                        (data == LEFT  && (orient == ORIENT_90  || orient == ORIENT_F90))  ||
                        (data == DOWN  && (orient == ORIENT_180 || orient == ORIENT_F0)))
                    sel_oper_move(swk->g3d,sel,scope,plane,ixi,+step,NULL,NULL);

                /* move next */
                else if(data == NEXT)
                    sel_oper_move(swk->g3d,sel,scope,plane,ixk,-step,NULL,NULL);

                /* move prev */
                else if(data == PREV)
                    sel_oper_move(swk->g3d,sel,scope,plane,ixk,+step,NULL,NULL);

                /* copy over original selection if requested */
                if(copy) g3d_u8_selbit_over(swk->g3d,sel);

                g3d_free(sel);
                sel_oper_count(swk->g3d,DATA->g3d,0,0,0,0,&swk->nvox,&swk->value);
                sel_sanity_check(GUI->viewport,GUI->selection);
                if(data >= RIGHT && data <= DOWN) {
                    replace_selslice(GUI->viewport,GUI->selection);
                    sel_update_info(GUI->viewport,GUI->selection);
                    if(SELECTION_IS_VISIBLE(GUI->selection)) refresh_canvas(ENV);
                } else if(data == PREV || data == NEXT) {
                    fl_set_slider_value(slider,(int)VIEW_K(GUI->viewport) + (data == PREV ? -(int)step : (int)step));
                    fl_call_object_callback(slider);
                }
            }
            break;
    }
    if(SELECTION_IS_VISIBLE(GUI->selection) && fl_get_button(GUI->overview->draw_sel))
        refresh_ov_canvases(GUI->overview);
}
#undef RIGHT
#undef UP
#undef LEFT
#undef DOWN
#undef PREV
#undef NEXT

void cb_sel_draw_menu ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);
    const enum xg3d_orientation orient = VIEW_ORIENT(GUI->viewport);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const marker *firstmk = SELECTION_MKLIST(GUI->selection);
    const unsigned int nmk = SELECTION_NMK(GUI->selection);
    const coord_t mvk = VIEW_K(GUI->viewport);
    const coord_t nx = VIEW_NX(GUI->viewport);
    const coord_t ny = VIEW_NY(GUI->viewport);
    const marker *mk;
    index_t nvx;
    double val;
    int op;

    switch((op = fl_get_menu(obj))) {
        case 2: /* Polygon (closed) */
        case 3: /* Polygon (open) */
            if(nmk < 2) { fl_show_alert2(0,_("Less than two markers!\fPlease set at least two markers.")); return; }
            sel_draw_polygon(swk->g3d,DATA->g3d,firstmk,plane,nx,ny,mvk,op == 2 && nmk > 2,&nvx,&val);
            break;
        case 6: /* 3-point circle (whole) */
        case 7: /* 3-point circle (arc) */
            if(nmk != 3) { fl_show_alert2(0,_("Other than three markers!\fPlease set exactly three markers.")); return; }
            mk = firstmk;
            if(mk->vox[PLANE_TO_K(plane)] != mk->next->vox[PLANE_TO_K(plane)] ||
                    mk->vox[PLANE_TO_K(plane)] != mk->next->next->vox[PLANE_TO_K(plane)]) {
                fl_show_alert2(0,_("Markers are not on the same slice!\fPlease set three markers on the same slice."));
                return;
            }
            sel_draw_circle3p(swk->g3d,DATA->g3d,firstmk,plane,nx,ny,mvk,op == 6,&nvx,&val);
            break;
        case 10: /* 5-point ellipse (whole) */
        case 11: /* 5-point ellipse (arc) */
            if(nmk != 5) {
                fl_show_alert2(0,_("Other than five markers!\fPlease set exactly five markers."));
                return;
            }
            mk = firstmk;
            if(mk->vox[PLANE_TO_K(plane)] != mk->next->vox[PLANE_TO_K(plane)] ||
                    mk->vox[PLANE_TO_K(plane)] != mk->next->next->vox[PLANE_TO_K(plane)] ||
                    mk->vox[PLANE_TO_K(plane)] != mk->next->next->next->vox[PLANE_TO_K(plane)] ||
                    mk->vox[PLANE_TO_K(plane)] != mk->next->next->next->next->vox[PLANE_TO_K(plane)]) {
                fl_show_alert2(0,_("Markers are not on the same slice!\fPlease set five markers on the same slice."));
                return;
            }
            if(sel_draw_ellipse5p(swk->g3d,DATA->g3d,firstmk,plane,nx,ny,mvk,op == 10,&nvx,&val)) {
                fl_show_alert2(0,_("Non-ellipse found!\fPlease set other five markers."));
                return;
            }
            break;
        case 14: /* Horizontal lines */
        case 15: /* Vertical lines */
            if(nmk == 0) { fl_show_alert2(0,_("No markers set!\fPlease set at least one marker.")); return; }
            sel_draw_lines(swk->g3d,DATA->g3d,firstmk,plane,nx,ny,mvk,(op == 15 && orient%2 == 0) || (op == 14 && orient%2 == 1),&nvx,&val);
            break;
        case 17: /* Points */
            if(nmk == 0) { fl_show_alert2(0,_("No markers set!\fPlease set at least one marker.")); return; }
            sel_draw_points(swk->g3d,DATA->g3d,firstmk,plane,nx,ny,mvk,false,&nvx,&val);
            break;
        case 18: /* Brush along path */
            if(nmk == 0) { fl_show_alert2(0,_("No markers set!\fPlease set at least one marker.")); return; }
            nvx = fl_get_spinner_value(GUI->selection->spin_brush);
            sel_draw_brushpath(swk->g3d,DATA->g3d,firstmk,plane,nx,ny,fl_get_choice(GUI->selection->ch_brush),false,&nvx,&val);
            break;
    }
    sel_clear_markers(GUI->selection);

    swk->nvox += nvx; swk->value += val;
    replace_selslice(GUI->viewport,GUI->selection);

    sel_sanity_check(GUI->viewport,GUI->selection);
    sel_update_info(GUI->viewport,GUI->selection);

    if(SELECTION_IS_VISIBLE(GUI->selection)) { 
        refresh_canvas(ENV);
        if(fl_get_button(GUI->overview->draw_sel))
            refresh_ov_canvases(GUI->overview);
    }
}

/* No parallelism for greater confidence */
void cb_sel_recalculate ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_selection_workspace *swk = SELECTION_WORKSPACE(GUI->selection);
    const int flag_calib = fl_get_button(GUI->selection->btn_uval);
    const grid3d *selslice = VIEW_SELSLICE(GUI->viewport);
    const grid3d *slice = VIEW_SLICE(GUI->viewport);
    const xg3d_header *hdr = DATA->hdr;
    double val,mean,sd,min,max;
    index_t nvx,tmp;

    fl_show_oneliner(_("Recalculating selection..."),fl_scrw/2,fl_scrh/2);

    sel_recal(selslice,slice,&nvx,&tmp,&val,&mean,&sd,&min,&max);
    VIEW_SELNVOX(GUI->viewport) = nvx; VIEW_SELVALUE(GUI->viewport) = val;
    fl_set_object_label(GUI->selection->tx_mean_s,"");
    fl_set_object_label(GUI->selection->tx_spread_s,"");
    fl_set_object_label(GUI->selection->tx_min_s,"");
    fl_set_object_label(GUI->selection->tx_max_s,"");
    if(nvx >= 1) {
        fl_set_object_label_f(GUI->selection->tx_mean_s,"%.8g",
                flag_calib ? CALIBVAL(hdr,mean) : mean);
        fl_set_object_label_f(GUI->selection->tx_min_s,"%.8g",
                flag_calib ? CALIBVAL(hdr,min) : min);
        fl_set_object_label_f(GUI->selection->tx_max_s,"%.8g",
                flag_calib ? CALIBVAL(hdr,max) : max);
    }
    if(nvx >= 2) {
        sd = sqrt(sd / (nvx - 1));
        fl_set_object_label_f(GUI->selection->tx_spread_s,"%.8g",
                flag_calib ? sd * hdr->cal_slope : sd);
    }
    if(tmp >= 1)
        debug("%"PRIindex" SEL_TMP voxels found, this should not happen!",tmp);

    sel_recal(swk->g3d,DATA->g3d,&nvx,&tmp,&val,&mean,&sd,&min,&max);
    swk->nvox = nvx; swk->value = val;
    fl_set_object_label(GUI->selection->tx_mean_g,"");
    fl_set_object_label(GUI->selection->tx_spread_g,"");
    fl_set_object_label(GUI->selection->tx_min_g,"");
    fl_set_object_label(GUI->selection->tx_max_g,"");
    if(nvx >= 1) {
        fl_set_object_label_f(GUI->selection->tx_mean_g,"%.8g",
                flag_calib ? CALIBVAL(hdr,mean) : mean);
        fl_set_object_label_f(GUI->selection->tx_min_g,"%.8g",
                flag_calib ? CALIBVAL(hdr,min) : min);
        fl_set_object_label_f(GUI->selection->tx_max_g,"%.8g",
                flag_calib ? CALIBVAL(hdr,max) : max);
    }
    if(nvx >= 2) {
        sd = sqrt(sd / (nvx - 1));
        fl_set_object_label_f(GUI->selection->tx_spread_g,"%.8g",
                flag_calib ? sd * hdr->cal_slope : sd);
    }
    if(tmp >= 1)
        error("%"PRIindex" SEL_TMP voxels found, this should not happen!",tmp);

    sel_sanity_check(GUI->viewport,GUI->selection);
    sel_update_info(GUI->viewport,GUI->selection);
    fl_set_object_lcolor(GUI->selection->tx_spread_s,FL_BLACK);
    fl_set_object_lcolor(GUI->selection->tx_min_s,FL_BLACK);
    fl_set_object_lcolor(GUI->selection->tx_max_s,FL_BLACK);
    fl_set_object_lcolor(GUI->selection->tx_spread_g,FL_BLACK);
    fl_set_object_lcolor(GUI->selection->tx_min_g,FL_BLACK);
    fl_set_object_lcolor(GUI->selection->tx_max_g,FL_BLACK);

    fl_hide_oneliner();
}

void cb_sel_refresh ( FL_OBJECT * obj  , long data  ) {
    if(data == 0 || (data > 0 && SELECTION_IS_VISIBLE(GUI->selection))) {
        refresh_canvas(ENV);
        if(fl_get_button(GUI->overview->draw_sel))
            refresh_ov_canvases(GUI->overview);
    }
}

void cb_sel_filled ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    if(fl_get_button(obj)) {
        activate_obj(GUI->selection->btn_greater,FL_YELLOW);
        activate_obj(GUI->selection->btn_lesser,FL_YELLOW);
    } else {
        deactivate_obj(GUI->selection->btn_greater);
        deactivate_obj(GUI->selection->btn_lesser);
    }
}

/* ==================
 * SETTINGS FUNCTIONS
 * ================== */

void setup_selection_settings(FD_selection *selection, const unsigned long valuemask, const xg3d_selection_settings *settings) {
    if(xg3d_settings_check(valuemask,SELECTION_SCOPE)) {
        fl_set_button(selection->btn_scope,settings->scope);
        if(settings->scope == SCOPE_GLOBAL)
            fl_set_menu_item_mode(selection->menu_sel_op,7,FL_PUP_GREY);
    }

    if(xg3d_settings_check(valuemask,SELECTION_VISIBLE))
        SELECTION_SET_VISIBLE(selection,settings->visible);
    if(xg3d_settings_check(valuemask,SELECTION_LABELS))
        fl_set_button(selection->btn_labels,settings->labels);
    if(xg3d_settings_check(valuemask,SELECTION_SELPATT)) {
        switch(settings->patt) {
            default:
            case SEL_PATT_CROSS:  fl_set_button(selection->btn_cross,1);  break;
            case SEL_PATT_DOT:    fl_set_button(selection->btn_dot,1);    break;
            case SEL_PATT_SQUARE: fl_set_button(selection->btn_square,1); break;
        }
    }

    if(xg3d_settings_check(valuemask,SELECTION_CLAMP))
        fl_set_button(selection->btn_clamp,settings->clamp);
    if(xg3d_settings_check(valuemask,SELECTION_REPLACE))
        fl_set_button(selection->btn_replace,settings->replace);
    if(xg3d_settings_check(valuemask,SELECTION_BRUSH_SHAPE))
        fl_set_choice(selection->ch_brush,settings->brush_shape);
    if(xg3d_settings_check(valuemask,SELECTION_BRUSH_SIZE))
        fl_set_spinner_value(selection->spin_brush,settings->brush_size);
    if(xg3d_settings_check(valuemask,SELECTION_SHAPE))
        fl_set_choice(selection->ch_shape,settings->shape);
    if(xg3d_settings_check(valuemask,SELECTION_NRAND))
        fl_set_spinner_value(selection->spin_nrand,settings->nrand);
    if(xg3d_settings_check(valuemask,SELECTION_ISO_FILLED))
        fl_set_button(selection->btn_filled,settings->iso_filled);
    if(xg3d_settings_check(valuemask,SELECTION_ISO_GREATER))
        fl_set_button(settings->iso_greater ? selection->btn_greater : selection->btn_lesser,1);
    if(xg3d_settings_check(valuemask,SELECTION_SET_MODE)) {
        switch(settings->mode) {
            case SEL_MODE_NONE:         fl_set_button(selection->btn_sel_none,1);         break;
            case SEL_MODE_BRUSH:        fl_set_button(selection->btn_sel_brush,1);        break;
            case SEL_MODE_SHAPE:        fl_set_button(selection->btn_sel_shape,1);        break;
            case SEL_MODE_LEVEL:        fl_set_button(selection->btn_sel_level,1);        break;
            case SEL_MODE_COMPONENT:    fl_set_button(selection->btn_sel_component,1);    break;
            case SEL_MODE_RANDOM:       fl_set_button(selection->btn_sel_random,1);       break;
            case SEL_MODE_FILL:         fl_set_button(selection->btn_sel_fill,1);         break;
            case SEL_MODE_MARKER:       fl_set_button(selection->btn_sel_marker,1);       break;
            case SEL_MODE_CONTOUR:      fl_set_button(selection->btn_sel_contour,1);      break;
            case SEL_MODE_ISOCOMPONENT: fl_set_button(selection->btn_sel_isocomponent,1); break;
        }
        _setup_selmode(selection,settings->mode);
    }

    if(xg3d_settings_check(valuemask,SELECTION_REP))
        fl_set_spinner_value(selection->spin_rep,settings->rep);
    if(xg3d_settings_check(valuemask,SELECTION_STEP))
        fl_set_spinner_value(selection->spin_step,settings->step);
    if(xg3d_settings_check(valuemask,SELECTION_PROPAGATE))
        fl_set_button(selection->btn_propagate,settings->propagate);

    if(xg3d_settings_check(valuemask,SELECTION_MARKER_LIST)) {
        SELECTION_NMK(selection) = settings->nmk;
        marker_free((marker*)SELECTION_MKLIST(selection));
        SELECTION_MKLIST(selection) = NULL;
        if(settings->nmk >= 1)
            SELECTION_MKLIST(selection) = marker_dup(settings->marker_list);
    }
}

xg3d_selection_settings* store_selection_settings(const FD_selection *selection, const unsigned long valuemask) {
    xg3d_selection_settings *settings = calloc(1,sizeof(xg3d_selection_settings));

    if(xg3d_settings_check(valuemask,SELECTION_SCOPE))
        settings->scope = fl_get_button(selection->btn_scope);

    if(xg3d_settings_check(valuemask,SELECTION_VISIBLE))
        settings->visible = SELECTION_IS_VISIBLE(selection);
    if(xg3d_settings_check(valuemask,SELECTION_LABELS))
        settings->labels = fl_get_button(selection->btn_labels);
    if(xg3d_settings_check(valuemask,SELECTION_SELPATT))
        settings->patt = fl_get_button(selection->btn_cross) ? SEL_PATT_CROSS :
            (fl_get_button(selection->btn_dot) ? SEL_PATT_DOT : SEL_PATT_SQUARE);

    if(xg3d_settings_check(valuemask,SELECTION_CLAMP))
        settings->clamp = fl_get_button(selection->btn_clamp);
    if(xg3d_settings_check(valuemask,SELECTION_REPLACE))
        settings->replace = fl_get_button(selection->btn_replace);
    if(xg3d_settings_check(valuemask,SELECTION_BRUSH_SHAPE))
        settings->brush_shape = fl_get_choice(selection->ch_brush);
    if(xg3d_settings_check(valuemask,SELECTION_BRUSH_SIZE))
        settings->brush_size = fl_get_spinner_value(selection->spin_brush);
    if(xg3d_settings_check(valuemask,SELECTION_SHAPE))
        settings->shape = fl_get_choice(selection->ch_shape);
    if(xg3d_settings_check(valuemask,SELECTION_NRAND))
        settings->nrand = fl_get_spinner_value(selection->spin_nrand);
    if(xg3d_settings_check(valuemask,SELECTION_ISO_FILLED))
        settings->iso_filled = fl_get_button(selection->btn_filled);
    if(xg3d_settings_check(valuemask,SELECTION_ISO_GREATER))
        settings->iso_greater = fl_get_button(selection->btn_greater);
    if(xg3d_settings_check(valuemask,SELECTION_SET_MODE))
        settings->mode = SELECTION_MODE(selection);

    if(xg3d_settings_check(valuemask,SELECTION_REP))
        settings->rep = fl_get_spinner_value(selection->spin_rep);
    if(xg3d_settings_check(valuemask,SELECTION_STEP))
        settings->step = fl_get_spinner_value(selection->spin_step);
    if(xg3d_settings_check(valuemask,SELECTION_PROPAGATE))
        settings->propagate = fl_get_button(selection->btn_propagate);

    if(xg3d_settings_check(valuemask,SELECTION_MARKER_LIST) && SELECTION_NMK(selection) >= 1) {
        settings->nmk = SELECTION_NMK(selection);
        settings->marker_list = marker_dup((marker*)SELECTION_MKLIST(selection));
    }

    return settings;
}
