#ifndef XG3D_io_h_
#define XG3D_io_h_

#include <stdint.h>

#include "xgrid3d.h"
#include "grid3d_io.h"
#include "nifti1_io.h"

#undef I
#include "NrrdIO.h"

#define HDR_TEST_MUPET(file,hdr) \
    (strncmp((file)+strlen(file)-8,".img.hdr",8) == 0 \
     && parse_mupet_header(file,hdr) == G3D_OK)
#define HDR_TEST_INTERFILE(file,hdr) \
    ((strncmp((file)+strlen(file)-3,".hv",3) == 0 \
      || strncmp((file)+strlen(file)-3,".hs",3) == 0) \
      && parse_interfile_header(file,hdr))
#define HDR_TEST_NIFTI(file,hdr) \
    ((strncmp((file)+strlen(file)-4,".nii",4) == 0 \
      || strncmp((file)+strlen(file)-7,".nii.gz",7) == 0) \
      && parse_nifti_header(file,hdr) == G3D_OK)
#define HDR_TEST_NRRD(file,hdr) \
    ((strncmp((file)+strlen(file)-5,".nrrd",5) == 0 \
      || strncmp((file)+strlen(file)-8,".nrrd.gz",8) == 0 \
      || strncmp((file)+strlen(file)-5,".nhdr",5) == 0) \
      && parse_nrrd_header(file,hdr) == G3D_OK)

#define HDR_TEST_ALL(file,hdr) \
    (HDR_TEST_MUPET(file,hdr) \
     || HDR_TEST_INTERFILE(file,hdr) \
     || HDR_TEST_NIFTI(file,hdr) \
     || HDR_TEST_NRRD(file,hdr))

g3d_ret load_data(xg3d_env*,const xg3d_header*);
void xg3d_header_free(xg3d_header*);
xg3d_header* xg3d_header_dup(const xg3d_header*);
void xg3d_header_fill_data(xg3d_env*,const xg3d_header*);
enum g3d_compr_type file_compr_type(const char*);
void xg3d_env_to_hdr(const xg3d_env*,xg3d_header*);

/* NIfTI stuff */
nifti_image* xg3d_to_nifti(const grid3d*,const xg3d_header*);
g3d_ret parse_nifti_header(const char*,xg3d_header*);

/* Interfile stuff */
g3d_ret parse_interfile_header(const char*,xg3d_header*);

/* uPET stuff */
int grep_mupet(FILE*,const char*,char**);
g3d_ret export_mupet_header(const char *,const xg3d_header*);
int raw_io_type_to_mupet(enum g3d_raw_io_type,const bool);
g3d_ret parse_mupet_header(const char*,xg3d_header*);

/* Nrrd stuff */
Nrrd* xg3d_to_nrrd(const grid3d*,const xg3d_header*);
g3d_ret parse_nrrd_header(const char*,xg3d_header*);

void set_type_text(FL_OBJECT*,enum g3d_raw_io_type,enum g3d_endian_type);
g3d_ret parse_type_arg(const char*, enum g3d_raw_io_type*);

#endif
