/**
 * Copyright (c) 2003 Billy Biggs <vektor@dumbterm.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Version 0.5
 *  - Change interface by Sirio Bolaños Puchet
 * Version 0.4
 *  - Various additional revisions by Nathan Moroney of HP Labs
 * Version 0.3
 *  - Further cleanups and a function to return all of J,C,h,Q,M,s.
 * Version 0.2
 *  - Cleanup, added missing functions.
 * Version 0.1
 *  - Initial release
 */

#ifndef CIECAM02_H_INCLUDED
#define CIECAM02_H_INCLUDED

/**
 * The following is an implementation of the forward and inverse
 * functions for XYZ to JCh values from CIECAM02, as well as a function
 * to return all of J, C, h, Q, M and s.  It has been tested against the
 * spreadsheet of example calculations posted by Mark D.  Fairchild on
 * his website (http://www.cis.rit.edu/fairchild/).
 *
 * This code should be used with XYZ values in 0 to 100, not 0 to 1 like
 * most of my other code uses.  For input from sRGB, I recommend that
 * you use these values: D65 whitepoint, 20% gray, La value of 4 cd/m^2
 * to correspond to an ambient illumination of 64 lux:
 *
 *  la = 4.0;
 *  yb = 20.0;
 *  xw = 95.05;
 *  yw = 100.00;
 *  zw = 108.88;
 *
 *  The f, c and nc parameters control the surround.  CIECAM02 uses
 *  these values for average (relative luminance > 20% of scene white),
 *  dim (between 0% and 20%), and dark (0%).  In general, use average
 *  for both input and output.
 *
 *  // Average
 *  f = 1.0; c = 0.690; nc = 1.0;
 *  // Dim
 *  f = 0.9; c = 0.590; nc = 0.95;
 *  // Dark
 *  f = 0.8; c = 0.525; nc = 0.8;
 *
 * J is the lightness.
 * C is the chroma.
 * h is the hue angle in 0-360.
 * Q is the brightness.
 * M is the colourfulness.
 * s is the saturation.
 */

#define VC_AVERAGE_f  1.00
#define VC_AVERAGE_c  0.69
#define VC_AVERAGE_nc 1.00

#define VC_DIM_f  0.90
#define VC_DIM_c  0.59
#define VC_DIM_nc 0.95

#define VC_DARK_f  0.800
#define VC_DARK_c  0.525
#define VC_DARK_nc 0.800

/**
 * Overall data structure for a CIECAM02 color
 */
struct CIECAM02color {
  double x, y, z;
  double J, C, h, H;
  double Q, M, s;
  double ac, bc;
  double as, bs;
  double am, bm;
};

/**
 * Overall data structure for CIECAM02 viewing conditions
 */
struct CIECAM02vc {
  double xw, yw, zw, aw;
  double la, yb;
  int surround;
  double n, z, f, c, nbb, nc, ncb, fl, d;
};

/**
 * Init viewing conditions data structure from xw, yw, zw, yb, la, surround (f, c, nc)
 */
void CIECAM02_initVC(struct CIECAM02vc*,const double[3],double,double,double,double,double);

/**
 * CIECAM02 forward transformation
 */
struct CIECAM02color CIECAM02_forward(struct CIECAM02color,struct CIECAM02vc);

/**
 * CIECAM02 inverse transformation
 */
struct CIECAM02color CIECAM02_inverse(struct CIECAM02color,struct CIECAM02vc);

#endif /* CIECAM02_H_INCLUDED */
