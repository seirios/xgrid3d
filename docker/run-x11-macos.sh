#!/bin/bash

# Setup X socket
socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\" 2>&1 >/dev/null &
PID=$!

# Use 100dpi X fonts (looks better)
xset +fp /opt/X11/share/fonts/100dpi
xset fp rehash

CMD="xgrid3d $@"
docker run --rm -it \
    -e DISPLAY=docker.for.mac.host.internal:0 \
    xgrid3d:x11 "$CMD"

# Cleanup fontpath
xset fp default
xset fp rehash

# Terminate X socket
kill $PID
