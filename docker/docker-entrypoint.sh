#!/bin/bash
if [ -n "$XAUTHORITY" ]; then
    chown user $XAUTHORITY
fi
su -c "$@" user
