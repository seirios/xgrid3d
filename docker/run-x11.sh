#!/bin/bash
CONTAINER_HOSTNAME=xgrid3d-container
TMP_XAUTH=$HOME/.Xauthority_$CONTAINER_HOSTNAME

# Add X11 authorization (if not already present)
if [ ! -f $TMP_XAUTH ]; then
    touch $TMP_XAUTH
    # Get Xauth key
    KEY=$(xauth list unix$DISPLAY | awk '{ print $3 }')
    xauth -f $TMP_XAUTH add $CONTAINER_HOSTNAME/unix$DISPLAY . $KEY
fi

# Use 100dpi X fonts (looks better)
xset +fp /usr/share/fonts/100dpi
xset fp rehash

CMD="xgrid3d $@"
docker run --rm -it \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v $TMP_XAUTH:/tmp/.Xauthority \
    -e XAUTHORITY=/tmp/.Xauthority \
    -e DISPLAY=unix$DISPLAY \
    -h $CONTAINER_HOSTNAME \
    xgrid3d:x11 "$CMD"

# Cleanup fontpath
xset fp default
xset fp rehash

# Remove X11 authorization
rm -f $TMP_XAUTH
