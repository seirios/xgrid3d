#!/bin/bash

# Adjust resolution to match your preference
RESOLUTION=1920x1080

docker run --rm \
    -p 5900:5900 \
    -e X11VNC_CREATE_GEOM=$RESOLUTION \
    xgrid3d:vnc
