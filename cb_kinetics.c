#include <errno.h>
#include <float.h>

#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "cb_datasets.h"
#include "cb_viewport.h"
#include "cb_mainview.h"
#include "cb_selection.h"
#include "cb_kinetics.h"
#include "cb_rois.h"

#include <gsl/gsl_statistics_float.h>

#include "io.h"
#include "util.h"
#include "util_fit2d.h"
#include "util_coordinates.h"
#include "util_nuclides.h"
#include "util_units.h"

#define FRAME_FORMAT "%-28.28s @ %8jus"

void kinetics_workspace_clear(xg3d_kinetics_workspace *x, const bool delete) {
    if(x != NULL) {
        xroi_free(x->xroi); x->xroi = NULL;
        xg3d_frames_free(x->frames,x->nframes);
        x->frames = NULL; x->nframes = 0;
        if(delete) free(x);
    }
}

void xg3d_frame_free(xg3d_frame *x) {
	if(x != NULL) {
		free(x->file);
		free(x->vals);
		free(x);
	}
}

void xg3d_frames_free(xg3d_frame **x, const unsigned int n) {
	if(x != NULL) {
		for(unsigned int i=0;i<n;i++)
			xg3d_frame_free(x[i]);
		free(x);
	}
}

int frame_cmp_time(const void *a, const void *b) {
	const xg3d_frame *ka = *((xg3d_frame**)a);
	const xg3d_frame *kb = *((xg3d_frame**)b);

	if     (ka->t < kb->t) return -1;
	else if(ka->t > kb->t) return  1;
	else                   return  0;
}

void refresh_kin_browser(FD_kinetics *kinetics) {
    xg3d_kinetics_workspace *kwk = KINETICS_WORKSPACE(kinetics);

	fl_clear_browser(kinetics->browser);
	for(unsigned int i=0;i<kwk->nframes;i++)
		fl_add_browser_line_f(kinetics->browser,FRAME_FORMAT,
				kwk->frames[i]->file,
				kwk->frames[i]->t);

	if(kwk->nframes == 0)
		deactivate_obj(kinetics->btn_inspect);
	else
		activate_obj(kinetics->btn_inspect,FL_YELLOW);

    if(fl_get_button(kinetics->btn_inspect)) {
        if(kwk->nframes == 0) {
            fl_set_button(kinetics->btn_inspect,0);
            fl_call_object_callback(kinetics->btn_inspect);
        } else
            fl_call_object_callback(kinetics->ch_inspect_mode);
    }
}

void cb_kin_remove ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_kinetics_workspace *kwk = KINETICS_WORKSPACE(GUI->kinetics);
    const int ix = fl_get_browser(GUI->kinetics->browser);

    if(ix == 0) return;

    if(kwk->nframes == 1)
        if(!fl_show_question(_("Remove last frame? Kinetics will be cleared."),0))
            return;

    xg3d_frame_free(kwk->frames[ix-1]);
    memmove(kwk->frames+ix-1,kwk->frames+ix,(kwk->nframes-ix)*sizeof(xg3d_frame*));
    kwk->frames = realloc(kwk->frames,(kwk->nframes-1)*sizeof(xg3d_frame*));
    kwk->nframes--;
    refresh_kin_browser(GUI->kinetics);
    if(kwk->nframes == 0) {
        xroi_free(kwk->xroi); kwk->xroi = NULL;
        fl_set_object_label(GUI->kinetics->txt_roi,"");
        fl_set_object_label(GUI->kinetics->txt_roi_size,"");
        fl_set_object_label(GUI->kinetics->txt_roi_vol,"");
        fl_set_object_label(GUI->kinetics->txt_roi_vox,"");
        fl_set_object_label(GUI->kinetics->txt_min,"");
        fl_set_object_label(GUI->kinetics->txt_max,"");
        fl_set_object_label(GUI->kinetics->txt_avg,"");
        fl_set_object_label(GUI->kinetics->txt_std,"");
        fl_set_object_label(GUI->kinetics->txt_med,"");
        fl_set_object_label(GUI->kinetics->txt_tot,"");
    }
}

int kin_input_time(uintmax_t *ot, const char *deflt) {
	char *eptr = NULL;
	const char *tt;
	double t;

	tt = fl_show_input(_("Time of frame (s) [suffix m(inutes),h(ours),d(ays)]:"),deflt);
	if(tt == NULL) { return 0; }
	if(float_time_validator(NULL,NULL,tt,0) == FL_VALID) {
		t = strtod(tt,&eptr);
		if(! *eptr) {
			*ot = (uintmax_t)round(t);
			return 1;
		} else if((*eptr == 'm' || *eptr == 'h' || *eptr == 'd') &&
				strchr(tt,*eptr) == eptr && eptr == tt + strlen(tt) - 1) {
			switch(*eptr) {
				case 'm': t *= MINUTE_TO_SECOND; break;
				case 'h': t *= HOUR_TO_SECOND; break;
				case 'd': t *= DAY_TO_SECOND; break;
			}
			*ot = (uintmax_t)round(t);
			return 1;
		} else
			return -1;
	} else
		return -1;
}

void cb_kin_settime ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_kinetics_workspace *kwk = KINETICS_WORKSPACE(GUI->kinetics);
	const int ix = fl_get_browser(GUI->kinetics->browser);
	char buf[24];
	uintmax_t t;
	int ret;

    if(ix == 0) return;
    snprintf(buf,sizeof(buf),"%"PRIuMAX,kwk->frames[ix-1]->t);
    if((ret = kin_input_time(&t,buf)) < 1) {
        if(ret < 0)
            fl_show_alert2(0,_("Invalid time!\fPlease input a valid positive time for this frame."));
        return;
    }
    if(t == kwk->frames[ix-1]->t) return; /* same time, do nothing */
    for(unsigned int i=0;i<kwk->nframes;i++)
        if(t == kwk->frames[i]->t) { /* check duplicate time */
            fl_show_alert2(0,_("Duplicate time found!\fPlease assign unique times to frames."));
            return;
        }
    kwk->frames[ix-1]->t = t;
    fl_replace_browser_line_f(GUI->kinetics->browser,ix, FRAME_FORMAT,
            kwk->frames[ix-1]->file, kwk->frames[ix-1]->t);
    if(fl_get_button(GUI->kinetics->btn_inspect))
        fl_call_object_callback(GUI->kinetics->ch_inspect_mode);
}

void cb_kin_inp_time ( FL_OBJECT * obj  , long data  ) {
    xg3d_kinetics_workspace *kwk = KINETICS_WORKSPACE(GUI->kinetics);
	char *eptr = NULL;
	const char *tt;
	double t;

	switch(data) {
		case 0: /* hl */
		case 1: /* ti */
		case 2: /* tf */
			tt = fl_get_input(obj); t = strtod(tt,&eptr);
			if(! *eptr)
				fl_set_input_f(obj,"%"PRIuMAX,(uintmax_t)round(t));
			else if((*eptr == 'm' || *eptr == 'h' || *eptr == 'd') &&
					strchr(tt,*eptr) == eptr && eptr == tt + strlen(tt) - 1) {
				switch(*eptr) {
					case 'm': t *= MINUTE_TO_SECOND; break;
					case 'h': t *= HOUR_TO_SECOND; break;
					case 'd': t *= DAY_TO_SECOND; break;
				}
				fl_set_input_f(obj,"%"PRIuMAX,(uintmax_t)round(t));
			}
			break;
		case 3: /* first frame */
			if(kwk->nframes >= 1)
				fl_set_input_f(GUI->kinetics->inp_ti,"%"PRIuMAX,kwk->frames[0]->t);
			break;
		case 4: /* last frame */
			if(kwk->nframes >= 1)
				fl_set_input_f(GUI->kinetics->inp_tf,"%"PRIuMAX,kwk->frames[kwk->nframes-1]->t);
			break;
	}
}

void cb_kin_compute ( FL_OBJECT * obj  , long data  ) {
    (void)data;
(void)nuclides;
    xg3d_kinetics_workspace *kwk = KINETICS_WORKSPACE(GUI->kinetics);
	const char *tival = fl_get_input(GUI->kinetics->inp_ti),
		       *tfval = fl_get_input(GUI->kinetics->inp_tf),
			   *hlval = fl_get_input(GUI->kinetics->inp_hl);
	const index_t nvox = kwk->xroi->roi->size;
	double lambda,ilambda;
    double *cumul = NULL;
	uintmax_t hl,ti,tf;
	xg3d_frame *frame;
    xg3d_data *dat;

	if(kwk->nframes == 0) {
		fl_show_alert2(0,_("No frames!\fPlease add at least one frame."));
		return;
	}

	if(tival == NULL || strlen(tival) == 0 || tfval == NULL || strlen(tfval) == 0) {
		fl_show_alert2(0,_("Empty time!\fPlease fill in initial and final times."));
		return;
	}

	ti = strtoumax(tival,NULL,10);
	tf = strtoumax(tfval,NULL,10);
	
	switch(fl_get_choice(GUI->kinetics->ch_model)) {
		case 1: /* Static */
			if(kwk->nframes > 1) {
				fl_show_alert2(0,_("More than one frame!\fStatic model requires just one frame."));
				return;
			}
			if(hlval == NULL || strlen(hlval) == 0) {
				fl_show_alert2(0,_("Empty half-life!\fPlease fill in half-life."));
				return;
			}
			frame = kwk->frames[0]; 
			hl = strtoumax(hlval,NULL,10); lambda = M_LN2/(double)hl; ilambda = (double)hl*M_LOG2E;
			cumul = malloc(nvox*sizeof(double));
			fl_show_oneliner(_("Computing cumulated activity..."),fl_scrw/2,fl_scrh/2);
#ifdef _OPENMP
#pragma omp parallel for
#endif
			for(index_t ix=0;ix<nvox;ix++)
				cumul[ix] = frame->vals[ix] * ilambda
                    * (frame->t >= 1 ? exp(lambda*frame->t) : 1.0)
                    * (ti >= 1 ? (exp(-lambda*ti) - exp(-lambda*tf)) : (1.0 - exp(-lambda*tf)));
			fl_hide_oneliner();
			break;
		case 2: /* Effective */
			if(kwk->nframes < 2) {
				fl_show_alert2(0,_("Less than two frames!\fEffective model requires at least two frames."));
				return;
			}
            /* TODO: do something */
			break;
		case 3: /* Biexponential */
			if(kwk->nframes < 4) {
				fl_show_alert2(0,_("Less than four frames!\fBiexponential model requires at least four frames."));
				return;
			}
            /* TODO: do something */
			break;
	}

    dat = calloc(1,sizeof(xg3d_data));
    dat->hdr = xg3d_header_dup(DATA->hdr);
    dat->hdr->cal_slope = 1.0;
    dat->hdr->cal_inter = 0.0;
    free(dat->hdr->file);
    dat->hdr->file = strdup("Cumulated activity");
    dat->g3d = g3d_alloc_dfill(G3D_FLOAT64,DATA->g3d->nx,DATA->g3d->ny,DATA->g3d->nz,.data=cumul); free(cumul);
    dat->stats = g3d_stats_get(dat->g3d,NULL,G3D_STATS_ALL);

    xg3d_datasets_add(ENV->data,dat);
    refresh_datasets_browser(GUI->datasets->browser,ENV->data);

	fl_set_object_label_f(GUI->kinetics->txt_min,"%.12g Bq·s",dat->stats->min);
	fl_set_object_label_f(GUI->kinetics->txt_max,"%.12g Bq·s",dat->stats->max);
	fl_set_object_label_f(GUI->kinetics->txt_tot,"%.12g Bq·s",dat->stats->sum);
	fl_set_object_label_f(GUI->kinetics->txt_avg,"%.12g Bq·s",dat->stats->mean);
	fl_set_object_label_f(GUI->kinetics->txt_std,"%.12g Bq·s",dat->stats->stdev);
	fl_set_object_label_f(GUI->kinetics->txt_med,"%.12g Bq·s",dat->stats->median);
}

void cb_kin_nuclide ( FL_OBJECT * obj  , long data  ) {
    (void)data;
	fl_set_input_f(GUI->kinetics->inp_hl,"%"PRIuMAX,
			(uintmax_t)round(nuclide_halflife(fl_get_menu_text(obj),UNIT_SECOND)));
}

/* =========
 * INSPECTOR
 * ========= */

void deactivate_kin_inspector(FD_kinetics *kinetics, const bool total, const bool voxel) {
	if(total) {
		deactivate_obj(kinetics->plot);
		deactivate_obj(kinetics->btn_test);
		deactivate_obj(kinetics->btn_fill);
		deactivate_obj(kinetics->menu_export);
		deactivate_obj(kinetics->br_test);
		deactivate_obj(kinetics->ch_inspect_mode);
	}
	if(voxel) {
		deactivate_obj(kinetics->btn_pick);
		deactivate_obj(kinetics->btn_show);
		deactivate_obj(kinetics->sli_vox);
		deactivate_obj(kinetics->spin_vox);
	}
}

static void activate_kin_inspector(FD_kinetics *kinetics, const bool total, const bool voxel) {
	if(total) {
		activate_obj(kinetics->plot,FL_BLACK);
		activate_obj(kinetics->btn_test,FL_COL1);
		activate_obj(kinetics->btn_fill,FL_YELLOW);
		activate_obj(kinetics->menu_export,FL_COL1);
		activate_obj(kinetics->br_test,FL_WHITE);
		activate_obj(kinetics->ch_inspect_mode,FL_BLACK);
	}
	if(voxel) {
		activate_obj(kinetics->btn_pick,FL_COL1);
		fl_set_object_color(kinetics->btn_pick,FL_YELLOW,FL_COL1);
		activate_obj(kinetics->btn_show,FL_COL1);
		fl_set_object_color(kinetics->btn_show,FL_YELLOW,FL_COL1);
		activate_obj(kinetics->sli_vox,FL_YELLOW);
		activate_obj(kinetics->spin_vox,FL_MCOL);
	}
}

void cb_kin_inspect ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_kinetics_workspace *kwk = KINETICS_WORKSPACE(GUI->kinetics);

	if(fl_get_button(obj)) {
		fl_set_spinner_bounds(GUI->kinetics->spin_vox,0,kwk->xroi->roi->size-1);
		fl_set_slider_bounds(GUI->kinetics->sli_vox,0,kwk->xroi->roi->size-1);
		fl_call_object_callback(GUI->kinetics->ch_inspect_mode);
	} else {
		fl_clear_xyplot(GUI->kinetics->plot);
		fl_clear_browser(GUI->kinetics->br_test);
		deactivate_kin_inspector(GUI->kinetics,true,true);
		fl_set_spinner_value(GUI->kinetics->spin_vox,0);
		fl_set_slider_value(GUI->kinetics->sli_vox,0);
	}
}

void cb_kin_inspect_mode ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_kinetics_workspace *kwk = KINETICS_WORKSPACE(GUI->kinetics);
	const unsigned int nframes = kwk->nframes;

	switch(fl_get_choice(obj)) {
		case 1: /* Voxel */
			activate_kin_inspector(GUI->kinetics,true,true);
			fl_call_object_callback(GUI->kinetics->spin_vox);
			break;
		case 2: { /* Total */
			activate_kin_inspector(GUI->kinetics,true,false);
			deactivate_kin_inspector(GUI->kinetics,false,true);
			float *x = malloc(nframes*sizeof(float));
			float *y = malloc(nframes*sizeof(float));
			for(unsigned int i=0;i<nframes;i++) {
				xg3d_frame *frame = kwk->frames[i];
				double tot = 0.0;
                double c = 0.0;
#ifdef _OPENMP
#pragma omp parallel for reduction(+:tot) reduction(+:c)
#endif
				for(index_t ix=0;ix<kwk->xroi->roi->size;ix++) {
					double v = frame->vals[ix];
					double t = tot + v;
					c = (t - tot) - v;
					tot = t;
				}
				tot = tot - c;
				x[i] = frame->t;
				y[i] = tot;
			}
			fl_clear_xyplot(GUI->kinetics->plot);

            float min,max,rng;
			gsl_stats_float_minmax(&min,&max,x,1,nframes); rng = max - min;
			fl_set_xyplot_xbounds(GUI->kinetics->plot,min-0.05*rng,max+0.05*rng);
			gsl_stats_float_minmax(&min,&max,y,1,nframes); rng = max - min;
			fl_set_xyplot_ybounds(GUI->kinetics->plot,min-0.05*rng,max+0.05*rng);
			fl_set_xyplot_data(GUI->kinetics->plot,x,y,nframes,"",_("Time [s]"),_("Activity Bq"));
			free(x); free(y);
            } break;
	}
}

void cb_kin_inspect_selvox ( FL_OBJECT * obj  , long data  ) {
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
	const g3d_roi *roi = KINETICS_ROI(GUI->kinetics);

	if(DATA->g3d == NULL) return;

	switch(data) {
		case 0: { /* From */
			if(SELECTION_NMK(GUI->selection) != 1) {
				fl_show_alert2(0,_("Other than one marker set!\fPlease set only one marker.")); return;
			}
            coord_t *vox = ((marker*)SELECTION_MKLIST(GUI->selection))->vox;
            index_t ix = roi->size;
			for(index_t i=0;i<roi->size;i++)
                if(ROI_J(i,roi) == vox[0] && ROI_I(i,roi) == vox[1] && ROI_K(i,roi) == vox[2]) {
                    ix = i; break; }
			if(ix == roi->size) {
				fl_show_alert2(0,_("Voxel not found!\fMarker set outside ROI."));
				return;
			}
			fl_set_spinner_value(GUI->kinetics->spin_vox,ix);
			fl_call_object_callback(GUI->kinetics->spin_vox);
            } break;
		case 1: { /* To */
			index_t i = fl_get_spinner_value(GUI->kinetics->spin_vox);
			sel_clear_markers(GUI->selection);
            /* map global coordinates to in-plane coordinates */
            sel_add_marker(ENV,
                    roi->vox[3*i+PLANE_TO_J(plane)],
                    roi->vox[3*i+PLANE_TO_I(plane)],
                    roi->vox[3*i+PLANE_TO_K(plane)],"");
			fl_set_button(GUI->selection->btn_sel_marker,1);
			fl_call_object_callback(GUI->selection->btn_sel_marker);
			fl_set_slider_value(GUI->mainview->slider,roi->vox[3*i+PLANE_TO_K(plane)]);
			fl_call_object_callback(GUI->mainview->slider);
            } break;
	}
}

void cb_kin_inspect_vox ( FL_OBJECT * obj  , long data  ) {
    xg3d_kinetics_workspace *kwk = KINETICS_WORKSPACE(GUI->kinetics);
	FL_OBJECT *slider = GUI->kinetics->sli_vox,
			  *spinner = GUI->kinetics->spin_vox;
	const int ret = fl_get_object_return_state(obj);
	const unsigned int nframes = kwk->nframes;

    if(data) /* slider */
		fl_set_slider_value(slider,fl_get_spinner_value(spinner));
    else /* spinner */
		fl_set_spinner_value(spinner,fl_get_slider_value(slider));

	if(data || (!data && ret == FL_RETURN_END)) {
		index_t ix = (index_t)fl_get_spinner_value(spinner);
		float *x = malloc(nframes*sizeof(float));
		float *y = malloc(nframes*sizeof(float));
		for(unsigned int i=0;i<nframes;i++) {
			xg3d_frame *frame = kwk->frames[i];
			x[i] = frame->t;
			y[i] = frame->vals[ix];
		}
		fl_clear_xyplot(GUI->kinetics->plot);

        float min,max,rng;
		gsl_stats_float_minmax(&min,&max,x,1,nframes); rng = max - min;
		fl_set_xyplot_xbounds(GUI->kinetics->plot,min-0.05*rng,max+0.05*rng);
		gsl_stats_float_minmax(&min,&max,y,1,nframes); rng = max - min;
		fl_set_xyplot_ybounds(GUI->kinetics->plot,min-0.05*rng,max+0.05*rng);
		fl_set_xyplot_data(GUI->kinetics->plot,x,y,nframes,"",_("Time [s]"),_("Activity Bq"));
		free(x); free(y);
		fl_clear_browser(GUI->kinetics->br_test);
	}
}

void cb_kin_inspect_fill ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    const int overlay_type = fl_get_button(obj) ? FL_FILL_XYPLOT : FL_EMPTY_XYPLOT;

    fl_set_xyplot_overlay_type(GUI->kinetics->plot,1,overlay_type);
}

#define FIT_POINTS 150
void cb_kin_inspect_test ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_kinetics_workspace *kwk = KINETICS_WORKSPACE(GUI->kinetics);
	const char *tival = fl_get_input(GUI->kinetics->inp_ti),
		       *tfval = fl_get_input(GUI->kinetics->inp_tf),
			   *hlval = fl_get_input(GUI->kinetics->inp_hl);

	if(tival == NULL || strlen(tival) == 0 || tfval == NULL || strlen(tfval) == 0) {
		fl_show_alert2(0,_("Empty time!\fPlease fill in initial and final times."));
		return;
	}

	uintmax_t ti = strtoumax(tival,NULL,10);
	uintmax_t tf = strtoumax(tfval,NULL,10);

    int n; float *fx,*fy;
	fl_get_xyplot_data_pointer(GUI->kinetics->plot,0,&fx,&fy,&n);
	switch(fl_get_choice(GUI->kinetics->ch_model)) {
        default:
		case 1: { /* static */
			if(n > 1) {
				fl_show_alert2(0,_("Too many time points!\fStatic model requires just one time point."));
				return;
			}
			if(hlval == NULL || strlen(hlval) == 0) {
				fl_show_alert2(0,_("Empty half-life!\fPlease fill in half-life."));
				return;
			}
			uintmax_t hl = strtoumax(hlval,NULL,10);
            double lambda = M_LN2/(double)hl;
            double ilambda = (double)hl*M_LOG2E;

			float *mx = malloc(FIT_POINTS*sizeof(float));
            float *my = malloc(FIT_POINTS*sizeof(float));
			float mx0 = FL_min((float)ti,fx[0]);
			float mx1 = FL_max((float)tf,fx[n-1]);
			float dx = (mx1-mx0) / (FIT_POINTS-1);
			int ixi = 0;
            int ixf = FIT_POINTS-1;
            float dtmp,dti=FLT_MAX,dtf=FLT_MAX;
			for(int i=0;i<FIT_POINTS;i++) {
				mx[i] = fmaf(i,dx,mx0);
				/* find integration interval */
				if((dtmp = fabs(mx[i]-(float)ti)) < dti) { ixi = i; dti = dtmp; }
				if((dtmp = fabs(mx[i]-(float)tf)) < dtf) { ixf = i; dtf = dtmp; }
				my[i] = fit_func_exp((double)(mx[i]-fx[0]),fy[0],lambda);
			}

                                        float min,max,rmg,my0,my1;
                float rng = mx1 - mx0;
                gsl_stats_float_minmax(&min,&max,fx,1,kwk->nframes); rmg = max - min;
                fl_set_xyplot_xbounds(GUI->kinetics->plot,FL_min(min-0.05*rmg,mx0-0.05*rng),FL_max(max+0.05*rmg,mx1+0.05*rng));
                gsl_stats_float_minmax(&my0,&my1,my,1,FIT_POINTS); rng = my1 - my0;
                gsl_stats_float_minmax(&min,&max,fy,1,kwk->nframes); rmg = max - min;
                fl_set_xyplot_ybounds(GUI->kinetics->plot,FL_min(min-0.05*rmg,my0-0.05*rng),FL_max(max+0.05*rmg,my1+0.05*rng));

                fl_add_xyplot_overlay(GUI->kinetics->plot,1,mx+ixi,my+ixi,ixf-ixi+1,FL_YELLOW);
                if(fl_get_button(GUI->kinetics->btn_fill))
                    fl_set_xyplot_overlay_type(GUI->kinetics->plot,1,FL_FILL_XYPLOT);
                else
                    fl_set_xyplot_overlay_type(GUI->kinetics->plot,1,FL_EMPTY_XYPLOT);
                fl_add_xyplot_overlay(GUI->kinetics->plot,2,mx,my,FIT_POINTS,FL_BLACK);
                fl_set_xyplot_overlay_type(GUI->kinetics->plot,2,FL_NORMAL_XYPLOT);
                free(mx); free(my);
           
			/* Compute cumulated activity */
			double cumul = fy[0] * ilambda
				* (fx[0] != 0.0 ? exp(lambda*fx[0]) : 1.0)
				* (ti >= 1 ? (exp(-lambda*ti) - exp(-lambda*tf)) : (1.0 - exp(-lambda*tf)));
			fl_clear_browser(GUI->kinetics->br_test);
			fl_add_browser_line_f(GUI->kinetics->br_test,_("Initial activity:\n %g Bq"),fy[0]);
			fl_add_browser_line_f(GUI->kinetics->br_test,_("Time interval:\n %g s"),(double)(tf-ti));
			fl_add_browser_line_f(GUI->kinetics->br_test,_("Cumulated activity:\n %g Bq·s"),cumul);
            } break;
		case 2: { /* effective */
			if(n < 2) {
				fl_show_alert2(0,_("Too few time points!\fEffective model requires at least two time points."));
				return;
			}

			double *x = malloc(n*sizeof(double));
            double *y = malloc(n*sizeof(double));
			for(int i=0;i<n;i++) { x[i] = (double)fx[i]; y[i] = (double)fy[i]; }
            double p_init[2] = {y[0],log(y[n-1]/y[0]) / (x[0]-x[n-1])};
            struct fit_result *fit = fit_exp(x,y,NULL,n,p_init);
			free(x); free(y);
			if(fit == NULL) {
				fl_show_alert2(0,_("Fit failed!\fPlease check time-activity plot."));
				return;
			}

			gnuplot_errors(fit);
			double lambda = fit->par[1];
            double ilambda = 1.0 / fit->par[1];

			float *mx = malloc(FIT_POINTS*sizeof(float));
            float *my = malloc(FIT_POINTS*sizeof(float));
			float mx0 = FL_min((float)ti,fx[0]);
			float mx1 = FL_max((float)tf,fx[n-1]);
			float dx = (mx1-mx0) / (FIT_POINTS-1);
			int ixi = 0;
            int ixf = FIT_POINTS-1;
            float dtmp,dti=FLT_MAX,dtf=FLT_MAX;
			for(int i=0;i<FIT_POINTS;i++) {
				mx[i] = fmaf(i,dx,mx0);
				/* find integration interval */
				if((dtmp = fabs(mx[i]-(float)ti)) < dti) { ixi = i; dti = dtmp; }
				if((dtmp = fabs(mx[i]-(float)tf)) < dtf) { ixf = i; dtf = dtmp; }
				my[i] = fit_func_exp((double)mx[i],fit->par[0],fit->par[1]);
			}

                            float min,max,rmg,my0,my1;
                float rng = mx1 - mx0;
                gsl_stats_float_minmax(&min,&max,fx,1,kwk->nframes); rmg = max - min;
                fl_set_xyplot_xbounds(GUI->kinetics->plot,FL_min(min-0.05*rmg,mx0-0.05*rng),FL_max(max+0.05*rmg,mx1+0.05*rng));
                gsl_stats_float_minmax(&my0,&my1,my,1,FIT_POINTS); rng = my1 - my0;
                gsl_stats_float_minmax(&min,&max,fy,1,kwk->nframes); rmg = max - min;
                fl_set_xyplot_ybounds(GUI->kinetics->plot,FL_min(min-0.05*rmg,my0-0.05*rng),FL_max(max+0.05*rmg,my1+0.05*rng));

                fl_add_xyplot_overlay(GUI->kinetics->plot,1,mx+ixi,my+ixi,ixf-ixi+1,FL_YELLOW);
                if(fl_get_button(GUI->kinetics->btn_fill))
                    fl_set_xyplot_overlay_type(GUI->kinetics->plot,1,FL_FILL_XYPLOT);
                else
                    fl_set_xyplot_overlay_type(GUI->kinetics->plot,1,FL_EMPTY_XYPLOT);
                fl_add_xyplot_overlay(GUI->kinetics->plot,2,mx,my,FIT_POINTS,FL_BLACK);
                fl_set_xyplot_overlay_type(GUI->kinetics->plot,2,FL_NORMAL_XYPLOT);
                free(mx); free(my);
           
			/* Compute cumulated activity */
			double cumul = fit->par[0] * ilambda * (ti >= 1 ? (exp(-lambda*ti) - exp(-lambda*tf)) : (1.0 - exp(-lambda*tf)));
			fl_clear_browser(GUI->kinetics->br_test);
			fl_add_browser_line_f(GUI->kinetics->br_test,_("A0 = %12g\n   ± %12g"),fit->par[0],fit->err[0]);
			fl_add_browser_line_f(GUI->kinetics->br_test,_(" l = %12g\n   ± %12g"),fit->par[1],fit->err[1]);
			fl_add_browser_line_f(GUI->kinetics->br_test,_("degrees of freedom:\n %u"),fit->dof);
			fl_add_browser_line_f(GUI->kinetics->br_test,_("final chi²:\n %g"),SQR(fit->chi));
			fl_add_browser_line_f(GUI->kinetics->br_test,_("rms of residuals:\n %g"),fit->dof == 0 ? NAN : fit->chi/sqrt(fit->dof));
			fl_add_browser_line_f(GUI->kinetics->br_test,_("reduced chi²:\n %g"),fit->dof == 0 ? NAN : SQR(fit->chi)/fit->dof);
			fl_add_browser_line(GUI->kinetics->br_test,_("@-"));
			fl_add_browser_line_f(GUI->kinetics->br_test,_("Time interval:\n %g s"),(double)(tf-ti));
			fl_add_browser_line_f(GUI->kinetics->br_test,_("Cumulated activity:\n %g Bq·s"),cumul);
			free_fit_result(fit);
            } break;
        case 3: /* XXX: biexponential */
			if(n < 4) {
				fl_show_alert2(0,_("Too few time points!\fBiexponential model requires at least four time points."));
				return;
			}
			break;
    }
}
#undef FIT_POINTS

void cb_kin_inspect_export ( FL_OBJECT * obj  , long data  ) {
    (void)data;

	switch(fl_get_menu(obj)) {
		case 1: { /* Data */
            const char *file = fl_show_fselector(_("Export kinetics data to..."),"","*.txt","");
            if(file == NULL) return;
            FILE *fp = fopen(file,"w");
            if(fp == NULL) {
                fl_show_alert2(0,_("Cannot open file for writing!\f%s"),file);
                return;
            }
            int n; float *x,*y;
            fl_get_xyplot_data_pointer(GUI->kinetics->plot,0,&x,&y,&n);
            for(int i=0;i<n;i++)
                fprintf(fp,"%g %g\n",x[i],y[i]);
            fclose(fp);
            } break;
		case 2: { /* Plot */
			FLPS_CONTROL *flpsc = flps_init();
			flpsc->orientation = FLPS_LANDSCAPE;
			flpsc->eps = 1;
			fl_object_ps_dump(GUI->kinetics->plot,NULL);
            } break;
	}
}

void cb_kin_model ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    if(fl_get_button(GUI->kinetics->btn_inspect))
        fl_call_object_callback(GUI->kinetics->ch_inspect_mode);
}


