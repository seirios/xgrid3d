/* Form definition file generated by fdesign on Sat Sep 28 21:14:16 2024 */

#include <stdlib.h>
#include "fd_menu.h"

#ifdef ENABLE_NLS
#include <libintl.h>
#define _(String) gettext (String)
#else
#define _(String) (String)
#endif

#include "icon_pirate.xbm"
#include "xg.xpm"

/***************************************
 ***************************************/

FD_menu *
create_form_menu( void )
{
    FL_OBJECT *obj;
    FD_menu *fdui = ( FD_menu * ) fl_malloc( sizeof *fdui );

    fdui->vdata = fdui->cdata = NULL;
    fdui->ldata = 0;

    fdui->menu = fl_bgn_form( FL_NO_BOX, 110, 520 );

    fdui->box = obj = fl_add_box( FL_FLAT_BOX, 0, 0, 110, 520, "" );

    fdui->btn_quit = obj = fl_add_bitmapbutton( FL_NORMAL_BUTTON, 5, 485, 100, 30, "" );
    fl_set_button_shortcut( obj, "^Q", 1 );
    fl_set_object_color( obj, FL_RED, FL_RED );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_button_mouse_buttons( obj, 1 );
    fl_set_bitmapbutton_data( obj, icon_pirate_width, icon_pirate_height, ( unsigned char * ) icon_pirate_bits );

    obj = fl_add_labelframe( FL_ENGRAVED_FRAME, 5, 185, 100, 150, _("I/O") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->btn_kinetics = obj = fl_add_button( FL_PUSH_BUTTON, 5, 375, 100, 30, _("Kinetics") );
    fl_set_button_shortcut( obj, "^T", 1 );
    fl_set_object_color( obj, FL_DARKCYAN, FL_DARKCYAN );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_menu_showhide, 3 );
    fl_set_button_mouse_buttons( obj, 5 );

    fdui->btn_kernel = obj = fl_add_button( FL_PUSH_BUTTON, 5, 410, 100, 30, _("Kernel") );
    fl_set_button_shortcut( obj, "^K", 1 );
    fl_set_object_color( obj, FL_OLIVEDRAB, FL_OLIVEDRAB );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_menu_showhide, 4 );
    fl_set_button_mouse_buttons( obj, 5 );

    fdui->btn_film = obj = fl_add_button( FL_PUSH_BUTTON, 5, 445, 100, 30, _("Film") );
    fl_set_button_shortcut( obj, "^L", 1 );
    fl_set_object_color( obj, FL_KHAKI, FL_KHAKI );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_menu_showhide, 5 );
    fl_set_button_mouse_buttons( obj, 5 );

    obj = fl_add_pixmap( FL_NORMAL_PIXMAP, 18, 5, 74, 69, "" );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_pixmap_data( obj, xg_xpm );

    obj = fl_add_text( FL_NORMAL_TEXT, 5, 475, 100, 10, _("@DnLine") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    obj = fl_add_button( FL_NORMAL_BUTTON, 5, 90, 100, 15, _("About xgrid3d") );
    fl_set_object_boxtype( obj, FL_FRAME_BOX );
    fl_set_object_color( obj, FL_LEFT_BCOL, FL_COL1 );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_menu_about, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    obj = fl_add_box( FL_NO_BOX, 5, 74, 100, 15, _("v0.3") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->btn_new = obj = fl_add_button( FL_NORMAL_BUTTON, 10, 195, 90, 30, _("New") );
    fl_set_button_shortcut( obj, "^N", 1 );
    fl_set_object_color( obj, FL_CORNFLOWERBLUE, FL_CORNFLOWERBLUE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_menu_io, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->menu_load = obj = fl_add_menu( FL_PULLDOWN_MENU, 10, 230, 90, 30, _("Load") );
    fl_set_object_shortcut( obj, "^O", 1 );
    fl_set_object_boxtype( obj, FL_UP_BOX );
    fl_set_object_color( obj, FL_PALEGREEN, FL_PALEGREEN );
    fl_set_object_lalign( obj, FL_ALIGN_LEFT | FL_ALIGN_INSIDE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_menu_io, 1 );
    fl_show_menu_symbol( obj, 1 );
    fl_addto_menu( obj, _("Data") );
    fl_addto_menu( obj, _("Workspace") );

    fdui->menu_save = obj = fl_add_menu( FL_PULLDOWN_MENU, 10, 265, 90, 30, _("Save") );
    fl_set_object_shortcut( obj, "^S", 1 );
    fl_set_object_boxtype( obj, FL_UP_BOX );
    fl_set_object_color( obj, FL_GOLDENROD, FL_GOLDENROD );
    fl_set_object_lalign( obj, FL_ALIGN_LEFT | FL_ALIGN_INSIDE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_menu_io, 2 );
    fl_show_menu_symbol( obj, 1 );
    fl_addto_menu( obj, _("Data") );
    fl_addto_menu( obj, _("Workspace") );

    fdui->btn_datasets = obj = fl_add_button( FL_PUSH_BUTTON, 5, 110, 100, 30, _("Datasets") );
    fl_set_button_shortcut( obj, "^D", 1 );
    fl_set_object_color( obj, FL_GOLD, FL_GOLD );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_menu_showhide, 0 );
    fl_set_button_mouse_buttons( obj, 5 );

    fdui->btn_mainview = obj = fl_add_button( FL_PUSH_BUTTON, 5, 340, 100, 30, _("Mainview") );
    fl_set_button_shortcut( obj, "^M", 1 );
    fl_set_object_color( obj, FL_FIREBRICK, FL_FIREBRICK );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_menu_showhide, 2 );
    fl_set_button_mouse_buttons( obj, 5 );

    obj = fl_add_button( FL_NORMAL_BUTTON, 10, 300, 90, 30, _("Close") );
    fl_set_button_shortcut( obj, "^W", 1 );
    fl_set_object_color( obj, FL_INDIANRED, FL_INDIANRED );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_menu_io, 3 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_function = obj = fl_add_button( FL_PUSH_BUTTON, 5, 145, 100, 30, _("Function") );
    fl_set_button_shortcut( obj, "^F", 1 );
    fl_set_object_color( obj, FL_DARKORANGE, FL_DARKORANGE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_menu_showhide, 1 );
    fl_set_button_mouse_buttons( obj, 5 );

    fl_end_form( );

    fdui->menu->fdui = fdui;

    return fdui;
}
