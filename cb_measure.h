#ifndef XG3D_measure_cb_h_
#define XG3D_measure_cb_h_

#include "fd_measure.h"
#include "fd_viewport.h"
#include "xgrid3d.h"

#define PROFILE_GET_CALIB(fd_measure) (fl_get_button(((FD_measure*)(fd_measure))->btn_units))
#define PROFILE_NPOINTS(fd_measure) (((FD_measure*)(fd_measure))->plot_profile->u_ldata)
#define PROFILE_VALUES(fd_measure) (((FD_measure*)(fd_measure))->plot_profile->u_vdata)

void clear_line_profile_plot(FD_measure*);
void update_line_profile(FD_measure*,FD_viewport*);
void update_line_profile_plot(FD_measure*,FD_viewport*);
void update_measurement(FD_measure*,FD_viewport*);
void update_measurement_live(FD_measure*,FD_viewport*,const coord_t,const coord_t,const coord_t);

#endif
