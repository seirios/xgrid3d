/* Form definition file generated by fdesign on Sat Sep 28 21:14:16 2024 */

#include <stdlib.h>
#include "fd_coloring.h"

#ifdef ENABLE_NLS
#include <libintl.h>
#define _(String) gettext (String)
#else
#define _(String) (String)
#endif


/***************************************
 ***************************************/

FD_coloring *
create_form_coloring( void )
{
    FL_OBJECT *obj;
    FD_coloring *fdui = ( FD_coloring * ) fl_malloc( sizeof *fdui );

    fdui->vdata = fdui->cdata = NULL;
    fdui->ldata = 0;

    fdui->coloring = fl_bgn_form( FL_NO_BOX, 300, 303 );

    obj = fl_add_box( FL_FLAT_BOX, 0, 0, 300, 303, "" );

    obj = fl_add_labelframe( FL_ENGRAVED_FRAME, 5, 8, 290, 98, _("Colormap") );
    fl_set_object_lstyle( obj, FL_ITALIC_STYLE );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->btn_reverse = obj = fl_add_checkbutton( FL_PUSH_BUTTON, 10, 50, 85, 25, _("Reverse") );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_lutflag, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_invert = obj = fl_add_checkbutton( FL_PUSH_BUTTON, 10, 75, 85, 25, _("Invert") );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_lutflag, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->tf_coloring = obj = fl_add_tabfolder( FL_TOP_TABFOLDER, 5, 130, 292, 168, "" );
    fl_set_object_resize( obj, FL_RESIZE_X );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_coloring_mode, 0 );

    fdui->btn_under = obj = fl_add_pixmapbutton( FL_NORMAL_BUTTON, 95, 50, 45, 25, _("Underflow color") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_flow_color, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_over = obj = fl_add_pixmapbutton( FL_NORMAL_BUTTON, 95, 75, 45, 25, _("Overflow color") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_flow_color, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    obj = fl_add_button( FL_NORMAL_BUTTON, 250, 50, 40, 50, _("Pick\nfrom\nLUT") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_flow_fromlut, 0 );

    fdui->lut_pix = obj = fl_add_pixmap( FL_NORMAL_PIXMAP, 22, 20, 256, 25, "" );
    fl_set_object_boxtype( obj, FL_DOWN_BOX );
    fl_set_object_resize( obj, FL_RESIZE_X );
    fl_set_object_gravity( obj, FL_North, FL_NoGravity );
    fl_set_pixmap_align( obj, FL_ALIGN_LEFT | FL_ALIGN_INSIDE, 0, 0 );

    obj = fl_add_box( FL_FLAT_BOX, 5, 110, 290, 20, _("Coloring mode") );
    fl_set_object_lstyle( obj, FL_BOLD_STYLE );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fl_end_form( );

    fdui->coloring->fdui = fdui;

    return fdui;
}

/***************************************
 ***************************************/

FD_col_grad *
create_form_col_grad( void )
{
    FL_OBJECT *obj;
    FD_col_grad *fdui = ( FD_col_grad * ) fl_malloc( sizeof *fdui );

    fdui->vdata = fdui->cdata = NULL;
    fdui->ldata = 0;

    fdui->col_grad = fl_bgn_form( FL_NO_BOX, 290, 143 );

    fdui->box = obj = fl_add_box( FL_FLAT_BOX, 0, 0, 290, 143, "" );

    fdui->sli_hue = obj = fl_add_slider( FL_HOR_NICE_SLIDER, 5, 17, 280, 20, _("Hue spin") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lalign( obj, FL_ALIGN_LEFT_TOP );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_hue_slider, 0 );
    fl_set_object_return( obj, FL_RETURN_CHANGED );
    fl_set_slider_bounds( obj, 0, 359 );
    fl_set_slider_value( obj, 120 );
    fl_set_slider_step( obj, 1 );
    fl_set_slider_increment( obj, 1, 1 );

    fdui->txt_hue = obj = fl_add_text( FL_NORMAL_TEXT, 248, 1, 41, 15, _("+120°") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );

    fdui->grp_grad_mode = fl_bgn_group( );

    fdui->btn_value = obj = fl_add_checkbutton( FL_RADIO_BUTTON, 210, 87, 80, 25, _("Value") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_gradient_type, 2 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_lightness = obj = fl_add_checkbutton( FL_RADIO_BUTTON, 0, 62, 110, 25, _("Lightness") );
    fl_set_object_callback( obj, cb_gradient_type, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_saturation = obj = fl_add_checkbutton( FL_RADIO_BUTTON, 0, 87, 110, 25, _("Saturation") );
    fl_set_object_callback( obj, cb_gradient_type, 3 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_luma = obj = fl_add_checkbutton( FL_RADIO_BUTTON, 0, 37, 110, 25, _("Luma") );
    fl_set_object_callback( obj, cb_gradient_type, 0 );
    fl_set_button( obj, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_grayscale = obj = fl_add_checkbutton( FL_RADIO_BUTTON, 110, 37, 100, 25, _("Grayscale") );
    fl_set_object_callback( obj, cb_gradient_type, 4 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_hotmetal = obj = fl_add_checkbutton( FL_RADIO_BUTTON, 110, 62, 100, 25, _("Hotmetal") );
    fl_set_object_callback( obj, cb_gradient_type, 5 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_turbo = obj = fl_add_checkbutton( FL_RADIO_BUTTON, 210, 37, 80, 25, _("Turbo") );
    fl_set_object_callback( obj, cb_gradient_type, 6 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_fire = obj = fl_add_checkbutton( FL_RADIO_BUTTON, 210, 62, 80, 25, _("Fire") );
    fl_set_object_callback( obj, cb_gradient_type, 7 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_random = obj = fl_add_checkbutton( FL_RADIO_BUTTON, 110, 87, 100, 25, _("Random") );
    fl_set_object_callback( obj, cb_gradient_type, 9 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_diverging = obj = fl_add_checkbutton( FL_RADIO_BUTTON, 0, 112, 110, 25, _("Diverging") );
    fl_set_object_callback( obj, cb_gradient_type, 8 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_mpl = obj = fl_add_checkbutton( FL_RADIO_BUTTON, 110, 112, 75, 25, _("MPL") );
    fl_set_object_callback( obj, cb_gradient_type, 10 );
    fl_set_button_mouse_buttons( obj, 1 );

    fl_end_group( );

    fdui->ch_grad_mpl = obj = fl_add_choice( FL_NORMAL_CHOICE2, 185, 112, 100, 25, "" );
    fl_set_object_callback( obj, cb_gradient_type, 10 );
    fl_set_choice_align( obj, FL_ALIGN_LEFT );
    fl_addto_choice( obj, _("Magma") );
    fl_addto_choice( obj, _("Inferno") );
    fl_addto_choice( obj, _("Plasma") );
    fl_addto_choice( obj, _("Viridis") );
    fl_addto_choice( obj, _("Parula") );
    fl_set_choice( obj, 1 );

    fl_end_form( );

    fdui->col_grad->fdui = fdui;

    return fdui;
}

/***************************************
 ***************************************/

FD_col_predef *
create_form_col_predef( void )
{
    FL_OBJECT *obj;
    FD_col_predef *fdui = ( FD_col_predef * ) fl_malloc( sizeof *fdui );

    fdui->vdata = fdui->cdata = NULL;
    fdui->ldata = 0;

    fdui->col_predef = fl_bgn_form( FL_NO_BOX, 290, 110 );

    fdui->box = obj = fl_add_box( FL_FLAT_BOX, 0, 0, 290, 110, "" );

    fdui->browser = obj = fl_add_browser( FL_HOLD_BROWSER, 5, 5, 279, 100, "" );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_col_predef, 0 );
    fl_set_browser_hscrollbar( obj, FL_OFF );

    fl_end_form( );

    fdui->col_predef->fdui = fdui;

    return fdui;
}

/***************************************
 ***************************************/

FD_col_table *
create_form_col_table( void )
{
    FL_OBJECT *obj;
    FD_col_table *fdui = ( FD_col_table * ) fl_malloc( sizeof *fdui );

    fdui->vdata = fdui->cdata = NULL;
    fdui->ldata = 0;

    fdui->col_table = fl_bgn_form( FL_NO_BOX, 290, 355 );

    fdui->box = obj = fl_add_box( FL_FLAT_BOX, 0, 0, 290, 355, "" );

    fdui->tab_canvas = obj = fl_add_canvas( FL_NORMAL_CANVAS, 3, 5, 208, 208, "" );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    obj = fl_add_button( FL_NORMAL_BUTTON, 90, 295, 95, 25, _("Load LUT") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_lutfile, 0 );

    fdui->btn_lutfile = obj = fl_add_checkbutton( FL_PUSH_BUTTON, 0, 295, 90, 25, _("Use LUT") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_lutfile_toggle, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->txt_file = obj = fl_add_text( FL_NORMAL_TEXT, 90, 325, 195, 25, "" );
    fl_set_object_boxtype( obj, FL_FRAME_BOX );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->grp_txt_rgb = fl_bgn_group( );

    fdui->txt_red = obj = fl_add_text( FL_NORMAL_TEXT, 216, 130, 69, 25, "" );
    fl_set_object_boxtype( obj, FL_EMBOSSED_BOX );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->txt_green = obj = fl_add_text( FL_NORMAL_TEXT, 216, 160, 69, 25, "" );
    fl_set_object_boxtype( obj, FL_EMBOSSED_BOX );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->txt_blue = obj = fl_add_text( FL_NORMAL_TEXT, 216, 190, 69, 25, "" );
    fl_set_object_boxtype( obj, FL_EMBOSSED_BOX );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fl_end_group( );

    fdui->txt_interval = obj = fl_add_text( FL_NORMAL_TEXT, 90, 235, 195, 25, "" );
    fl_set_object_boxtype( obj, FL_FRAME_BOX );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    fdui->menu_mod = obj = fl_add_menu( FL_PULLDOWN_MENU, 214, 5, 74, 25, _("Modify") );
    fl_set_object_boxtype( obj, FL_SHADOW_BOX );
    fl_set_object_color( obj, FL_COL1, FL_COL1 );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_LEFT | FL_ALIGN_INSIDE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_pal_modify, 0 );
    fl_show_menu_symbol( obj, 1 );
    fl_addto_menu( obj, _("Invert") );
    fl_addto_menu( obj, _("Reflect") );
    fl_addto_menu( obj, _("Highlight") );

    fdui->menu_sel = obj = fl_add_menu( FL_PULLDOWN_MENU, 214, 35, 74, 25, _("Select") );
    fl_set_object_boxtype( obj, FL_SHADOW_BOX );
    fl_set_object_color( obj, FL_COL1, FL_COL1 );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_LEFT | FL_ALIGN_INSIDE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_pal_select, 0 );
    fl_show_menu_symbol( obj, 1 );
    fl_addto_menu( obj, _("Only") );
    fl_addto_menu( obj, _("Add") );
    fl_addto_menu( obj, _("Remove") );

    fdui->txt_occup = obj = fl_add_text( FL_NORMAL_TEXT, 90, 265, 115, 25, "" );
    fl_set_object_boxtype( obj, FL_FRAME_BOX );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    obj = fl_add_button( FL_NORMAL_BUTTON, 190, 295, 95, 25, _("Save LUT") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_lutfile, 1 );

    obj = fl_add_box( FL_NO_BOX, 0, 235, 90, 25, _("Interval:") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    obj = fl_add_box( FL_NO_BOX, 0, 265, 90, 25, _("Occupancy:") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    obj = fl_add_box( FL_NO_BOX, 0, 325, 90, 25, _("LUT name:") );
    fl_set_object_lalign( obj, FL_ALIGN_RIGHT | FL_ALIGN_INSIDE );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    obj = fl_add_box( FL_NO_BOX, 220, 110, 65, 20, _("RGB") );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    obj = fl_add_box( FL_NO_BOX, 5, 215, 210, 15, _("Double click or enter to pick color") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lstyle( obj, FL_ITALIC_STYLE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );

    obj = fl_add_button( FL_NORMAL_BUTTON, 214, 65, 74, 25, _("Edit as ramps") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_pal_ramps, 0 );

    fdui->btn_occup = obj = fl_add_button( FL_NORMAL_BUTTON, 210, 265, 75, 25, _("Compute") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_resize( obj, FL_RESIZE_NONE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_occup_compute, 0 );

    fl_end_form( );

    fdui->col_table->fdui = fdui;

    return fdui;
}

/***************************************
 ***************************************/

FD_col_ramp *
create_form_col_ramp( void )
{
    FL_OBJECT *obj;
    FD_col_ramp *fdui = ( FD_col_ramp * ) fl_malloc( sizeof *fdui );

    fdui->vdata = fdui->cdata = NULL;
    fdui->ldata = 0;

    fdui->col_ramp = fl_bgn_form( FL_NO_BOX, 290, 395 );

    fdui->box = obj = fl_add_box( FL_FLAT_BOX, 0, 0, 290, 395, "" );

    obj = fl_add_text( FL_NORMAL_TEXT, 5, 104, 10, 15, _("0") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );

    fdui->plot_red = obj = fl_add_xyplot( FL_ACTIVE_XYPLOT, 15, 30, 270, 90, "" );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );
    fl_set_object_callback( obj, cb_ramp_update, 0 );
    fl_set_object_return( obj, FL_RETURN_CHANGED );

    fdui->plot_green = obj = fl_add_xyplot( FL_ACTIVE_XYPLOT, 15, 150, 270, 90, "" );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );
    fl_set_object_callback( obj, cb_ramp_update, 1 );
    fl_set_object_return( obj, FL_RETURN_CHANGED );

    fdui->plot_blue = obj = fl_add_xyplot( FL_ACTIVE_XYPLOT, 15, 270, 270, 90, "" );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );
    fl_set_object_callback( obj, cb_ramp_update, 2 );
    fl_set_object_return( obj, FL_RETURN_CHANGED );

    fdui->btn_red_plus = obj = fl_add_button( FL_PUSH_BUTTON, 85, 5, 20, 20, _("+") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_pm, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_red_minus = obj = fl_add_button( FL_PUSH_BUTTON, 110, 5, 20, 20, _("-") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_pm, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_red_smooth = obj = fl_add_button( FL_PUSH_BUTTON, 135, 5, 45, 20, _("Smooth") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_smooth, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_green_plus = obj = fl_add_button( FL_PUSH_BUTTON, 85, 125, 20, 20, _("+") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_pm, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_green_minus = obj = fl_add_button( FL_PUSH_BUTTON, 110, 125, 20, 20, _("-") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_pm, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_green_smooth = obj = fl_add_button( FL_PUSH_BUTTON, 135, 125, 45, 20, _("Smooth") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_smooth, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_blue_plus = obj = fl_add_button( FL_PUSH_BUTTON, 85, 245, 20, 20, _("+") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_pm, 2 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_blue_minus = obj = fl_add_button( FL_PUSH_BUTTON, 110, 245, 20, 20, _("-") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_pm, 2 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_blue_smooth = obj = fl_add_button( FL_PUSH_BUTTON, 135, 245, 45, 20, _("Smooth") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_smooth, 2 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->menu_preset = obj = fl_add_menu( FL_PULLDOWN_MENU, 5, 365, 110, 25, _("Preset") );
    fl_set_object_boxtype( obj, FL_SHADOW_BOX );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lalign( obj, FL_ALIGN_LEFT | FL_ALIGN_INSIDE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_preset, 0 );
    fl_show_menu_symbol( obj, 1 );

    obj = fl_add_text( FL_NORMAL_TEXT, 5, 32, 10, 15, _("1") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );

    obj = fl_add_text( FL_NORMAL_TEXT, 8, 29, 6, 6, _("-") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );

    obj = fl_add_text( FL_NORMAL_TEXT, 9, 115, 6, 6, _("-") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );

    obj = fl_add_text( FL_NORMAL_TEXT, 5, 224, 10, 15, _("0") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );

    obj = fl_add_text( FL_NORMAL_TEXT, 5, 152, 10, 15, _("1") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );

    obj = fl_add_text( FL_NORMAL_TEXT, 8, 149, 6, 6, _("-") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );

    obj = fl_add_text( FL_NORMAL_TEXT, 9, 235, 6, 6, _("-") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );

    obj = fl_add_text( FL_NORMAL_TEXT, 5, 344, 10, 15, _("0") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );

    obj = fl_add_text( FL_NORMAL_TEXT, 5, 272, 10, 15, _("1") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );

    obj = fl_add_text( FL_NORMAL_TEXT, 8, 269, 6, 6, _("-") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );

    obj = fl_add_text( FL_NORMAL_TEXT, 9, 355, 6, 6, _("-") );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_lstyle( obj, FL_FIXED_STYLE );

    fdui->btn_red = obj = fl_add_lightbutton( FL_PUSH_BUTTON, 5, 5, 75, 20, _("Red") );
    fl_set_object_boxtype( obj, FL_FLAT_BOX );
    fl_set_object_color( obj, FL_COL1, FL_RED );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_update, 0 );
    fl_set_button( obj, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_green = obj = fl_add_lightbutton( FL_PUSH_BUTTON, 5, 125, 75, 20, _("Green") );
    fl_set_object_boxtype( obj, FL_FLAT_BOX );
    fl_set_object_color( obj, FL_COL1, FL_GREEN );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_update, 1 );
    fl_set_button( obj, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_blue = obj = fl_add_lightbutton( FL_PUSH_BUTTON, 5, 245, 75, 20, _("Blue") );
    fl_set_object_boxtype( obj, FL_FLAT_BOX );
    fl_set_object_color( obj, FL_COL1, FL_BLUE );
    fl_set_object_lalign( obj, FL_ALIGN_CENTER );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_update, 2 );
    fl_set_button( obj, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_load = obj = fl_add_button( FL_NORMAL_BUTTON, 120, 365, 80, 25, _("Load") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_io, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_red_clear = obj = fl_add_button( FL_NORMAL_BUTTON, 240, 5, 45, 20, _("Clear") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_clear, 0 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_green_clear = obj = fl_add_button( FL_NORMAL_BUTTON, 240, 125, 45, 20, _("Clear") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_clear, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->btn_blue_clear = obj = fl_add_button( FL_NORMAL_BUTTON, 240, 245, 45, 20, _("Clear") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_clear, 2 );
    fl_set_button_mouse_buttons( obj, 1 );

    fdui->menu_red_copy = obj = fl_add_menu( FL_PULLDOWN_MENU, 185, 5, 50, 20, _("Copy") );
    fl_set_object_boxtype( obj, FL_SHADOW_BOX );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_LEFT | FL_ALIGN_INSIDE );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_copy, 0 );
    fl_show_menu_symbol( obj, 1 );
    fl_addto_menu( obj, _("Green") );
    fl_addto_menu( obj, _("Blue") );

    fdui->menu_green_copy = obj = fl_add_menu( FL_PULLDOWN_MENU, 185, 125, 50, 20, _("Copy") );
    fl_set_object_boxtype( obj, FL_SHADOW_BOX );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_LEFT | FL_ALIGN_INSIDE );
    fl_set_object_callback( obj, cb_ramp_copy, 1 );
    fl_show_menu_symbol( obj, 1 );
    fl_addto_menu( obj, _("Red") );
    fl_addto_menu( obj, _("Blue") );

    fdui->menu_blue_copy = obj = fl_add_menu( FL_PULLDOWN_MENU, 185, 245, 50, 20, _("Copy") );
    fl_set_object_boxtype( obj, FL_SHADOW_BOX );
    fl_set_object_lsize( obj, -8 );
    fl_set_object_lalign( obj, FL_ALIGN_LEFT | FL_ALIGN_INSIDE );
    fl_set_object_callback( obj, cb_ramp_copy, 2 );
    fl_show_menu_symbol( obj, 1 );
    fl_addto_menu( obj, _("Red") );
    fl_addto_menu( obj, _("Green") );

    fdui->btn_save = obj = fl_add_button( FL_NORMAL_BUTTON, 205, 365, 80, 25, _("Save") );
    fl_set_object_color( obj, FL_COL1, FL_YELLOW );
    fl_set_object_gravity( obj, FL_NorthWest, FL_NoGravity );
    fl_set_object_callback( obj, cb_ramp_io, 1 );
    fl_set_button_mouse_buttons( obj, 1 );

    fl_end_form( );

    fdui->col_ramp->fdui = fdui;

    return fdui;
}
