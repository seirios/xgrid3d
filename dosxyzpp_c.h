#ifndef XG3D_dosxyzpp_c_h_
#define XG3D_dosxyzpp_c_h_

#define stringMACROproto(name) void dosxyzpp_set##name(dosxyzpp*,char*)

#define stringMACROdefine(name) \
    void dosxyzpp_set##name(dosxyzpp *x, char *string) { \
        x->set##name(string); \
    }

#define intMACROproto(name) int dosxyzpp_##name(dosxyzpp*)

#define intMACROdefine(name) \
    int dosxyzpp_##name(dosxyzpp *x) { \
        return x->name(); \
    }

#define voidMACROproto(name) void dosxyzpp_set##name(dosxyzpp*)

#define voidMACROdefine(name) \
    void dosxyzpp_set##name(dosxyzpp *x) { \
        x->set##name(); \
    }

#ifdef __cplusplus
extern "C" {
#endif

    typedef struct DOSXYZpp_Application dosxyzpp;

    dosxyzpp* dosxyzpp_new();
    void dosxyzpp_delete(dosxyzpp*);

    stringMACROproto(Input);
    stringMACROproto(HenHouse);
    stringMACROproto(EgsHome);
    stringMACROproto(PegsFile);
    stringMACROproto(InputFile);
    stringMACROproto(OutputFile);
    stringMACROproto(AppName);
    stringMACROproto(AppDir);
    stringMACROproto(RunDir);

    const char* dosxyzpp_getRunDir(dosxyzpp*);

    void dosxyzpp_setParallel(dosxyzpp*,long,long);
    voidMACROproto(Batch);
    voidMACROproto(Quiet);
    voidMACROproto(SimpleRun);

    intMACROproto(initGeom);
    intMACROproto(initSimulation);
    intMACROproto(runSimulation);
    intMACROproto(finishSimulation);

    double dosxyzpp_getEtot(dosxyzpp*);
    double dosxyzpp_getMass(dosxyzpp*,int);

    void dosxyzpp_getDoseScore(dosxyzpp*,int,double*,double*);

    int dosxyzpp_getNRegions(dosxyzpp*);
    long dosxyzpp_getNCase(dosxyzpp*);

#ifdef __cplusplus
}
#endif

#endif
