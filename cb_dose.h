#ifndef XG3D_dose_cb_h_
#define XG3D_dose_cb_h_

#include "xgrid3d.h"
#include "fd_dose.h"

typedef struct {
    grid3d *electron; /* electron kernel data */
    grid3d *photon;   /* photon kernel data */
    grid3d *beta;     /* beta kernel data */
    char *nuclide;    /* kernel nuclide name */
    char *medium;     /* kernel medium name */
    size_t dim;       /* kernel side size */
    double vox;       /* kernel side length */
} xg3d_kernel_data;

#define KERNEL_DATA(fd_dose) ((xg3d_kernel_data*)(((FD_dose*)(fd_dose))->vdata))
#define KERNEL_ELECTRON(fd_dose) (KERNEL_DATA(fd_dose)->electron)
#define KERNEL_PHOTON(fd_dose) (KERNEL_DATA(fd_dose)->photon)
#define KERNEL_BETA(fd_dose) (KERNEL_DATA(fd_dose)->beta)
#define KERNEL_NUCLIDE(fd_dose) (KERNEL_DATA(fd_dose)->nuclide)
#define KERNEL_MEDIUM(fd_dose) (KERNEL_DATA(fd_dose)->medium)
#define KERNEL_DIM(fd_dose) (KERNEL_DATA(fd_dose)->dim)
#define KERNEL_VOX(fd_dose) (KERNEL_DATA(fd_dose)->vox)

void update_dose_info(xg3d_env*);
void update_ker_list(FD_dose*,char**);
void update_ker_voxcolor(FD_dose*,const xg3d_header*);
void kernel_data_clear(xg3d_kernel_data*,const bool);

void setup_dose(xg3d_env*,xg3d_data*);
void setup_dose_err(xg3d_env*,xg3d_data*);

#endif
