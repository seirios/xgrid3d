#ifndef XG3D_function_cb_h_
#define XG3D_function_cb_h_

#include "fd_function.h"

enum fun_var_type {
    VAR_DATA,
    VAR_PARAM
};

struct fun_var_data {
    const xg3d_data *data;
};

struct fun_var_param {
    double value;
    double left;
    double right;
};

union fun_var_value {
    struct fun_var_data data;
    struct fun_var_param param;
};

struct fun_var {
    char* symbol;
    enum fun_var_type type;
    union fun_var_value value;
};

typedef struct {
    size_t nvar;
    char *string;
    struct fun_var **vars;
    xg3d_data *preview_data;
} xg3d_function_workspace;

#define FUNCTION_WORKSPACE(fd_function) ((xg3d_function_workspace*)(((FD_function*)(fd_function))->vdata))
#define FUNCTION_NVAR(fd_function) (FUNCTION_WORKSPACE(fd_function)->nvar)
#define FUNCTION_VARS(fd_function) ((struct fun_var**)(FUNCTION_WORKSPACE(fd_function)->vars))
#define FUNCTION_STRING(fd_function) (FUNCTION_WORKSPACE(fd_function)->string)
#define FUNCTION_PREVIEW_DATA(fd_function) (FUNCTION_WORKSPACE(fd_function)->preview_data)

void function_workspace_clear(xg3d_function_workspace*,const bool);

#endif
