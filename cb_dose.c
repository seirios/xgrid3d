#include <libgen.h>

#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_roi.h"
#include "grid3d_fftw.h"
#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "cb_dialog.h"
#include "cb_datasets.h"
#include "cb_dose.h"
#include "cb_viewport.h"
#include "cb_overview.h"
#include "cb_contour.h"
#include "fd_dvhs.h"

#include "util.h"
#include "imgtor.h"
#include "util_units.h"

void kernel_data_clear(xg3d_kernel_data *x, const bool delete) {
    if(x != NULL) {
        g3d_free(x->electron); x->electron = NULL;
        g3d_free(x->photon); x->photon = NULL;
        g3d_free(x->beta); x->beta = NULL;
        free(x->nuclide); x->nuclide = NULL;
        free(x->medium); x->medium = NULL;
        x->dim = 0; x->vox = 0.0;
        if(delete) free(x);
    }
}

void update_ker_voxcolor(FD_dose *dose, const xg3d_header *hdr) {
	FL_OBJECT *txt_kervox = dose->txt_kervox;
    const double kernel_vox = KERNEL_VOX(dose);

    fl_set_object_lcolor(txt_kervox,
            (kernel_vox == hdr->vx && kernel_vox == hdr->vy && kernel_vox == hdr->vz) ?
            FL_BLACK : FL_RED);
}

void update_ker_list(FD_dose *dose, char **kernel_dir) {
	char *env;

    *kernel_dir = (env = getenv("XG3D_KERNEL_DIR")) ? strdup(env) : strdup("/usr/share/xgrid3d/kernel");
	fl_clear_choice(dose->ch_kernel);
    int nf;
	const FL_Dirlist *list = fl_get_dirlist(*kernel_dir,"*.kernel",&nf,0);
    if(NULL != list) {
        for(int i=0;i<nf;i++) {
            if(list[i].type == FT_FILE) {
                char *file = strdup(list[i].name);
                char *p = strstr(file,".kernel"); *p = '\0';
                fl_addto_choice(dose->ch_kernel,file);
                free(file);
            }
        }
        fl_free_dirlist(list);
    }
	fl_set_choice(dose->ch_kernel,0);
	fl_call_object_callback(dose->ch_kernel);
}

static double _calibrate(const double x, const void *p) {
	const double *calib = (double*)p;
	return calib[0] + calib[1] * x;
}

static int _load_ker_file(xg3d_kernel_data *kerdata, const char *file) {
	char path[PATH_MAX];
	double dval;
	char *tval;
	int ival;
	FILE *fp;

	snprintf(path,sizeof(path),"%s/%s.kernel",kernel_dir,file);
	fp = fopen(path,"r");
    if(fp == NULL) return 1;
    if(grep_mupet(fp,"nuclide",&tval)) {
        kerdata->nuclide = strdup(tval);
        free(tval);
    } else { fclose(fp); return 1; }
    if(grep_mupet(fp,"medium",&tval)) {
        kerdata->medium = strdup(tval);
        free(tval);
    } else { fclose(fp); return 1; }
    if(grep_mupet(fp,"dim",&tval)) {
        ival = strtol(tval,NULL,10);
        kerdata->dim = (unsigned int)ival;
        free(tval);
    } else { fclose(fp); return 1; }
    if(grep_mupet(fp,"vox",&tval)) {
        dval = strtod(tval,NULL);
        kerdata->vox = dval;
        free(tval);
    } else { fclose(fp); return 1; }
    if(grep_mupet(fp,"electron",&tval)) {
        if(strcmp(tval,"yes") == 0) {
            snprintf(path,sizeof(path),"%s/%s_electron.img.gz",kernel_dir,file);
            /* double-precision, gzip-compressed, little-endian */
            g3d_input_raw(path,kerdata->dim,kerdata->dim,kerdata->dim,RAW_FLOAT64,0,COMPR_GZIP,ENDIAN_LITTLE,&kerdata->electron);
            if(kerdata->electron == NULL) { fclose(fp); return 1; }
        }
        free(tval);
    }
    if(grep_mupet(fp,"photon",&tval)) {
        if(strcmp(tval,"yes") == 0) {
            snprintf(path,sizeof(path),"%s/%s_photon.img.gz",kernel_dir,file);
            /* double-precision, gzip-compressed, little-endian */
            g3d_input_raw(path,kerdata->dim,kerdata->dim,kerdata->dim,RAW_FLOAT64,0,COMPR_GZIP,ENDIAN_LITTLE,&kerdata->photon);
            if(kerdata->photon == NULL) { fclose(fp); return 1; }
        }
        free(tval);
    }
    if(grep_mupet(fp,"beta",&tval)) {
        if(strcmp(tval,"yes") == 0) {
            snprintf(path,sizeof(path),"%s/%s_beta.img.gz",kernel_dir,file);
            /* double-precision, gzip-compressed, little-endian */
            g3d_input_raw(path,kerdata->dim,kerdata->dim,kerdata->dim,RAW_FLOAT64,0,COMPR_GZIP,ENDIAN_LITTLE,&kerdata->beta);
            if(kerdata->beta == NULL) { fclose(fp); return 1; }
        }
        free(tval);
    }
    if(kerdata->electron == NULL && kerdata->photon == NULL && kerdata->beta == NULL) { fclose(fp); return 1; }
    fclose(fp);
    return 0;
}

#define OVERLAY_BETA 1
#define OVERLAY_ELECTRON 2
#define OVERLAY_PHOTON 3
void cb_dose_kernel ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_kernel_data *kerdata = KERNEL_DATA(GUI->dose);
    const double unit = fl_get_button(GUI->dose->btn_kervoxu) ? CM_TO_MM : 1.0;
    const char *file = fl_get_choice_text(GUI->dose->ch_kernel);
    float *xx = NULL,*yy = NULL;
    size_t ndist;

    fl_clear_xyplot(GUI->dose->plot_ker);
    kernel_data_clear(kerdata,false);
    deactivate_obj(GUI->dose->btn_kerelec); fl_set_button(GUI->dose->btn_kerelec,0);
    deactivate_obj(GUI->dose->btn_kerphot); fl_set_button(GUI->dose->btn_kerphot,0);
    deactivate_obj(GUI->dose->btn_kerbeta); fl_set_button(GUI->dose->btn_kerbeta,0);
    fl_set_object_label(GUI->dose->txt_kernuc,"");
    fl_set_object_label(GUI->dose->txt_kermed,"");
    fl_set_object_label(GUI->dose->txt_kerdim,"");
    fl_set_object_label(GUI->dose->txt_kervox,"");
    deactivate_obj(GUI->dose->btn_kervoxu);

    if(file == NULL) return;

    fl_show_oneliner(_("Loading kernel..."),fl_scrw/2,fl_scrh/2);
    if(_load_ker_file(kerdata,file)) {
        fl_hide_oneliner();
        fl_show_alert2(0,_("Failed loading kernel!\f%s"),file);
        return;
    }

    activate_obj(GUI->dose->btn_kervoxu,FL_COL1);

    if(kerdata->beta != NULL) {
        activate_obj(GUI->dose->btn_kerbeta,FL_RED);
        fl_set_button(GUI->dose->btn_kerbeta,1);
        double *r,*dose;
        imgtor(kerdata->beta->dat,kerdata->dim,kerdata->vox,&r,&dose,&ndist);
        xx = malloc(ndist*sizeof(float)); yy = malloc(ndist*sizeof(float));
        for(size_t i=0;i<ndist;i++) { xx[i] = r[i]; yy[i] = dose[i]; }
        free(r); free(dose);
        fl_add_xyplot_overlay(GUI->dose->plot_ker,OVERLAY_BETA,xx,yy,ndist,FL_RED);
        fl_set_xyplot_overlay_type(GUI->dose->plot_ker,OVERLAY_BETA,FL_EMPTY_XYPLOT);
    }

    if(kerdata->electron != NULL) {
        activate_obj(GUI->dose->btn_kerelec,FL_BLUE);
        fl_set_button(GUI->dose->btn_kerelec,1);
        double *r,*dose;
        imgtor(kerdata->electron->dat,kerdata->dim,kerdata->vox,&r,&dose,&ndist);
        if(xx != NULL && yy != NULL) {
            float *y = malloc(ndist*sizeof(float));
            for(size_t i=0;i<ndist;i++) { y[i] = dose[i]; yy[i] += y[i]; }
            fl_add_xyplot_overlay(GUI->dose->plot_ker,OVERLAY_ELECTRON,xx,y,ndist,FL_BLUE);
            free(y);
        } else {
            xx = malloc(ndist*sizeof(float)); yy = malloc(ndist*sizeof(float));
            for(size_t i=0;i<ndist;i++) { xx[i] = r[i]; yy[i] = dose[i]; }
            fl_add_xyplot_overlay(GUI->dose->plot_ker,OVERLAY_ELECTRON,xx,yy,ndist,FL_BLUE);
        }
        free(r); free(dose);
        fl_set_xyplot_overlay_type(GUI->dose->plot_ker,OVERLAY_ELECTRON,FL_EMPTY_XYPLOT);
    }

    if(kerdata->photon != NULL) {
        activate_obj(GUI->dose->btn_kerphot,FL_YELLOW);
        fl_set_button(GUI->dose->btn_kerphot,1);
        double *r,*dose;
        imgtor(kerdata->photon->dat,kerdata->dim,kerdata->vox,&r,&dose,&ndist);
        if(xx != NULL && yy != NULL) {
            float *y = malloc(ndist*sizeof(float));
            for(size_t i=0;i<ndist;i++) {
                y[i] = dose[i];
                yy[i] += y[i];
            }
            fl_add_xyplot_overlay(GUI->dose->plot_ker,OVERLAY_PHOTON,xx,y,ndist,FL_YELLOW);
            free(y);
        } else {
            xx = malloc(ndist*sizeof(float)); yy = malloc(ndist*sizeof(float));
            for(size_t i=0;i<ndist;i++) { xx[i] = r[i]; yy[i] = dose[i]; }
            fl_add_xyplot_overlay(GUI->dose->plot_ker,OVERLAY_PHOTON,xx,yy,ndist,FL_YELLOW);
        }
        free(r); free(dose);
        fl_set_xyplot_overlay_type(GUI->dose->plot_ker,OVERLAY_PHOTON,FL_EMPTY_XYPLOT);
    }

    fl_set_xyplot_data(GUI->dose->plot_ker,xx,yy,ndist,"",_("r [cm]"),_("Dose Gy/Bq.s"));
    free(xx); free(yy);

    update_ker_voxcolor(GUI->dose,DATA->hdr);
    fl_set_object_label_f(GUI->dose->txt_kernuc,"%s",kerdata->nuclide);
    fl_set_object_label_f(GUI->dose->txt_kermed,"%s",kerdata->medium);
    fl_set_object_label_f(GUI->dose->txt_kerdim,"%zu",kerdata->dim);
    fl_set_object_label_f(GUI->dose->txt_kervox,"%g",kerdata->vox*unit);
    fl_hide_oneliner();
}

void cb_dose_ker_btn ( FL_OBJECT * obj  , long data  ) {
    const int overlay_type = fl_get_button(obj) ? FL_EMPTY_XYPLOT : FL_NORMAL_XYPLOT;
    fl_set_xyplot_overlay_type(GUI->dose->plot_ker,data,overlay_type);
}

void cb_dose_ker_export ( FL_OBJECT * obj  , long data  ) {
	switch(data) {
        case 0: { /* Kernel data */
            const char *file = fl_show_fselector(_("Export kernel data to..."),"","*.txt","");
            if(file == NULL) return;
            FILE *fp = fopen(file,"w");
            if(fp == NULL) {
                fl_show_alert2(0,_("Cannot open file for writing!\f%s"),file);
                return;
            }
            int n;
            float *x,*y[4] = {NULL,NULL,NULL,NULL};
            fl_get_xyplot_data_pointer(GUI->dose->plot_ker,0,&x,&y[0],&n);
            if(KERNEL_BETA(GUI->dose) != NULL)
                fl_get_xyplot_data_pointer(GUI->dose->plot_ker,OVERLAY_BETA,&x,&y[OVERLAY_BETA],&n);
            if(KERNEL_ELECTRON(GUI->dose) != NULL)
                fl_get_xyplot_data_pointer(GUI->dose->plot_ker,OVERLAY_ELECTRON,&x,&y[OVERLAY_ELECTRON],&n);
            if(KERNEL_PHOTON(GUI->dose) != NULL)
                fl_get_xyplot_data_pointer(GUI->dose->plot_ker,OVERLAY_PHOTON,&x,&y[OVERLAY_PHOTON],&n);
            fprintf(fp,"#%10.10s\t%10.10s\t%10.10s\t%10.10s\t%10.10s\n","R (cm)","TOTAL","BETA","ELECTRON","PHOTON");
            for(int i=0;i<n;i++)
                fprintf(fp,"%10.7g\t%10.7g\t%10.7g\t%10.7g\t%10.7g\n",x[i],y[0][i],
                        y[OVERLAY_BETA] != NULL ? y[OVERLAY_BETA][i] : 0.0,
                        y[OVERLAY_ELECTRON] != NULL ? y[OVERLAY_ELECTRON][i] : 0.0,
                        y[OVERLAY_PHOTON] != NULL ? y[OVERLAY_PHOTON][i] : 0.0);
            fclose(fp);
            } break;
		case 1: { /* Kernel plot */
			FLPS_CONTROL *flpsc = flps_init();
			flpsc->orientation = FLPS_LANDSCAPE;
			flpsc->eps = 1;
			fl_object_ps_dump(GUI->dose->plot_ker,NULL);
            } break;
	}
}
#undef OVERLAY_ELECTRON
#undef OVERLAY_PHOTON
#undef OVERLAY_BETA

void cb_kervox_unit ( FL_OBJECT * obj  , long data  ) {
    (void)data;
	const double unit = fl_get_button(obj) ? CM_TO_MM : 1.0;

    fl_set_object_label(obj,fl_get_button(obj) ? "mm" : "cm");
	fl_set_object_label_f(GUI->dose->txt_kervox,"%g",KERNEL_VOX(GUI->dose)*unit);
}

#undef ENV
void update_dose_info(xg3d_env *ENV) {
    char *base;

    if(ENV->data->dose != NULL) {
        base = strdup(ENV->data->dose->hdr->file);
        fl_set_object_label(GUI->dose->txt_dose,base); free(base);
        fl_set_object_label_f(GUI->dose->txt_min,"%.8e Gy",
                ENV->data->dose->stats->min);
        fl_set_object_label_f(GUI->dose->txt_max,"%.8e Gy",
                ENV->data->dose->stats->max);
        fl_set_object_label_f(GUI->dose->txt_avg,"%.8e Gy",
                ENV->data->dose->stats->sum/ENV->data->dose->g3d->nvox);
    } else {
        fl_set_object_label(GUI->dose->txt_dose,_("NONE"));
        fl_set_object_label(GUI->dose->txt_min,"");
        fl_set_object_label(GUI->dose->txt_max,"");
        fl_set_object_label(GUI->dose->txt_avg,"");
    }
    if(ENV->data->dose_err != NULL) {
        base = strdup(ENV->data->dose_err->hdr->file);
        fl_set_object_label(GUI->dose->txt_dose_err,base); free(base);
        fl_set_object_label_f(GUI->dose->txt_errmin,"%.8e Gy",
                ENV->data->dose_err->stats->min);
        fl_set_object_label_f(GUI->dose->txt_errmax,"%.8e Gy",
                ENV->data->dose_err->stats->max);
        fl_set_object_label_f(GUI->dose->txt_erravg,"%.8e Gy",
                ENV->data->dose_err->stats->sum/ENV->data->dose_err->g3d->nvox);
    } else {
        fl_set_object_label(GUI->dose->txt_dose_err,_("NONE"));
        fl_set_object_label(GUI->dose->txt_errmin,"");
        fl_set_object_label(GUI->dose->txt_errmax,"");
        fl_set_object_label(GUI->dose->txt_erravg,"");
    }
}
#define ENV _default_env

#undef ENV
void setup_dose(xg3d_env *ENV, xg3d_data *dose) {
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);

    if(dose != NULL) {
        if(memcmp(dose->hdr->dim,DATA->hdr->dim,3 * sizeof(coord_t)) != 0) {
            fl_show_alert2(0,_("Cannot assign dataset!\fDimensions mismatch."));
            return;
        }
        ENV->data->dose = dose;
        VIEW_DATA_LAYER(ov_viewports[PLANE_XY],LAYER_DOSE) = dose;
        VIEW_DATA_LAYER(ov_viewports[PLANE_XZ],LAYER_DOSE) = dose;
        VIEW_DATA_LAYER(ov_viewports[PLANE_ZY],LAYER_DOSE) = dose;
    } else {
        ENV->data->dose = NULL;
        VIEW_DATA_LAYER(ov_viewports[PLANE_XY],LAYER_DOSE) = NULL;
        VIEW_DATA_LAYER(ov_viewports[PLANE_XZ],LAYER_DOSE) = NULL;
        VIEW_DATA_LAYER(ov_viewports[PLANE_ZY],LAYER_DOSE) = NULL;
        fl_set_button(GUI->contour->btn_show,0);
    }
    replace_ov_slices(ov_viewports,LAYER_DOSE);
    update_dose_info(ENV);
}
#define ENV _default_env

#undef ENV
void setup_dose_err(xg3d_env *ENV, xg3d_data *dose_err) {
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);

    if(dose_err != NULL) {
        if(memcmp(dose_err->hdr->dim,DATA->hdr->dim,3*sizeof(coord_t)) != 0) {
            fl_show_alert2(0,_("Cannot assign dataset!\fDimensions mismatch."));
            return;
        }
        ENV->data->dose_err = dose_err;
        VIEW_DATA_LAYER(ov_viewports[PLANE_XY],LAYER_DOSE_ERR) = dose_err;
        VIEW_DATA_LAYER(ov_viewports[PLANE_XZ],LAYER_DOSE_ERR) = dose_err;
        VIEW_DATA_LAYER(ov_viewports[PLANE_ZY],LAYER_DOSE_ERR) = dose_err;
    } else {
        ENV->data->dose_err = NULL;
        VIEW_DATA_LAYER(ov_viewports[PLANE_XY],LAYER_DOSE_ERR) = NULL;
        VIEW_DATA_LAYER(ov_viewports[PLANE_XZ],LAYER_DOSE_ERR) = NULL;
        VIEW_DATA_LAYER(ov_viewports[PLANE_ZY],LAYER_DOSE_ERR) = NULL;
    }
    replace_ov_slices(ov_viewports,LAYER_DOSE_ERR);
    update_dose_info(ENV);
}
#define ENV _default_env

#define SET_ELEC fl_get_button(GUI->dose->btn_kerelec)
#define SET_PHOT fl_get_button(GUI->dose->btn_kerphot)
#define SET_BETA fl_get_button(GUI->dose->btn_kerbeta)
void cb_dose_compute ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    xg3d_kernel_data *kerdata = KERNEL_DATA(GUI->dose);
	const unsigned int ix = fl_get_browser(GUI->dose->br_source);
	const g3d_roi *roi = ix >= 1 ? DATA->rois[ix - 1]->roi : NULL;

    if(roi == NULL) {
		fl_show_alert2(0,_("No ROI selected!\fPlease select a source ROI."));
        return;
    }
    if(kerdata->beta == NULL && kerdata->electron == NULL && kerdata->photon == NULL) {
        fl_show_alert2(0,_("No kernel loaded!\fPlease load a kernel first."));
        return;
    }

	grid3d *ker = NULL;
	const grid3d *kers[3] = { NULL, NULL, NULL };
    if(SET_ELEC && SET_PHOT && SET_BETA) {
        kers[0] = kerdata->beta;
        kers[1] = kerdata->electron;
        kers[2] = kerdata->photon;
        ker = g3d_add(kers,G3D_FLOAT64,kers[0]->nx,kers[0]->ny,kers[0]->nz,3);
    } else if(SET_ELEC && SET_PHOT) {
        kers[0] = kerdata->electron;
        kers[1] = kerdata->photon;
        ker = g3d_add(kers,G3D_FLOAT64,kers[0]->nx,kers[0]->ny,kers[0]->nz,2);
    } else if(SET_ELEC && SET_BETA) {
        kers[0] = kerdata->beta;
        kers[1] = kerdata->electron;
        ker = g3d_add(kers,G3D_FLOAT64,kers[0]->nx,kers[0]->ny,kers[0]->nz,2);
    } else if(SET_PHOT && SET_BETA) {
        kers[0] = kerdata->beta;
        kers[1] = kerdata->photon;
        ker = g3d_add(kers,G3D_FLOAT64,kers[0]->nx,kers[0]->ny,kers[0]->nz,2);
    } else if(SET_ELEC)
        ker = g3d_dup(kerdata->electron);
    else if(SET_PHOT)
        ker = g3d_dup(kerdata->photon);
    else if(SET_BETA)
        ker = g3d_dup(kerdata->beta);
    else {
        fl_show_alert2(0,_("No emissions selected!\fPlease select at least one emission."));
        return;
    }

    if(ker == NULL) {
        fl_show_alert2(0,_("Falied summing kernel!\fPlease check kernel files."));
        return;
    }

    fl_show_oneliner(_("Computing dose estimate..."),fl_scrw/2,fl_scrh/2);
    grid3d *act = g3d_dup(DATA->g3d);
    g3d_roi_mask_dfill(act,roi,0.0);
    double calib_data[2] = {DATA->hdr->cal_inter, DATA->hdr->cal_slope};
    g3d_roi_dapply(act,roi,_calibrate,calib_data);

    xg3d_data *const dat_dose = calloc(1,sizeof(xg3d_data));
    dat_dose->hdr = xg3d_header_dup(DATA->hdr);
    dat_dose->hdr->cal_slope = 1.0;
    dat_dose->hdr->cal_inter = 0.0;
    free(dat_dose->hdr->file);
    dat_dose->hdr->file = strdup("Dose from kernel");
    dat_dose->g3d = g3d_convolve_fftw(act,ker,0.0,1); /* DVK convolution */
    g3d_free(act); g3d_free(ker);
    dat_dose->stats = g3d_stats_get(dat_dose->g3d,NULL,G3D_STATS_MINMAXSUM);

    xg3d_datasets_add(ENV->data,dat_dose);
    refresh_datasets_browser(GUI->datasets->browser,ENV->data);

    setup_dose(ENV,dat_dose);
    setup_dose_err(ENV,NULL);

    fl_hide_oneliner();
    fl_call_object_callback(GUI->dvhs->btn_clear);
    refresh_ov_canvases(GUI->overview);
}
#undef SET_ELEC
#undef SET_PHOT
#undef SET_BETA

void cb_dose_ker_reload ( FL_OBJECT * obj  , long data  ) {
    (void)data;
	free(kernel_dir);
    update_ker_list(GUI->dose,&kernel_dir);
}
