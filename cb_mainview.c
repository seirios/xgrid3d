#include <libgen.h>
#include <float.h>
#include <unistd.h>

#ifndef XG3D_h_
#define XG3D_h_
#define XUTIL_DEFINE_FUNCTIONS
#include <unistd.h>
#include <forms.h>
#include <flimage.h>
#include "grid3d.h"
#include "grid3d_io.h"
#include "grid3d_quant.h"
#include "grid3d_roi.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>
#include <assert.h>
#include <omp.h>
/* FD typedefs */
#include "xgrid3d_fd.h"
#include "gettext.h"
#ifdef ENABLE_NLS
#define _(String) gettext(String)
#define _p(Context,String) pgettext(Context,String)
#else
#define _(String) (String)
#define _p(Context,String) (String)
#endif
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%s:%d: " M "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__);
#define error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__);
#define fatal(M, ...) { error(M, __VA_ARGS__); exit(EXIT_FAILURE); }
#define HERE debug("HERE %d",0);
#define THREEWAY_IF(condA,resA,condB,resB,resC) ((condA) ? (resA) : ((condB) ? (resB) : (resC)))
#define XOR(opA,opB) (((opA) && !(opB)) || (!(opA) && (opB)))
#define UNIFONT        30
#define UNIFONT_BOLD   (UNIFONT + FL_BOLD_STYLE)
#define UNIFONT_ITALIC (UNIFONT + FL_ITALIC_STYLE)
#define SQR(x) ((x)*(x))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define ACTIVE_FOLDER(obj) \
    (fl_get_active_folder_number(obj) == 0 ? fl_get_folder_number(obj) : fl_get_active_folder_number(obj))
#define xg3d_settings_check(mask,flags) (((mask) & (flags)) == (flags))
#define XG3D_TITLE_FORMAT "xgrid3d<%d> - %s",getpid()
/* callbacks only! */
#define _default_env  ((xg3d_env*)(obj->form->u_vdata))
#define _default_gui  ((xg3d_gui*)(ENV->gui))
#define _default_sets ((xg3d_datasets*)(ENV->data))
#define _default_data ((xg3d_data*)(SETS->main))
/* environment access */
#define ENV  _default_env
#define GUI  _default_gui
#define SETS _default_sets
#define DATA _default_data


#define MAX_LAYERS 16
enum xg3d_layer {
    LAYER_BASE,
    LAYER_ERROR,
    LAYER_OVERLAY,
    LAYER_DOSE,
    LAYER_DOSE_ERR,
    LAYER_PREVIEW,
    LAYER_FREE,
    LAYER_MAX = MAX_LAYERS - 1
};
/* in same order as browser */
enum xg3d_attr {
    ATTR_MAIN = 1,
    ATTR_OVERLAY,
    ATTR_DOSE,
    ATTR_DOSE_ERROR,
    ATTR_SINOGRAM
};
enum xg3d_orientation {
    ORIENT_0,
    ORIENT_90,
    ORIENT_180,
    ORIENT_270,
    ORIENT_FLIP,
    ORIENT_F0   = ORIENT_FLIP | ORIENT_0,
    ORIENT_F90  = ORIENT_FLIP | ORIENT_90,
    ORIENT_F180 = ORIENT_FLIP | ORIENT_180,
    ORIENT_F270 = ORIENT_FLIP | ORIENT_270
};
enum xg3d_acq_mod {
    ACQ_UNKNOWN = -1,
    ACQ_PET = 0,
    ACQ_CT = 1,
    ACQ_SPECT = 2
};
enum xg3d_scope {
    SCOPE_SLICE = 0,
    SCOPE_GLOBAL = 1
};
/* in same order as folders */
enum xg3d_quant_mode {
    QUANT_INTERVALS = 1,
    QUANT_PREDEF,
    QUANT_CUSTOM
};
/* in same order as folders */
enum xg3d_quant_rng_type {
    QUANT_RNG_MINMAX = 1,
    QUANT_RNG_CENWID
};
/* in same order as folders */
enum xg3d_color_mode {
    COLOR_GRADIENT = 1,
    COLOR_PREDEF,
    COLOR_PALETTE,
    COLOR_RAMP
};
/* image formats */
enum xg3d_io_format {
    IO_RAW,
    IO_NIFTI,
    IO_MUPET,
    IO_INTERFILE,
    IO_NRRD
};
/* xg3d header */
typedef struct {
    char *file;                  /* data file path */
    enum g3d_raw_io_type type;   /* data type */
    coord_t dim[3];              /* dimensions */
    double vx;                   /* voxel size x (cm) */
    double vy;                   /* voxel size y (cm) */
    double vz;                   /* voxel size z (cm) */
    coord_t ox;                  /* origin voxel x */
    coord_t oy;                  /* origin voxel y */
    coord_t oz;                  /* origin voxel z */
    double cal_slope;            /* calibration slope */
    double cal_inter;            /* calibration intercept */
    enum xg3d_acq_mod acqmod;    /* acquisition modality */
    enum g3d_compr_type compr;   /* compression type */
    enum g3d_endian_type endian; /* endianness */
    off_t offset;                /* read offset */
    enum xg3d_io_format format;  /* image file format */
} xg3d_header;
/* xg3d ROI */
typedef struct {
    g3d_roi *roi;        /* list of voxels */
    double total;        /* total value in ROI */
    double min;          /* minimum value in ROI */
    double max;          /* maximum value in ROI */
    coord_t j,i,k;       /* ROI origin */
    coord_t dj,di,dk;    /* ROI extents */
    FL_PACKED color;     /* ROI color */
    gsl_histogram *hist; /* ROI histogram */
} xg3d_roi;
/* xg3d LUT */
#ifndef XG3D_STRUCT_XG3D_LUT
#define XG3D_STRUCT_XG3D_LUT
typedef struct {
    unsigned int ncol;    /* number of used colors */
    FL_PACKED colors[256];/* LUT */
    bool underflow;       /* underflow? */
    bool overflow;        /* overflow? */
} xg3d_lut;
#endif
/* xg3d frame */
typedef struct {
    char *file;   /* data file */
    uintmax_t t;  /* time of frame (seconds) */
    double *vals; /* values in ROI for this frame */
} xg3d_frame;
/* xg3d data */
typedef struct {
    char *name;       /* data name */
    grid3d *g3d;      /* data grid3d */
    xg3d_header *hdr; /* data header */
    g3d_stats *stats; /* data statistics */
    nroi_t nrois;     /* number of ROIs */
    xg3d_roi **rois;  /* ROI array */
} xg3d_data;
/* xg3d datasets */
typedef struct {
    unsigned int nsets;  /* number of datasets */
    xg3d_data **sets;    /* dataset array */
    xg3d_data *main;     /* current main dataset */
    xg3d_data *overlay;  /* current overlay dataset */
    xg3d_data *dose;     /* current dose dataset */
    xg3d_data *dose_err; /* current dose error dataset */
    xg3d_data *sino;     /* current sinogram dataset */
} xg3d_datasets;
/* Pointers to all GUI FD_* structs */
/* order matters, children must come before parent */
typedef struct {
    FD_calib_std *calib_std;
    FD_calibration *calibration;
    FD_col_grad *col_grad;
    FD_col_predef *col_predef;
    FD_col_table *col_table;
    FD_col_ramp *col_ramp;
    FD_coloring *coloring;
    FD_contour *contour;
    FD_ct *ct;
    FD_info *info_datasets;
    FD_datasets *datasets;
    FD_open_raw *open_raw;
    FD_open_hdr *open_hdr;
    FD_open_def *open_def;
    FD_opendlg *opendlg;
    FD_savedlg *savedlg;
    FD_newdlg *newdlg;
    FD_dose *dose;
    FD_dvhs *dvhs;
    FD_film *film;
    FD_fun_data *fun_data;
    FD_fun_param *fun_param;
    FD_function *function;
    FD_info *info;
    FD_kernel *kernel;
    FD_kinetics *kinetics;
    FD_measure *measure;
    FD_menu *menu;
    FD_operations *operations;
    FD_overlay *overlay;
    FD_quant_rng_minmax *quant_rng_minmax;
    FD_quant_rng_cenwid *quant_rng_cenwid;
    FD_quant_intervals *quant_intervals;
    FD_quant_predef *quant_predef;
    FD_quant_custom *quant_custom;
    FD_quantization *quantization;
    FD_rois *rois;
    FD_roi_trans *roi_trans;
    FD_roi_stats *roi_stats;
    FD_selection *selection;
    FD_surface *surface;
    FD_viewport *fun_view_xy;
    FD_viewport *fun_view_xz;
    FD_viewport *fun_view_zy;
    FD_overview *fun_preview;
    FD_viewport *view_xy;
    FD_viewport *view_xz;
    FD_viewport *view_zy;
    FD_overview *overview;
    FD_viewport *viewport;
    FD_mainview *mainview;
} xg3d_gui;
/* xg3d environment */
typedef struct {
    bool use_sel;
    bool buffered;
    xg3d_gui *gui;
    xg3d_datasets *data;
} xg3d_env;
/* !!! GLOBAL VARIABLES !!! */
extern char *kernel_dir;  /* directory where kernels reside */
extern int cursor;        /* crosshair cursor */
extern int selcursor;     /* brush cursor */
/* !!! END GLOBAL VARIABLES !!! */
/* calibrated values */
#define CALIBDVAL(hdr,g3d,i) CALIBVAL(hdr,g3d_get_dvalue(g3d,i))
inline static double CALIBVAL(const xg3d_header *hdr, const double x) {
    return hdr->cal_inter + hdr->cal_slope * x;
}
inline static double UNCALIBVAL(const xg3d_header *hdr, const double x) {
    return (x - hdr->cal_inter) / hdr->cal_slope;
}
inline static double CALIBSUM(const xg3d_header *hdr, const double x, const index_t N) {
    return N * hdr->cal_inter + hdr->cal_slope * x;
}
g3d_ret xg3d_datasets_add(xg3d_datasets*,xg3d_data*);
g3d_ret xg3d_datasets_remove(xg3d_datasets*,const xg3d_data*);
void xg3d_data_clear(xg3d_data*,const bool);
xg3d_data* xg3d_data_dup(const xg3d_data*);
void setup_main(xg3d_env*,xg3d_data*);
void setup_from_data(xg3d_env*,const bool);
void set_default_acq_settings(xg3d_env*,xg3d_header*,const bool);
void load_single_show_mainview(xg3d_env*);
#endif
#include "cb_viewport.h"
#include "cb_dialog.h"
#include "cb_mainview.h"
#include "cb_surface.h"
#include "cb_measure.h"
#include "cb_info.h"
#include "cb_overview.h"
#include "fd_overlay.h"
#include "cb_selection.h"
#include "cb_calibration.h"
#include "cb_coloring.h"
#include "cb_quantization.h"
#include "cb_rois.h"
#include "cb_dose.h"
#include "cb_dvhs.h"
#include "cb_operations.h"

#include <gsl/gsl_math.h>

#include "selection.h"
#include "dl.h"
#include "util.h"
#include "util_coordinates.h"
#include "util_units.h"
#include "icons_orient.h"
#include "gnuplot_i.h"

static void _clear_view_positioners(FD_viewport *view) {
    const Window win = view->viewport->window;
    const int dm = fl_get_drawmode();
    static XRectangle rec[4];

    if(!fl_winisvalid(win)) return;

    rec[0].x = view->pos_top->x;
    rec[0].y = view->pos_top->y;
    rec[0].width = view->pos_top->w;
    rec[0].height = view->pos_top->h;
    rec[1].x = view->pos_left->x;
    rec[1].y = view->pos_left->y;
    rec[1].width = view->pos_left->w;
    rec[1].height = view->pos_left->h;
    rec[2].x = view->pos_bottom->x;
    rec[2].y = view->pos_bottom->y;
    rec[2].width = view->pos_bottom->w;
    rec[2].height = view->pos_bottom->h;
    rec[3].x = view->pos_right->x;
    rec[3].y = view->pos_right->y;
    rec[3].width = view->pos_right->w;
    rec[3].height = view->pos_right->h;

    fl_drawmode(GXcopy);
    fl_color(FL_COL1);
    XFillRectangles(fl_display,win,fl_get_gc(),rec,4);
    fl_drawmode(dm);
}

static void _update_view_positioners(FD_viewport *view, FL_Coord x, FL_Coord y) {
    const enum xg3d_orientation orient = VIEW_ORIENT(view);
    const unsigned int zoom_x = VIEW_ZOOM_X(view);
    const unsigned int zoom_y = VIEW_ZOOM_Y(view);
    const Window win = view->viewport->window;
    static FL_Coord olx=0,oly=0;
    static XSegment seg[8];
    const int dm = fl_get_drawmode(),
              lw = fl_get_linewidth();

    if(!fl_winisvalid(win)) return;

    const unsigned int zoom_h = (orient % 2 == 0) ? zoom_x : zoom_y;
    const unsigned int zoom_v = (orient % 2 == 0) ? zoom_y : zoom_x;

    /* snap to grid */
    x = (x / zoom_h) * zoom_h;
    y = (y / zoom_v) * zoom_v;

    /* horizontal segments */
    seg[0].x1 = seg[4].x1 = view->pos_left->x;
    seg[0].x2 = seg[4].x2 = view->pos_left->x + OV_THICK;
    seg[1].x1 = seg[5].x1 = view->pos_right->x;
    seg[1].x2 = seg[5].x2 = view->pos_right->x + OV_THICK;

    seg[0].y1 = seg[0].y2 = seg[1].y1 = seg[1].y2 = view->pos_left->y + oly + zoom_v / 2;
    seg[4].y1 = seg[4].y2 = seg[5].y1 = seg[5].y2 = view->pos_left->y + y   + zoom_v / 2;

    /* vertical segments */
    seg[2].y1 = seg[6].y1 = view->pos_top->y;
    seg[2].y2 = seg[6].y2 = view->pos_top->y + OV_THICK;
    seg[3].y1 = seg[7].y1 = view->pos_bottom->y;
    seg[3].y2 = seg[7].y2 = view->pos_bottom->y + OV_THICK;

    seg[2].x1 = seg[2].x2 = seg[3].x1 = seg[3].x2 = view->pos_top->x + olx + zoom_h / 2;
    seg[6].x1 = seg[6].x2 = seg[7].x1 = seg[7].x2 = view->pos_top->x + x   + zoom_h / 2;

    olx = x; oly = y; // update static variables

    fl_drawmode(GXcopy);

    /* horizontal segments */
    fl_linewidth(zoom_v);
    /* clear previous segments */
    fl_color(FL_COL1);
    XDrawSegments(fl_display,win,fl_get_gc(),seg,2);
    /* draw new segments */
    fl_color(VIEW_CURSOR_COLOR(view));
    XDrawSegments(fl_display,win,fl_get_gc(),&seg[4],2);

    /* vertical segments */
    fl_linewidth(zoom_h);
    /* clear previous segments */
    fl_color(FL_COL1);
    XDrawSegments(fl_display,win,fl_get_gc(),&seg[2],2);
    /* draw new segments */
    fl_color(VIEW_CURSOR_COLOR(view));
    XDrawSegments(fl_display,win,fl_get_gc(),&seg[6],2);

    fl_drawmode(dm);
    fl_linewidth(lw);
}

void draw_sel_voxel(FD_viewport *view, FD_selection *selection, const coord_t j, const coord_t i) {
    _selpattf *patt_sel = SELECTION_PATTERN_FUNC(selection);
    const Window win = FL_ObjWin(VIEW_CANVAS(view));
    const enum xg3d_orientation orient = VIEW_ORIENT(view);
    const unsigned int zoom_x = (orient % 2 == 0) ? VIEW_ZOOM_X(view) : VIEW_ZOOM_Y(view);
    const unsigned int zoom_y = (orient % 2 == 0) ? VIEW_ZOOM_Y(view) : VIEW_ZOOM_X(view);
    const int dm = fl_get_drawmode();
    FL_Coord wx = 0,wy = 0;

    if(!fl_winisvalid(win)) return;

    fl_winset(win);
    fl_drawmode(SEL_GXMODE);
    grid_to_win(view,&wx,&wy,j,i);
    patt_sel(wx,wy,zoom_x,zoom_y,SEL_COLOR);
    fl_drawmode(dm);
}

void draw_meas_point(FD_viewport *view, const coord_t j, const coord_t i, const FL_COLOR color) {
    const Window win = FL_ObjWin(VIEW_CANVAS(view));
    const enum xg3d_orientation orient = VIEW_ORIENT(view);
    const unsigned int zoom_x = (orient % 2 == 0) ? VIEW_ZOOM_X(view) : VIEW_ZOOM_Y(view);
    const unsigned int zoom_y = (orient % 2 == 0) ? VIEW_ZOOM_Y(view) : VIEW_ZOOM_X(view);
    const int dm = fl_get_drawmode();
    FL_Coord wx = 0,wy = 0;

    if(!fl_winisvalid(win)) return;

    fl_winset(win);
    fl_drawmode(GXcopy);
    grid_to_win(view,&wx,&wy,j,i);
    if(zoom_x == 1 && zoom_y == 1)
        fl_point(wx,wy,color);
    else
        fl_rectf(wx,wy,zoom_x,zoom_y,color);
    fl_drawmode(dm);
}

static void _draw_marker_label(FD_viewport *view, const marker *mk, const FL_COLOR color) {
    const enum g3d_slice_plane plane = VIEW_PLANE(view);
    const Window win = FL_ObjWin(VIEW_CANVAS(view));
    const enum xg3d_orientation orient = VIEW_ORIENT(view);
    const unsigned int zoom_x = (orient % 2 == 0) ? VIEW_ZOOM_X(view) : VIEW_ZOOM_Y(view);
    const unsigned int zoom_y = (orient % 2 == 0) ? VIEW_ZOOM_Y(view) : VIEW_ZOOM_X(view);
    const int dm = fl_get_drawmode();
    FL_Coord wx = 0,wy = 0;

    if(!fl_winisvalid(win)) return;

    fl_winset(win);
    fl_drawmode(GXcopy);
    grid_to_win(view,&wx,&wy,mk->vox[PLANE_TO_J(plane)],mk->vox[PLANE_TO_I(plane)]);
    fl_draw_text_beside(mk->align,wx + zoom_x / 2,wy + zoom_y / 2,1,1,color,UNIFONT,FL_NORMAL_SIZE,mk->label);
    fl_drawmode(dm);
}

static void _draw_meas_line(FD_measure *measure, FD_viewport *view, const FL_Coord x1, const FL_Coord y1, const FL_Coord x2, const FL_Coord y2) {
    const Window win = FL_ObjWin(VIEW_CANVAS(view));
    const enum xg3d_orientation orient = VIEW_ORIENT(view);
    const unsigned int zx = (orient % 2 == 0) ? VIEW_ZOOM_X(view) / 2 : VIEW_ZOOM_Y(view) / 2;
    const unsigned int zy = (orient % 2 == 0) ? VIEW_ZOOM_Y(view) / 2 : VIEW_ZOOM_X(view) / 2;
    const int dm = fl_get_drawmode();

    if(!fl_winisvalid(win)) return;

    fl_winset(win);
    fl_drawmode(SEL_GXMODE);
    if(fl_get_button(measure->btn_line)) /* Line */
        fl_line(x1 + zx,y1 + zy,x2 + zx,y2 + zy,SEL_COLOR);
    else { /* Circle: XXX: when zx != zy, this is not accurate! (it should deform into an ellipse) */
        double cx = 0.5 * (x1 + x2 + 2 * zx);
        double cy = 0.5 * (y1 + y2 + 2 * zy);
        double r = hypot(x1 + zx - cx,y1 + zy - cy);
        fl_circ(cx,cy,r,SEL_COLOR);
    }
    fl_drawmode(dm);
}

static void _draw_shape_line(FD_viewport *view, enum xg3d_selection_shape shape, FL_Coord x1, FL_Coord y1, FL_Coord x2, FL_Coord y2) {
    const Window win = FL_ObjWin(VIEW_CANVAS(view));
    const enum xg3d_orientation orient = VIEW_ORIENT(view);
    const unsigned int zx = (orient % 2 == 0) ? VIEW_ZOOM_X(view) / 2 : VIEW_ZOOM_Y(view) / 2;
    const unsigned int zy = (orient % 2 == 0) ? VIEW_ZOOM_Y(view) / 2 : VIEW_ZOOM_X(view) / 2;
    const int dm = fl_get_drawmode();
    static FL_POINT box[5];
    double cx,cy,r;

    if(!fl_winisvalid(win)) return;

    fl_winset(win);
    fl_drawmode(SEL_GXMODE);
    switch(shape) {
        case SEL_SHAPE_BOX:
            box[0].x = box[3].x = x1 + zx;
            box[0].y = box[1].y = y1 + zy;
            box[1].x = box[2].x = x2 + zx; 
            box[2].y = box[3].y = y2 + zy;
            fl_polyl(box,4,SEL_COLOR);
            break;
        case SEL_SHAPE_ELLIPSOID:
            fl_ovall(FL_min(x1,x2),FL_min(y1,y2),FL_abs(x2-x1),FL_abs(y2-y1),SEL_COLOR);
            break;
        case SEL_SHAPE_SPHERE:
        case SEL_SHAPE_CYLINDER:
            cx = 0.5 * (x1 + x2 + 2 * zx);
            cy = 0.5 * (y1 + y2 + 2 * zy);
            r = hypot(x1 + zx - cx,y1 + zy - cy);
            fl_circ(cx,cy,r,SEL_COLOR);
            break;
        case SEL_SHAPE_LINE:
        case SEL_SHAPE_PLANE:
            fl_line(x1 + zx,y1 + zy,x2 + zx,y2 + zy,SEL_COLOR);
            break;
    }
    fl_drawmode(dm);
}

static void _draw_xhair(FD_viewport *view, const coord_t j, const coord_t i) {
    const Window win = FL_ObjWin(VIEW_CANVAS(view));
    const enum xg3d_orientation orient = VIEW_ORIENT(view);
    const unsigned int zoom_x = (orient % 2 == 0) ? VIEW_ZOOM_X(view) : VIEW_ZOOM_Y(view);
    const unsigned int zoom_y = (orient % 2 == 0) ? VIEW_ZOOM_Y(view) : VIEW_ZOOM_X(view);
    const int dm = fl_get_drawmode(),
              lw = fl_get_linewidth();
    FL_Coord wx=0,wy=0,ww,wh;
    XSegment seg[2];

    if(!fl_winisvalid(win)) return;

    fl_get_winsize(win,&ww,&wh);
    grid_to_win(view,&wx,&wy,j,i);
    fl_drawmode(GXxor);
    /* correct for line width */
    wx += zoom_x / 2;
    wy += zoom_y / 2;
    seg[0].x1 = seg[1].y1 = 0;
    seg[0].y2 = seg[0].y1 = wy;
    seg[1].x1 = seg[1].x2 = wx;
    seg[0].x2 = ww;
    seg[1].y2 = wh;
    fl_color(VIEW_CURSOR_COLOR(view));
    /* horizontal line */
    fl_linewidth(zoom_y);
    XDrawSegments(fl_display,win,fl_get_gc(),seg,1);
    /* vertical line */
    fl_linewidth(zoom_x);
    XDrawSegments(fl_display,win,fl_get_gc(),&seg[1],1);

    fl_drawmode(dm);
    fl_linewidth(lw);
}

#if OFFMAP_PAINT_SELECTION
#define INVERT(x) ((x) = ~(x))
#define INVERT_RGB(j,i) INVERT(img->red[i][j]); INVERT(img->green[i][j]); INVERT(img->blue[i][j]);
/* XXX: WARNING: this was NOT updated to use different X and Y zoom values */
static void _paint_selection(FD_viewport *view, FD_selection *selection, FL_IMAGE *img) {
    const enum xg3d_selection_pattern pattern = SELECTION_PATTERN(selection);
    const grid3d *sel = VIEW_SELSLICE(view);
    unsigned int zoom = VIEW_ZOOM(view);
    const coord_t nx = sel->nx;
    const coord_t ny = sel->ny;

    if(zoom == 1) {
#ifdef _OPENMP
#pragma omp parallel for
#endif
        for(slice_t ix=0;ix<nx*ny;ix++)
            if(g3d_selbit_check(sel,ix)) {
                FL_Coord wx,wy;
                grid_to_win(view,&wx,&wy,ix%nx,ix/nx);
                INVERT_RGB(wx,wy);
            }
    } else {
        switch(pattern) {
            default:
            case SEL_PATT_CROSS:
#ifdef _OPENMP
#pragma omp parallel for
#endif
                for(slice_t ix=0;ix<nx*ny;ix++)
                    if(g3d_selbit_check(sel,ix)) {
                        FL_Coord wx,wy;
                        grid_to_win(view,&wx,&wy,ix%nx,ix/nx);
                        /* draw cross */
                        for(unsigned int z=0;z<zoom;z++) {
                            INVERT_RGB(wx+z,wy+z);
                            INVERT_RGB(wx+zoom-z-1,wy+z);
                        }
                        if(zoom % 2) {
                            INVERT_RGB(wx+zoom/2,wy+zoom/2);
                        }
                    }
                break;
            case SEL_PATT_DOT:
#ifdef _OPENMP
#pragma omp parallel for
#endif
                for(slice_t ix=0;ix<nx*ny;ix++)
                    if(g3d_selbit_check(sel,ix)) {
                        FL_Coord wx,wy;
                        grid_to_win(view,&wx,&wy,ix%nx,ix/nx);
                        /* draw dot */
                        INVERT_RGB(wx+zoom/2,wy+zoom/2);
                    }
                break;
            case SEL_PATT_SQUARE:
#ifdef _OPENMP
#pragma omp parallel for
#endif
                for(slice_t ix=0;ix<nx*ny;ix++)
                    if(g3d_selbit_check(sel,ix)) {
                        FL_Coord wx,wy;
                        grid_to_win(view,&wx,&wy,ix%nx,ix/nx);
                        /* draw square */
                        for(unsigned int z=0;z<zoom*zoom;z++) {
                            INVERT_RGB(wx+z%zoom,wy+z/zoom);
                        }
                    }
                break;
        }
    }
}
#undef INVERT_RGB
#undef INVERT

static void _draw_markers(FD_viewport *view, FD_selection *selection, const bool measure) {
    const enum xg3d_selection_mode selmode = SELECTION_MODE(selection);
    const xg3d_view_workspace *vwk = VIEW_WORKSPACE(view);
    const enum g3d_slice_plane plane = VIEW_PLANE(view);
    const FL_COLOR cur_color = VIEW_CURSOR_COLOR(view);
    const Window win = FL_ObjWin(VIEW_CANVAS(view));
    const coord_t spj = vwk->sp[PLANE_TO_J(plane)];
    const coord_t spi = vwk->sp[PLANE_TO_I(plane)];
    const coord_t spk = vwk->sp[PLANE_TO_K(plane)];
    const coord_t k = VIEW_K(view);

    if(!fl_winisvalid(win)) return;

    if(selmode == SEL_MODE_SHAPE && vwk->spset && spk == k)
        draw_meas_point(view,spj,spi,cur_color);

    if(SELECTION_NMK(selection) > 0 && !measure) {
        marker *mk = (marker*)SELECTION_MKLIST(selection);
        while(mk != NULL) {
            if(mk->vox[PLANE_TO_K(plane)] == k) { /* map global coordinates to in-plane coordinates */
                draw_meas_point(view,mk->vox[PLANE_TO_J(plane)],mk->vox[PLANE_TO_I(plane)],cur_color);
                if(fl_get_button(selection->btn_labels) && mk->label != NULL)
                    _draw_marker_label(view,mk,cur_color);
            }
            mk = mk->next;
        }
    }
}
#else
static void _draw_selection(FD_viewport *view, FD_selection *selection, const bool measure) {
    const enum xg3d_selection_mode selmode = SELECTION_MODE(selection);
    _selpattf *patt_sel = SELECTION_PATTERN_FUNC(selection);
    const xg3d_view_workspace *vwk = VIEW_WORKSPACE(view);
    const enum g3d_slice_plane plane = VIEW_PLANE(view);
    const FL_COLOR cur_color = VIEW_CURSOR_COLOR(view);
    const Window win = FL_ObjWin(VIEW_CANVAS(view));
    const coord_t spj = vwk->sp[PLANE_TO_J(plane)];
    const coord_t spi = vwk->sp[PLANE_TO_I(plane)];
    const coord_t spk = vwk->sp[PLANE_TO_K(plane)];
    const int dm = fl_get_drawmode();
    const coord_t nx = VIEW_NX(view);
    const coord_t ny = VIEW_NY(view);
    const coord_t k = VIEW_K(view);

    if(!fl_winisvalid(win)) return;

    const unsigned int zoom_h = (vwk->orient % 2 == 0) ? vwk->zoom_x : vwk->zoom_y;
    const unsigned int zoom_v = (vwk->orient % 2 == 0) ? vwk->zoom_y : vwk->zoom_x;

    if(vwk->sel->nvox >= 1 && fl_get_button(selection->btn_visible)) {
        fl_winset(win); fl_drawmode(SEL_GXMODE);
        for(slice_t i=0;i<nx*ny;i++)
            if(g3d_selbit_check(vwk->sel->g3d,i)) {
                FL_Coord wx,wy;
                grid_to_win(view,&wx,&wy,i%nx,i/nx);
                patt_sel(wx,wy,zoom_h,zoom_v,SEL_COLOR);
            }
    }

    fl_drawmode(GXcopy);
    if(selmode == SEL_MODE_SHAPE && vwk->spset && spk == k)
        draw_meas_point(view,spj,spi,cur_color);

    if(SELECTION_NMK(selection) > 0 && !measure) {
        marker *mk = (marker*)SELECTION_MKLIST(selection);
        while(mk != NULL) {
            if(mk->vox[PLANE_TO_K(plane)] == k) { /* map global coordinates to in-plane coordinates */
                draw_meas_point(view,mk->vox[PLANE_TO_J(plane)],mk->vox[PLANE_TO_I(plane)],cur_color);
                if(fl_get_button(selection->btn_labels) && mk->label != NULL)
                    _draw_marker_label(view,mk,cur_color);
            }
            mk = mk->next;
        }
    }

    fl_drawmode(dm);
}
#endif

static void _draw_measurement(FD_measure *measure, FD_viewport *view) {
    const xg3d_view_workspace *vwk = VIEW_WORKSPACE(view);
    const FL_COLOR cur_color = VIEW_CURSOR_COLOR(view);
    const coord_t spj = vwk->sp[PLANE_TO_J(vwk->plane)];
    const coord_t spi = vwk->sp[PLANE_TO_I(vwk->plane)];
    const coord_t spk = vwk->sp[PLANE_TO_K(vwk->plane)];
    const coord_t epj = vwk->ep[PLANE_TO_J(vwk->plane)];
    const coord_t epi = vwk->ep[PLANE_TO_I(vwk->plane)];
    const coord_t epk = vwk->ep[PLANE_TO_K(vwk->plane)];
    const coord_t k = VIEW_K(view);
    FL_Coord wx1,wy1,wx2,wy2;

    unsigned int m,M,t;
    m = spk; M = epk; if(M < m) { t = m; m = M; M = t; }

    if(vwk->spset && vwk->epset) /* both points set */
        if(k >= m && k <= M) {
            grid_to_win(view,&wx1,&wy1,spj,spi);
            grid_to_win(view,&wx2,&wy2,epj,epi);
            _draw_meas_line(measure,view,wx1,wy1,wx2,wy2);
        }

    if(vwk->spset && spk == k)
        draw_meas_point(view,spj,spi,cur_color);
    if(vwk->epset && epk == k)
        draw_meas_point(view,epj,epi,cur_color);
}

void restore_image_voxel(FD_viewport *view, FD_selection *selection, const coord_t j, const coord_t i) {
    const grid3d *selslice = VIEW_SELSLICE(view);
    const FL_IMAGE *img = VIEW_IMAGE(view);
    FL_Coord imx = 0,
             imy = 0;

    grid_to_win(view,&imx,&imy,j,i);
    fl_mapcolor(FL_FREE_COL1,img->red_lut[img->ci[imy][imx]],img->green_lut[img->ci[imy][imx]],img->blue_lut[img->ci[imy][imx]]);
    draw_meas_point(view,j,i,FL_FREE_COL1);
    if(selslice != NULL && g3d_selbit_check(selslice,j+selslice->nx*i))
        draw_sel_voxel(view,selection,j,i);
}

void replace_slice(FD_viewport *view, FD_selection *selection, const grid3d *data, const coord_t k) {
    const enum g3d_slice_plane plane = VIEW_PLANE(view);
    index_t *selnvoxels = (index_t*)&VIEW_SELNVOX(view);
    grid3d **selslice = (grid3d**)&VIEW_SELSLICE(view);
    double *selvalue = (double*)&VIEW_SELVALUE(view);
    grid3d **slice = (grid3d**)&VIEW_SLICE(view);
    double val,mean,sd,min,max;
    index_t nvx;

    g3d_free(*slice);
    *slice = g3d_slice(data,k,plane);

    if(SELECTION_G3D(selection) != NULL) {
        g3d_free(*selslice);
        *selslice = g3d_slice(SELECTION_G3D(selection),k,plane);
        g3d_u8_setup_nhood(*selslice,SCOPE_SLICE);

        sel_recal(*selslice,*slice,&nvx,NULL,&val,&mean,&sd,&min,&max);
        *selnvoxels = nvx;
        *selvalue = val;

        fl_set_object_label(selection->tx_min_s,"");
        fl_set_object_label(selection->tx_max_s,"");
        fl_set_object_label(selection->tx_spread_s,"");
        if(nvx >= 1) {
            fl_set_object_label_f(selection->tx_min_s,"%.8g",min);
            fl_set_object_label_f(selection->tx_max_s,"%.8g",max);
        }
        if(nvx >= 2)
            fl_set_object_label_f(selection->tx_spread_s,"%.8g",sqrt(sd / (nvx - 1)));

        sel_update_info(view,selection);
        fl_set_object_lcolor(selection->tx_min_s,FL_BLACK);
        fl_set_object_lcolor(selection->tx_max_s,FL_BLACK);
        fl_set_object_lcolor(selection->tx_spread_s,FL_BLACK);
    }
}

#undef ENV
void replace_quantslice(xg3d_env *ENV) {
    const grid3d *slice = VIEW_SLICE(GUI->viewport);
    grid3d **qslice = &VIEW_QUANT_SLICE(GUI->viewport);

    g3d_free(*qslice);
    *qslice = g3d_alloc(G3D_UINT8,slice->nx,slice->ny,slice->nz,.alloc=true);
    g3d_quant_apply(slice,*qslice,QUANTIZATION_INFO(GUI->quantization));
    if(GUI->mainview != NULL && GUI->surface != NULL && fl_get_button(GUI->mainview->surface))
        update_surface_plot(ENV);
}
#define ENV _default_env

void replace_img(FD_viewport *view, FD_quantization *quantization) {
    const unsigned int nlvl = QUANTIZATION_NLEVELS(quantization);
    const enum xg3d_orientation orient = VIEW_ORIENT(view);
    const grid3d *quantslice = VIEW_QUANT_SLICE(view);
    const unsigned int zoom_x = VIEW_ZOOM_X(view);
    const unsigned int zoom_y = VIEW_ZOOM_Y(view);
    FL_IMAGE **img = (FL_IMAGE**)&VIEW_IMAGE(view);
    const bool flip = orient_flip_check(orient);
    const unsigned int theta = 90 * (orient &~ ORIENT_FLIP);

    *img = g3d_to_flimage(quantslice,.levels=nlvl,.zoom_x=zoom_x,.zoom_y=zoom_y,.theta=theta,.flip=flip,.im=img);
}

#define FRAME_W (img->w + 2 * OV_THICK + 2)
#define FRAME_H (img->h + 2 * OV_THICK + 2)
#define DEF_SCROLL_THICK 18 /* from fli_get_default_scrollbarsize */
void resize_canvas(FD_mainview *mainview, FD_viewport *view) {
    FL_Coord formw = MV_LEFT_MARGIN, formh = MV_TOP_MARGIN + MV_SLIDER_THICK;
    FL_Coord framew,frameh,framew_max,frameh_max;
    const FL_IMAGE *img = VIEW_IMAGE(view);
    double xs,ys;

    framew_max = MV_MAX_WIDTH - MV_LEFT_MARGIN;
    frameh_max = MV_MAX_HEIGHT - MV_TOP_MARGIN - MV_SLIDER_THICK - MV_BOT_MARGIN;

    if(FRAME_W > framew_max && FRAME_H > frameh_max) {
        framew = FL_min(FRAME_W+DEF_SCROLL_THICK+2,framew_max);
        frameh = FL_min(FRAME_H+DEF_SCROLL_THICK+2,frameh_max);
        xs = fl_get_formbrowser_xscroll(mainview->fb);
        ys = fl_get_formbrowser_yscroll(mainview->fb);
    } else if(FRAME_W > framew_max) {
        framew = FL_min(FRAME_W,framew_max);
        frameh = FL_min(FRAME_H+DEF_SCROLL_THICK+2,frameh_max);
        xs = fl_get_formbrowser_xscroll(mainview->fb);
        ys = 0.0;
    } else if(FRAME_H > frameh_max) {
        framew = FL_min(FRAME_W+DEF_SCROLL_THICK+2,framew_max);
        frameh = FL_min(FRAME_H,frameh_max);
        xs = 0.0;
        ys = fl_get_formbrowser_yscroll(mainview->fb);
    } else {
        framew = FL_min(FRAME_W,framew_max);
        frameh = FL_min(FRAME_H,frameh_max);
        xs = ys = 0.0;
    }

    formw += framew;
    formh += frameh + MV_BOT_MARGIN;

    formw = FL_clamp(formw,MV_MIN_WIDTH,MV_MAX_WIDTH);
    formh = FL_clamp(formh,MV_MIN_HEIGHT,MV_MAX_HEIGHT);

    fl_freeze_form(mainview->mainview);
    fl_set_form_size(mainview->mainview,formw,formh);
    fl_set_object_geometry(mainview->prev,MV_LEFT_MARGIN,MV_TOP_MARGIN,MV_THICK,MV_SLIDER_THICK);
    fl_set_object_geometry(mainview->slider,MV_LEFT_MARGIN+MV_THICK,MV_TOP_MARGIN,framew-2*MV_THICK,MV_SLIDER_THICK);
    fl_set_object_geometry(mainview->next,MV_LEFT_MARGIN+framew-MV_THICK,MV_TOP_MARGIN,MV_THICK,MV_SLIDER_THICK);
    fl_set_object_geometry(mainview->fb,MV_LEFT_MARGIN,MV_TOP_MARGIN+MV_SLIDER_THICK,framew,frameh);
    fl_unfreeze_form(mainview->mainview);

    if(fl_object_is_visible(mainview->fb))
        if(img->w != view->canvas->w || img->h != view->canvas->h) {
            fl_freeze_form(view->viewport);
            fl_set_form_size(view->viewport,2*OV_THICK+img->w,2*OV_THICK+img->h);
            fl_set_object_geometry(view->canvas,OV_THICK,OV_THICK,img->w,img->h);
            fl_set_object_geometry(view->pos_left,0,OV_THICK,OV_THICK,img->h);
            fl_set_object_geometry(view->pos_right,OV_THICK+img->w,OV_THICK,OV_THICK,img->h);
            fl_set_object_geometry(view->pos_top,OV_THICK,0,img->w,OV_THICK);
            fl_set_object_geometry(view->pos_bottom,OV_THICK,OV_THICK+img->h,img->w,OV_THICK);
            fl_unfreeze_form(view->viewport);
            fl_set_formbrowser_maxarea(mainview->fb,view->viewport->w,view->viewport->h);
            fl_redraw_object(mainview->fb);
            fl_set_formbrowser_xscroll(mainview->fb,xs);
            fl_set_formbrowser_yscroll(mainview->fb,ys);
            fl_redraw_object(mainview->fb);
        }

    _clear_view_positioners(view);
}
#undef FRAME_W
#undef FRAME_H

#undef ENV
void refresh_canvas(xg3d_env *ENV) {
    const int measure = fl_get_button(GUI->mainview->measure);
    const Window win = FL_ObjWin(VIEW_CANVAS(GUI->viewport));
    const xg3d_lut *lut = COLORING_LUT(GUI->coloring);
    FL_IMAGE *img = VIEW_IMAGE(GUI->viewport);

    if(!fl_winisvalid(win)) return; /* invalid window */

#if OFFMAP_PAINT_SELECTION
    /* paint selection in RGB image */
    if(VIEW_SELNVOX(GUI->viewport) >= 1 && fl_get_button(GUI->selection->btn_visible)) {
        FL_IMAGE *rgb = flimage_dup(img);
        rgb->force_convert = 1;
        flimage_convert(rgb,FL_IMAGE_RGB,0);
        _paint_selection(GUI->viewport,GUI->selection,rgb);
        flimage_display(rgb,win);
        flimage_free(rgb);
    } else
        flimage_display(img,win);
    _draw_markers(GUI->viewport,GUI->selection,measure);
#else
    VIEW_MOVED(GUI->viewport) = false; /* set flag for drawing */
    g3d_flimage_set_lut(img,lut->colors); /* set image colors */
    flimage_display(img,win); /* redisplay image */
    /* redraw stuff */
    _draw_selection(GUI->viewport,GUI->selection,measure); /* draw selection */
#endif

    if(measure) _draw_measurement(GUI->measure,GUI->viewport); /* draw measurement if measure active */
}
#define ENV _default_env

#undef ENV
static void _update_status(xg3d_env *ENV) {
    const grid3d *quantslice = VIEW_QUANT_SLICE(GUI->viewport);
    const Window win = FL_ObjWin(VIEW_CANVAS(GUI->viewport));
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const grid3d *slice = VIEW_SLICE(GUI->viewport);
    const coord_t k = VIEW_K(GUI->viewport);
    const xg3d_header *hdr = DATA->hdr;
    const coord_t ox = hdr->ox;
    const coord_t oy = hdr->oy;
    const coord_t oz = hdr->oz;

    int flag_calib = false;
    const coord_t *dj,*di,*dk;
    const char *ulabel = "";
    const coord_t *vox[3];
    unsigned int keymask;
    FL_Coord x,y,w,h;
    double vx,vy,vz;
    coord_t j=0,i=0;

    if(MAINVIEW_GET_CALIB(GUI->mainview)) {
        flag_calib = true;
        ulabel = fl_get_input(GUI->calibration->inp_ulabel);
    }

    switch(fl_get_choice(GUI->mainview->ch_ulen)) {
        default:
        case 1: /* vox */
            vx = vy = vz = 1.0;
            break;
        case 2: /* cm */
            vx = hdr->vx;
            vy = hdr->vy;
            vz = hdr->vz;
            break;
        case 3: /* mm */
            vx = hdr->vx * CM_TO_MM;
            vy = hdr->vy * CM_TO_MM;
            vz = hdr->vz * CM_TO_MM;
            break;
    }

    vox[0] = &j; vox[1] = &i; vox[2] = &k;
    dj = vox[PLANE_TO_J(plane)];
    di = vox[PLANE_TO_I(plane)];
    dk = vox[PLANE_TO_K(plane)];

    fl_get_win_mouse(win,&x,&y,&keymask);
    fl_get_winsize(win,&w,&h);
    if(x >= 0 && x < w && y >= 0 && y < h) {
        win_to_grid(GUI->viewport,x,y,&j,&i);
        _update_view_positioners(GUI->viewport,x,y);
        const double fvx = vx * ((int)*dj - (int)ox);
        const double fvy = vy * ((int)*di - (int)oy);
        const double fvz = vz * ((int)*dk - (int)oz);
        const size_t ix_slice = (size_t)j + i * slice->nx;
        const size_t ix_qslice = (size_t)j + i * quantslice->nx;
        assert(ix_slice == ix_qslice);
        if(flag_calib) {
            fl_set_object_label_f(GUI->mainview->status,
                    "x:%6.4g y:%6.4g z:%6.4g | %s: %.8g%s | %s: %s",fvx,fvy,fvz,_("value"),
                    CALIBDVAL(hdr,slice,ix_slice),ulabel,_("level"),
                    quant_level_string(GUI->quantization,g3d_u8(quantslice,ix_qslice),hdr,flag_calib));
        } else {
            switch(hdr->type) {
                case RAW_BIT:
                case RAW_UINT8:
                case RAW_UINT16:
                case RAW_UINT32:
                case RAW_UINT64:
                    fl_set_object_label_f(GUI->mainview->status,
                            "x:%6.4g y:%6.4g z:%6.4g | %s: %ju%s | %s: %s",fvx,fvy,fvz,_("value"),
                            (uintmax_t)g3d_get_dvalue(slice,ix_slice),ulabel,_("level"),
                            quant_level_string(GUI->quantization,g3d_u8(quantslice,ix_qslice),hdr,flag_calib));
                    break;
                case RAW_INT8:
                case RAW_INT16:
                case RAW_INT32:
                case RAW_INT64:
                    fl_set_object_label_f(GUI->mainview->status,
                            "x:%6.4g y:%6.4g z:%6.4g | %s: %jd%s | %s: %s",fvx,fvy,fvz,_("value"),
                            (intmax_t)g3d_get_dvalue(slice,ix_slice),ulabel,_("level"),
                            quant_level_string(GUI->quantization,g3d_u8(quantslice,ix_qslice),hdr,flag_calib));
                    break;
                case RAW_FLOAT32:
                case RAW_FLOAT64:
                    fl_set_object_label_f(GUI->mainview->status,
                            "x:%6.4g y:%6.4g z:%6.4g | %s: %.8g%s | %s: %s",fvx,fvy,fvz,_("value"),
                            g3d_get_dvalue(slice,ix_slice),ulabel,_("level"),
                            quant_level_string(GUI->quantization,g3d_u8(quantslice,j+i*quantslice->nx),hdr,flag_calib));
                    break;
            }
        }
    }
}
#define ENV _default_env

void cb_cursorColor ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    FD_viewport **ov_viewports = OVERVIEW_VIEWPORTS(GUI->overview);

    FL_COLOR cur_color = fl_show_colormap(VIEW_CURSOR_COLOR(GUI->viewport));
    VIEW_CURSOR_COLOR(GUI->viewport) =
    VIEW_CURSOR_COLOR(ov_viewports[PLANE_XY]) =
    VIEW_CURSOR_COLOR(ov_viewports[PLANE_XZ]) =
    VIEW_CURSOR_COLOR(ov_viewports[PLANE_ZY]) = cur_color;
    fl_set_cursor_color(cursor,cur_color,FL_BLACK);
    if(selcursor) fl_set_cursor_color(selcursor,cur_color,FL_BLACK);
    fl_set_object_lcolor(GUI->viewport->pos_left,cur_color);
    fl_set_object_lcolor(GUI->viewport->pos_top,cur_color);
    fl_set_object_lcolor(GUI->viewport->pos_right,cur_color);
    fl_set_object_lcolor(GUI->viewport->pos_bottom,cur_color);
                fl_set_object_lcolor(ov_viewports[PLANE_XY]->pos_left,cur_color);
        fl_set_object_lcolor(ov_viewports[PLANE_XY]->pos_top,cur_color);
        fl_set_object_lcolor(ov_viewports[PLANE_XY]->pos_right,cur_color);
        fl_set_object_lcolor(ov_viewports[PLANE_XY]->pos_bottom,cur_color);
           fl_set_object_lcolor(ov_viewports[PLANE_XZ]->pos_left,cur_color);
        fl_set_object_lcolor(ov_viewports[PLANE_XZ]->pos_top,cur_color);
        fl_set_object_lcolor(ov_viewports[PLANE_XZ]->pos_right,cur_color);
        fl_set_object_lcolor(ov_viewports[PLANE_XZ]->pos_bottom,cur_color);
           fl_set_object_lcolor(ov_viewports[PLANE_ZY]->pos_left,cur_color);
        fl_set_object_lcolor(ov_viewports[PLANE_ZY]->pos_top,cur_color);
        fl_set_object_lcolor(ov_viewports[PLANE_ZY]->pos_right,cur_color);
        fl_set_object_lcolor(ov_viewports[PLANE_ZY]->pos_bottom,cur_color);
       refresh_canvas(ENV);
}

static void _set_orient_bitmap(FL_OBJECT *bit_orient, const enum g3d_slice_plane plane, const enum xg3d_orientation orient) {
    switch(plane) {
        case PLANE_XY:
                                        switch(orient) {
                    case ORIENT_0:        fl_set_bitmap_data(bit_orient,orient_XY0_width,orient_XY0_height,orient_XY0_bits);
 break;
                    case ORIENT_90:       fl_set_bitmap_data(bit_orient,orient_XY1_width,orient_XY1_height,orient_XY1_bits);
 break;
                    case ORIENT_180:      fl_set_bitmap_data(bit_orient,orient_XY2_width,orient_XY2_height,orient_XY2_bits);
 break;
                    case ORIENT_270:      fl_set_bitmap_data(bit_orient,orient_XY3_width,orient_XY3_height,orient_XY3_bits);
 break;
                    case ORIENT_F0:       fl_set_bitmap_data(bit_orient,orient_XY4_width,orient_XY4_height,orient_XY4_bits);
 break;
                    case ORIENT_F90:      fl_set_bitmap_data(bit_orient,orient_XY5_width,orient_XY5_height,orient_XY5_bits);
 break;
                    case ORIENT_F180:     fl_set_bitmap_data(bit_orient,orient_XY6_width,orient_XY6_height,orient_XY6_bits);
 break;
                    case ORIENT_F270:     fl_set_bitmap_data(bit_orient,orient_XY7_width,orient_XY7_height,orient_XY7_bits);
 break;
                }
                       break;
        case PLANE_XZ:
                            switch(orient) {
                    case ORIENT_0:        fl_set_bitmap_data(bit_orient,orient_XZ0_width,orient_XZ0_height,orient_XZ0_bits);
 break;
                    case ORIENT_90:       fl_set_bitmap_data(bit_orient,orient_XZ1_width,orient_XZ1_height,orient_XZ1_bits);
 break;
                    case ORIENT_180:      fl_set_bitmap_data(bit_orient,orient_XZ2_width,orient_XZ2_height,orient_XZ2_bits);
 break;
                    case ORIENT_270:      fl_set_bitmap_data(bit_orient,orient_XZ3_width,orient_XZ3_height,orient_XZ3_bits);
 break;
                    case ORIENT_F0:       fl_set_bitmap_data(bit_orient,orient_XZ4_width,orient_XZ4_height,orient_XZ4_bits);
 break;
                    case ORIENT_F90:      fl_set_bitmap_data(bit_orient,orient_XZ5_width,orient_XZ5_height,orient_XZ5_bits);
 break;
                    case ORIENT_F180:     fl_set_bitmap_data(bit_orient,orient_XZ6_width,orient_XZ6_height,orient_XZ6_bits);
 break;
                    case ORIENT_F270:     fl_set_bitmap_data(bit_orient,orient_XZ7_width,orient_XZ7_height,orient_XZ7_bits);
 break;
                }
                       break;
        case PLANE_ZY:
                            switch(orient) {
                    case ORIENT_0:        fl_set_bitmap_data(bit_orient,orient_ZY0_width,orient_ZY0_height,orient_ZY0_bits);
 break;
                    case ORIENT_90:       fl_set_bitmap_data(bit_orient,orient_ZY1_width,orient_ZY1_height,orient_ZY1_bits);
 break;
                    case ORIENT_180:      fl_set_bitmap_data(bit_orient,orient_ZY2_width,orient_ZY2_height,orient_ZY2_bits);
 break;
                    case ORIENT_270:      fl_set_bitmap_data(bit_orient,orient_ZY3_width,orient_ZY3_height,orient_ZY3_bits);
 break;
                    case ORIENT_F0:       fl_set_bitmap_data(bit_orient,orient_ZY4_width,orient_ZY4_height,orient_ZY4_bits);
 break;
                    case ORIENT_F90:      fl_set_bitmap_data(bit_orient,orient_ZY5_width,orient_ZY5_height,orient_ZY5_bits);
 break;
                    case ORIENT_F180:     fl_set_bitmap_data(bit_orient,orient_ZY6_width,orient_ZY6_height,orient_ZY6_bits);
 break;
                    case ORIENT_F270:     fl_set_bitmap_data(bit_orient,orient_ZY7_width,orient_ZY7_height,orient_ZY7_bits);
 break;
                }
                       break;
        default:
            fprintf(stderr,"%s: invalid g3d_slice_plane value passed: %d",__func__,plane);
            abort();
    }
}

void cb_mainview_showhide ( FL_OBJECT * obj  , long data  ) {
    const int button = fl_get_button_numb(obj);
    FL_FORM *form = NULL;
    char buf[1024];

    switch(data) {
        case 0: /* Menu */
            form = GUI->menu->menu;
            break;
        case 1: /* Info */
            form = GUI->info->info;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Information"));
            break;
        case 2: /* Overview */
            form = GUI->overview->overview;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Overview"));
            break;
        case 3: /* Calibration */
            form = GUI->calibration->calibration;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Calibration"));
            break;
        case 4: /* Selection */
            form = GUI->selection->selection;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Selection"));
            break;
        case 5: /* Quantization */
            form = GUI->quantization->quantization;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Quantization"));
            break;
        case 6: /* Coloring */
            form = GUI->coloring->coloring;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Coloring"));
            break;
        case 7: /* ROIs */
            form = GUI->rois->rois;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("ROIs"));
            break;
        case 8: /* Dose */
            form = GUI->dose->dose;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Dose"));
            break;
        case 9: /* DVHs */
            form = GUI->dvhs->dvhs;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("DVHs"));
            break;
        case 10: /* Operations */
            form = GUI->operations->operations;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("Operations"));
            break;
        case 11: /* CT */
            form = GUI->ct->ct;
            snprintf(buf,sizeof(buf),XG3D_TITLE_FORMAT,_("CT"));
            break;
    }

    if(form == GUI->menu->menu) {
        fl_raise_form(form);
        fl_winfocus(form->window);
        return;
    }

    if(button == FL_RIGHT_MOUSE && fl_form_is_visible(form)) {
        fl_set_button(obj,1);
        fl_raise_form(form);
        fl_winfocus(form->window);
        return;
    }

    if(fl_form_is_visible(form)) {
        fl_hide_form(form);
        fl_deactivate_form(form);
        return;
    } else {
        fl_activate_form(form);
        fl_show_form(form,FL_PLACE_SIZE,FL_FULLBORDER,buf);
        fl_raise_form(form);
        fl_winfocus(form->window);
    }

    if(form == GUI->coloring->coloring)
        update_lut_pix(GUI->coloring);
    else if(form == GUI->selection->selection)
        if(fl_get_button(GUI->mainview->measure))
            fl_deactivate_form(form);
}

void cb_orient ( FL_OBJECT * obj  , long data  ) {
    enum xg3d_orientation orient = VIEW_ORIENT(GUI->viewport);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    FL_IMAGE *img = VIEW_IMAGE(GUI->viewport);
    const int w = img->w,
              h = img->h;

    switch(data) {
        case 0: /* CCW */
            orient = orient_flip_check(orient) ?
                (orient - 1) | ORIENT_FLIP :
                (orient + 1) &~ ORIENT_FLIP;
            flimage_rotate(img,900,FLIMAGE_NOSUBPIXEL);
            break;
        case 1: /* CW */
            orient = orient_flip_check(orient) ?
                ((orient &~ ORIENT_FLIP) + 1) | ORIENT_FLIP :
                ((orient | ORIENT_FLIP) - 1) &~ ORIENT_FLIP;
            flimage_rotate(img,-900,FLIMAGE_NOSUBPIXEL);
            break;
        case 2: /* Flip */
            orient = (orient + ORIENT_FLIP) &~ (ORIENT_FLIP << 1);
            flimage_flip(img,'r');
            break;
    }

    VIEW_ORIENT(GUI->viewport) = orient;
    MAINVIEW_STORE_ORIENT(GUI->mainview,plane,orient);
    _set_orient_bitmap(GUI->mainview->bit_orient,plane,orient);
    if(w != img->w || h != img->h)
        resize_canvas(GUI->mainview,GUI->viewport);
    refresh_canvas(ENV);
}

void cb_zoom ( FL_OBJECT * obj  , long data  ) {
    static FL_POPUP *popup_zoom = NULL;

    static FL_POPUP_ITEM items_zoom[] = {
            {"X",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
           {"Y",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
           {NULL,NULL,NULL,0,0}
    };

    if(popup_zoom == NULL) {
        popup_zoom = fl_popup_create(None,"",items_zoom);
        fl_popup_set_policy(popup_zoom,FL_POPUP_DRAG_SELECT);
        fl_popup_set_color(popup_zoom,FL_POPUP_HIGHLIGHT_COLOR,FL_YELLOW);
        fl_popup_set_color(popup_zoom,FL_POPUP_HIGHLIGHT_TEXT_COLOR,FL_BLACK);

                {
            FL_POPUP_ENTRY *entry = fl_popup_entry_get_by_position(popup_zoom,0);
            fl_popup_entry_set_text(entry,_("X"));
            fl_popup_entry_set_value(entry,0);
        }
               {
            FL_POPUP_ENTRY *entry = fl_popup_entry_get_by_position(popup_zoom,1);
            fl_popup_entry_set_text(entry,_("Y"));
            fl_popup_entry_set_value(entry,1);
        }
           }

    long mbtn = fl_mouse_button();
    const int incr = 1 - 2 * data; // 0 -> 1; 1 -> -1;

    unsigned int zoom_x = VIEW_ZOOM_X(GUI->viewport);
    unsigned int zoom_y = VIEW_ZOOM_Y(GUI->viewport);

    if(mbtn == 1) { // left mouse button, zoom
        zoom_x += incr;
        zoom_y += incr;

                zoom_x = FL_clamp(zoom_x,1,MVC_MAX_ZOOM);
        zoom_y = FL_clamp(zoom_y,1,MVC_MAX_ZOOM);

        if(zoom_x != VIEW_ZOOM_X(GUI->viewport) || zoom_y != VIEW_ZOOM_Y(GUI->viewport)) {
            VIEW_ZOOM_X(GUI->viewport) = zoom_x;
            VIEW_ZOOM_Y(GUI->viewport) = zoom_y;

            if(zoom_x == zoom_y)
                fl_set_object_label_f(GUI->mainview->txt_zoom,"%ux",zoom_x);
            else
                fl_set_object_label_f(GUI->mainview->txt_zoom,"%ux%u",zoom_x,zoom_y);

            replace_img(GUI->viewport,GUI->quantization);
            resize_canvas(GUI->mainview,GUI->viewport);
            refresh_canvas(ENV);
            sel_set_cursor(GUI->viewport,GUI->selection);
        }
           } else if(mbtn == 3) { // right mouse button, popup
        FL_POPUP *popup = popup_zoom;
        FL_OBJECT *button = incr > 0 ? GUI->mainview->zoomIn : GUI->mainview->zoomOut;
        fl_popup_set_title(popup,incr > 0 ? _("Zoom in") : _("Zoom out"));
        fl_popup_set_position(popup,button->form->x+button->x+button->w,button->form->y+button->y);
        FL_POPUP_RETURN *retpop = fl_popup_do(popup); if(retpop == NULL) return;
        assert(retpop->val >= 0 && retpop->val <= 1);

        if(retpop->val) zoom_y += incr;
        else zoom_x += incr;

                zoom_x = FL_clamp(zoom_x,1,MVC_MAX_ZOOM);
        zoom_y = FL_clamp(zoom_y,1,MVC_MAX_ZOOM);

        if(zoom_x != VIEW_ZOOM_X(GUI->viewport) || zoom_y != VIEW_ZOOM_Y(GUI->viewport)) {
            VIEW_ZOOM_X(GUI->viewport) = zoom_x;
            VIEW_ZOOM_Y(GUI->viewport) = zoom_y;

            if(zoom_x == zoom_y)
                fl_set_object_label_f(GUI->mainview->txt_zoom,"%ux",zoom_x);
            else
                fl_set_object_label_f(GUI->mainview->txt_zoom,"%ux%u",zoom_x,zoom_y);

            replace_img(GUI->viewport,GUI->quantization);
            resize_canvas(GUI->mainview,GUI->viewport);
            refresh_canvas(ENV);
            sel_set_cursor(GUI->viewport,GUI->selection);
        }
           }
}

void cb_setzoom ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    unsigned int zoom_x = VIEW_ZOOM_X(GUI->viewport);
    unsigned int zoom_y = VIEW_ZOOM_Y(GUI->viewport);
    char def[8],buf[64];

    snprintf(def,sizeof(def),"%ux%u",zoom_x,zoom_y);
    snprintf(buf,sizeof(buf),"%s:",_("Set zoom level"));
    const char *txt = fl_show_input(buf,def);
    if(txt == NULL || strlen(txt) == 0) return;

    if(sscanf(txt,"%ux%u",&zoom_x,&zoom_y) == 2);
    else if(sscanf(txt,"%ux",&zoom_x) == 1) zoom_y = zoom_x;
    else {
        fl_show_alert2(0,_("Bad input!\fPlease provide a valid zoom level."));
        return;
    }

            zoom_x = FL_clamp(zoom_x,1,MVC_MAX_ZOOM);
        zoom_y = FL_clamp(zoom_y,1,MVC_MAX_ZOOM);

        if(zoom_x != VIEW_ZOOM_X(GUI->viewport) || zoom_y != VIEW_ZOOM_Y(GUI->viewport)) {
            VIEW_ZOOM_X(GUI->viewport) = zoom_x;
            VIEW_ZOOM_Y(GUI->viewport) = zoom_y;

            if(zoom_x == zoom_y)
                fl_set_object_label_f(GUI->mainview->txt_zoom,"%ux",zoom_x);
            else
                fl_set_object_label_f(GUI->mainview->txt_zoom,"%ux%u",zoom_x,zoom_y);

            replace_img(GUI->viewport,GUI->quantization);
            resize_canvas(GUI->mainview,GUI->viewport);
            refresh_canvas(ENV);
            sel_set_cursor(GUI->viewport,GUI->selection);
        }
       }

void cb_plane ( FL_OBJECT * obj  , long data  ) {
    enum g3d_slice_plane plane = data;
    const coord_t k = MAINVIEW_GET_K(GUI->mainview,plane);

    VIEW_K(GUI->viewport) = k; /* set stored k */
    VIEW_PLANE(GUI->viewport) = plane;
    VIEW_ORIENT(GUI->viewport) = MAINVIEW_GET_ORIENT(GUI->mainview,plane); /* set stored orient */
    fl_set_button(GUI->mainview->flip,orient_flip_check(VIEW_ORIENT(GUI->viewport)));
    fl_set_slider_bounds(GUI->mainview->slider,0.0,DATA->hdr->dim[PLANE_TO_K(plane)]-1);
    fl_set_slider_value(GUI->mainview->slider,k);
    fl_set_object_label_f(GUI->mainview->txt_k,"%u",k);
    replace_slice(GUI->viewport,GUI->selection,DATA->g3d,k);
    update_datarange(ENV,false); /* update datarange if SLICE scope */
    set_range_inpsli(ENV,false); /* update range inpsli if SLICE scope */
    replace_quantslice(ENV);
    replace_img(GUI->viewport,GUI->quantization);
    _set_orient_bitmap(GUI->mainview->bit_orient,plane,VIEW_ORIENT(GUI->viewport));
    resize_canvas(GUI->mainview,GUI->viewport);
    refresh_canvas(ENV);
    sel_update_info(GUI->viewport,GUI->selection);

    if(fl_get_button(GUI->mainview->measure) && fl_get_button(GUI->measure->btn_circle)
            && VIEW_SPSET(GUI->viewport) && VIEW_EPSET(GUI->viewport))
        fl_call_object_callback(GUI->measure->btn_clear);
}

void cb_measure ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    FL_FORM *const form = GUI->measure->measure;
    const int button = fl_get_button_numb(obj);
    static enum xg3d_selection_mode selmode;
    FL_Coord w,h; fl_get_object_size(GUI->mainview->box,&w,&h);

    if(button == FL_RIGHT_MOUSE && fl_form_is_visible(form)) {
        fl_set_button(obj,1);
        fl_raise_form(form);
        return;
    }

    if(!fl_form_is_visible(form)) {
        selmode = SELECTION_MODE(GUI->selection);
        SELECTION_MODE(GUI->selection) = SEL_MODE_NONE; /* disable selection temporarily */
        fl_deactivate_form(GUI->selection->selection);
        sel_set_cursor(GUI->viewport,GUI->selection);

        fl_activate_form(form);
        fl_show_form_f(form,FL_PLACE_SIZE,FL_TRANSIENT,XG3D_TITLE_FORMAT,_("Measurement"));
        fl_raise_form(form);
    } else {
        SELECTION_MODE(GUI->selection) = selmode;
        fl_activate_form(GUI->selection->selection);

        fl_hide_form(form);
        fl_deactivate_form(form);
    }

    fl_call_object_callback(GUI->measure->btn_clear);
}

void cb_surface ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    FL_FORM *const form = GUI->surface->surface;
    const int button = fl_get_button_numb(obj);
    gnuplot_ctrl *gplot;

    if(button == FL_RIGHT_MOUSE && fl_form_is_visible(form)) {
        fl_set_button(obj,1);
        fl_raise_form(form);
        return;
    }

    if(!fl_form_is_visible(form)) {
        if((gplot = gnuplot_init()) == NULL) {
            fl_show_alert2(0,_("Cannot plot surface!\fgnuplot not found in PATH"));
            fl_set_button(obj,0);
            return;
        }
        SURFACE_GPLOT(GUI->surface) = gplot;
        fl_activate_form(form);
        fl_show_form_f(form,FL_PLACE_FREE,FL_TRANSIENT,XG3D_TITLE_FORMAT,_("Surface plot"));
        fl_raise_form(form);

        gnuplot_cmd(gplot,"set title noenh '%s'",DATA->hdr->file);
        gnuplot_cmd(gplot,"unset ztics");
        gnuplot_cmd(gplot,"set border 15");
        gnuplot_cmd(gplot,"unset colorbox");
        gnuplot_cmd(gplot,"set xyplane at 1");
        update_surface_plot(ENV);
    } else {
        if(SURFACE_GPLOT(GUI->surface) != NULL) {
            gnuplot_close(SURFACE_GPLOT(GUI->surface));
            SURFACE_GPLOT(GUI->surface) = NULL;
        }

        fl_hide_form(form);
        fl_deactivate_form(form);
    }
}

void cb_slider ( FL_OBJECT * obj  , long data  ) {
    const coord_t newk = fl_get_slider_value(GUI->mainview->slider);

    switch(data) {
        case 0: /* slider */
            VIEW_K(GUI->viewport) = newk;
            MAINVIEW_STORE_K(GUI->mainview,VIEW_PLANE(GUI->viewport),newk); /* store k */
            fl_set_object_label_f(GUI->mainview->txt_k,"%u",newk);
            replace_slice(GUI->viewport,GUI->selection,DATA->g3d,newk);
            /* update quantization if SLICE scope */
            if(QUANTIZATION_GET_SCOPE(GUI->quantization) == SCOPE_SLICE) {
                update_datarange(ENV,false);
                set_range_inpsli(ENV,false);
                compute_quantization(GUI->quantization,VIEW_SLICE(GUI->viewport));
                update_quant_coloring(GUI->quantization,GUI->coloring,DATA->hdr);
            }
            replace_quantslice(ENV);
            replace_img(GUI->viewport,GUI->quantization);
            refresh_canvas(ENV);
            break;
        case 1: /* prev button */
            if(newk == 0) return;
            fl_set_slider_value(GUI->mainview->slider,newk-1);
            fl_call_object_callback(GUI->mainview->slider);
            break;
        case 2: /* next button */
            fl_set_slider_value(GUI->mainview->slider,newk+1);
            fl_call_object_callback(GUI->mainview->slider);
            break;
    }
}

void cb_setk ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    int k = VIEW_K(GUI->viewport);
    char def[8],buf[64];

    snprintf(def,sizeof(def),"%d",k);
    snprintf(buf,sizeof(buf),"%s:",_("Go to slice"));
    const char *txt = fl_show_input(buf,def);
    if(txt == NULL || strlen(txt) == 0) return;

    if(*txt == '+' || *txt == '-') // signed -> increment/decrement
        k += atoi(txt);
    else // unsigned -> set
        k = atoi(txt);

    fl_set_slider_value(GUI->mainview->slider,k);
    fl_call_object_callback(GUI->mainview->slider);
}

#define WHITE FL_PACK(255,255,255)
#define BLACK FL_PACK(0,0,0)
#undef ENV
static g3d_ret xg3d_flimage_add_lut(xg3d_env *ENV, FL_IMAGE *flimg) {
    const xg3d_quantization_workspace *qwk = QUANTIZATION_WORKSPACE(GUI->quantization);
    const int ch = fl_get_char_height(FL_FIXED_STYLE,FL_TINY_SIZE,NULL,NULL);
    const int cw = fl_get_char_width(FL_FIXED_STYLE,FL_TINY_SIZE);
    const int flag_calib = MAINVIEW_GET_CALIB(GUI->mainview);
    const Window win = FL_ObjWin(VIEW_CANVAS(GUI->viewport));
    const xg3d_lut *lut = COLORING_LUT(GUI->coloring);
    const xg3d_header *hdr = DATA->hdr;
    FLIMAGE_TEXT txt;
    char buf[256];

    if(!fl_winisvalid(win)) return G3D_FAIL; /* invalid window */

    unsigned int w = flimg->w;
    unsigned int h = flimg->h;
    unsigned int lvlh = h / lut->ncol;
    unsigned int toth = lut->ncol * lvlh;
    unsigned int padh = (h - toth) / 2;

    txt.align = FL_ALIGN_LEFT; txt.style = FL_FIXED_STYLE; txt.size = FL_TINY_SIZE;
    txt.color = BLACK; txt.bcolor = WHITE; txt.nobk = false; txt.angle = 0;
    flimg->force_convert = 1;
    flimage_convert(flimg,FL_IMAGE_RGB,0);
    flimg->fill_color = WHITE;
    flimage_add_marker(flimg,"line",w,h/2,h+1,0,FL_SOLID,false,900,BLACK,0);
    flimage_add_marker(flimg,"line",w+18,h/2,h+1,0,FL_SOLID,false,900,BLACK,0);
    flimage_crop(flimg,0,0,-(1+14*cw),0);
    flimage_add_marker(flimg,"line",w+1+16+6,h-(1+padh),8,0,FL_SOLID,false,0,BLACK,0);
    snprintf(buf,sizeof(buf),"%+8.3E",
            flag_calib ? CALIBVAL(hdr,qwk->min) : qwk->min);
    txt.str = buf; txt.len = 10; txt.x = w+1+16; txt.y = h-(3+ch/2+padh);
    flimage_add_text_struct(flimg,&txt);
    for(unsigned int i=0;i<padh;i++)
        flimage_add_marker(flimg,"line",w+1+8,h-(1+i),16,0,FL_SOLID,false,0,lut->colors[0],0);
    for(unsigned int i=0;i<lut->ncol;i++)
        for(unsigned int j=0;j<lvlh;j++)
            flimage_add_marker(flimg,"line",w+1+8,h-(1+i*lvlh+j+padh),16,0,FL_SOLID,false,0,lut->colors[i],0);
    for(unsigned int i=padh+lvlh+(lut->ncol-1)*lvlh;i<h;i++)
        flimage_add_marker(flimg,"line",w+1+8,h-(1+i),16,0,FL_SOLID,false,0,lut->colors[lut->ncol-1],0);

    unsigned int i = lut->ncol-1;
    unsigned int j = lvlh-1;
    if(lut->ncol > 2) {
        flimage_add_marker(flimg,"line",w+1+16+6,h-(1+(lut->ncol/2)*lvlh+(lvlh/2)+padh),8,0,FL_SOLID,false,0,BLACK,0);
        snprintf(buf,sizeof(buf),"%+8.3E",
                flag_calib ? CALIBVAL(hdr,qwk->info->bkpts[lut->ncol/2]) :
                qwk->info->bkpts[lut->ncol/2]);
        txt.str = buf; txt.len = 10; txt.x = w+1+16; txt.y = h-(1+(lut->ncol/2)*lvlh+(lvlh/2)-ch/2+padh);
        flimage_add_text_struct(flimg,&txt);
    }
    flimage_add_marker(flimg,"line",w+1+16+6,h-(1+i*lvlh+j+padh),8,0,FL_SOLID,false,0,BLACK,0);
    snprintf(buf,sizeof(buf),"%+8.3E",
            flag_calib ? CALIBVAL(hdr,qwk->max) : qwk->max);
    txt.str = buf; txt.len = 10; txt.x = w+1+16; txt.y = h-(1+i*lvlh+j-ch/2+padh);
    flimage_add_text_struct(flimg,&txt);
    if(flag_calib) {
        const char *ulabel = fl_get_input(GUI->calibration->inp_ulabel);
        if((txt.len = strlen(ulabel)) > 0) {
            txt.str = (char*)ulabel; txt.x = w+1+16; txt.y = h-(1+(lut->ncol/2)*lvlh+(lvlh/2)-ch/2+padh+ch+4);
            flimage_add_text_struct(flimg,&txt);
        }
    }
    flimage_display(flimg,win);
    flimage_render_annotation(flimg,win);

    return G3D_OK;
}
#define ENV _default_env
#undef WHITE
#undef BLACK

void cb_save ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    static FL_POPUP *popup = NULL;
    FL_POPUP_RETURN *retpop;
    static FL_POPUP_ITEM items[] = {
        {"LUT",NULL,NULL,FL_POPUP_TOGGLE,FL_POPUP_NONE},
        {"_Image",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
        {"All",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
        {"Movie",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
        {"_Slice",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
        {NULL,NULL,NULL,0,0}
    };
    static FL_POPUP_ENTRY *entry_lut,
                          *entry_image,
                          *entry_all,
                          *entry_movie,
                          *entry_slice;

    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const Window win = FL_ObjWin(VIEW_CANVAS(GUI->viewport));
    static const char *plane_names[3] = {"XY","XZ","ZY"};
    char buf[PATH_MAX],cmd[1024];
    const char *file = NULL;

    if(!fl_winisvalid(win)) return; /* invalid window */

    double zero,maxk; fl_get_slider_bounds(GUI->mainview->slider,&zero,&maxk);

    if(popup == NULL) {
        popup = fl_popup_create(None,_("Save view"),items);
        fl_popup_set_policy(popup,FL_POPUP_DRAG_SELECT);
        fl_popup_set_color(popup,FL_POPUP_HIGHLIGHT_COLOR,FL_YELLOW);
        fl_popup_set_color(popup,FL_POPUP_HIGHLIGHT_TEXT_COLOR,FL_BLACK);

        entry_lut = fl_popup_entry_get_by_text(popup,"LUT");
        fl_popup_entry_set_text(entry_lut,_("LUT"));
        entry_image = fl_popup_entry_get_by_text(popup,"_Image");
        fl_popup_entry_set_text(entry_image,_("Image"));
        entry_all = fl_popup_entry_get_by_text(popup,"All");
        fl_popup_entry_set_text(entry_all,_("All"));
        entry_movie = fl_popup_entry_get_by_text(popup,"Movie");
        fl_popup_entry_set_text(entry_movie,_("Movie"));
        entry_slice = fl_popup_entry_get_by_text(popup,"_Slice");
        fl_popup_entry_set_text(entry_slice,_("Slice"));
    }

    if(((FL_IMAGE*)VIEW_IMAGE(GUI->viewport))->h < (int)QUANTIZATION_NLEVELS(GUI->quantization))
        fl_popup_entry_raise_state(entry_lut,FL_POPUP_DISABLED);
    else
        fl_popup_entry_clear_state(entry_lut,FL_POPUP_DISABLED);

    fl_popup_set_position(popup,obj->form->x+obj->x+obj->w,obj->form->y+obj->y);
    retpop = fl_popup_do(popup);
    if(retpop == NULL) return;

    switch(retpop->val) {
        case 1: { /* Image */
            file = fl_show_fselector(_("Save main view as..."),"","*.ppm","");
            if(file == NULL) return;
            FL_IMAGE *flimg = flimage_dup(VIEW_IMAGE(GUI->viewport));
            if(fl_popup_entry_get_state(entry_lut) == FL_POPUP_CHECKED)
                xg3d_flimage_add_lut(ENV,flimg);
            enforce_ext(file,"ppm",buf,sizeof(buf));
            flimage_dump(flimg,buf,"ppm");
            flimage_delete_all_markers(flimg);
            flimage_free(flimg);
            refresh_canvas(ENV);
            } break;
        case 2: /* All */
        case 3: { /* Movie */
#define FRAME_FORMAT "/tmp/xg3d_frame_%06u.ppm"
            if(retpop->val == 3) { /* Movie */
                if(WEXITSTATUS(fl_exe_command("ffmpeg",1)) != 1) {
                    fl_show_alert2(0,_("ffmpeg not present!\fCannot create movie."));
                    break;
                }
                file = fl_show_fselector(_("Save movie as..."),"","*.mp4","");
            } else
                file = fl_show_fselector(_("Save all slides as (prefix)..."),"","*","");
            if(file == NULL) return;
            fl_show_oneliner(_("Saving slides..."),fl_scrw/2,fl_scrh/2);
            coord_t oldk = fl_get_slider_value(GUI->mainview->slider);
            for(coord_t k=0;k<=(coord_t)maxk;k++) {
                if(retpop->val == 3) /* Movie */
                    snprintf(buf,sizeof(buf),FRAME_FORMAT,k);
                else
                    snprintf(buf,sizeof(buf),"%s_%06u.ppm",file,k);
                fl_set_slider_value(GUI->mainview->slider,k);
                fl_call_object_callback(GUI->mainview->slider);
                FL_IMAGE *flimg = flimage_dup(VIEW_IMAGE(GUI->viewport));
                if(fl_popup_entry_get_state(entry_lut) == FL_POPUP_CHECKED)
                    xg3d_flimage_add_lut(ENV,flimg);
                flimage_dump(flimg,buf,"ppm");
                flimage_delete_all_markers(flimg);
                flimage_free(flimg);
            }
            fl_set_slider_value(GUI->mainview->slider,oldk);
            fl_call_object_callback(GUI->mainview->slider);
            fl_hide_oneliner();
            if(retpop->val == 3) { /* Movie */
                fl_show_oneliner(_("Creating movie..."),fl_scrw/2,fl_scrh/2);
                enforce_ext(file,"mp4",buf,sizeof(buf));
                snprintf(cmd,sizeof(cmd),"ffmpeg -y -i /tmp/xg3d_frame_%%06d.ppm %s",buf);
                if(WEXITSTATUS(fl_exe_command(cmd,1)))
                    fl_show_alert2(0,_("Failed creating movie!\fPlease check file paths."));
                for(coord_t k=0;k<=(coord_t)maxk;k++) {
                    snprintf(buf,sizeof(buf),FRAME_FORMAT,k);
                    unlink(buf);
                }
                fl_hide_oneliner();
            }
            } break;
        case 4: { /* Slice */
            xg3d_data *tmp = calloc(1,sizeof(xg3d_data));
            tmp->hdr = ENV->data->main->hdr;
            tmp->g3d = VIEW_SLICE(GUI->viewport);
            snprintf(buf,sizeof(buf),"%s_%s%d",tmp->hdr->file,plane_names[plane],VIEW_K(GUI->viewport));
            savedlg_show(GUI->savedlg,tmp,buf);
            free(tmp);
            } break;
    }
}

void cb_slides ( FL_OBJECT * obj  , long data  ) {
    (void)data;
    static FL_POPUP *popup = NULL;
    FL_POPUP_RETURN *retpop;
    static FL_POPUP_ITEM items[] = {
        {"with minimum",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
        {"with maximum",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
        {"selected",NULL,NULL,FL_POPUP_NORMAL,FL_POPUP_NONE},
        {NULL,NULL,NULL,0,0}
    };
    static FL_POPUP_ENTRY *entry_min,
                          *entry_max,
                          *entry_sel;

    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const grid3d *selection = SELECTION_G3D(GUI->selection);

    if(popup == NULL) {
        popup = fl_popup_create(None,_("Go to slice"),items);
        fl_popup_set_policy(popup,FL_POPUP_DRAG_SELECT);
        fl_popup_set_color(popup,FL_POPUP_HIGHLIGHT_COLOR,FL_YELLOW);
        fl_popup_set_color(popup,FL_POPUP_HIGHLIGHT_TEXT_COLOR,FL_BLACK);

        entry_min = fl_popup_entry_get_by_text(popup,"with minimum");
        fl_popup_entry_set_text(entry_min,_("with minimum"));
        entry_max = fl_popup_entry_get_by_text(popup,"with maximum");
        fl_popup_entry_set_text(entry_max,_("with maximum"));
        entry_sel = fl_popup_entry_get_by_text(popup,"selected");
        fl_popup_entry_set_text(entry_sel,_("selected"));
    }

    if((retpop = fl_popup_do(popup)) == NULL) return;

    index_t ix;
    switch(retpop->val) {
        default:
        case 0: /* min */
        case 1: { /* max */
            double val = (retpop->val == 0)
                ? QUANTIZATION_ABSMIN(GUI->quantization)
                : QUANTIZATION_ABSMAX(GUI->quantization);
            ix = DATA->g3d->nvox;
            for(index_t i=0;i<DATA->g3d->nvox;i++)
                if(g3d_get_dvalue(DATA->g3d,i) == val) {
                    ix = i; break; }
            if(ix == DATA->g3d->nvox) return;
            } break;
        case 2: { /* selected */
            ix = selection->nvox;
            for(index_t i=0;i<selection->nvox;i++)
                if(g3d_selbit_check(selection,i)) {
                    ix = i; break; }
            if(ix == selection->nvox) return;
            } break;
    }

    coord_t jik[3];
    UNCOORD_DIM(ix,jik,DATA->g3d->nx,DATA->g3d->nxy);
    fl_set_slider_value(GUI->mainview->slider,jik[PLANE_TO_K(plane)]);
    fl_call_object_callback(GUI->mainview->slider);
}

void cb_units ( FL_OBJECT * obj  , long data  ) {
    switch(data) {
        case 0: /* data units */
            fl_set_object_label(obj,fl_get_button(obj) ? "CAL" : "DAT");
            break;
        case 1: /* length units */
            fl_set_object_label(obj,fl_get_button(obj) ? "mm" : "cm");
            if(VIEW_SPSET(GUI->viewport) && VIEW_EPSET(GUI->viewport))
                update_measurement(GUI->measure,GUI->viewport);
            break;
    }
}

static void _update_sel_shape_tooltip(FD_viewport *view, const int ulen, const bool show) {
    const xg3d_view_workspace *vwk = VIEW_WORKSPACE(view);
    const enum g3d_slice_plane plane = VIEW_PLANE(view);
    const xg3d_data *data = VIEW_DATA(view);
    FL_OBJECT *canvas = VIEW_CANVAS(view);
    const int spset = vwk->spset;
    const coord_t spj = vwk->sp[0];
    const coord_t spi = vwk->sp[1];
    const coord_t spk = vwk->sp[2];
    const coord_t *ep = vwk->ep;

    coord_t dj,di,dk;
    double vx,vy,vz;
    FL_Coord wx,wy;
    char buf[1024];
    int dx,dy,dz;

    switch(ulen) {
        default:
        case 1: /* vox */
            vx = vy = vz = 1.0;
            break;
        case 2: /* cm */
            vx = data->hdr->vx;
            vy = data->hdr->vy;
            vz = data->hdr->vz;
            break;
        case 3: /* mm */
            vx = data->hdr->vx * CM_TO_MM;
            vy = data->hdr->vy * CM_TO_MM;
            vz = data->hdr->vz * CM_TO_MM;
            break;
    }

    if(show && spset) {
        grid_to_win(view,&wx,&wy,ep[PLANE_TO_J(plane)],ep[PLANE_TO_I(plane)]);
        dj = ep[0]; di = ep[1]; dk = ep[2];
        dx = FL_abs(spj-dj);
        dy = FL_abs(spi-di);
        dz = FL_abs(spk-dk);
        snprintf(buf,sizeof(buf),"dx:%g dy:%g dz:%g",dx*vx,dy*vy,dz*vz);
        fl_set_oneliner_font(FL_FIXED_STYLE,FL_SMALL_SIZE);
        fl_show_oneliner(buf,canvas->form->parent->x + MV_LEFT_MARGIN + canvas->x + 1,
                             canvas->form->parent->y + MV_TOP_MARGIN + MV_SLIDER_THICK + 1 +
                             canvas->y + canvas->h);
    } else {
        fl_hide_oneliner();
        fl_set_oneliner_font(FL_NORMAL_STYLE,FL_NORMAL_SIZE);
    }
}

/* possibly the most important function! */
int cb_mainview_canvas(FL_OBJECT *obj, Window win, int ww, int wh, XEvent *xev, void *data FL_UNUSED_ARG) {
    xg3d_view_workspace *vwk = VIEW_WORKSPACE(GUI->viewport);
    const enum xg3d_orientation orient = VIEW_ORIENT(GUI->viewport);
    const unsigned int zoom_x = (orient % 2 == 0) ? VIEW_ZOOM_X(GUI->viewport) : VIEW_ZOOM_Y(GUI->viewport);
    const unsigned int zoom_y = (orient % 2 == 0) ? VIEW_ZOOM_Y(GUI->viewport) : VIEW_ZOOM_X(GUI->viewport);
    const unsigned int brush_size = fl_get_spinner_value(GUI->selection->spin_brush);
    const enum xg3d_selection_brush brush = fl_get_choice(GUI->selection->ch_brush);
    const enum xg3d_selection_shape shape = fl_get_choice(GUI->selection->ch_shape);
    const enum xg3d_scope sel_scope = fl_get_button(GUI->selection->btn_scope);
    const enum xg3d_selection_mode selmode = SELECTION_MODE(GUI->selection);
    const enum g3d_slice_plane plane = VIEW_PLANE(GUI->viewport);
    const FL_COLOR cur_color = VIEW_CURSOR_COLOR(GUI->viewport);
    const int measure = fl_get_button(GUI->mainview->measure);
    const int ulen = fl_get_choice(GUI->mainview->ch_ulen);
    const coord_t nx = VIEW_NX(GUI->viewport);
    const coord_t k = VIEW_K(GUI->viewport);
    coord_t jold = vwk->oldpos % nx;
    coord_t iold = vwk->oldpos / nx;
    FL_Coord wx1,wy1,wx2,wy2;
    unsigned int keymask;
    FL_Coord x,y;
    coord_t j,i;

    /* stuff for double-click detection */
    static unsigned long last_clicktime = 0;

    switch(xev->type) {
        case Expose:
            refresh_canvas(ENV);
            fl_remove_selected_xevent(win,PointerMotionHintMask); /* receive all mouse motion events -> really necessary? */
            fl_set_cursor_color(cursor,cur_color,FL_BLACK); /* setup cursor color */
            fl_set_cursor(win,cursor); /* setup cursor */
            break;
        case ButtonRelease:
            switch(xev->xbutton.button) {
                case FL_LEFT_MOUSE:
                    if(measure) {
                        /* nothing */
                    } else if(selmode != SEL_MODE_NONE) {
                        sel_sanity_check(GUI->viewport,GUI->selection);
                        sel_update_info(GUI->viewport,GUI->selection);
                    } else {
                        if(vwk->xhair) { /* crosshair drawn */
                            _draw_xhair(GUI->viewport,jold,iold); /* clear crosshair */
                            vwk->xhair = false; /* no longer drawing crosshair */
                            fl_set_cursor(win,cursor); /* reset cursor */
                        } else if(vwk->pan) {
                            vwk->pan = false; /* no longer panning */
                            fl_set_cursor(win,cursor); /* reset cursor */
                        }
                    }
                    break;
                case FL_MIDDLE_MOUSE:
                    if(measure) {
                        fl_set_cursor(win,cursor);
                    } else if(selmode != SEL_MODE_NONE) {
                        if(selmode == SEL_MODE_BRUSH)
                            fl_set_cursor(win,selcursor); /* reset brush cursor */
                        else
                            fl_set_cursor(win,cursor); /* reset cursor */
                    } else {
                        if(vwk->slide) {
                            vwk->slide = false; /* no longer sliding */
                            fl_set_cursor(win,cursor); /* reset cursor */
                        }
                    }
                    break;
                case FL_RIGHT_MOUSE:
                    if(measure) {
                        /* nothing */
                    } else if(selmode != SEL_MODE_NONE) {
                        sel_sanity_check(GUI->viewport,GUI->selection);
                        sel_update_info(GUI->viewport,GUI->selection);
                    } else {
                        if(vwk->level) {
                            vwk->level = false; /* no longer leveling */
                            fl_set_cursor(win,cursor); /* reset cursor */
                        }
                    }
                    break;
                case FL_SCROLLUP_MOUSE:
                    /* redraw things */
                                                                fl_get_win_mouse(win,&x,&y,&keymask);
                        win_to_grid(GUI->viewport,x,y,&j,&i);

                        if(measure) {
                            /* update measurement info (with mouse over canvas) */
                            update_measurement_live(GUI->measure,GUI->viewport,j,i,k + 1);
                        } else if(selmode != SEL_MODE_NONE) {
                            if(selmode == SEL_MODE_SHAPE) {
                                if(vwk->spset) { /* start point set (selection ongoing) */
                                    /* move endpoint to new slice */
                                    vwk->ep[PLANE_TO_K(plane)] = k + 1;
                                    /* redraw shape */
                                    grid_to_win(GUI->viewport,&wx1,&wy1,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                                    grid_to_win(GUI->viewport,&wx2,&wy2,vwk->ep[PLANE_TO_J(plane)]-1,vwk->ep[PLANE_TO_I(plane)]-1);
                                    _draw_shape_line(GUI->viewport,shape,wx1,wy1,wx2,wy2);
                                    vwk->moved = true; /* slice changed */
                                    /* update shape selection tooltip */
                                    _update_sel_shape_tooltip(GUI->viewport,ulen,true);
                                }
                            } else if(selmode == SEL_MODE_BRUSH) {
                                /* if only left button pressed */
                                if((xev->xmotion.state) & Button1Mask && !((xev->xmotion.state) & Button3Mask)) {
                                    /* draw selection (set) */
                                    sel_brush(ENV,brush,brush_size,j,i,k,SEL_OP_SET);
                                    sel_update_info(GUI->viewport,GUI->selection);
                                }
                                /* if only right button pressed */
                                else if((xev->xmotion.state) & Button3Mask && !((xev->xmotion.state) & Button1Mask)) {
                                    /* draw selection (unset) */
                                    sel_brush(ENV,brush,brush_size,j,i,k,SEL_OP_UNSET);
                                    sel_update_info(GUI->viewport,GUI->selection);
                                }
                            }
                        } else {
                            if(vwk->xhair) /* crosshair being drawn */
                                _draw_xhair(GUI->viewport,jold,iold); /* redraw crosshair */
                        }
                        /* update status */
                        _update_status(ENV);
                                       break;
                case FL_SCROLLDOWN_MOUSE:
                                            fl_get_win_mouse(win,&x,&y,&keymask);
                        win_to_grid(GUI->viewport,x,y,&j,&i);

                        if(measure) {
                            /* update measurement info (with mouse over canvas) */
                            update_measurement_live(GUI->measure,GUI->viewport,j,i,k - 1);
                        } else if(selmode != SEL_MODE_NONE) {
                            if(selmode == SEL_MODE_SHAPE) {
                                if(vwk->spset) { /* start point set (selection ongoing) */
                                    /* move endpoint to new slice */
                                    vwk->ep[PLANE_TO_K(plane)] = k - 1;
                                    /* redraw shape */
                                    grid_to_win(GUI->viewport,&wx1,&wy1,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                                    grid_to_win(GUI->viewport,&wx2,&wy2,vwk->ep[PLANE_TO_J(plane)]-1,vwk->ep[PLANE_TO_I(plane)]-1);
                                    _draw_shape_line(GUI->viewport,shape,wx1,wy1,wx2,wy2);
                                    vwk->moved = true; /* slice changed */
                                    /* update shape selection tooltip */
                                    _update_sel_shape_tooltip(GUI->viewport,ulen,true);
                                }
                            } else if(selmode == SEL_MODE_BRUSH) {
                                /* if only left button pressed */
                                if((xev->xmotion.state) & Button1Mask && !((xev->xmotion.state) & Button3Mask)) {
                                    /* draw selection (set) */
                                    sel_brush(ENV,brush,brush_size,j,i,k,SEL_OP_SET);
                                    sel_update_info(GUI->viewport,GUI->selection);
                                }
                                /* if only right button pressed */
                                else if((xev->xmotion.state) & Button3Mask && !((xev->xmotion.state) & Button1Mask)) {
                                    /* draw selection (unset) */
                                    sel_brush(ENV,brush,brush_size,j,i,k,SEL_OP_UNSET);
                                    sel_update_info(GUI->viewport,GUI->selection);
                                }
                            }
                        } else {
                            if(vwk->xhair) /* crosshair being drawn */
                                _draw_xhair(GUI->viewport,jold,iold); /* redraw crosshair */
                        }
                        /* update status */
                        _update_status(ENV);
                                       break;
            }
            break;
        case ButtonPress:
            switch(xev->xbutton.button) {
                case FL_LEFT_MOUSE:
                    if(xev->xbutton.state & Button2Mask || xev->xbutton.state & Button3Mask) return FL_IGNORE; /* exclude other buttons */
                    fl_get_win_mouse(win,&x,&y,&keymask);
                    win_to_grid(GUI->viewport,x,y,&j,&i);

                    if(measure) {
                        if(!(vwk->spset && vwk->sp[PLANE_TO_J(plane)] == j
                                        && vwk->sp[PLANE_TO_I(plane)] == i
                                        && vwk->sp[PLANE_TO_K(plane)] == k) /* differs from start point */
                                && !(vwk->epset && vwk->ep[PLANE_TO_J(plane)] == j 
                                                && vwk->ep[PLANE_TO_I(plane)] == i
                                                && vwk->ep[PLANE_TO_K(plane)] == k) /* differs from end point */
                                ) {

                            /* reset on non-coplanar circle */
                            if(fl_get_button(GUI->measure->btn_circle) && vwk->epset && vwk->ep[PLANE_TO_K(plane)] != k)
                                    fl_call_object_callback(GUI->measure->btn_clear);

                            { /* redraw things */
                                unsigned int spk = vwk->sp[PLANE_TO_K(plane)]; /* start point slice */
                                unsigned int epk = vwk->ep[PLANE_TO_K(plane)]; /* end point slice */
                                if(vwk->epset) { /* end point set */
                                    if(vwk->spset && k <= MAX(spk,epk) && k >= MIN(spk,epk)) { /* start point set and current slice between limits */
                                        /* clear measurement line */
                                        grid_to_win(GUI->viewport,&wx1,&wy1,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                                        grid_to_win(GUI->viewport,&wx2,&wy2,vwk->ep[PLANE_TO_J(plane)],vwk->ep[PLANE_TO_I(plane)]);
                                        _draw_meas_line(GUI->measure,GUI->viewport,wx1,wy1,wx2,wy2);
                                        /* restore start point */
                                        if(k == spk)
                                            restore_image_voxel(GUI->viewport,GUI->selection,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                                    }
                                    /* draw measurement line */
                                    grid_to_win(GUI->viewport,&wx1,&wy1,j,i);
                                    grid_to_win(GUI->viewport,&wx2,&wy2,vwk->ep[PLANE_TO_J(plane)],vwk->ep[PLANE_TO_I(plane)]);
                                    _draw_meas_line(GUI->measure,GUI->viewport,wx1,wy1,wx2,wy2);
                                    /* redraw end point above line if in current slice */
                                    if(k == epk)
                                        draw_meas_point(GUI->viewport,vwk->ep[PLANE_TO_J(plane)],vwk->ep[PLANE_TO_I(plane)],cur_color);
                                } else if(vwk->spset && k == spk) /* start point set and in current slice */
                                    restore_image_voxel(GUI->viewport,GUI->selection,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]); /* restore start point */
                            }

                            /* store and set start point */
                            vwk->sp[PLANE_TO_J(plane)] = j;
                            vwk->sp[PLANE_TO_I(plane)] = i;
                            vwk->sp[PLANE_TO_K(plane)] = k;
                            vwk->spset = true;
                            /* draw start point (above line drawn above) */
                            draw_meas_point(GUI->viewport,j,i,cur_color);
                            /* if end point set, update line profile */
                            if(vwk->epset) {
                                update_line_profile(GUI->measure,GUI->viewport);
                                update_measurement(GUI->measure,GUI->viewport);
                            }
                            /* update measurement info (with mouse over canvas) */
                            update_measurement_live(GUI->measure,GUI->viewport,j,i,k);
                        }
                    } else if(selmode != SEL_MODE_NONE) {
                        switch(selmode) {
                            case SEL_MODE_NONE: break;
                            case SEL_MODE_SHAPE:
                                if(!vwk->spset) { /* start point unset */
                                    /* store mouse position */
                                    vwk->oldpos = j + i*nx;
                                    /* store and set start point */
                                    vwk->sp[PLANE_TO_J(plane)] = j;
                                    vwk->sp[PLANE_TO_I(plane)] = i;
                                    vwk->sp[PLANE_TO_K(plane)] = k;
                                    vwk->spset = true;
                                    /* store and set end point */
                                    vwk->ep[PLANE_TO_J(plane)] = j + 1;
                                    vwk->ep[PLANE_TO_I(plane)] = i + 1;
                                    vwk->ep[PLANE_TO_K(plane)] = k + 1;
                                    vwk->epset = true;
                                    /* draw start point */
                                    draw_meas_point(GUI->viewport,j,i,cur_color);
                                    /* show shape selection tooltip */
                                    _update_sel_shape_tooltip(GUI->viewport,ulen,true);
                                } else { /* start point set */
                                    /* store end point */
                                    vwk->ep[PLANE_TO_J(plane)] = j + 1;
                                    vwk->ep[PLANE_TO_I(plane)] = i + 1;
                                    vwk->ep[PLANE_TO_K(plane)] = k + 1;
                                    /* clear shape */
                                    grid_to_win(GUI->viewport,&wx1,&wy1,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                                    grid_to_win(GUI->viewport,&wx2,&wy2,vwk->ep[PLANE_TO_J(plane)]-1,vwk->ep[PLANE_TO_I(plane)]-1);
                                    _draw_shape_line(GUI->viewport,shape,wx1,wy1,wx2,wy2);
                                    /* restore starting point */
                                    restore_image_voxel(GUI->viewport,GUI->selection,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                                    /* unset start and end points */
                                    vwk->spset = vwk->epset = false;
                                    /* replace (clear previous) selection if requested */
                                    if(fl_get_button(GUI->selection->btn_replace))
                                        fl_call_object_callback(sel_scope == SCOPE_SLICE ?
                                                GUI->selection->btn_clear_slice :
                                                GUI->selection->btn_clear_global);
                                    /* perform selection (set) */
                                    sel_shape(ENV,SEL_OP_SET);
                                    /* hide shape selection tooltip */
                                    _update_sel_shape_tooltip(GUI->viewport,ulen,false);
                                }
                                break;
                            case SEL_MODE_BRUSH:
                                /* replace (clear previous) selection if requested */
                                if(fl_get_button(GUI->selection->btn_replace))
                                    fl_call_object_callback(sel_scope == SCOPE_SLICE ?
                                            GUI->selection->btn_clear_slice :
                                            GUI->selection->btn_clear_global);
                                /* store mouse position */
                                vwk->oldpos = j + i*nx;
                                /* perform selection (set) */
                                sel_brush(ENV,brush,brush_size,j,i,k,SEL_OP_SET);
                                break;
                            case SEL_MODE_LEVEL:
                                /* replace (clear previous) selection if requested */
                                if(fl_get_button(GUI->selection->btn_replace))
                                    fl_call_object_callback(sel_scope == SCOPE_SLICE ?
                                            GUI->selection->btn_clear_slice :
                                            GUI->selection->btn_clear_global);
                                /* perform selection (set) */
                                sel_level(ENV,j,i,sel_scope,SEL_OP_SET);
                                break;
                            case SEL_MODE_COMPONENT:
                                /* replace (clear previous) selection if requested */
                                if(fl_get_button(GUI->selection->btn_replace))
                                    fl_call_object_callback(sel_scope == SCOPE_SLICE ?
                                            GUI->selection->btn_clear_slice :
                                            GUI->selection->btn_clear_global);
                                /* perform selection (set) */
                                sel_level_comp(ENV,j,i,sel_scope,SEL_OP_SET);
                                break;
                            case SEL_MODE_RANDOM:
                                /* store mouse position */
                                vwk->oldpos = j + i*nx;
                                /* replace (clear previous) selection if requested */
                                if(fl_get_button(GUI->selection->btn_replace))
                                    fl_call_object_callback(sel_scope == SCOPE_SLICE ?
                                            GUI->selection->btn_clear_slice :
                                            GUI->selection->btn_clear_global);
                                /* perform selection (set) */
                                sel_random(ENV,SEL_OP_SET);
                                break;
                            case SEL_MODE_FILL:
                                /* replace (clear previous) selection if requested */
                                if(fl_get_button(GUI->selection->btn_replace))
                                    fl_call_object_callback(sel_scope == SCOPE_SLICE ?
                                            GUI->selection->btn_clear_slice :
                                            GUI->selection->btn_clear_global);
                                /* perform selection (set) */
                                sel_fill(ENV,j,i,sel_scope,SEL_OP_SET);
                                break;
                            case SEL_MODE_MARKER:
                                /* replace (clear previous) selection if requested */
                                if(fl_get_button(GUI->selection->btn_replace)) {
                                    sel_clear_markers(GUI->selection);
                                    refresh_canvas(ENV);
                                }   
                                /* perform selection (set) */
                                sel_add_marker(ENV,j,i,k,"");
                                break;
                            case SEL_MODE_CONTOUR:
                                /* replace (clear previous) selection if requested */
                                if(fl_get_button(GUI->selection->btn_replace))
                                    fl_call_object_callback(sel_scope == SCOPE_SLICE ?
                                            GUI->selection->btn_clear_slice :
                                            GUI->selection->btn_clear_global);
                                /* perform selection (set) */
                                sel_contour(ENV,j,i,sel_scope,SEL_OP_SET,false);
                                break;
                            case SEL_MODE_ISOCOMPONENT:
                                /* replace (clear previous) selection if requested */
                                if(fl_get_button(GUI->selection->btn_replace))
                                    fl_call_object_callback(sel_scope == SCOPE_SLICE ?
                                            GUI->selection->btn_clear_slice :
                                            GUI->selection->btn_clear_global);
                                /* perform selection (set) */
                                sel_contour(ENV,j,i,sel_scope,SEL_OP_SET,true);
                                break;
                        }
                        /* update selection info */
                        sel_sanity_check(GUI->viewport,GUI->selection);
                        sel_update_info(GUI->viewport,GUI->selection);
                    } else {
                        /* check for double click (code from xforms' FL_DBLCLICK) */
                        if ( !(FL_abs(vwk->xold - x) > 4 || FL_abs(vwk->yold - y) > 4) /* moved 4 px or less */
                                && xev->xbutton.time - last_clicktime < FL_CLICK_TIMEOUT /* short delay */
                           ) { /* double click */
                            /* change cursor */
                            fl_set_cursor(win,XC_fleur);
                            /* set panning */
                            vwk->pan = true;
                        } else { /* single click */
                            /* hide cursor */
                            fl_set_cursor(win,FL_INVISIBLE_CURSOR);
                            /* draw crosshair */
                            _draw_xhair(GUI->viewport,j,i);
                            /* store mouse position */
                            vwk->oldpos = j + i*nx;
                            /* set drawing crosshair */
                            vwk->xhair = true;
                        }

                        last_clicktime = xev ? xev->xbutton.time : 0;
                        vwk->xold = x; vwk->yold = y;
                    }
                    /* update status */
                    _update_status(ENV);
                    break;
                case FL_MIDDLE_MOUSE:
                    if(xev->xbutton.state & Button1Mask || xev->xbutton.state & Button3Mask) return FL_IGNORE; /* exclude other buttons */
                    fl_get_win_mouse(win,&x,&y,&keymask);

                    if(measure) {
                        /* nothing */
                    } else if(selmode != SEL_MODE_NONE) {
                        /* nothing */
                    } else {
                        /* change cursor */
                        fl_set_cursor(win,XC_double_arrow);
                        /* store y mouse window position */
                        vwk->yold = y;
                        /* set sliding */
                        vwk->slide = true;
                    }
                    break;
                case FL_RIGHT_MOUSE:
                    if(xev->xbutton.state & Button1Mask || xev->xbutton.state & Button2Mask) return FL_IGNORE; /* exclude other buttons */
                    fl_get_win_mouse(win,&x,&y,&keymask);
                    win_to_grid(GUI->viewport,x,y,&j,&i);

                    if(measure) {
                        if(!(vwk->epset && vwk->ep[PLANE_TO_J(plane)] == j
                                        && vwk->ep[PLANE_TO_I(plane)] == i
                                        && vwk->ep[PLANE_TO_K(plane)] == k) /* differs from start point */
                                && !(vwk->spset && vwk->sp[PLANE_TO_J(plane)] == j 
                                                && vwk->sp[PLANE_TO_I(plane)] == i
                                                && vwk->sp[PLANE_TO_K(plane)] == k) /* differs from end point */
                                ) {

                            /* reset on non-coplanar circle */
                            if(fl_get_button(GUI->measure->btn_circle) && vwk->spset && vwk->sp[PLANE_TO_K(plane)] != k)
                                    fl_call_object_callback(GUI->measure->btn_clear);

                            { /* redraw things */
                                unsigned int spk = vwk->sp[PLANE_TO_K(plane)]; /* start point slice */
                                unsigned int epk = vwk->ep[PLANE_TO_K(plane)]; /* end point slice */
                                if(vwk->spset) { /* start point set */
                                    if(vwk->epset && k <= MAX(spk,epk) && k >= MIN(spk,epk)) { /* end point set and current slice between limits */
                                        /* clear measurement line */
                                        grid_to_win(GUI->viewport,&wx1,&wy1,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                                        grid_to_win(GUI->viewport,&wx2,&wy2,vwk->ep[PLANE_TO_J(plane)],vwk->ep[PLANE_TO_I(plane)]);
                                        _draw_meas_line(GUI->measure,GUI->viewport,wx1,wy1,wx2,wy2); 
                                        /* restore end point */
                                        if(k == epk)
                                            restore_image_voxel(GUI->viewport,GUI->selection,vwk->ep[PLANE_TO_J(plane)],vwk->ep[PLANE_TO_I(plane)]);
                                    }
                                    /* draw measurement line */
                                    grid_to_win(GUI->viewport,&wx1,&wy1,j,i);
                                    grid_to_win(GUI->viewport,&wx2,&wy2,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                                    _draw_meas_line(GUI->measure,GUI->viewport,wx1,wy1,wx2,wy2);
                                    /* redraw start point above line if in current slice */
                                    if(k == spk)
                                        draw_meas_point(GUI->viewport,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)],cur_color);
                                } else if(vwk->epset && k == epk) /* end point set and in current slice */
                                    restore_image_voxel(GUI->viewport,GUI->selection,vwk->ep[PLANE_TO_J(plane)],vwk->ep[PLANE_TO_I(plane)]); /* restore end point */
                            }

                            /* store and set end point */
                            vwk->ep[PLANE_TO_J(plane)] = j;
                            vwk->ep[PLANE_TO_I(plane)] = i;
                            vwk->ep[PLANE_TO_K(plane)] = k;
                            vwk->epset = true;
                            /* draw end point (above line drawn above) */
                            draw_meas_point(GUI->viewport,j,i,cur_color);
                            /* if start point set, update line profile */
                            if(vwk->spset) {
                                update_line_profile(GUI->measure,GUI->viewport);
                                update_measurement(GUI->measure,GUI->viewport);
                            }
                            /* update measurement info (with mouse over canvas) */
                            update_measurement_live(GUI->measure,GUI->viewport,j,i,k);
                        }
                    } else if(selmode != SEL_MODE_NONE) {
                        switch(selmode) {
                            case SEL_MODE_NONE: break;
                            case SEL_MODE_SHAPE:
                                if(vwk->spset) { /* start point set */
                                    /* store endpoint */
                                    vwk->ep[PLANE_TO_J(plane)] = j + 1;
                                    vwk->ep[PLANE_TO_I(plane)] = i + 1;
                                    vwk->ep[PLANE_TO_K(plane)] = k + 1;
                                    /* clear shape */
                                    grid_to_win(GUI->viewport,&wx1,&wy1,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                                    grid_to_win(GUI->viewport,&wx2,&wy2,vwk->ep[PLANE_TO_J(plane)]-1,vwk->ep[PLANE_TO_I(plane)]-1);
                                    _draw_shape_line(GUI->viewport,shape,wx1,wy1,wx2,wy2);
                                    /* restore start point */
                                    restore_image_voxel(GUI->viewport,GUI->selection,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                                    /* unset start and end points */
                                    vwk->spset = vwk->epset = false;
                                    /* perform selection (unset) */
                                    sel_shape(ENV,SEL_OP_UNSET);
                                    /* hide shape selection tooltip */
                                    _update_sel_shape_tooltip(GUI->viewport,ulen,false);
                                }
                                break;
                            case SEL_MODE_BRUSH:
                                /* store mouse position */
                                vwk->oldpos = j + i*nx;
                                /* perform selection (unset) */
                                sel_brush(ENV,brush,brush_size,j,i,k,SEL_OP_UNSET);
                                break;
                            case SEL_MODE_LEVEL:
                                /* perform selection (unset) */
                                sel_level(ENV,j,i,sel_scope,SEL_OP_UNSET);
                                break;
                            case SEL_MODE_COMPONENT:
                                /* perform selection (unset) */
                                sel_level_comp(ENV,j,i,sel_scope,SEL_OP_UNSET);
                                break;
                            case SEL_MODE_RANDOM:
                                /* store mouse position */
                                vwk->oldpos = j + i*nx;
                                /* perform selection (unset) */
                                sel_random(ENV,SEL_OP_UNSET);
                                break;
                            case SEL_MODE_FILL:
                                /* perform selection (unset) */
                                sel_fill(ENV,j,i,sel_scope,SEL_OP_UNSET);
                                break;
                            case SEL_MODE_MARKER:
                                /* perform selection (unset) */
                                sel_del_marker(ENV,j,i,k);
                                break;
                            case SEL_MODE_CONTOUR:
                                /* perform selection (unset) */
                                sel_contour(ENV,j,i,sel_scope,SEL_OP_UNSET,false);
                                break;
                            case SEL_MODE_ISOCOMPONENT:
                                /* perform selection (unset) */
                                sel_contour(ENV,j,i,sel_scope,SEL_OP_UNSET,true);
                                break;
                        }
                        /* update selection info */
                        sel_sanity_check(GUI->viewport,GUI->selection);
                        sel_update_info(GUI->viewport,GUI->selection);
                    } else {
                        if(QUANTIZATION_MODE(GUI->quantization) != QUANT_INTERVALS) return FL_IGNORE;

                        /* change cursor */
                        fl_set_cursor(win,XC_diamond_cross);
                        /* store mouse window position */
                        vwk->xold = x; vwk->yold = y;
                        /* set leveling */
                        vwk->level = true;
                    }
                    /* update status */
                    _update_status(ENV);
                    break;
                case FL_SCROLLUP_MOUSE:
                    /* change slice +1 */
                    fl_set_slider_value(GUI->mainview->slider,k+1);
                    fl_call_object_callback(GUI->mainview->slider);
                    break;
                case FL_SCROLLDOWN_MOUSE:
                    /* change slice -1 */
                    fl_set_slider_value(GUI->mainview->slider,k-1);
                    fl_call_object_callback(GUI->mainview->slider);
                    break;
            }
            break;
        case MotionNotify:
            /* get mouse position in window*/
            fl_get_win_mouse(win,&x,&y,&keymask);
            /* get mouse position in grid */
            win_to_grid(GUI->viewport,x,y,&j,&i);

            /* if mouse outside canvas, do nothing */
            if(x < 0 || x >= ww || y < 0 || y >= wh) return FL_IGNORE;

            if(measure) {
                if((xev->xmotion.state & ShiftMask) && (vwk->spset || vwk->epset)) {
                    FL_Coord wx,wy;

                    if(vwk->spset)
                        grid_to_win(GUI->viewport,&wx,&wy,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                    else if(vwk->epset)
                        grid_to_win(GUI->viewport,&wx,&wy,vwk->ep[PLANE_TO_J(plane)],vwk->ep[PLANE_TO_I(plane)]);

                    if(abs(wy - y) < abs(wx - x))
                        fl_set_win_mouse(win,x,wy); // snap horizontally
                    else
                        fl_set_win_mouse(win,wx,y); // snap vertically
                }

                /* update measurement info (with mouse over canvas) */
                update_measurement_live(GUI->measure,GUI->viewport,j,i,k);
            } else if(selmode != SEL_MODE_NONE) {
                if(selmode == SEL_MODE_BRUSH) {
                    /* if only left button pressed */
                    if((xev->xmotion.state) & Button1Mask && !((xev->xmotion.state) & Button3Mask)) {
                        if(j != jold || i != iold)
                            sel_brush(ENV,brush,brush_size,j,i,k,SEL_OP_SET);
                        vwk->oldpos = j + i*nx;
                        sel_update_info(GUI->viewport,GUI->selection);
                    }
                    /* if only right button pressed */
                    else if((xev->xmotion.state) & Button3Mask && !((xev->xmotion.state) & Button1Mask)) {
                        if(j != jold || i != iold)
                            sel_brush(ENV,brush,brush_size,j,i,k,SEL_OP_UNSET);
                        vwk->oldpos = j + i*nx;
                        sel_update_info(GUI->viewport,GUI->selection);
                    }
                } else if(selmode == SEL_MODE_RANDOM) {
                    /* if only left button pressed */
                    if((xev->xmotion.state) & Button1Mask && !((xev->xmotion.state) & Button3Mask)) {
                        if(j != jold || i != iold)
                            sel_random(ENV,SEL_OP_SET);
                        vwk->oldpos = j + i*nx;
                        sel_update_info(GUI->viewport,GUI->selection);
                    }
                    /* if only right button pressed */
                    else if((xev->xmotion.state) & Button3Mask && !((xev->xmotion.state) & Button1Mask)) {
                        if(j != jold || i != iold)
                            sel_random(ENV,SEL_OP_UNSET);
                        vwk->oldpos = j + i*nx;
                        sel_update_info(GUI->viewport,GUI->selection);
                    }
                } else if(selmode == SEL_MODE_SHAPE) {
                    if(vwk->spset) { /* start point set (selection ongoing) */
                        /* get start point window position */
                        grid_to_win(GUI->viewport,&wx1,&wy1,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                        if(vwk->moved == false) { /* plane changed */
                            vwk->ep[PLANE_TO_K(plane)] = k + 1; /* store new end point slice */
                            /* redraw shape */
                            grid_to_win(GUI->viewport,&wx2,&wy2,vwk->ep[PLANE_TO_J(plane)]-1,vwk->ep[PLANE_TO_I(plane)]-1);
                            _draw_shape_line(GUI->viewport,shape,wx1,wy1,wx2,wy2);
                        }
                        if(j != jold || i != iold) { /* if moved */
                            if(vwk->epset) { /* end point set (shape drawn) */
                                /* clear shape */
                                grid_to_win(GUI->viewport,&wx2,&wy2,vwk->ep[PLANE_TO_J(plane)]-1,vwk->ep[PLANE_TO_I(plane)]-1);
                                _draw_shape_line(GUI->viewport,shape,wx1,wy1,wx2,wy2);
                            }
                            /* store new end point */
                            vwk->ep[PLANE_TO_J(plane)] = j + 1;
                            vwk->ep[PLANE_TO_I(plane)] = i + 1;
                            /* get new mouse position in window */
                            grid_to_win(GUI->viewport,&wx2,&wy2,j,i);
                            /* draw shape */
                            _draw_shape_line(GUI->viewport,shape,wx1,wy1,wx2,wy2);
                        }
                        /* store mouse position */
                        vwk->oldpos = j + i*nx;
                        /* update shape selection tooltip */
                        _update_sel_shape_tooltip(GUI->viewport,ulen,true);
                    }
                }
            } else {
                if(vwk->slide) { /* sliding */
                    fl_set_slider_value(GUI->mainview->slider,k+(vwk->yold-y)); /* inverted y axis */
                    fl_call_object_callback(GUI->mainview->slider);
                    /* store y mouse window position */
                    vwk->yold = y;
                } else if(vwk->xhair) { /* crosshair being drawn */
                    if(j != jold || i != iold) { /* if moved */
                        /* clear old crosshair */
                        _draw_xhair(GUI->viewport,jold,iold);
                        /* draw new crosshair */
                        _draw_xhair(GUI->viewport,j,i);
                    }
                    /* store mouse grid position */
                    vwk->oldpos = j + i*nx;
                } else if(vwk->pan) { /* panning */
                    const bool fbh = formbrowser_overflow(GUI->mainview->fb,false);
                    const bool fbv = formbrowser_overflow(GUI->mainview->fb,true);
                    if(fbh || fbv) {
                        /* get formbrowser offsets */
                        int fbox = fl_get_formbrowser_xoffset(GUI->mainview->fb);
                        int fboy = fl_get_formbrowser_yoffset(GUI->mainview->fb);
                        /* get mouse position in toplevel window (not formbrowser's) */
                        fl_get_win_mouse(FL_ObjWin(GUI->mainview->fb),&x,&y,&keymask);
                        if(fbh && x != vwk->xold)
                            fl_set_formbrowser_xoffset(GUI->mainview->fb,fbox + (x < vwk->xold ? -zoom_x : zoom_x));
                        if(fbv && y != vwk->yold)
                            fl_set_formbrowser_yoffset(GUI->mainview->fb,fboy + (y < vwk->yold ? -zoom_y : zoom_y));
                        fl_redraw_object(GUI->mainview->fb);
                        /* store mouse toplevel (not formbrowser's) window position */
                        vwk->xold = x; vwk->yold = y;
                    }
                } else if(vwk->level) { /* leveling */
                    if(x != vwk->xold) {
                        FL_OBJECT *slider = GUI->quant_rng_minmax->sli_min;
                        double min,max;
                        fl_get_slider_bounds(slider,&min,&max);
                        double val = fl_get_slider_value(slider);
                        int sign = x < vwk->xold ? -1 : 1;
                        val += sign * (max-min)/100.0;
                        fl_set_slider_value(slider,val);
                        fl_call_object_callback(slider);
                    }
                    if(y != vwk->yold) {
                        FL_OBJECT *slider = GUI->quant_rng_minmax->sli_max;
                        double min,max;
                        fl_get_slider_bounds(slider,&min,&max);
                        double val = fl_get_slider_value(slider);
                        int sign = y > vwk->yold ? -1 : 1; /* inverted y axis */
                        val += sign * (max-min)/100.0;
                        fl_set_slider_value(slider,val);
                        fl_call_object_callback(slider);
                    }
                    set_range_inpsli(ENV,true);
                    /* store mouse window position */
                    vwk->xold = x; vwk->yold = y;
                }
            }

            /* set moved XXX: WHY? */
            vwk->moved = true;
            /* update status */
            _update_status(ENV);
            break;
        case EnterNotify:
            if(measure) {
                fl_get_win_mouse(win,&x,&y,&keymask);
                win_to_grid(GUI->viewport,x,y,&j,&i);
                /* update measurement info (with mouse over canvas) */
                update_measurement_live(GUI->measure,GUI->viewport,j,i,k);
            } else if(selmode != SEL_MODE_NONE) {
                if(selmode == SEL_MODE_SHAPE) {
                    if(vwk->spset && vwk->moved) {
                        /* redraw shape */
                        grid_to_win(GUI->viewport,&wx1,&wy1,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                        grid_to_win(GUI->viewport,&wx2,&wy2,vwk->ep[PLANE_TO_J(plane)]-1,vwk->ep[PLANE_TO_I(plane)]-1);
                        _draw_shape_line(GUI->viewport,shape,wx1,wy1,wx2,wy2);
                        /* update shape selection tooltip */
                        _update_sel_shape_tooltip(GUI->viewport,ulen,true);
                    }
                } else if(selmode == SEL_MODE_BRUSH) {
                    /* set selection cursor */
                    sel_set_cursor(GUI->viewport,GUI->selection);
                }
            } else {
                if(vwk->xhair) /* crosshair being drawn */
                    fl_set_cursor(win,FL_INVISIBLE_CURSOR); /* hide cursor */
            }
            /* update status */
            _update_status(ENV);
            break;
        case LeaveNotify:
            if(measure) {
                update_measurement(GUI->measure,GUI->viewport);
            } else if(selmode != SEL_MODE_NONE) {
                if(selmode == SEL_MODE_SHAPE) {
                    if(vwk->spset) { /* start point set (selection ongoing) */
                        /* clear shape */
                        grid_to_win(GUI->viewport,&wx1,&wy1,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                        grid_to_win(GUI->viewport,&wx2,&wy2,vwk->ep[PLANE_TO_J(plane)]-1,vwk->ep[PLANE_TO_I(plane)]-1);
                        _draw_shape_line(GUI->viewport,shape,wx1,wy1,wx2,wy2);
                        /* hide shape selection tooltip */
                        _update_sel_shape_tooltip(GUI->viewport,ulen,false);
                    }
                }
            } else {
                if(vwk->xhair) /* crosshair being drawn */
                    fl_reset_cursor(win); /* show default cursor */
            }
            /* clear status */
            fl_set_object_label(GUI->mainview->status,"");
            /* clear positioners */
            _clear_view_positioners(GUI->viewport);
            break;
        case KeyPress:
            switch(XLookupKeysym((XKeyEvent*)xev,0)) {
                case XK_Escape:
                    if(measure) {
                        /* clear line profile */
                        fl_call_object_callback(GUI->measure->btn_clear);
                    } else if(selmode != SEL_MODE_NONE) {
                        if(selmode == SEL_MODE_SHAPE) {
                            if(vwk->spset) { /* ongoing selection */
                                /* unset start and end points */
                                vwk->spset = vwk->epset = false;
                                /* hide shape selection tooltip */
                                _update_sel_shape_tooltip(GUI->viewport,ulen,false);
                                refresh_canvas(ENV);
                            }
                        }                   
                    } else {
                        /* nothing */
                    }
                    break;
                case XK_Left:  fl_get_mouse(&x,&y,&keymask); fl_set_mouse(x - zoom_x,y); break;
                case XK_Right: fl_get_mouse(&x,&y,&keymask); fl_set_mouse(x + zoom_x,y); break;
                case XK_Up:    fl_get_mouse(&x,&y,&keymask); fl_set_mouse(x,y - zoom_y); break;
                case XK_Down:  fl_get_mouse(&x,&y,&keymask); fl_set_mouse(x,y + zoom_y); break;
                case XK_y:
                    { /* copy status to clipboard */
                        const char *label = fl_get_object_label(GUI->mainview->status);
                        fl_stuff_clipboard(GUI->mainview->status,0,label,strlen(label),NULL);
                    }
                    break;
                case XK_c:
                    /* clear slice selection */
                    fl_call_object_callback(GUI->selection->btn_clear_slice);
                    break;
                case XK_Next: // change slice +1
                    fl_set_slider_value(GUI->mainview->slider,k + 1);
                    fl_call_object_callback(GUI->mainview->slider);
                                            fl_get_win_mouse(win,&x,&y,&keymask);
                        win_to_grid(GUI->viewport,x,y,&j,&i);

                        if(measure) {
                            /* update measurement info (with mouse over canvas) */
                            update_measurement_live(GUI->measure,GUI->viewport,j,i,k + 1);
                        } else if(selmode != SEL_MODE_NONE) {
                            if(selmode == SEL_MODE_SHAPE) {
                                if(vwk->spset) { /* start point set (selection ongoing) */
                                    /* move endpoint to new slice */
                                    vwk->ep[PLANE_TO_K(plane)] = k + 1;
                                    /* redraw shape */
                                    grid_to_win(GUI->viewport,&wx1,&wy1,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                                    grid_to_win(GUI->viewport,&wx2,&wy2,vwk->ep[PLANE_TO_J(plane)]-1,vwk->ep[PLANE_TO_I(plane)]-1);
                                    _draw_shape_line(GUI->viewport,shape,wx1,wy1,wx2,wy2);
                                    vwk->moved = true; /* slice changed */
                                    /* update shape selection tooltip */
                                    _update_sel_shape_tooltip(GUI->viewport,ulen,true);
                                }
                            } else if(selmode == SEL_MODE_BRUSH) {
                                /* if only left button pressed */
                                if((xev->xmotion.state) & Button1Mask && !((xev->xmotion.state) & Button3Mask)) {
                                    /* draw selection (set) */
                                    sel_brush(ENV,brush,brush_size,j,i,k,SEL_OP_SET);
                                    sel_update_info(GUI->viewport,GUI->selection);
                                }
                                /* if only right button pressed */
                                else if((xev->xmotion.state) & Button3Mask && !((xev->xmotion.state) & Button1Mask)) {
                                    /* draw selection (unset) */
                                    sel_brush(ENV,brush,brush_size,j,i,k,SEL_OP_UNSET);
                                    sel_update_info(GUI->viewport,GUI->selection);
                                }
                            }
                        } else {
                            if(vwk->xhair) /* crosshair being drawn */
                                _draw_xhair(GUI->viewport,jold,iold); /* redraw crosshair */
                        }
                        /* update status */
                        _update_status(ENV);
                                       break;
                case XK_Prior: // change slice -1
                    fl_set_slider_value(GUI->mainview->slider,k - 1);
                    fl_call_object_callback(GUI->mainview->slider);
                                            fl_get_win_mouse(win,&x,&y,&keymask);
                        win_to_grid(GUI->viewport,x,y,&j,&i);

                        if(measure) {
                            /* update measurement info (with mouse over canvas) */
                            update_measurement_live(GUI->measure,GUI->viewport,j,i,k - 1);
                        } else if(selmode != SEL_MODE_NONE) {
                            if(selmode == SEL_MODE_SHAPE) {
                                if(vwk->spset) { /* start point set (selection ongoing) */
                                    /* move endpoint to new slice */
                                    vwk->ep[PLANE_TO_K(plane)] = k - 1;
                                    /* redraw shape */
                                    grid_to_win(GUI->viewport,&wx1,&wy1,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                                    grid_to_win(GUI->viewport,&wx2,&wy2,vwk->ep[PLANE_TO_J(plane)]-1,vwk->ep[PLANE_TO_I(plane)]-1);
                                    _draw_shape_line(GUI->viewport,shape,wx1,wy1,wx2,wy2);
                                    vwk->moved = true; /* slice changed */
                                    /* update shape selection tooltip */
                                    _update_sel_shape_tooltip(GUI->viewport,ulen,true);
                                }
                            } else if(selmode == SEL_MODE_BRUSH) {
                                /* if only left button pressed */
                                if((xev->xmotion.state) & Button1Mask && !((xev->xmotion.state) & Button3Mask)) {
                                    /* draw selection (set) */
                                    sel_brush(ENV,brush,brush_size,j,i,k,SEL_OP_SET);
                                    sel_update_info(GUI->viewport,GUI->selection);
                                }
                                /* if only right button pressed */
                                else if((xev->xmotion.state) & Button3Mask && !((xev->xmotion.state) & Button1Mask)) {
                                    /* draw selection (unset) */
                                    sel_brush(ENV,brush,brush_size,j,i,k,SEL_OP_UNSET);
                                    sel_update_info(GUI->viewport,GUI->selection);
                                }
                            }
                        } else {
                            if(vwk->xhair) /* crosshair being drawn */
                                _draw_xhair(GUI->viewport,jold,iold); /* redraw crosshair */
                        }
                        /* update status */
                        _update_status(ENV);
                                       break;
            }
            break;
        case ClientMessage: // X11-based IPC (20-byte messages)
        {
            char cmd[5] = { 0 }; memcpy(cmd,xev->xclient.data.b,4);  // four-letter instruction
            uint8_t dat[16]; memcpy(dat,xev->xclient.data.b + 4,16); // sixteen-byte data
            if(0);
            else if(strcmp(cmd,"NEXT") == 0) {
                fl_set_slider_value(GUI->mainview->slider,k + 1);
                fl_call_object_callback(GUI->mainview->slider);
                                        fl_get_win_mouse(win,&x,&y,&keymask);
                        win_to_grid(GUI->viewport,x,y,&j,&i);

                        if(measure) {
                            /* update measurement info (with mouse over canvas) */
                            update_measurement_live(GUI->measure,GUI->viewport,j,i,k + 1);
                        } else if(selmode != SEL_MODE_NONE) {
                            if(selmode == SEL_MODE_SHAPE) {
                                if(vwk->spset) { /* start point set (selection ongoing) */
                                    /* move endpoint to new slice */
                                    vwk->ep[PLANE_TO_K(plane)] = k + 1;
                                    /* redraw shape */
                                    grid_to_win(GUI->viewport,&wx1,&wy1,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                                    grid_to_win(GUI->viewport,&wx2,&wy2,vwk->ep[PLANE_TO_J(plane)]-1,vwk->ep[PLANE_TO_I(plane)]-1);
                                    _draw_shape_line(GUI->viewport,shape,wx1,wy1,wx2,wy2);
                                    vwk->moved = true; /* slice changed */
                                    /* update shape selection tooltip */
                                    _update_sel_shape_tooltip(GUI->viewport,ulen,true);
                                }
                            } else if(selmode == SEL_MODE_BRUSH) {
                                /* if only left button pressed */
                                if((xev->xmotion.state) & Button1Mask && !((xev->xmotion.state) & Button3Mask)) {
                                    /* draw selection (set) */
                                    sel_brush(ENV,brush,brush_size,j,i,k,SEL_OP_SET);
                                    sel_update_info(GUI->viewport,GUI->selection);
                                }
                                /* if only right button pressed */
                                else if((xev->xmotion.state) & Button3Mask && !((xev->xmotion.state) & Button1Mask)) {
                                    /* draw selection (unset) */
                                    sel_brush(ENV,brush,brush_size,j,i,k,SEL_OP_UNSET);
                                    sel_update_info(GUI->viewport,GUI->selection);
                                }
                            }
                        } else {
                            if(vwk->xhair) /* crosshair being drawn */
                                _draw_xhair(GUI->viewport,jold,iold); /* redraw crosshair */
                        }
                        /* update status */
                        _update_status(ENV);
                               }
            else if(strcmp(cmd,"PREV") == 0) {
                fl_set_slider_value(GUI->mainview->slider,k - 1);
                fl_call_object_callback(GUI->mainview->slider);
                                        fl_get_win_mouse(win,&x,&y,&keymask);
                        win_to_grid(GUI->viewport,x,y,&j,&i);

                        if(measure) {
                            /* update measurement info (with mouse over canvas) */
                            update_measurement_live(GUI->measure,GUI->viewport,j,i,k - 1);
                        } else if(selmode != SEL_MODE_NONE) {
                            if(selmode == SEL_MODE_SHAPE) {
                                if(vwk->spset) { /* start point set (selection ongoing) */
                                    /* move endpoint to new slice */
                                    vwk->ep[PLANE_TO_K(plane)] = k - 1;
                                    /* redraw shape */
                                    grid_to_win(GUI->viewport,&wx1,&wy1,vwk->sp[PLANE_TO_J(plane)],vwk->sp[PLANE_TO_I(plane)]);
                                    grid_to_win(GUI->viewport,&wx2,&wy2,vwk->ep[PLANE_TO_J(plane)]-1,vwk->ep[PLANE_TO_I(plane)]-1);
                                    _draw_shape_line(GUI->viewport,shape,wx1,wy1,wx2,wy2);
                                    vwk->moved = true; /* slice changed */
                                    /* update shape selection tooltip */
                                    _update_sel_shape_tooltip(GUI->viewport,ulen,true);
                                }
                            } else if(selmode == SEL_MODE_BRUSH) {
                                /* if only left button pressed */
                                if((xev->xmotion.state) & Button1Mask && !((xev->xmotion.state) & Button3Mask)) {
                                    /* draw selection (set) */
                                    sel_brush(ENV,brush,brush_size,j,i,k,SEL_OP_SET);
                                    sel_update_info(GUI->viewport,GUI->selection);
                                }
                                /* if only right button pressed */
                                else if((xev->xmotion.state) & Button3Mask && !((xev->xmotion.state) & Button1Mask)) {
                                    /* draw selection (unset) */
                                    sel_brush(ENV,brush,brush_size,j,i,k,SEL_OP_UNSET);
                                    sel_update_info(GUI->viewport,GUI->selection);
                                }
                            }
                        } else {
                            if(vwk->xhair) /* crosshair being drawn */
                                _draw_xhair(GUI->viewport,jold,iold); /* redraw crosshair */
                        }
                        /* update status */
                        _update_status(ENV);
                               }
        }
            break;
    }

    return FL_IGNORE;
}
